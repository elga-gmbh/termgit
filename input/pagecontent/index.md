> **For the english version please click [here](index_en.md).**


### Willkommen auf dem österreichischen e-Health Terminologiebrowser!

Der österreichische e-Health Terminologiebrowser ist die erste Anlaufstelle für Terminologien in Österreich. Er stellt alle notwendigen Codesysteme, Value-Sets und Concept-Maps zur Verfügung, die in der gesamten österreichischen e-Health Landschaft verwendet werden können. Dadurch wird es IT-Systemen ermöglicht, dieselben Terminologien im selben Zeitraum zu verwenden um Informationen auszutauschen.


### Überblick

Der österreichische e-Health Terminologiebrowser bietet die folgenden Ressourcen: 

- [Codesysteme](codesystems.html)
- [Value-Sets](valuesets.html)
- [Concept-Maps](conceptmaps.html)
- and [Andere Ressourcen](artifacts.html), die mit den gehosteten Terminologien in Zusammenhang stehen.

Die folgenden Links bieten zusätzliche Informationen über

- die [Governance](index.md#governance) der Terminologien.
- den [technologischen Hintergrund](technical_documentation_de.md) mit Angaben zu 
  - der [Architektur von TerminoloGit](architecture_de.md).
  - der [Einrichtung (Setup)](setup_de.md).
  - [häufigen Anwendungsfällen (Use Cases)](use_cases_de.md).
  - den unterstützen [Dateiformate](file_formats_de.md).
  - den [UI Funktionen](ui_features_de.md).
- die [Lizenz und rechtliche Bedingungen](support_de.md#lizenz-und-rechtliche-bedingungen).


### Governance
Dieser Abschnitt beschreibt die Governance von Terminologien für ELGA und die österreichische e-Health-Landschaft. Innerhalb von ELGA und den österreichischen e-Health-Systemen wird eine Vielzahl von unterschiedlichen Terminologien aus verschiedenen internationalen, nationalen und lokalen Quellen mit unterschiedlichen Strukturen verwendet. Der Austrian e-Health TerminoloGit (auch TerminoloGit oder TermGit genannt) dient dazu, diese in eine möglichst einheitliche Form zu bringen und hier und in den ergänzenden Systemen verfügbar zu machen (siehe [österreichischer e-Health TerminoloGit](index.md#österreichischer-e-health-terminologit)).

#### Terminologie-Typen

Bei den Terminologien wird zwischen Codesystemen, Value-Sets und Concept-Maps unterschieden.

##### Codesysteme

Unter einem Codesystem versteht man die Sammlung von Codes, deren Metadaten und die Metadaten des Codesystems. Codesysteme können auch eine Ontologie, eine Tabelle oder eine einfache Aufzählung sein. [LOINC](https://loinc.org/), [SNOMED CT](https://www.snomed.org/) und [HL7® CodeSystems](https://www.hl7.org/fhir/codesystem.html) sind Beispiele für solche Codesysteme.
	
Die Aktualisierung der Codesysteme und deren Zeitpunkt oder Häufigkeit liegt in der Verantwortung der Ersteller und damit meist außerhalb des Einflussbereichs von ELGA. Codesysteme können nur dann außerhalb der Verantwortung der Ersteller korrigiert werden, wenn ein technisches Problem vorliegt. 
Wann immer möglich, sollte es vermieden werden, bestehende externe Codesysteme zu duplizieren und ganz oder teilweise auf TerminoloGit zu hosten. Entsprechende selbst gehostete Codesysteme auf TerminoloGit werden nach und nach außer Betrieb genommen, und die Codes der zugehörigen Value-Sets werden direkt auf das ursprüngliche Codesystem referenziert.

Ein Codesystem wird nicht nur durch einen canonical URI, sondern auch durch eine OID eindeutig identifiziert. Die Bedeutung eines Codes kann nur zusammen mit dem canonical URI oder der OID des Codesystems entschlüsselt werden.

Ein Grundsatz der Terminologieentwicklung ist, dass Codes nicht gelöscht und auch die Bedeutung eines Codes nicht verändert werden darf [^1]. Solche Änderungen führen zu einer Inkonsistenz bei der Verwendung von älteren codierten Daten mit der aktuellen Version des Codesystems. Üblicherweise werden solche „inkompatiblen“ Versionsstände des Codesystems mit unterschiedlichen canonical URIs und OIDs versehen, um sie voneinander unterscheiden zu können (z. B. die jährlichen Versionen von ICD-10). 

##### Value-Sets

Wo immer durch z.B. ELGA CDA-Implementierungsleitfäden eine Werteauswahl getroffen wird, wird ein passendes Value-Set ausgewählt bzw. definiert und mit seinem canonical URI und OID angegeben. ELGA Value-Sets beziehen sich, mit wenigen Ausnahmen, auf eine bestimmte „Version“ eines oder mehrerer Codesysteme, die durch den canonical URI und die OID des Codesystems identifiziert werden können. Dadurch werden die Value-Sets unabhängig von Änderungen in den Codesystemen. Darüber hinaus können Value-Sets eine eigene Reihenfolge und Baumstruktur (Hierarchie) enthalten.
Sämtliche in österreichischen Implementierungsleitfäden verwendeten Value-Sets werden auf dem österreichischen e-Health TerminoloGit veröffentlicht. Für fast alle Value-Sets gilt, dass sie extensional gehalten werden, d.h. die Konzepte sind explizit im Value-Set aufgelistet und nicht dynamisch mit z.B. Filtern über das Codesystem verknüpft. Dadurch sind für extensional gehaltene Value-Sets Expansions stets vorhanden.
Wie häufig Value-Sets geändert werden, hängt von der jeweiligen Anwendung ab. Value-Sets, die für „strukturelle“ Elemente (z.B. im CDA-Header, XDS-Metadaten) benötigt werden, ändern sich selten. „Inhaltliche“ Value-Sets (z. B. für Arzneimittellisten, Laboranalysen, Diagnosen-Codierungen) werden sich entsprechend häufig ändern müssen. Hier ist mit jährlichen bis wöchentlichen Änderungen zu rechnen. In Ausnahmefällen kann zum Zeitpunkt der Erstellung festgelegt werden, dass ein Value-Set nicht geändert oder versioniert werden darf (ValueSet.immutable wird auf true gesetzt).

Der Inhalt von Value-Sets kann sich ändern, der Name und die OID eines Value-Sets bleiben aber gleich. Wird eine neue Version erstellt, so werden die Versionsnummer mit Veröffentlichungsdatum (in `ValueSet.version` mit z.B.: `2.3.1+20220618`) und das Änderungsdatum (`ValueSet.date`) angegeben. Damit kann die Gültigkeit zu einer bestimmten Zeit rekonstruiert werden.
Value-Sets bleiben solange gültig, bis eine neuere Version dieses Value-Sets existiert oder es außer Kraft gesetzt wird. Eine neue Version ist ab dem Datum des Uploads gültig, es sei denn, der Status des Value-Sets wurde auf Entwurf gesetzt und das Änderungsdatum (`ValueSet.date`) liegt in der Zukunft. Wenn Codes nicht mehr verwendet werden dürfen, wird eine neue Version des Value-Sets erstellt, in der die nicht mehr zu verwendenden Codes auf `inactive=true` gesetzt werden. Inaktivierte Codes werden grundsätzlich nicht aus einem Value-Set herausgenommen, sondern verbleiben dort aus historischen Gründen.

##### Value-Set Binding

Für ELGA und die österreichischen eHealth-Systeme gilt grundsätzlich eine DYNAMISCHE Bindung an Value-Sets. Das bedeutet, dass immer die aktuell am Terminologieserver publizierte Version eines Value-Sets anzuwenden ist.
Value-Sets können auch STATISCH an ein Code-Element gebunden werden. In Implementierungsleitfäden wird dies durch die Angabe des Value-Sets mit Name/URI, OID, Version und Datum (ValueSet.date) gekennzeichnet.

##### Concept-Maps

Mit Concept-Maps können Beziehungen zwischen Terminologien hergestellt werden. So können beispielsweise die Werte einer lokalen Klassifikation auf die entsprechenden Werte einer internationalen Klassifikation abgebildet oder zwei gemeinsame Terminologien miteinander verknüpft werden (z. B. SNOMED-CT und Orphanet). Mappings erfolgen in eine Richtung, vom Quell- zum Zielsystem. In vielen Fällen sind auch die umgekehrten Zuordnungen gültig, doch kann dies nicht als gegeben vorausgesetzt werden.

Jedes Mapping von einem Ausgangskonzept zu einem Zielkonzept enthält ein Beziehungselement, das die semantische Entsprechung zwischen den beiden beschreibt, z. B. „äquivalent“ (equivalent) oder „subsumiert“ (subsumed). In manchen Fällen gibt es kein gültiges Mapping.

Wie bei Codesystemen sollte es vermieden werden, bestehende externe Concept-Maps zu duplizieren. Sie sollten selbst auf TerminoloGit gehosted werden (z. B. SNOMED-Refsets).


#### Terminologie-Updates

Wenn eine Terminologie aktualisiert wird, gilt die folgende Governance.

> Beachten Sie, dass Terminologien nicht aktualisiert werden sollten, während Integrationstests laufen, bei denen Inhalte verwendet werden. Die Terminologieverwalter sollten in diesem Fall vom technischen Personal benachrichtigt werden.

##### Versionisierung

Für die Versionierung von Terminologien wie Codesysteme oder Value-Sets gilt folgendes 
Versionisierungsschema:

    MAJOR.MINOR.PATCH+YYYYMMDD

Für einzelne Terminologien wie Codesysteme oder Value-Sets gilt:
- Wenn sich die Intention oder die Definition der Terminologie ändert, wird eine neue Terminologie mit eigener [Canonical URL](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.url) und OID veröffentlicht.
   - Wenn sich die Identität eines Konzepts ändert, der Code aber gleich bleibt, z. B. ICD-10, muss eine neue Terminologie mit eigenem canonical URI und OID erstellt werden.
 - `MAJOR` wird erhöht, wenn z.B.:
   - der [Codes eines Attributs (= Property)](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.code) sich ändert;
   - die [URI](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.uri), der [Datentyp](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.type) oder das dahinterliegende Regelwerk eines Attributs, [wie in der Beschreibung festgehalten](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.description), sich ändern;
   - Attribute entfernt werden;
   - die [Canonical URI des Codesystems, aus welchem ein Code stammt](https://hl7.org/fhir/R4B/valueset-definitions.html#ValueSet.compose.include.system), sich ändert;
 - `MINOR` wird erhöht, wenn z.B.:
   - ein [Konzept](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.concept) hinzugefügt wird (der Wertebereich ändert sich und muss vom Implementierer in seiner Implementierung aktualisiert werden);
   - ein Konzept [deprecated/retired](https://hl7.org/fhir/R4B/codesystem.html#status)/[inactive](https://hl7.org/fhir/R4B/valueset-definitions.html#ValueSet.expansion.contains.inactive) gesetzt wird;
   - der [Inhalt eines Attributs](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.concept.property.value_x_) geändert wird, ohne dass sich die Bedeutung des Konzepts ändert, z.B. `ELGA_Gültigkeit=false` anstelle von `ELGA_Gültigkeit=true`;
   - der [Display-Name](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.concept.display) eines Konzepts geändert wird;
   - die Struktur/Gruppierung sich ändert, z.B. die [Hierarchie](https://hl7.org/fhir/R4B/valueset.html#hierarchy) beim Value-Set;
   - ein neues [Attribut](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property) ergänzt wird;
   - die [Beschreibung eines Attributs](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.description) sich ändert, aber das zugrundeliegende Regelwerk gleich bleibt;
   - Metadaten einer Terminologie sich ändern (z.B. die [Beschreibung](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.description)), aber die Bedeutung der Terminologie gleich bleibt;
- `PATCH` wird erhöht, wenn z.B.: 
   - Rechtschreibfehler oder Tippfehler in den Attributen (nicht DisplayName oder Code) oder Metadaten korrigiert werden.
- `YYYYMMDD` entspricht dem Datum, an dem die Terminologie aktualisiert wurde. 
   - Idealerweise entspricht dieses Datum den Elementen [ValueSet.date](https://hl7.org/fhir/R4B/valueset-definitions.html#ValueSet.date) oder [Codesystem.date](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.date). Dies ist aber nicht verpflichtend, z.B. für Terminologien, die in der Zukunft Gültigkeit erlangen sollen.

Für [ConceptMaps](https://hl7.org/fhir/R5/conceptmap.html) sind die obengenannten Punkte sinngemäß umzusetzen.

###### Abrufen älterer Terminologieversionen
Die Terminologien, die über die Registerkarten [Value Sets](valuesets.html), [Code Systems](codesystems.html) und [ConceptMaps](conceptmaps.html) angezeigt und abgerufen werden können, stellen immer die neuesten Versionen dar.

Alle veröffentlichten Versionen können unter der Registerkarte `Previous Versions` abgerufen werden. Wenn alte Versionen angezeigt werden, wird eine Warnung angezeigt, die darauf hinweist, dass es sich um eine alte Version handelt. Dieser Hinweis enthält auch einen Link zur aktuellen Version der betreffenden Terminologie.


##### Auslaufende Terminologien

Terminologien, die nicht mehr aktiv gepflegt werden, können in den "Ruhestand" versetzt werden, indem ihr Status auf `retired` gesetzt wird (z.B. `ValueSet.status=retired`). Solche Terminologien sollten nicht mehr verwendet werden, sind aber aus historischen Gründen noch auf TerminoloGit verfügbar.

##### Statusübergangsdiagramm für Terminologien
[![status_life_cycle_for_terminologies](input/images/status_life_cycle_for_terminologies.drawio.png){: style="width: 100%"}](input/images/status_life_cycle_for_terminologies.drawio.png)

- `status = draft`: Nur bestimmte Benutzer dürfen Terminologien erstellen, ändern oder importieren. Terminologieadministratoren können Terminologien ändern und technisch freigeben. Neu erstellte oder importierte Terminologien oder Terminologieversionen können den Status `draft` haben, wenn sie nicht in der Produktion verwendet werden sollen.
- `status = active`: Die erstellten oder geänderten Terminologien stehen nach einer Freigabe durch einen Terminologieadministrator den Terminologie-Consumern zur Verfügung und können in der Produktion verwendet werden.
- `status = retired`: Diese Terminologien dürfen nicht (mehr) verwendet werden.
- `status = unknown`: Dieser Status wird in TerminoloGit nicht verwendet.

##### Statusübergangsdiagramm für Konzepte

Das folgende Statusübergangsdiagramm gilt für alle Konzepte in Codesystemen oder Value-Sets:

[![status_life_cycle_for_concepts](input/images/status_life_cycle_for_concepts.png){: style="width: 100%"}](input/images/status_life_cycle_for_concepts.png)

###### Ausnahme für Terminologien von externen Terminologiebesitzern
Das angegebene Versionisierungsschema ist für die österreichische Governance festgelegt und weicht von der ursprünglichen FHIR-Spezifikation hinsichtlich des inaktiven Status im Wertebereich ab. Daher kann es sein, dass es für Terminologien, die von externen Terminologiebesitzern (z.B. HL7®) gepflegt werden, nicht anwendbar ist.

##### Informationen über Aktualisierungen

Aktualisierungsmeldungen zu Terminologien werden im [Extranet des Semantic Competence Center](https://confluence.elga.gv.at/display/SCCTERM) veröffentlicht (eine kostenlose Registrierung ist erforderlich). Darüber hinaus ist ein [automatischer Import von Terminologien](use_cases_de.md#automatischer-import-von-terminologien) oder ein [manueller Import von Terminologien](use_cases_de.md#manueller-import-von-terminologien) möglich.

> Beachten Sie, dass eine Prüfung auf aktualisierte Terminologien mindestens **wöchentlich** erfolgen sollte.

#### Rollen

Der österreichische e-Health TerminoloGit wird von den folgenden Benutzergruppen verwendet:

| Benutzergruppe | Aufgabe | GitLab.com Rolle | Beschreibung |
| --- | --- | --- | ---|
| Terminologie-Consumer | Lesezugriff | keine, nicht erforderlich | Als Terminologie-Consumer ist für die Anzeige und den Export von Terminologien bzw. das Überprüfen auf neue Versionen keine Selbstregistrierung erforderlich. |
| Terminologie-Verantwortlicher | Terminologie-Wartung | Developer | Die Anlage oder Aktualisierung von Terminologien wird vom Terminologie-Verantwortlichen durchgeführt. Jede Terminologie wird von mindestens einem anderen Terminologie-Verantwortlichen geprüft. |
| Terminologie-Authentifizierer | Terminologiefreigabe | Developer | Der Terminologie-Authentifizierer gibt jede neu erstellte oder aktualisierte Terminologie im Auftrag des österreichischen Gesundheitsministeriums frei. |
| Terminologie-Administrator | Freigabe und Publikation | Maintainer | Bevor Terminologien öffentlich abrufbar werden, erfolgt eine Publikation durch einen Terminologie-Administrator. |

#### Publikationsprozess für Terminologien externer Herausgeber

Für die Veröffentlichung von Terminologien, die von ELGA-externen Terminologieverwaltern für das Austrian e-Health TerminoloGit erstellt werden, ist folgender Prozess definiert.

##### Voraussetzungen

- Kenntnisse über Git oder GitLab werden vorausgesetzt.
- Die gewünschte Terminologie unterliegt keinen externen Lizenzbestimmungen.
- Die gewünschte Terminologie besitzt bereits eine eigene OID und optional einen eigenen Namen. Wenn es noch keine OID für die Terminologie gibt, kann sie über die [OID-Plattform](https://www.gesundheit.gv.at/OID_Frontend/oid-frontend/application.htm?section=5) angefordert werden.
- Die auf dieser Seite erläuterte Governance und die Verfahren sind bekannt.
- Die gewünschte Terminologie wird für Interoperabilitätszwecke verwendet und erfüllt Mindestanforderungen an die semantische Qualität (z. B. Verwendung internationaler Codesysteme, keine Replikation bestehender Terminologien usw.).

##### Vorgehen

1. Kontaktieren Sie die ELGA GmbH über die E-Mail-Adresse [cda@elga.gv.at](mailto:cda@elga.gv.at) und fügen Sie das **[ausgefüllte Antragsformular](input/files/Applicationform_external_terminology_developer_de.pdf)** bei.
2. Das ausgefüllte Formular wird von der ELGA GmbH technisch und rechtlich geprüft.
3. Nach erfolgreicher Prüfung, [erstellen Sie einen Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#create-a-fork) von [TerminoloGit](https://gitlab.com/elga-gmbh/termgit).
4. Innerhalb des Forks legen Sie einen neuen Branch an, der die Informationen über die neue Terminologie enthält.
5. Für die erstmalige Erstellung der Importdatei werden **Importvorlagen** für **[Codesystem](input/files/CodeSystem-cs-import-template.1.propcsv.xlsx)** oder **[Value-Set](input/files/ValueSet-vs-import-template.1.propcsv.xlsx)** in Form von XLSX-Dateien bereitgestellt. Die erforderlichen Attribute sind im Abschnitt [Proprietary CSV](file_formats_de.md#proprietäres-csv-propcsv---csv-und-xlsx) beschrieben.
   1. Das Hochladen in das GitLab-Projekt kann über verschiedene Wege erfolgen (z.B. GitLab WebIDE oder Software wie Sourcetree). Für Fragen und Unterstützung kann sich der externe Terminologieverwalter an das TerminoloGit-Team wenden ([cda@elga.gv.at](mailto:cda@elga.gv.at)).
   2. Thematisch zusammenhängende Terminologien sollten zur Wahrung der Übersichtlichkeit gemeinsam in einem Zweig bearbeitet werden.
   3. Commits müssen der Git-Governance entsprechen: [https://cbea.ms/git-commit/](https://cbea.ms/git-commit/)
6. Nach Fertigstellung der Terminologie erstellen Sie einen Merge Request vom Fork in das [TerminoloGit](https://gitlab.com/elga-gmbh/termgit) Repository.
   1. Die semantische und technische Überprüfung wird von der ELGA GmbH durchgeführt.
   2. Wenn die Anforderungen erfüllt sind, wird der Branch mit dem default Branch zusammengeführt.
7. Nach erfolgreichem Durchlauf der Pipelines wird geprüft, ob die gewünschten Terminologien korrekt und vollständig veröffentlicht wurden.
8. Abschließend wird der Terminologie-Verantwortliche über die Veröffentlichung informiert und ein Update der [Semantic Competence Center changelist](https://confluence.elga.gv.at/display/SCCTERM/) veröffentlicht.


### TerminoloGit in Österreich
Der österreichische e-Health Terminologiebrowser basiert auf einem GitLab.com Repository und wird durch ein sogenanntes [*tergi*](https://gitlab.com/elga-gmbh/terminologit-tergi) ergänzt, das ein gespiegeltes Git Repository sowie einen FHIR® Server bereitstellt. Zusammen bilden sie das **Austrian e-Health TerminoloGit**. Damit gibt es mehrere Möglichkeiten, auf die gehosteten Terminologien zuzugreifen und sie abzurufen.

> Beachten Sie, dass der Zugriff auf die Ressourcen von *tergi* auch über die sicheren Netze der österreichischen e-Health Infrastruktur (eHiNet / Healix und GIN) möglich ist.


|  | Internet | Sicheres Netz |
|---|---|---|
| Österreichischer e-Health Terminologie-Browser | [https://termgit.elga.gv.at/](https://termgit.elga.gv.at/) | nicht verfügbar |
| GitLab.com Repository | [https://gitlab.com/elga-gmbh/termgit](https://gitlab.com/elga-gmbh/termgit) | nicht verfügbar |
| *tergi* Git Repository | [https://tergi.elga.gv.at/git-server/api/v1/repos/tergi/terminologit/](https://tergi.elga.gv.at/git-server/api/v1/repos/tergi/terminologit/) | [https://tergi.elga-services.at/git-server/api/v1/repos/tergi/terminologit/](https://tergi.elga-services.at/git-server/api/v1/repos/tergi/terminologit/) |
| *tergi* FHIR® Server | [https://tergi.elga.gv.at/fhir-server/api/v4/](https://tergi.elga.gv.at/fhir-server/api/v4/) | [https://tergi.elga-services.at/fhir-server/api/v4/](https://tergi.elga-services.at/fhir-server/api/v4/) |

#### GitLab Projekt Überblick

Der Inhalt des [österreichischen e-Health Terminologiebrowsers](https://termgit.elga.gv.at/) basiert auf den folgenden zwei GitLab Projekten:

| Projektbezeichnung | Repository URL | Beschreibung | GitLab Project ID | GitLab Seiten URL |
| --- | --- | --- | --- | --- |
| TerminoloGit | [https://gitlab.com/elga-gmbh/termgit](https://gitlab.com/elga-gmbh/termgit) | Repository für veröffentlichte, aktuelle Terminologien (Dateiformate).<br/><br/>*Hinweis:* Der Inhalt der letzten erfolgreichen Pipeline eines beliebigen Git-Branches dieses Repositories wird unter der angegebenen GitLab URL angezeigt. | 33179072 | [https://elga-gmbh.gitlab.io/termgit](https://elga-gmbh.gitlab.io/termgit) |
| TerminoloGit HTML | [https://gitlab.com/elga-gmbh/termgit-html](https://gitlab.com/elga-gmbh/termgit-html) | Repository für die statischen HTML-Seiten, erzeugt vom HL7® FHIR® IG Publisher basierend auf dem `main` branch von [https://gitlab.com/elga-gmbh/termgit](https://gitlab.com/elga-gmbh/termgit). | 33179017 | **[https://termgit.elga.gv.at](https://termgit.elga.gv.at)** |

#### Einschränkungen der Nutzung
Das österreichische e-Health TerminoloGit ist in erster Linie ein System zur Bereitstellung von Codesystemen, Value-Sets und Concept-Maps. Es ist nicht dazu gedacht, Echtzeitzugriff für die Verarbeitung durch Informationssysteme zu bieten. Es kann derzeit nicht als Online-Ressource (zur Überprüfung einzelner Werte) verwendet werden, sondern als Quelle für Terminologien, die lokal abgerufen und gespeichert werden können. Wenn eine solche Funktionalität benötigt wird, sollten Sie die Einrichtung einer eigenen [*tergi*](https://gitlab.com/elga-gmbh/terminologit-tergi) Instanz in Betracht ziehen.

#### Entsprechung zum alten Terminologieserver

Der alte Terminologieserver wurde bis Oktober 2022 betrieben. Wenn bestimmte alte Versionen von Terminologien (vor 2022) benötigt werden, können diese beim CDA-Team angefordert werden [cda@elga.gv.at](mailto:cda@elga.gv.at).

Derzeit werden die folgenden Download-Formate unter der Registerkarte 'Download' der jeweiligen Terminologie angeboten:

| *neuer* Terminologieserver | *alter* Terminologieserver | Notiz |
| ----------- | ----------- | ----------- |
| FHIR R4 xml `.4.fhir.xml` | keine Entsprechung       | |
| FHIR R4 json `.4.fhir.json`   | keine Entsprechung        | |
| fsh v1 `.1.fsh`   | keine Entsprechung        | |
| fsh v2 `.2.fsh`   | keine Entsprechung        | |
| ClaML v2 `.2.claml.xml`   | ClaML        | |
| ClaML v3 `.3.claml.xml`   | keine Entsprechung        | |
| propCSV v1 `.1.propcsv.csv`   | keine Entsprechung        | |
| spreadCT v1 | `.1.spreadct.xlsx` |  keine Entsprechung        | |
| SVSextELGA v1 `.1.svsextelga.xml`   | SVS        | **Warnung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen<br/>enthalten, die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar. |
| SVSextELGA v2 `.2.svsextelga.xml`   | SVS        | **Warnung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen<br/>enthalten, die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar. <br/>Es sind jedoch alle Konzepteigenschaften (concept properties) verfügbar. |
| outdatedCSV v1 `.1.outdatedcsv.csv`   | CSV nach dem TermPub-Standard        | **Warnung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen<br/>enthalten, die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar. |
| outdatedCSV v2 `.2.outdatedcsv.csv`   | CSV wie es <br/>von TermPub bereitgestellt wird <br/>(basierend auf TermPub Standard).        | **Warnung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen<br/>enthalten, die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar. <br/>Es sind jedoch alle Konzepteigenschaften (concept properties) verfügbar. |

Eine genauere Beschreibung der einzelnen Download-Formate finden Sie [in der technischen Dokumentation](file_formats_de.md).

### Referenzen

[^1]:Konzept-Permanenz (Concept Permanence), beschrieben in „Cimino’s Desiderata“ (Desiderata for Controlled Medical Vocabularies in the TwentyFirst Century; 1998; [http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3415631/](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3415631/))