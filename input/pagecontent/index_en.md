> **Für die deutsche Version bitte [hier](index.md) klicken.**

### Welcome to the Austrian e-Health Terminology Browser!

The Austrian e-Health Terminology Browser is the first point of contact regarding terminologies in Austria. It provides all necessary code systems, value sets and concept maps which may be used throughout the Austrian e-Health landscape. By this, IT systems are enabled to exchange information using the same terminologies at the same time.

### Overview

The Austrian e-Health Terminology Browser provides the following resources:

- [code systems](codesystems.html)
- [value sets](valuesets.html)
- [concept maps](conceptmaps.html)
- and [other resources](artifacts.html) which are related to the hosted terminologies.

The following links provide additional information about

- the [governance](index_en.md#governance) of terminologies.
- the [technological background](technical_documentation_en.md) specifying
  - the [architecture of TerminoloGit](architecture_en.md).
  - the [setup](setup_en.md).
  - [common use cases](use_cases_en.md).
  - the supported [file formats](file_formats_en.md).
  - the [UI features](ui_features_en.md).
- the [license and legal terms](support_en.md#license-and-legal-terms).



### Governance

This section describes the management of terminologies for ELGA and the Austrian e-Health landscape. Within ELGA and the Austrian e-Health systems, a large number of different terminologies from various international, national and local sources with different structures are used. The Austrian e-Health TerminoloGit (also known as TerminoloGit or TermGit) is used to bring them into as uniform a form as possible and make them available here and in the complementing systems (see [Austrian e-Health TerminoloGit](index_en.md#austrian-e-health-terminologit)).

#### Terminology types

For terminologies, a distinction is made between code systems, value sets and concept maps.

##### Code systems

A code system is understood to be the collection of codes, their metadata and the metadata of the code system. Code systems can also be an ontology, table, or simple enumeration. [LOINC](https://loinc.org/), [SNOMED CT](https://www.snomed.org/), and [HL7® CodeSystems](https://www.hl7.org/fhir/codesystem.html) are examples of such code systems.

The updating of code systems and their timing or frequency is the responsibility of the creators and thus mostly beyond the control of ELGA. Code systems can only be corrected outside the responsibility of the creators if there is a technical problem. Whenever possible, it should be avoided to duplicate existing external code systems and host them fully or partially on TerminoloGit. Corresponding self-hosted code systems on TerminoloGit are gradually retired and the codes of the associated value sets are referenced directly to the original code system.

A code system is uniquely identified not only by a canonical URI but also by an OID. The meaning of a code can only be deciphered together with the canonical URI or OID of the code system.

A principle of terminology development is that codes must not be deleted, nor must the meaning of a code be changed [^1]. Such changes create an inconsistency in the use of older coded data with the current version of the code system. Typically, such "incompatible" version states of the code system are given different canonical URIs and OIDs to keep them apart (e.g., the annual versions of ICD-10).

##### Value sets

Wherever a value selection is made for e.g. ELGA CDA implementation guides, a suitable value set is selected or defined and specified with its canonical URI and OID. ELGA value sets refer, with a few exceptions, to a specific "version" of one or more code systems which can be identified by the  canonical URI and OID of the code system. This makes the value sets independent of changes in the code systems. In addition, value sets can contain their own sequence and tree structure (hierarchy).

All value sets used in Austrian implementation guides are published on the Austrian e-Health TerminoloGit. It applies to almost all value sets that they are extensionally specified, which means that the concepts are explicitly listed in the value set and are not dynamically linked with e.g. filters to the corresponding code system. For the extensionally specified value sets the expansion is always present.

The frequency of changes of a value set depends on its respective application. Value sets that are needed for "structural" elements (e.g. in the CDA header, XDS metadata) will rarely change. "Content" value sets (e.g. for drug lists, laboratory analyses, diagnosis codes) will have to change correspondingly frequently. Regarding the latter, annual to weekly changes are to be expected. In exceptional cases, it is possible to specify that a value set must not be changed or versioned at the time of creation (ValueSet.immutable is set to true).

Contents of value sets may change, but the canonical URI and OID of a value set remain the same. For new versions, the version number with release date (in `ValueSet.version` with e.g.: `2.3.1+20220618`) and the modification date (`ValueSet.date`) are specified. This allows to reconstruct the validity at a certain time.

Value sets remain valid until a newer version of this value sets exists or its retirement. A new version is valid from date of the upload unless the status of the value set has been set to draft and the modification date (`ValueSet.date`) is in the future. If codes are not allowed to be used any more, a new version of the value set is created, in which the codes not to be used any more are set to `inactive=true`. Inactivated codes will principally not be withdrawn from a value set but remain there for historical reasons.

##### Value Set Binding

For ELGA and the Austrian e-Health systems, a DYNAMIC binding to value sets applies in principle. This means that the version of a value set currently published within the Austrian e-Health TerminoloGit must always be used.

Value Sets can also be STATICALLY bound to a code element. In implementation guides this is indicated by identifying the value set by its canoncial or OID and specifying its version and/or date.

##### Concept maps

Relationships between terminologies can be established with concept maps. For example, the values of a local classification can be mapped to the corresponding values of an international classification, or two common terminologies can be linked (e.g., SNOMED-CT and Orphanet). Mappings are one way, from the source to the target system. In many cases, the reverse mappings are valid, but this cannot be assumed to be the case.

Each mapping from a source concept to a target concept includes a relationship element describing the semantic correspondence between the two, e.g., "equivalent" or "subsumed". In some cases, there is no valid mapping.

As with code systems, it should be avoided to duplicate existing external concept maps and self-host them on TerminoloGit (e.g. SNOMED refsets).

#### Terminology updates

When a terminology is updated, the following governance applies.

> Note, that terminologies should not be updated while integration tests are running where content is used. Terminology managers should be alerted by technical staff in this case.

##### Versioning

The following versioning scheme applies to the versioning of terminologies such as code systems or value sets:

    MAJOR.MINOR.PATCH+YYYYMMDD

For terminologies such as code systems or value sets the following applies:

- If the intention or definition of the terminology changes, a new terminology with its own [canonical URL](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.url) and OID is published.
  - If the identity of a concept changes but the code remains the same, e.g. ICD-10, then a new terminology with its own canonical URI and OID must be created.
- `MAJOR` is incremented if e.g:
  - the [code of a property](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.code) changes;
  - the [URI](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.uri), the [data type](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.type) or the underlying set of rules [as conveyed in the description](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.description) of a property change;
  - properties are removed;
  - the [canonical URI of the code system from which a code comes from](https://hl7.org/fhir/R4B/valueset-definitions.html#ValueSet.compose.include.system) changes 
- `MINOR` is incremented if e.g:
  - a [concept](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.concept) is added (the value range changes and must be updated by the implementer in its implementation);
  - a concept is set to [deprecated/retired](https://hl7.org/fhir/R4B/codesystem.html#status)/[inactive](https://hl7.org/fhir/R4B/valueset-definitions.html#ValueSet.expansion.contains.inactive);
  - the [value of a concept's property](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.concept.property.value_x_) changes and the meaning of the concept does not change, e.g. `ELGA_Gültigkeit=false` instead of `ELGA_Gültigkeit=true`;
  - the [display name](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.concept.display) of a concept is changed;
  - structure/grouping in a value set changes, e.g. the [hierarchical expansion](https://hl7.org/fhir/R4B/valueset.html#hierarchy);
  - a new [property](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property) is added;
  - the [property's description](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.property.description) changes without changing the underlying set of rules
  - metadata of a terminology changes (e.g. [description](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.publisher)) without changing the meaning of the terminology;
- `PATCH` is incremented if e.g:
  - spelling mistakes or typos in a property (not display name or code) or metadata are corrected;
- `YYYYMMDD` corresponds to the date on which the terminology was updated.
  - Ideally, this date corresponds to the element [ValueSet.date](https://hl7.org/fhir/R4B/valueset-definitions.html#ValueSet.date) or [Codesystem.date](https://hl7.org/fhir/R4B/codesystem-definitions.html#CodeSystem.date) but does not have to, e.g. for terminologies valid in the future.

For [ConceptMaps](https://hl7.org/fhir/R5/conceptmap.html), the above points are to be implemented analogously.


###### Retrieving older terminology versions
The terminologies that can be displayed and accessed via the [value sets](valuesets.html), [code systems](codesystems.html) and [concept maps](conceptmaps.html) tab always represent the most recent versions.

All published versions can be retrieved under the tab `Previous Versions`. When old versions are displayed, a warning is shown highlighting that this is an old version. This notice also includes a link to the current version of that terminology.


##### Retiring terminologies

Terminologies which are not actively being maintained any more may be retired by setting their status to `retired` (e.g. `ValueSet.status=retired`). Such terminologies should not be used any more but are still available on TerminoloGit for historical reasons.

##### Status transition diagram for terminologies

The following status transition diagram applies to all terminologies:

[![status_life_cycle_for_terminologies](input/images/status_life_cycle_for_terminologies.drawio.png){: style="width: 100%"}](input/images/status_life_cycle_for_terminologies.drawio.png)

- `status = draft`: Only certain users are allowed to create, modify or import terminologies. Terminology administrators can modify and technically release terminologies. Newly created or newly imported terminologies or terminology versions can have the status `draft` if they are not supposed to be used in production.
- `status = active`: The created or modified terminologies are available to terminology consumers after a release by a terminology administrator and may be used in production.
- `status = retired`: These terminologies may not be used (any more).
- `status = unknown`: This status is not used within TerminoloGit.

##### Status transition diagram for concepts

The following status transition diagram applies to all concepts in code systems or value sets:

[![status_life_cycle_for_concepts](input/images/status_life_cycle_for_concepts.png){: style="width: 100%"}](input/images/status_life_cycle_for_concepts.png)

###### Exception for terminologies of external terminology owners

The specified versioning scheme is set for the Austrian governance and differs from the original FHIR specification concerning the inactive status in the value set. Therefore, it may not be applicable to terminologies which are maintained by external terminology owners (e.g. HL7®).

##### Information about updates

Update messages regarding terminologies are published at the [Extranet of the Semantic Competence Center](https://confluence.elga.gv.at/display/SCCTERM) (a free of charge registration is necessary). In addition, an [automatic import of terminologies](use_cases_en.md#automatic-import-of-terminologies) or a [manual import of terminologies](use_cases_en.md#manual-import-of-terminologies) is possible.

> Note, that checks for updated terminologies should be performed at least **weekly**.

#### Roles

The Austrian e-Health TerminoloGit is used by the following user groups:

| user group | task | GitLab.com role | description |
| --- | --- | --- | ---|
| Terminology Consumer | reading access | none, not required | As a terminology consumer, no self-registration is required for viewing and exporting terminologies or checking for new versions. |
| Terminology Manager | terminology maintenance | Developer | The creation or update of terminologies is done by a terminology manager. Each terminology is checked by at least one other terminology manager. |
| Terminology Authenticator | terminology clearance​ | Developer | The terminology authenticator clears each newly created or updated terminology on behalf of the Austrian ministry of health. |
| Terminology Administrator | release and publication | Maintainer | Before terminologies become publicly available, they are released by a terminology administrator. |

#### Publication process for terminologies created by external publishers

The following process is defined for the publication of terminologies created by ELGA-external terminology managers for the Austrian e-Health TerminoloGit.

##### Requirements

- Knowledge of Git or GitLab is assumed to be known.
- The desired terminology is not subject to external licensing requirements.
- The desired terminology already has its own OID and optionally its own canonical URI. If there is no OID for the terminology yet, it can be requested via the [OID platform](https://www.gesundheit.gv.at/OID_Frontend/oid-frontend/application.htm?section=5).
- The governance and procedures explained on this page are familiar.
- The desired terminology is used for interoperability purposes and fulfils minimum semantic quality requirements (e.g. use of international code systems, no replication of existing terminologies, etc.).

##### Procedure

1. Contact ELGA GmbH via e-mail address [cda@elga.gv.at](mailto:cda@elga.gv.at) having the **[completed application form](input/files/Applicationform_external_terminology_developer_de.pdf)** (currently available in German only) attached.
2. The completed form will be checked technically and legally by ELGA GmbH.
3. After successful verification, [create a fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#create-a-fork) of [TerminoloGit](https://gitlab.com/elga-gmbh/termgit).
4. Within the fork, create a new branch which will contain the information about the new terminology.
5. For the initial creation of the import file, **import templates** for **[code system](input/files/CodeSystem-cs-import-template.1.propcsv.xlsx)** or **[value set](input/files/ValueSet-vs-import-template.1.propcsv.xlsx)** are provided in the form of XLSX files. The required attributes are described in the section [Proprietary CSV](file_formats_en.md#proprietary-csv-propcsv---csv-and-xlsx).
   1. Uploading to the GitLab project can be done via different ways (e.g. GitLab WebIDE or software like Sourcetree). For questions and support, the external terminology manager can contact the TerminoloGit team ([cda@elga.gv.at](mailto:cda@elga.gv.at)).
   2. Thematically related terminologies should be edited together in one branch to maintain clarity.
   3. Commits must comply with Git governance: [https://cbea.ms/git-commit/](https://cbea.ms/git-commit/)
6. After completion of the terminology, create a merge request from the fork into the [TerminoloGit](https://gitlab.com/elga-gmbh/termgit) repository.
   1. The semantic and technical review is performed by the ELGA GmbH.
   2. If the requirements are met, the branch is merged to the default branch.
7. After the pipelines have been successfully run, a check is made to ensure that the desired terminologies have been published correctly and completely.
8. Finally, the terminology manager is informed about the publication and an update of the [Semantic Competence Center changelist](https://confluence.elga.gv.at/display/SCCTERM/) is published.

### TerminoloGit in Austria

The Austrian e-Health Terminology Browser is based on a GitLab.com repository and is complemented by a so-called [*tergi*](https://gitlab.com/elga-gmbh/terminologit-tergi) which provides a mirrored Git repository as well as a FHIR® server. Together they form the **Austrian e-Health TerminoloGit**. As a result, there exist several ways how the hosted terminologies may be accessed and retrieved.

> Note, the *tergi's* resources can also be accessed from within the secure networks within the Austrian e-Health infrastructure (eHiNet / Healix and GIN).

|  | Internet | Secure Network |
|---|---|---|
| Austrian e-Health Terminology Browser | [https://termgit.elga.gv.at/](https://termgit.elga.gv.at/) | not available |
| GitLab.com repository | [https://gitlab.com/elga-gmbh/termgit](https://gitlab.com/elga-gmbh/termgit) | not available |
| *tergi* Git repository | [https://tergi.elga.gv.at/git-server/api/v1/repos/tergi/terminologit/](https://tergi.elga.gv.at/git-server/api/v1/repos/tergi/terminologit/) | [https://tergi.elga-services.at/git-server/api/v1/repos/tergi/terminologit/](https://tergi.elga-services.at/git-server/api/v1/repos/tergi/terminologit/) |
| *tergi* FHIR® server | [https://tergi.elga.gv.at/fhir-server/api/v4/](https://tergi.elga.gv.at/fhir-server/api/v4/) | [https://tergi.elga-services.at/fhir-server/api/v4/](https://tergi.elga-services.at/fhir-server/api/v4/) |


#### GitLab project overview

The content of the [Austrian e-Health Terminology Browser](https://termgit.elga.gv.at/) is based on the following two GitLab projects:

| Project Name | Repository URL | Description | GitLab Project ID | GitLab Pages URL |
| --- | --- | --- | --- | --- |
| TerminoloGit | [https://gitlab.com/elga-gmbh/termgit](https://gitlab.com/elga-gmbh/termgit) | Repository for published, up-to-date terminologies (file formats).<br/><br/>*Note:* The content of the last successful pipeline of any Git branch of this repository is displayed under the specified GitLab Pages URL. | 33179072 | [https://elga-gmbh.gitlab.io/termgit](https://elga-gmbh.gitlab.io/termgit) |
| TerminoloGit HTML | [https://gitlab.com/elga-gmbh/termgit-html](https://gitlab.com/elga-gmbh/termgit-html) | Repository for the static HTML pages created by the HL7® FHIR® IG Publisher based on the `main` branch of [https://gitlab.com/elga-gmbh/termgit](https://gitlab.com/elga-gmbh/termgit). | 33179017 | **[https://termgit.elga.gv.at](https://termgit.elga.gv.at)** |

#### Limits of use

The Austrian e-Health TerminoloGit is primarily a system for providing code systems, value sets and concept maps. It is not intended to provide real-time access for processing by information systems. It is currently not capable to be used as an online resource (for checking individual values), but as a source for terminologies to be retrieved and stored locally. If such a functionality is required, consider setting up your own [*tergi*](https://gitlab.com/elga-gmbh/terminologit-tergi).

#### Correspondence to the old terminology server

The old terminology server was operated until October 2022. If specific old versions of terminologies (prior to 2022) are sought, they can be requested from the CDA team [cda@elga.gv.at](mailto:cda@elga.gv.at).

Currently, the following download formats are offered under the 'Download' tab of the respective terminology:

| *new* Terminologieserver | *old* Terminologieserver | Note |
| ----------- | ----------- | ----------- |
| FHIR R4 xml `.4.fhir.xml` | no correspondence       | |
| FHIR R4 json `.4.fhir.json`   | no correspondence        | |
| fsh v1 `.1.fsh`   | no correspondence        | |
| fsh v2 `.2.fsh`   | no correspondence        | |
| ClaML v2 `.2.claml.xml`   | ClaML        | |
| ClaML v3 `.3.claml.xml`   | no correspondence        | |
| propCSV v1 `.1.propcsv.csv`   | no correspondence        | |
| spreadCT v1 | `.1.spreadct.xlsx` |  no correspondence        | |
| SVSextELGA v1 `.1.svsextelga.xml`   | SVS        | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. |
| SVSextELGA v2 `.2.svsextelga.xml`   | SVS        | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. <br/>However, all concept properties are available. |
| outdatedCSV v1 `.1.outdatedcsv.csv`   | CSV according to TermPub standard        | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. |
| outdatedCSV v2 `.2.outdatedcsv.csv`   | CSV as it is <br/>supplied by TermPub <br/>(based on TermPub standard).        | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. <br/>However, all concept properties are available. |

A more detailed description of each download format can be found [in the technical documentation](file_formats_en.md).


### References

[^1]:Concept Permanence, described in "Cimino's Desiderata" (Desiderata for Controlled Medical Vocabularies in the TwentyFirst Century; 1998; [http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3415631/](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3415631/))
