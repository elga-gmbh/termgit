> **Für die deutsche Version bitte [hier](support_de.md) klicken.**

Depending on the subject of support requested, please, choose one of the following channels.

### Technical issues
Suggestions for improvements or error messages detected during the use of **TerminoloGit** can be reported via the GitLab ticket system at **[https://gitlab.com/elga-gmbh/termgit-dev/-/issues/new](https://gitlab.com/elga-gmbh/termgit-dev/-/issues/new)**.

### Issues regarding terminologies
All questions regarding ELGA and Austrian e-Health relevant **terminologies** that cannot be answered with this documentation can be reported to **[cda@elga.gv.at](mailto:cda@elga.gv.at)**.

### ELGA
Questions regarding your ELGA and ELGA-Portal please contact **[info@elga-serviceline.at](mailto:info@elga-serviceline.at)** or **[+43 (0)50 124 4411](tel:+43501244411)**
(working days from 7am to 7pm).

### Imprint
The imprint of **[elga.gv.at/en/imprint](https://www.elga.gv.at/en/imprint/)** applies.

### Privacy Policy
The only in german available **[elga.gv.at/datenschutzerklaerung](https://www.elga.gv.at/datenschutzerklaerung/)** applies.

### License and Legal Terms

TerminoloGit is licensed under the GNU General Public License v3.0 or later - see the [LICENSE.md](https://gitlab.com/elga-gmbh/termgit-dev/-/blob/stable/LICENSE.md) file for details.

#### HL7®

HL7®, HEALTH LEVEL SEVEN® and FHIR® are trademarks owned by Health Level Seven International, registered with the United States Patent and Trademark Office.

This Implementation Guide contains and references intellectual property owned by third parties (“Third Party IP”). Acceptance of these License Terms does not grant any rights with respect to Third Party IP. The licensee alone is responsible for identifying and obtaining any necessary licenses or authorizations to utilize Third Party IP in connection with the specification or otherwise.

#### SNOMED CT®

Some terminologies contain information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the Member Licensing and Distribution Service (MLDS). [https://mlds.ihtsdotools.org/#/landing/AT?lang=de](https://mlds.ihtsdotools.org/#/landing/AT?lang=de)

#### IP Statements

Extracted automatically by the HL7® FHIR® IG publisher.

{% include ip-statements.xhtml %}
