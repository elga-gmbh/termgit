Instance: austrian-designation-use 
InstanceOf: ValueSet 
Usage: #definition 
* id = "austrian-designation-use" 
* url = "https://termgit.elga.gv.at/ValueSet/austrian-designation-use" 
* name = "austrian-designation-use" 
* title = "Austrian Designation Use" 
* status = #active 
* version = "2.2.0+20240402" 
* description = "Mögliche Designations für den österreichischen eHealth-Bereich" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.76" 
* date = "2024-04-02" 
* publisher = "ELGA GmbH" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"
* compose.include[0].version = "2.1.0+20240402"
* compose.include[0].concept[0].code = "hinweise"
* compose.include[0].concept[0].display = "hinweise"
* compose.include[0].concept[1].code = "concept_beschreibung"
* compose.include[0].concept[1].display = "concept_beschreibung"
* compose.include[0].concept[2].code = "relationships"
* compose.include[0].concept[2].display = "relationships"
* compose.include[0].concept[3].code = "einheit_codiert"
* compose.include[0].concept[3].display = "einheit_codiert"
* compose.include[0].concept[4].code = "einheit_print"
* compose.include[0].concept[4].display = "einheit_print"
* compose.include[0].concept[5].code = "alt"
* compose.include[0].concept[5].display = "alt"
* compose.include[0].concept[6].code = "alterslimit_min"
* compose.include[0].concept[6].display = "alterslimit_min"
* compose.include[0].concept[7].code = "alterslimit_max"
* compose.include[0].concept[7].display = "alterslimit_max"

* expansion.timestamp = "2024-04-04T10:08:38.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"
* expansion.contains[0].version = "2.1.0+20240402"
* expansion.contains[0].code = #hinweise
* expansion.contains[0].display = "hinweise"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"
* expansion.contains[1].version = "2.1.0+20240402"
* expansion.contains[1].code = #concept_beschreibung
* expansion.contains[1].display = "concept_beschreibung"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"
* expansion.contains[2].version = "2.1.0+20240402"
* expansion.contains[2].code = #relationships
* expansion.contains[2].display = "relationships"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"
* expansion.contains[3].version = "2.1.0+20240402"
* expansion.contains[3].code = #einheit_codiert
* expansion.contains[3].display = "einheit_codiert"
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"
* expansion.contains[4].version = "2.1.0+20240402"
* expansion.contains[4].code = #einheit_print
* expansion.contains[4].display = "einheit_print"
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"
* expansion.contains[5].version = "2.1.0+20240402"
* expansion.contains[5].code = #alt
* expansion.contains[5].display = "alt"
* expansion.contains[6].system = "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"
* expansion.contains[6].version = "2.1.0+20240402"
* expansion.contains[6].code = #alterslimit_min
* expansion.contains[6].display = "alterslimit_min"
* expansion.contains[7].system = "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"
* expansion.contains[7].version = "2.1.0+20240402"
* expansion.contains[7].code = #alterslimit_max
* expansion.contains[7].display = "alterslimit_max"
