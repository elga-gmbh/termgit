Instance: elga-gtelvogdarollen 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-gtelvogdarollen" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-gtelvogdarollen" 
* name = "elga-gtelvogdarollen" 
* title = "ELGA_GTelVoGDARollen" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230131" 
* description = "**Description:** In this code system, the roles from the Health Telematics Regulation 2013 (GTelV 2013) and supplementary roles that are not included in the regulation ,are represented.

**Beschreibung:** In diesem Codesystemen werden die Rollen aus der Gesundheitstelematikverordnung 2013 (GTelV 2013) und ergänzende Rollen, die nicht in der Verordnung enthalten sind, abgebildet." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.2" 
* date = "2020-09-18" 
* count = 61 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #10 "Teil 1: Rollen für Personen"
* #10 ^property[0].code = #child 
* #10 ^property[0].valueCode = #100 
* #10 ^property[1].code = #child 
* #10 ^property[1].valueCode = #101 
* #10 ^property[2].code = #child 
* #10 ^property[2].valueCode = #154 
* #10 ^property[3].code = #child 
* #10 ^property[3].valueCode = #155 
* #10 ^property[4].code = #child 
* #10 ^property[4].valueCode = #156 
* #10 ^property[5].code = #child 
* #10 ^property[5].valueCode = #158 
* #10 ^property[6].code = #child 
* #10 ^property[6].valueCode = #200 
* #10 ^property[7].code = #child 
* #10 ^property[7].valueCode = #201 
* #10 ^property[8].code = #child 
* #10 ^property[8].valueCode = #202 
* #10 ^property[9].code = #child 
* #10 ^property[9].valueCode = #203 
* #10 ^property[10].code = #child 
* #10 ^property[10].valueCode = #204 
* #10 ^property[11].code = #child 
* #10 ^property[11].valueCode = #205 
* #10 ^property[12].code = #child 
* #10 ^property[12].valueCode = #206 
* #10 ^property[13].code = #child 
* #10 ^property[13].valueCode = #207 
* #10 ^property[14].code = #child 
* #10 ^property[14].valueCode = #208 
* #10 ^property[15].code = #child 
* #10 ^property[15].valueCode = #209 
* #10 ^property[16].code = #child 
* #10 ^property[16].valueCode = #210 
* #10 ^property[17].code = #child 
* #10 ^property[17].valueCode = #211 
* #10 ^property[18].code = #child 
* #10 ^property[18].valueCode = #212 
* #10 ^property[19].code = #child 
* #10 ^property[19].valueCode = #213 
* #10 ^property[20].code = #child 
* #10 ^property[20].valueCode = #214 
* #10 ^property[21].code = #child 
* #10 ^property[21].valueCode = #215 
* #10 ^property[22].code = #child 
* #10 ^property[22].valueCode = #216 
* #100 "Ärztin/Arzt für Allgemeinmedizin"
* #100 ^definition = Ärztin/Arzt für Allgemeinmedizin, gegebenenfalls unter Beifügung der gemäß Ärztinnen-/Ärzte-Ausbildungsordnung 2006 (ÄAO 2006), BGBl. II Nr. 286/2006, in der jeweils geltenden Fassung, zutreffenden Zusatzbezeichnung des Additivfaches in runden Klammern
* #100 ^property[0].code = #parent 
* #100 ^property[0].valueCode = #10 
* #101 "Approbierte Ärztin/Approbierter Arzt"
* #101 ^property[0].code = #parent 
* #101 ^property[0].valueCode = #10 
* #154 "Fachärztin/Facharzt für Zahn-, Mund- und Kieferheilkunde"
* #154 ^property[0].code = #parent 
* #154 ^property[0].valueCode = #10 
* #155 "Zahnärztin/Zahnarzt"
* #155 ^property[0].code = #parent 
* #155 ^property[0].valueCode = #10 
* #156 "Dentistin/Dentist"
* #156 ^property[0].code = #parent 
* #156 ^property[0].valueCode = #10 
* #158 "Fachärztin/Facharzt"
* #158 ^definition = Fachärztin/Facharzt, unter Beifügung der gemäß ÄAO 2006 zutreffenden Berufsbezeichnung
* #158 ^property[0].code = #parent 
* #158 ^property[0].valueCode = #10 
* #200 "Psychotherapeutin/Psychotherapeut"
* #200 ^property[0].code = #parent 
* #200 ^property[0].valueCode = #10 
* #201 "Klinische Psychologin/Klinischer Psychologe"
* #201 ^property[0].code = #parent 
* #201 ^property[0].valueCode = #10 
* #202 "Gesundheitspsychologin/Gesundheitspsychologe"
* #202 ^property[0].code = #parent 
* #202 ^property[0].valueCode = #10 
* #203 "Musiktherapeutin/Musiktherapeut"
* #203 ^property[0].code = #parent 
* #203 ^property[0].valueCode = #10 
* #204 "Hebamme"
* #204 ^property[0].code = #parent 
* #204 ^property[0].valueCode = #10 
* #205 "Physiotherapeutin/Physiotherapeut"
* #205 ^property[0].code = #parent 
* #205 ^property[0].valueCode = #10 
* #206 "Biomedizinische Analytikerin/Biomedizinischer Analytiker"
* #206 ^property[0].code = #parent 
* #206 ^property[0].valueCode = #10 
* #207 "Radiologietechnologin/Radiologietechnologe"
* #207 ^property[0].code = #parent 
* #207 ^property[0].valueCode = #10 
* #208 "Diätologin/Diätologe"
* #208 ^property[0].code = #parent 
* #208 ^property[0].valueCode = #10 
* #209 "Ergotherapeutin/Ergotherapeut"
* #209 ^property[0].code = #parent 
* #209 ^property[0].valueCode = #10 
* #210 "Logopädin/Logopäde"
* #210 ^property[0].code = #parent 
* #210 ^property[0].valueCode = #10 
* #211 "Orthoptistin/Orthoptist"
* #211 ^property[0].code = #parent 
* #211 ^property[0].valueCode = #10 
* #212 "Diplomierte Gesundheits- und Krankenschwester/Diplomierter Gesundheits- und Krankenpfleger"
* #212 ^property[0].code = #parent 
* #212 ^property[0].valueCode = #10 
* #213 "Diplomierte Kinderkrankenschwester/Diplomierter Kinderkrankenpfleger"
* #213 ^property[0].code = #parent 
* #213 ^property[0].valueCode = #10 
* #214 "Diplomierte psychiatrische Gesundheits- und Krankenschwester/Diplomierter psychiatrischer Gesundheits- und Krankenpfleger"
* #214 ^property[0].code = #parent 
* #214 ^property[0].valueCode = #10 
* #215 "Heilmasseurin/Heilmasseur"
* #215 ^property[0].code = #parent 
* #215 ^property[0].valueCode = #10 
* #216 "Diplomierte Kardiotechnikerin/Diplomierter Kardiotechniker"
* #216 ^property[0].code = #parent 
* #216 ^property[0].valueCode = #10 
* #20 "Teil 2: Rollen für Organisationen"
* #20 ^property[0].code = #child 
* #20 ^property[0].valueCode = #300 
* #20 ^property[1].code = #child 
* #20 ^property[1].valueCode = #301 
* #20 ^property[2].code = #child 
* #20 ^property[2].valueCode = #302 
* #20 ^property[3].code = #child 
* #20 ^property[3].valueCode = #303 
* #20 ^property[4].code = #child 
* #20 ^property[4].valueCode = #304 
* #20 ^property[5].code = #child 
* #20 ^property[5].valueCode = #305 
* #20 ^property[6].code = #child 
* #20 ^property[6].valueCode = #306 
* #20 ^property[7].code = #child 
* #20 ^property[7].valueCode = #307 
* #20 ^property[8].code = #child 
* #20 ^property[8].valueCode = #309 
* #20 ^property[9].code = #child 
* #20 ^property[9].valueCode = #310 
* #20 ^property[10].code = #child 
* #20 ^property[10].valueCode = #311 
* #20 ^property[11].code = #child 
* #20 ^property[11].valueCode = #312 
* #20 ^property[12].code = #child 
* #20 ^property[12].valueCode = #313 
* #20 ^property[13].code = #child 
* #20 ^property[13].valueCode = #314 
* #20 ^property[14].code = #child 
* #20 ^property[14].valueCode = #315 
* #20 ^property[15].code = #child 
* #20 ^property[15].valueCode = #316 
* #20 ^property[16].code = #child 
* #20 ^property[16].valueCode = #317 
* #20 ^property[17].code = #child 
* #20 ^property[17].valueCode = #318 
* #20 ^property[18].code = #child 
* #20 ^property[18].valueCode = #319 
* #20 ^property[19].code = #child 
* #20 ^property[19].valueCode = #320 
* #20 ^property[20].code = #child 
* #20 ^property[20].valueCode = #321 
* #20 ^property[21].code = #child 
* #20 ^property[21].valueCode = #322 
* #20 ^property[22].code = #child 
* #20 ^property[22].valueCode = #400 
* #20 ^property[23].code = #child 
* #20 ^property[23].valueCode = #401 
* #20 ^property[24].code = #child 
* #20 ^property[24].valueCode = #403 
* #20 ^property[25].code = #child 
* #20 ^property[25].valueCode = #404 
* #20 ^property[26].code = #child 
* #20 ^property[26].valueCode = #405 
* #20 ^property[27].code = #child 
* #20 ^property[27].valueCode = #406 
* #20 ^property[28].code = #child 
* #20 ^property[28].valueCode = #407 
* #20 ^property[29].code = #child 
* #20 ^property[29].valueCode = #408 
* #20 ^property[30].code = #child 
* #20 ^property[30].valueCode = #500 
* #20 ^property[31].code = #child 
* #20 ^property[31].valueCode = #501 
* #300 "Allgemeine Krankenanstalt"
* #300 ^property[0].code = #parent 
* #300 ^property[0].valueCode = #20 
* #301 "Sonderkrankenanstalt"
* #301 ^property[0].code = #parent 
* #301 ^property[0].valueCode = #20 
* #302 "Pflegeanstalt"
* #302 ^property[0].code = #parent 
* #302 ^property[0].valueCode = #20 
* #303 "Sanatorium"
* #303 ^property[0].code = #parent 
* #303 ^property[0].valueCode = #20 
* #304 "Selbstständiges Ambulatorium"
* #304 ^property[0].code = #parent 
* #304 ^property[0].valueCode = #20 
* #305 "Pflegeeinrichtung"
* #305 ^property[0].code = #parent 
* #305 ^property[0].valueCode = #20 
* #306 "Mobile Pflege"
* #306 ^property[0].code = #parent 
* #306 ^property[0].valueCode = #20 
* #307 "Kuranstalt"
* #307 ^property[0].code = #parent 
* #307 ^property[0].valueCode = #20 
* #309 "Straf- und Maßnahmenvollzug"
* #309 ^property[0].code = #parent 
* #309 ^property[0].valueCode = #20 
* #310 "Untersuchungsanstalt"
* #310 ^property[0].code = #parent 
* #310 ^property[0].valueCode = #20 
* #311 "Öffentliche Apotheke"
* #311 ^property[0].code = #parent 
* #311 ^property[0].valueCode = #20 
* #312 "Gewebebank"
* #312 ^property[0].code = #parent 
* #312 ^property[0].valueCode = #20 
* #313 "Blutspendeeinrichtung"
* #313 ^property[0].code = #parent 
* #313 ^property[0].valueCode = #20 
* #314 "Augen- und Kontaktlinsenoptik"
* #314 ^property[0].code = #parent 
* #314 ^property[0].valueCode = #20 
* #315 "Hörgeräteakustik"
* #315 ^property[0].code = #parent 
* #315 ^property[0].valueCode = #20 
* #316 "Orthopädische Produkte"
* #316 ^property[0].code = #parent 
* #316 ^property[0].valueCode = #20 
* #317 "Zahntechnik"
* #317 ^property[0].code = #parent 
* #317 ^property[0].valueCode = #20 
* #318 "Rettungsdienst"
* #318 ^property[0].code = #parent 
* #318 ^property[0].valueCode = #20 
* #319 "Zahnärztliche Gruppenpraxis"
* #319 ^property[0].code = #parent 
* #319 ^property[0].valueCode = #20 
* #320 "Ärztliche Gruppenpraxis"
* #320 ^property[0].code = #parent 
* #320 ^property[0].valueCode = #20 
* #321 "Gewebeentnahmeeinrichtung"
* #321 ^property[0].code = #parent 
* #321 ^property[0].valueCode = #20 
* #322 "Arbeitsmedizinisches Zentrum"
* #322 ^property[0].code = #parent 
* #322 ^property[0].valueCode = #20 
* #400 "Gesundheitsmanagement"
* #400 ^property[0].code = #parent 
* #400 ^property[0].valueCode = #20 
* #401 "Öffentlicher Gesundheitsdienst"
* #401 ^property[0].code = #parent 
* #401 ^property[0].valueCode = #20 
* #403 "ELGA-Ombudsstelle"
* #403 ^property[0].code = #parent 
* #403 ^property[0].valueCode = #20 
* #404 "Widerspruchstelle"
* #404 ^property[0].code = #parent 
* #404 ^property[0].valueCode = #20 
* #405 "Patientenvertretung"
* #405 ^property[0].code = #parent 
* #405 ^property[0].valueCode = #20 
* #406 "Sozialversicherung"
* #406 ^property[0].code = #parent 
* #406 ^property[0].valueCode = #20 
* #407 "Krankenfürsorge"
* #407 ^property[0].code = #parent 
* #407 ^property[0].valueCode = #20 
* #408 "Gesundheitsversicherung"
* #408 ^property[0].code = #parent 
* #408 ^property[0].valueCode = #20 
* #500 "IKT-Gesundheitsservice"
* #500 ^property[0].code = #parent 
* #500 ^property[0].valueCode = #20 
* #501 "Verrechnungsservice"
* #501 ^property[0].code = #parent 
* #501 ^property[0].valueCode = #20 
* #30 "Ergänzende Rollen"
* #30 ^property[0].code = #child 
* #30 ^property[0].valueCode = #157 
* #30 ^property[1].code = #child 
* #30 ^property[1].valueCode = #217 
* #30 ^property[2].code = #child 
* #30 ^property[2].valueCode = #218 
* #157 "Ärztin/Arzt in Ausbildung"
* #157 ^property[0].code = #parent 
* #157 ^property[0].valueCode = #30 
* #217 "Pflegeassistentin/Pflegeassistent"
* #217 ^property[0].code = #parent 
* #217 ^property[0].valueCode = #30 
* #218 "Pflegefachassistentin/Pflegefachassistent"
* #218 ^property[0].code = #parent 
* #218 ^property[0].valueCode = #30 
