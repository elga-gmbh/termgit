Instance: medikationpackaging 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "medikationpackaging" 
* url = "https://termgit.elga.gv.at/CodeSystem/medikationpackaging" 
* name = "medikationpackaging" 
* title = "Medikation_Packaging" 
* status = #active 
* content = #complete 
* version = "1.4.0+20250121" 
* description = "**Description:** Medikation Packaging. Liability and terms of use you can find on this website https://www.basg.gv.at/fuer-unternehmen/online-service/nutzungshinweise#termgit

**Beschreibung:** Medikation Packaging. Haftung und Nutzungsbestimmungen finden Sie unter folgender Webseite https://www.basg.gv.at/fuer-unternehmen/online-service/nutzungshinweise#termgit" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.219" 
* date = "2025-01-21" 
* count = 37 
* #100000073490 "Ampulle"
* #100000073491 "Applikator"
* #100000073493 "Beutel"
* #100000073495 "Kanister"
* #100000073496 "Blisterpackung"
* #100000073497 "Flasche"
* #100000073498 "Schachtel"
* #100000073501 "Kanüle"
* #100000073503 "Patrone"
* #100000073509 "Streudose"
* #100000073512 "Tropfbehältnis"
* #100000073514 "Druckbehältnis"
* #100000073517 "Inhalator"
* #100000073523 "Weithalsgefäss"
* #100000073530 "Mehrdosenbehältnis"
* #100000073531 "Mehrdosenbehältnis mit gasfreiem Pumpsystem"
* #100000073541 "Gießbehältnis"
* #100000073543 "Fertigpen"
* #100000073544 "Fertigspritze"
* #100000073545 "Druckbehältnis"
* #100000073547 "Beutel"
* #100000073548 "Einritzer"
* #100000073550 "Einzeldosisbehältnis"
* #100000073553 "Spraydose"
* #100000073559 "Folienstreifen"
* #100000073560 "Tablettenbehältnis"
* #100000073561 "Tube"
* #100000073563 "Durchstechflasche"
* #100000116195 "Kalenderpackung"
* #100000137702 "Behältnis"
* #100000174066 "Einzeldosisblister"
* #200000005585 "Hülle"
* #900000000002 "Injektionsset (NE)"
* #900000000006 "Applikationssystem (NE)"
* #900000000007 "Tiegel (NE)"
* #900000000009 "Behältnis (NE)"
* #900000000011 "Generator (NE)"
