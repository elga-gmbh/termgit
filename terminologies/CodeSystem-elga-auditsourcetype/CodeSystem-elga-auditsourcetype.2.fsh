Instance: elga-auditsourcetype 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-auditsourcetype" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype" 
* name = "elga-auditsourcetype" 
* title = "ELGA_AuditSourceType" 
* status = #active 
* content = #complete 
* version = "1.2.0+20250113" 
* description = "ELGA Codeliste für autonome ELGA-Anwendungen und Services (relevant für das Berechtigungssystem)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.152" 
* date = "2025-01-13" 
* publisher = "ELGA GmbH" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 16 
* #0 "PAP"
* #0 ^definition = ELGA PAP
* #1 "KBS"
* #1 ^definition = ELGA Kontaktbestätigungsservice
* #10 "CDM"
* #10 ^definition = Zentrales Content Delete Management Service
* #11 "CDD"
* #11 ^definition = Dezentraler Content Delete Management Daemon
* #12 "e-Medikation"
* #13 "EMS Epidemiologisches Meldesystem/EPI-Service"
* #14 "DSUB Broker"
* #15 "DICOMweb"
* #15 ^definition = QIDO-SF bzw. WADO-SF
* #2 "ETS"
* #2 ^definition = ELGA Token Service
* #3 "Policy Repository"
* #4 "Bürgerportal"
* #5 "Policy Administraion"
* #5 ^definition = Tool für die Verwaltung allgemeiner Policies
* #6 "GDA"
* #6 ^definition = GDA System
* #7 "Gateway"
* #7 ^definition = Zwischengeschaltetes System, Proxy,...
* #8 "ZGF"
* #8 ^definition = Zugriffssteuerungsfassade eines ELGA Bereichs
* #9 "A-ARR"
* #9 ^definition = Aggregiertes Audit Record Repository
