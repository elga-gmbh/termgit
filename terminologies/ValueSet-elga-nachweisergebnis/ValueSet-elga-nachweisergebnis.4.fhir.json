{
  "resourceType": "ValueSet",
  "id": "elga-nachweisergebnis",
  "meta": {
    "profile": [
      "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset"
    ]
  },
  "url": "https://termgit.elga.gv.at/ValueSet/elga-nachweisergebnis",
  "identifier": [
    {
      "use": "official",
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.2.40.0.34.10.186"
    }
  ],
  "version": "2.1.0+20240325",
  "name": "elga-nachweisergebnis",
  "title": "ELGA_NachweisErgebnis",
  "status": "active",
  "date": "2024-03-25",
  "description": "**Description:** Result-Codes for presence or detection methods. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).\n\n**Beschreibung:** Ergebnis-Codes f\u00fcr Nachweis-Methoden. Enth\u00e4lt durch SNOMED International urheberrechtlich gesch\u00fctzte Information. Jede Verwendung von SNOMED CT in \u00d6sterreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in \u00d6sterreich statt und erf\u00fcllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen k\u00f6nnen \u00fcber das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden.",
  "compose": {
    "include": [
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.6.96"
          }
        ],
        "system": "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug",
        "version": "1.0.0+20230131",
        "concept": [
          {
            "code": "260411009",
            "display": "Presence findings (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "Vorhandensein"
              }
            ]
          },
          {
            "code": "260415000",
            "display": "Not detected (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "nicht nachgewiesen"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "neg."
              }
            ]
          },
          {
            "code": "260373001",
            "display": "Detected (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "nachgewiesen"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "pos. "
              }
            ]
          },
          {
            "code": "117363000",
            "display": "Ordinal value (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "Ordinalskala"
              }
            ]
          },
          {
            "code": "260413007",
            "display": "None (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "keine "
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "-"
              }
            ]
          },
          {
            "code": "89292003",
            "display": "Rare (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "vereinzelt"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "+"
              }
            ]
          },
          {
            "code": "57176003",
            "display": "Few (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "sp\u00e4rlich"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "++"
              }
            ]
          },
          {
            "code": "260354000",
            "display": "Moderate number (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "m\u00e4\u00dfig"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "+++"
              }
            ]
          },
          {
            "code": "260396001",
            "display": "Numerous (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "reichlich"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "++++"
              }
            ]
          }
        ]
      }
    ]
  },
  "expansion": {
    "timestamp": "2024-03-25T06:36:54+00:00",
    "contains": [
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.6.96"
          }
        ],
        "system": "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug",
        "abstract": true,
        "version": "1.0.0+20230131",
        "code": "260411009",
        "display": "Presence findings (qualifier value)",
        "designation": [
          {
            "language": "de-AT",
            "value": "Vorhandensein"
          }
        ],
        "contains": [
          {
            "extension": [
              {
                "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
                "valueOid": "urn:oid:2.16.840.1.113883.6.96"
              }
            ],
            "system": "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug",
            "version": "1.0.0+20230131",
            "code": "260415000",
            "display": "Not detected (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "nicht nachgewiesen"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "neg."
              }
            ]
          },
          {
            "extension": [
              {
                "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
                "valueOid": "urn:oid:2.16.840.1.113883.6.96"
              }
            ],
            "system": "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug",
            "version": "1.0.0+20230131",
            "code": "260373001",
            "display": "Detected (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "nachgewiesen"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "pos. "
              }
            ]
          }
        ]
      },
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.6.96"
          }
        ],
        "system": "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug",
        "abstract": true,
        "version": "1.0.0+20230131",
        "code": "117363000",
        "display": "Ordinal value (qualifier value)",
        "designation": [
          {
            "language": "de-AT",
            "value": "Ordinalskala"
          }
        ],
        "contains": [
          {
            "extension": [
              {
                "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
                "valueOid": "urn:oid:2.16.840.1.113883.6.96"
              }
            ],
            "system": "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug",
            "version": "1.0.0+20230131",
            "code": "260413007",
            "display": "None (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "keine "
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "-"
              }
            ]
          },
          {
            "extension": [
              {
                "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
                "valueOid": "urn:oid:2.16.840.1.113883.6.96"
              }
            ],
            "system": "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug",
            "version": "1.0.0+20230131",
            "code": "89292003",
            "display": "Rare (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "vereinzelt"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "+"
              }
            ]
          },
          {
            "extension": [
              {
                "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
                "valueOid": "urn:oid:2.16.840.1.113883.6.96"
              }
            ],
            "system": "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug",
            "version": "1.0.0+20230131",
            "code": "57176003",
            "display": "Few (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "sp\u00e4rlich"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "++"
              }
            ]
          },
          {
            "extension": [
              {
                "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
                "valueOid": "urn:oid:2.16.840.1.113883.6.96"
              }
            ],
            "system": "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug",
            "version": "1.0.0+20230131",
            "code": "260354000",
            "display": "Moderate number (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "m\u00e4\u00dfig"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "+++"
              }
            ]
          },
          {
            "extension": [
              {
                "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
                "valueOid": "urn:oid:2.16.840.1.113883.6.96"
              }
            ],
            "system": "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug",
            "version": "1.0.0+20230131",
            "code": "260396001",
            "display": "Numerous (qualifier value)",
            "designation": [
              {
                "language": "de-AT",
                "value": "reichlich"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "hinweise",
                  "display": "hinweise"
                },
                "value": "++++"
              }
            ]
          }
        ]
      }
    ]
  }
}