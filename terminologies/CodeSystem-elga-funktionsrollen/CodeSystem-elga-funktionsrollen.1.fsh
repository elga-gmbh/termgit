Instance: elga-funktionsrollen 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-funktionsrollen" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-funktionsrollen" 
* name = "elga-funktionsrollen" 
* title = "ELGA_Funktionsrollen" 
* status = #active 
* content = #complete 
* version = "1.1.0+20240820" 
* description = "ELGA Codeliste für Funktionsrollen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.158" 
* date = "2024-08-20" 
* publisher = "ELGA GmbH" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 5 
* concept[0].code = #607
* concept[0].display = "ELGA-Widerspruchstelle"
* concept[1].code = #608
* concept[1].display = "ELGA-Regelwerkadministration"
* concept[2].code = #609
* concept[2].display = "ELGA-Sicherheitsadministration"
* concept[3].code = #610
* concept[3].display = "ELGA-Teilnehmerin/Teilnehmer"
* concept[3].definition = "Bürgerin/Bürger"
* concept[4].code = #611
* concept[4].display = "ELGA-Vollmachtnehmende Person"
* concept[4].definition = "Vertretende Person einer ELGA-Teilnehmerin/ Teilnehmer (natürliche Person)"
