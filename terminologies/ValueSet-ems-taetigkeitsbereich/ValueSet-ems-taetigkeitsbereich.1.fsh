Instance: ems-taetigkeitsbereich 
InstanceOf: ValueSet 
Usage: #definition 
* id = "ems-taetigkeitsbereich" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/ems-taetigkeitsbereich" 
* name = "ems-taetigkeitsbereich" 
* title = "EMS_Taetigkeitsbereich" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "**Description:** Concepts for type of occupation. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Konzepte für Tätigkeitsbereich. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.21" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #223366009
* compose.include[0].concept[0].display = "Healthcare professional (occupation)"
* compose.include[0].concept[1].code = #265935008
* compose.include[0].concept[1].display = "Welfare occupation (occupation)"
* compose.include[0].concept[2].code = #266006009
* compose.include[0].concept[2].display = "Food/drink processor (occupation)"
* compose.include[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.81"
* compose.include[1].system = "https://termgit.elga.gv.at/CodeSystem/ems-nachweis"
* compose.include[1].version = "1.0.0+20230131"
* compose.include[1].concept[0].code = #NA
* compose.include[1].concept[0].display = "nicht anwendbar"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #223366009
* expansion.contains[0].display = "Healthcare professional (occupation)"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #265935008
* expansion.contains[1].display = "Welfare occupation (occupation)"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[2].version = "1.0.0+20230131"
* expansion.contains[2].code = #266006009
* expansion.contains[2].display = "Food/drink processor (occupation)"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem/ems-nachweis"
* expansion.contains[3].version = "1.0.0+20230131"
* expansion.contains[3].code = #NA
* expansion.contains[3].display = "nicht anwendbar"
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.81"
