Instance: elga-medikationpharmazeutischeempfehlungstatus 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-medikationpharmazeutischeempfehlungstatus" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-medikationpharmazeutischeempfehlungstatus" 
* name = "elga-medikationpharmazeutischeempfehlungstatus" 
* title = "ELGA_MedikationPharmazeutischeEmpfehlungStatus" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "**Description:** ELGA ValueSet for Pharmaceutical Advice status

**Beschreibung:** ELGA ValueSet für Pharmazeutische Empfehlung Status" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.71" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.3.6.1.4.1.19376.1.9.2.1"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/ihe-pharmaceutical-advice-status-list"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = "CANCEL"
* compose.include[0].concept[0].display = "Storno/Absetzen"
* compose.include[0].concept[1].code = "CHANGE"
* compose.include[0].concept[1].display = "Änderung"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/ihe-pharmaceutical-advice-status-list"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #CANCEL
* expansion.contains[0].display = "Storno/Absetzen"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.3.6.1.4.1.19376.1.9.2.1"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/ihe-pharmaceutical-advice-status-list"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #CHANGE
* expansion.contains[1].display = "Änderung"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.3.6.1.4.1.19376.1.9.2.1"
