Instance: atcdabbr-noinformationqualifier 
InstanceOf: ValueSet 
Usage: #definition 
* id = "atcdabbr-noinformationqualifier" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/atcdabbr-noinformationqualifier" 
* name = "atcdabbr-noinformationqualifier" 
* title = "atcdabbr_NoInformationQualifier" 
* status = #active 
* version = "1.2.0+20240325" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.210" 
* date = "2024-03-25" 
* copyright = "Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das Mitglieder-Lizenzierungs- und Distributions-Service (MLDS) direkt beim jeweiligen NRC beantragt werden. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the Member Licensing and Distribution Service (MLDS)." 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* compose.include[0].version = "1.4.0+20231016"
* compose.include[0].concept[0].code = #373068000
* compose.include[0].concept[0].display = "Undetermined (qualifier value)"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Nicht bekannt (weder Patient noch Arzt wissen etwas)" 
* compose.include[0].concept[1].code = #1631000175102
* compose.include[0].concept[1].display = "Patient not asked (contextual qualifier) (qualifier value)"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Nicht gefragt (unbekannt, weil der Arzt den Patienten nicht gefragt hat)" 
* compose.include[0].concept[2].code = #876785008
* compose.include[0].concept[2].display = "Unobtainable (qualifier value)"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Nicht erhebbar (unbekannt, weil der Arzt den Patienten nicht fragen konnte)" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[0].version = "1.4.0+20231016"
* expansion.contains[0].code = #373068000
* expansion.contains[0].display = "Undetermined (qualifier value)"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Nicht bekannt (weder Patient noch Arzt wissen etwas)" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[1].version = "1.4.0+20231016"
* expansion.contains[1].code = #1631000175102
* expansion.contains[1].display = "Patient not asked (contextual qualifier) (qualifier value)"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "Nicht gefragt (unbekannt, weil der Arzt den Patienten nicht gefragt hat)" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[2].version = "1.4.0+20231016"
* expansion.contains[2].code = #876785008
* expansion.contains[2].display = "Unobtainable (qualifier value)"
* expansion.contains[2].designation[0].language = #de-AT 
* expansion.contains[2].designation[0].value = "Nicht erhebbar (unbekannt, weil der Arzt den Patienten nicht fragen konnte)" 
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
