{
  "resourceType": "ValueSet",
  "id": "elga-telecomaddressuse",
  "meta": {
    "profile": [
      "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset"
    ]
  },
  "url": "https://termgit.elga.gv.at/ValueSet/elga-telecomaddressuse",
  "identifier": [
    {
      "use": "official",
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.2.40.0.34.10.36"
    }
  ],
  "version": "1.3.0+20240820",
  "name": "elga-telecomaddressuse",
  "title": "ELGA_TelecomAddressUse",
  "status": "active",
  "date": "2024-08-20",
  "publisher": "ELGA GmbH",
  "contact": [
    {
      "name": "https://www.elga.gv.at",
      "telecom": [
        {
          "system": "url",
          "value": "https://www.elga.gv.at"
        }
      ]
    }
  ],
  "description": "**Description:** Uses of Addresses\n\n**Beschreibung:** Beschreibt die Verwendungsart einer Adresse",
  "compose": {
    "include": [
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.5.1119"
          }
        ],
        "system": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
        "version": "2.1.0",
        "concept": [
          {
            "code": "H",
            "display": "home address",
            "designation": [
              {
                "language": "de-AT",
                "value": "Wohnort"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "zu Hause (A communication address at a home, attempted contacts for business purposes might intrude privacy and chances are one will contact family or other household members instead of the person one wishes to call. Typically used with urgent cases, or if no other contacts are available.)"
              }
            ]
          },
          {
            "code": "HP",
            "display": "primary home",
            "designation": [
              {
                "language": "de-AT",
                "value": "Hauptwohnsitz"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "Privat (The primary home, to reach a person after business hours.)"
              }
            ]
          },
          {
            "code": "HV",
            "display": "vacation home",
            "designation": [
              {
                "language": "de-AT",
                "value": "Ferienwohnort"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "Im Urlaub (A vacation home, to reach a person while on vacation.)"
              }
            ]
          },
          {
            "code": "WP",
            "display": "work place",
            "designation": [
              {
                "language": "de-AT",
                "value": "Gesch\u00e4ftlich"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "Gesch\u00e4ftlich (An office address. First choice for business related contacts during business hours.)"
              }
            ]
          },
          {
            "code": "AS",
            "display": "answering service",
            "designation": [
              {
                "language": "de-AT",
                "value": "Anrufbeantworter"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "Anrufbeantworter (An automated answering machine used for less urgent cases and if the main purpose of contact is to leave a message or access an automated announcement.)"
              }
            ]
          },
          {
            "code": "EC",
            "display": "emergency contact",
            "designation": [
              {
                "language": "de-AT",
                "value": "Notfall-Telefonnummer"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "Notfallnummer (A contact specifically designated to be used for emergencies. This is the first choice in emergencies, independent of any other use codes.)"
              }
            ]
          },
          {
            "code": "MC",
            "display": "mobile contact)",
            "designation": [
              {
                "language": "de-AT",
                "value": "Mobiltelefon"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "Mobiltelefon (Mobiler Kontakt)"
              }
            ]
          },
          {
            "code": "PG",
            "display": "pager",
            "designation": [
              {
                "language": "de-AT",
                "value": "Pager"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "Pager"
              }
            ]
          },
          {
            "code": "TMP",
            "display": "temporary address",
            "designation": [
              {
                "language": "de-AT",
                "value": "Pflege"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "Pflegeadresse (temporary address)"
              }
            ]
          }
        ]
      }
    ]
  },
  "expansion": {
    "timestamp": "2024-08-20T09:00:32+00:00",
    "contains": [
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.5.1119"
          }
        ],
        "system": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
        "version": "2.1.0",
        "code": "H",
        "display": "home address",
        "designation": [
          {
            "language": "de-AT",
            "value": "Wohnort"
          },
          {
            "use": {
              "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
              "code": "concept_beschreibung",
              "display": "concept_beschreibung"
            },
            "value": "zu Hause (A communication address at a home, attempted contacts for business purposes might intrude privacy and chances are one will contact family or other household members instead of the person one wishes to call. Typically used with urgent cases, or if no other contacts are available.)"
          }
        ],
        "contains": [
          {
            "extension": [
              {
                "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
                "valueOid": "urn:oid:2.16.840.1.113883.5.1119"
              }
            ],
            "system": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
            "version": "2.1.0",
            "code": "HP",
            "display": "primary home",
            "designation": [
              {
                "language": "de-AT",
                "value": "Hauptwohnsitz"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "Privat (The primary home, to reach a person after business hours.)"
              }
            ]
          },
          {
            "extension": [
              {
                "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
                "valueOid": "urn:oid:2.16.840.1.113883.5.1119"
              }
            ],
            "system": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
            "version": "2.1.0",
            "code": "HV",
            "display": "vacation home",
            "designation": [
              {
                "language": "de-AT",
                "value": "Ferienwohnort"
              },
              {
                "use": {
                  "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                  "code": "concept_beschreibung",
                  "display": "concept_beschreibung"
                },
                "value": "Im Urlaub (A vacation home, to reach a person while on vacation.)"
              }
            ]
          }
        ]
      },
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.5.1119"
          }
        ],
        "system": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
        "version": "2.1.0",
        "code": "WP",
        "display": "work place",
        "designation": [
          {
            "language": "de-AT",
            "value": "Gesch\u00e4ftlich"
          },
          {
            "use": {
              "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
              "code": "concept_beschreibung",
              "display": "concept_beschreibung"
            },
            "value": "Gesch\u00e4ftlich (An office address. First choice for business related contacts during business hours.)"
          }
        ]
      },
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.5.1119"
          }
        ],
        "system": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
        "version": "2.1.0",
        "code": "AS",
        "display": "answering service",
        "designation": [
          {
            "language": "de-AT",
            "value": "Anrufbeantworter"
          },
          {
            "use": {
              "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
              "code": "concept_beschreibung",
              "display": "concept_beschreibung"
            },
            "value": "Anrufbeantworter (An automated answering machine used for less urgent cases and if the main purpose of contact is to leave a message or access an automated announcement.)"
          }
        ]
      },
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.5.1119"
          }
        ],
        "system": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
        "version": "2.1.0",
        "code": "EC",
        "display": "emergency contact",
        "designation": [
          {
            "language": "de-AT",
            "value": "Notfall-Telefonnummer"
          },
          {
            "use": {
              "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
              "code": "concept_beschreibung",
              "display": "concept_beschreibung"
            },
            "value": "Notfallnummer (A contact specifically designated to be used for emergencies. This is the first choice in emergencies, independent of any other use codes.)"
          }
        ]
      },
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.5.1119"
          }
        ],
        "system": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
        "version": "2.1.0",
        "code": "MC",
        "display": "mobile contact)",
        "designation": [
          {
            "language": "de-AT",
            "value": "Mobiltelefon"
          },
          {
            "use": {
              "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
              "code": "concept_beschreibung",
              "display": "concept_beschreibung"
            },
            "value": "Mobiltelefon (Mobiler Kontakt)"
          }
        ]
      },
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.5.1119"
          }
        ],
        "system": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
        "version": "2.1.0",
        "code": "PG",
        "display": "pager",
        "designation": [
          {
            "language": "de-AT",
            "value": "Pager"
          },
          {
            "use": {
              "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
              "code": "concept_beschreibung",
              "display": "concept_beschreibung"
            },
            "value": "Pager"
          }
        ]
      },
      {
        "extension": [
          {
            "url": "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid",
            "valueOid": "urn:oid:2.16.840.1.113883.5.1119"
          }
        ],
        "system": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
        "version": "2.1.0",
        "code": "TMP",
        "display": "temporary address",
        "designation": [
          {
            "language": "de-AT",
            "value": "Pflege"
          },
          {
            "use": {
              "system": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
              "code": "concept_beschreibung",
              "display": "concept_beschreibung"
            },
            "value": "Pflegeadresse (temporary address)"
          }
        ]
      }
    ]
  }
}