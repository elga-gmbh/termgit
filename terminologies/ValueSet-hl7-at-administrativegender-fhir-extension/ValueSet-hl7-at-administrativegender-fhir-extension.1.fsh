Instance: hl7-at-administrativegender-fhir-extension 
InstanceOf: ValueSet 
Usage: #definition 
* id = "hl7-at-administrativegender-fhir-extension" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/hl7-at-administrativegender-fhir-extension" 
* name = "hl7-at-administrativegender-fhir-extension" 
* title = "HL7-at_AdministrativeGender-FHIR-Extension" 
* status = #active 
* version = "1.2.0+20240820" 
* description = "Österreichische Extension zum internationalen FHIR-Value-Set http://hl7.org/fhir/ValueSet/administrative-gender " 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.83" 
* date = "2024-08-20" 
* publisher = "HL7 Austria" 
* contact[0].name = "https://www.hl7.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.hl7.at" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.224"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/hl7-at-administrativegender-ergaenzung"
* compose.include[0].version = "1.0.0+20231113"
* compose.include[0].concept[0].code = #D
* compose.include[0].concept[0].display = "Divers"
* compose.include[0].concept[1].code = #I
* compose.include[0].concept[1].display = "Inter"
* compose.include[0].concept[2].code = #O
* compose.include[0].concept[2].display = "Offen"

* expansion.timestamp = "2024-08-20T09:00:32.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/hl7-at-administrativegender-ergaenzung"
* expansion.contains[0].version = "1.0.0+20231113"
* expansion.contains[0].code = #D
* expansion.contains[0].display = "Divers"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.224"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/hl7-at-administrativegender-ergaenzung"
* expansion.contains[1].version = "1.0.0+20231113"
* expansion.contains[1].code = #I
* expansion.contains[1].display = "Inter"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.224"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/hl7-at-administrativegender-ergaenzung"
* expansion.contains[2].version = "1.0.0+20231113"
* expansion.contains[2].code = #O
* expansion.contains[2].display = "Offen"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.224"
