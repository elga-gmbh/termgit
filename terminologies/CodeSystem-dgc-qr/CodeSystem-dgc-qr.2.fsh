Instance: dgc-qr 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "dgc-qr" 
* url = "https://termgit.elga.gv.at/CodeSystem/dgc-qr" 
* name = "dgc-qr" 
* title = "DGC_QR" 
* status = #retired 
* content = #complete 
* version = "2.5.0+20230523" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.202" 
* date = "2023-05-23" 
* copyright = "Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS)." 
* count = 616 
* property[0].code = #Relationships 
* property[0].type = #string 
* property[1].code = #hints 
* property[1].type = #string 
* property[2].code = #status 
* property[2].type = #code 
* #1065 "Becton Dickinson, BD Veritor System for Rapid Detection of SARS CoV 2"
* #1065 ^property[0].code = #hints 
* #1065 ^property[0].valueString = "T^E^B" 
* #1065 ^property[1].code = #Relationships 
* #1065 ^property[1].valueString = "$.t..ma" 
* #1097 "Quidel Corporation, Sofia 2 SARS Antigen FIA"
* #1097 ^property[0].code = #hints 
* #1097 ^property[0].valueString = "T^E^B" 
* #1097 ^property[1].code = #Relationships 
* #1097 ^property[1].valueString = "$.t..ma" 
* #1114 "Sugentech, Inc., SGTi-flex COVID-19 Ag"
* #1114 ^property[0].code = #hints 
* #1114 ^property[0].valueString = "T^E^B" 
* #1114 ^property[1].code = #Relationships 
* #1114 ^property[1].valueString = "$.t..ma" 
* #1119305005 "SARS-CoV-2 antigen vaccine"
* #1119305005 ^property[0].code = #hints 
* #1119305005 ^property[0].valueString = "T^E^B" 
* #1119305005 ^property[1].code = #Relationships 
* #1119305005 ^property[1].valueString = "$.v..vp" 
* #1119349007 "SARS-CoV-2 mRNA vaccine"
* #1119349007 ^property[0].code = #hints 
* #1119349007 ^property[0].valueString = "T^E^B" 
* #1119349007 ^property[1].code = #Relationships 
* #1119349007 ^property[1].valueString = "$.v..vp" 
* #1144 "Green Cross Medical Science Corp., GENEDIA W COVID-19 Ag"
* #1144 ^property[0].code = #hints 
* #1144 ^property[0].valueString = "T^E^B" 
* #1144 ^property[1].code = #Relationships 
* #1144 ^property[1].valueString = "$.t..ma" 
* #1162 "nal von minden GmbH, NADAL COVID-19 Ag Test"
* #1162 ^property[0].code = #hints 
* #1162 ^property[0].valueString = "T^E^B" 
* #1162 ^property[1].code = #Relationships 
* #1162 ^property[1].valueString = "$.t..ma" 
* #1173 "CerTest Biotec, CerTest SARS-CoV-2 Card test"
* #1173 ^property[0].code = #hints 
* #1173 ^property[0].valueString = "T^E^B" 
* #1173 ^property[1].code = #Relationships 
* #1173 ^property[1].valueString = "$.t..ma" 
* #1178 "Shenzhen Microprofit Biotech Co., Ltd., SARS-CoV-2 Spike Protein Test Kit (Colloidal Gold Chromatographic Immunoassay)"
* #1178 ^property[0].code = #hints 
* #1178 ^property[0].valueString = "T^E^B" 
* #1178 ^property[1].code = #Relationships 
* #1178 ^property[1].valueString = "$.t..ma" 
* #1180 "MEDsan GmbH, MEDsan SARS-CoV-2 Antigen Rapid Test"
* #1180 ^property[0].code = #hints 
* #1180 ^property[0].valueString = "T^E^B" 
* #1180 ^property[1].code = #Relationships 
* #1180 ^property[1].valueString = "$.t..ma" 
* #1190 "möLab, COVID-19 Rapid Antigen Test"
* #1190 ^property[0].code = #hints 
* #1190 ^property[0].valueString = "T^E^B" 
* #1190 ^property[1].code = #Relationships 
* #1190 ^property[1].valueString = "$.t..ma" 
* #1197 "Goldsite Diagnostic Inc., SARS-CoV-2 Antigen Kit (Colloidal Gold)"
* #1197 ^property[0].code = #hints 
* #1197 ^property[0].valueString = "T^E^B" 
* #1197 ^property[1].code = #Relationships 
* #1197 ^property[1].valueString = "$.t..ma" 
* #1199 "Oncosem Onkolojik Sistemler San. ve Tic. A.S., CAT"
* #1199 ^property[0].code = #hints 
* #1199 ^property[0].valueString = "T^E^B" 
* #1199 ^property[1].code = #Relationships 
* #1199 ^property[1].valueString = "$.t..ma" 
* #1201 "ScheBo Biotech AG, ScheBo SARS CoV-2 Quick Antigen"
* #1201 ^property[0].code = #hints 
* #1201 ^property[0].valueString = "T^E^B" 
* #1201 ^property[1].code = #Relationships 
* #1201 ^property[1].valueString = "$.t..ma" 
* #1215 "Hangzhou Laihe Biotech Co., LYHER Novel Coronavirus (COVID-19) Antigen Test Kit (Colloidal Gold) for self-testing"
* #1215 ^property[0].code = #hints 
* #1215 ^property[0].valueString = "T^E^B" 
* #1215 ^property[1].code = #Relationships 
* #1215 ^property[1].valueString = "$.t..ma" 
* #1216 "Guangdong Longsee Biomedical Co., Ltd., COVID-2019-nCoV Ag Rapid TestDetection Kit(Immuno-Chromatography)"
* #1216 ^property[0].code = #hints 
* #1216 ^property[0].valueString = "T^E^B" 
* #1216 ^property[1].code = #Relationships 
* #1216 ^property[1].valueString = "$.t..ma" 
* #1218 "Siemens Healthineers, CLINITEST Rapid COVID-19 Antigen Test"
* #1218 ^property[0].code = #hints 
* #1218 ^property[0].valueString = "T^E^B" 
* #1218 ^property[1].code = #Relationships 
* #1218 ^property[1].valueString = "$.t..ma" 
* #1223 "BIOSYNEX SA , BIOSYNEX COVID-19 Ag BSS"
* #1223 ^property[0].code = #hints 
* #1223 ^property[0].valueString = "T^E^B" 
* #1223 ^property[1].code = #Relationships 
* #1223 ^property[1].valueString = "$.t..ma" 
* #1225 "DDS DIAGNOSTIC, Test Rapid Covid-19 Antigen (tampon nazofaringian)"
* #1225 ^property[0].code = #hints 
* #1225 ^property[0].valueString = "T^E^B" 
* #1225 ^property[1].code = #Relationships 
* #1225 ^property[1].valueString = "$.t..ma" 
* #1228 "Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Spike Protein Test Kit (Fluorescence Immunoassay)"
* #1228 ^property[0].code = #hints 
* #1228 ^property[0].valueString = "T^E^B" 
* #1228 ^property[1].code = #Relationships 
* #1228 ^property[1].valueString = "$.t..ma" 
* #1232 "Abbott Rapid Diagnostics, Panbio COVID-19 Ag Rapid Test"
* #1232 ^property[0].code = #hints 
* #1232 ^property[0].valueString = "T^E^B" 
* #1232 ^property[1].code = #Relationships 
* #1232 ^property[1].valueString = "$.t..ma" 
* #1236 "BTNX Inc., Rapid Response COVID-19 Antigen Rapid Test Device"
* #1236 ^property[0].code = #hints 
* #1236 ^property[0].valueString = "T^E^B" 
* #1236 ^property[1].code = #Relationships 
* #1236 ^property[1].valueString = "$.t..ma" 
* #1242 "BIONOTE, NowCheck COVID-19 Ag Test"
* #1242 ^property[0].code = #hints 
* #1242 ^property[0].valueString = "T^E^B" 
* #1242 ^property[1].code = #Relationships 
* #1242 ^property[1].valueString = "$.t..ma" 
* #1243 "Edinburgh Genetics Limited, Edinburgh Genetics ActivXpress+ COVID-19 Antigen Complete Testing Kit"
* #1243 ^property[0].code = #hints 
* #1243 ^property[0].valueString = "T^E^B" 
* #1243 ^property[1].code = #Relationships 
* #1243 ^property[1].valueString = "$.t..ma" 
* #1244 "GenBody Inc, GenBody COVID-19 Ag Test"
* #1244 ^property[0].code = #hints 
* #1244 ^property[0].valueString = "T^E^B" 
* #1244 ^property[1].code = #Relationships 
* #1244 ^property[1].valueString = "$.t..ma" 
* #1253 "GenSure Biotech Inc, Gensure COVID-19 Antigen Rapid Test Kit (REF: P2004) (DIA-COVID -19 Ag Rapid Test)"
* #1253 ^property[0].code = #hints 
* #1253 ^property[0].valueString = "T^E^B" 
* #1253 ^property[1].code = #Relationships 
* #1253 ^property[1].valueString = "$.t..ma" 
* #1257 "Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Rapid Test"
* #1257 ^property[0].code = #hints 
* #1257 ^property[0].valueString = "T^E^B" 
* #1257 ^property[1].code = #Relationships 
* #1257 ^property[1].valueString = "$.t..ma" 
* #1258 "Hangzhou AllTest Biotech Co., Ltd, SARS-CoV-2 and Influenza A+B Antigen Combo Rapid Test (Nasopharyngeal Swab)"
* #1258 ^property[0].code = #hints 
* #1258 ^property[0].valueString = "T^E^B" 
* #1258 ^property[1].code = #Relationships 
* #1258 ^property[1].valueString = "$.t..ma" 
* #1266 "Labnovation Technologies Inc., SARS-CoV-2 Antigen Rapid Test Kit"
* #1266 ^property[0].code = #hints 
* #1266 ^property[0].valueString = "T^E^B" 
* #1266 ^property[1].code = #Relationships 
* #1266 ^property[1].valueString = "$.t..ma" 
* #1267 "LumiQuick Diagnostics Inc., QuickProfil COVID-19 ANTIGEN Test"
* #1267 ^property[0].code = #hints 
* #1267 ^property[0].valueString = "T^E^B" 
* #1267 ^property[1].code = #Relationships 
* #1267 ^property[1].valueString = "$.t..ma" 
* #1268 "LumiraDX , LumiraDx SARS-CoV-2 Ag Test"
* #1268 ^property[0].code = #hints 
* #1268 ^property[0].valueString = "T^E^B" 
* #1268 ^property[1].code = #Relationships 
* #1268 ^property[1].valueString = "$.t..ma" 
* #1271 "Precision Biosensor Inc., Exdia COVI-19 Ag Test"
* #1271 ^property[0].code = #hints 
* #1271 ^property[0].valueString = "T^E^B" 
* #1271 ^property[1].code = #Relationships 
* #1271 ^property[1].valueString = "$.t..ma" 
* #1276 "Willi Fox GmbH, Willi Fox COVID-19 Antigen rapid test"
* #1276 ^property[0].code = #hints 
* #1276 ^property[0].valueString = "T^E^B" 
* #1276 ^property[1].code = #Relationships 
* #1276 ^property[1].valueString = "$.t..ma" 
* #1278 "Xiamen Boson Biotech Co, Rapid SARS-CoV-2 Antigen Test card"
* #1278 ^property[0].code = #hints 
* #1278 ^property[0].valueString = "T^E^B" 
* #1278 ^property[1].code = #Relationships 
* #1278 ^property[1].valueString = "$.t..ma" 
* #1286 "BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Fluorescence Immunochromatography)"
* #1286 ^property[0].code = #hints 
* #1286 ^property[0].valueString = "T^E^B" 
* #1286 ^property[1].code = #Relationships 
* #1286 ^property[1].valueString = "$.t..ma" 
* #1295 "Zhejiang Anji Saianfu Biotech Co.., Ltd., reOpenTest COVID-19  Antigen Rapid Test"
* #1295 ^property[0].code = #hints 
* #1295 ^property[0].valueString = "T^E^B" 
* #1295 ^property[1].code = #Relationships 
* #1295 ^property[1].valueString = "$.t..ma" 
* #1296 "Zhejiang Anji Saianfu Biotech Co.., Ltd., AndLucky COVID-19 Antigen Rapid Test"
* #1296 ^property[0].code = #hints 
* #1296 ^property[0].valueString = "T^E^B" 
* #1296 ^property[1].code = #Relationships 
* #1296 ^property[1].valueString = "$.t..ma" 
* #1304 "AMEDA Labordiagnostik GmbH, AMP Rapid Test SARS-CoV-2 Ag"
* #1304 ^property[0].code = #hints 
* #1304 ^property[0].valueString = "T^E^B" 
* #1304 ^property[1].code = #Relationships 
* #1304 ^property[1].valueString = "$.t..ma" 
* #1319 "SGA Medikal, V-Chek SARS-CoV2- Rapid Ag Tets (Coloidal Gold)"
* #1319 ^property[0].code = #hints 
* #1319 ^property[0].valueString = "T^E^B" 
* #1319 ^property[1].code = #Relationships 
* #1319 ^property[1].valueString = "$.t..ma" 
* #1324 "Guangzhou Decheng Biotechnology CO., Ltd, V-CHEK, 2019-nCoV Ag Rapid Test Kit (Immuno-chromatography)"
* #1324 ^property[0].code = #hints 
* #1324 ^property[0].valueString = "T^E^B" 
* #1324 ^property[1].code = #Relationships 
* #1324 ^property[1].valueString = "$.t..ma" 
* #1331 "Beijing Lepu Medical Technology, SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)"
* #1331 ^property[0].code = #hints 
* #1331 ^property[0].valueString = "T^E^B" 
* #1331 ^property[1].code = #Relationships 
* #1331 ^property[1].valueString = "$.t..ma" 
* #1333 "Joinstar Biomedical Technology Co. Ltd, COVID-19 Antigen Rapid Test (Colloidal Gold)"
* #1333 ^property[0].code = #hints 
* #1333 ^property[0].valueString = "T^E^B" 
* #1333 ^property[1].code = #Relationships 
* #1333 ^property[1].valueString = "$.t..ma" 
* #1341 "Qingdao Hightop Biotech Co. Ltd, SARS-CoV-2 Antigen Rapid Test"
* #1341 ^property[0].code = #hints 
* #1341 ^property[0].valueString = "T^E^B" 
* #1341 ^property[1].code = #Relationships 
* #1341 ^property[1].valueString = "$.t..ma" 
* #1343 "Zhejiang Orient Gene Biotech Co., Ltd, Coronavirus Ag Rapid Test Cassette (Swab)"
* #1343 ^property[0].code = #hints 
* #1343 ^property[0].valueString = "T^E^B" 
* #1343 ^property[1].code = #Relationships 
* #1343 ^property[1].valueString = "$.t..ma" 
* #1347 "Shenzhen YHLO Biotech Co., Ltd., GLINE-2019-nCoV Ag"
* #1347 ^property[0].code = #hints 
* #1347 ^property[0].valueString = "T^E^B" 
* #1347 ^property[1].code = #Relationships 
* #1347 ^property[1].valueString = "$.t..ma" 
* #1353 "LINKCARE (NANTONG DIAGNOS BIO), COVID-19 Antigen Test Kit (Colloidal Gold)"
* #1353 ^property[0].code = #hints 
* #1353 ^property[0].valueString = "T^E^B" 
* #1353 ^property[1].code = #Relationships 
* #1353 ^property[1].valueString = "$.t..ma" 
* #1357 "SGA Medikal, V-Chek SARS-CoV-2 Rapid Ag Test (colloidal gold)"
* #1357 ^property[0].code = #hints 
* #1357 ^property[0].valueString = "T^E^B" 
* #1357 ^property[1].code = #Relationships 
* #1357 ^property[1].valueString = "$.t..ma" 
* #1360 "Guangdong Wesail Biotech Co. Ltd, COVID-19 AG Test Kit"
* #1360 ^property[0].code = #hints 
* #1360 ^property[0].valueString = "T^E^B" 
* #1360 ^property[1].code = #Relationships 
* #1360 ^property[1].valueString = "$.t..ma" 
* #1363 "Hangzhou Clongene Biotech Co., Ltd., COVID-19 Antigen Rapid Test Kit"
* #1363 ^property[0].code = #hints 
* #1363 ^property[0].valueString = "T^E^B" 
* #1363 ^property[1].code = #Relationships 
* #1363 ^property[1].valueString = "$.t..ma" 
* #1365 "Hangzhou Clongene Biotech Co., Ltd., COVID-19/Influenza A+B Antigen Combo Rapid Test"
* #1365 ^property[0].code = #hints 
* #1365 ^property[0].valueString = "T^E^B" 
* #1365 ^property[1].code = #Relationships 
* #1365 ^property[1].valueString = "$.t..ma" 
* #1375 "DIALAB GmbH, DIAQUICK COVID -19 Ag Cassette"
* #1375 ^property[0].code = #hints 
* #1375 ^property[0].valueString = "T^E^B" 
* #1375 ^property[1].code = #Relationships 
* #1375 ^property[1].valueString = "$.t..ma" 
* #1392 "Hangzhou Testsea Biotechnology Co., Ltd., Testsealabs Covid-19 Antigen Rapid Test Cassette"
* #1392 ^property[0].code = #hints 
* #1392 ^property[0].valueString = "T^E^B" 
* #1392 ^property[1].code = #Relationships 
* #1392 ^property[1].valueString = "$.t..ma" 
* #1420 "NanoEntek, FREND Covid-19 Ag"
* #1420 ^property[0].code = #hints 
* #1420 ^property[0].valueString = "T^E^B" 
* #1420 ^property[1].code = #Relationships 
* #1420 ^property[1].valueString = "$.t..ma" 
* #1437 "Guangzhou Wondfo Biotech Co., Ltd, Wondfo 2019-nCoV Antigen Test (Lateral Flow Method)"
* #1437 ^property[0].code = #hints 
* #1437 ^property[0].valueString = "T^E^B" 
* #1437 ^property[1].code = #Relationships 
* #1437 ^property[1].valueString = "$.t..ma" 
* #1443 "Vitrosens Biotechnology Co. Ltd, RapidFor SARS-CoV-2 Ag Test Kit"
* #1443 ^property[0].code = #hints 
* #1443 ^property[0].valueString = "T^E^B" 
* #1443 ^property[1].code = #Relationships 
* #1443 ^property[1].valueString = "$.t..ma" 
* #1446 "Hangzhou Singclean Medical Products Co., Ltd, COVID-19 Antigen Test Kit (Colloidal Gold)"
* #1446 ^property[0].code = #hints 
* #1446 ^property[0].valueString = "T^E^B" 
* #1446 ^property[1].code = #Relationships 
* #1446 ^property[1].valueString = "$.t..ma" 
* #1456 "Xiamen Wiz Biotech Co., Ltd., SARS-CoV-2 Antigen Rapid Test"
* #1456 ^property[0].code = #hints 
* #1456 ^property[0].valueString = "T^E^B" 
* #1456 ^property[1].code = #Relationships 
* #1456 ^property[1].valueString = "$.t..ma" 
* #1457 "Acon Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Antigen Rapid Test"
* #1457 ^property[0].code = #hints 
* #1457 ^property[0].valueString = "T^E^B" 
* #1457 ^property[1].code = #Relationships 
* #1457 ^property[1].valueString = "$.t..ma" 
* #1465 "Triplex International Biosciences Co., Ltd, China, SARS-CoV-2 Antigen Rapid Test Kit"
* #1465 ^property[0].code = #hints 
* #1465 ^property[0].valueString = "T^E^B" 
* #1465 ^property[1].code = #Relationships 
* #1465 ^property[1].valueString = "$.t..ma" 
* #1466 "TODA PHARMA, TODA CORONADIAG Ag"
* #1466 ^property[0].code = #hints 
* #1466 ^property[0].valueString = "T^E^B" 
* #1466 ^property[1].code = #Relationships 
* #1466 ^property[1].valueString = "$.t..ma" 
* #1468 "ACON Laboratories, Inc, Flowflex SARS-CoV-2 Antigen Rapid Test"
* #1468 ^property[0].code = #hints 
* #1468 ^property[0].valueString = "T^E^B" 
* #1468 ^property[1].code = #Relationships 
* #1468 ^property[1].valueString = "$.t..ma" 
* #1481 "MP Biomedicals , Rapid SARS-CoV-2 Antigen Test Card"
* #1481 ^property[0].code = #hints 
* #1481 ^property[0].valueString = "T^E^B" 
* #1481 ^property[1].code = #Relationships 
* #1481 ^property[1].valueString = "$.t..ma" 
* #1485 "Beijing Wantai Biological Pharmacy Enterprise Co., Ltd, Wantai SARS-CoV-2 Ag Rapid Test (colloidal gold)"
* #1485 ^property[0].code = #hints 
* #1485 ^property[0].valueString = "T^E^B" 
* #1485 ^property[1].code = #Relationships 
* #1485 ^property[1].valueString = "$.t..ma" 
* #1489 "Safecare Biotech Hangzhou Co, COVID-19 Antigen Rapid Test Kit (Swab)"
* #1489 ^property[0].code = #hints 
* #1489 ^property[0].valueString = "T^E^B" 
* #1489 ^property[1].code = #Relationships 
* #1489 ^property[1].valueString = "$.t..ma" 
* #1490 "Safecare Biotech Hangzhou Co, Multi-Respiratory Virus Antigen Test Kit (Swab) (Influenza A+B/COVID-19)"
* #1490 ^property[0].code = #hints 
* #1490 ^property[0].valueString = "T^E^B" 
* #1490 ^property[1].code = #Relationships 
* #1490 ^property[1].valueString = "$.t..ma" 
* #1494 "BIOSYNEX SA, BIOSYNEX COVID-19 Ag+ BSS"
* #1494 ^property[0].code = #hints 
* #1494 ^property[0].valueString = "T^E^B" 
* #1494 ^property[1].code = #Relationships 
* #1494 ^property[1].valueString = "$.t..ma" 
* #1495 "Prognosis Biotech, Rapid Test Ag 2019-nCov"
* #1495 ^property[0].code = #hints 
* #1495 ^property[0].valueString = "T^E^B" 
* #1495 ^property[1].code = #Relationships 
* #1495 ^property[1].valueString = "$.t..ma" 
* #1501 "New Gene (Hangzhou) Bioengineering Co., Ltd., COVID-19 Antigen Detection Kit"
* #1501 ^property[0].code = #hints 
* #1501 ^property[0].valueString = "T^E^B" 
* #1501 ^property[1].code = #Relationships 
* #1501 ^property[1].valueString = "$.t..ma" 
* #1573 "Nantong Egens Biotechnology Co.,Ltd, COVID-19 Antigen Rapid Test Kit"
* #1573 ^property[0].code = #hints 
* #1573 ^property[0].valueString = "T^E^B" 
* #1573 ^property[1].code = #Relationships 
* #1573 ^property[1].valueString = "$.t..ma" 
* #1581 "CTK Biotech, Inc, OnSite COVID-19 Ag Rapid Test"
* #1581 ^property[0].code = #hints 
* #1581 ^property[0].valueString = "T^E^B" 
* #1581 ^property[1].code = #Relationships 
* #1581 ^property[1].valueString = "$.t..ma" 
* #1592 "Shenzhen Lifotronic Technology Co., Ltd., Antigen Rapid Test Ag SARS-CoV-2"
* #1592 ^property[0].code = #hints 
* #1592 ^property[0].valueString = "T^E^B" 
* #1592 ^property[1].code = #Relationships 
* #1592 ^property[1].valueString = "$.t..ma" 
* #1593 "OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Rapid Test"
* #1593 ^property[0].code = #hints 
* #1593 ^property[0].valueString = "T^E^B" 
* #1593 ^property[1].code = #Relationships 
* #1593 ^property[1].valueString = "$.t..ma" 
* #1599 "Biomerica Inc., Biomerica COVID-19 Antigen Rapid Test (nasopharyngeal swab)"
* #1599 ^property[0].code = #hints 
* #1599 ^property[0].valueString = "T^E^B" 
* #1599 ^property[1].code = #Relationships 
* #1599 ^property[1].valueString = "$.t..ma" 
* #1604 "Roche (SD BIOSENSOR), SARS-CoV-2 Antigen Rapid Test"
* #1604 ^property[0].code = #hints 
* #1604 ^property[0].valueString = "T^E^B" 
* #1604 ^property[1].code = #Relationships 
* #1604 ^property[1].valueString = "$.t..ma" 
* #1610 "Hangzhou Clongene Biotech Co., Ltd, COVID-19 Antigen Rapid Test Casette"
* #1610 ^property[0].code = #hints 
* #1610 ^property[0].valueString = "T^E^B" 
* #1610 ^property[1].code = #Relationships 
* #1610 ^property[1].valueString = "$.t..ma" 
* #1618 "Artron Laboratories Inc., Artron COVID-19 Antigen Test"
* #1618 ^property[0].code = #hints 
* #1618 ^property[0].valueString = "T^E^B" 
* #1618 ^property[1].code = #Relationships 
* #1618 ^property[1].valueString = "$.t..ma" 
* #1646 "NG Biotech, NG-Test SARS-CoV-2 Ag"
* #1646 ^property[0].code = #hints 
* #1646 ^property[0].valueString = "T^E^B" 
* #1646 ^property[1].code = #Relationships 
* #1646 ^property[1].valueString = "$.t..ma" 
* #1647 "CALTH Inc., AllCheck COVID19 Ag"
* #1647 ^property[0].code = #hints 
* #1647 ^property[0].valueString = "T^E^B" 
* #1647 ^property[1].code = #Relationships 
* #1647 ^property[1].valueString = "$.t..ma" 
* #1654 "Asan Pharmaceutical CO., LTD, Asan Easy Test COVID-19 Ag"
* #1654 ^property[0].code = #hints 
* #1654 ^property[0].valueString = "T^E^B" 
* #1654 ^property[1].code = #Relationships 
* #1654 ^property[1].valueString = "$.t..ma" 
* #1689 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., Covid-19 Ag Test"
* #1689 ^property[0].code = #hints 
* #1689 ^property[0].valueString = "T^E^B" 
* #1689 ^property[1].code = #Relationships 
* #1689 ^property[1].valueString = "$.t..ma" 
* #1691 "Chil T?bbi Malzeme Sanayi ve Ticaret Limited Sirketi, CHIL COVID-19 Antigen Rapid Test (Nasopharyngeal / Oropharyngeal Swab-Casette)"
* #1691 ^property[0].code = #hints 
* #1691 ^property[0].valueString = "T^E^B" 
* #1691 ^property[1].code = #Relationships 
* #1691 ^property[1].valueString = "$.t..ma" 
* #1722 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., TOYO Covid-19 Ag Tes"
* #1722 ^property[0].code = #hints 
* #1722 ^property[0].valueString = "T^E^B" 
* #1722 ^property[1].code = #Relationships 
* #1722 ^property[1].valueString = "$.t..ma" 
* #1736 "Anhui Deep Blue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold)"
* #1736 ^property[0].code = #hints 
* #1736 ^property[0].valueString = "T^E^B" 
* #1736 ^property[1].code = #Relationships 
* #1736 ^property[1].valueString = "$.t..ma" 
* #1739 "Eurobio Scientific, EBS SARS-CoV-2 Ag Rapid Test"
* #1739 ^property[0].code = #hints 
* #1739 ^property[0].valueString = "T^E^B" 
* #1739 ^property[1].code = #Relationships 
* #1739 ^property[1].valueString = "$.t..ma" 
* #1747 "Guangdong Hecin Scientific, Inc., 2019-nCoV Antigen Test Kit (colloidal gold method)"
* #1747 ^property[0].code = #hints 
* #1747 ^property[0].valueString = "T^E^B" 
* #1747 ^property[1].code = #Relationships 
* #1747 ^property[1].valueString = "$.t..ma" 
* #1751 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., RAPIDAN TESTER Covid-19 Ag Test"
* #1751 ^property[0].code = #hints 
* #1751 ^property[0].valueString = "T^E^B" 
* #1751 ^property[1].code = #Relationships 
* #1751 ^property[1].valueString = "$.t..ma" 
* #1759 "Hubei Jinjian Biology Co., Ltd, SARS-CoV-2 Antigen Test Kit"
* #1759 ^property[0].code = #hints 
* #1759 ^property[0].valueString = "T^E^B" 
* #1759 ^property[1].code = #Relationships 
* #1759 ^property[1].valueString = "$.t..ma" 
* #1762 "Novatech, SARS-CoV-2 Antigen Rapid Test"
* #1762 ^property[0].code = #hints 
* #1762 ^property[0].valueString = "T^E^B" 
* #1762 ^property[1].code = #Relationships 
* #1762 ^property[1].valueString = "$.t..ma" 
* #1763 "Xiamen AmonMed Biotechnology Co., Ltd., COVID-19 Antigen Rapid Test Kit (Colloidal Gold)"
* #1763 ^property[0].code = #hints 
* #1763 ^property[0].valueString = "T^E^B" 
* #1763 ^property[1].code = #Relationships 
* #1763 ^property[1].valueString = "$.t..ma" 
* #1764 "JOYSBIO (Tianjin) Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)"
* #1764 ^property[0].code = #hints 
* #1764 ^property[0].valueString = "T^E^B" 
* #1764 ^property[1].code = #Relationships 
* #1764 ^property[1].valueString = "$.t..ma" 
* #1767 "Healgen Scientific Limited, Coronavirus Ag Rapid Test Cassette (Swab)"
* #1767 ^property[0].code = #hints 
* #1767 ^property[0].valueString = "T^E^B" 
* #1767 ^property[1].code = #Relationships 
* #1767 ^property[1].valueString = "$.t..ma" 
* #1768 "Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Immuno-fluorescence)"
* #1768 ^property[0].code = #hints 
* #1768 ^property[0].valueString = "T^E^B" 
* #1768 ^property[1].code = #Relationships 
* #1768 ^property[1].valueString = "$.t..ma" 
* #1769 "Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Colloidal Gold)"
* #1769 ^property[0].code = #hints 
* #1769 ^property[0].valueString = "T^E^B" 
* #1769 ^property[1].code = #Relationships 
* #1769 ^property[1].valueString = "$.t..ma" 
* #1773 "Wuhan Life Origin Biotech Joint Stock Co., Ltd., SARS-CoV-2 Antigen Assay Kit (Immunochromatography)"
* #1773 ^property[0].code = #hints 
* #1773 ^property[0].valueString = "T^E^B" 
* #1773 ^property[1].code = #Relationships 
* #1773 ^property[1].valueString = "$.t..ma" 
* #1775 "MEXACARE GmbH, MEXACARE COVID-19 Antigen Rapid Test"
* #1775 ^property[0].code = #hints 
* #1775 ^property[0].valueString = "T^E^B" 
* #1775 ^property[1].code = #Relationships 
* #1775 ^property[1].valueString = "$.t..ma" 
* #1778 "Beijing Kewei Clinical Diagnostic Reagent Inc, COVID19 Antigen Rapid Test Kit"
* #1778 ^property[0].code = #hints 
* #1778 ^property[0].valueString = "T^E^B" 
* #1778 ^property[1].code = #Relationships 
* #1778 ^property[1].valueString = "$.t..ma" 
* #1780 "Spring Healthcare Services AG, SARS-Cov-2 Antigen Rapid Test Cassette (swab)"
* #1780 ^property[0].code = #hints 
* #1780 ^property[0].valueString = "T^E^B" 
* #1780 ^property[1].code = #Relationships 
* #1780 ^property[1].valueString = "$.t..ma" 
* #1783 "InTec PRODUCTS, INC, Rapid SARS-CoV-2 Antigen Test (nasopharyngeal/nasal specimen)"
* #1783 ^property[0].code = #hints 
* #1783 ^property[0].valueString = "T^E^B" 
* #1783 ^property[1].code = #Relationships 
* #1783 ^property[1].valueString = "$.t..ma" 
* #1791 "Immunospark s.r.l., Rapid SARS-Cov2 Antigen Test"
* #1791 ^property[0].code = #hints 
* #1791 ^property[0].valueString = "T^E^B" 
* #1791 ^property[1].code = #Relationships 
* #1791 ^property[1].valueString = "$.t..ma" 
* #1800 "Avalun, Ksmart SARS-COV2 Antigen Rapid Test"
* #1800 ^property[0].code = #hints 
* #1800 ^property[0].valueString = "T^E^B" 
* #1800 ^property[1].code = #Relationships 
* #1800 ^property[1].valueString = "$.t..ma" 
* #1801 "Innova Medical Group.Inc, Innova SARS-CoV-2 Antigen Rapid Qualitative Test"
* #1801 ^property[0].code = #hints 
* #1801 ^property[0].valueString = "T^E^B" 
* #1801 ^property[1].code = #Relationships 
* #1801 ^property[1].valueString = "$.t..ma" 
* #1813 "Shenzhen Kisshealth Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (GICA)"
* #1813 ^property[0].code = #hints 
* #1813 ^property[0].valueString = "T^E^B" 
* #1813 ^property[1].code = #Relationships 
* #1813 ^property[1].valueString = "$.t..ma" 
* #1815 "Anhui DeepBlue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) ? Nasal swab"
* #1815 ^property[0].code = #hints 
* #1815 ^property[0].valueString = "T^E^B" 
* #1815 ^property[1].code = #Relationships 
* #1815 ^property[1].valueString = "$.t..ma" 
* #1820 "Getein Biotech, Inc, SARS-CoV-2 Antigen (Colloidal Gold)"
* #1820 ^property[0].code = #hints 
* #1820 ^property[0].valueString = "T^E^B" 
* #1820 ^property[1].code = #Relationships 
* #1820 ^property[1].valueString = "$.t..ma" 
* #1822 "Anbio (Xiamen) Biotechnology Co., Ltd., Rapid COVID-19 Antigen-Test (colloidal Gold)"
* #1822 ^property[0].code = #hints 
* #1822 ^property[0].valueString = "T^E^B" 
* #1822 ^property[1].code = #Relationships 
* #1822 ^property[1].valueString = "$.t..ma" 
* #1833 "AAZ-LMB, COVID-VIRO Rapid antigen test COVID-19"
* #1833 ^property[0].code = #hints 
* #1833 ^property[0].valueString = "T^E^B" 
* #1833 ^property[1].code = #Relationships 
* #1833 ^property[1].valueString = "$.t..ma" 
* #1849 "Nanjing Vazyme Medical Technology Co., Ltd., Severe Acute Respiratory Syndrome Coronavirus 2 (SARS-CoV-2) Antigen Detection Kit (Colloidal Gold-Based)"
* #1849 ^property[0].code = #hints 
* #1849 ^property[0].valueString = "T^E^B" 
* #1849 ^property[1].code = #Relationships 
* #1849 ^property[1].valueString = "$.t..ma" 
* #1855 "GA Generic Assays GmbH, GA CoV-2 Antigen Rapid Test"
* #1855 ^property[0].code = #hints 
* #1855 ^property[0].valueString = "T^E^B" 
* #1855 ^property[1].code = #Relationships 
* #1855 ^property[1].valueString = "$.t..ma" 
* #1865 "ACON Biotech(Hangzhou) Co., Ltd., Flowflex SARS-CoV-2 Antigen Rapid Test (Nasal/Saliva)"
* #1865 ^property[0].code = #hints 
* #1865 ^property[0].valueString = "T^E^B" 
* #1865 ^property[1].code = #Relationships 
* #1865 ^property[1].valueString = "$.t..ma" 
* #1870 "Beijing Hotgen Biotech Co., Ltd., Novel Coronavirus 2019-nCoV Antigen Test (Colloidal Gold)"
* #1870 ^property[0].code = #hints 
* #1870 ^property[0].valueString = "T^E^B" 
* #1870 ^property[1].code = #Relationships 
* #1870 ^property[1].valueString = "$.t..ma" 
* #1876 "Hangzhou Biotest Biotech Co., Ltd, COVID-19 Antigen Rapid Test Cassette(Nasal Swab)"
* #1876 ^property[0].code = #hints 
* #1876 ^property[0].valueString = "T^E^B" 
* #1876 ^property[1].code = #Relationships 
* #1876 ^property[1].valueString = "$.t..ma" 
* #1880 "NG Biotech, Ninonasal"
* #1880 ^property[0].code = #hints 
* #1880 ^property[0].valueString = "T^E^B" 
* #1880 ^property[1].code = #Relationships 
* #1880 ^property[1].valueString = "$.t..ma" 
* #1884 "Xiamen Wiz Biotech Co., Ltd., SARS-CoV-2 Antigen Rapid Test (Colloidal Gold)"
* #1884 ^property[0].code = #hints 
* #1884 ^property[0].valueString = "T^E^B" 
* #1884 ^property[1].code = #Relationships 
* #1884 ^property[1].valueString = "$.t..ma" 
* #1899 "Jiangsu Konsung Bio-Medical Science and Technology Co., H+W_COVID-19 Antigen Rapid Test Kit (Colloidal Gold)"
* #1899 ^property[0].code = #hints 
* #1899 ^property[0].valueString = "T^E^B" 
* #1899 ^property[1].code = #Relationships 
* #1899 ^property[1].valueString = "$.t..ma" 
* #1902 "Zhuhai Encode Medical Engineering Co.,Ltd, ENCODE SARS-COV-2 Antigen Rapid Test Device"
* #1902 ^property[0].code = #hints 
* #1902 ^property[0].valueString = "T^E^B" 
* #1902 ^property[1].code = #Relationships 
* #1902 ^property[1].valueString = "$.t..ma" 
* #1920 "Jiangsu Diagnostics Biotechnology Co., Ltd, COVID-19 Antigen Rapid Test Cassette (Colloidal Gold)"
* #1920 ^property[0].code = #hints 
* #1920 ^property[0].valueString = "T^E^B" 
* #1920 ^property[1].code = #Relationships 
* #1920 ^property[1].valueString = "$.t..ma" 
* #1926 "ARISTA Biotech Pte.LTD., ARISTA COVID-19 Antigen Rapid Test"
* #1926 ^property[0].code = #hints 
* #1926 ^property[0].valueString = "T^E^B" 
* #1926 ^property[1].code = #Relationships 
* #1926 ^property[1].valueString = "$.t..ma" 
* #1929 "Hoyotek Biomedical Co.,Ltd., Corona Virus (COVID-19) Antigen Rapid Test (Colloidal Gold)"
* #1929 ^property[0].code = #hints 
* #1929 ^property[0].valueString = "T^E^B" 
* #1929 ^property[1].code = #Relationships 
* #1929 ^property[1].valueString = "$.t..ma" 
* #1942 "Surge Medical Inc., COVID-19 Antigen Test Kit"
* #1942 ^property[0].code = #hints 
* #1942 ^property[0].valueString = "T^E^B" 
* #1942 ^property[1].code = #Relationships 
* #1942 ^property[1].valueString = "$.t..ma" 
* #1945 "Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette"
* #1945 ^property[0].code = #hints 
* #1945 ^property[0].valueString = "T^E^B" 
* #1945 ^property[1].code = #Relationships 
* #1945 ^property[1].valueString = "$.t..ma" 
* #1952 "Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette (nasal, nasopharyngeal, oropharyngeal, saliva)"
* #1952 ^property[0].code = #hints 
* #1952 ^property[0].valueString = "T^E^B" 
* #1952 ^property[1].code = #Relationships 
* #1952 ^property[1].valueString = "$.t..ma" 
* #1957 "Zhuhai Lituo Biotechnology Co., Ltd., COVID-19 Antigen Detection Kit (Colloidal Gold)"
* #1957 ^property[0].code = #hints 
* #1957 ^property[0].valueString = "T^E^B" 
* #1957 ^property[1].code = #Relationships 
* #1957 ^property[1].valueString = "$.t..ma" 
* #1962 "Rapigen Inc., BIOCREDIT COVID-19 Ag"
* #1962 ^property[0].code = #hints 
* #1962 ^property[0].valueString = "T^E^B" 
* #1962 ^property[1].code = #Relationships 
* #1962 ^property[1].valueString = "$.t..ma" 
* #1963 "Rapigen Inc., BIOCREDIT COVID-19 Ag Test Nasal"
* #1963 ^property[0].code = #hints 
* #1963 ^property[0].valueString = "T^E^B" 
* #1963 ^property[1].code = #Relationships 
* #1963 ^property[1].valueString = "$.t..ma" 
* #1967 "Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold Chromatographic Immunoassay)"
* #1967 ^property[0].code = #hints 
* #1967 ^property[0].valueString = "T^E^B" 
* #1967 ^property[1].code = #Relationships 
* #1967 ^property[1].valueString = "$.t..ma" 
* #1968 "Shenzhen Microprofit Biotech Co., Ltd., SARS-CoV-2 Antigen Test Kit (Fluorescence Immunoassay)"
* #1968 ^property[0].code = #hints 
* #1968 ^property[0].valueString = "T^E^B" 
* #1968 ^property[1].code = #Relationships 
* #1968 ^property[1].valueString = "$.t..ma" 
* #1988 "Inzek International Trading B.V., Biozek covid-19 Antigen Rapidtest BCOV-502"
* #1988 ^property[0].code = #hints 
* #1988 ^property[0].valueString = "T^E^B" 
* #1988 ^property[1].code = #Relationships 
* #1988 ^property[1].valueString = "$.t..ma" 
* #1989 "Boditech Med Inc, AFIAS COVID-19 Ag"
* #1989 ^property[0].code = #hints 
* #1989 ^property[0].valueString = "T^E^B" 
* #1989 ^property[1].code = #Relationships 
* #1989 ^property[1].valueString = "$.t..ma" 
* #2006 "Jiangsu Medomics medical technology Co.,Ltd., SARS-CoV-2 antigen Test Kit (LFIA)"
* #2006 ^property[0].code = #hints 
* #2006 ^property[0].valueString = "T^E^B" 
* #2006 ^property[1].code = #Relationships 
* #2006 ^property[1].valueString = "$.t..ma" 
* #2012 "Genrui Biotech Inc, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2012 ^property[0].code = #hints 
* #2012 ^property[0].valueString = "T^E^B" 
* #2012 ^property[1].code = #Relationships 
* #2012 ^property[1].valueString = "$.t..ma" 
* #2013 "Biotical Health S.L.U.BIOTICAL HEALTH S.L.U, biotical SARS-CoV-2 Ag Card"
* #2013 ^property[0].code = #hints 
* #2013 ^property[0].valueString = "T^E^B" 
* #2013 ^property[1].code = #Relationships 
* #2013 ^property[1].valueString = "$.t..ma" 
* #2017 "Shenzen Ultra-Diagnostics Biotec Co., SARS-CoV-2 Antigen test Kit (colloidal gold)"
* #2017 ^property[0].code = #hints 
* #2017 ^property[0].valueString = "T^E^B" 
* #2017 ^property[1].code = #Relationships 
* #2017 ^property[1].valueString = "$.t..ma" 
* #2026 "Shenzhen Reagent Technology Co.,Ltd, SARS-CoV-2 antigen IVD kit SWAB"
* #2026 ^property[0].code = #hints 
* #2026 ^property[0].valueString = "T^E^B" 
* #2026 ^property[1].code = #Relationships 
* #2026 ^property[1].valueString = "$.t..ma" 
* #2029 "Merlin Biomedical (Xiamen) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Cassette"
* #2029 ^property[0].code = #hints 
* #2029 ^property[0].valueString = "T^E^B" 
* #2029 ^property[1].code = #Relationships 
* #2029 ^property[1].valueString = "$.t..ma" 
* #2031 "BIO-RAD, CORONAVIRUS AG RAPID TEST CASSETTE"
* #2031 ^property[0].code = #hints 
* #2031 ^property[0].valueString = "T^E^B" 
* #2031 ^property[1].code = #Relationships 
* #2031 ^property[1].valueString = "$.t..ma" 
* #2035 "BioMaxima SA, SARS-CoV-2 Ag Rapid Test"
* #2035 ^property[0].code = #hints 
* #2035 ^property[0].valueString = "T^E^B" 
* #2035 ^property[1].code = #Relationships 
* #2035 ^property[1].valueString = "$.t..ma" 
* #2038 "Koch Biotechnology (Beijing) Co., Ltd, COVID-19 Antigen Rapid Test Kit"
* #2038 ^property[0].code = #hints 
* #2038 ^property[0].valueString = "T^E^B" 
* #2038 ^property[1].code = #Relationships 
* #2038 ^property[1].valueString = "$.t..ma" 
* #2052 "SD BIOSENSOR Inc. (manufacturer), STANDARD Q COVID-19 Ag Test Nasal"
* #2052 ^property[0].code = #hints 
* #2052 ^property[0].valueString = "T^E^B" 
* #2052 ^property[1].code = #Relationships 
* #2052 ^property[1].valueString = "$.t..ma" 
* #2063 "Hangzhou Sejoy Electronics & Instruments Co.,Ltd., SARS-CoV-2 & Influenza A+B Antigen Combo Rapid Test Cassette"
* #2063 ^property[0].code = #hints 
* #2063 ^property[0].valueString = "T^E^B" 
* #2063 ^property[1].code = #Relationships 
* #2063 ^property[1].valueString = "$.t..ma" 
* #2067 "BIOTEKE CORPORATION (WUXI) CO., LTD, SARS-CoV-2 Antigen Test Kit (colloidal gold method)"
* #2067 ^property[0].code = #hints 
* #2067 ^property[0].valueString = "T^E^B" 
* #2067 ^property[1].code = #Relationships 
* #2067 ^property[1].valueString = "$.t..ma" 
* #2072 "Beijing Jinwofu Bioengineering Technology Co.,Ltd., Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Kit"
* #2072 ^property[0].code = #hints 
* #2072 ^property[0].valueString = "T^E^B" 
* #2072 ^property[1].code = #Relationships 
* #2072 ^property[1].valueString = "$.t..ma" 
* #2074 "Triplex International Biosciences Co., Ltd, SARS-CoV-2 Antigen Rapid Test Kit"
* #2074 ^property[0].code = #hints 
* #2074 ^property[0].valueString = "T^E^B" 
* #2074 ^property[1].code = #Relationships 
* #2074 ^property[1].valueString = "$.t..ma" 
* #2075 "Amazing Biotech (Shanghai) Co.,LTD., COVID-19 Antigen Sealing tube test strip (Colloidal Gold)"
* #2075 ^property[0].code = #hints 
* #2075 ^property[0].valueString = "T^E^B" 
* #2075 ^property[1].code = #Relationships 
* #2075 ^property[1].valueString = "$.t..ma" 
* #2078 "ArcDia International Oy Ltd, mariPOC Respi+"
* #2078 ^property[0].code = #hints 
* #2078 ^property[0].valueString = "T^E^B" 
* #2078 ^property[1].code = #Relationships 
* #2078 ^property[1].valueString = "$.t..ma" 
* #2079 "ArcDia International Oy Ltd, mariPOC Quick Flu+"
* #2079 ^property[0].code = #hints 
* #2079 ^property[0].valueString = "T^E^B" 
* #2079 ^property[1].code = #Relationships 
* #2079 ^property[1].valueString = "$.t..ma" 
* #2089 "Anhui Formaster Biosci Co., Ltd., New Coronavirus (COVID-19) Antigen Rapid Test"
* #2089 ^property[0].code = #hints 
* #2089 ^property[0].valueString = "T^E^B" 
* #2089 ^property[1].code = #Relationships 
* #2089 ^property[1].valueString = "$.t..ma" 
* #2090 "Wuhan UNscience Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit"
* #2090 ^property[0].code = #hints 
* #2090 ^property[0].valueString = "T^E^B" 
* #2090 ^property[1].code = #Relationships 
* #2090 ^property[1].valueString = "$.t..ma" 
* #2097 "Sansure Biotech Inc, SARS-CoV-2 Rapid Antigen Test (Colloidal Gold Method)"
* #2097 ^property[0].code = #hints 
* #2097 ^property[0].valueString = "T^E^B" 
* #2097 ^property[1].code = #Relationships 
* #2097 ^property[1].valueString = "$.t..ma" 
* #2098 "Wuhan EasyDiagnosis Biomedicine Co., Ltd., COVID-19  (SARS-CoV-2) Antigen-Test kit"
* #2098 ^property[0].code = #hints 
* #2098 ^property[0].valueString = "T^E^B" 
* #2098 ^property[1].code = #Relationships 
* #2098 ^property[1].valueString = "$.t..ma" 
* #2100 "VivaChek Biotech (Hangzhou) Co., Ltd, China, Verino Pro SARS CoV 2 Ag Rapid Test"
* #2100 ^property[0].code = #hints 
* #2100 ^property[0].valueString = "T^E^B" 
* #2100 ^property[1].code = #Relationships 
* #2100 ^property[1].valueString = "$.t..ma" 
* #2101 "AXIOM Gesellschaft für Diagnostica und Biochemica mbH, COVID-19 Antigen Rapid Test"
* #2101 ^property[0].code = #hints 
* #2101 ^property[0].valueString = "T^E^B" 
* #2101 ^property[1].code = #Relationships 
* #2101 ^property[1].valueString = "$.t..ma" 
* #2107 "Jiangsu Bioperfectus Technologies Co., Ltd., Novel Corona Virus (SARS-CoV-2) Ag Rapid Test Kit"
* #2107 ^property[0].code = #hints 
* #2107 ^property[0].valueString = "T^E^B" 
* #2107 ^property[1].code = #Relationships 
* #2107 ^property[1].valueString = "$.t..ma" 
* #2108 "AESKU.Diagnostics GmbH & Co KG, AESKU.RAPID SARS-CoV-2"
* #2108 ^property[0].code = #hints 
* #2108 ^property[0].valueString = "T^E^B" 
* #2108 ^property[1].code = #Relationships 
* #2108 ^property[1].valueString = "$.t..ma" 
* #2109 "Shenzhen Lvshiyuan Biotechnology Co., Ltd., Green Spring SARS-CoV-2 Antigen-Rapid test-Set"
* #2109 ^property[0].code = #hints 
* #2109 ^property[0].valueString = "T^E^B" 
* #2109 ^property[1].code = #Relationships 
* #2109 ^property[1].valueString = "$.t..ma" 
* #2111 "VivaChek Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Ag Rapid Test"
* #2111 ^property[0].code = #hints 
* #2111 ^property[0].valueString = "T^E^B" 
* #2111 ^property[1].code = #Relationships 
* #2111 ^property[1].valueString = "$.t..ma" 
* #2116 "PerGrande Biotech Development Co., Ltd., SARS-CoV-2 Antigen Detection Kit (Colloidal Gold Immunochromato-graphic assay)"
* #2116 ^property[0].code = #hints 
* #2116 ^property[0].valueString = "T^E^B" 
* #2116 ^property[1].code = #Relationships 
* #2116 ^property[1].valueString = "$.t..ma" 
* #2124 "Fujirebio, Lumipulse G SARS-CoV-2 Ag"
* #2124 ^property[0].code = #hints 
* #2124 ^property[0].valueString = "T^E^B" 
* #2124 ^property[1].code = #Relationships 
* #2124 ^property[1].valueString = "$.t..ma" 
* #2128 "Lumigenex (Suzhou) Co., Ltd, PocRoc SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold)"
* #2128 ^property[0].code = #hints 
* #2128 ^property[0].valueString = "T^E^B" 
* #2128 ^property[1].code = #Relationships 
* #2128 ^property[1].valueString = "$.t..ma" 
* #2130 "Affimedix Inc., TestNOW - COVID-19 Antigen Test"
* #2130 ^property[0].code = #hints 
* #2130 ^property[0].valueString = "T^E^B" 
* #2130 ^property[1].code = #Relationships 
* #2130 ^property[1].valueString = "$.t..ma" 
* #2139 "Hangzhou Lysun Biotechnology Co., Ltd., COVID-19 antigen Rapid Test Device (Colloidal Gold)"
* #2139 ^property[0].code = #hints 
* #2139 ^property[0].valueString = "T^E^B" 
* #2139 ^property[1].code = #Relationships 
* #2139 ^property[1].valueString = "$.t..ma" 
* #2143 "Wuxi Biohermes Bio & Medical Technology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Lateral Flow Assay)"
* #2143 ^property[0].code = #hints 
* #2143 ^property[0].valueString = "T^E^B" 
* #2143 ^property[1].code = #Relationships 
* #2143 ^property[1].valueString = "$.t..ma" 
* #2144 "Jiangsu Well Biotech Co., Ltd., COVID-19 Ag Rapid Test Device"
* #2144 ^property[0].code = #hints 
* #2144 ^property[0].valueString = "T^E^B" 
* #2144 ^property[1].code = #Relationships 
* #2144 ^property[1].valueString = "$.t..ma" 
* #2147 "Fujirebio , ESPLINE SARS-CoV-2"
* #2147 ^property[0].code = #hints 
* #2147 ^property[0].valueString = "T^E^B" 
* #2147 ^property[1].code = #Relationships 
* #2147 ^property[1].valueString = "$.t..ma" 
* #2150 "Chongqing M&D Biotechnology Co. Ltd, 2019-nCoV Antigen Test Kit"
* #2150 ^property[0].code = #hints 
* #2150 ^property[0].valueString = "T^E^B" 
* #2150 ^property[1].code = #Relationships 
* #2150 ^property[1].valueString = "$.t..ma" 
* #2152 "Shenzhen CAS-Envision Medical Technology Co., Ltd., SARS-CoV-2-Antigen Rapid Detection Kit"
* #2152 ^property[0].code = #hints 
* #2152 ^property[0].valueString = "T^E^B" 
* #2152 ^property[1].code = #Relationships 
* #2152 ^property[1].valueString = "$.t..ma" 
* #2164 "Nanjing Synthgene Medical Technology Co., Ltd., SARS-COV-2 Nucleocapsid (N) Antigen Rapid Detection Kit (Colloidal gold method)"
* #2164 ^property[0].code = #hints 
* #2164 ^property[0].valueString = "T^E^B" 
* #2164 ^property[1].code = #Relationships 
* #2164 ^property[1].valueString = "$.t..ma" 
* #2183 "Getein Biotech, Inc., One Step Test for SARS-CoV-2 Antigen (Colloidal Gold)"
* #2183 ^property[0].code = #hints 
* #2183 ^property[0].valueString = "T^E^B" 
* #2183 ^property[1].code = #Relationships 
* #2183 ^property[1].valueString = "$.t..ma" 
* #2200 "NanoRepro AG, NanoRepro SARS-CoV-2 Antigen Rapid Test"
* #2200 ^property[0].code = #hints 
* #2200 ^property[0].valueString = "T^E^B" 
* #2200 ^property[1].code = #Relationships 
* #2200 ^property[1].valueString = "$.t..ma" 
* #2201 "Zybio Inc., SARS-CoV-2 Antigen Assay Kit (Colloidal Gold Method)"
* #2201 ^property[0].code = #hints 
* #2201 ^property[0].valueString = "T^E^B" 
* #2201 ^property[1].code = #Relationships 
* #2201 ^property[1].valueString = "$.t..ma" 
* #2226 "ZET medikal tekstil dis tic Ltd. Sti, SOFTEC SARS COV-2 COVID-19 ANTIGEN TEST KIT"
* #2226 ^property[0].code = #hints 
* #2226 ^property[0].valueString = "T^E^B" 
* #2226 ^property[1].code = #Relationships 
* #2226 ^property[1].valueString = "$.t..ma" 
* #2228 "Roche (SD BIOSENSOR), SARS-CoV-2 Rapid Antigen Test Nasal"
* #2228 ^property[0].code = #hints 
* #2228 ^property[0].valueString = "T^E^B" 
* #2228 ^property[1].code = #Relationships 
* #2228 ^property[1].valueString = "$.t..ma" 
* #2230 "BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test (Colloidal Gold Method)"
* #2230 ^property[0].code = #hints 
* #2230 ^property[0].valueString = "T^E^B" 
* #2230 ^property[1].code = #Relationships 
* #2230 ^property[1].valueString = "$.t..ma" 
* #2233 "Shenzhen Microprofit Biotech Co., Ltd., SARS-CoV-2 & Influenza A/B & RSV Antigen Combo Test Kit (Colloidal Gold Chromatographic Immunoassay)"
* #2233 ^property[0].code = #hints 
* #2233 ^property[0].valueString = "T^E^B" 
* #2233 ^property[1].code = #Relationships 
* #2233 ^property[1].valueString = "$.t..ma" 
* #2241 "NESAPOR EUROPA SL, MARESKIT COVID-19 ANTIGEN RAPID TEST KIT"
* #2241 ^property[0].code = #hints 
* #2241 ^property[0].valueString = "T^E^B" 
* #2241 ^property[1].code = #Relationships 
* #2241 ^property[1].valueString = "$.t..ma" 
* #2242 "DNA Diagnostic, COVID-19 Antigen Detection Kit"
* #2242 ^property[0].code = #hints 
* #2242 ^property[0].valueString = "T^E^B" 
* #2242 ^property[1].code = #Relationships 
* #2242 ^property[1].valueString = "$.t..ma" 
* #2247 "BioGnost Ltd, CoviGnost AG Test Device 1x20"
* #2247 ^property[0].code = #hints 
* #2247 ^property[0].valueString = "T^E^B" 
* #2247 ^property[1].code = #Relationships 
* #2247 ^property[1].valueString = "$.t..ma" 
* #2256 "Sigmed Sp. z o.o, Redtest Professional Sars-CoV-2 Antigen Rapid Test (Covid-19 Ag)"
* #2256 ^property[0].code = #hints 
* #2256 ^property[0].valueString = "T^E^B" 
* #2256 ^property[1].code = #Relationships 
* #2256 ^property[1].valueString = "$.t..ma" 
* #2257 "Hangzhou AllTest Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test (Nasal Swab)"
* #2257 ^property[0].code = #hints 
* #2257 ^property[0].valueString = "T^E^B" 
* #2257 ^property[1].code = #Relationships 
* #2257 ^property[1].valueString = "$.t..ma" 
* #2260 "Multi-G bvba, Covid19Check-NAS"
* #2260 ^property[0].code = #hints 
* #2260 ^property[0].valueString = "T^E^B" 
* #2260 ^property[1].code = #Relationships 
* #2260 ^property[1].valueString = "$.t..ma" 
* #2267 "ACRO Biotech Inc, SARS-CoV-2 Antigen Rapid Test (Nasal Swab)"
* #2267 ^property[0].code = #hints 
* #2267 ^property[0].valueString = "T^E^B" 
* #2267 ^property[1].code = #Relationships 
* #2267 ^property[1].valueString = "$.t..ma" 
* #2271 "Pantest SA, Pantest Coronavirus Ag (Nasopharyngeal Swab)"
* #2271 ^property[0].code = #hints 
* #2271 ^property[0].valueString = "T^E^B" 
* #2271 ^property[1].code = #Relationships 
* #2271 ^property[1].valueString = "$.t..ma" 
* #2273 "Dräger Safety AG & Co. KGaA, Dräger Antigen Test SARS-CoV-2"
* #2273 ^property[0].code = #hints 
* #2273 ^property[0].valueString = "T^E^B" 
* #2273 ^property[1].code = #Relationships 
* #2273 ^property[1].valueString = "$.t..ma" 
* #2277 "Assure Tech. (Hangzhou) Co., Ltd., COVID-19 Antigen Nasal Test Kit"
* #2277 ^property[0].code = #hints 
* #2277 ^property[0].valueString = "T^E^B" 
* #2277 ^property[1].code = #Relationships 
* #2277 ^property[1].valueString = "$.t..ma" 
* #2278 "Innovation Biotech(Beijing) Co.Ltd, Coronavirus (SARS-Cov-2) Antigen Rapid Test Cassette (Nasal swab)"
* #2278 ^property[0].code = #hints 
* #2278 ^property[0].valueString = "T^E^B" 
* #2278 ^property[1].code = #Relationships 
* #2278 ^property[1].valueString = "$.t..ma" 
* #2280 "Assure Tech. (Hangzhou) Co., Ltd., COVID-19 & Influenza A/B Antigen Nasal Test Kit"
* #2280 ^property[0].code = #hints 
* #2280 ^property[0].valueString = "T^E^B" 
* #2280 ^property[1].code = #Relationships 
* #2280 ^property[1].valueString = "$.t..ma" 
* #2282 "Becton Dickinson, BD Kit for Rapid Detection of SARS-CoV-2"
* #2282 ^property[0].code = #hints 
* #2282 ^property[0].valueString = "T^E^B" 
* #2282 ^property[1].code = #Relationships 
* #2282 ^property[1].valueString = "$.t..ma" 
* #2290 "Rapid Pathogen Screening, Inc, LIAISON Quick Detect Covid Ag Assay"
* #2290 ^property[0].code = #hints 
* #2290 ^property[0].valueString = "T^E^B" 
* #2290 ^property[1].code = #Relationships 
* #2290 ^property[1].valueString = "$.t..ma" 
* #2297 "SureScreen Diagnostics, SARS-CoV-2 Rapid Antigen Test Cassette"
* #2297 ^property[0].code = #hints 
* #2297 ^property[0].valueString = "T^E^B" 
* #2297 ^property[1].code = #Relationships 
* #2297 ^property[1].valueString = "$.t..ma" 
* #2301 "Nanjing Liming Bio-Products Co., Ltd., StrongStep SARS-CoV-2 Antigen Rapid Test"
* #2301 ^property[0].code = #hints 
* #2301 ^property[0].valueString = "T^E^B" 
* #2301 ^property[1].code = #Relationships 
* #2301 ^property[1].valueString = "$.t..ma" 
* #2302 "Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Test Cassette (Nasopharyngeal Swab)(FIA)"
* #2302 ^property[0].code = #hints 
* #2302 ^property[0].valueString = "T^E^B" 
* #2302 ^property[1].code = #Relationships 
* #2302 ^property[1].valueString = "$.t..ma" 
* #2317 "Hangzhou Immuno BiotechCo., Ltd, SARS-CoV2 Antigen Rapid Test"
* #2317 ^property[0].code = #hints 
* #2317 ^property[0].valueString = "T^E^B" 
* #2317 ^property[1].code = #Relationships 
* #2317 ^property[1].valueString = "$.t..ma" 
* #2319 "Hangzhou AllTest Biotech Co., Ltd, CVAG4080A ? GSD NovaGen SARS-CoV-2 Ag Rapid Test (NP Swab)"
* #2319 ^property[0].code = #hints 
* #2319 ^property[0].valueString = "T^E^B" 
* #2319 ^property[1].code = #Relationships 
* #2319 ^property[1].valueString = "$.t..ma" 
* #2325 "Hangzhou AllTest Biotech Co., Ltd, GSD NovaGen SARS-CoV-2 Ag Rapid Test (Nasal Swab)"
* #2325 ^property[0].code = #hints 
* #2325 ^property[0].valueString = "T^E^B" 
* #2325 ^property[1].code = #Relationships 
* #2325 ^property[1].valueString = "$.t..ma" 
* #2350 "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device"
* #2350 ^property[0].code = #hints 
* #2350 ^property[0].valueString = "T^E^B" 
* #2350 ^property[1].code = #Relationships 
* #2350 ^property[1].valueString = "$.t..ma" 
* #2374 "ABIOTEQ, Cora Gentest-19"
* #2374 ^property[0].code = #hints 
* #2374 ^property[0].valueString = "T^E^B" 
* #2374 ^property[1].code = #Relationships 
* #2374 ^property[1].valueString = "$.t..ma" 
* #2380 "BioSpeedia International, COVID19Speed-Antigen Test BSD_0503"
* #2380 ^property[0].code = #hints 
* #2380 ^property[0].valueString = "T^E^B" 
* #2380 ^property[1].code = #Relationships 
* #2380 ^property[1].valueString = "$.t..ma" 
* #2388 "Citest Diagnostics Inc., COVID-19 Antigen Test Cassette (Nasopharyngeal Swab) (FIA)"
* #2388 ^property[0].code = #hints 
* #2388 ^property[0].valueString = "T^E^B" 
* #2388 ^property[1].code = #Relationships 
* #2388 ^property[1].valueString = "$.t..ma" 
* #2414 "Shenzhen Huian Biosci Technology Co., Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2414 ^property[0].code = #hints 
* #2414 ^property[0].valueString = "T^E^B" 
* #2414 ^property[1].code = #Relationships 
* #2414 ^property[1].valueString = "$.t..ma" 
* #2415 "Shenzhen Dymind Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2415 ^property[0].code = #hints 
* #2415 ^property[0].valueString = "T^E^B" 
* #2415 ^property[1].code = #Relationships 
* #2415 ^property[1].valueString = "$.t..ma" 
* #2419 "InTec PRODUCTS, INC., Rapid SARS-CoV-2 Antigen Test (nasopharyngeal specimen)"
* #2419 ^property[0].code = #hints 
* #2419 ^property[0].valueString = "T^E^B" 
* #2419 ^property[1].code = #Relationships 
* #2419 ^property[1].valueString = "$.t..ma" 
* #2449 "Citest Diagnostics Inc., COVID-19 Antigen Rapid Test (Swab)"
* #2449 ^property[0].code = #hints 
* #2449 ^property[0].valueString = "T^E^B" 
* #2449 ^property[1].code = #Relationships 
* #2449 ^property[1].valueString = "$.t..ma" 
* #24756 "SD BIOSENSOR Inc., STANDARD i-Q COVID/Flu Ag Combo Test"
* #24756 ^property[0].code = #hints 
* #24756 ^property[0].valueString = "T^E^B" 
* #24756 ^property[1].code = #Relationships 
* #24756 ^property[1].valueString = "$.t..ma" 
* #24757 "SD BIOSENSOR Inc., STANDARD F COVID/Flu Ag Combo FIA"
* #24757 ^property[0].code = #hints 
* #24757 ^property[0].valueString = "T^E^B" 
* #24757 ^property[1].code = #Relationships 
* #24757 ^property[1].valueString = "$.t..ma" 
* #24758 "SD BIOSENSOR Inc., STANDARD Q COVID-19 Ag Test 2.0"
* #24758 ^property[0].code = #hints 
* #24758 ^property[0].valueString = "T^E^B" 
* #24758 ^property[1].code = #Relationships 
* #24758 ^property[1].valueString = "$.t..ma" 
* #2491 "Shen Zhen Zi Jian Biotechnology Co. Ltd., SARS-CoV-2 (COVID-19) Antigen Rapid Detection Kit (Lateral Flow Method)"
* #2491 ^property[0].code = #hints 
* #2491 ^property[0].valueString = "T^E^B" 
* #2491 ^property[1].code = #Relationships 
* #2491 ^property[1].valueString = "$.t..ma" 
* #2494 "Beijing O&D Biotech Co., Ltd., COVID-19 Antigen Rapid Test"
* #2494 ^property[0].code = #hints 
* #2494 ^property[0].valueString = "T^E^B" 
* #2494 ^property[1].code = #Relationships 
* #2494 ^property[1].valueString = "$.t..ma" 
* #2506 "Nanjing Norman Biological Technology Co., Ltd., Novel Coronavirus (2019-nCoV) Antigen Testing Kit (Colloidal Gold)"
* #2506 ^property[0].code = #hints 
* #2506 ^property[0].valueString = "T^E^B" 
* #2506 ^property[1].code = #Relationships 
* #2506 ^property[1].valueString = "$.t..ma" 
* #2519 "BIOLAN HEALTH, S.L, COVID-19 Antigen Rapid Test (Colloidal Gold Method)"
* #2519 ^property[0].code = #hints 
* #2519 ^property[0].valueString = "T^E^B" 
* #2519 ^property[1].code = #Relationships 
* #2519 ^property[1].valueString = "$.t..ma" 
* #2533 "Dynamiker Biotechnolgy(Tianjin) Co., Ltd., Dynamiker SARS-CoV-2 Ag Rapid Test"
* #2533 ^property[0].code = #hints 
* #2533 ^property[0].valueString = "T^E^B" 
* #2533 ^property[1].code = #Relationships 
* #2533 ^property[1].valueString = "$.t..ma" 
* #2538 "AUTOBIO DIAGNOSTICS., LTD., SARS-CoV-2 Ag Rapid Test"
* #2538 ^property[0].code = #hints 
* #2538 ^property[0].valueString = "T^E^B" 
* #2538 ^property[1].code = #Relationships 
* #2538 ^property[1].valueString = "$.t..ma" 
* #25546 "Roche (SD BIOSENSOR), SARS-CoV-2 Rapid Antigen Test 2.0"
* #25546 ^property[0].code = #hints 
* #25546 ^property[0].valueString = "T^E^B" 
* #25546 ^property[1].code = #Relationships 
* #25546 ^property[1].valueString = "$.t..ma" 
* #2555 "IEDAU INTERNATIONAL GMBH, Covid-19 Antigen Schnelltest (Colloidales Gold)"
* #2555 ^property[0].code = #hints 
* #2555 ^property[0].valueString = "T^E^B" 
* #2555 ^property[1].code = #Relationships 
* #2555 ^property[1].valueString = "$.t..ma" 
* #2560 "Lomina Superbio, a.s., Lomina SARS-CoV-2 Antigen LTX test"
* #2560 ^property[0].code = #hints 
* #2560 ^property[0].valueString = "T^E^B" 
* #2560 ^property[1].code = #Relationships 
* #2560 ^property[1].valueString = "$.t..ma" 
* #2579 "AccuBioTech Co.,Ltd, Accu-Tell SARS-CoV-2 Ag Cassette"
* #2579 ^property[0].code = #hints 
* #2579 ^property[0].valueString = "T^E^B" 
* #2579 ^property[1].code = #Relationships 
* #2579 ^property[1].valueString = "$.t..ma" 
* #2584 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., INFO Covid-19 Ag Test"
* #2584 ^property[0].code = #hints 
* #2584 ^property[0].valueString = "T^E^B" 
* #2584 ^property[1].code = #Relationships 
* #2584 ^property[1].valueString = "$.t..ma" 
* #2586 "Jiangsu Mole Bioscience CO., LTD., SARS-CoV-2 Antigen Test Cassette"
* #2586 ^property[0].code = #hints 
* #2586 ^property[0].valueString = "T^E^B" 
* #2586 ^property[1].code = #Relationships 
* #2586 ^property[1].valueString = "$.t..ma" 
* #2588 "Changzhou Biowin Pharmaceutical Co.,Ltd., Novel Coronavirus(COVID-19) Antigen Test Kit (Colloidal Gold)"
* #2588 ^property[0].code = #hints 
* #2588 ^property[0].valueString = "T^E^B" 
* #2588 ^property[1].code = #Relationships 
* #2588 ^property[1].valueString = "$.t..ma" 
* #260373001 "detected"
* #260373001 ^designation[0].language = #de-AT 
* #260373001 ^designation[0].value = "nachgewiesen" 
* #260373001 ^property[0].code = #Relationships 
* #260373001 ^property[0].valueString = "$.t..tr" 
* #260415000 "not detected"
* #260415000 ^designation[0].language = #de-AT 
* #260415000 ^designation[0].value = "nicht nachgewiesen" 
* #260415000 ^property[0].code = #hints 
* #260415000 ^property[0].valueString = "T^E^B" 
* #260415000 ^property[1].code = #Relationships 
* #260415000 ^property[1].valueString = "$.t..tr" 
* #2608 "Neo-nostics (Suzhou) Bioengineering Co., Ltd., COVID 19 Antigen Test Kit (Colloidal Gold Method)"
* #2608 ^property[0].code = #hints 
* #2608 ^property[0].valueString = "T^E^B" 
* #2608 ^property[1].code = #Relationships 
* #2608 ^property[1].valueString = "$.t..ma" 
* #2629 "Hangzhou DIAN Biotechnology Co., Ltd., COVID-19 Antigen Test Cassette"
* #2629 ^property[0].code = #hints 
* #2629 ^property[0].valueString = "T^E^B" 
* #2629 ^property[1].code = #Relationships 
* #2629 ^property[1].valueString = "$.t..ma" 
* #2640 "Mologic Ltd, COVIOS Ag COVID-19 Antigen Rapid Diagnostic Test"
* #2640 ^property[0].code = #hints 
* #2640 ^property[0].valueString = "T^E^B" 
* #2640 ^property[1].code = #Relationships 
* #2640 ^property[1].valueString = "$.t..ma" 
* #2642 "Genobio Pharmaceutical Co., Ltd., Virusee SARS-CoV-2 Antigen Rapid Test (Colloidal Gold)"
* #2642 ^property[0].code = #hints 
* #2642 ^property[0].valueString = "T^E^B" 
* #2642 ^property[1].code = #Relationships 
* #2642 ^property[1].valueString = "$.t..ma" 
* #2671 "Hangzhou Sejoy Electronics & Instruments Co. Ltd., SARS-CoV-2 Antigen Rapid Test Cassette"
* #2671 ^property[0].code = #hints 
* #2671 ^property[0].valueString = "T^E^B" 
* #2671 ^property[1].code = #Relationships 
* #2671 ^property[1].valueString = "$.t..ma" 
* #2672 "Pierenkemper GmbH, (SARS-CoV-2) Antigen Rapid Test COVIDENT (SWAB) COVID-19"
* #2672 ^property[0].code = #hints 
* #2672 ^property[0].valueString = "T^E^B" 
* #2672 ^property[1].code = #Relationships 
* #2672 ^property[1].valueString = "$.t..ma" 
* #2678 "NDFOS Co., Ltd., ND COVID-19 Ag Test"
* #2678 ^property[0].code = #hints 
* #2678 ^property[0].valueString = "T^E^B" 
* #2678 ^property[1].code = #Relationships 
* #2678 ^property[1].valueString = "$.t..ma" 
* #2684 "Zhejiang GENE SCIENCE Co., Ltd, Novel Coronavirus (COVID-19) Antigen Detection Kit (Swab)"
* #2684 ^property[0].code = #hints 
* #2684 ^property[0].valueString = "T^E^B" 
* #2684 ^property[1].code = #Relationships 
* #2684 ^property[1].valueString = "$.t..ma" 
* #2685 "PRIMA Lab SA, COVID-19 Antigen Rapid Test"
* #2685 ^property[0].code = #hints 
* #2685 ^property[0].valueString = "T^E^B" 
* #2685 ^property[1].code = #Relationships 
* #2685 ^property[1].valueString = "$.t..ma" 
* #2687 "Zephyr Biomedicals - Tulip Diagnostics, PerkinElmer COVID 19 Antigen Test (NS, NP)"
* #2687 ^property[0].code = #hints 
* #2687 ^property[0].valueString = "T^E^B" 
* #2687 ^property[1].code = #Relationships 
* #2687 ^property[1].valueString = "$.t..ma" 
* #2691 "CALTH Inc., AllCheck COVID19 Ag Nasal"
* #2691 ^property[0].code = #hints 
* #2691 ^property[0].valueString = "T^E^B" 
* #2691 ^property[1].code = #Relationships 
* #2691 ^property[1].valueString = "$.t..ma" 
* #2695 "Glallergen CO., LTD., Novel Coronavirus (2019-nCoV) Antigen Test Kit (Colloidal gold immunochromatography)"
* #2695 ^property[0].code = #hints 
* #2695 ^property[0].valueString = "T^E^B" 
* #2695 ^property[1].code = #Relationships 
* #2695 ^property[1].valueString = "$.t..ma" 
* #2696 "Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASAL ANTIGEN RAPID TEST"
* #2696 ^property[0].code = #hints 
* #2696 ^property[0].valueString = "T^E^B" 
* #2696 ^property[1].code = #Relationships 
* #2696 ^property[1].valueString = "$.t..ma" 
* #2724 "Fosun Diagnostics (Shanghai) Co.,Ltd., China, Fosun Covid-19 Ag Card"
* #2724 ^property[0].code = #hints 
* #2724 ^property[0].valueString = "T^E^B" 
* #2724 ^property[1].code = #Relationships 
* #2724 ^property[1].valueString = "$.t..ma" 
* #2741 "OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Plus Rapid Test"
* #2741 ^property[0].code = #hints 
* #2741 ^property[0].valueString = "T^E^B" 
* #2741 ^property[1].code = #Relationships 
* #2741 ^property[1].valueString = "$.t..ma" 
* #2742 "Wuhan HealthCare Biotechnology Co. Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2742 ^property[0].code = #hints 
* #2742 ^property[0].valueString = "T^E^B" 
* #2742 ^property[1].code = #Relationships 
* #2742 ^property[1].valueString = "$.t..ma" 
* #2746 "Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASOPHARYNGEAL RAPID ANTIGEN TEST"
* #2746 ^property[0].code = #hints 
* #2746 ^property[0].valueString = "T^E^B" 
* #2746 ^property[1].code = #Relationships 
* #2746 ^property[1].valueString = "$.t..ma" 
* #2754 "Qingdao Hightop Biotech Co., Ltd., SARS-CoV-2/Flu A+B/RSV Antigen Rapid Test"
* #2754 ^property[0].code = #hints 
* #2754 ^property[0].valueString = "T^E^B" 
* #2754 ^property[1].code = #Relationships 
* #2754 ^property[1].valueString = "$.t..ma" 
* #2756 "DNA Diagnostic, SARS-CoV-2 Antigen Rapid Test"
* #2756 ^property[0].code = #hints 
* #2756 ^property[0].valueString = "T^E^B" 
* #2756 ^property[1].code = #Relationships 
* #2756 ^property[1].valueString = "$.t..ma" 
* #2763 "ScheBo Biotech, ScheBo SARS CoV-2 Quick ANTIGEN (Colloidal Gold Method)"
* #2763 ^property[0].code = #hints 
* #2763 ^property[0].valueString = "T^E^B" 
* #2763 ^property[1].code = #Relationships 
* #2763 ^property[1].valueString = "$.t..ma" 
* #2769 "Beijing Beier Bioengineering Co., Ltd, Covid-19 Antigen Rapid Test Kit"
* #2769 ^property[0].code = #hints 
* #2769 ^property[0].valueString = "T^E^B" 
* #2769 ^property[1].code = #Relationships 
* #2769 ^property[1].valueString = "$.t..ma" 
* #2782 "Primer Design Limited, PathFlow COVID-19 Rapid Antigen PRO"
* #2782 ^property[0].code = #hints 
* #2782 ^property[0].valueString = "T^E^B" 
* #2782 ^property[1].code = #Relationships 
* #2782 ^property[1].valueString = "$.t..ma" 
* #2812 "Shenzhen Huaree Technology Co.,Ltd, SARS-CoV-2 Antigen Rapid Test Kit (Immunochromatography)"
* #2812 ^property[0].code = #hints 
* #2812 ^property[0].valueString = "T^E^B" 
* #2812 ^property[1].code = #Relationships 
* #2812 ^property[1].valueString = "$.t..ma" 
* #2853 "GenSure Biotech Inc., GenSure COVID-19 Antigen Rapid Test Kit"
* #2853 ^property[0].code = #hints 
* #2853 ^property[0].valueString = "T^E^B" 
* #2853 ^property[1].code = #Relationships 
* #2853 ^property[1].valueString = "$.t..ma" 
* #28531000087107 "COVID-19 vaccine"
* #28531000087107 ^designation[0].language = #de-AT 
* #28531000087107 ^designation[0].value = "SARS-CoV-2-Impfstoff" 
* #28531000087107 ^property[0].code = #hints 
* #28531000087107 ^property[0].valueString = "T^E^B" 
* #28531000087107 ^property[1].code = #Relationships 
* #28531000087107 ^property[1].valueString = "$.v..vp" 
* #2858 "Bioscience (Tianjin) Diagnostic Technology Co.,Ltd, Novel Coronavirus (2019-nCoV) Antigen Rapid Detection"
* #2858 ^property[0].code = #hints 
* #2858 ^property[0].valueString = "T^E^B" 
* #2858 ^property[1].code = #Relationships 
* #2858 ^property[1].valueString = "$.t..ma" 
* #2862 "Hangzhou Funworld Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test Device"
* #2862 ^property[0].code = #hints 
* #2862 ^property[0].valueString = "T^E^B" 
* #2862 ^property[1].code = #Relationships 
* #2862 ^property[1].valueString = "$.t..ma" 
* #2866 "Lifecosm Biotech Limited, COVID-19 Antigen Test Cassette"
* #2866 ^property[0].code = #hints 
* #2866 ^property[0].valueString = "T^E^B" 
* #2866 ^property[1].code = #Relationships 
* #2866 ^property[1].valueString = "$.t..ma" 
* #2885 "Hangzhou GENESIS Biodetection and Biocontrol CO.,LTD, KaiBiLi COVID-19 Antigen Pro"
* #2885 ^property[0].code = #hints 
* #2885 ^property[0].valueString = "T^E^B" 
* #2885 ^property[1].code = #Relationships 
* #2885 ^property[1].valueString = "$.t..ma" 
* #2935 "SG Medical, Inc, InstaView COVID-19 Antigen"
* #2935 ^property[0].code = #hints 
* #2935 ^property[0].valueString = "T^E^B" 
* #2935 ^property[1].code = #Relationships 
* #2935 ^property[1].valueString = "$.t..ma" 
* #2936 "Shenzhen AMPER Biotechnology Co Ltd, AMPER COVID-19 Antigen Rapid Testing Kit (Colloidal Gold)"
* #2936 ^property[0].code = #hints 
* #2936 ^property[0].valueString = "T^E^B" 
* #2936 ^property[1].code = #Relationships 
* #2936 ^property[1].valueString = "$.t..ma" 
* #2941 "Shenzhen Kingfocus Biomedical Engineering Co., Ltd., COVID-19 Antigen Detection Kit (Quantum Dots-Based Immunofluorescence Chromatography)"
* #2941 ^property[0].code = #hints 
* #2941 ^property[0].valueString = "T^E^B" 
* #2941 ^property[1].code = #Relationships 
* #2941 ^property[1].valueString = "$.t..ma" 
* #2942 "Hangzhou Zheda Dixun Biological Gene Engineering Co., Ltd., SARS-CoV-2 Nucleocapsid (N) Antigen Rapid Test Cassette (Swab)"
* #2942 ^property[0].code = #hints 
* #2942 ^property[0].valueString = "T^E^B" 
* #2942 ^property[1].code = #Relationships 
* #2942 ^property[1].valueString = "$.t..ma" 
* #2963 "Jiangxi Province JinHuan Medical Instrument Co., LTD., DREHA Novel Coronavirus (SARS-CoV-2) Antigen Rapid Detection Kit"
* #2963 ^property[0].code = #hints 
* #2963 ^property[0].valueString = "T^E^B" 
* #2963 ^property[1].code = #Relationships 
* #2963 ^property[1].valueString = "$.t..ma" 
* #2973 "Medifood Hungary Innovation Kft., MediDia COVID-19 Ag"
* #2973 ^property[0].code = #hints 
* #2973 ^property[0].valueString = "T^E^B" 
* #2973 ^property[1].code = #Relationships 
* #2973 ^property[1].valueString = "$.t..ma" 
* #2975 "Abionic SA, IVD CAPSULE COVID-19-NP"
* #2975 ^property[0].code = #hints 
* #2975 ^property[0].valueString = "T^E^B" 
* #2975 ^property[1].code = #Relationships 
* #2975 ^property[1].valueString = "$.t..ma" 
* #2979 "Hangzhou Jucheng Medical Products Co., Ltd, SARS-CoV-2 Ag Rapid Test Kit"
* #2979 ^property[0].code = #hints 
* #2979 ^property[0].valueString = "T^E^B" 
* #2979 ^property[1].code = #Relationships 
* #2979 ^property[1].valueString = "$.t..ma" 
* #3004 "Jiangxi Province JinHuan Medical Instrument Co., LTD., MEDSYS Novel Coronavirus (SARS-CoV-2) Antigen Rapid Detection Kit"
* #3004 ^property[0].code = #hints 
* #3004 ^property[0].valueString = "T^E^B" 
* #3004 ^property[1].code = #Relationships 
* #3004 ^property[1].valueString = "$.t..ma" 
* #3005 "Hangzhou Aichek Medical Technology Co., Ltd., COVID-19 Antigen Rapid Test Device"
* #3005 ^property[0].code = #hints 
* #3005 ^property[0].valueString = "T^E^B" 
* #3005 ^property[1].code = #Relationships 
* #3005 ^property[1].valueString = "$.t..ma" 
* #3015 "Suzhou Soochow University Saier Immuno Biotech Co., Ltd., InstantSure Covid-19 Ag CARD"
* #3015 ^property[0].code = #hints 
* #3015 ^property[0].valueString = "T^E^B" 
* #3015 ^property[1].code = #Relationships 
* #3015 ^property[1].valueString = "$.t..ma" 
* #3026 "SD BIOSENSOR Healthcare Private Limited, Ultra COVID-19 Ag"
* #3026 ^property[0].code = #hints 
* #3026 ^property[0].valueString = "T^E^B" 
* #3026 ^property[1].code = #Relationships 
* #3026 ^property[1].valueString = "$.t..ma" 
* #3037 "AAZ-LMB, COVID-VIRO ALL IN"
* #3037 ^property[0].code = #hints 
* #3037 ^property[0].valueString = "T^E^B" 
* #3037 ^property[1].code = #Relationships 
* #3037 ^property[1].valueString = "$.t..ma" 
* #3084 "InTec Products Inc., AQ+ COVID-19 Ag Rapid Test"
* #3084 ^property[0].code = #hints 
* #3084 ^property[0].valueString = "T^E^B" 
* #3084 ^property[1].code = #Relationships 
* #3084 ^property[1].valueString = "$.t..ma" 
* #3093 "TBG BIOTECHNOLOGY XIAMEN INC., SARS-CoV-2 Antigen Rapid Test (Nasal Swab)"
* #3093 ^property[0].code = #hints 
* #3093 ^property[0].valueString = "T^E^B" 
* #3093 ^property[1].code = #Relationships 
* #3093 ^property[1].valueString = "$.t..ma" 
* #3096 "Rapid Labs Ltd., SARS-CoV-2 Antigen Rapid Test (Nasopharyngeal Swab)"
* #3096 ^property[0].code = #hints 
* #3096 ^property[0].valueString = "T^E^B" 
* #3096 ^property[1].code = #Relationships 
* #3096 ^property[1].valueString = "$.t..ma" 
* #3099 "BioDetect (Xiamen) Biotechnology Co., Ltd., RAPID SARS-COV-2 ANTIGEN TEST CARD"
* #3099 ^property[0].code = #hints 
* #3099 ^property[0].valueString = "T^E^B" 
* #3099 ^property[1].code = #Relationships 
* #3099 ^property[1].valueString = "$.t..ma" 
* #3107 "Nanjing Vazyme Medical Technology Co., Ltd., Severe Acute Respiratory Syndrome Coronavirus 2 (SARS-CoV-2) Antigen Detection Kit (Colloidal Gold-Based) – Nasal swab"
* #3107 ^property[0].code = #hints 
* #3107 ^property[0].valueString = "T^E^B" 
* #3107 ^property[1].code = #Relationships 
* #3107 ^property[1].valueString = "$.t..ma" 
* #3143 "VivaChek Biotech (Hangzhou) Co., Ltd; Rapid Gold Pro SARS-CoV-2 AG Test"
* #3143 ^property[0].code = #hints 
* #3143 ^property[0].valueString = "T^E^B" 
* #3143 ^property[1].code = #Relationships 
* #3143 ^property[1].valueString = "$.t..ma" 
* #3153 "Huachenyang (Shenzhen) Technology Co., Ltd., COVID-19 Ag Rapid Test Kit"
* #3153 ^property[0].code = #hints 
* #3153 ^property[0].valueString = "T^E^B" 
* #3153 ^property[1].code = #Relationships 
* #3153 ^property[1].valueString = "$.t..ma" 
* #3190 "Green Cross Medical Science Corp., GENEDIA W COVID-19 Ag 643K"
* #3190 ^property[0].code = #hints 
* #3190 ^property[0].valueString = "T^E^B" 
* #3190 ^property[1].code = #Relationships 
* #3190 ^property[1].valueString = "$.t..ma" 
* #344 "SD BIOSENSOR Inc., STANDARD F COVID-19 Ag FIA"
* #344 ^property[0].code = #hints 
* #344 ^property[0].valueString = "T^E^B" 
* #344 ^property[1].code = #Relationships 
* #344 ^property[1].valueString = "$.t..ma" 
* #345 "SD BIOSENSOR Inc., STANDARD Q COVID-19 Ag Test"
* #345 ^property[0].code = #hints 
* #345 ^property[0].valueString = "T^E^B" 
* #345 ^property[1].code = #Relationships 
* #345 ^property[1].valueString = "$.t..ma" 
* #3937 "Cofoe Medical Technology Co., Ltd., SARS-CoV-2 Antigen Test Kit(Colloidal Gold Method)"
* #3937 ^property[0].code = #hints 
* #3937 ^property[0].valueString = "T^E^B" 
* #3937 ^property[1].code = #Relationships 
* #3937 ^property[1].valueString = "$.t..ma" 
* #3966 "Chastru Biotech Limited, COVID-19 Antigen Rapid Test Cassette"
* #3966 ^property[0].code = #hints 
* #3966 ^property[0].valueString = "T^E^B" 
* #3966 ^property[1].code = #Relationships 
* #3966 ^property[1].valueString = "$.t..ma" 
* #4049 "NINGBO LVTANG BIOTECHNOLOGY Co. Ltd., SARS-CoV-2 Antigen Detection Kit (Colloidal Gold Method)"
* #4049 ^property[0].code = #hints 
* #4049 ^property[0].valueString = "T^E^B" 
* #4049 ^property[1].code = #Relationships 
* #4049 ^property[1].valueString = "$.t..ma" 
* #4053 "Acon Biotech (Hangzhou) Co., Ltd, Flowflex SARS-CoV-2 Antigen Rapid Test Slim"
* #4053 ^property[0].code = #hints 
* #4053 ^property[0].valueString = "T^E^B" 
* #4053 ^property[1].code = #Relationships 
* #4053 ^property[1].valueString = "$.t..ma" 
* #4057 "Acon Biotech (Hangzhou) Co., Ltd, Flowflex SARS-CoV-2 Antigen Rapid Test (Nasal/ Nasopharyngeal/ Saliva)"
* #4057 ^property[0].code = #hints 
* #4057 ^property[0].valueString = "T^E^B" 
* #4057 ^property[1].code = #Relationships 
* #4057 ^property[1].valueString = "$.t..ma" 
* #4058 "Vitrosens Biotechnology Co., Ltd, RAPIDNEXT SARS-CoV-2 Rapid Antigen Test Kit"
* #4058 ^property[0].code = #hints 
* #4058 ^property[0].valueString = "T^E^B" 
* #4058 ^property[1].code = #Relationships 
* #4058 ^property[1].valueString = "$.t..ma" 
* #4059 "Vitrosens Biotechnology Co., Ltd, RapidSens SARS-CoV-2 Rapid Antigen Test Kit"
* #4059 ^property[0].code = #hints 
* #4059 ^property[0].valueString = "T^E^B" 
* #4059 ^property[1].code = #Relationships 
* #4059 ^property[1].valueString = "$.t..ma" 
* #4064 "Glallergen Co. Ltd, Novel Coronavirus (2019-nCoV) Antigen Test Kit (Latex Method)"
* #4064 ^property[0].code = #hints 
* #4064 ^property[0].valueString = "T^E^B" 
* #4064 ^property[1].code = #Relationships 
* #4064 ^property[1].valueString = "$.t..ma" 
* #4075 "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device"
* #4075 ^property[0].code = #hints 
* #4075 ^property[0].valueString = "T^E^B" 
* #4075 ^property[1].code = #Relationships 
* #4075 ^property[1].valueString = "$.t..ma" 
* #4081 "General Biologicals Corporation, GB COVID-19 Ag POCT"
* #4081 ^property[0].code = #hints 
* #4081 ^property[0].valueString = "T^E^B" 
* #4081 ^property[1].code = #Relationships 
* #4081 ^property[1].valueString = "$.t..ma" 
* #4083 "Vitrosens Biotechnology Co., Ltd, ENMED SARS-CoV-2 Rapid Antigen Test Kit"
* #4083 ^property[0].code = #hints 
* #4083 ^property[0].valueString = "T^E^B" 
* #4083 ^property[1].code = #Relationships 
* #4083 ^property[1].valueString = "$.t..ma" 
* #4111 "Wuhan Uni-science Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit"
* #4111 ^property[0].code = #hints 
* #4111 ^property[0].valueString = "T^E^B" 
* #4111 ^property[1].code = #Relationships 
* #4111 ^property[1].valueString = "$.t..ma" 
* #5743 "Hangzhou Biotest Biotech Co., Ltd., COVID-19 Antigen Rapid Test Cassette (Nasal Swab)"
* #5743 ^property[0].code = #hints 
* #5743 ^property[0].valueString = "T^E^B" 
* #5743 ^property[1].code = #Relationships 
* #5743 ^property[1].valueString = "$.t..ma" 
* #5751 "Shenzhen Microprofit Biotech Co., Ltd., SARS-CoV-2 & Influenza A/B Antigen Combo Test Kit (Colloidal Gold Chromatographic Immunoassay)"
* #5751 ^property[0].code = #hints 
* #5751 ^property[0].valueString = "T^E^B" 
* #5751 ^property[1].code = #Relationships 
* #5751 ^property[1].valueString = "$.t..ma" 
* #5812 "Rapid Labs Ltd., SARS-CoV-2 Antigen Rapid Test (Nasal Swab)"
* #5812 ^property[0].code = #hints 
* #5812 ^property[0].valueString = "T^E^B" 
* #5812 ^property[1].code = #Relationships 
* #5812 ^property[1].valueString = "$.t..ma" 
* #768 "ArcDia International Ltd, mariPOC SARS-CoV-2"
* #768 ^property[0].code = #hints 
* #768 ^property[0].valueString = "T^E^B" 
* #768 ^property[1].code = #Relationships 
* #768 ^property[1].valueString = "$.t..ma" 
* #770 "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device"
* #770 ^property[0].code = #hints 
* #770 ^property[0].valueString = "T^E^B" 
* #770 ^property[1].code = #Relationships 
* #770 ^property[1].valueString = "$.t..ma" 
* #82334004 "Indeterminate"
* #82334004 ^designation[0].language = #de-AT 
* #82334004 ^designation[0].value = "nicht bestimmbar" 
* #82334004 ^property[0].code = #Relationships 
* #82334004 ^property[0].valueString = "$.t..tr" 
* #840539006 "SARSCOV2"
* #840539006 ^designation[0].language = #de-AT 
* #840539006 ^designation[0].value = "COVID-19" 
* #840539006 ^property[0].code = #hints 
* #840539006 ^property[0].valueString = "T^E^B" 
* #840539006 ^property[1].code = #Relationships 
* #840539006 ^property[1].valueString = "$.tg" 
* #AC "Ascension Island"
* #AC ^designation[0].language = #de-AT 
* #AC ^designation[0].value = "Ascension (verwaltet von St. Helena, reserviert für UPU und ITU)" 
* #AC ^property[0].code = #hints 
* #AC ^property[0].valueString = "T^E^B" 
* #AC ^property[1].code = #Relationships 
* #AC ^property[1].valueString = "$.co" 
* #AD "Andorra"
* #AD ^property[0].code = #hints 
* #AD ^property[0].valueString = "T^E^B" 
* #AD ^property[1].code = #Relationships 
* #AD ^property[1].valueString = "$.co" 
* #AE "United Arab Emirates (the)"
* #AE ^designation[0].language = #de-AT 
* #AE ^designation[0].value = "Vereinigte Arab.Emirate" 
* #AE ^property[0].code = #hints 
* #AE ^property[0].valueString = "T^E^B" 
* #AE ^property[1].code = #Relationships 
* #AE ^property[1].valueString = "$.co" 
* #AF "Afghanistan"
* #AF ^property[0].code = #hints 
* #AF ^property[0].valueString = "T^E^B" 
* #AF ^property[1].code = #Relationships 
* #AF ^property[1].valueString = "$.co" 
* #AG "Antigua and Barbuda"
* #AG ^designation[0].language = #de-AT 
* #AG ^designation[0].value = "Antigua und Barbuda" 
* #AG ^property[0].code = #hints 
* #AG ^property[0].valueString = "T^E^B" 
* #AG ^property[1].code = #Relationships 
* #AG ^property[1].valueString = "$.co" 
* #AI "Anguilla"
* #AI ^property[0].code = #hints 
* #AI ^property[0].valueString = "T^E^B" 
* #AI ^property[1].code = #Relationships 
* #AI ^property[1].valueString = "$.co" 
* #AL "Albania"
* #AL ^designation[0].language = #de-AT 
* #AL ^designation[0].value = "Albanien" 
* #AL ^property[0].code = #hints 
* #AL ^property[0].valueString = "T^E^B" 
* #AL ^property[1].code = #Relationships 
* #AL ^property[1].valueString = "$.co" 
* #AM "Armenia"
* #AM ^designation[0].language = #de-AT 
* #AM ^designation[0].value = "Armenien" 
* #AM ^property[0].code = #hints 
* #AM ^property[0].valueString = "T^E^B" 
* #AM ^property[1].code = #Relationships 
* #AM ^property[1].valueString = "$.co" 
* #AN "Netherlands Antilles"
* #AN ^designation[0].language = #de-AT 
* #AN ^designation[0].value = "Niedl.Antillen" 
* #AN ^property[0].code = #hints 
* #AN ^property[0].valueString = "T^E^B" 
* #AN ^property[1].code = #Relationships 
* #AN ^property[1].valueString = "$.co" 
* #AO "Angola"
* #AO ^property[0].code = #hints 
* #AO ^property[0].valueString = "T^E^B" 
* #AO ^property[1].code = #Relationships 
* #AO ^property[1].valueString = "$.co" 
* #AQ "Antarctica"
* #AQ ^designation[0].language = #de-AT 
* #AQ ^designation[0].value = "Antarktis" 
* #AQ ^property[0].code = #hints 
* #AQ ^property[0].valueString = "T^E^B" 
* #AQ ^property[1].code = #Relationships 
* #AQ ^property[1].valueString = "$.co" 
* #AR "Argentina"
* #AR ^designation[0].language = #de-AT 
* #AR ^designation[0].value = "Argentinien" 
* #AR ^property[0].code = #hints 
* #AR ^property[0].valueString = "T^E^B" 
* #AR ^property[1].code = #Relationships 
* #AR ^property[1].valueString = "$.co" 
* #AS "American Samoa"
* #AS ^designation[0].language = #de-AT 
* #AS ^designation[0].value = "Amerikan.Samoa" 
* #AS ^property[0].code = #hints 
* #AS ^property[0].valueString = "T^E^B" 
* #AS ^property[1].code = #Relationships 
* #AS ^property[1].valueString = "$.co" 
* #AT "Austria"
* #AT ^designation[0].language = #de-AT 
* #AT ^designation[0].value = "Österreich" 
* #AT ^property[0].code = #hints 
* #AT ^property[0].valueString = "T^E^B" 
* #AT ^property[1].code = #Relationships 
* #AT ^property[1].valueString = "$.co" 
* #AU "Australia"
* #AU ^designation[0].language = #de-AT 
* #AU ^designation[0].value = "Australien" 
* #AU ^property[0].code = #hints 
* #AU ^property[0].valueString = "T^E^B" 
* #AU ^property[1].code = #Relationships 
* #AU ^property[1].valueString = "$.co" 
* #AW "Aruba"
* #AW ^property[0].code = #hints 
* #AW ^property[0].valueString = "T^E^B" 
* #AW ^property[1].code = #Relationships 
* #AW ^property[1].valueString = "$.co" 
* #AX "Åland Islands"
* #AX ^designation[0].language = #de-AT 
* #AX ^designation[0].value = "Alandinseln" 
* #AX ^property[0].code = #hints 
* #AX ^property[0].valueString = "T^E^B" 
* #AX ^property[1].code = #Relationships 
* #AX ^property[1].valueString = "$.co" 
* #AZ "Azerbaijan"
* #AZ ^designation[0].language = #de-AT 
* #AZ ^designation[0].value = "Aserbaidschan" 
* #AZ ^property[0].code = #hints 
* #AZ ^property[0].valueString = "T^E^B" 
* #AZ ^property[1].code = #Relationships 
* #AZ ^property[1].valueString = "$.co" 
* #AZD2816 "AZD2816"
* #AZD2816 ^designation[0].language = #de-AT 
* #AZD2816 ^designation[0].value = "COVID-19-Impfstoff AZD2816 (AstraZeneca AB)" 
* #AZD2816 ^property[0].code = #hints 
* #AZD2816 ^property[0].valueString = "T^E^B" 
* #AZD2816 ^property[1].code = #Relationships 
* #AZD2816 ^property[1].valueString = "$.v..mp" 
* #Abdala "Abdala"
* #Abdala ^designation[0].language = #de-AT 
* #Abdala ^designation[0].value = "COVID-19-Impfstoff Abdala (Center for Genetic Engineering and Biotechnology)" 
* #Abdala ^property[0].code = #hints 
* #Abdala ^property[0].valueString = "T^E^B" 
* #Abdala ^property[1].code = #Relationships 
* #Abdala ^property[1].valueString = "$.v..mp" 
* #BA "Bosnia and Herzegovina"
* #BA ^designation[0].language = #de-AT 
* #BA ^designation[0].value = "Bosnien und Herzegowina" 
* #BA ^property[0].code = #hints 
* #BA ^property[0].valueString = "T^E^B" 
* #BA ^property[1].code = #Relationships 
* #BA ^property[1].valueString = "$.co" 
* #BB "Barbados"
* #BB ^property[0].code = #hints 
* #BB ^property[0].valueString = "T^E^B" 
* #BB ^property[1].code = #Relationships 
* #BB ^property[1].valueString = "$.co" 
* #BBIBP-CorV "BBIBP-CorV"
* #BBIBP-CorV ^designation[0].language = #de-AT 
* #BBIBP-CorV ^designation[0].value = "COVID-19-Impfstoff BBIBP-CorV (Sinopharm)" 
* #BBIBP-CorV ^property[0].code = #hints 
* #BBIBP-CorV ^property[0].valueString = "T^E^B" 
* #BBIBP-CorV ^property[1].code = #Relationships 
* #BBIBP-CorV ^property[1].valueString = "$.v..mp" 
* #BD "Bangladesh"
* #BD ^designation[0].language = #de-AT 
* #BD ^designation[0].value = "Bangladesch" 
* #BD ^property[0].code = #hints 
* #BD ^property[0].valueString = "T^E^B" 
* #BD ^property[1].code = #Relationships 
* #BD ^property[1].valueString = "$.co" 
* #BE "Belgium"
* #BE ^designation[0].language = #de-AT 
* #BE ^designation[0].value = "Belgien" 
* #BE ^property[0].code = #hints 
* #BE ^property[0].valueString = "T^E^B" 
* #BE ^property[1].code = #Relationships 
* #BE ^property[1].valueString = "$.co" 
* #BF "Burkina Faso"
* #BF ^property[0].code = #hints 
* #BF ^property[0].valueString = "T^E^B" 
* #BF ^property[1].code = #Relationships 
* #BF ^property[1].valueString = "$.co" 
* #BG "Bulgaria"
* #BG ^designation[0].language = #de-AT 
* #BG ^designation[0].value = "Bulgarien" 
* #BG ^property[0].code = #hints 
* #BG ^property[0].valueString = "T^E^B" 
* #BG ^property[1].code = #Relationships 
* #BG ^property[1].valueString = "$.co" 
* #BH "Bahrain"
* #BH ^property[0].code = #hints 
* #BH ^property[0].valueString = "T^E^B" 
* #BH ^property[1].code = #Relationships 
* #BH ^property[1].valueString = "$.co" 
* #BI "Burundi"
* #BI ^property[0].code = #hints 
* #BI ^property[0].valueString = "T^E^B" 
* #BI ^property[1].code = #Relationships 
* #BI ^property[1].valueString = "$.co" 
* #BJ "Benin"
* #BJ ^property[0].code = #hints 
* #BJ ^property[0].valueString = "T^E^B" 
* #BJ ^property[1].code = #Relationships 
* #BJ ^property[1].valueString = "$.co" 
* #BL "Saint Barthélemy"
* #BL ^designation[0].language = #de-AT 
* #BL ^designation[0].value = "Saint-Barthélemy" 
* #BL ^property[0].code = #hints 
* #BL ^property[0].valueString = "T^E^B" 
* #BL ^property[1].code = #Relationships 
* #BL ^property[1].valueString = "$.co" 
* #BM "Bermuda"
* #BM ^property[0].code = #hints 
* #BM ^property[0].valueString = "T^E^B" 
* #BM ^property[1].code = #Relationships 
* #BM ^property[1].valueString = "$.co" 
* #BN "Brunei Darussalam"
* #BN ^property[0].code = #hints 
* #BN ^property[0].valueString = "T^E^B" 
* #BN ^property[1].code = #Relationships 
* #BN ^property[1].valueString = "$.co" 
* #BO "Bolivia (Plurinational State of)"
* #BO ^designation[0].language = #de-AT 
* #BO ^designation[0].value = "Bolivien" 
* #BO ^property[0].code = #hints 
* #BO ^property[0].valueString = "T^E^B" 
* #BO ^property[1].code = #Relationships 
* #BO ^property[1].valueString = "$.co" 
* #BQ "Bonaire, Sint Eustatius and Saba"
* #BQ ^designation[0].language = #de-AT 
* #BQ ^designation[0].value = "Bonaire,  Saba,  Sint Eustatius" 
* #BQ ^property[0].code = #hints 
* #BQ ^property[0].valueString = "T^E^B" 
* #BQ ^property[1].code = #Relationships 
* #BQ ^property[1].valueString = "$.co" 
* #BR "Brazil"
* #BR ^designation[0].language = #de-AT 
* #BR ^designation[0].value = "Brasilien" 
* #BR ^property[0].code = #hints 
* #BR ^property[0].valueString = "T^E^B" 
* #BR ^property[1].code = #Relationships 
* #BR ^property[1].valueString = "$.co" 
* #BS "Bahamas (the)"
* #BS ^designation[0].language = #de-AT 
* #BS ^designation[0].value = "Bahamas" 
* #BS ^property[0].code = #hints 
* #BS ^property[0].valueString = "T^E^B" 
* #BS ^property[1].code = #Relationships 
* #BS ^property[1].valueString = "$.co" 
* #BT "Bhutan"
* #BT ^property[0].code = #hints 
* #BT ^property[0].valueString = "T^E^B" 
* #BT ^property[1].code = #Relationships 
* #BT ^property[1].valueString = "$.co" 
* #BV "Bouvet Island"
* #BV ^designation[0].language = #de-AT 
* #BV ^designation[0].value = "Bouvetinsel" 
* #BV ^property[0].code = #hints 
* #BV ^property[0].valueString = "T^E^B" 
* #BV ^property[1].code = #Relationships 
* #BV ^property[1].valueString = "$.co" 
* #BW "Botswana"
* #BW ^designation[0].language = #de-AT 
* #BW ^designation[0].value = "Botsuana" 
* #BW ^property[0].code = #hints 
* #BW ^property[0].valueString = "T^E^B" 
* #BW ^property[1].code = #Relationships 
* #BW ^property[1].valueString = "$.co" 
* #BY "Belarus"
* #BY ^property[0].code = #hints 
* #BY ^property[0].valueString = "T^E^B" 
* #BY ^property[1].code = #Relationships 
* #BY ^property[1].valueString = "$.co" 
* #BZ "Belize"
* #BZ ^property[0].code = #hints 
* #BZ ^property[0].valueString = "T^E^B" 
* #BZ ^property[1].code = #Relationships 
* #BZ ^property[1].valueString = "$.co" 
* #Bharat-Biotech "Bharat Biotech"
* #Bharat-Biotech ^property[0].code = #hints 
* #Bharat-Biotech ^property[0].valueString = "T^E^B" 
* #Bharat-Biotech ^property[1].code = #Relationships 
* #Bharat-Biotech ^property[1].valueString = "$.v..ma" 
* #CA "Canada"
* #CA ^designation[0].language = #de-AT 
* #CA ^designation[0].value = "Kanada" 
* #CA ^property[0].code = #hints 
* #CA ^property[0].valueString = "T^E^B" 
* #CA ^property[1].code = #Relationships 
* #CA ^property[1].valueString = "$.co" 
* #CC "Cocos (Keeling) Islands (the)"
* #CC ^designation[0].language = #de-AT 
* #CC ^designation[0].value = "Kokosinseln(Au)" 
* #CC ^property[0].code = #hints 
* #CC ^property[0].valueString = "T^E^B" 
* #CC ^property[1].code = #Relationships 
* #CC ^property[1].valueString = "$.co" 
* #CD "Congo (the Democratic Republic of the)"
* #CD ^designation[0].language = #de-AT 
* #CD ^designation[0].value = "Kongo,Dem.Rep.(Kinshasa)" 
* #CD ^property[0].code = #hints 
* #CD ^property[0].valueString = "T^E^B" 
* #CD ^property[1].code = #Relationships 
* #CD ^property[1].valueString = "$.co" 
* #CF "Central African Republic (the)"
* #CF ^designation[0].language = #de-AT 
* #CF ^designation[0].value = "Zentralafrikan.Republik" 
* #CF ^property[0].code = #hints 
* #CF ^property[0].valueString = "T^E^B" 
* #CF ^property[1].code = #Relationships 
* #CF ^property[1].valueString = "$.co" 
* #CG "Congo (the)"
* #CG ^designation[0].language = #de-AT 
* #CG ^designation[0].value = "Kongo,Rep. (Brazzaville)" 
* #CG ^property[0].code = #hints 
* #CG ^property[0].valueString = "T^E^B" 
* #CG ^property[1].code = #Relationships 
* #CG ^property[1].valueString = "$.co" 
* #CH "Switzerland"
* #CH ^designation[0].language = #de-AT 
* #CH ^designation[0].value = "Schweiz" 
* #CH ^property[0].code = #hints 
* #CH ^property[0].valueString = "T^E^B" 
* #CH ^property[1].code = #Relationships 
* #CH ^property[1].valueString = "$.co" 
* #CI "Côte d'Ivoire"
* #CI ^designation[0].language = #de-AT 
* #CI ^designation[0].value = "Cote dIvoire" 
* #CI ^property[0].code = #hints 
* #CI ^property[0].valueString = "T^E^B" 
* #CI ^property[1].code = #Relationships 
* #CI ^property[1].valueString = "$.co" 
* #CIGB "Center for Genetic Engineering and Biotechnology (CIGB)"
* #CIGB ^property[0].code = #hints 
* #CIGB ^property[0].valueString = "T^E^B" 
* #CIGB ^property[1].code = #Relationships 
* #CIGB ^property[1].valueString = "$.v..ma" 
* #CK "Cook Islands (the)"
* #CK ^designation[0].language = #de-AT 
* #CK ^designation[0].value = "Cookinseln" 
* #CK ^property[0].code = #hints 
* #CK ^property[0].valueString = "T^E^B" 
* #CK ^property[1].code = #Relationships 
* #CK ^property[1].valueString = "$.co" 
* #CL "Chile"
* #CL ^property[0].code = #hints 
* #CL ^property[0].valueString = "T^E^B" 
* #CL ^property[1].code = #Relationships 
* #CL ^property[1].valueString = "$.co" 
* #CM "Cameroon"
* #CM ^designation[0].language = #de-AT 
* #CM ^designation[0].value = "Kamerun" 
* #CM ^property[0].code = #hints 
* #CM ^property[0].valueString = "T^E^B" 
* #CM ^property[1].code = #Relationships 
* #CM ^property[1].valueString = "$.co" 
* #CN "China"
* #CN ^property[0].code = #hints 
* #CN ^property[0].valueString = "T^E^B" 
* #CN ^property[1].code = #Relationships 
* #CN ^property[1].valueString = "$.co" 
* #CO "Colombia"
* #CO ^designation[0].language = #de-AT 
* #CO ^designation[0].value = "Kolumbien" 
* #CO ^property[0].code = #hints 
* #CO ^property[0].valueString = "T^E^B" 
* #CO ^property[1].code = #Relationships 
* #CO ^property[1].valueString = "$.co" 
* #CR "Costa Rica"
* #CR ^property[0].code = #hints 
* #CR ^property[0].valueString = "T^E^B" 
* #CR ^property[1].code = #Relationships 
* #CR ^property[1].valueString = "$.co" 
* #CU "Cuba"
* #CU ^designation[0].language = #de-AT 
* #CU ^designation[0].value = "Kuba" 
* #CU ^property[0].code = #hints 
* #CU ^property[0].valueString = "T^E^B" 
* #CU ^property[1].code = #Relationships 
* #CU ^property[1].valueString = "$.co" 
* #CV "Cabo Verde"
* #CV ^designation[0].language = #de-AT 
* #CV ^designation[0].value = "Kap Verde" 
* #CV ^property[0].code = #hints 
* #CV ^property[0].valueString = "T^E^B" 
* #CV ^property[1].code = #Relationships 
* #CV ^property[1].valueString = "$.co" 
* #CVnCoV "CVnCoV"
* #CVnCoV ^designation[0].language = #de-AT 
* #CVnCoV ^designation[0].value = "COVID-19-Impfstoff CVnCoV (CureVac)" 
* #CVnCoV ^property[0].code = #hints 
* #CVnCoV ^property[0].valueString = "T^E^B" 
* #CVnCoV ^property[1].code = #Relationships 
* #CVnCoV ^property[1].valueString = "$.v..mp" 
* #CX "Christmas Island"
* #CX ^designation[0].language = #de-AT 
* #CX ^designation[0].value = "Weihnachtsinsel" 
* #CX ^property[0].code = #hints 
* #CX ^property[0].valueString = "T^E^B" 
* #CX ^property[1].code = #Relationships 
* #CX ^property[1].valueString = "$.co" 
* #CY "Cyprus"
* #CY ^designation[0].language = #de-AT 
* #CY ^designation[0].value = "Zypern" 
* #CY ^property[0].code = #hints 
* #CY ^property[0].valueString = "T^E^B" 
* #CY ^property[1].code = #Relationships 
* #CY ^property[1].valueString = "$.co" 
* #CZ "Czechia"
* #CZ ^designation[0].language = #de-AT 
* #CZ ^designation[0].value = "Tschechische Republik" 
* #CZ ^property[0].code = #hints 
* #CZ ^property[0].valueString = "T^E^B" 
* #CZ ^property[1].code = #Relationships 
* #CZ ^property[1].valueString = "$.co" 
* #Chumakov-Federal-Scientific-Center "Chumakov Federal Scientific Center for Research and Development of Immune-and-Biological Products"
* #Chumakov-Federal-Scientific-Center ^property[0].code = #hints 
* #Chumakov-Federal-Scientific-Center ^property[0].valueString = "T^E^B" 
* #Chumakov-Federal-Scientific-Center ^property[1].code = #Relationships 
* #Chumakov-Federal-Scientific-Center ^property[1].valueString = "$.v..ma" 
* #Convidecia "Convidecia"
* #Convidecia ^designation[0].language = #de-AT 
* #Convidecia ^designation[0].value = "COVID-19-Impfstoff Convidecia (CanSino Biologics)" 
* #Convidecia ^property[0].code = #hints 
* #Convidecia ^property[0].valueString = "T^E^B" 
* #Convidecia ^property[1].code = #Relationships 
* #Convidecia ^property[1].valueString = "$.v..mp" 
* #CoronaVac "CoronaVac"
* #CoronaVac ^designation[0].language = #de-AT 
* #CoronaVac ^designation[0].value = "COVID-19-Impfstoff CoronaVac (Sinovac Biotech)" 
* #CoronaVac ^property[0].code = #hints 
* #CoronaVac ^property[0].valueString = "T^E^B" 
* #CoronaVac ^property[1].code = #Relationships 
* #CoronaVac ^property[1].valueString = "$.v..mp" 
* #Covaxin "Covaxin (also known as BBV152 A, B, C)"
* #Covaxin ^designation[0].language = #de-AT 
* #Covaxin ^designation[0].value = "COVID-19-Impfstoff Covaxin (Bharat Biotech)" 
* #Covaxin ^property[0].code = #hints 
* #Covaxin ^property[0].valueString = "T^E^B" 
* #Covaxin ^property[1].code = #Relationships 
* #Covaxin ^property[1].valueString = "$.v..mp" 
* #CoviVac "CoviVac"
* #CoviVac ^designation[0].language = #de-AT 
* #CoviVac ^designation[0].value = "COVID-19-Impfstoff CoviVac (Chumakov-Federal-Scientific-Center)" 
* #CoviVac ^property[0].code = #hints 
* #CoviVac ^property[0].valueString = "T^E^B" 
* #CoviVac ^property[1].code = #Relationships 
* #CoviVac ^property[1].valueString = "$.v..mp" 
* #Covid-19-adsorvida-inativada "Vacina adsorvida covid-19 (inativada)"
* #Covid-19-adsorvida-inativada ^designation[0].language = #de-AT 
* #Covid-19-adsorvida-inativada ^designation[0].value = "COVID-19-Impfstoff Vacina adsorvida covid-19 (inativada) (Instituto Butantan)" 
* #Covid-19-adsorvida-inativada ^property[0].code = #hints 
* #Covid-19-adsorvida-inativada ^property[0].valueString = "T^E^B" 
* #Covid-19-adsorvida-inativada ^property[1].code = #Relationships 
* #Covid-19-adsorvida-inativada ^property[1].valueString = "$.v..mp" 
* #Covid-19-recombinant "Covid-19 (recombinant)"
* #Covid-19-recombinant ^designation[0].language = #de-AT 
* #Covid-19-recombinant ^designation[0].value = "COVID-19-Impfstoff Covid-19 (recombinant) (Fiocruz)" 
* #Covid-19-recombinant ^property[0].code = #hints 
* #Covid-19-recombinant ^property[0].valueString = "T^E^B" 
* #Covid-19-recombinant ^property[1].code = #Relationships 
* #Covid-19-recombinant ^property[1].valueString = "$.v..mp" 
* #Covifenz "Covifenz"
* #Covifenz ^designation[0].language = #de-AT 
* #Covifenz ^designation[0].value = "COVID-19-Impfstoff Covifenz (Medicago Inc.)" 
* #Covifenz ^property[0].code = #hints 
* #Covifenz ^property[0].valueString = "T^E^B" 
* #Covifenz ^property[1].code = #Relationships 
* #Covifenz ^property[1].valueString = "$.v..mp" 
* #Covishield "Covishield (ChAdOx1_nCoV-19)"
* #Covishield ^designation[0].language = #de-AT 
* #Covishield ^designation[0].value = "COVID-19-Impfstoff Covishield (Serum Institute of India)" 
* #Covishield ^property[0].code = #hints 
* #Covishield ^property[0].valueString = "T^E^B" 
* #Covishield ^property[1].code = #Relationships 
* #Covishield ^property[1].valueString = "$.v..mp" 
* #Covovax "Covovax"
* #Covovax ^designation[0].language = #de-AT 
* #Covovax ^designation[0].value = "COVID-19-Impfstoff Covovax (Serum Institute Of India Private Limited)" 
* #Covovax ^property[0].code = #hints 
* #Covovax ^property[0].valueString = "T^E^B" 
* #Covovax ^property[1].code = #Relationships 
* #Covovax ^property[1].valueString = "$.v..mp" 
* #DE "Germany"
* #DE ^designation[0].language = #de-AT 
* #DE ^designation[0].value = "Deutschland" 
* #DE ^property[0].code = #hints 
* #DE ^property[0].valueString = "T^E^B" 
* #DE ^property[1].code = #Relationships 
* #DE ^property[1].valueString = "$.co" 
* #DJ "Djibouti"
* #DJ ^designation[0].language = #de-AT 
* #DJ ^designation[0].value = "Dschibuti" 
* #DJ ^property[0].code = #hints 
* #DJ ^property[0].valueString = "T^E^B" 
* #DJ ^property[1].code = #Relationships 
* #DJ ^property[1].valueString = "$.co" 
* #DK "Denmark"
* #DK ^designation[0].language = #de-AT 
* #DK ^designation[0].value = "Dänemark" 
* #DK ^property[0].code = #hints 
* #DK ^property[0].valueString = "T^E^B" 
* #DK ^property[1].code = #Relationships 
* #DK ^property[1].valueString = "$.co" 
* #DM "Dominica"
* #DM ^property[0].code = #hints 
* #DM ^property[0].valueString = "T^E^B" 
* #DM ^property[1].code = #Relationships 
* #DM ^property[1].valueString = "$.co" 
* #DO "Dominican Republic (the)"
* #DO ^designation[0].language = #de-AT 
* #DO ^designation[0].value = "Dominikanische Republik" 
* #DO ^property[0].code = #hints 
* #DO ^property[0].valueString = "T^E^B" 
* #DO ^property[1].code = #Relationships 
* #DO ^property[1].valueString = "$.co" 
* #DZ "Algeria"
* #DZ ^designation[0].language = #de-AT 
* #DZ ^designation[0].value = "Algerien" 
* #DZ ^property[0].code = #hints 
* #DZ ^property[0].valueString = "T^E^B" 
* #DZ ^property[1].code = #Relationships 
* #DZ ^property[1].valueString = "$.co" 
* #EC "Ecuador"
* #EC ^property[0].code = #hints 
* #EC ^property[0].valueString = "T^E^B" 
* #EC ^property[1].code = #Relationships 
* #EC ^property[1].valueString = "$.co" 
* #EE "Estonia"
* #EE ^designation[0].language = #de-AT 
* #EE ^designation[0].value = "Estland" 
* #EE ^property[0].code = #hints 
* #EE ^property[0].valueString = "T^E^B" 
* #EE ^property[1].code = #Relationships 
* #EE ^property[1].valueString = "$.co" 
* #EG "Egypt"
* #EG ^designation[0].language = #de-AT 
* #EG ^designation[0].value = "Ägypten" 
* #EG ^property[0].code = #hints 
* #EG ^property[0].valueString = "T^E^B" 
* #EG ^property[1].code = #Relationships 
* #EG ^property[1].valueString = "$.co" 
* #EH "Western Sahara"
* #EH ^designation[0].language = #de-AT 
* #EH ^designation[0].value = "Westsahara" 
* #EH ^property[0].code = #hints 
* #EH ^property[0].valueString = "T^E^B" 
* #EH ^property[1].code = #Relationships 
* #EH ^property[1].valueString = "$.co" 
* #ER "Eritrea"
* #ER ^property[0].code = #hints 
* #ER ^property[0].valueString = "T^E^B" 
* #ER ^property[1].code = #Relationships 
* #ER ^property[1].valueString = "$.co" 
* #ES "Spain"
* #ES ^designation[0].language = #de-AT 
* #ES ^designation[0].value = "Spanien" 
* #ES ^property[0].code = #hints 
* #ES ^property[0].valueString = "T^E^B" 
* #ES ^property[1].code = #Relationships 
* #ES ^property[1].valueString = "$.co" 
* #ET "Ethiopia"
* #ET ^designation[0].language = #de-AT 
* #ET ^designation[0].value = "Äthiopien" 
* #ET ^property[0].code = #hints 
* #ET ^property[0].valueString = "T^E^B" 
* #ET ^property[1].code = #Relationships 
* #ET ^property[1].valueString = "$.co" 
* #EU/1/20/1507 "Spikevax"
* #EU/1/20/1507 ^designation[0].language = #de-AT 
* #EU/1/20/1507 ^designation[0].value = "COVID-19 Vaccine Moderna, Injektionsdispersion, COVID-19-mRNA-Impfstoff (Nukleosid-modifiziert)" 
* #EU/1/20/1507 ^property[0].code = #hints 
* #EU/1/20/1507 ^property[0].valueString = "T^E^B" 
* #EU/1/20/1507 ^property[1].code = #Relationships 
* #EU/1/20/1507 ^property[1].valueString = "$.v..mp" 
* #EU/1/20/1525 "Jcovden"
* #EU/1/20/1525 ^designation[0].language = #de-AT 
* #EU/1/20/1525 ^designation[0].value = "COVID-19 Vaccine Janssen Injektionssuspension, COVID-19-Impfstoff (Ad26.COV2-S [rekombinant])" 
* #EU/1/20/1525 ^property[0].code = #hints 
* #EU/1/20/1525 ^property[0].valueString = "T^E^B" 
* #EU/1/20/1525 ^property[1].code = #Relationships 
* #EU/1/20/1525 ^property[1].valueString = "$.v..mp" 
* #EU/1/20/1528 "Comirnaty"
* #EU/1/20/1528 ^designation[0].language = #de-AT 
* #EU/1/20/1528 ^designation[0].value = "Comirnaty Konzentrat zur Herstellung einer Injektionsdispersion, COVID-19-mRNA-Impfstoff (Nukleosid-modifiziert)" 
* #EU/1/20/1528 ^property[0].code = #hints 
* #EU/1/20/1528 ^property[0].valueString = "T^E^B" 
* #EU/1/20/1528 ^property[1].code = #Relationships 
* #EU/1/20/1528 ^property[1].valueString = "$.v..mp" 
* #EU/1/21/1529 "Vaxzevria"
* #EU/1/21/1529 ^designation[0].language = #de-AT 
* #EU/1/21/1529 ^designation[0].value = "COVID-19 Vaccine AstraZeneca Injektionssuspension, COVID-19-Impfstoff (ChAdOx1-S [rekombinant])" 
* #EU/1/21/1529 ^property[0].code = #hints 
* #EU/1/21/1529 ^property[0].valueString = "T^E^B" 
* #EU/1/21/1529 ^property[1].code = #Relationships 
* #EU/1/21/1529 ^property[1].valueString = "$.v..mp" 
* #EU/1/21/1580 "VidPrevtyn"
* #EU/1/21/1580 ^designation[0].language = #de-AT 
* #EU/1/21/1580 ^designation[0].value = "VidPrevtyn Beta Lösung und Emulsion zur Herstellung einer Emulsion zur Injektion COVID-19-Impfstoff (rekombinant, adjuvantiert)" 
* #EU/1/21/1580 ^property[0].code = #hints 
* #EU/1/21/1580 ^property[0].valueString = "T^E^B" 
* #EU/1/21/1580 ^property[1].code = #Relationships 
* #EU/1/21/1580 ^property[1].valueString = "$.v..mp" 
* #EU/1/21/1618 "Nuvaxovid"
* #EU/1/21/1618 ^designation[0].language = #de-AT 
* #EU/1/21/1618 ^designation[0].value = "Nuvaxovid Injektionsdispersion; COVID-19 Impfstoff (rekombinant, adjuvantiert)" 
* #EU/1/21/1618 ^property[0].code = #hints 
* #EU/1/21/1618 ^property[0].valueString = "T^E^B" 
* #EU/1/21/1618 ^property[1].code = #Relationships 
* #EU/1/21/1618 ^property[1].valueString = "$.v..mp" 
* #EU/1/21/1624 "Valneva"
* #EU/1/21/1624 ^designation[0].language = #de-AT 
* #EU/1/21/1624 ^designation[0].value = "COVID-19-Impfstoff (inaktiviert, adjuvantiert) Valneva Injektionssuspension; COVID-19-Impfstoff (inaktiviert, adjuvantiert, adsorbiert)" 
* #EU/1/21/1624 ^property[0].code = #hints 
* #EU/1/21/1624 ^property[0].valueString = "T^E^B" 
* #EU/1/21/1624 ^property[1].code = #Relationships 
* #EU/1/21/1624 ^property[1].valueString = "$.v..mp" 
* #EpiVacCorona "EpiVacCorona"
* #EpiVacCorona ^designation[0].language = #de-AT 
* #EpiVacCorona ^designation[0].value = "COVID-19-Impfstoff EpiVacCorona (Vektor-Institut)" 
* #EpiVacCorona ^property[0].code = #hints 
* #EpiVacCorona ^property[0].valueString = "T^E^B" 
* #EpiVacCorona ^property[1].code = #Relationships 
* #EpiVacCorona ^property[1].valueString = "$.v..mp" 
* #EpiVacCorona-N "EpiVacCorona-N"
* #EpiVacCorona-N ^designation[0].language = #de-AT 
* #EpiVacCorona-N ^designation[0].value = "COVID-19-Impfstoff EpiVacCorona-N (Vector Institute)" 
* #EpiVacCorona-N ^property[0].code = #hints 
* #EpiVacCorona-N ^property[0].valueString = "T^E^B" 
* #EpiVacCorona-N ^property[1].code = #Relationships 
* #EpiVacCorona-N ^property[1].valueString = "$.v..mp" 
* #FI "Finland"
* #FI ^designation[0].language = #de-AT 
* #FI ^designation[0].value = "Finnland" 
* #FI ^property[0].code = #hints 
* #FI ^property[0].valueString = "T^E^B" 
* #FI ^property[1].code = #Relationships 
* #FI ^property[1].valueString = "$.co" 
* #FJ "Fiji"
* #FJ ^designation[0].language = #de-AT 
* #FJ ^designation[0].value = "Fidschi" 
* #FJ ^property[0].code = #hints 
* #FJ ^property[0].valueString = "T^E^B" 
* #FJ ^property[1].code = #Relationships 
* #FJ ^property[1].valueString = "$.co" 
* #FK "Falkland Islands (the) [Malvinas]"
* #FK ^designation[0].language = #de-AT 
* #FK ^designation[0].value = "Falklandinseln" 
* #FK ^property[0].code = #hints 
* #FK ^property[0].valueString = "T^E^B" 
* #FK ^property[1].code = #Relationships 
* #FK ^property[1].valueString = "$.co" 
* #FM "Micronesia (Federated States of)"
* #FM ^designation[0].language = #de-AT 
* #FM ^designation[0].value = "Mikronesien,Föd.Staat.von" 
* #FM ^property[0].code = #hints 
* #FM ^property[0].valueString = "T^E^B" 
* #FM ^property[1].code = #Relationships 
* #FM ^property[1].valueString = "$.co" 
* #FO "Faroe Islands (the)"
* #FO ^designation[0].language = #de-AT 
* #FO ^designation[0].value = "Färöer" 
* #FO ^property[0].code = #hints 
* #FO ^property[0].valueString = "T^E^B" 
* #FO ^property[1].code = #Relationships 
* #FO ^property[1].valueString = "$.co" 
* #FR "France"
* #FR ^designation[0].language = #de-AT 
* #FR ^designation[0].value = "Frankreich" 
* #FR ^property[0].code = #hints 
* #FR ^property[0].valueString = "T^E^B" 
* #FR ^property[1].code = #Relationships 
* #FR ^property[1].valueString = "$.co" 
* #Finlay-Institute "Finlay Institute of Vaccines"
* #Finlay-Institute ^property[0].code = #hints 
* #Finlay-Institute ^property[0].valueString = "T^E^B" 
* #Finlay-Institute ^property[1].code = #Relationships 
* #Finlay-Institute ^property[1].valueString = "$.v..ma" 
* #Fiocruz "Fiocruz"
* #Fiocruz ^property[0].code = #hints 
* #Fiocruz ^property[0].valueString = "T^E^B" 
* #Fiocruz ^property[1].code = #Relationships 
* #Fiocruz ^property[1].valueString = "$.v..ma" 
* #GA "Gabon"
* #GA ^designation[0].language = #de-AT 
* #GA ^designation[0].value = "Gabun" 
* #GA ^property[0].code = #hints 
* #GA ^property[0].valueString = "T^E^B" 
* #GA ^property[1].code = #Relationships 
* #GA ^property[1].valueString = "$.co" 
* #GB "United Kingdom of Great Britain and Northern Ireland (the)"
* #GB ^designation[0].language = #de-AT 
* #GB ^designation[0].value = "Vereinigtes Königreich" 
* #GB ^property[0].code = #hints 
* #GB ^property[0].valueString = "T^E^B" 
* #GB ^property[1].code = #Relationships 
* #GB ^property[1].valueString = "$.co" 
* #GD "Grenada"
* #GD ^property[0].code = #hints 
* #GD ^property[0].valueString = "T^E^B" 
* #GD ^property[1].code = #Relationships 
* #GD ^property[1].valueString = "$.co" 
* #GE "Georgia"
* #GE ^designation[0].language = #de-AT 
* #GE ^designation[0].value = "Georgien" 
* #GE ^property[0].code = #hints 
* #GE ^property[0].valueString = "T^E^B" 
* #GE ^property[1].code = #Relationships 
* #GE ^property[1].valueString = "$.co" 
* #GF "French Guiana"
* #GF ^designation[0].language = #de-AT 
* #GF ^designation[0].value = "Franz.Guayana" 
* #GF ^property[0].code = #hints 
* #GF ^property[0].valueString = "T^E^B" 
* #GF ^property[1].code = #Relationships 
* #GF ^property[1].valueString = "$.co" 
* #GG "Guernsey"
* #GG ^designation[0].language = #de-AT 
* #GG ^designation[0].value = "Guernsey (nur SV)" 
* #GG ^property[0].code = #hints 
* #GG ^property[0].valueString = "T^E^B" 
* #GG ^property[1].code = #Relationships 
* #GG ^property[1].valueString = "$.co" 
* #GH "Ghana"
* #GH ^property[0].code = #hints 
* #GH ^property[0].valueString = "T^E^B" 
* #GH ^property[1].code = #Relationships 
* #GH ^property[1].valueString = "$.co" 
* #GI "Gibraltar"
* #GI ^property[0].code = #hints 
* #GI ^property[0].valueString = "T^E^B" 
* #GI ^property[1].code = #Relationships 
* #GI ^property[1].valueString = "$.co" 
* #GL "Greenland"
* #GL ^designation[0].language = #de-AT 
* #GL ^designation[0].value = "Grönland" 
* #GL ^property[0].code = #hints 
* #GL ^property[0].valueString = "T^E^B" 
* #GL ^property[1].code = #Relationships 
* #GL ^property[1].valueString = "$.co" 
* #GM "Gambia (the)"
* #GM ^designation[0].language = #de-AT 
* #GM ^designation[0].value = "Gambia" 
* #GM ^property[0].code = #hints 
* #GM ^property[0].valueString = "T^E^B" 
* #GM ^property[1].code = #Relationships 
* #GM ^property[1].valueString = "$.co" 
* #GN "Guinea"
* #GN ^property[0].code = #hints 
* #GN ^property[0].valueString = "T^E^B" 
* #GN ^property[1].code = #Relationships 
* #GN ^property[1].valueString = "$.co" 
* #GP "Guadeloupe"
* #GP ^property[0].code = #hints 
* #GP ^property[0].valueString = "T^E^B" 
* #GP ^property[1].code = #Relationships 
* #GP ^property[1].valueString = "$.co" 
* #GQ "Equatorial Guinea"
* #GQ ^designation[0].language = #de-AT 
* #GQ ^designation[0].value = "Äquatorialguinea" 
* #GQ ^property[0].code = #hints 
* #GQ ^property[0].valueString = "T^E^B" 
* #GQ ^property[1].code = #Relationships 
* #GQ ^property[1].valueString = "$.co" 
* #GR "Greece"
* #GR ^designation[0].language = #de-AT 
* #GR ^designation[0].value = "Griechenland" 
* #GR ^property[0].code = #hints 
* #GR ^property[0].valueString = "T^E^B" 
* #GR ^property[1].code = #Relationships 
* #GR ^property[1].valueString = "$.co" 
* #GT "Guatemala"
* #GT ^property[0].code = #hints 
* #GT ^property[0].valueString = "T^E^B" 
* #GT ^property[1].code = #Relationships 
* #GT ^property[1].valueString = "$.co" 
* #GU "Guam"
* #GU ^property[0].code = #hints 
* #GU ^property[0].valueString = "T^E^B" 
* #GU ^property[1].code = #Relationships 
* #GU ^property[1].valueString = "$.co" 
* #GW "Guinea-Bissau"
* #GW ^property[0].code = #hints 
* #GW ^property[0].valueString = "T^E^B" 
* #GW ^property[1].code = #Relationships 
* #GW ^property[1].valueString = "$.co" 
* #GY "Guyana"
* #GY ^property[0].code = #hints 
* #GY ^property[0].valueString = "T^E^B" 
* #GY ^property[1].code = #Relationships 
* #GY ^property[1].valueString = "$.co" 
* #Gamaleya-Research-Institute "Gamaleya Research Institute"
* #Gamaleya-Research-Institute ^property[0].code = #hints 
* #Gamaleya-Research-Institute ^property[0].valueString = "T^E^B" 
* #Gamaleya-Research-Institute ^property[1].code = #Relationships 
* #Gamaleya-Research-Institute ^property[1].valueString = "$.v..ma" 
* #HK "Hong Kong"
* #HK ^designation[0].language = #de-AT 
* #HK ^designation[0].value = "Hongkong" 
* #HK ^property[0].code = #hints 
* #HK ^property[0].valueString = "T^E^B" 
* #HK ^property[1].code = #Relationships 
* #HK ^property[1].valueString = "$.co" 
* #HL "Saint Helena"
* #HL ^designation[0].language = #de-AT 
* #HL ^designation[0].value = "St. Helena" 
* #HL ^property[0].code = #hints 
* #HL ^property[0].valueString = "T^E^B" 
* #HL ^property[1].code = #Relationships 
* #HL ^property[1].valueString = "$.co" 
* #HM "Heard Island and McDonald Islands"
* #HM ^designation[0].language = #de-AT 
* #HM ^designation[0].value = "Heard/McDonaldi" 
* #HM ^property[0].code = #hints 
* #HM ^property[0].valueString = "T^E^B" 
* #HM ^property[1].code = #Relationships 
* #HM ^property[1].valueString = "$.co" 
* #HN "Honduras"
* #HN ^property[0].code = #hints 
* #HN ^property[0].valueString = "T^E^B" 
* #HN ^property[1].code = #Relationships 
* #HN ^property[1].valueString = "$.co" 
* #HR "Croatia"
* #HR ^designation[0].language = #de-AT 
* #HR ^designation[0].value = "Kroatien" 
* #HR ^property[0].code = #hints 
* #HR ^property[0].valueString = "T^E^B" 
* #HR ^property[1].code = #Relationships 
* #HR ^property[1].valueString = "$.co" 
* #HT "Haiti"
* #HT ^property[0].code = #hints 
* #HT ^property[0].valueString = "T^E^B" 
* #HT ^property[1].code = #Relationships 
* #HT ^property[1].valueString = "$.co" 
* #HU "Hungary"
* #HU ^designation[0].language = #de-AT 
* #HU ^designation[0].value = "Ungarn" 
* #HU ^property[0].code = #hints 
* #HU ^property[0].valueString = "T^E^B" 
* #HU ^property[1].code = #Relationships 
* #HU ^property[1].valueString = "$.co" 
* #Hayat-Vax "Hayat-Vax"
* #Hayat-Vax ^designation[0].language = #de-AT 
* #Hayat-Vax ^designation[0].value = "COVID-19-Impfstoff Hayat-Vax (Gulf Pharmaceutical Industries)" 
* #Hayat-Vax ^property[0].code = #hints 
* #Hayat-Vax ^property[0].valueString = "T^E^B" 
* #Hayat-Vax ^property[1].code = #Relationships 
* #Hayat-Vax ^property[1].valueString = "$.v..mp" 
* #ID "Indonesia"
* #ID ^designation[0].language = #de-AT 
* #ID ^designation[0].value = "Indonesien" 
* #ID ^property[0].code = #hints 
* #ID ^property[0].valueString = "T^E^B" 
* #ID ^property[1].code = #Relationships 
* #ID ^property[1].valueString = "$.co" 
* #IE "Ireland"
* #IE ^designation[0].language = #de-AT 
* #IE ^designation[0].value = "Irland" 
* #IE ^property[0].code = #hints 
* #IE ^property[0].valueString = "T^E^B" 
* #IE ^property[1].code = #Relationships 
* #IE ^property[1].valueString = "$.co" 
* #IL "Israel"
* #IL ^property[0].code = #hints 
* #IL ^property[0].valueString = "T^E^B" 
* #IL ^property[1].code = #Relationships 
* #IL ^property[1].valueString = "$.co" 
* #IM "Isle of Man"
* #IM ^designation[0].language = #de-AT 
* #IM ^designation[0].value = "Isle of Man (nur SV)" 
* #IM ^property[0].code = #hints 
* #IM ^property[0].valueString = "T^E^B" 
* #IM ^property[1].code = #Relationships 
* #IM ^property[1].valueString = "$.co" 
* #IN "India"
* #IN ^designation[0].language = #de-AT 
* #IN ^designation[0].value = "Indien" 
* #IN ^property[0].code = #hints 
* #IN ^property[0].valueString = "T^E^B" 
* #IN ^property[1].code = #Relationships 
* #IN ^property[1].valueString = "$.co" 
* #IO "British Indian Ocean Territory (the)"
* #IO ^designation[0].language = #de-AT 
* #IO ^designation[0].value = "Br.Terr/Ind.Ozn" 
* #IO ^property[0].code = #hints 
* #IO ^property[0].valueString = "T^E^B" 
* #IO ^property[1].code = #Relationships 
* #IO ^property[1].valueString = "$.co" 
* #IQ "Iraq"
* #IQ ^designation[0].language = #de-AT 
* #IQ ^designation[0].value = "Irak" 
* #IQ ^property[0].code = #hints 
* #IQ ^property[0].valueString = "T^E^B" 
* #IQ ^property[1].code = #Relationships 
* #IQ ^property[1].valueString = "$.co" 
* #IR "Iran (Islamic Republic of)"
* #IR ^designation[0].language = #de-AT 
* #IR ^designation[0].value = "Iran,Islamische Republik" 
* #IR ^property[0].code = #hints 
* #IR ^property[0].valueString = "T^E^B" 
* #IR ^property[1].code = #Relationships 
* #IR ^property[1].valueString = "$.co" 
* #IS "Iceland"
* #IS ^designation[0].language = #de-AT 
* #IS ^designation[0].value = "Island" 
* #IS ^property[0].code = #hints 
* #IS ^property[0].valueString = "T^E^B" 
* #IS ^property[1].code = #Relationships 
* #IS ^property[1].valueString = "$.co" 
* #IT "Italy"
* #IT ^designation[0].language = #de-AT 
* #IT ^designation[0].value = "Italien" 
* #IT ^property[0].code = #hints 
* #IT ^property[0].valueString = "T^E^B" 
* #IT ^property[1].code = #Relationships 
* #IT ^property[1].valueString = "$.co" 
* #Inactivated-SARS-CoV-2-Vero-Cell "Inactivated SARS-CoV-2 (Vero Cell)"
* #Inactivated-SARS-CoV-2-Vero-Cell ^designation[0].language = #de-AT 
* #Inactivated-SARS-CoV-2-Vero-Cell ^designation[0].value = "COVID-19-Impfstoff Inactivated-SARS-CoV-2-Vero-Cell" 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[0].code = #Relationships 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[0].valueString = "$.v..mp" 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[1].code = #status 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[1].valueCode = #retired 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[2].code = #hints 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[2].valueString = "DEPRECATED" 
* #Instituto-Butantan "Instituto Butantan"
* #Instituto-Butantan ^property[0].code = #hints 
* #Instituto-Butantan ^property[0].valueString = "T^E^B" 
* #Instituto-Butantan ^property[1].code = #Relationships 
* #Instituto-Butantan ^property[1].valueString = "$.v..ma" 
* #J07BX03 "covid-19 vaccines"
* #J07BX03 ^designation[0].language = #de-AT 
* #J07BX03 ^designation[0].value = "Covid-19 Vakzine" 
* #J07BX03 ^property[0].code = #hints 
* #J07BX03 ^property[0].valueString = "T^E^B" 
* #J07BX03 ^property[1].code = #status 
* #J07BX03 ^property[1].valueCode = #retired 
* #J07BX03 ^property[2].code = #hints 
* #J07BX03 ^property[2].valueString = "DEPRECATED" 
* #JE "Jersey"
* #JE ^designation[0].language = #de-AT 
* #JE ^designation[0].value = "Jersey (nur SV)" 
* #JE ^property[0].code = #hints 
* #JE ^property[0].valueString = "T^E^B" 
* #JE ^property[1].code = #Relationships 
* #JE ^property[1].valueString = "$.co" 
* #JM "Jamaica"
* #JM ^designation[0].language = #de-AT 
* #JM ^designation[0].value = "Jamaika" 
* #JM ^property[0].code = #hints 
* #JM ^property[0].valueString = "T^E^B" 
* #JM ^property[1].code = #Relationships 
* #JM ^property[1].valueString = "$.co" 
* #JO "Jordan"
* #JO ^designation[0].language = #de-AT 
* #JO ^designation[0].value = "Jordanien" 
* #JO ^property[0].code = #hints 
* #JO ^property[0].valueString = "T^E^B" 
* #JO ^property[1].code = #Relationships 
* #JO ^property[1].valueString = "$.co" 
* #JP "Japan"
* #JP ^property[0].code = #hints 
* #JP ^property[0].valueString = "T^E^B" 
* #JP ^property[1].code = #Relationships 
* #JP ^property[1].valueString = "$.co" 
* #KE "Kenya"
* #KE ^designation[0].language = #de-AT 
* #KE ^designation[0].value = "Kenia" 
* #KE ^property[0].code = #hints 
* #KE ^property[0].valueString = "T^E^B" 
* #KE ^property[1].code = #Relationships 
* #KE ^property[1].valueString = "$.co" 
* #KG "Kyrgyzstan"
* #KG ^designation[0].language = #de-AT 
* #KG ^designation[0].value = "Kirgisistan" 
* #KG ^property[0].code = #hints 
* #KG ^property[0].valueString = "T^E^B" 
* #KG ^property[1].code = #Relationships 
* #KG ^property[1].valueString = "$.co" 
* #KH "Cambodia"
* #KH ^designation[0].language = #de-AT 
* #KH ^designation[0].value = "Kambodscha" 
* #KH ^property[0].code = #hints 
* #KH ^property[0].valueString = "T^E^B" 
* #KH ^property[1].code = #Relationships 
* #KH ^property[1].valueString = "$.co" 
* #KI "Kiribati"
* #KI ^property[0].code = #hints 
* #KI ^property[0].valueString = "T^E^B" 
* #KI ^property[1].code = #Relationships 
* #KI ^property[1].valueString = "$.co" 
* #KM "Comoros (the)"
* #KM ^designation[0].language = #de-AT 
* #KM ^designation[0].value = "Komoren" 
* #KM ^property[0].code = #hints 
* #KM ^property[0].valueString = "T^E^B" 
* #KM ^property[1].code = #Relationships 
* #KM ^property[1].valueString = "$.co" 
* #KN "Saint Kitts and Nevis"
* #KN ^designation[0].language = #de-AT 
* #KN ^designation[0].value = "St.Kitts und Nevis" 
* #KN ^property[0].code = #hints 
* #KN ^property[0].valueString = "T^E^B" 
* #KN ^property[1].code = #Relationships 
* #KN ^property[1].valueString = "$.co" 
* #KP "Korea (the Democratic People's Republic of)"
* #KP ^designation[0].language = #de-AT 
* #KP ^designation[0].value = "Korea,Dem.Volksrepublik" 
* #KP ^property[0].code = #hints 
* #KP ^property[0].valueString = "T^E^B" 
* #KP ^property[1].code = #Relationships 
* #KP ^property[1].valueString = "$.co" 
* #KR "Korea (the Republic of)"
* #KR ^designation[0].language = #de-AT 
* #KR ^designation[0].value = "Korea,Republik" 
* #KR ^property[0].code = #hints 
* #KR ^property[0].valueString = "T^E^B" 
* #KR ^property[1].code = #Relationships 
* #KR ^property[1].valueString = "$.co" 
* #KS "Kosovo (Republic of)"
* #KS ^designation[0].language = #de-AT 
* #KS ^designation[0].value = "Kosovo, Republik" 
* #KS ^property[0].code = #hints 
* #KS ^property[0].valueString = "T^E^B" 
* #KS ^property[1].code = #Relationships 
* #KS ^property[1].valueString = "$.co" 
* #KW "Kuwait"
* #KW ^property[0].code = #hints 
* #KW ^property[0].valueString = "T^E^B" 
* #KW ^property[1].code = #Relationships 
* #KW ^property[1].valueString = "$.co" 
* #KY "Cayman Islands (the)"
* #KY ^designation[0].language = #de-AT 
* #KY ^designation[0].value = "Kaimaninseln" 
* #KY ^property[0].code = #hints 
* #KY ^property[0].valueString = "T^E^B" 
* #KY ^property[1].code = #Relationships 
* #KY ^property[1].valueString = "$.co" 
* #KZ "Kazakhstan"
* #KZ ^designation[0].language = #de-AT 
* #KZ ^designation[0].value = "Kasachstan" 
* #KZ ^property[0].code = #hints 
* #KZ ^property[0].valueString = "T^E^B" 
* #KZ ^property[1].code = #Relationships 
* #KZ ^property[1].valueString = "$.co" 
* #LA "Lao People's Democratic Republic (the)"
* #LA ^designation[0].language = #de-AT 
* #LA ^designation[0].value = "Laos,Demokr.Volksrepublik" 
* #LA ^property[0].code = #hints 
* #LA ^property[0].valueString = "T^E^B" 
* #LA ^property[1].code = #Relationships 
* #LA ^property[1].valueString = "$.co" 
* #LB "Lebanon"
* #LB ^designation[0].language = #de-AT 
* #LB ^designation[0].value = "Libanon" 
* #LB ^property[0].code = #hints 
* #LB ^property[0].valueString = "T^E^B" 
* #LB ^property[1].code = #Relationships 
* #LB ^property[1].valueString = "$.co" 
* #LC "Saint Lucia"
* #LC ^designation[0].language = #de-AT 
* #LC ^designation[0].value = "St.Lucia" 
* #LC ^property[0].code = #hints 
* #LC ^property[0].valueString = "T^E^B" 
* #LC ^property[1].code = #Relationships 
* #LC ^property[1].valueString = "$.co" 
* #LI "Liechtenstein"
* #LI ^property[0].code = #hints 
* #LI ^property[0].valueString = "T^E^B" 
* #LI ^property[1].code = #Relationships 
* #LI ^property[1].valueString = "$.co" 
* #LK "Sri Lanka"
* #LK ^property[0].code = #hints 
* #LK ^property[0].valueString = "T^E^B" 
* #LK ^property[1].code = #Relationships 
* #LK ^property[1].valueString = "$.co" 
* #LP217197-5 "ANTIKOERPER"
* #LP217197-5 ^definition = Rapid immunoassay
* #LP217197-5 ^designation[0].language = #de-AT 
* #LP217197-5 ^designation[0].value = "Antikörpernachweis auf SARS-CoV-2" 
* #LP217197-5 ^property[0].code = #Relationships 
* #LP217197-5 ^property[0].valueString = "$.t..tt" 
* #LP217198-3 "ANTIGEN"
* #LP217198-3 ^definition = Rapid immunoassay
* #LP217198-3 ^designation[0].language = #de-AT 
* #LP217198-3 ^designation[0].value = "Antigen-Test auf SARS-CoV-2" 
* #LP217198-3 ^property[0].code = #hints 
* #LP217198-3 ^property[0].valueString = "T^E^B" 
* #LP217198-3 ^property[1].code = #Relationships 
* #LP217198-3 ^property[1].valueString = "$.t..tt" 
* #LP6464-4 "PCR"
* #LP6464-4 ^definition = Nucleic acid amplification with probe detection
* #LP6464-4 ^designation[0].language = #de-AT 
* #LP6464-4 ^designation[0].value = "PCR-Test auf SARS-CoV-2" 
* #LP6464-4 ^property[0].code = #hints 
* #LP6464-4 ^property[0].valueString = "T^E^B" 
* #LP6464-4 ^property[1].code = #Relationships 
* #LP6464-4 ^property[1].valueString = "$.t..tt" 
* #LR "Liberia"
* #LR ^property[0].code = #hints 
* #LR ^property[0].valueString = "T^E^B" 
* #LR ^property[1].code = #Relationships 
* #LR ^property[1].valueString = "$.co" 
* #LS "Lesotho"
* #LS ^property[0].code = #hints 
* #LS ^property[0].valueString = "T^E^B" 
* #LS ^property[1].code = #Relationships 
* #LS ^property[1].valueString = "$.co" 
* #LT "Lithuania"
* #LT ^designation[0].language = #de-AT 
* #LT ^designation[0].value = "Litauen" 
* #LT ^property[0].code = #hints 
* #LT ^property[0].valueString = "T^E^B" 
* #LT ^property[1].code = #Relationships 
* #LT ^property[1].valueString = "$.co" 
* #LU "Luxembourg"
* #LU ^designation[0].language = #de-AT 
* #LU ^designation[0].value = "Luxemburg" 
* #LU ^property[0].code = #hints 
* #LU ^property[0].valueString = "T^E^B" 
* #LU ^property[1].code = #Relationships 
* #LU ^property[1].valueString = "$.co" 
* #LV "Latvia"
* #LV ^designation[0].language = #de-AT 
* #LV ^designation[0].value = "Lettland" 
* #LV ^property[0].code = #hints 
* #LV ^property[0].valueString = "T^E^B" 
* #LV ^property[1].code = #Relationships 
* #LV ^property[1].valueString = "$.co" 
* #LY "Libya"
* #LY ^designation[0].language = #de-AT 
* #LY ^designation[0].value = "Libysch-Arab.Dschamahir." 
* #LY ^property[0].code = #hints 
* #LY ^property[0].valueString = "T^E^B" 
* #LY ^property[1].code = #Relationships 
* #LY ^property[1].valueString = "$.co" 
* #MA "Morocco"
* #MA ^designation[0].language = #de-AT 
* #MA ^designation[0].value = "Marokko" 
* #MA ^property[0].code = #hints 
* #MA ^property[0].valueString = "T^E^B" 
* #MA ^property[1].code = #Relationships 
* #MA ^property[1].valueString = "$.co" 
* #MC "Monaco"
* #MC ^property[0].code = #hints 
* #MC ^property[0].valueString = "T^E^B" 
* #MC ^property[1].code = #Relationships 
* #MC ^property[1].valueString = "$.co" 
* #MD "Moldova (the Republic of)"
* #MD ^designation[0].language = #de-AT 
* #MD ^designation[0].value = "Moldau" 
* #MD ^property[0].code = #hints 
* #MD ^property[0].valueString = "T^E^B" 
* #MD ^property[1].code = #Relationships 
* #MD ^property[1].valueString = "$.co" 
* #ME "Montenegro"
* #ME ^property[0].code = #hints 
* #ME ^property[0].valueString = "T^E^B" 
* #ME ^property[1].code = #Relationships 
* #ME ^property[1].valueString = "$.co" 
* #MG "Madagascar"
* #MG ^designation[0].language = #de-AT 
* #MG ^designation[0].value = "Madagaskar" 
* #MG ^property[0].code = #hints 
* #MG ^property[0].valueString = "T^E^B" 
* #MG ^property[1].code = #Relationships 
* #MG ^property[1].valueString = "$.co" 
* #MH "Marshall Islands (the)"
* #MH ^designation[0].language = #de-AT 
* #MH ^designation[0].value = "Marshallinseln" 
* #MH ^property[0].code = #hints 
* #MH ^property[0].valueString = "T^E^B" 
* #MH ^property[1].code = #Relationships 
* #MH ^property[1].valueString = "$.co" 
* #MK "Republic of North Macedonia"
* #MK ^designation[0].language = #de-AT 
* #MK ^designation[0].value = "Mazedonien,ehem.jug.Rep." 
* #MK ^property[0].code = #hints 
* #MK ^property[0].valueString = "T^E^B" 
* #MK ^property[1].code = #Relationships 
* #MK ^property[1].valueString = "$.co" 
* #ML "Mali"
* #ML ^property[0].code = #hints 
* #ML ^property[0].valueString = "T^E^B" 
* #ML ^property[1].code = #Relationships 
* #ML ^property[1].valueString = "$.co" 
* #MM "Myanmar"
* #MM ^designation[0].language = #de-AT 
* #MM ^designation[0].value = "Myanmar (früher:Birma)" 
* #MM ^property[0].code = #hints 
* #MM ^property[0].valueString = "T^E^B" 
* #MM ^property[1].code = #Relationships 
* #MM ^property[1].valueString = "$.co" 
* #MN "Mongolia"
* #MN ^designation[0].language = #de-AT 
* #MN ^designation[0].value = "Mongolei" 
* #MN ^property[0].code = #hints 
* #MN ^property[0].valueString = "T^E^B" 
* #MN ^property[1].code = #Relationships 
* #MN ^property[1].valueString = "$.co" 
* #MO "Macao"
* #MO ^designation[0].language = #de-AT 
* #MO ^designation[0].value = "Macau" 
* #MO ^property[0].code = #hints 
* #MO ^property[0].valueString = "T^E^B" 
* #MO ^property[1].code = #Relationships 
* #MO ^property[1].valueString = "$.co" 
* #MP "Northern Mariana Islands (the)"
* #MP ^designation[0].language = #de-AT 
* #MP ^designation[0].value = "Marianen,Nördl." 
* #MP ^property[0].code = #hints 
* #MP ^property[0].valueString = "T^E^B" 
* #MP ^property[1].code = #Relationships 
* #MP ^property[1].valueString = "$.co" 
* #MQ "Martinique"
* #MQ ^property[0].code = #hints 
* #MQ ^property[0].valueString = "T^E^B" 
* #MQ ^property[1].code = #Relationships 
* #MQ ^property[1].valueString = "$.co" 
* #MR "Mauritania"
* #MR ^designation[0].language = #de-AT 
* #MR ^designation[0].value = "Mauretanien" 
* #MR ^property[0].code = #hints 
* #MR ^property[0].valueString = "T^E^B" 
* #MR ^property[1].code = #Relationships 
* #MR ^property[1].valueString = "$.co" 
* #MS "Montserrat"
* #MS ^property[0].code = #hints 
* #MS ^property[0].valueString = "T^E^B" 
* #MS ^property[1].code = #Relationships 
* #MS ^property[1].valueString = "$.co" 
* #MT "Malta"
* #MT ^property[0].code = #hints 
* #MT ^property[0].valueString = "T^E^B" 
* #MT ^property[1].code = #Relationships 
* #MT ^property[1].valueString = "$.co" 
* #MU "Mauritius"
* #MU ^property[0].code = #hints 
* #MU ^property[0].valueString = "T^E^B" 
* #MU ^property[1].code = #Relationships 
* #MU ^property[1].valueString = "$.co" 
* #MV "Maldives"
* #MV ^designation[0].language = #de-AT 
* #MV ^designation[0].value = "Malediven" 
* #MV ^property[0].code = #hints 
* #MV ^property[0].valueString = "T^E^B" 
* #MV ^property[1].code = #Relationships 
* #MV ^property[1].valueString = "$.co" 
* #MVC-COV1901 "MVC COVID-19 vaccine"
* #MVC-COV1901 ^designation[0].language = #de-AT 
* #MVC-COV1901 ^designation[0].value = "COVID-19-Impfstoff MVC COVID-19 vaccine (Medigen Vaccine Biologics Corporation)" 
* #MVC-COV1901 ^property[0].code = #hints 
* #MVC-COV1901 ^property[0].valueString = "T^E^B" 
* #MVC-COV1901 ^property[1].code = #Relationships 
* #MVC-COV1901 ^property[1].valueString = "$.v..mp" 
* #MW "Malawi"
* #MW ^property[0].code = #hints 
* #MW ^property[0].valueString = "T^E^B" 
* #MW ^property[1].code = #Relationships 
* #MW ^property[1].valueString = "$.co" 
* #MX "Mexico"
* #MX ^designation[0].language = #de-AT 
* #MX ^designation[0].value = "Mexiko" 
* #MX ^property[0].code = #hints 
* #MX ^property[0].valueString = "T^E^B" 
* #MX ^property[1].code = #Relationships 
* #MX ^property[1].valueString = "$.co" 
* #MY "Malaysia"
* #MY ^property[0].code = #hints 
* #MY ^property[0].valueString = "T^E^B" 
* #MY ^property[1].code = #Relationships 
* #MY ^property[1].valueString = "$.co" 
* #MZ "Mozambique"
* #MZ ^designation[0].language = #de-AT 
* #MZ ^designation[0].value = "Mosambik" 
* #MZ ^property[0].code = #hints 
* #MZ ^property[0].valueString = "T^E^B" 
* #MZ ^property[1].code = #Relationships 
* #MZ ^property[1].valueString = "$.co" 
* #NA "Namibia"
* #NA ^property[0].code = #hints 
* #NA ^property[0].valueString = "T^E^B" 
* #NA ^property[1].code = #Relationships 
* #NA ^property[1].valueString = "$.co" 
* #NC "New Caledonia"
* #NC ^designation[0].language = #de-AT 
* #NC ^designation[0].value = "Neukaledonien" 
* #NC ^property[0].code = #hints 
* #NC ^property[0].valueString = "T^E^B" 
* #NC ^property[1].code = #Relationships 
* #NC ^property[1].valueString = "$.co" 
* #NE "Niger (the)"
* #NE ^designation[0].language = #de-AT 
* #NE ^designation[0].value = "Niger" 
* #NE ^property[0].code = #hints 
* #NE ^property[0].valueString = "T^E^B" 
* #NE ^property[1].code = #Relationships 
* #NE ^property[1].valueString = "$.co" 
* #NF "Norfolk Island"
* #NF ^designation[0].language = #de-AT 
* #NF ^designation[0].value = "Norfolkinsel" 
* #NF ^property[0].code = #hints 
* #NF ^property[0].valueString = "T^E^B" 
* #NF ^property[1].code = #Relationships 
* #NF ^property[1].valueString = "$.co" 
* #NG "Nigeria"
* #NG ^property[0].code = #hints 
* #NG ^property[0].valueString = "T^E^B" 
* #NG ^property[1].code = #Relationships 
* #NG ^property[1].valueString = "$.co" 
* #NI "Nicaragua"
* #NI ^property[0].code = #hints 
* #NI ^property[0].valueString = "T^E^B" 
* #NI ^property[1].code = #Relationships 
* #NI ^property[1].valueString = "$.co" 
* #NL "Netherlands (the)"
* #NL ^designation[0].language = #de-AT 
* #NL ^designation[0].value = "Niederlande" 
* #NL ^property[0].code = #hints 
* #NL ^property[0].valueString = "T^E^B" 
* #NL ^property[1].code = #Relationships 
* #NL ^property[1].valueString = "$.co" 
* #NO "Norway"
* #NO ^designation[0].language = #de-AT 
* #NO ^designation[0].value = "Norwegen" 
* #NO ^property[0].code = #hints 
* #NO ^property[0].valueString = "T^E^B" 
* #NO ^property[1].code = #Relationships 
* #NO ^property[1].valueString = "$.co" 
* #NP "Nepal"
* #NP ^property[0].code = #hints 
* #NP ^property[0].valueString = "T^E^B" 
* #NP ^property[1].code = #Relationships 
* #NP ^property[1].valueString = "$.co" 
* #NR "Nauru"
* #NR ^property[0].code = #hints 
* #NR ^property[0].valueString = "T^E^B" 
* #NR ^property[1].code = #Relationships 
* #NR ^property[1].valueString = "$.co" 
* #NU "Niue"
* #NU ^property[0].code = #hints 
* #NU ^property[0].valueString = "T^E^B" 
* #NU ^property[1].code = #Relationships 
* #NU ^property[1].valueString = "$.co" 
* #NVSI "National Vaccine and Serum Institute, China"
* #NVSI ^property[0].code = #hints 
* #NVSI ^property[0].valueString = "T^E^B" 
* #NVSI ^property[1].code = #Relationships 
* #NVSI ^property[1].valueString = "$.v..ma" 
* #NVSI-06-08 "NVSI-06-08"
* #NVSI-06-08 ^designation[0].language = #de-AT 
* #NVSI-06-08 ^designation[0].value = "COVID-19-Impfstoff NVSI-06-08 (National Vaccine and Serum Institute, China)" 
* #NVSI-06-08 ^property[0].code = #hints 
* #NVSI-06-08 ^property[0].valueString = "T^E^B" 
* #NVSI-06-08 ^property[1].code = #Relationships 
* #NVSI-06-08 ^property[1].valueString = "$.v..mp" 
* #NVX-CoV2373 "NVX-CoV2373"
* #NVX-CoV2373 ^designation[0].language = #de-AT 
* #NVX-CoV2373 ^designation[0].value = "COVID-19-Impfstoff NVX-CoV2373 (Novavax)" 
* #NVX-CoV2373 ^property[0].code = #Relationships 
* #NVX-CoV2373 ^property[0].valueString = "$.v..mp" 
* #NVX-CoV2373 ^property[1].code = #status 
* #NVX-CoV2373 ^property[1].valueCode = #retired 
* #NVX-CoV2373 ^property[2].code = #hints 
* #NVX-CoV2373 ^property[2].valueString = "DEPRECATED" 
* #NZ "New Zealand"
* #NZ ^designation[0].language = #de-AT 
* #NZ ^designation[0].value = "Neuseeland" 
* #NZ ^property[0].code = #hints 
* #NZ ^property[0].valueString = "T^E^B" 
* #NZ ^property[1].code = #Relationships 
* #NZ ^property[1].valueString = "$.co" 
* #OM "Oman"
* #OM ^property[0].code = #hints 
* #OM ^property[0].valueString = "T^E^B" 
* #OM ^property[1].code = #Relationships 
* #OM ^property[1].valueString = "$.co" 
* #ORG-100000788 "Sanofi Pasteur"
* #ORG-100000788 ^property[0].code = #hints 
* #ORG-100000788 ^property[0].valueString = "T^E^B" 
* #ORG-100000788 ^property[1].code = #Relationships 
* #ORG-100000788 ^property[1].valueString = "$.v..ma" 
* #ORG-100001417 "Janssen-Cilag International"
* #ORG-100001417 ^property[0].code = #hints 
* #ORG-100001417 ^property[0].valueString = "T^E^B" 
* #ORG-100001417 ^property[1].code = #Relationships 
* #ORG-100001417 ^property[1].valueString = "$.v..ma" 
* #ORG-100001699 "AstraZeneca AB"
* #ORG-100001699 ^property[0].code = #hints 
* #ORG-100001699 ^property[0].valueString = "T^E^B" 
* #ORG-100001699 ^property[1].code = #Relationships 
* #ORG-100001699 ^property[1].valueString = "$.v..ma" 
* #ORG-100001981 "Serum Institute Of India Private Limited"
* #ORG-100001981 ^property[0].code = #hints 
* #ORG-100001981 ^property[0].valueString = "T^E^B" 
* #ORG-100001981 ^property[1].code = #Relationships 
* #ORG-100001981 ^property[1].valueString = "$.v..ma" 
* #ORG-100006270 "Curevac AG"
* #ORG-100006270 ^property[0].code = #hints 
* #ORG-100006270 ^property[0].valueString = "T^E^B" 
* #ORG-100006270 ^property[1].code = #Relationships 
* #ORG-100006270 ^property[1].valueString = "$.v..ma" 
* #ORG-100007893 "R-Pharm CJSC"
* #ORG-100007893 ^property[0].code = #hints 
* #ORG-100007893 ^property[0].valueString = "T^E^B" 
* #ORG-100007893 ^property[1].code = #Relationships 
* #ORG-100007893 ^property[1].valueString = "$.v..ma" 
* #ORG-100008549 "Medicago Inc."
* #ORG-100008549 ^property[0].code = #hints 
* #ORG-100008549 ^property[0].valueString = "T^E^B" 
* #ORG-100008549 ^property[1].code = #Relationships 
* #ORG-100008549 ^property[1].valueString = "$.v..ma" 
* #ORG-100010771 "Sinopharm Weiqida Europe Pharmaceutical s.r.o. - Prague location"
* #ORG-100010771 ^property[0].code = #hints 
* #ORG-100010771 ^property[0].valueString = "T^E^B" 
* #ORG-100010771 ^property[1].code = #Relationships 
* #ORG-100010771 ^property[1].valueString = "$.v..ma" 
* #ORG-100013793 "CanSino Biologics"
* #ORG-100013793 ^property[0].code = #hints 
* #ORG-100013793 ^property[0].valueString = "T^E^B" 
* #ORG-100013793 ^property[1].code = #Relationships 
* #ORG-100013793 ^property[1].valueString = "$.v..ma" 
* #ORG-100020693 "China Sinopharm International Corp. - Beijing location"
* #ORG-100020693 ^property[0].code = #hints 
* #ORG-100020693 ^property[0].valueString = "T^E^B" 
* #ORG-100020693 ^property[1].code = #Relationships 
* #ORG-100020693 ^property[1].valueString = "$.v..ma" 
* #ORG-100023050 "Gulf Pharmaceutical Industries"
* #ORG-100023050 ^property[0].code = #hints 
* #ORG-100023050 ^property[0].valueString = "T^E^B" 
* #ORG-100023050 ^property[1].code = #Relationships 
* #ORG-100023050 ^property[1].valueString = "$.v..ma" 
* #ORG-100024420 "Sinopharm Zhijun (Shenzhen) Pharmaceutical Co. Ltd. - Shenzhen location"
* #ORG-100024420 ^property[0].code = #hints 
* #ORG-100024420 ^property[0].valueString = "T^E^B" 
* #ORG-100024420 ^property[1].code = #Relationships 
* #ORG-100024420 ^property[1].valueString = "$.v..ma" 
* #ORG-100026614 "Sinocelltech Ltd."
* #ORG-100026614 ^property[0].code = #hints 
* #ORG-100026614 ^property[0].valueString = "T^E^B" 
* #ORG-100026614 ^property[1].code = #Relationships 
* #ORG-100026614 ^property[1].valueString = "$.v..ma" 
* #ORG-100030215 "Biontech Manufacturing GmbH"
* #ORG-100030215 ^property[0].code = #hints 
* #ORG-100030215 ^property[0].valueString = "T^E^B" 
* #ORG-100030215 ^property[1].code = #Relationships 
* #ORG-100030215 ^property[1].valueString = "$.v..ma" 
* #ORG-100031184 "Moderna Biotech Spain S.L."
* #ORG-100031184 ^property[0].code = #hints 
* #ORG-100031184 ^property[0].valueString = "T^E^B" 
* #ORG-100031184 ^property[1].code = #Relationships 
* #ORG-100031184 ^property[1].valueString = "$.v..ma" 
* #ORG-100032020 "Novavax CZ, a.s."
* #ORG-100032020 ^property[0].code = #hints 
* #ORG-100032020 ^property[0].valueString = "T^E^B" 
* #ORG-100032020 ^property[1].code = #Relationships 
* #ORG-100032020 ^property[1].valueString = "$.v..ma" 
* #ORG-100033914 "Medigen Vaccine Biologics Corporation"
* #ORG-100033914 ^property[0].code = #hints 
* #ORG-100033914 ^property[0].valueString = "T^E^B" 
* #ORG-100033914 ^property[1].code = #Relationships 
* #ORG-100033914 ^property[1].valueString = "$.v..ma" 
* #ORG-100036422 "Valneva France"
* #ORG-100036422 ^property[0].code = #hints 
* #ORG-100036422 ^property[0].valueString = "T^E^B" 
* #ORG-100036422 ^property[1].code = #Relationships 
* #ORG-100036422 ^property[1].valueString = "$.v..ma" 
* #PA "Panama"
* #PA ^property[0].code = #hints 
* #PA ^property[0].valueString = "T^E^B" 
* #PA ^property[1].code = #Relationships 
* #PA ^property[1].valueString = "$.co" 
* #PE "Peru"
* #PE ^property[0].code = #hints 
* #PE ^property[0].valueString = "T^E^B" 
* #PE ^property[1].code = #Relationships 
* #PE ^property[1].valueString = "$.co" 
* #PF "French Polynesia"
* #PF ^designation[0].language = #de-AT 
* #PF ^designation[0].value = "Franz.Polynesien" 
* #PF ^property[0].code = #hints 
* #PF ^property[0].valueString = "T^E^B" 
* #PF ^property[1].code = #Relationships 
* #PF ^property[1].valueString = "$.co" 
* #PG "Papua New Guinea"
* #PG ^designation[0].language = #de-AT 
* #PG ^designation[0].value = "Papua-Neuguinea" 
* #PG ^property[0].code = #hints 
* #PG ^property[0].valueString = "T^E^B" 
* #PG ^property[1].code = #Relationships 
* #PG ^property[1].valueString = "$.co" 
* #PH "Philippines (the)"
* #PH ^designation[0].language = #de-AT 
* #PH ^designation[0].value = "Philippinen" 
* #PH ^property[0].code = #hints 
* #PH ^property[0].valueString = "T^E^B" 
* #PH ^property[1].code = #Relationships 
* #PH ^property[1].valueString = "$.co" 
* #PK "Pakistan"
* #PK ^property[0].code = #hints 
* #PK ^property[0].valueString = "T^E^B" 
* #PK ^property[1].code = #Relationships 
* #PK ^property[1].valueString = "$.co" 
* #PL "Poland"
* #PL ^designation[0].language = #de-AT 
* #PL ^designation[0].value = "Polen" 
* #PL ^property[0].code = #hints 
* #PL ^property[0].valueString = "T^E^B" 
* #PL ^property[1].code = #Relationships 
* #PL ^property[1].valueString = "$.co" 
* #PM "Saint Pierre and Miquelon"
* #PM ^designation[0].language = #de-AT 
* #PM ^designation[0].value = "St.Pierre/Miquel" 
* #PM ^property[0].code = #hints 
* #PM ^property[0].valueString = "T^E^B" 
* #PM ^property[1].code = #Relationships 
* #PM ^property[1].valueString = "$.co" 
* #PN "Pitcairn"
* #PN ^designation[0].language = #de-AT 
* #PN ^designation[0].value = "Pitcairninseln" 
* #PN ^property[0].code = #hints 
* #PN ^property[0].valueString = "T^E^B" 
* #PN ^property[1].code = #Relationships 
* #PN ^property[1].valueString = "$.co" 
* #PR "Puerto Rico"
* #PR ^property[0].code = #hints 
* #PR ^property[0].valueString = "T^E^B" 
* #PR ^property[1].code = #Relationships 
* #PR ^property[1].valueString = "$.co" 
* #PS "Palestine, State of"
* #PS ^designation[0].language = #de-AT 
* #PS ^designation[0].value = "Paläst/Wjld-Gaza" 
* #PS ^property[0].code = #hints 
* #PS ^property[0].valueString = "T^E^B" 
* #PS ^property[1].code = #Relationships 
* #PS ^property[1].valueString = "$.co" 
* #PT "Portugal"
* #PT ^property[0].code = #hints 
* #PT ^property[0].valueString = "T^E^B" 
* #PT ^property[1].code = #Relationships 
* #PT ^property[1].valueString = "$.co" 
* #PW "Palau"
* #PW ^property[0].code = #hints 
* #PW ^property[0].valueString = "T^E^B" 
* #PW ^property[1].code = #Relationships 
* #PW ^property[1].valueString = "$.co" 
* #PY "Paraguay"
* #PY ^property[0].code = #hints 
* #PY ^property[0].valueString = "T^E^B" 
* #PY ^property[1].code = #Relationships 
* #PY ^property[1].valueString = "$.co" 
* #QA "Qatar"
* #QA ^designation[0].language = #de-AT 
* #QA ^designation[0].value = "Katar" 
* #QA ^property[0].code = #hints 
* #QA ^property[0].valueString = "T^E^B" 
* #QA ^property[1].code = #Relationships 
* #QA ^property[1].valueString = "$.co" 
* #R-COVI "R-COVI"
* #R-COVI ^designation[0].language = #de-AT 
* #R-COVI ^designation[0].value = "COVID-19-Impfstoff R-COVI (R-Pharm CJSC)" 
* #R-COVI ^property[0].code = #hints 
* #R-COVI ^property[0].valueString = "T^E^B" 
* #R-COVI ^property[1].code = #Relationships 
* #R-COVI ^property[1].valueString = "$.v..mp" 
* #RE "Réunion"
* #RE ^designation[0].language = #de-AT 
* #RE ^designation[0].value = "Reunion" 
* #RE ^property[0].code = #hints 
* #RE ^property[0].valueString = "T^E^B" 
* #RE ^property[1].code = #Relationships 
* #RE ^property[1].valueString = "$.co" 
* #RO "Romania"
* #RO ^designation[0].language = #de-AT 
* #RO ^designation[0].value = "Rumänien" 
* #RO ^property[0].code = #hints 
* #RO ^property[0].valueString = "T^E^B" 
* #RO ^property[1].code = #Relationships 
* #RO ^property[1].valueString = "$.co" 
* #RS "Serbia"
* #RS ^designation[0].language = #de-AT 
* #RS ^designation[0].value = "Serbien" 
* #RS ^property[0].code = #hints 
* #RS ^property[0].valueString = "T^E^B" 
* #RS ^property[1].code = #Relationships 
* #RS ^property[1].valueString = "$.co" 
* #RU "Russian Federation (the)"
* #RU ^designation[0].language = #de-AT 
* #RU ^designation[0].value = "Russische Föderation" 
* #RU ^property[0].code = #hints 
* #RU ^property[0].valueString = "T^E^B" 
* #RU ^property[1].code = #Relationships 
* #RU ^property[1].valueString = "$.co" 
* #RW "Rwanda"
* #RW ^designation[0].language = #de-AT 
* #RW ^designation[0].value = "Ruanda" 
* #RW ^property[0].code = #hints 
* #RW ^property[0].valueString = "T^E^B" 
* #RW ^property[1].code = #Relationships 
* #RW ^property[1].valueString = "$.co" 
* #SA "Saudi Arabia"
* #SA ^designation[0].language = #de-AT 
* #SA ^designation[0].value = "Saudi-Arabien" 
* #SA ^property[0].code = #hints 
* #SA ^property[0].valueString = "T^E^B" 
* #SA ^property[1].code = #Relationships 
* #SA ^property[1].valueString = "$.co" 
* #SB "Solomon Islands"
* #SB ^designation[0].language = #de-AT 
* #SB ^designation[0].value = "Salomonen" 
* #SB ^property[0].code = #hints 
* #SB ^property[0].valueString = "T^E^B" 
* #SB ^property[1].code = #Relationships 
* #SB ^property[1].valueString = "$.co" 
* #SC "Seychelles"
* #SC ^designation[0].language = #de-AT 
* #SC ^designation[0].value = "Seychellen" 
* #SC ^property[0].code = #hints 
* #SC ^property[0].valueString = "T^E^B" 
* #SC ^property[1].code = #Relationships 
* #SC ^property[1].valueString = "$.co" 
* #SCTV01C "SCTV01C"
* #SCTV01C ^designation[0].language = #de-AT 
* #SCTV01C ^designation[0].value = "COVID-19-Impfstoff SCTV01C (Sinocelltech Ltd.)" 
* #SCTV01C ^property[0].code = #hints 
* #SCTV01C ^property[0].valueString = "T^E^B" 
* #SCTV01C ^property[1].code = #Relationships 
* #SCTV01C ^property[1].valueString = "$.v..mp" 
* #SD "Sudan (the)"
* #SD ^designation[0].language = #de-AT 
* #SD ^designation[0].value = "Sudan" 
* #SD ^property[0].code = #hints 
* #SD ^property[0].valueString = "T^E^B" 
* #SD ^property[1].code = #Relationships 
* #SD ^property[1].valueString = "$.co" 
* #SE "Sweden"
* #SE ^designation[0].language = #de-AT 
* #SE ^designation[0].value = "Schweden" 
* #SE ^property[0].code = #hints 
* #SE ^property[0].valueString = "T^E^B" 
* #SE ^property[1].code = #Relationships 
* #SE ^property[1].valueString = "$.co" 
* #SG "Singapore"
* #SG ^designation[0].language = #de-AT 
* #SG ^designation[0].value = "Singapur" 
* #SG ^property[0].code = #hints 
* #SG ^property[0].valueString = "T^E^B" 
* #SG ^property[1].code = #Relationships 
* #SG ^property[1].valueString = "$.co" 
* #SH "Saint Helena, Ascension and Tristan da Cunha"
* #SH ^designation[0].language = #de-AT 
* #SH ^designation[0].value = "Sankt Helena" 
* #SH ^property[0].code = #hints 
* #SH ^property[0].valueString = "T^E^B" 
* #SH ^property[1].code = #Relationships 
* #SH ^property[1].valueString = "$.co" 
* #SI "Slovenia"
* #SI ^designation[0].language = #de-AT 
* #SI ^designation[0].value = "Slowenien" 
* #SI ^property[0].code = #hints 
* #SI ^property[0].valueString = "T^E^B" 
* #SI ^property[1].code = #Relationships 
* #SI ^property[1].valueString = "$.co" 
* #SJ "Svalbard and Jan Mayen"
* #SJ ^designation[0].language = #de-AT 
* #SJ ^designation[0].value = "Svalbard/JanMay" 
* #SJ ^property[0].code = #hints 
* #SJ ^property[0].valueString = "T^E^B" 
* #SJ ^property[1].code = #Relationships 
* #SJ ^property[1].valueString = "$.co" 
* #SK "Slovakia"
* #SK ^designation[0].language = #de-AT 
* #SK ^designation[0].value = "Slowakei" 
* #SK ^property[0].code = #hints 
* #SK ^property[0].valueString = "T^E^B" 
* #SK ^property[1].code = #Relationships 
* #SK ^property[1].valueString = "$.co" 
* #SL "Sierra Leone"
* #SL ^property[0].code = #hints 
* #SL ^property[0].valueString = "T^E^B" 
* #SL ^property[1].code = #Relationships 
* #SL ^property[1].valueString = "$.co" 
* #SM "San Marino"
* #SM ^property[0].code = #hints 
* #SM ^property[0].valueString = "T^E^B" 
* #SM ^property[1].code = #Relationships 
* #SM ^property[1].valueString = "$.co" 
* #SN "Senegal"
* #SN ^property[0].code = #hints 
* #SN ^property[0].valueString = "T^E^B" 
* #SN ^property[1].code = #Relationships 
* #SN ^property[1].valueString = "$.co" 
* #SO "Somalia"
* #SO ^property[0].code = #hints 
* #SO ^property[0].valueString = "T^E^B" 
* #SO ^property[1].code = #Relationships 
* #SO ^property[1].valueString = "$.co" 
* #SR "Suriname"
* #SR ^property[0].code = #hints 
* #SR ^property[0].valueString = "T^E^B" 
* #SR ^property[1].code = #Relationships 
* #SR ^property[1].valueString = "$.co" 
* #ST "Sao Tome and Principe"
* #ST ^designation[0].language = #de-AT 
* #ST ^designation[0].value = "Sao Tome und Principe" 
* #ST ^property[0].code = #hints 
* #ST ^property[0].valueString = "T^E^B" 
* #ST ^property[1].code = #Relationships 
* #ST ^property[1].valueString = "$.co" 
* #SU "Soviet Union"
* #SU ^designation[0].language = #de-AT 
* #SU ^designation[0].value = "eheml. Sowjetunion" 
* #SU ^property[0].code = #hints 
* #SU ^property[0].valueString = "T^E^B" 
* #SU ^property[1].code = #Relationships 
* #SU ^property[1].valueString = "$.co" 
* #SV "El Salvador"
* #SV ^property[0].code = #hints 
* #SV ^property[0].valueString = "T^E^B" 
* #SV ^property[1].code = #Relationships 
* #SV ^property[1].valueString = "$.co" 
* #SY "Syrian Arab Republic"
* #SY ^designation[0].language = #de-AT 
* #SY ^designation[0].value = "Syrien,Arabische Republik" 
* #SY ^property[0].code = #hints 
* #SY ^property[0].valueString = "T^E^B" 
* #SY ^property[1].code = #Relationships 
* #SY ^property[1].valueString = "$.co" 
* #SZ "Eswatini"
* #SZ ^designation[0].language = #de-AT 
* #SZ ^designation[0].value = "Swasiland" 
* #SZ ^property[0].code = #hints 
* #SZ ^property[0].valueString = "T^E^B" 
* #SZ ^property[1].code = #Relationships 
* #SZ ^property[1].valueString = "$.co" 
* #Sinopharm-WIBP "Sinopharm - Wuhan Institute of Biological Products"
* #Sinopharm-WIBP ^property[0].code = #hints 
* #Sinopharm-WIBP ^property[0].valueString = "T^E^B" 
* #Sinopharm-WIBP ^property[1].code = #Relationships 
* #Sinopharm-WIBP ^property[1].valueString = "$.v..ma" 
* #Sinovac-Biotech "Sinovac Biotech"
* #Sinovac-Biotech ^property[0].code = #hints 
* #Sinovac-Biotech ^property[0].valueString = "T^E^B" 
* #Sinovac-Biotech ^property[1].code = #Relationships 
* #Sinovac-Biotech ^property[1].valueString = "$.v..ma" 
* #Soberana-02 "Soberana 02"
* #Soberana-02 ^designation[0].language = #de-AT 
* #Soberana-02 ^designation[0].value = "COVID-19-Impfstoff Soberana 02 (Finlay-Institute)" 
* #Soberana-02 ^property[0].code = #hints 
* #Soberana-02 ^property[0].valueString = "T^E^B" 
* #Soberana-02 ^property[1].code = #Relationships 
* #Soberana-02 ^property[1].valueString = "$.v..mp" 
* #Soberana-Plus "Soberana Plus"
* #Soberana-Plus ^designation[0].language = #de-AT 
* #Soberana-Plus ^designation[0].value = "COVID-19-Impfstoff Soberana Plus (Finlay-Institute)" 
* #Soberana-Plus ^property[0].code = #hints 
* #Soberana-Plus ^property[0].valueString = "T^E^B" 
* #Soberana-Plus ^property[1].code = #Relationships 
* #Soberana-Plus ^property[1].valueString = "$.v..mp" 
* #Sputnik-Light "Sputnik Light"
* #Sputnik-Light ^designation[0].language = #de-AT 
* #Sputnik-Light ^designation[0].value = "COVID-19-Impfstoff Sputnik-Light (Gamaleya)" 
* #Sputnik-Light ^property[0].code = #hints 
* #Sputnik-Light ^property[0].valueString = "T^E^B" 
* #Sputnik-Light ^property[1].code = #Relationships 
* #Sputnik-Light ^property[1].valueString = "$.v..mp" 
* #Sputnik-M "Sputnik-M"
* #Sputnik-M ^designation[0].language = #de-AT 
* #Sputnik-M ^designation[0].value = "COVID-19-Impfstoff Sputnik-M (Gamaleya Research Institute)" 
* #Sputnik-M ^property[0].code = #hints 
* #Sputnik-M ^property[0].valueString = "T^E^B" 
* #Sputnik-M ^property[1].code = #Relationships 
* #Sputnik-M ^property[1].valueString = "$.v..mp" 
* #Sputnik-V "Sputnik-V"
* #Sputnik-V ^designation[0].language = #de-AT 
* #Sputnik-V ^designation[0].value = "COVID-19-Impfstoff Sputnik-V (Gamaleya)" 
* #Sputnik-V ^property[0].code = #hints 
* #Sputnik-V ^property[0].valueString = "T^E^B" 
* #Sputnik-V ^property[1].code = #Relationships 
* #Sputnik-V ^property[1].valueString = "$.v..mp" 
* #TA "Tristan da Cunha"
* #TA ^property[0].code = #hints 
* #TA ^property[0].valueString = "T^E^B" 
* #TA ^property[1].code = #Relationships 
* #TA ^property[1].valueString = "$.co" 
* #TC "Turks and Caicos Islands (the)"
* #TC ^designation[0].language = #de-AT 
* #TC ^designation[0].value = "Turks/Caicosin" 
* #TC ^property[0].code = #hints 
* #TC ^property[0].valueString = "T^E^B" 
* #TC ^property[1].code = #Relationships 
* #TC ^property[1].valueString = "$.co" 
* #TD "Chad"
* #TD ^designation[0].language = #de-AT 
* #TD ^designation[0].value = "Tschad" 
* #TD ^property[0].code = #hints 
* #TD ^property[0].valueString = "T^E^B" 
* #TD ^property[1].code = #Relationships 
* #TD ^property[1].valueString = "$.co" 
* #TF "French Southern Territories (the)"
* #TF ^designation[0].language = #de-AT 
* #TF ^designation[0].value = "Fr.Südgebiete" 
* #TF ^property[0].code = #hints 
* #TF ^property[0].valueString = "T^E^B" 
* #TF ^property[1].code = #Relationships 
* #TF ^property[1].valueString = "$.co" 
* #TG "Togo"
* #TG ^property[0].code = #hints 
* #TG ^property[0].valueString = "T^E^B" 
* #TG ^property[1].code = #Relationships 
* #TG ^property[1].valueString = "$.co" 
* #TH "Thailand"
* #TH ^property[0].code = #hints 
* #TH ^property[0].valueString = "T^E^B" 
* #TH ^property[1].code = #Relationships 
* #TH ^property[1].valueString = "$.co" 
* #TJ "Tajikistan"
* #TJ ^designation[0].language = #de-AT 
* #TJ ^designation[0].value = "Tadschikistan" 
* #TJ ^property[0].code = #hints 
* #TJ ^property[0].valueString = "T^E^B" 
* #TJ ^property[1].code = #Relationships 
* #TJ ^property[1].valueString = "$.co" 
* #TK "Tokelau"
* #TK ^property[0].code = #hints 
* #TK ^property[0].valueString = "T^E^B" 
* #TK ^property[1].code = #Relationships 
* #TK ^property[1].valueString = "$.co" 
* #TL "Timor-Leste"
* #TL ^property[0].code = #hints 
* #TL ^property[0].valueString = "T^E^B" 
* #TL ^property[1].code = #Relationships 
* #TL ^property[1].valueString = "$.co" 
* #TM "Turkmenistan"
* #TM ^property[0].code = #hints 
* #TM ^property[0].valueString = "T^E^B" 
* #TM ^property[1].code = #Relationships 
* #TM ^property[1].valueString = "$.co" 
* #TN "Tunisia"
* #TN ^designation[0].language = #de-AT 
* #TN ^designation[0].value = "Tunesien" 
* #TN ^property[0].code = #hints 
* #TN ^property[0].valueString = "T^E^B" 
* #TN ^property[1].code = #Relationships 
* #TN ^property[1].valueString = "$.co" 
* #TO "Tonga"
* #TO ^property[0].code = #hints 
* #TO ^property[0].valueString = "T^E^B" 
* #TO ^property[1].code = #Relationships 
* #TO ^property[1].valueString = "$.co" 
* #TR "Turkey"
* #TR ^designation[0].language = #de-AT 
* #TR ^designation[0].value = "Türkei" 
* #TR ^property[0].code = #hints 
* #TR ^property[0].valueString = "T^E^B" 
* #TR ^property[1].code = #Relationships 
* #TR ^property[1].valueString = "$.co" 
* #TT "Trinidad and Tobago"
* #TT ^designation[0].language = #de-AT 
* #TT ^designation[0].value = "Trinidad und Tobago" 
* #TT ^property[0].code = #hints 
* #TT ^property[0].valueString = "T^E^B" 
* #TT ^property[1].code = #Relationships 
* #TT ^property[1].valueString = "$.co" 
* #TV "Tuvalu"
* #TV ^property[0].code = #hints 
* #TV ^property[0].valueString = "T^E^B" 
* #TV ^property[1].code = #Relationships 
* #TV ^property[1].valueString = "$.co" 
* #TW "Taiwan (Province of China)"
* #TW ^designation[0].language = #de-AT 
* #TW ^designation[0].value = "Taiwan" 
* #TW ^property[0].code = #hints 
* #TW ^property[0].valueString = "T^E^B" 
* #TW ^property[1].code = #Relationships 
* #TW ^property[1].valueString = "$.co" 
* #TZ "Tanzania, United Republic of"
* #TZ ^designation[0].language = #de-AT 
* #TZ ^designation[0].value = "Tansania,Vereinigte Rep." 
* #TZ ^property[0].code = #hints 
* #TZ ^property[0].valueString = "T^E^B" 
* #TZ ^property[1].code = #Relationships 
* #TZ ^property[1].valueString = "$.co" 
* #UA "Ukraine"
* #UA ^property[0].code = #hints 
* #UA ^property[0].valueString = "T^E^B" 
* #UA ^property[1].code = #Relationships 
* #UA ^property[1].valueString = "$.co" 
* #UG "Uganda"
* #UG ^property[0].code = #hints 
* #UG ^property[0].valueString = "T^E^B" 
* #UG ^property[1].code = #Relationships 
* #UG ^property[1].valueString = "$.co" 
* #UM "United States Minor Outlying Islands (the)"
* #UM ^designation[0].language = #de-AT 
* #UM ^designation[0].value = "Amerik.Ozeanien" 
* #UM ^property[0].code = #hints 
* #UM ^property[0].valueString = "T^E^B" 
* #UM ^property[1].code = #Relationships 
* #UM ^property[1].valueString = "$.co" 
* #US "United States of America (the)"
* #US ^designation[0].language = #de-AT 
* #US ^designation[0].value = "Vereinigte Staaten" 
* #US ^property[0].code = #hints 
* #US ^property[0].valueString = "T^E^B" 
* #US ^property[1].code = #Relationships 
* #US ^property[1].valueString = "$.co" 
* #UY "Uruguay"
* #UY ^property[0].code = #hints 
* #UY ^property[0].valueString = "T^E^B" 
* #UY ^property[1].code = #Relationships 
* #UY ^property[1].valueString = "$.co" 
* #UZ "Uzbekistan"
* #UZ ^designation[0].language = #de-AT 
* #UZ ^designation[0].value = "Usbekistan" 
* #UZ ^property[0].code = #hints 
* #UZ ^property[0].valueString = "T^E^B" 
* #UZ ^property[1].code = #Relationships 
* #UZ ^property[1].valueString = "$.co" 
* #VA "Holy See (the)"
* #VA ^designation[0].language = #de-AT 
* #VA ^designation[0].value = "Vatikanstadt" 
* #VA ^property[0].code = #hints 
* #VA ^property[0].valueString = "T^E^B" 
* #VA ^property[1].code = #Relationships 
* #VA ^property[1].valueString = "$.co" 
* #VC "Saint Vincent and the Grenadines"
* #VC ^designation[0].language = #de-AT 
* #VC ^designation[0].value = "St.Vincent u.d.Grenadinen" 
* #VC ^property[0].code = #hints 
* #VC ^property[0].valueString = "T^E^B" 
* #VC ^property[1].code = #Relationships 
* #VC ^property[1].valueString = "$.co" 
* #VE "Venezuela (Bolivarian Republic of)"
* #VE ^designation[0].language = #de-AT 
* #VE ^designation[0].value = "Venezuela" 
* #VE ^property[0].code = #hints 
* #VE ^property[0].valueString = "T^E^B" 
* #VE ^property[1].code = #Relationships 
* #VE ^property[1].valueString = "$.co" 
* #VG "Virgin Islands (British)"
* #VG ^designation[0].language = #de-AT 
* #VG ^designation[0].value = "Br.Jungfernin." 
* #VG ^property[0].code = #hints 
* #VG ^property[0].valueString = "T^E^B" 
* #VG ^property[1].code = #Relationships 
* #VG ^property[1].valueString = "$.co" 
* #VI "Virgin Islands (U.S.)"
* #VI ^designation[0].language = #de-AT 
* #VI ^designation[0].value = "Am.Jungfernin." 
* #VI ^property[0].code = #hints 
* #VI ^property[0].valueString = "T^E^B" 
* #VI ^property[1].code = #Relationships 
* #VI ^property[1].valueString = "$.co" 
* #VLA2001 "VLA2001"
* #VLA2001 ^designation[0].language = #de-AT 
* #VLA2001 ^designation[0].value = "COVID-19-Impfstoff VLA2001 (Valneva France)" 
* #VLA2001 ^property[0].code = #hints 
* #VLA2001 ^property[0].valueString = "T^E^B" 
* #VLA2001 ^property[1].code = #Relationships 
* #VLA2001 ^property[1].valueString = "$.v..mp" 
* #VN "Viet Nam"
* #VN ^designation[0].language = #de-AT 
* #VN ^designation[0].value = "Vietnam" 
* #VN ^property[0].code = #hints 
* #VN ^property[0].valueString = "T^E^B" 
* #VN ^property[1].code = #Relationships 
* #VN ^property[1].valueString = "$.co" 
* #VU "Vanuatu"
* #VU ^property[0].code = #hints 
* #VU ^property[0].valueString = "T^E^B" 
* #VU ^property[1].code = #Relationships 
* #VU ^property[1].valueString = "$.co" 
* #Vector-Institute "Vector Institute"
* #Vector-Institute ^property[0].code = #hints 
* #Vector-Institute ^property[0].valueString = "T^E^B" 
* #Vector-Institute ^property[1].code = #Relationships 
* #Vector-Institute ^property[1].valueString = "$.v..ma" 
* #Vidprevtyn "Vidprevtyn"
* #Vidprevtyn ^designation[0].language = #de-AT 
* #Vidprevtyn ^designation[0].value = "COVID-19-Impfstoff Vidprevtyn (Sanofi Pasteur)" 
* #Vidprevtyn ^property[0].code = #hints 
* #Vidprevtyn ^property[0].valueString = "T^E^B" 
* #Vidprevtyn ^property[1].code = #Relationships 
* #Vidprevtyn ^property[1].valueString = "$.v..mp" 
* #WF "Wallis and Futuna"
* #WF ^designation[0].language = #de-AT 
* #WF ^designation[0].value = "Wallis/Futuna" 
* #WF ^property[0].code = #hints 
* #WF ^property[0].valueString = "T^E^B" 
* #WF ^property[1].code = #Relationships 
* #WF ^property[1].valueString = "$.co" 
* #WIBP-CorV "WIBP-CorV"
* #WIBP-CorV ^designation[0].language = #de-AT 
* #WIBP-CorV ^designation[0].value = "COVID-19-Impfstoff WIBP-CorV (Sinopharm - Wuhan Institute of Biological Products)" 
* #WIBP-CorV ^property[0].code = #hints 
* #WIBP-CorV ^property[0].valueString = "T^E^B" 
* #WIBP-CorV ^property[1].code = #Relationships 
* #WIBP-CorV ^property[1].valueString = "$.v..mp" 
* #WS "Samoa"
* #WS ^property[0].code = #hints 
* #WS ^property[0].valueString = "T^E^B" 
* #WS ^property[1].code = #Relationships 
* #WS ^property[1].valueString = "$.co" 
* #XK "Republic of Kosovo"
* #XK ^property[0].code = #hints 
* #XK ^property[0].valueString = "T^E^B" 
* #XK ^property[1].code = #Relationships 
* #XK ^property[1].valueString = "$.co" 
* #XX "Stateless"
* #XX ^designation[0].language = #de-AT 
* #XX ^designation[0].value = "Staatenlos" 
* #XX ^property[0].code = #hints 
* #XX ^property[0].valueString = "T^E^B" 
* #XX ^property[1].code = #Relationships 
* #XX ^property[1].valueString = "$.co" 
* #YE "Yemen"
* #YE ^designation[0].language = #de-AT 
* #YE ^designation[0].value = "Jemen" 
* #YE ^property[0].code = #hints 
* #YE ^property[0].valueString = "T^E^B" 
* #YE ^property[1].code = #Relationships 
* #YE ^property[1].valueString = "$.co" 
* #YS-SC2-010 "YS-SC2-010"
* #YS-SC2-010 ^designation[0].language = #de-AT 
* #YS-SC2-010 ^designation[0].value = "COVID-19-Impfstoff YS-SC2-010 (Yisheng Biopharma)" 
* #YS-SC2-010 ^property[0].code = #hints 
* #YS-SC2-010 ^property[0].valueString = "T^E^B" 
* #YS-SC2-010 ^property[1].code = #Relationships 
* #YS-SC2-010 ^property[1].valueString = "$.v..mp" 
* #YT "Mayotte"
* #YT ^property[0].code = #hints 
* #YT ^property[0].valueString = "T^E^B" 
* #YT ^property[1].code = #Relationships 
* #YT ^property[1].valueString = "$.co" 
* #YU "Yugoslavia"
* #YU ^designation[0].language = #de-AT 
* #YU ^designation[0].value = "eheml. Yugoslawien" 
* #YU ^property[0].code = #hints 
* #YU ^property[0].valueString = "T^E^B" 
* #YU ^property[1].code = #Relationships 
* #YU ^property[1].valueString = "$.co" 
* #YY "Unknown"
* #YY ^designation[0].language = #de-AT 
* #YY ^designation[0].value = "Unbekannt" 
* #YY ^property[0].code = #hints 
* #YY ^property[0].valueString = "T^E^B" 
* #YY ^property[1].code = #Relationships 
* #YY ^property[1].valueString = "$.co" 
* #Yisheng-Biopharma "Yisheng Biopharma"
* #Yisheng-Biopharma ^property[0].code = #hints 
* #Yisheng-Biopharma ^property[0].valueString = "T^E^B" 
* #Yisheng-Biopharma ^property[1].code = #Relationships 
* #Yisheng-Biopharma ^property[1].valueString = "$.v..ma" 
* #ZA "South Africa"
* #ZA ^designation[0].language = #de-AT 
* #ZA ^designation[0].value = "Südafrika" 
* #ZA ^property[0].code = #hints 
* #ZA ^property[0].valueString = "T^E^B" 
* #ZA ^property[1].code = #Relationships 
* #ZA ^property[1].valueString = "$.co" 
* #ZM "Zambia"
* #ZM ^designation[0].language = #de-AT 
* #ZM ^designation[0].value = "Sambia" 
* #ZM ^property[0].code = #hints 
* #ZM ^property[0].valueString = "T^E^B" 
* #ZM ^property[1].code = #Relationships 
* #ZM ^property[1].valueString = "$.co" 
* #ZW "Zimbabwe"
* #ZW ^designation[0].language = #de-AT 
* #ZW ^designation[0].value = "Simbabwe" 
* #ZW ^property[0].code = #hints 
* #ZW ^property[0].valueString = "T^E^B" 
* #ZW ^property[1].code = #Relationships 
* #ZW ^property[1].valueString = "$.co" 
