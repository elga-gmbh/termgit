Instance: elga-lebensmittelallergene 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-lebensmittelallergene" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-lebensmittelallergene" 
* name = "elga-lebensmittelallergene" 
* title = "ELGA-Lebensmittelallergene" 
* status = #active 
* content = #complete 
* version = "2.0.0+20230131" 
* description = "Liste von Stoffen oder Erzeugnissen, die Allergien oder Unverträglichkeiten auslösen (Quelle: https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:02011R1169-20180101&from=DE#E0017)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.215" 
* date = "2022-11-02" 
* publisher = "ELGA GmbH" 
* count = 14 
* concept[0].code = #EU0001
* concept[0].display = "Glutenhaltiges Getreide"
* concept[0].definition = "Glutenhaltiges Getreide, namentlich Weizen (wie Dinkel und Khorasan-Weizen), Roggen, Gerste, Hafer oder Hybridstämme davon, sowie daraus hergestellte Erzeugnisse, ausgenommen
a) Glukosesirupe auf Weizenbasis einschließlich Dextrose und daraus gewonnene Erzeugnisse, soweit das Verfahren, das sie durchlaufen haben, die Allergenität, die von der EFSA für das entsprechende Erzeugnis ermittelt wurde, aus dem sie gewonnen wurden, wahrscheinlich nicht erhöht,
b) Maltodextrine auf Weizenbasis und daraus gewonnene Erzeugnisse, soweit das Verfahren, das sie durchlaufen haben, die Allergenität, die von der EFSA für das entsprechende Erzeugnis ermittelt wurde, aus dem sie gewonnen wurden, wahrscheinlich nicht erhöht,
c) Glukosesirupe auf Gerstenbasis,
d) Getreide zur Herstellung von alkoholischen Destillaten einschließlich Ethylalkohol landwirtschaftlichen Ursprungs"
* concept[1].code = #EU0002
* concept[1].display = "Krebstiere"
* concept[1].definition = "Krebstiere und daraus gewonnene Erzeugnisse"
* concept[2].code = #EU0003
* concept[2].display = "Eier"
* concept[2].definition = "Eier und daraus gewonnene Erzeugnisse"
* concept[3].code = #EU0004
* concept[3].display = "Fische"
* concept[3].definition = "Fische und daraus gewonnene Erzeugnisse, außer
a) Fischgelatine, die als Trägerstoff für Vitamin- oder Karotinoidzubereitungen verwendet wird,
b) Fischgelatine oder Hausenblase, die als Klärhilfsmittel in Bier und Wein verwendet wird"
* concept[4].code = #EU0005
* concept[4].display = "Erdnüsse"
* concept[4].definition = "Erdnüsse und daraus gewonnene Erzeugnisse"
* concept[5].code = #EU0006
* concept[5].display = "Sojabohnen"
* concept[5].definition = "Sojabohnen und daraus gewonnene Erzeugnisse, außer
a) vollständig raffiniertes Sojabohnenöl und -fett und daraus gewonnene Erzeugnisse, soweit das Verfahren, das sie durchlaufen haben, die Allergenität, die von der EFSA für das entsprechende Erzeugnis ermittelt wurde, aus dem sie gewonnen wurden, wahrscheinlich nicht erhöht,
b) natürliche gemischte Tocopherole (E306), natürliches D-alpha-Tocopherol, natürliches D-alpha-Tocopherolacetat, natürliches D-alpha-Tocopherolsukzinat aus Sojabohnenquellen,
c) aus pflanzlichen Ölen gewonnene Phytosterine und Phytosterinester aus Sojabohnenquellen,
d) aus Pflanzenölsterinen gewonnene Phytostanolester aus Sojabohnenquellen"
* concept[6].code = #EU0007
* concept[6].display = "Milch"
* concept[6].definition = "Milch und daraus gewonnene Erzeugnisse (einschließlich Laktose), außer
a) Molke zur Herstellung von alkoholischen Destillaten einschließlich Ethylalkohol landwirtschaftlichen Ursprungs; 
b) Lactit"
* concept[7].code = #EU0008
* concept[7].display = "Schalenfrüchte"
* concept[7].definition = "Schalenfrüchte, namentlich Mandeln (Amygdalus communis L.), Haselnüsse (Corylus avellana), Walnüsse (Juglans regia), Kaschunüsse (Anacardium occidentale), Pecannüsse (Carya illinoiesis (Wangenh.) K. Koch), Paranüsse (Bertholletia excelsa), Pistazien (Pistacia vera), Macadamia- oder Queenslandnüsse (Macadamia ternifolia) sowie daraus gewonnene Erzeugnisse, außer Nüssen zur Herstellung von alkoholischen Destillaten einschließlich Ethylalkohol landwirtschaftlichen Ursprungs"
* concept[8].code = #EU0009
* concept[8].display = "Sellerie"
* concept[8].definition = "Sellerie und daraus gewonnene Erzeugnisse"
* concept[9].code = #EU0010
* concept[9].display = "Senf"
* concept[9].definition = "Senf und daraus gewonnene Erzeugnisse"
* concept[10].code = #EU0011
* concept[10].display = "Sesamsamen"
* concept[10].definition = "Sesamsamen und daraus gewonnene Erzeugnisse"
* concept[11].code = #EU0012
* concept[11].display = "Schwefeldioxid und Sulphite"
* concept[11].definition = "Schwefeldioxid und Sulphite in Konzentrationen von mehr als 10 mg/kg oder 10 mg/l als insgesamt vorhandenes SO 2 , die für verzehrfertige oder gemäß den Anweisungen des Herstellers in den ursprünglichen Zustand zurückgeführte Erzeugnisse zu berechnen sind"
* concept[12].code = #EU0013
* concept[12].display = "Lupinen"
* concept[12].definition = "Lupinen und daraus gewonnene Erzeugnisse"
* concept[13].code = #EU0014
* concept[13].display = "Weichtiere"
* concept[13].definition = "Weichtiere und daraus gewonnene Erzeugnisse"
