Instance: elga-seitenlokalisation 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-seitenlokalisation" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-seitenlokalisation" 
* name = "elga-seitenlokalisation" 
* title = "ELGA_Seitenlokalisation" 
* status = #active 
* version = "1.1.0+20240325" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.176" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.3.7.1.7"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/sciphox-seitenlokalisation"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = "B"
* compose.include[0].concept[0].display = "beidseits"
* compose.include[0].concept[1].code = "L"
* compose.include[0].concept[1].display = "links"
* compose.include[0].concept[2].code = "M"
* compose.include[0].concept[2].display = "Mittellinienzone"
* compose.include[0].concept[3].code = "R"
* compose.include[0].concept[3].display = "rechts"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/sciphox-seitenlokalisation"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #B
* expansion.contains[0].display = "beidseits"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.3.7.1.7"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/sciphox-seitenlokalisation"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #L
* expansion.contains[1].display = "links"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.3.7.1.7"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/sciphox-seitenlokalisation"
* expansion.contains[2].version = "1.0.0+20230131"
* expansion.contains[2].code = #M
* expansion.contains[2].display = "Mittellinienzone"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.3.7.1.7"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem/sciphox-seitenlokalisation"
* expansion.contains[3].version = "1.0.0+20230131"
* expansion.contains[3].code = #R
* expansion.contains[3].display = "rechts"
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:2.16.840.1.113883.3.7.1.7"
