Instance: elga-entlassungsmanagementa 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-entlassungsmanagementa" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-entlassungsmanagementa" 
* name = "elga-entlassungsmanagementa" 
* title = "ELGA_EntlassungsmanagementA" 
* status = #active 
* version = "1.2.0+20240820" 
* description = "**Description:** Describes the variety in a discharge management disposition (describes the kind of discharge date)

**Beschreibung:** Beschreibt die Möglichkeiten in einer Entlassungsmanagement-Disposition (Beschreibt die Art des Entlassungsdatums)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.53" 
* date = "2024-08-20" 
* publisher = "ELGA GmbH" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.28"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-entlassungsmanagementart"
* compose.include[0].version = "1.0.1+20240129"
* compose.include[0].concept[0].code = "GEPLENTLDAT"
* compose.include[0].concept[0].display = "Geplantes Entlassungsdatum"

* expansion.timestamp = "2024-08-19T10:38:49.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-entlassungsmanagementart"
* expansion.contains[0].version = "1.0.1+20240129"
* expansion.contains[0].code = #GEPLENTLDAT
* expansion.contains[0].display = "Geplantes Entlassungsdatum"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.28"
