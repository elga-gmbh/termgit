Instance: elga-absentorunknownallergies 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-absentorunknownallergies" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-absentorunknownallergies" 
* name = "elga-absentorunknownallergies" 
* title = "ELGA_AbsentOrUnknownAllergies" 
* status = #active 
* version = "3.0.0+20231122" 
* description = "**Description:** Absent Allergy Or Intolerance 

**Beschreibung:** Keine Allergie oder Intoleranz" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.178" 
* date = "2023-11-22" 
* compose.include[0].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* compose.include[0].version = "1.1.0"
* compose.include[0].concept[0].code = #no-allergy-info
* compose.include[0].concept[0].display = "No information about allergies"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "keine Information über Überempfindlichkeiten verfügbar" 
* compose.include[0].concept[1].code = #no-known-allergies
* compose.include[0].concept[1].display = "No known allergies"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "keine Überempfindlichkeiten" 
* compose.include[0].concept[2].code = #no-known-medication-allergies
* compose.include[0].concept[2].display = "No known medication allergies"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "keine Arzneimittelallergien" 
* compose.include[0].concept[3].code = #no-known-environmental-allergies
* compose.include[0].concept[3].display = "No known environmental allergies"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "keine Umweltallergien" 
* compose.include[0].concept[4].code = #no-known-food-allergies
* compose.include[0].concept[4].display = "No known food allergies"
* compose.include[0].concept[4].designation[0].language = #de-AT 
* compose.include[0].concept[4].designation[0].value = "keine Lebensmittelallergien" 

* expansion.timestamp = "2023-11-23T13:49:27.0000Z"

* expansion.contains[0].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* expansion.contains[0].version = "1.1.0"
* expansion.contains[0].code = #no-allergy-info
* expansion.contains[0].display = "No information about allergies"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "keine Information über Überempfindlichkeiten verfügbar" 
* expansion.contains[1].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* expansion.contains[1].version = "1.1.0"
* expansion.contains[1].code = #no-known-allergies
* expansion.contains[1].display = "No known allergies"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "keine Überempfindlichkeiten" 
* expansion.contains[1].contains[0].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* expansion.contains[1].contains[0].version = "1.1.0"
* expansion.contains[1].contains[0].code = #no-known-medication-allergies
* expansion.contains[1].contains[0].display = "No known medication allergies"
* expansion.contains[1].contains[0].designation[0].language = #de-AT 
* expansion.contains[1].contains[0].designation[0].value = "keine Arzneimittelallergien" 
* expansion.contains[1].contains[1].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* expansion.contains[1].contains[1].version = "1.1.0"
* expansion.contains[1].contains[1].code = #no-known-environmental-allergies
* expansion.contains[1].contains[1].display = "No known environmental allergies"
* expansion.contains[1].contains[1].designation[0].language = #de-AT 
* expansion.contains[1].contains[1].designation[0].value = "keine Umweltallergien" 
* expansion.contains[1].contains[2].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* expansion.contains[1].contains[2].version = "1.1.0"
* expansion.contains[1].contains[2].code = #no-known-food-allergies
* expansion.contains[1].contains[2].display = "No known food allergies"
* expansion.contains[1].contains[2].designation[0].language = #de-AT 
* expansion.contains[1].contains[2].designation[0].value = "keine Lebensmittelallergien" 
