<?xml version="1.0" ?>
<ValueSet xmlns="http://hl7.org/fhir">
    <id value="elga-conditionstatuscode"/>
    <meta>
        <profile value="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset"/>
    </meta>
    <url value="https://termgit.elga.gv.at/ValueSet/elga-conditionstatuscode"/>
    <identifier>
        <use value="official"/>
        <system value="urn:ietf:rfc:3986"/>
        <value value="urn:oid:1.2.40.0.34.10.198"/>
    </identifier>
    <version value="2.1.0+20240325"/>
    <name value="elga-conditionstatuscode"/>
    <title value="ELGA_ConditionStatusCode"/>
    <status value="active"/>
    <date value="2024-03-25"/>
    <description value="**Description:** Condition or Problem Status Code&#10;&#10;**Beschreibung:** Value Set für den Status Code eines Problems"/>
    <compose>
        <include>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.4.642.3.155"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/ips-conditionclinicalstatuscodes"/>
            <version value="1.0.0+20230131"/>
            <concept>
                <code value="active"/>
                <display value="Active"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="bestehend"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject is currently experiencing the symptoms of the condition or there is evidence of the condition"/>
                </designation>
            </concept>
            <concept>
                <code value="well-controlled"/>
                <display value="Well controlled"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="gut eingestellt"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject's condition is adequately or well managed such that the recommended  evidence-based clinical outcome targets are met"/>
                </designation>
            </concept>
            <concept>
                <code value="poorly-controlled"/>
                <display value="Poorly controlled"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="mangelhaft eingestellt"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The  subject's condition is inadequately/poorly managed such that the recommended evidence-based clinical outcome targets are not met"/>
                </designation>
            </concept>
            <concept>
                <code value="recurrence"/>
                <display value="Recurrence"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Rezidiv"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject is experiencing a re-occurrence or repeating of a previously resolved condition. Example: recurrence of (previously resolved) urinary tract infection, pancreatitis, cholangitis, conjunctivitis"/>
                </designation>
            </concept>
            <concept>
                <code value="relapse"/>
                <display value="Relapse"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Relaps"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject is experiencing a return of a condition, or signs and symptoms after a period of improvement or remission. Examples: relapse of cancer, multiple sclerosis, rheumatoid arthritis, systemic lupus erythematosus, bipolar disorder, [psychotic relapse of] schizophrenia, etc"/>
                </designation>
            </concept>
            <concept>
                <code value="inactive"/>
                <display value="Inactive"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="nicht mehr bestehend"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject is no longer experiencing the symptoms of the condition or there is no longer evidence of the condition"/>
                </designation>
            </concept>
            <concept>
                <code value="remission"/>
                <display value="Remission"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="in Remission"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject is no longer experiencing the symptoms of the condition, but there is a risk of the symptoms or condition returning"/>
                </designation>
            </concept>
            <concept>
                <code value="resolved"/>
                <display value="Resolved"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="abgeklungen"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject is no longer experiencing the symptoms of the condition and there is a negligible perceived risk of the symptoms returning"/>
                </designation>
            </concept>
        </include>
    </compose>
    <expansion>
        <timestamp value="2024-03-25T06:36:54.0000Z"/>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.4.642.3.155"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/ips-conditionclinicalstatuscodes"/>
            <version value="1.0.0+20230131"/>
            <code value="active"/>
            <display value="Active"/>
            <designation>
                <language value="de-AT"/>
                <value value="bestehend"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="concept_beschreibung"/>
                    <display value="concept_beschreibung"/>
                </use>
                <value value="The subject is currently experiencing the symptoms of the condition or there is evidence of the condition"/>
            </designation>
            <contains>
                <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                    <valueOid value="urn:oid:2.16.840.1.113883.4.642.3.155"/>
                </extension>
                <system value="https://termgit.elga.gv.at/CodeSystem/ips-conditionclinicalstatuscodes"/>
                <version value="1.0.0+20230131"/>
                <code value="well-controlled"/>
                <display value="Well controlled"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="gut eingestellt"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject's condition is adequately or well managed such that the recommended  evidence-based clinical outcome targets are met"/>
                </designation>
            </contains>
            <contains>
                <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                    <valueOid value="urn:oid:2.16.840.1.113883.4.642.3.155"/>
                </extension>
                <system value="https://termgit.elga.gv.at/CodeSystem/ips-conditionclinicalstatuscodes"/>
                <version value="1.0.0+20230131"/>
                <code value="poorly-controlled"/>
                <display value="Poorly controlled"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="mangelhaft eingestellt"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The  subject's condition is inadequately/poorly managed such that the recommended evidence-based clinical outcome targets are not met"/>
                </designation>
            </contains>
            <contains>
                <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                    <valueOid value="urn:oid:2.16.840.1.113883.4.642.3.155"/>
                </extension>
                <system value="https://termgit.elga.gv.at/CodeSystem/ips-conditionclinicalstatuscodes"/>
                <version value="1.0.0+20230131"/>
                <code value="recurrence"/>
                <display value="Recurrence"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Rezidiv"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject is experiencing a re-occurrence or repeating of a previously resolved condition. Example: recurrence of (previously resolved) urinary tract infection, pancreatitis, cholangitis, conjunctivitis"/>
                </designation>
            </contains>
            <contains>
                <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                    <valueOid value="urn:oid:2.16.840.1.113883.4.642.3.155"/>
                </extension>
                <system value="https://termgit.elga.gv.at/CodeSystem/ips-conditionclinicalstatuscodes"/>
                <version value="1.0.0+20230131"/>
                <code value="relapse"/>
                <display value="Relapse"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Relaps"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject is experiencing a return of a condition, or signs and symptoms after a period of improvement or remission. Examples: relapse of cancer, multiple sclerosis, rheumatoid arthritis, systemic lupus erythematosus, bipolar disorder, [psychotic relapse of] schizophrenia, etc"/>
                </designation>
            </contains>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.4.642.3.155"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/ips-conditionclinicalstatuscodes"/>
            <version value="1.0.0+20230131"/>
            <code value="inactive"/>
            <display value="Inactive"/>
            <designation>
                <language value="de-AT"/>
                <value value="nicht mehr bestehend"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="concept_beschreibung"/>
                    <display value="concept_beschreibung"/>
                </use>
                <value value="The subject is no longer experiencing the symptoms of the condition or there is no longer evidence of the condition"/>
            </designation>
            <contains>
                <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                    <valueOid value="urn:oid:2.16.840.1.113883.4.642.3.155"/>
                </extension>
                <system value="https://termgit.elga.gv.at/CodeSystem/ips-conditionclinicalstatuscodes"/>
                <version value="1.0.0+20230131"/>
                <code value="remission"/>
                <display value="Remission"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="in Remission"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject is no longer experiencing the symptoms of the condition, but there is a risk of the symptoms or condition returning"/>
                </designation>
            </contains>
            <contains>
                <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                    <valueOid value="urn:oid:2.16.840.1.113883.4.642.3.155"/>
                </extension>
                <system value="https://termgit.elga.gv.at/CodeSystem/ips-conditionclinicalstatuscodes"/>
                <version value="1.0.0+20230131"/>
                <code value="resolved"/>
                <display value="Resolved"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="abgeklungen"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="The subject is no longer experiencing the symptoms of the condition and there is a negligible perceived risk of the symptoms returning"/>
                </designation>
            </contains>
        </contains>
    </expansion>
</ValueSet>
