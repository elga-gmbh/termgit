Instance: elga-auditparticipantobjecttype 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-auditparticipantobjecttype" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjecttype" 
* name = "elga-auditparticipantobjecttype" 
* title = "ELGA_AuditParticipantObjectType" 
* status = #active 
* content = #complete 
* version = "1.1.0+20240820" 
* description = "Codeliste der ELGA spezifischen Audit Participant Object Type Codes. Der Audit Participant Object Type beschreibt die Art eines Objekts, welches im Audit Event referenziert ist." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.153" 
* date = "2024-08-20" 
* publisher = "ELGA GmbH" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 5 
* #0 "ELGA Objekt"
* #100 "Policy"
* #110 "Kontaktbestätigung"
* #120 "Security Token"
* #130 "Vollmacht"
