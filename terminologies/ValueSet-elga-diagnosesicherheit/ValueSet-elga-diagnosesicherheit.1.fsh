Instance: elga-diagnosesicherheit 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-diagnosesicherheit" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-diagnosesicherheit" 
* name = "elga-diagnosesicherheit" 
* title = "ELGA_Diagnosesicherheit" 
* status = #active 
* version = "1.1.0+20240325" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.168" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.3.7.1.8"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/sciphox-diagnosenzusatz"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #G
* compose.include[0].concept[0].display = "Gesichert"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "Gesicherte Diagnose " 
* compose.include[0].concept[1].code = #V
* compose.include[0].concept[1].display = "Verdacht auf"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[0].value = "Verdachtsdiagnose " 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[1].value = "uncertaintyCode = UN" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/sciphox-diagnosenzusatz"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #G
* expansion.contains[0].display = "Gesichert"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[0].value = "Gesicherte Diagnose " 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.3.7.1.8"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/sciphox-diagnosenzusatz"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #V
* expansion.contains[1].display = "Verdacht auf"
* expansion.contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[0].value = "Verdachtsdiagnose " 
* expansion.contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* expansion.contains[1].designation[1].value = "uncertaintyCode = UN" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.3.7.1.8"
