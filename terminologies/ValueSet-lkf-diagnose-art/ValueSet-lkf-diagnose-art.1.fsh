Instance: lkf-diagnose-art 
InstanceOf: ValueSet 
Usage: #definition 
* id = "lkf-diagnose-art" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/lkf-diagnose-art" 
* name = "lkf-diagnose-art" 
* title = "LKF_Diagnose-Art" 
* status = #active 
* version = "1.1.0+20240325" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.67" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.207"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-art"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #D
* compose.include[0].concept[0].display = "Aktuelle/Behandelte Diagnose"
* compose.include[0].concept[1].code = #V
* compose.include[0].concept[1].display = "Verdachtsdiagnose"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-art"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #D
* expansion.contains[0].display = "Aktuelle/Behandelte Diagnose"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.207"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-art"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #V
* expansion.contains[1].display = "Verdachtsdiagnose"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.207"
