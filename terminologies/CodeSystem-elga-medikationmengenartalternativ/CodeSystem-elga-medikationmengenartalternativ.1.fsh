Instance: elga-medikationmengenartalternativ 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-medikationmengenartalternativ" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-medikationmengenartalternativ" 
* name = "elga-medikationmengenartalternativ" 
* title = "ELGA_MedikationMengenartAlternativ" 
* status = #retired 
* content = #complete 
* version = "1.1.0+20230616" 
* description = "**Description:** Codelist of alternative units of measure (for countable units).

**Beschreibung:** Codeliste für alternative Mengenarten (zählbare Einheiten)." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.2" 
* date = "2024-09-03" 
* count = 2 
* concept[0].code = #{HUB}
* concept[0].display = "Actuation"
* concept[1].code = #{TAB}
* concept[1].display = "Tablet"
