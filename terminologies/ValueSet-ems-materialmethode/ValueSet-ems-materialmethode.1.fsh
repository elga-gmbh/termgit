Instance: ems-materialmethode 
InstanceOf: ValueSet 
Usage: #definition 
* id = "ems-materialmethode" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/ems-materialmethode" 
* name = "ems-materialmethode" 
* title = "EMS_MaterialMethode" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "**Description:** Concepts for EMS methods for specimen extractions

**Beschreibung:** Konzepte für Methode der Materialgewinnung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.16" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.99"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/ems-materialmethode"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #HIST
* compose.include[0].concept[0].display = "Methode Material Histologie"
* compose.include[0].concept[1].code = #KULTUR
* compose.include[0].concept[1].display = "Methode Kultur Material"
* compose.include[0].concept[2].code = #MAT1
* compose.include[0].concept[2].display = "Primary Sample"
* compose.include[0].concept[3].code = #MAT2
* compose.include[0].concept[3].display = "Secondary Sample"
* compose.include[0].concept[4].code = #MIKRO
* compose.include[0].concept[4].display = "Methode Mikroskopie Material"
* compose.include[0].concept[5].code = #NAT
* compose.include[0].concept[5].display = "Methode NAT Material"
* compose.include[0].concept[6].code = #SERO
* compose.include[0].concept[6].display = "Methode Material Serologie"
* compose.include[0].concept[7].code = #VIRUS
* compose.include[0].concept[7].display = "Methode Material Virusnachweis"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/ems-materialmethode"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #HIST
* expansion.contains[0].display = "Methode Material Histologie"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.99"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/ems-materialmethode"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #KULTUR
* expansion.contains[1].display = "Methode Kultur Material"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.99"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/ems-materialmethode"
* expansion.contains[2].version = "1.0.0+20230131"
* expansion.contains[2].code = #MAT1
* expansion.contains[2].display = "Primary Sample"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.99"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem/ems-materialmethode"
* expansion.contains[3].version = "1.0.0+20230131"
* expansion.contains[3].code = #MAT2
* expansion.contains[3].display = "Secondary Sample"
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.99"
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem/ems-materialmethode"
* expansion.contains[4].version = "1.0.0+20230131"
* expansion.contains[4].code = #MIKRO
* expansion.contains[4].display = "Methode Mikroskopie Material"
* expansion.contains[4].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[4].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.99"
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem/ems-materialmethode"
* expansion.contains[5].version = "1.0.0+20230131"
* expansion.contains[5].code = #NAT
* expansion.contains[5].display = "Methode NAT Material"
* expansion.contains[5].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[5].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.99"
* expansion.contains[6].system = "https://termgit.elga.gv.at/CodeSystem/ems-materialmethode"
* expansion.contains[6].version = "1.0.0+20230131"
* expansion.contains[6].code = #SERO
* expansion.contains[6].display = "Methode Material Serologie"
* expansion.contains[6].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[6].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.99"
* expansion.contains[7].system = "https://termgit.elga.gv.at/CodeSystem/ems-materialmethode"
* expansion.contains[7].version = "1.0.0+20230131"
* expansion.contains[7].code = #VIRUS
* expansion.contains[7].display = "Methode Material Virusnachweis"
* expansion.contains[7].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[7].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.99"
