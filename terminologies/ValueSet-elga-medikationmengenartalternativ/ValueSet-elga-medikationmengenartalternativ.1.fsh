Instance: elga-medikationmengenartalternativ 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-medikationmengenartalternativ" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-medikationmengenartalternativ" 
* name = "elga-medikationmengenartalternativ" 
* title = "ELGA_MedikationMengenartAlternativ" 
* status = #retired 
* version = "1.2.0+20240325" 
* description = "ELGA ValueSet für alternative Mengenarten (zählbar)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.67" 
* date = "2024-09-03" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.10.1.4.3.4.3.2"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-medikationmengenartalternativ"
* compose.include[0].version = "1.1.0+20230616"
* compose.include[0].concept[0].code = #{HUB}
* compose.include[0].concept[0].display = "Actuation"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Hub/Hübe" 
* compose.include[0].concept[1].code = #{TAB}
* compose.include[0].concept[1].display = "Tablet"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Tablette" 

* expansion.timestamp = "2024-08-14T09:12:26.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-medikationmengenartalternativ"
* expansion.contains[0].version = "1.1.0+20230616"
* expansion.contains[0].code = #{HUB}
* expansion.contains[0].display = "Actuation"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Hub/Hübe" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.10.1.4.3.4.3.2"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/elga-medikationmengenartalternativ"
* expansion.contains[1].version = "1.1.0+20230616"
* expansion.contains[1].code = #{TAB}
* expansion.contains[1].display = "Tablet"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "Tablette" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.10.1.4.3.4.3.2"
