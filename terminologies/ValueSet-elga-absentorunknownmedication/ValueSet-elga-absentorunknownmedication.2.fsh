Instance: elga-absentorunknownmedication 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-absentorunknownmedication" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-absentorunknownmedication" 
* name = "elga-absentorunknownmedication" 
* title = "ELGA_AbsentOrUnknownMedication" 
* status = #active 
* version = "2.0.0+20231117" 
* description = "Keine Medikation" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.191" 
* date = "2023-11-17" 
* compose.include[0].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* compose.include[0].version = "1.1.0"
* compose.include[0].concept[0].code = "no-medication-info"
* compose.include[0].concept[0].display = "No information about medications"
* compose.include[0].concept[1].code = "no-known-medications"
* compose.include[0].concept[1].display = "No known medications"

* expansion.timestamp = "2023-11-23T13:49:26.0000Z"

* expansion.contains[0].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* expansion.contains[0].version = "1.1.0"
* expansion.contains[0].code = #no-medication-info
* expansion.contains[0].display = "No information about medications"
* expansion.contains[1].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* expansion.contains[1].version = "1.1.0"
* expansion.contains[1].code = #no-known-medications
* expansion.contains[1].display = "No known medications"
