Instance: elga-confidentiality 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-confidentiality" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-confidentiality" 
* name = "elga-confidentiality" 
* title = "ELGA_Confidentiality" 
* status = #active 
* version = "1.3.0+20240820" 
* description = "**Description:** This Value Set is used to declare confidentiality for the entire CDA-Document. Due to the fact that all medical documents are confidential and the access rules within ELGA are controlled by the ''Access Control''- set of regulations, all documents are assigned ConfidentialityCode N (normal). This is a limited subset of the original codelist.

**Beschreibung:** Das Value Set wird verwendet, um die Vertraulichkeit für das gesamte CDA-Dokument anzugeben. Da alle medizinischen Dokumente vertraulich sind und die Zugriffsregeln in ELGA außerhalb des Dokuments durch das Berechtigungsregelwerk gesteuert werden, bekommen alle Dokumente den ConfidentialityCode N (normal).     Eingeschränktes Subset der originalen Codeliste." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.7" 
* date = "2024-08-20" 
* publisher = "ELGA GmbH" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* compose.include[0].version = "2.2.0"
* compose.include[0].concept[0].code = "L"
* compose.include[0].concept[0].display = "low"
* compose.include[0].concept[1].code = "M"
* compose.include[0].concept[1].display = "moderate"
* compose.include[0].concept[2].code = "N"
* compose.include[0].concept[2].display = "normal"
* compose.include[0].concept[3].code = "R"
* compose.include[0].concept[3].display = "restricted"
* compose.include[0].concept[4].code = "U"
* compose.include[0].concept[4].display = "unrestricted"
* compose.include[0].concept[5].code = "V"
* compose.include[0].concept[5].display = "very restricted"

* expansion.timestamp = "2024-09-04T09:10:21.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* expansion.contains[0].version = "2.2.0"
* expansion.contains[0].code = #L
* expansion.contains[0].display = "low"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
* expansion.contains[1].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* expansion.contains[1].version = "2.2.0"
* expansion.contains[1].code = #M
* expansion.contains[1].display = "moderate"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
* expansion.contains[2].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* expansion.contains[2].version = "2.2.0"
* expansion.contains[2].code = #N
* expansion.contains[2].display = "normal"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
* expansion.contains[3].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* expansion.contains[3].version = "2.2.0"
* expansion.contains[3].code = #R
* expansion.contains[3].display = "restricted"
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
* expansion.contains[4].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* expansion.contains[4].version = "2.2.0"
* expansion.contains[4].code = #U
* expansion.contains[4].display = "unrestricted"
* expansion.contains[4].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[4].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
* expansion.contains[5].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* expansion.contains[5].version = "2.2.0"
* expansion.contains[5].code = #V
* expansion.contains[5].display = "very restricted"
* expansion.contains[5].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[5].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
