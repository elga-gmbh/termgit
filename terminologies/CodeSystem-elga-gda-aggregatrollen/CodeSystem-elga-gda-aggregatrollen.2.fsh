Instance: elga-gda-aggregatrollen 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-gda-aggregatrollen" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen" 
* name = "elga-gda-aggregatrollen" 
* title = "ELGA_GDA_Aggregatrollen" 
* status = #active 
* content = #complete 
* version = "2.3.0+20250226" 
* description = "ELGA Codeliste für GDA Aggregatrollen (höchste Hierarchieebene)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.3" 
* date = "2025-02-26" 
* publisher = "ELGA GmbH" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 21 
* #700 "Ärztin/Arzt"
* #701 "Zahnärztin/Zahnarzt"
* #702 "Krankenanstalt"
* #703 "Einrichtung der Pflege"
* #704 "Apotheke"
* #705 "ELGA-Beratung"
* #706 "ELGA-Ombudsstelle"
* #707 "eHealth-Servicestelle"
* #716 "Amtsärztin/Amtsarzt"
* #717 "Korrekturberechtigte Person"
* #718 "Krisenmanagerin/Krisenmanager"
* #719 "Auswertungsberechtigte Person"
* #720 "Verrechnungsberechtigte Person"
* #721 "Arbeitsmedizin"
* #722 "Hebamme"
* #723 "Straf- und Maßnahmenvollzug"
* #724 "Labor und Pathologie"
* #725 "EMS/EPI-Service"
* #726 "Diplomierte Gesundheits- und Krankenpflegerin/Diplomierter Gesundheits- und Krankenpfleger"
* #727 "NCPeH cross-border services"
* #728 "Rettungsdienst"
