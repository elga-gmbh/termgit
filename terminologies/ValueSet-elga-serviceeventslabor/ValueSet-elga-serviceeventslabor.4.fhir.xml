<?xml version="1.0" ?>
<ValueSet xmlns="http://hl7.org/fhir">
    <id value="elga-serviceeventslabor"/>
    <meta>
        <profile value="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset"/>
    </meta>
    <url value="https://termgit.elga.gv.at/ValueSet/elga-serviceeventslabor"/>
    <identifier>
        <use value="official"/>
        <system value="urn:ietf:rfc:3986"/>
        <value value="urn:oid:1.2.40.0.34.10.22"/>
    </identifier>
    <version value="1.3.0+20240820"/>
    <name value="elga-serviceeventslabor"/>
    <title value="ELGA_ServiceEventsLabor"/>
    <status value="active"/>
    <date value="2024-08-20"/>
    <publisher value="ELGA GmbH"/>
    <contact>
        <name value="https://www.elga.gv.at"/>
        <telecom>
            <system value="url"/>
            <value value="https://www.elga.gv.at"/>
        </telecom>
    </contact>
    <description value="**Description:** Set of valid ServiceEvent codes for the laboratory report (this set does not display a hierarchical structure). Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).&#10;&#10;**Beschreibung:** Codes der erlaubten ServiceEvents für den Laborbefund. (nicht hierarchische Liste). Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden."/>
    <compose>
        <include>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <concept>
                <code value="1"/>
                <display value="Allgemeiner Laborbefund"/>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Nur für EIS Basic Befunde"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="Entspricht LOINC 26436-6 (Laboratory studies)"/>
                </designation>
            </concept>
            <concept>
                <code value="10"/>
                <display value="Probeninformation"/>
            </concept>
            <concept>
                <code value="100"/>
                <display value="Transfusions-/Transplantationsdiagnostik/Immungenetik"/>
            </concept>
            <concept>
                <code value="1000"/>
                <display value="Medikamente"/>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="Entspricht LOINC 18721-1 (Therapeutic drug monitoring studies)"/>
                </designation>
            </concept>
            <concept>
                <code value="1100"/>
                <display value="Infektionsdiagnostik"/>
            </concept>
            <concept>
                <code value="1300"/>
                <display value="Autoimmundiagnostik"/>
            </concept>
            <concept>
                <code value="1400"/>
                <display value="Urindiagnostik"/>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="Entspricht LOINC 18729-4 (Urinalysis studies)"/>
                </designation>
            </concept>
            <concept>
                <code value="1500"/>
                <display value="Stuhldiagnostik"/>
            </concept>
            <concept>
                <code value="1600"/>
                <display value="Liquordiagnostik"/>
            </concept>
            <concept>
                <code value="1800"/>
                <display value="Allergiediagnostik"/>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="Entspricht LOINC 18716-1 (Allergy studies)"/>
                </designation>
            </concept>
            <concept>
                <code value="20"/>
                <display value="Befundbewertung"/>
            </concept>
            <concept>
                <code value="200"/>
                <display value="Blutgasanalytik"/>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="Entspricht LOINC 18767-4 (Blood gas studies)"/>
                </designation>
            </concept>
            <concept>
                <code value="2300"/>
                <display value="Genetische Diagnostik"/>
            </concept>
            <concept>
                <code value="2500"/>
                <display value="Sonstige"/>
            </concept>
            <concept>
                <code value="300"/>
                <display value="Hämatologie"/>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="Entspricht LOINC 18723-7 (Hematology studies)"/>
                </designation>
            </concept>
            <concept>
                <code value="400"/>
                <display value="Gerinnung/Hämostaseologie"/>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="Entspricht LOINC 18720-3 (Coagulation studies)"/>
                </designation>
            </concept>
            <concept>
                <code value="500"/>
                <display value="Klinische Chemie/Proteindiagnostik"/>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="Entspricht LOINC 18719-5 (Chemistry studies)"/>
                </designation>
            </concept>
            <concept>
                <code value="600"/>
                <display value="Hormone/Vitamine/Tumormarker"/>
            </concept>
            <concept>
                <code value="900"/>
                <display value="Toxikologie"/>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="Entspricht LOINC 18728-6 (Toxicology studies)"/>
                </designation>
            </concept>
        </include>
        <include>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.1"/>
            </extension>
            <system value="http://loinc.org"/>
            <version value="2.75"/>
            <concept>
                <code value="10164-2"/>
                <display value="History of Present illness Narrative"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Anamnese"/>
                </designation>
            </concept>
            <concept>
                <code value="18725-2"/>
                <display value="Microbiology studies (set)"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Microbiology studies"/>
                </designation>
            </concept>
            <concept>
                <code value="46239-0"/>
                <display value="Chief complaint+Reason for visit Narrative"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Überweisungsgrund"/>
                </designation>
            </concept>
        </include>
        <include>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.96"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
            <version value="1.6.0+20231108"/>
            <concept>
                <code value="108262000"/>
                <display value="Molekularer Erregernachweis"/>
            </concept>
            <concept>
                <code value="15220000"/>
                <display value="Weitere Analysen"/>
            </concept>
            <concept>
                <code value="395538009"/>
                <display value="Mikroskopie"/>
            </concept>
            <concept>
                <code value="400999005"/>
                <display value="Angeforderte Untersuchung"/>
            </concept>
            <concept>
                <code value="446394004"/>
                <display value="Kultureller Erregernachweis"/>
            </concept>
            <concept>
                <code value="722143004"/>
                <display value="Infektionsserologie"/>
            </concept>
        </include>
        <include>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.40"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-sections"/>
            <version value="1.0.1+20240129"/>
            <concept>
                <code value="BEIL"/>
                <display value="Beilagen"/>
            </concept>
        </include>
    </compose>
    <expansion>
        <timestamp value="2024-09-04T09:10:21.0000Z"/>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="1"/>
            <display value="Allgemeiner Laborbefund"/>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="concept_beschreibung"/>
                    <display value="concept_beschreibung"/>
                </use>
                <value value="Nur für EIS Basic Befunde"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="hinweise"/>
                    <display value="hinweise"/>
                </use>
                <value value="Entspricht LOINC 26436-6 (Laboratory studies)"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="10"/>
            <display value="Probeninformation"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="100"/>
            <display value="Transfusions-/Transplantationsdiagnostik/Immungenetik"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="1000"/>
            <display value="Medikamente"/>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="hinweise"/>
                    <display value="hinweise"/>
                </use>
                <value value="Entspricht LOINC 18721-1 (Therapeutic drug monitoring studies)"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="1100"/>
            <display value="Infektionsdiagnostik"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="1300"/>
            <display value="Autoimmundiagnostik"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="1400"/>
            <display value="Urindiagnostik"/>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="hinweise"/>
                    <display value="hinweise"/>
                </use>
                <value value="Entspricht LOINC 18729-4 (Urinalysis studies)"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="1500"/>
            <display value="Stuhldiagnostik"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="1600"/>
            <display value="Liquordiagnostik"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="1800"/>
            <display value="Allergiediagnostik"/>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="hinweise"/>
                    <display value="hinweise"/>
                </use>
                <value value="Entspricht LOINC 18716-1 (Allergy studies)"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="20"/>
            <display value="Befundbewertung"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="200"/>
            <display value="Blutgasanalytik"/>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="hinweise"/>
                    <display value="hinweise"/>
                </use>
                <value value="Entspricht LOINC 18767-4 (Blood gas studies)"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="2300"/>
            <display value="Genetische Diagnostik"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="2500"/>
            <display value="Sonstige"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="300"/>
            <display value="Hämatologie"/>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="hinweise"/>
                    <display value="hinweise"/>
                </use>
                <value value="Entspricht LOINC 18723-7 (Hematology studies)"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="400"/>
            <display value="Gerinnung/Hämostaseologie"/>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="hinweise"/>
                    <display value="hinweise"/>
                </use>
                <value value="Entspricht LOINC 18720-3 (Coagulation studies)"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="500"/>
            <display value="Klinische Chemie/Proteindiagnostik"/>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="hinweise"/>
                    <display value="hinweise"/>
                </use>
                <value value="Entspricht LOINC 18719-5 (Chemistry studies)"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="600"/>
            <display value="Hormone/Vitamine/Tumormarker"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.11"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
            <version value="1.2.1+20240129"/>
            <code value="900"/>
            <display value="Toxikologie"/>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="hinweise"/>
                    <display value="hinweise"/>
                </use>
                <value value="Entspricht LOINC 18728-6 (Toxicology studies)"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.1"/>
            </extension>
            <system value="http://loinc.org"/>
            <version value="2.75"/>
            <code value="10164-2"/>
            <display value="History of Present illness Narrative"/>
            <designation>
                <language value="de-AT"/>
                <value value="Anamnese"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.1"/>
            </extension>
            <system value="http://loinc.org"/>
            <version value="2.75"/>
            <code value="18725-2"/>
            <display value="Microbiology studies (set)"/>
            <designation>
                <language value="de-AT"/>
                <value value="Microbiology studies"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.1"/>
            </extension>
            <system value="http://loinc.org"/>
            <version value="2.75"/>
            <code value="46239-0"/>
            <display value="Chief complaint+Reason for visit Narrative"/>
            <designation>
                <language value="de-AT"/>
                <value value="Überweisungsgrund"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.96"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
            <version value="1.6.0+20231108"/>
            <code value="108262000"/>
            <display value="Molekularer Erregernachweis"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.96"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
            <version value="1.6.0+20231108"/>
            <code value="15220000"/>
            <display value="Weitere Analysen"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.96"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
            <version value="1.6.0+20231108"/>
            <code value="395538009"/>
            <display value="Mikroskopie"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.96"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
            <version value="1.6.0+20231108"/>
            <code value="400999005"/>
            <display value="Angeforderte Untersuchung"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.96"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
            <version value="1.6.0+20231108"/>
            <code value="446394004"/>
            <display value="Kultureller Erregernachweis"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:2.16.840.1.113883.6.96"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
            <version value="1.6.0+20231108"/>
            <code value="722143004"/>
            <display value="Infektionsserologie"/>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.40.0.34.5.40"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/elga-sections"/>
            <version value="1.0.1+20240129"/>
            <code value="BEIL"/>
            <display value="Beilagen"/>
        </contains>
    </expansion>
</ValueSet>
