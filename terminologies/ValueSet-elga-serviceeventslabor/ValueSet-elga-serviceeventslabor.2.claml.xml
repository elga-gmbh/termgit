<ClaML version="2.0.0">
    <Meta name="titleLong" value="ELGA_ServiceEventsLabor"/>
    <Meta name="resource" value="ValueSet"/>
    <Meta name="id" value="elga-serviceeventslabor"/>
    <Meta name="profile" value="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset"/>
    <Meta name="url" value="https://termgit.elga.gv.at/ValueSet/elga-serviceeventslabor"/>
    <Meta name="preliminary" value="false"/>
    <Meta name="status" value="active"/>
    <Meta name="description_eng" value="Set of valid ServiceEvent codes for the laboratory report (this set does not display a hierarchical structure). Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS)."/>
    <Meta name="description" value="Codes der erlaubten ServiceEvents für den Laborbefund. (nicht hierarchische Liste). Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden."/>
    <Meta name="website" value="https://www.elga.gv.at"/>
    <Meta name="count" value="29"/>
    <Meta name="extension" value="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid|oid"/>
    <Identifier uid="1.2.40.0.34.10.22"/>
    <Title name="elga-serviceeventslabor" version="1.3.0+20240820" date="2024-08-20">ELGA_ServiceEventsLabor</Title>
    <Authors>
        <Author name="ELGA GmbH"></Author>
        <Author name="https://www.elga.gv.at">url^https://www.elga.gv.at^^^^</Author>
    </Authors>
    <ClassKinds>
        <ClassKind name="code"/>
    </ClassKinds>
    <RubricKinds>
        <RubricKind name="preferred"/>
    </RubricKinds>
    <Class code="1">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_HINTS" value="Entspricht LOINC 26436-6 (Laboratory studies)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Allgemeiner Laborbefund</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^concept_beschreibung^concept_beschreibung^|Nur für EIS Basic Befunde</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^hinweise^hinweise^|Entspricht LOINC 26436-6 (Laboratory studies)</Label>
        </Rubric>
    </Class>
    <Class code="10">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Probeninformation</Label>
        </Rubric>
    </Class>
    <Class code="100">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Transfusions-/Transplantationsdiagnostik/Immungenetik</Label>
        </Rubric>
    </Class>
    <Class code="1000">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_HINTS" value="Entspricht LOINC 18721-1 (Therapeutic drug monitoring studies)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Medikamente</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^hinweise^hinweise^|Entspricht LOINC 18721-1 (Therapeutic drug monitoring studies)</Label>
        </Rubric>
    </Class>
    <Class code="1100">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Infektionsdiagnostik</Label>
        </Rubric>
    </Class>
    <Class code="1300">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Autoimmundiagnostik</Label>
        </Rubric>
    </Class>
    <Class code="1400">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_HINTS" value="Entspricht LOINC 18729-4 (Urinalysis studies)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Urindiagnostik</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^hinweise^hinweise^|Entspricht LOINC 18729-4 (Urinalysis studies)</Label>
        </Rubric>
    </Class>
    <Class code="1500">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Stuhldiagnostik</Label>
        </Rubric>
    </Class>
    <Class code="1600">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Liquordiagnostik</Label>
        </Rubric>
    </Class>
    <Class code="1800">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_HINTS" value="Entspricht LOINC 18716-1 (Allergy studies)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Allergiediagnostik</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^hinweise^hinweise^|Entspricht LOINC 18716-1 (Allergy studies)</Label>
        </Rubric>
    </Class>
    <Class code="20">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Befundbewertung</Label>
        </Rubric>
    </Class>
    <Class code="200">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_HINTS" value="Entspricht LOINC 18767-4 (Blood gas studies)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Blutgasanalytik</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^hinweise^hinweise^|Entspricht LOINC 18767-4 (Blood gas studies)</Label>
        </Rubric>
    </Class>
    <Class code="2300">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Genetische Diagnostik</Label>
        </Rubric>
    </Class>
    <Class code="2500">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Sonstige</Label>
        </Rubric>
    </Class>
    <Class code="300">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_HINTS" value="Entspricht LOINC 18723-7 (Hematology studies)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Hämatologie</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^hinweise^hinweise^|Entspricht LOINC 18723-7 (Hematology studies)</Label>
        </Rubric>
    </Class>
    <Class code="400">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_HINTS" value="Entspricht LOINC 18720-3 (Coagulation studies)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Gerinnung/Hämostaseologie</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^hinweise^hinweise^|Entspricht LOINC 18720-3 (Coagulation studies)</Label>
        </Rubric>
    </Class>
    <Class code="500">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_HINTS" value="Entspricht LOINC 18719-5 (Chemistry studies)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Klinische Chemie/Proteindiagnostik</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^hinweise^hinweise^|Entspricht LOINC 18719-5 (Chemistry studies)</Label>
        </Rubric>
    </Class>
    <Class code="600">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Hormone/Vitamine/Tumormarker</Label>
        </Rubric>
    </Class>
    <Class code="900">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung"/>
        <Meta name="codeSystemVersion" value="1.2.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_HINTS" value="Entspricht LOINC 18728-6 (Toxicology studies)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.11"/>
        <Rubric kind="preferred">
            <Label>Toxikologie</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^hinweise^hinweise^|Entspricht LOINC 18728-6 (Toxicology studies)</Label>
        </Rubric>
    </Class>
    <Class code="10164-2">
        <Meta name="codeSystem" value="http://loinc.org"/>
        <Meta name="codeSystemVersion" value="2.75"/>
        <Meta name="TS_ATTRIBUTE_MEANING" value="Anamnese"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:2.16.840.1.113883.6.1"/>
        <Rubric kind="preferred">
            <Label>History of Present illness Narrative</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|Anamnese</Label>
        </Rubric>
    </Class>
    <Class code="18725-2">
        <Meta name="codeSystem" value="http://loinc.org"/>
        <Meta name="codeSystemVersion" value="2.75"/>
        <Meta name="TS_ATTRIBUTE_MEANING" value="Microbiology studies"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:2.16.840.1.113883.6.1"/>
        <Rubric kind="preferred">
            <Label>Microbiology studies (set)</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|Microbiology studies</Label>
        </Rubric>
    </Class>
    <Class code="46239-0">
        <Meta name="codeSystem" value="http://loinc.org"/>
        <Meta name="codeSystemVersion" value="2.75"/>
        <Meta name="TS_ATTRIBUTE_MEANING" value="Überweisungsgrund"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:2.16.840.1.113883.6.1"/>
        <Rubric kind="preferred">
            <Label>Chief complaint+Reason for visit Narrative</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|Überweisungsgrund</Label>
        </Rubric>
    </Class>
    <Class code="108262000">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
        <Meta name="codeSystemVersion" value="1.6.0+20231108"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:2.16.840.1.113883.6.96"/>
        <Rubric kind="preferred">
            <Label>Molekularer Erregernachweis</Label>
        </Rubric>
    </Class>
    <Class code="15220000">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
        <Meta name="codeSystemVersion" value="1.6.0+20231108"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:2.16.840.1.113883.6.96"/>
        <Rubric kind="preferred">
            <Label>Weitere Analysen</Label>
        </Rubric>
    </Class>
    <Class code="395538009">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
        <Meta name="codeSystemVersion" value="1.6.0+20231108"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:2.16.840.1.113883.6.96"/>
        <Rubric kind="preferred">
            <Label>Mikroskopie</Label>
        </Rubric>
    </Class>
    <Class code="400999005">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
        <Meta name="codeSystemVersion" value="1.6.0+20231108"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:2.16.840.1.113883.6.96"/>
        <Rubric kind="preferred">
            <Label>Angeforderte Untersuchung</Label>
        </Rubric>
    </Class>
    <Class code="446394004">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
        <Meta name="codeSystemVersion" value="1.6.0+20231108"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:2.16.840.1.113883.6.96"/>
        <Rubric kind="preferred">
            <Label>Kultureller Erregernachweis</Label>
        </Rubric>
    </Class>
    <Class code="722143004">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"/>
        <Meta name="codeSystemVersion" value="1.6.0+20231108"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:2.16.840.1.113883.6.96"/>
        <Rubric kind="preferred">
            <Label>Infektionsserologie</Label>
        </Rubric>
    </Class>
    <Class code="BEIL">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/elga-sections"/>
        <Meta name="codeSystemVersion" value="1.0.1+20240129"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.40.0.34.5.40"/>
        <Rubric kind="preferred">
            <Label>Beilagen</Label>
        </Rubric>
    </Class>
</ClaML>
