Instance: rads-thyroidimaging 
InstanceOf: ValueSet 
Usage: #definition 
* id = "rads-thyroidimaging" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/rads-thyroidimaging" 
* name = "rads-thyroidimaging" 
* title = "RADS_ThyroidImaging" 
* status = #draft 
* version = "0.2.0+20240820" 
* description = "**Description:** Value set for the diagnostic imaging report (v3). The value set will remain in draft status until the codes will be replaced by SNOMED CT codes (presumably with Austrian extension February 2025 release). Users are advised not to use the value set before it enters the active stage. Note: The Reporting and Data Systems (RADS) assessment scales are a registered trademark of the American College of Radiology.

**Beschreibung:** Value-Set für den Befund bildgebende Diagnostik (V3). Das Value-Set verbleibt im Entwurf-Status "draft", bis die Codes durch SNOMED-CT-Codes ersetzt werden (voraussichtlich mit dem Austrian-Extension-Release im Februar 2025). Den Benutzer:innen wird empfohlen, das Value-Set nicht zu nutzen, solange es noch nicht aktiviert ist (Status "active"). Achtung: Die "Reporting and Data Systems (RADS)"-Skalen sind eine eingetragene Marke des American College of Radiology." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.94" 
* date = "2024-08-20" 
* publisher = "American College of Radiology" 
* contact[0].name = "American College of Radiology" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.acr.org/" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.76"
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/ACR"
* compose.include[0].version = "2.0.1"
* compose.include[0].concept[0].code = #TR1
* compose.include[0].concept[0].display = "TR1"
* compose.include[0].concept[0].designation[0].language = #en-US 
* compose.include[0].concept[0].designation[0].value = "Thyroid assessment (Category 1) - Benign" 
* compose.include[0].concept[1].code = #TR2
* compose.include[0].concept[1].display = "TR2"
* compose.include[0].concept[1].designation[0].language = #en-US 
* compose.include[0].concept[1].designation[0].value = "Thyroid assessment (Category 2) - Not Suspicious" 
* compose.include[0].concept[2].code = #TR3
* compose.include[0].concept[2].display = "TR3"
* compose.include[0].concept[2].designation[0].language = #en-US 
* compose.include[0].concept[2].designation[0].value = "Thyroid assessment (Category 3) - Mildly Suspicious" 
* compose.include[0].concept[3].code = #TR4
* compose.include[0].concept[3].display = "TR4"
* compose.include[0].concept[3].designation[0].language = #en-US 
* compose.include[0].concept[3].designation[0].value = "Thyroid assessment (Category 4) - Moderately Suspicious" 
* compose.include[0].concept[4].code = #TR5
* compose.include[0].concept[4].display = "TR5"
* compose.include[0].concept[4].designation[0].language = #en-US 
* compose.include[0].concept[4].designation[0].value = "Thyroid assessment (Category 5) - Highly Suspicious" 

* expansion.timestamp = "2024-09-04T09:10:21.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[0].version = "2.0.1"
* expansion.contains[0].code = #TR1
* expansion.contains[0].display = "TR1"
* expansion.contains[0].designation[0].language = #en-US 
* expansion.contains[0].designation[0].value = "Thyroid assessment (Category 1) - Benign" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.76"
* expansion.contains[1].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[1].version = "2.0.1"
* expansion.contains[1].code = #TR2
* expansion.contains[1].display = "TR2"
* expansion.contains[1].designation[0].language = #en-US 
* expansion.contains[1].designation[0].value = "Thyroid assessment (Category 2) - Not Suspicious" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.76"
* expansion.contains[2].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[2].version = "2.0.1"
* expansion.contains[2].code = #TR3
* expansion.contains[2].display = "TR3"
* expansion.contains[2].designation[0].language = #en-US 
* expansion.contains[2].designation[0].value = "Thyroid assessment (Category 3) - Mildly Suspicious" 
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.76"
* expansion.contains[3].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[3].version = "2.0.1"
* expansion.contains[3].code = #TR4
* expansion.contains[3].display = "TR4"
* expansion.contains[3].designation[0].language = #en-US 
* expansion.contains[3].designation[0].value = "Thyroid assessment (Category 4) - Moderately Suspicious" 
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.76"
* expansion.contains[4].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[4].version = "2.0.1"
* expansion.contains[4].code = #TR5
* expansion.contains[4].display = "TR5"
* expansion.contains[4].designation[0].language = #en-US 
* expansion.contains[4].designation[0].value = "Thyroid assessment (Category 5) - Highly Suspicious" 
* expansion.contains[4].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[4].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.76"
