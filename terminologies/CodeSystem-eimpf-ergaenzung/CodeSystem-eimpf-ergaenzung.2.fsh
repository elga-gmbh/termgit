Instance: eimpf-ergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "eimpf-ergaenzung" 
* url = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung" 
* name = "eimpf-ergaenzung" 
* title = "eImpf_Ergaenzung" 
* status = #active 
* content = #complete 
* version = "2.27.0+20250304" 
* description = "**Description:** Supplementary codes for the e-vaccination  **Beschreibung:** Ergänzende Codes für den e-Impfpass" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.183" 
* date = "2025-03-04" 
* publisher = "ELGA GmbH" 
* contact[0].name = "https://www.elga.gv.at/" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at/" 
* count = 212 
* property[0].code = #status 
* property[0].type = #code 
* #B "Auffrischung"
* #B.I "Auffrischungsserie Teil 1"
* #B.II "Auffrischungsserie Teil 2"
* #D1 "Dosis 1"
* #D2 "Dosis 2"
* #D3 "Dosis 3"
* #D4 "Dosis 4"
* #D5 "Dosis 5"
* #D6 "Dosis 6"
* #D7 "Dosis 7"
* #D8 "Dosis 8"
* #D9 "Dosis 9"
* #DGC-R "Genesungs-Zertifikat"
* #DGC-R ^property[0].code = #status 
* #DGC-R ^property[0].valueCode = #retired 
* #DGC-T "Test-Zertifikat"
* #DGC-T ^property[0].code = #status 
* #DGC-T ^property[0].valueCode = #retired 
* #DGC-V "Impf-Zertifikat"
* #DGC-V ^property[0].code = #status 
* #DGC-V ^property[0].valueCode = #retired 
* #IG1 "Indikationsimpfung für Risikogruppe"
* #IG2 "Wiederholungsimpfung aufgrund medizinischer Indikation"
* #IS001 "Bildungseinrichtung"
* #IS002 "Arbeitsplatz/Betrieb"
* #IS003 "Krankenhaus inkl. Kur- und Rehaeinrichtungen"
* #IS004 "Ordination"
* #IS005 "Öffentliche Impfstelle"
* #IS006 "Wohnbereich"
* #IS007 "Öffentliche Impfstraße / Impfbus"
* #IS008 "Betreute Wohneinrichtung"
* #IS009 "Impfprogramm"
* #IS010 "Kostenfreies Impfprogramm"
* #IS010 ^definition = Kostenfreies Impfprogramm des Bundes, der Bundesländer und der Sozialversicherung
* #IS011 "Öffentliches Impfprogramm (ÖIP) Influenza"
* #IS011 ^definition = Öffentliches Impfprogramm (ÖIP) Influenza des Bundes, der Bundesländer und der Sozialversicherung
* #SCHEMA001 "Kombinationsschema Di-Te-Pert-HiB-IPV-HepB, Erstimpfung bis 1 Jahr"
* #SCHEMA001 ^property[0].code = #status 
* #SCHEMA001 ^property[0].valueCode = #retired 
* #SCHEMA002 "Kombinationsschema Di-Te-Pert-HiB-IPV-HepB, Erstimpfung ab 1 Jahr"
* #SCHEMA002 ^property[0].code = #status 
* #SCHEMA002 ^property[0].valueCode = #retired 
* #SCHEMA003 "Kombinationsschema Di-Te-Pert-IPV-HepB, Erstimpfung ab 6 Jahren"
* #SCHEMA003 ^property[0].code = #status 
* #SCHEMA003 ^property[0].valueCode = #retired 
* #SCHEMA004 "Kombinationsschema Di-Te-Pert-HiB-IPV-HepB, Indikation"
* #SCHEMA004 ^property[0].code = #status 
* #SCHEMA004 ^property[0].valueCode = #retired 
* #SCHEMA005 "FSME Grundschema, FSME-Immun"
* #SCHEMA005 ^property[0].code = #status 
* #SCHEMA005 ^property[0].valueCode = #retired 
* #SCHEMA006 "FSME Grundschema, Encepur"
* #SCHEMA006 ^property[0].code = #status 
* #SCHEMA006 ^property[0].valueCode = #retired 
* #SCHEMA007 "FSME Grundschema, FSME-Immun, Erstimpfung bis 1 Jahr"
* #SCHEMA007 ^property[0].code = #status 
* #SCHEMA007 ^property[0].valueCode = #retired 
* #SCHEMA008 "FSME Grundschema, Encepur, Erstimpfung bis 1 Jahr"
* #SCHEMA008 ^property[0].code = #status 
* #SCHEMA008 ^property[0].valueCode = #retired 
* #SCHEMA009 "FSME Schnellschema, FSME-Immun"
* #SCHEMA009 ^property[0].code = #status 
* #SCHEMA009 ^property[0].valueCode = #retired 
* #SCHEMA010 "FSME Schnellschema, Encepur"
* #SCHEMA010 ^property[0].code = #status 
* #SCHEMA010 ^property[0].valueCode = #retired 
* #SCHEMA011 "Haemophilus influenzae Typ B Indikationsschema"
* #SCHEMA011 ^property[0].code = #status 
* #SCHEMA011 ^property[0].valueCode = #retired 
* #SCHEMA012 "Hepatitis A Monokomponente Indikationsschema"
* #SCHEMA012 ^property[0].code = #status 
* #SCHEMA012 ^property[0].valueCode = #retired 
* #SCHEMA013 "Hepatitis AB Grundschema"
* #SCHEMA013 ^property[0].code = #status 
* #SCHEMA013 ^property[0].valueCode = #retired 
* #SCHEMA014 "Hepatitis A Monokomponente Grundschema"
* #SCHEMA014 ^property[0].code = #status 
* #SCHEMA014 ^property[0].valueCode = #retired 
* #SCHEMA015 "Hepatitis AB Schnellschema"
* #SCHEMA015 ^property[0].code = #status 
* #SCHEMA015 ^property[0].valueCode = #retired 
* #SCHEMA016 "Hepatitis A & Typhus Grundschema"
* #SCHEMA016 ^property[0].code = #status 
* #SCHEMA016 ^property[0].valueCode = #retired 
* #SCHEMA017 "Hepatitis B Monokomponente Grundschema"
* #SCHEMA017 ^property[0].code = #status 
* #SCHEMA017 ^property[0].valueCode = #retired 
* #SCHEMA019 "Hepatitis B Non-/Low-Responder Grundschema"
* #SCHEMA019 ^property[0].code = #status 
* #SCHEMA019 ^property[0].valueCode = #retired 
* #SCHEMA020 "HPV Grundschema"
* #SCHEMA020 ^property[0].code = #status 
* #SCHEMA020 ^property[0].valueCode = #retired 
* #SCHEMA021 "HPV Grundschema, ab 21 Jahren"
* #SCHEMA021 ^property[0].code = #status 
* #SCHEMA021 ^property[0].valueCode = #retired 
* #SCHEMA022 "HPV Indikationsschema"
* #SCHEMA022 ^property[0].code = #status 
* #SCHEMA022 ^property[0].valueCode = #retired 
* #SCHEMA023 "Influenza Grundschema, Erstimpfung Kinder"
* #SCHEMA023 ^property[0].code = #status 
* #SCHEMA023 ^property[0].valueCode = #retired 
* #SCHEMA024 "Influenza Grundschema, Einmalimpfung"
* #SCHEMA024 ^property[0].code = #status 
* #SCHEMA024 ^property[0].valueCode = #retired 
* #SCHEMA025 "MMR Grundschema ab 9 Monaten"
* #SCHEMA025 ^property[0].code = #status 
* #SCHEMA025 ^property[0].valueCode = #retired 
* #SCHEMA027 "MMR Indikationsschema, Erstimpfung 6-8 Monate"
* #SCHEMA027 ^property[0].code = #status 
* #SCHEMA027 ^property[0].valueCode = #retired 
* #SCHEMA028 "Meningokokken B Grundschema, Erstimpfung 2-5 Monate, 4 Dosen"
* #SCHEMA028 ^property[0].code = #status 
* #SCHEMA028 ^property[0].valueCode = #retired 
* #SCHEMA029 "Meningokokken B Grundschema, Erstimpfung 2-5 Monate"
* #SCHEMA029 ^property[0].code = #status 
* #SCHEMA029 ^property[0].valueCode = #retired 
* #SCHEMA030 "Meningokokken B Grundschema, Erstimpfung 6-11 Monate"
* #SCHEMA030 ^property[0].code = #status 
* #SCHEMA030 ^property[0].valueCode = #retired 
* #SCHEMA031 "Meningokokken B Grundschema, Erstimpfung 12-23 Monate"
* #SCHEMA031 ^property[0].code = #status 
* #SCHEMA031 ^property[0].valueCode = #retired 
* #SCHEMA032 "Meningokokken B Grundschema, Erstimpfung 2-25 Jahre, Bexsero"
* #SCHEMA032 ^property[0].code = #status 
* #SCHEMA032 ^property[0].valueCode = #retired 
* #SCHEMA033 "Meningokokken B Indikationsschema, ab 25 Jahren, Bexsero"
* #SCHEMA033 ^property[0].code = #status 
* #SCHEMA033 ^property[0].valueCode = #retired 
* #SCHEMA034 "Meningokokken B Indikationsschema, ab 25 Jahren, Trumenba"
* #SCHEMA034 ^property[0].code = #status 
* #SCHEMA034 ^property[0].valueCode = #retired 
* #SCHEMA035 "Meningokokken C Indikationsschema, 2-4 Monate, Neisvac C"
* #SCHEMA035 ^property[0].code = #status 
* #SCHEMA035 ^property[0].valueCode = #retired 
* #SCHEMA036 "Meningokokken C Indikationsschema, 2-12 Monate, Menjugate"
* #SCHEMA036 ^property[0].code = #status 
* #SCHEMA036 ^property[0].valueCode = #retired 
* #SCHEMA037 "Meningokokken C Indikationsschema, 4-12 Monate, Neisvac C"
* #SCHEMA037 ^property[0].code = #status 
* #SCHEMA037 ^property[0].valueCode = #retired 
* #SCHEMA038 "Meningokokken C Grundschema, ab 1 Jahr"
* #SCHEMA038 ^property[0].code = #status 
* #SCHEMA038 ^property[0].valueCode = #retired 
* #SCHEMA039 "Meningokokken ACWY Grundschema, ab 10 Jahren"
* #SCHEMA039 ^property[0].code = #status 
* #SCHEMA039 ^property[0].valueCode = #retired 
* #SCHEMA040 "Meningokokken ACWY Indikationsschema, ab 1 Jahr"
* #SCHEMA040 ^property[0].code = #status 
* #SCHEMA040 ^property[0].valueCode = #retired 
* #SCHEMA041 "Meningokokken ACWY Indikationsschema, Erstimpfung bis 1 Jahr"
* #SCHEMA041 ^property[0].code = #status 
* #SCHEMA041 ^property[0].valueCode = #retired 
* #SCHEMA042 "Pneumokokken Grundschema, Erstimpfung bis 1 Jahr"
* #SCHEMA042 ^property[0].code = #status 
* #SCHEMA042 ^property[0].valueCode = #retired 
* #SCHEMA043 "Pneumokokken Grundschema, Erstimpfung 1-2 Jahre"
* #SCHEMA043 ^property[0].code = #status 
* #SCHEMA043 ^property[0].valueCode = #retired 
* #SCHEMA044 "Pneumokokken Indikationsschema (hohes Risiko), Erstimpfung bis 1 Jahr"
* #SCHEMA044 ^property[0].code = #status 
* #SCHEMA044 ^property[0].valueCode = #retired 
* #SCHEMA045 "Pneumokokken Indikationsschema (hohes Risiko), Erstimpfung im 2. Lebensjahr"
* #SCHEMA045 ^property[0].code = #status 
* #SCHEMA045 ^property[0].valueCode = #retired 
* #SCHEMA046 "Pneumokokken Indikationsschema (hohes Risiko), Erstimpfung ab 3 Jahren"
* #SCHEMA046 ^property[0].code = #status 
* #SCHEMA046 ^property[0].valueCode = #retired 
* #SCHEMA050 "Rotavirus Grundschema, 2 Dosen, Erstimpfung ab der vollendeten 6.-20. Lebenswoche, Rotarix"
* #SCHEMA050 ^property[0].code = #status 
* #SCHEMA050 ^property[0].valueCode = #retired 
* #SCHEMA051 "Rotavirus Grundschema, 2 Dosen, Erstimpfung ab der vollendeten 24.-28. Lebenswoche, Rotateq"
* #SCHEMA051 ^property[0].code = #status 
* #SCHEMA051 ^property[0].valueCode = #retired 
* #SCHEMA052 "Rotavirus Grundschema, 1 Dosis, Erstimpfung ab der vollendeten 20.-24. Lebenswoche, Rotarix"
* #SCHEMA052 ^property[0].code = #status 
* #SCHEMA052 ^property[0].valueCode = #retired 
* #SCHEMA053 "Rotavirus Grundschema, 1 Dosis, Erstimpfung ab der vollendeten 28.-32. Lebenswoche, Rotateq"
* #SCHEMA053 ^property[0].code = #status 
* #SCHEMA053 ^property[0].valueCode = #retired 
* #SCHEMA054 "Rotavirus Grundschema, 3 Dosen, Erstimpfung ab der vollendeten 6.- 24. Lebenswoche, Rotateq"
* #SCHEMA054 ^property[0].code = #status 
* #SCHEMA054 ^property[0].valueCode = #retired 
* #SCHEMA060 "Varizellen Grundschema, ab 1 Jahr"
* #SCHEMA060 ^property[0].code = #status 
* #SCHEMA060 ^property[0].valueCode = #retired 
* #SCHEMA061 "Kombinationsschema MMRV, ab 9 Monaten, MMR - MMRV - V"
* #SCHEMA061 ^property[0].code = #status 
* #SCHEMA061 ^property[0].valueCode = #retired 
* #SCHEMA062 "Kombinationsschema MMRV, ab 12 Monaten, MMR - MMRV - V"
* #SCHEMA062 ^property[0].code = #status 
* #SCHEMA062 ^property[0].valueCode = #retired 
* #SCHEMA063 "Varizellen Indikationsschema, Erstimpfung bis 1 Jahr"
* #SCHEMA063 ^property[0].code = #status 
* #SCHEMA063 ^property[0].valueCode = #retired 
* #SCHEMA064 "Herpes Zoster Grundschema, ab 50 Jahren"
* #SCHEMA064 ^property[0].code = #status 
* #SCHEMA064 ^property[0].valueCode = #retired 
* #SCHEMA065 "Gelbfieber Grundschema, ab dem vollendeten 9. Lebensmonat"
* #SCHEMA065 ^property[0].code = #status 
* #SCHEMA065 ^property[0].valueCode = #retired 
* #SCHEMA066 "Japanische Enzephalitis Grundschema, ab 2 Monaten"
* #SCHEMA066 ^property[0].code = #status 
* #SCHEMA066 ^property[0].valueCode = #retired 
* #SCHEMA067 "Japanische Enzephalitis Schnellschema, ab 18 Jahren"
* #SCHEMA067 ^property[0].code = #status 
* #SCHEMA067 ^property[0].valueCode = #retired 
* #SCHEMA068 "Typhus Grundschema, Injektion"
* #SCHEMA068 ^property[0].code = #status 
* #SCHEMA068 ^property[0].valueCode = #retired 
* #SCHEMA069 "Typhus Grundschema, Oral"
* #SCHEMA069 ^property[0].code = #status 
* #SCHEMA069 ^property[0].valueCode = #retired 
* #SCHEMA070 "Tollwut Grundschema"
* #SCHEMA070 ^property[0].code = #status 
* #SCHEMA070 ^property[0].valueCode = #retired 
* #SCHEMA071 "Tollwut Schnellschema"
* #SCHEMA071 ^property[0].code = #status 
* #SCHEMA071 ^property[0].valueCode = #retired 
* #SCHEMA072 "Tollwut postexpositionelles Schema, Essen 4 Dosen"
* #SCHEMA072 ^property[0].code = #status 
* #SCHEMA072 ^property[0].valueCode = #retired 
* #SCHEMA073 "Tollwut postexpositionelles Schema, Essen 5 Dosen"
* #SCHEMA073 ^property[0].code = #status 
* #SCHEMA073 ^property[0].valueCode = #retired 
* #SCHEMA074 "Tollwut postexpositionelles Schema, Zagreb"
* #SCHEMA074 ^property[0].code = #status 
* #SCHEMA074 ^property[0].valueCode = #retired 
* #SCHEMA075 "Tollwut postexpositionelles Schema, mit Tollwut-Vorimpfung"
* #SCHEMA075 ^property[0].code = #status 
* #SCHEMA075 ^property[0].valueCode = #retired 
* #SCHEMA076 "Cholera Grundschema, Erstimpfung 2-6 Jahre"
* #SCHEMA076 ^property[0].code = #status 
* #SCHEMA076 ^property[0].valueCode = #retired 
* #SCHEMA078 "Cholera Grundschema, Erstimpfung ab 6 Jahren"
* #SCHEMA078 ^property[0].code = #status 
* #SCHEMA078 ^property[0].valueCode = #retired 
* #SCHEMA079 "Meningokokken B Grundschema, Erstimpfung 10-25 Jahre, Trumenba"
* #SCHEMA079 ^property[0].code = #status 
* #SCHEMA079 ^property[0].valueCode = #retired 
* #SCHEMA080 "Kombinationsschema Di-Te-Pert-IPV"
* #SCHEMA080 ^property[0].code = #status 
* #SCHEMA080 ^property[0].valueCode = #retired 
* #SCHEMA081 "Kombinationsschema Di-Te-Pert"
* #SCHEMA081 ^property[0].code = #status 
* #SCHEMA081 ^property[0].valueCode = #retired 
* #SCHEMA082 "Kombinationsschema Di-Te"
* #SCHEMA082 ^property[0].code = #status 
* #SCHEMA082 ^property[0].valueCode = #retired 
* #SCHEMA083 "Tetanus Monokomponente"
* #SCHEMA083 ^property[0].code = #status 
* #SCHEMA083 ^property[0].valueCode = #retired 
* #SCHEMA084 "Poliomyelitis Indikationsschema ab dem vollendeten 2. Lebensmonat"
* #SCHEMA084 ^property[0].code = #status 
* #SCHEMA084 ^property[0].valueCode = #retired 
* #SCHEMA085 "Hepatitis B Monokomponente Indikationsschema"
* #SCHEMA085 ^property[0].code = #status 
* #SCHEMA085 ^property[0].valueCode = #retired 
* #SCHEMA086 "SARS-CoV-2 Grundschema, Comirnaty"
* #SCHEMA086 ^property[0].code = #status 
* #SCHEMA086 ^property[0].valueCode = #retired 
* #SCHEMA087 "SARS-CoV-2 Grundschema, Spikevax"
* #SCHEMA087 ^property[0].code = #status 
* #SCHEMA087 ^property[0].valueCode = #retired 
* #SCHEMA088 "SARS-CoV-2 Grundschema, Vaxzevria"
* #SCHEMA088 ^property[0].code = #status 
* #SCHEMA088 ^property[0].valueCode = #retired 
* #SCHEMA089 "SARS-CoV-2 Grundschema, Jcovden"
* #SCHEMA089 ^property[0].code = #status 
* #SCHEMA089 ^property[0].valueCode = #retired 
* #SCHEMA090 "SARS-CoV-2 Grundschema, Auslandsimpfung"
* #SCHEMA090 ^property[0].code = #status 
* #SCHEMA090 ^property[0].valueCode = #retired 
* #SCHEMA091 "Herpes Zoster Grundschema, nach Zostavax"
* #SCHEMA091 ^property[0].code = #status 
* #SCHEMA091 ^property[0].valueCode = #retired 
* #SCHEMA092 "Herpes Zoster Indikationsschema, ab 18 Jahren"
* #SCHEMA092 ^property[0].code = #status 
* #SCHEMA092 ^property[0].valueCode = #retired 
* #SCHEMA093 "MMR Grundschema, ab 1 Jahr"
* #SCHEMA093 ^property[0].code = #status 
* #SCHEMA093 ^property[0].valueCode = #retired 
* #SCHEMA094 "SARS-CoV-2 Indikationsschema, Comirnaty"
* #SCHEMA094 ^property[0].code = #status 
* #SCHEMA094 ^property[0].valueCode = #retired 
* #SCHEMA095 "SARS-CoV-2 Indikationsschema, Spikevax"
* #SCHEMA095 ^property[0].code = #status 
* #SCHEMA095 ^property[0].valueCode = #retired 
* #SCHEMA096 "SARS-CoV-2 heterologes Schema (Impfstoffwechsel)"
* #SCHEMA096 ^property[0].code = #status 
* #SCHEMA096 ^property[0].valueCode = #retired 
* #SCHEMA097 "Pneumokokken Grundschema, Erstimpfung 2-5 Jahre"
* #SCHEMA097 ^property[0].code = #status 
* #SCHEMA097 ^property[0].valueCode = #retired 
* #SCHEMA098 "Pneumokokken Grundschema, ab 60 Jahre"
* #SCHEMA098 ^property[0].code = #status 
* #SCHEMA098 ^property[0].valueCode = #retired 
* #SCHEMA099 "SARS-CoV-2 Grundschema, Nuvaxovid"
* #SCHEMA099 ^property[0].code = #status 
* #SCHEMA099 ^property[0].valueCode = #retired 
* #SCHEMA100 "Kombinationsschema MMRV, ab 9 Monaten, MMRV - MMRV"
* #SCHEMA100 ^property[0].code = #status 
* #SCHEMA100 ^property[0].valueCode = #retired 
* #SCHEMA101 "Kombinationsschema MMRV, ab 12 Monaten, MMRV - MMRV"
* #SCHEMA101 ^property[0].code = #status 
* #SCHEMA101 ^property[0].valueCode = #retired 
* #SCHEMA102 "Hepatitis B Monokomponente Schnellschema durch Indikation"
* #SCHEMA102 ^property[0].code = #status 
* #SCHEMA102 ^property[0].valueCode = #retired 
* #SCHEMA103 "Hepatitis B Prophylaxe Neugeborener Indikationsschema"
* #SCHEMA103 ^property[0].code = #status 
* #SCHEMA103 ^property[0].valueCode = #retired 
* #SCHEMA104 "Influenza Indikationsschema"
* #SCHEMA104 ^property[0].code = #status 
* #SCHEMA104 ^property[0].valueCode = #retired 
* #SCHEMA105 "Kombinationsschema Di-Te-IPV"
* #SCHEMA105 ^property[0].code = #status 
* #SCHEMA105 ^property[0].valueCode = #retired 
* #SCHEMA106 "Meningokokken ACWY Indikationsschema, Nimenrix bis 6 Monate"
* #SCHEMA106 ^property[0].code = #status 
* #SCHEMA106 ^property[0].valueCode = #retired 
* #SCHEMA107 "Meningokokken ACWY Indikationsschema, Nimenrix ab 6 Monate"
* #SCHEMA107 ^property[0].code = #status 
* #SCHEMA107 ^property[0].valueCode = #retired 
* #SCHEMA108 "Meningokokken ACWY Indikationsschema, Nimenrix ab 1 Jahr"
* #SCHEMA108 ^property[0].code = #status 
* #SCHEMA108 ^property[0].valueCode = #retired 
* #SCHEMA109 "Meningokokken ACWY Indikationsschema, Menveo ab 2 Jahre"
* #SCHEMA109 ^property[0].code = #status 
* #SCHEMA109 ^property[0].valueCode = #retired 
* #SCHEMA110 "COVID-19 Indikationsschema"
* #SCHEMA110 ^property[0].code = #status 
* #SCHEMA110 ^property[0].valueCode = #retired 
* #SCHEMA111 "Mpox/Pocken Indikationsschema"
* #SCHEMA111 ^property[0].code = #status 
* #SCHEMA111 ^property[0].valueCode = #retired 
* #SCHEMA112 "SARS-CoV-2 Grundschema, Valneva"
* #SCHEMA112 ^property[0].code = #status 
* #SCHEMA112 ^property[0].valueCode = #retired 
* #SCHEMA113 "Kombinationsschema MMRV, ab 9 Monaten, MMRV - MMR - V"
* #SCHEMA113 ^property[0].code = #status 
* #SCHEMA113 ^property[0].valueCode = #retired 
* #SCHEMA114 "Kombinationsschema MMRV, ab 12 Monaten, MMRV - MMR - V"
* #SCHEMA114 ^property[0].code = #status 
* #SCHEMA114 ^property[0].valueCode = #retired 
* #SCHEMA115 "Mpox/Pocken Indikationsschema, einmalige Impfung"
* #SCHEMA115 ^property[0].code = #status 
* #SCHEMA115 ^property[0].valueCode = #retired 
* #SCHEMA116 "Pneumokokken Indikationsschema (erhöhtes Risiko), ab 50 Jahren"
* #SCHEMA116 ^property[0].code = #status 
* #SCHEMA116 ^property[0].valueCode = #retired 
* #SCHEMA117 "COVID-19 Grundschema, Kleinkind"
* #SCHEMA117 ^property[0].code = #status 
* #SCHEMA117 ^property[0].valueCode = #retired 
* #SCHEMA118 "Pneumokokken Indikationsschema, Frühgeborene"
* #SCHEMA118 ^property[0].code = #status 
* #SCHEMA118 ^property[0].valueCode = #retired 
* #SCHEMA119 "Dengue-Fieber Grundschema, 2 Dosen"
* #SCHEMA119 ^property[0].code = #status 
* #SCHEMA119 ^property[0].valueCode = #retired 
* #SCHEMA120 "Poliomyelitis Indikationsschema ab dem vollendeten 1. Lebensjahr"
* #SCHEMA120 ^property[0].code = #status 
* #SCHEMA120 ^property[0].valueCode = #retired 
* #SCHEMA121 "RSV Grundschema"
* #SCHEMA121 ^property[0].code = #status 
* #SCHEMA121 ^property[0].valueCode = #retired 
* #SCHEMA122 "COVID-19 Grundschema"
* #SCHEMA122 ^property[0].code = #status 
* #SCHEMA122 ^property[0].valueCode = #retired 
* #SCHEMA123 "Tollwut Schnellschema (WHO)"
* #SCHEMA123 ^property[0].code = #status 
* #SCHEMA123 ^property[0].valueCode = #retired 
* #SCHEMA124 "Tollwut Schnellschema (WHO), intradermal"
* #SCHEMA124 ^property[0].code = #status 
* #SCHEMA124 ^property[0].valueCode = #retired 
* #SCHEMA125 "Meningokokken ACWY, ab 1 Jahr"
* #SCHEMA125 ^property[0].code = #status 
* #SCHEMA125 ^property[0].valueCode = #retired 
* #SCHEMA126 "FSME beschleunigtes konventionelles Schema, Encepur"
* #SCHEMA126 ^property[0].code = #status 
* #SCHEMA126 ^property[0].valueCode = #retired 
* #SCHEMA201 "6-fach (2+1) Di-Te-Pert-HiB-IPV-HepB inkl. Auffrischungsimpfungen"
* #SCHEMA202 "6-fach (3+1) Di-Te-Pert-HiB-IPV-HepB inkl. Auffrischungsimpfungen, Indikation"
* #SCHEMA203 "Cholera"
* #SCHEMA204 "COVID-19"
* #SCHEMA205 "COVID-19, Indikation"
* #SCHEMA206 "Dengue"
* #SCHEMA207 "Di-Te-Pert"
* #SCHEMA208 "Di-Te-Pert-IPV"
* #SCHEMA209 "FSME"
* #SCHEMA210 "FSME, beschleunigtes konventionelles Schema"
* #SCHEMA211 "FSME, Schnellschema"
* #SCHEMA212 "Gelbfieber"
* #SCHEMA213 "Hepatitis A"
* #SCHEMA215 "Hepatitis AB"
* #SCHEMA216 "Hepatitis B"
* #SCHEMA217 "Hepatitis B, Indikation"
* #SCHEMA218 "Hepatitis B, Indikation Prophylaxe Neugeborener"
* #SCHEMA219 "Hepatitis B, Schnellschema"
* #SCHEMA220 "Herpes Zoster"
* #SCHEMA221 "Herpes Zoster, Indikation"
* #SCHEMA222 "HiB, Indikation"
* #SCHEMA223 "HPV"
* #SCHEMA224 "HPV, Indikation"
* #SCHEMA225 "Influenza"
* #SCHEMA226 "Influenza, Indikation"
* #SCHEMA227 "Japanische Enzephalitis"
* #SCHEMA228 "Japanische Enzephalitis, Schnellschema"
* #SCHEMA229 "Meningokokken ACWY"
* #SCHEMA230 "Meningokokken ACWY, Indikation"
* #SCHEMA231 "Meningokokken B"
* #SCHEMA232 "Meningokokken B, Indikation"
* #SCHEMA233 "Meningokokken C"
* #SCHEMA234 "MMR"
* #SCHEMA235 "MMR - MMRV - V"
* #SCHEMA236 "MMR, Indikation"
* #SCHEMA237 "MMRV"
* #SCHEMA238 "Mpox"
* #SCHEMA239 "Pneumokokken"
* #SCHEMA241 "Pneumokokken, Indikation Frühgeborene"
* #SCHEMA242 "Pneumokokken, Indikation"
* #SCHEMA244 "Rotavirus"
* #SCHEMA245 "RSV"
* #SCHEMA246 "Tollwut"
* #SCHEMA247 "Tollwut postexpositionell, Essen"
* #SCHEMA248 "Tollwut postexpositionell, mit Vorimpfung"
* #SCHEMA249 "Tollwut postexpositionell, Zagreb"
* #SCHEMA250 "Tollwut, Schnellschema"
* #SCHEMA251 "Tollwut, Schnellschema (WHO)"
* #SCHEMA252 "Tollwut, Schnellschema (WHO) intradermal"
* #SCHEMA253 "Typhus"
* #SCHEMA254 "Varizellen"
* #SCHEMA255 "Varizellen, Indikation"
* #SCHEMA256 "Chikungunya"
* #SCHEMA257 "Zoonotische Influenza (H5N8)"
* #SCHEMA258 "Hepatitis AB, Schnellschema"
* #_AllgemeineBevölkerung "Allgemeine Bevölkerung"
* #_AllgemeineBevölkerung ^property[0].code = #status 
* #_AllgemeineBevölkerung ^property[0].valueCode = #retired 
* #_Ausnahme "Ausnahme"
* #_Blau "Blau"
* #_Einzel "Impfungen"
* #_Empfohlene "Allgemein empfohlene Impfungen"
* #_Gelb "Gelb"
* #_Grau "Grau"
* #_Grün "Grün"
* #_Kombinationen "Kombinationsimpfungen"
* #_Reise "Reise-/Indikationsimpfungen"
* #_Rot "Rot"
* #_SpezielleRisikogruppefür "Spezielle Risikogruppe für"
* #_Weitere "Weitere Impfungen"
* #off-label-use "off-label-use"
