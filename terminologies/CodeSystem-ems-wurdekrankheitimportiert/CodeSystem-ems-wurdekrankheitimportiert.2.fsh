Instance: ems-wurdekrankheitimportiert 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "ems-wurdekrankheitimportiert" 
* url = "https://termgit.elga.gv.at/CodeSystem/ems-wurdekrankheitimportiert" 
* name = "ems-wurdekrankheitimportiert" 
* title = "EMS_WurdeKrankheitImportiert" 
* status = #active 
* content = #complete 
* version = "1.0.1+20240129" 
* description = "**Description:** Codelist for the epidemiological reporting system Austria coding whether the illness was imported

**Beschreibung:** Codeliste epidemiologisches Meldesystem: wurde die Krankheit importiert" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.110" 
* date = "2024-01-29" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 4 
* #ICASE "infiziert durch importierten Fall"
* #IMP "importiert"
* #NIMP "nicht importiert"
* #UNK "unbekannt"
