Instance: elga-formatcodezusatz 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-formatcodezusatz" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-formatcodezusatz" 
* name = "elga-formatcodezusatz" 
* title = "ELGA_FormatCodeZusatz" 
* status = #active 
* content = #complete 
* version = "1.1.0+20240820" 
* description = "**Description:** Defintion of valid additions for the FormatCode by declaration in Context of XDS document metadata

**Beschreibung:** Definition der gültigen Zusätze für den FormatCode bei Angabe im Rahmen der XDS Dokument Metadaten" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.41" 
* date = "2024-08-20" 
* publisher = "ELGA GmbH" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 6 
* #CDAL1 "CDAL1"
* #CDAL1 ^definition = Das Dokument enthält im CDA Body Bereich strukturierte Angaben gemäß CDA Level 1 (structuredBody)
* #GIF "GIF"
* #GIF ^definition = Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild im Graphics Interchange Format (GIF).
* #JPG "JPG"
* #JPG ^definition = Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild gemäß der Norm ISO/IEC 10918-1 bzw. CCITT Recommendation T.81 der Joint Photographic Experts Group (JPEG).
* #PDF "PDF"
* #PDF ^definition = Das Dokument enthält im CDA Body Bereich ein eingebettetes PDF/A-1a Dokument.
* #PNG "PNG"
* #PNG ^definition = Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild im Portable Network Graphics (PNG) Format.
* #TEXT "TEXT"
* #TEXT ^definition = Das Dokument enthält im CDA Body Bereich unstrukturierten Text gemäß CDA Level 1 (nonXMLBody)
