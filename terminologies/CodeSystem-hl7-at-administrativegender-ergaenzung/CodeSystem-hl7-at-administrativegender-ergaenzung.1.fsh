Instance: hl7-at-administrativegender-ergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "hl7-at-administrativegender-ergaenzung" 
* url = "https://termgit.elga.gv.at/CodeSystem/hl7-at-administrativegender-ergaenzung" 
* name = "hl7-at-administrativegender-ergaenzung" 
* title = "HL7-at_AdministrativeGender-Ergaenzung" 
* status = #active 
* content = #complete 
* version = "1.1.0+20240820" 
* description = "Österreich-spezifische Codes, die für die weitere Untergliederung des dritten Geschlechts dokumentiert werden können." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.224" 
* date = "2024-08-20" 
* publisher = "HL7 Austria" 
* contact[0].name = "HL7 Austria" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.hl7.at" 
* count = 3 
* concept[0].code = #D
* concept[0].display = "Divers"
* concept[1].code = #I
* concept[1].display = "Inter"
* concept[2].code = #O
* concept[2].display = "Offen"
