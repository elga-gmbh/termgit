Instance: oeaek-zertifikate 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "oeaek-zertifikate" 
* url = "https://termgit.elga.gv.at/CodeSystem/oeaek-zertifikate" 
* name = "oeaek-zertifikate" 
* title = "OEAEK_Zertifikate" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230131" 
* description = "**Description:** Code List of all certificates valid in Austria issued by the Medical Chamber of Austria

**Beschreibung:** Code Liste aller in Österreich gültigen ÖÄK Zertifikate" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.3.1.10.50" 
* date = "2018-07-01" 
* count = 25 
* #104 "Reisemedizin"
* #3 "Angiologische Basisdiagnostik"
* #4210 "Sonographie Abdomen"
* #4212 "Sonographie Arterien"
* #4214 "Sonographie Bewegungsapparat"
* #4216 "Sonographie Bulbus und Anhängsel"
* #4218 "Sonographie Echokardiographie"
* #4220 "Sonographie Hirnversorgende Arterien"
* #4222 "Sonographie Hüftsonographie"
* #4224 "Sonographie Mammae"
* #4226 "Sonographie Orbita"
* #4228 "Sonographie Päd Echokardiographie"
* #4230 "Sonographie Pädiatrische Echokardiographie"
* #4232 "Sonographie Pädiatrische Sonographie"
* #4234 "Sonographie Pleura und Lunge"
* #4236 "Sonographie Schilddrüse"
* #4238 "Sonographie Smallpart"
* #4240 "Sonographie TEE Fokussierte Echokardiographie"
* #4242 "Sonographie transkraniell"
* #4244 "Sonographie Urogenital"
* #4246 "Sonographie Venen"
* #4248 "Sonographie Weiblicher Unterbauch"
* #54 "Elektroenzephalographie"
* #7 "Ärztliche Wundbehandlung"
* #8 "Basismodul Sexualmedizin"
