Instance: icd-o-3 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "icd-o-3" 
* url = "https://termgit.elga.gv.at/CodeSystem/icd-o-3" 
* name = "icd-o-3" 
* title = "ICD-O-3" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230131" 
* description = "**Beschreibung:** Internationale Klassifikation der Krankheiten für die Onkologie, Dritte Ausgabe, Zweite Revision 2019

**Versions-Beschreibung:** a.Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM).   b.ICD-O-3-Kodes, -Begriffe und -Texte Bundesinstitut für Arzneimittel und Medizinprodukte (BfArM), übersetzt  von der International classification of diseases for oncology, 3rd edition ? ICD-O-3, herausgegeben durch die Weltgesundheitsorganisation   c.Die ICD-O-3 muss so genutzt werden, wie in der Klassifikation und in den einführenden Kapiteln und in den Anhängen zur ICD-O-3 beschrieben." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.6.43.1" 
* date = "2021-05-28" 
* copyright = "Copyright WHO, BfArM 2003 - 2020" 
* count = 1622 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #M "Morphologie"
* #M ^property[0].code = #child 
* #M ^property[0].valueCode = #800-800 
* #M ^property[1].code = #child 
* #M ^property[1].valueCode = #801-804 
* #M ^property[2].code = #child 
* #M ^property[2].valueCode = #805-808 
* #M ^property[3].code = #child 
* #M ^property[3].valueCode = #809-811 
* #M ^property[4].code = #child 
* #M ^property[4].valueCode = #812-813 
* #M ^property[5].code = #child 
* #M ^property[5].valueCode = #814-838 
* #M ^property[6].code = #child 
* #M ^property[6].valueCode = #839-842 
* #M ^property[7].code = #child 
* #M ^property[7].valueCode = #843-843 
* #M ^property[8].code = #child 
* #M ^property[8].valueCode = #844-849 
* #M ^property[9].code = #child 
* #M ^property[9].valueCode = #850-854 
* #M ^property[10].code = #child 
* #M ^property[10].valueCode = #855-855 
* #M ^property[11].code = #child 
* #M ^property[11].valueCode = #856-857 
* #M ^property[12].code = #child 
* #M ^property[12].valueCode = #858-858 
* #M ^property[13].code = #child 
* #M ^property[13].valueCode = #859-867 
* #M ^property[14].code = #child 
* #M ^property[14].valueCode = #868-871 
* #M ^property[15].code = #child 
* #M ^property[15].valueCode = #872-879 
* #M ^property[16].code = #child 
* #M ^property[16].valueCode = #880-880 
* #M ^property[17].code = #child 
* #M ^property[17].valueCode = #881-883 
* #M ^property[18].code = #child 
* #M ^property[18].valueCode = #884-884 
* #M ^property[19].code = #child 
* #M ^property[19].valueCode = #885-888 
* #M ^property[20].code = #child 
* #M ^property[20].valueCode = #889-892 
* #M ^property[21].code = #child 
* #M ^property[21].valueCode = #893-899 
* #M ^property[22].code = #child 
* #M ^property[22].valueCode = #900-903 
* #M ^property[23].code = #child 
* #M ^property[23].valueCode = #904-904 
* #M ^property[24].code = #child 
* #M ^property[24].valueCode = #905-905 
* #M ^property[25].code = #child 
* #M ^property[25].valueCode = #906-909 
* #M ^property[26].code = #child 
* #M ^property[26].valueCode = #910-910 
* #M ^property[27].code = #child 
* #M ^property[27].valueCode = #911-911 
* #M ^property[28].code = #child 
* #M ^property[28].valueCode = #912-916 
* #M ^property[29].code = #child 
* #M ^property[29].valueCode = #917-917 
* #M ^property[30].code = #child 
* #M ^property[30].valueCode = #918-924 
* #M ^property[31].code = #child 
* #M ^property[31].valueCode = #925-925 
* #M ^property[32].code = #child 
* #M ^property[32].valueCode = #926-926 
* #M ^property[33].code = #child 
* #M ^property[33].valueCode = #927-934 
* #M ^property[34].code = #child 
* #M ^property[34].valueCode = #935-937 
* #M ^property[35].code = #child 
* #M ^property[35].valueCode = #938-948 
* #M ^property[36].code = #child 
* #M ^property[36].valueCode = #949-952 
* #M ^property[37].code = #child 
* #M ^property[37].valueCode = #953-953 
* #M ^property[38].code = #child 
* #M ^property[38].valueCode = #954-957 
* #M ^property[39].code = #child 
* #M ^property[39].valueCode = #958-958 
* #M ^property[40].code = #child 
* #M ^property[40].valueCode = #959-972 
* #M ^property[41].code = #child 
* #M ^property[41].valueCode = #973-973 
* #M ^property[42].code = #child 
* #M ^property[42].valueCode = #974-974 
* #M ^property[43].code = #child 
* #M ^property[43].valueCode = #975-975 
* #M ^property[44].code = #child 
* #M ^property[44].valueCode = #976-976 
* #M ^property[45].code = #child 
* #M ^property[45].valueCode = #980-994 
* #M ^property[46].code = #child 
* #M ^property[46].valueCode = #995-996 
* #M ^property[47].code = #child 
* #M ^property[47].valueCode = #997-997 
* #M ^property[48].code = #child 
* #M ^property[48].valueCode = #998-999 
* #800-800 "Neoplasien o.n.A."
* #800-800 ^property[0].code = #parent 
* #800-800 ^property[0].valueCode = #M 
* #800-800 ^property[1].code = #child 
* #800-800 ^property[1].valueCode = #8000:0 
* #800-800 ^property[2].code = #child 
* #800-800 ^property[2].valueCode = #8000:1 
* #800-800 ^property[3].code = #child 
* #800-800 ^property[3].valueCode = #8000:3 
* #800-800 ^property[4].code = #child 
* #800-800 ^property[4].valueCode = #8000:6 
* #800-800 ^property[5].code = #child 
* #800-800 ^property[5].valueCode = #8000:9 
* #800-800 ^property[6].code = #child 
* #800-800 ^property[6].valueCode = #8001:0 
* #800-800 ^property[7].code = #child 
* #800-800 ^property[7].valueCode = #8001:1 
* #800-800 ^property[8].code = #child 
* #800-800 ^property[8].valueCode = #8001:3 
* #800-800 ^property[9].code = #child 
* #800-800 ^property[9].valueCode = #8002:3 
* #800-800 ^property[10].code = #child 
* #800-800 ^property[10].valueCode = #8003:3 
* #800-800 ^property[11].code = #child 
* #800-800 ^property[11].valueCode = #8004:3 
* #800-800 ^property[12].code = #child 
* #800-800 ^property[12].valueCode = #8005:0 
* #800-800 ^property[13].code = #child 
* #800-800 ^property[13].valueCode = #8005:3 
* #8000:0 "Benigne Neoplasie o.n.A."
* #8000:0 ^designation[0].value = "Benigner Tumor" 
* #8000:0 ^designation[1].value = "Benigner unklassifizierter Tumor" 
* #8000:0 ^property[0].code = #parent 
* #8000:0 ^property[0].valueCode = #800-800 
* #8000:1 "Neoplasie fraglicher Dignität"
* #8000:1 ^designation[0].value = "Neoplasie o.n.A." 
* #8000:1 ^designation[1].value = "Tumor o.n.A." 
* #8000:1 ^designation[2].value = "Unklassifizierter Tumor, Borderline-Typ" 
* #8000:1 ^designation[3].value = "Unklassifizierter Tumor fraglicher Dignität" 
* #8000:1 ^property[0].code = #parent 
* #8000:1 ^property[0].valueCode = #800-800 
* #8000:3 "Maligne Neoplasie"
* #8000:3 ^designation[0].value = "Blastom o.n.A." 
* #8000:3 ^designation[1].value = "Krebs" 
* #8000:3 ^designation[2].value = "Maligner Tumor o.n.A." 
* #8000:3 ^designation[3].value = "Malignom" 
* #8000:3 ^designation[4].value = "Unklassifizierter maligner Tumor" 
* #8000:3 ^property[0].code = #parent 
* #8000:3 ^property[0].valueCode = #800-800 
* #8000:6 "Neoplasie, Metastase"
* #8000:6 ^designation[0].value = "Sekundäre Neoplasie" 
* #8000:6 ^designation[1].value = "Sekundärtumor" 
* #8000:6 ^designation[2].value = "Tumorembolus" 
* #8000:6 ^designation[3].value = "Tumormetastase" 
* #8000:6 ^property[0].code = #parent 
* #8000:6 ^property[0].valueCode = #800-800 
* #8000:9 "Maligne Neoplasie, unsicher ob Primärtumor oder Metastase"
* #8000:9 ^designation[0].value = "Unklassifizierter maligner Tumor, unsicher ob Primärtumor oder Metastase" 
* #8000:9 ^property[0].code = #parent 
* #8000:9 ^property[0].valueCode = #800-800 
* #8001:0 "Benigne Tumorzellen"
* #8001:0 ^property[0].code = #parent 
* #8001:0 ^property[0].valueCode = #800-800 
* #8001:1 "Tumorzellen fraglicher Dignität"
* #8001:1 ^designation[0].value = "Tumorzellen o.n.A." 
* #8001:1 ^property[0].code = #parent 
* #8001:1 ^property[0].valueCode = #800-800 
* #8001:3 "Maligne Tumorzellen"
* #8001:3 ^property[0].code = #parent 
* #8001:3 ^property[0].valueCode = #800-800 
* #8002:3 "Kleinzelliger maligner Tumor"
* #8002:3 ^property[0].code = #parent 
* #8002:3 ^property[0].valueCode = #800-800 
* #8003:3 "Riesenzelliger maligner Tumor"
* #8003:3 ^property[0].code = #parent 
* #8003:3 ^property[0].valueCode = #800-800 
* #8004:3 "Spindelzelliger maligner Tumor"
* #8004:3 ^designation[0].value = "Maligner Tumor vom fusiformen Zelltyp" 
* #8004:3 ^property[0].code = #parent 
* #8004:3 ^property[0].valueCode = #800-800 
* #8005:0 "Klarzelliger Tumor o.n.A."
* #8005:0 ^property[0].code = #parent 
* #8005:0 ^property[0].valueCode = #800-800 
* #8005:3 "Klarzelliger maligner Tumor"
* #8005:3 ^property[0].code = #parent 
* #8005:3 ^property[0].valueCode = #800-800 
* #801-804 "Epitheliale Neoplasien o.n.A."
* #801-804 ^property[0].code = #parent 
* #801-804 ^property[0].valueCode = #M 
* #801-804 ^property[1].code = #child 
* #801-804 ^property[1].valueCode = #8010:0 
* #801-804 ^property[2].code = #child 
* #801-804 ^property[2].valueCode = #8010:2 
* #801-804 ^property[3].code = #child 
* #801-804 ^property[3].valueCode = #8010:3 
* #801-804 ^property[4].code = #child 
* #801-804 ^property[4].valueCode = #8010:6 
* #801-804 ^property[5].code = #child 
* #801-804 ^property[5].valueCode = #8010:9 
* #801-804 ^property[6].code = #child 
* #801-804 ^property[6].valueCode = #8011:0 
* #801-804 ^property[7].code = #child 
* #801-804 ^property[7].valueCode = #8011:3 
* #801-804 ^property[8].code = #child 
* #801-804 ^property[8].valueCode = #8012:3 
* #801-804 ^property[9].code = #child 
* #801-804 ^property[9].valueCode = #8013:3 
* #801-804 ^property[10].code = #child 
* #801-804 ^property[10].valueCode = #8014:3 
* #801-804 ^property[11].code = #child 
* #801-804 ^property[11].valueCode = #8015:3 
* #801-804 ^property[12].code = #child 
* #801-804 ^property[12].valueCode = #8020:3 
* #801-804 ^property[13].code = #child 
* #801-804 ^property[13].valueCode = #8021:3 
* #801-804 ^property[14].code = #child 
* #801-804 ^property[14].valueCode = #8022:3 
* #801-804 ^property[15].code = #child 
* #801-804 ^property[15].valueCode = #8023:3 
* #801-804 ^property[16].code = #child 
* #801-804 ^property[16].valueCode = #8030:3 
* #801-804 ^property[17].code = #child 
* #801-804 ^property[17].valueCode = #8031:3 
* #801-804 ^property[18].code = #child 
* #801-804 ^property[18].valueCode = #8032:3 
* #801-804 ^property[19].code = #child 
* #801-804 ^property[19].valueCode = #8033:3 
* #801-804 ^property[20].code = #child 
* #801-804 ^property[20].valueCode = #8034:3 
* #801-804 ^property[21].code = #child 
* #801-804 ^property[21].valueCode = #8035:3 
* #801-804 ^property[22].code = #child 
* #801-804 ^property[22].valueCode = #8040:0 
* #801-804 ^property[23].code = #child 
* #801-804 ^property[23].valueCode = #8040:1 
* #801-804 ^property[24].code = #child 
* #801-804 ^property[24].valueCode = #8041:3 
* #801-804 ^property[25].code = #child 
* #801-804 ^property[25].valueCode = #8042:3 
* #801-804 ^property[26].code = #child 
* #801-804 ^property[26].valueCode = #8043:3 
* #801-804 ^property[27].code = #child 
* #801-804 ^property[27].valueCode = #8044:3 
* #801-804 ^property[28].code = #child 
* #801-804 ^property[28].valueCode = #8045:3 
* #801-804 ^property[29].code = #child 
* #801-804 ^property[29].valueCode = #8046:3 
* #8010:0 "Benigne epitheliale Neoplasie"
* #8010:0 ^designation[0].value = "Benigner epithelialer Tumor" 
* #8010:0 ^property[0].code = #parent 
* #8010:0 ^property[0].valueCode = #801-804 
* #8010:2 "Carcinoma in situ o.n.A."
* #8010:2 ^designation[0].value = "Intraepitheliales Karzinom o.n.A." 
* #8010:2 ^property[0].code = #parent 
* #8010:2 ^property[0].valueCode = #801-804 
* #8010:3 "Karzinom o.n.A."
* #8010:3 ^designation[0].value = "Maligner epithelialer Tumor" 
* #8010:3 ^property[0].code = #parent 
* #8010:3 ^property[0].valueCode = #801-804 
* #8010:6 "Karzinom-Metastase o.n.A."
* #8010:6 ^designation[0].value = "Sekundäres Karzinom" 
* #8010:6 ^property[0].code = #parent 
* #8010:6 ^property[0].valueCode = #801-804 
* #8010:9 "Karzinomatose"
* #8010:9 ^property[0].code = #parent 
* #8010:9 ^property[0].valueCode = #801-804 
* #8011:0 "Benignes Epitheliom"
* #8011:0 ^property[0].code = #parent 
* #8011:0 ^property[0].valueCode = #801-804 
* #8011:3 "Malignes Epitheliom"
* #8011:3 ^designation[0].value = "Epitheliom o.n.A." 
* #8011:3 ^property[0].code = #parent 
* #8011:3 ^property[0].valueCode = #801-804 
* #8012:3 "Großzelliges Karzinom o.n.A."
* #8012:3 ^property[0].code = #parent 
* #8012:3 ^property[0].valueCode = #801-804 
* #8013:3 "Großzelliges neuroendokrines Karzinom"
* #8013:3 ^designation[0].value = "Kombiniertes großzelliges neuroendokrines Karzinom" 
* #8013:3 ^property[0].code = #parent 
* #8013:3 ^property[0].valueCode = #801-804 
* #8014:3 "Großzelliges Karzinom mit rhabdoidem Phänotyp"
* #8014:3 ^property[0].code = #parent 
* #8014:3 ^property[0].valueCode = #801-804 
* #8015:3 "Glaszellkarzinom"
* #8015:3 ^designation[0].value = "Glassy cell carcinoma" 
* #8015:3 ^property[0].code = #parent 
* #8015:3 ^property[0].valueCode = #801-804 
* #8020:3 "Undifferenziertes Karzinom o.n.A."
* #8020:3 ^designation[0].value = "Anaplastisches undifferenziertes Karzinom" 
* #8020:3 ^designation[1].value = "Entdifferenziertes Karzinom" 
* #8020:3 ^designation[2].value = "Wenig differenziertes Karzinom o.n.A." 
* #8020:3 ^property[0].code = #parent 
* #8020:3 ^property[0].valueCode = #801-804 
* #8021:3 "Anaplastisches Karzinom o.n.A."
* #8021:3 ^property[0].code = #parent 
* #8021:3 ^property[0].valueCode = #801-804 
* #8022:3 "Pleomorphes Karzinom"
* #8022:3 ^property[0].code = #parent 
* #8022:3 ^property[0].valueCode = #801-804 
* #8023:3 "NUT (Nuclear protein in testis)-assoziiertes Karzinom"
* #8023:3 ^designation[0].value = "NUT-Karzinom" 
* #8023:3 ^designation[1].value = "NUT-Mittellinien-Karzinom" 
* #8023:3 ^property[0].code = #parent 
* #8023:3 ^property[0].valueCode = #801-804 
* #8030:3 "Riesenzell- und Spindelzellkarzinom"
* #8030:3 ^property[0].code = #parent 
* #8030:3 ^property[0].valueCode = #801-804 
* #8031:3 "Riesenzellkarzinom"
* #8031:3 ^property[0].code = #parent 
* #8031:3 ^property[0].valueCode = #801-804 
* #8032:3 "Spindelzellkarzinom o.n.A."
* #8032:3 ^property[0].code = #parent 
* #8032:3 ^property[0].valueCode = #801-804 
* #8033:3 "Pseudosarkomatöses Karzinom"
* #8033:3 ^designation[0].value = "Sarkomatoides Karzinom" 
* #8033:3 ^property[0].code = #parent 
* #8033:3 ^property[0].valueCode = #801-804 
* #8034:3 "Polygonalzelliges Karzinom"
* #8034:3 ^property[0].code = #parent 
* #8034:3 ^property[0].valueCode = #801-804 
* #8035:3 "Karzinom mit osteoklastenähnlichen Riesenzellen"
* #8035:3 ^property[0].code = #parent 
* #8035:3 ^property[0].valueCode = #801-804 
* #8040:0 "Benignes Tumorlet"
* #8040:0 ^property[0].code = #parent 
* #8040:0 ^property[0].valueCode = #801-804 
* #8040:1 "Tumorlet o.n.A."
* #8040:1 ^property[0].code = #parent 
* #8040:1 ^property[0].valueCode = #801-804 
* #8041:3 "Kleinzelliges Karzinom o.n.A."
* #8041:3 ^designation[0].value = "Kleinzelliges Karzinom vom pulmonalen Typ" 
* #8041:3 ^designation[1].value = "Reservezellkarzinom" 
* #8041:3 ^designation[2].value = "Rundzellkarzinom" 
* #8041:3 ^designation[3].value = "Kleinzelliges neuroendokrines Karzinom" 
* #8041:3 ^property[0].code = #parent 
* #8041:3 ^property[0].valueCode = #801-804 
* #8042:3 "Haferzell-Karzinom"
* #8042:3 ^designation[0].value = "Oat-Cell-Karzinom" 
* #8042:3 ^property[0].code = #parent 
* #8042:3 ^property[0].valueCode = #801-804 
* #8043:3 "Kleinzelliges spindelzelliges Karzinom"
* #8043:3 ^property[0].code = #parent 
* #8043:3 ^property[0].valueCode = #801-804 
* #8044:3 "Kleinzelliges Karzinom vom Intermediärtyp"
* #8044:3 ^designation[0].value = "Kleinzelliges Karzinom vom hyperkalzämischen Typ" 
* #8044:3 ^property[0].code = #parent 
* #8044:3 ^property[0].valueCode = #801-804 
* #8045:3 "Kombiniertes kleinzelliges Karzinom"
* #8045:3 ^designation[0].value = "Gemischtes kleinzelliges Karzinom" 
* #8045:3 ^designation[1].value = "Kombiniertes Haferzell- und Adenokarzinom" 
* #8045:3 ^designation[2].value = "Kombiniertes Haferzell- und Plattenepithelkarzinom" 
* #8045:3 ^designation[3].value = "Kombiniertes klein- und großzelliges Karzinom" 
* #8045:3 ^property[0].code = #parent 
* #8045:3 ^property[0].valueCode = #801-804 
* #8046:3 "Nichtkleinzelliges Karzinom"
* #8046:3 ^property[0].code = #parent 
* #8046:3 ^property[0].valueCode = #801-804 
* #805-808 "Plattenepithelneoplasien"
* #805-808 ^property[0].code = #parent 
* #805-808 ^property[0].valueCode = #M 
* #805-808 ^property[1].code = #child 
* #805-808 ^property[1].valueCode = #8050:0 
* #805-808 ^property[2].code = #child 
* #805-808 ^property[2].valueCode = #8050:2 
* #805-808 ^property[3].code = #child 
* #805-808 ^property[3].valueCode = #8050:3 
* #805-808 ^property[4].code = #child 
* #805-808 ^property[4].valueCode = #8051:0 
* #805-808 ^property[5].code = #child 
* #805-808 ^property[5].valueCode = #8051:3 
* #805-808 ^property[6].code = #child 
* #805-808 ^property[6].valueCode = #8052:0 
* #805-808 ^property[7].code = #child 
* #805-808 ^property[7].valueCode = #8052:2 
* #805-808 ^property[8].code = #child 
* #805-808 ^property[8].valueCode = #8052:3 
* #805-808 ^property[9].code = #child 
* #805-808 ^property[9].valueCode = #8053:0 
* #805-808 ^property[10].code = #child 
* #805-808 ^property[10].valueCode = #8054:0 
* #805-808 ^property[11].code = #child 
* #805-808 ^property[11].valueCode = #8054:3 
* #805-808 ^property[12].code = #child 
* #805-808 ^property[12].valueCode = #8060:0 
* #805-808 ^property[13].code = #child 
* #805-808 ^property[13].valueCode = #8070:0 
* #805-808 ^property[14].code = #child 
* #805-808 ^property[14].valueCode = #8070:2 
* #805-808 ^property[15].code = #child 
* #805-808 ^property[15].valueCode = #8070:3 
* #805-808 ^property[16].code = #child 
* #805-808 ^property[16].valueCode = #8070:6 
* #805-808 ^property[17].code = #child 
* #805-808 ^property[17].valueCode = #8071:2 
* #805-808 ^property[18].code = #child 
* #805-808 ^property[18].valueCode = #8071:3 
* #805-808 ^property[19].code = #child 
* #805-808 ^property[19].valueCode = #8072:0 
* #805-808 ^property[20].code = #child 
* #805-808 ^property[20].valueCode = #8072:3 
* #805-808 ^property[21].code = #child 
* #805-808 ^property[21].valueCode = #8073:3 
* #805-808 ^property[22].code = #child 
* #805-808 ^property[22].valueCode = #8074:3 
* #805-808 ^property[23].code = #child 
* #805-808 ^property[23].valueCode = #8075:3 
* #805-808 ^property[24].code = #child 
* #805-808 ^property[24].valueCode = #8076:2 
* #805-808 ^property[25].code = #child 
* #805-808 ^property[25].valueCode = #8076:3 
* #805-808 ^property[26].code = #child 
* #805-808 ^property[26].valueCode = #8077:0 
* #805-808 ^property[27].code = #child 
* #805-808 ^property[27].valueCode = #8077:2 
* #805-808 ^property[28].code = #child 
* #805-808 ^property[28].valueCode = #8078:3 
* #805-808 ^property[29].code = #child 
* #805-808 ^property[29].valueCode = #8080:2 
* #805-808 ^property[30].code = #child 
* #805-808 ^property[30].valueCode = #8081:2 
* #805-808 ^property[31].code = #child 
* #805-808 ^property[31].valueCode = #8082:3 
* #805-808 ^property[32].code = #child 
* #805-808 ^property[32].valueCode = #8083:3 
* #805-808 ^property[33].code = #child 
* #805-808 ^property[33].valueCode = #8084:0 
* #805-808 ^property[34].code = #child 
* #805-808 ^property[34].valueCode = #8084:3 
* #805-808 ^property[35].code = #child 
* #805-808 ^property[35].valueCode = #8085:3 
* #805-808 ^property[36].code = #child 
* #805-808 ^property[36].valueCode = #8086:3 
* #8050:0 "Papillom o.n.A."
* #8050:0 ^property[0].code = #parent 
* #8050:0 ^property[0].valueCode = #805-808 
* #8050:2 "Papilläres Carcinoma in situ"
* #8050:2 ^property[0].code = #parent 
* #8050:2 ^property[0].valueCode = #805-808 
* #8050:3 "Papilläres Karzinom o.n.A."
* #8050:3 ^property[0].code = #parent 
* #8050:3 ^property[0].valueCode = #805-808 
* #8051:0 "Verruköses Papillom"
* #8051:0 ^property[0].code = #parent 
* #8051:0 ^property[0].valueCode = #805-808 
* #8051:3 "Verruköses Karzinom o.n.A."
* #8051:3 ^designation[0].value = "Verruköses Epidermoidkarzinom" 
* #8051:3 ^designation[1].value = "Verruköses Plattenepithelkarzinom" 
* #8051:3 ^property[0].code = #parent 
* #8051:3 ^property[0].valueCode = #805-808 
* #8052:0 "Plattenepithelpapillom o.n.A."
* #8052:0 ^designation[0].value = "Keratotisches Papillom" 
* #8052:0 ^designation[1].value = "Plattenepithelpapillom" 
* #8052:0 ^property[0].code = #parent 
* #8052:0 ^property[0].valueCode = #805-808 
* #8052:2 "Nichtinvasives papilläres Plattenepithelkarzinom"
* #8052:2 ^designation[0].value = "Papilläres Plattenepithel-Carcinoma in situ" 
* #8052:2 ^property[0].code = #parent 
* #8052:2 ^property[0].valueCode = #805-808 
* #8052:3 "Papilläres Plattenepithelkarzinom"
* #8052:3 ^designation[0].value = "Papilläres Epidermoidkarzinom" 
* #8052:3 ^property[0].code = #parent 
* #8052:3 ^property[0].valueCode = #805-808 
* #8053:0 "Invertiertes Plattenepithel-Papillom"
* #8053:0 ^property[0].code = #parent 
* #8053:0 ^property[0].valueCode = #805-808 
* #8054:0 "Warziges Dyskeratom"
* #8054:0 ^property[0].code = #parent 
* #8054:0 ^property[0].valueCode = #805-808 
* #8054:3 "Warziges Karzinom"
* #8054:3 ^designation[0].value = "Kondylomatöses Karzinom" 
* #8054:3 ^designation[1].value = "Warziges Basaloidkarzinom" 
* #8054:3 ^property[0].code = #parent 
* #8054:3 ^property[0].valueCode = #805-808 
* #8060:0 "Plattenepithel-Papillomatose"
* #8060:0 ^designation[0].value = "Papillomatose o.n.A." 
* #8060:0 ^property[0].code = #parent 
* #8060:0 ^property[0].valueCode = #805-808 
* #8070:0 "Aktinische Keratose"
* #8070:0 ^designation[0].value = "Arsenkeratose" 
* #8070:0 ^designation[1].value = "PUVA-Keratose" 
* #8070:0 ^property[0].code = #parent 
* #8070:0 ^property[0].valueCode = #805-808 
* #8070:2 "Plattenepithel-Carcinoma in situ o.n.A."
* #8070:2 ^designation[0].value = "Epidermoid-Carcinoma in situ o.n.A." 
* #8070:2 ^designation[1].value = "Intraepidermales Karzinom o.n.A." 
* #8070:2 ^designation[2].value = "Intraepitheliales Plattenepithelkarzinom o.n.A." 
* #8070:2 ^property[0].code = #parent 
* #8070:2 ^property[0].valueCode = #805-808 
* #8070:3 "Plattenepithelkarzinom o.n.A."
* #8070:3 ^designation[0].value = "Epidermoidkarzinom o.n.A." 
* #8070:3 ^designation[1].value = "Spinaliom" 
* #8070:3 ^property[0].code = #parent 
* #8070:3 ^property[0].valueCode = #805-808 
* #8070:6 "Plattenepithelkarzinom-Metastase o.n.A."
* #8070:6 ^property[0].code = #parent 
* #8070:6 ^property[0].valueCode = #805-808 
* #8071:2 "Differenzierte intraepitheliale Neoplasie"
* #8071:2 ^designation[0].value = "Differenzierte intraepitheliale Neoplasie des Penis (PeIN)" 
* #8071:2 ^designation[1].value = "Differenzierte vulväre intraepitheliale Neoplasie (VIN)" 
* #8071:2 ^property[0].code = #parent 
* #8071:2 ^property[0].valueCode = #805-808 
* #8071:3 "Verhornendes Plattenepithelkarzinom o.n.A."
* #8071:3 ^designation[0].value = "Großzelliges verhornendes Plattenepithelkarzinom" 
* #8071:3 ^designation[1].value = "Keratoakanthom" 
* #8071:3 ^designation[2].value = "Verhornendes Epidermoidkarzinom" 
* #8071:3 ^property[0].code = #parent 
* #8071:3 ^property[0].valueCode = #805-808 
* #8072:0 "Großzelliges Akanthom"
* #8072:0 ^property[0].code = #parent 
* #8072:0 ^property[0].valueCode = #805-808 
* #8072:3 "Großzelliges nichtverhornendes Plattenepithelkarzinom o.n.A."
* #8072:3 ^designation[0].value = "Großzelliges nichtverhornendes Epidermoidkarzinom" 
* #8072:3 ^designation[1].value = "Nichtverhornendes Plattenepithelkarzinom o.n.A." 
* #8072:3 ^property[0].code = #parent 
* #8072:3 ^property[0].valueCode = #805-808 
* #8073:3 "Kleinzelliges nichtverhornendes Plattenepithelkarzinom"
* #8073:3 ^designation[0].value = "Kleinzelliges nichtverhornendes Epidermoidkarzinom" 
* #8073:3 ^property[0].code = #parent 
* #8073:3 ^property[0].valueCode = #805-808 
* #8074:3 "Spindelzelliges Plattenepithelkarzinom"
* #8074:3 ^designation[0].value = "Sarkomatöses Plattenepithelkarzinom" 
* #8074:3 ^designation[1].value = "Spindelzelliges Epidermoidkarzinom" 
* #8074:3 ^designation[2].value = "Pseudovaskuläres Plattenepithelkarzinom" 
* #8074:3 ^property[0].code = #parent 
* #8074:3 ^property[0].valueCode = #805-808 
* #8075:3 "Adenoides Plattenepithelkarzinom"
* #8075:3 ^designation[0].value = "Akantholytisches Plattenepithelkarzinom" 
* #8075:3 ^designation[1].value = "Pseudoglanduläres Plattenepithelkarzinom" 
* #8075:3 ^property[0].code = #parent 
* #8075:3 ^property[0].valueCode = #805-808 
* #8076:2 "Plattenepithel-Carcinoma in situ mit fraglicher Stromainvasion"
* #8076:2 ^designation[0].value = "Epidermoid-Carcinoma in situ mit fraglicher Stromainvasion" 
* #8076:2 ^property[0].code = #parent 
* #8076:2 ^property[0].valueCode = #805-808 
* #8076:3 "Mikroinvasives Plattenepithelkarzinom"
* #8076:3 ^property[0].code = #parent 
* #8076:3 ^property[0].valueCode = #805-808 
* #8077:0 "Squamöse intraepitheliale Neoplasie, niedriggradig"
* #8077:0 ^definition = Siehe Kodierrichtlinien
* #8077:0 ^designation[0].value = "Niedriggradige squamöse intraeptheliale Läsion" 
* #8077:0 ^designation[1].value = "Squamöse intraepitheliale Neoplasie, Grad 1" 
* #8077:0 ^designation[2].value = "Anale intraepitheliale Neoplasie, niedriggradig" 
* #8077:0 ^designation[3].value = "Ösophageale squamöse intraepitheliale Neoplasie (Dysplasie), niedriggradig" 
* #8077:0 ^designation[4].value = "Zervikale intraepitheliale Neoplasie, niedriggradig" 
* #8077:0 ^property[0].code = #parent 
* #8077:0 ^property[0].valueCode = #805-808 
* #8077:2 "Squamöse intraepitheliale Neoplasie, hochgradig"
* #8077:2 ^definition = Siehe Kodierrichtlinien
* #8077:2 ^designation[0].value = "Hochgradige squamöse Dysplasie" 
* #8077:2 ^designation[1].value = "Hochgradige squamöse intraepitheliale Läsion" 
* #8077:2 ^designation[2].value = "Squamöse intraepitheliale Neoplasie, Grad 2" 
* #8077:2 ^designation[3].value = "Squamöse intraepitheliale Neoplasie, Grad 3" 
* #8077:2 ^designation[4].value = "Anale intraepitheliale Neoplasie Grad 3AIN 3" 
* #8077:2 ^designation[5].value = "Ösophageale squamöse intraepitheliale Neoplasie (Dysplasie), hochgradig" 
* #8077:2 ^designation[6].value = "Vaginale intraepitheliale Neoplasie Grad 3VAIN 3" 
* #8077:2 ^designation[7].value = "Vulväre intraepitheliale Neoplasie Grad3VIN 3" 
* #8077:2 ^designation[8].value = "Zervikale intraepitheliale Neoplasie Grad 3CIN 3 mit schwerer Dysplasie" 
* #8077:2 ^designation[9].value = "Zervikale intraepitheliale Neoplasie Grad 3CIN 3 o.n.A." 
* #8077:2 ^property[0].code = #parent 
* #8077:2 ^property[0].valueCode = #805-808 
* #8078:3 "Plattenepithelkarzinom mit Hornbildung"
* #8078:3 ^property[0].code = #parent 
* #8078:3 ^property[0].valueCode = #805-808 
* #8080:2 "Erythroplasie Queyrat"
* #8080:2 ^property[0].code = #parent 
* #8080:2 ^property[0].valueCode = #805-808 
* #8081:2 "M. Bowen"
* #8081:2 ^designation[0].value = "Intraepitheliales Plattenepithelkarzinom vom Bowen-Typ" 
* #8081:2 ^property[0].code = #parent 
* #8081:2 ^property[0].valueCode = #805-808 
* #8082:3 "Lymphoepitheliales Karzinom"
* #8082:3 ^designation[0].value = "Lymphoepitheliales Plattenepithelkarzinom" 
* #8082:3 ^designation[1].value = "Lymphoepitheliom" 
* #8082:3 ^designation[2].value = "Schmincke-Tumor" 
* #8082:3 ^property[0].code = #parent 
* #8082:3 ^property[0].valueCode = #805-808 
* #8083:3 "Basaloides Plattenepithelkarzinom"
* #8083:3 ^designation[0].value = "Papilläres Basaloidkarzinom" 
* #8083:3 ^property[0].code = #parent 
* #8083:3 ^property[0].valueCode = #805-808 
* #8084:0 "Klarzellakanthom"
* #8084:0 ^property[0].code = #parent 
* #8084:0 ^property[0].valueCode = #805-808 
* #8084:3 "Klarzelliges Plattenepithelkarzinom"
* #8084:3 ^property[0].code = #parent 
* #8084:3 ^property[0].valueCode = #805-808 
* #8085:3 "Plattenepithelkarzinom, HPV-positiv"
* #8085:3 ^property[0].code = #parent 
* #8085:3 ^property[0].valueCode = #805-808 
* #8086:3 "Plattenepithelkarzinom, HPV-negativ"
* #8086:3 ^property[0].code = #parent 
* #8086:3 ^property[0].valueCode = #805-808 
* #809-811 "Basalzellneoplasien"
* #809-811 ^property[0].code = #parent 
* #809-811 ^property[0].valueCode = #M 
* #809-811 ^property[1].code = #child 
* #809-811 ^property[1].valueCode = #8090:1 
* #809-811 ^property[2].code = #child 
* #809-811 ^property[2].valueCode = #8090:3 
* #809-811 ^property[3].code = #child 
* #809-811 ^property[3].valueCode = #8091:3 
* #809-811 ^property[4].code = #child 
* #809-811 ^property[4].valueCode = #8092:3 
* #809-811 ^property[5].code = #child 
* #809-811 ^property[5].valueCode = #8093:3 
* #809-811 ^property[6].code = #child 
* #809-811 ^property[6].valueCode = #8094:3 
* #809-811 ^property[7].code = #child 
* #809-811 ^property[7].valueCode = #8095:3 
* #809-811 ^property[8].code = #child 
* #809-811 ^property[8].valueCode = #8096:0 
* #809-811 ^property[9].code = #child 
* #809-811 ^property[9].valueCode = #8097:3 
* #809-811 ^property[10].code = #child 
* #809-811 ^property[10].valueCode = #8098:3 
* #809-811 ^property[11].code = #child 
* #809-811 ^property[11].valueCode = #8100:0 
* #809-811 ^property[12].code = #child 
* #809-811 ^property[12].valueCode = #8100:3 
* #809-811 ^property[13].code = #child 
* #809-811 ^property[13].valueCode = #8101:0 
* #809-811 ^property[14].code = #child 
* #809-811 ^property[14].valueCode = #8102:0 
* #809-811 ^property[15].code = #child 
* #809-811 ^property[15].valueCode = #8102:3 
* #809-811 ^property[16].code = #child 
* #809-811 ^property[16].valueCode = #8103:0 
* #809-811 ^property[17].code = #child 
* #809-811 ^property[17].valueCode = #8103:1 
* #809-811 ^property[18].code = #child 
* #809-811 ^property[18].valueCode = #8104:0 
* #809-811 ^property[19].code = #child 
* #809-811 ^property[19].valueCode = #8110:0 
* #809-811 ^property[20].code = #child 
* #809-811 ^property[20].valueCode = #8110:3 
* #8090:1 "Basalzelltumor"
* #8090:1 ^property[0].code = #parent 
* #8090:1 ^property[0].valueCode = #809-811 
* #8090:3 "Basalzellkarzinom o.n.A."
* #8090:3 ^designation[0].value = "Basalzellepitheliom" 
* #8090:3 ^designation[1].value = "Ulcus rodens" 
* #8090:3 ^designation[2].value = "Basalzellkarzinom mit adnexaler Differenzierung" 
* #8090:3 ^designation[3].value = "Pigmentiertes Basalzellkarzinom" 
* #8090:3 ^property[0].code = #parent 
* #8090:3 ^property[0].valueCode = #809-811 
* #8091:3 "Superfizielles Basalzellkarzinom"
* #8091:3 ^designation[0].value = "Multifokales oberflächliches Basalzellkarzinom" 
* #8091:3 ^designation[1].value = "Multizentrisches Basaliom" 
* #8091:3 ^property[0].code = #parent 
* #8091:3 ^property[0].valueCode = #809-811 
* #8092:3 "Infiltrierendes Basalzellkarzinom o.n.A."
* #8092:3 ^designation[0].value = "Nicht sklerosierendes infiltrierendes Basalzellkarzinom" 
* #8092:3 ^designation[1].value = "Sklerosierendes infiltrierendes BasalzellkarzinomBasalzellkarzinom vom Morpheatyp" 
* #8092:3 ^designation[2].value = "Sklerosierendes infiltrierendes BasalzellkarzinomDesmoplastisches Basalzellkarzinom" 
* #8092:3 ^property[0].code = #parent 
* #8092:3 ^property[0].valueCode = #809-811 
* #8093:3 "Fibroepitheliales Basalzellkarzinom"
* #8093:3 ^designation[0].value = "Fibroepitheliales Basalzellkarzinom Pinkus" 
* #8093:3 ^designation[1].value = "Fibroepitheliom o.n.A." 
* #8093:3 ^designation[2].value = "Fibroepitheliom Pinkus" 
* #8093:3 ^designation[3].value = "Pinkus-Tumor" 
* #8093:3 ^property[0].code = #parent 
* #8093:3 ^property[0].valueCode = #809-811 
* #8094:3 "Basosquamöses Karzinom"
* #8094:3 ^designation[0].value = "Gemischt basalzellig-plattenepitheliales Karzinom" 
* #8094:3 ^property[0].code = #parent 
* #8094:3 ^property[0].valueCode = #809-811 
* #8095:3 "Metatypisches Karzinom"
* #8095:3 ^property[0].code = #parent 
* #8095:3 ^property[0].valueCode = #809-811 
* #8096:0 "Intraepidermales Epitheliom Typ Borst-Jadassohn"
* #8096:0 ^property[0].code = #parent 
* #8096:0 ^property[0].valueCode = #809-811 
* #8097:3 "Noduläres Basalzellkarzinom"
* #8097:3 ^designation[0].value = "Mikronoduläres Basalzellkarzinom" 
* #8097:3 ^property[0].code = #parent 
* #8097:3 ^property[0].valueCode = #809-811 
* #8098:3 "Adenoides Basalzellkarzinom"
* #8098:3 ^property[0].code = #parent 
* #8098:3 ^property[0].valueCode = #809-811 
* #8100:0 "Trichoepitheliom"
* #8100:0 ^designation[0].value = "Brooke-Tumor" 
* #8100:0 ^designation[1].value = "Epithelioma adenoides cysticum" 
* #8100:0 ^property[0].code = #parent 
* #8100:0 ^property[0].valueCode = #809-811 
* #8100:3 "Trichoblastisches Karzinom"
* #8100:3 ^designation[0].value = "Trichoblastisches Karzinosarkom" 
* #8100:3 ^property[0].code = #parent 
* #8100:3 ^property[0].valueCode = #809-811 
* #8101:0 "Trichofollikulom"
* #8101:0 ^property[0].code = #parent 
* #8101:0 ^property[0].valueCode = #809-811 
* #8102:0 "Tricholemmom"
* #8102:0 ^property[0].code = #parent 
* #8102:0 ^property[0].valueCode = #809-811 
* #8102:3 "Tricholemmkarzinom"
* #8102:3 ^designation[0].value = "Tricholemmales Karzinom" 
* #8102:3 ^property[0].code = #parent 
* #8102:3 ^property[0].valueCode = #809-811 
* #8103:0 "Pilartumor"
* #8103:0 ^property[0].code = #parent 
* #8103:0 ^property[0].valueCode = #809-811 
* #8103:1 "Proliferierende Tricholemmzyste"
* #8103:1 ^designation[0].value = "Proliferierender tricholemmaler Tumor" 
* #8103:1 ^property[0].code = #parent 
* #8103:1 ^property[0].valueCode = #809-811 
* #8104:0 "Pilarscheidenakanthom"
* #8104:0 ^designation[0].value = "Tumor des follikulären Infundibulums" 
* #8104:0 ^property[0].code = #parent 
* #8104:0 ^property[0].valueCode = #809-811 
* #8110:0 "Pilomatrikom, o.n.A."
* #8110:0 ^designation[0].value = "Pilomatrixom, o.n.A." 
* #8110:0 ^designation[1].value = "Verkalkendes Epitheliom Malherbe" 
* #8110:0 ^designation[2].value = "Melanozytisches Matrikom" 
* #8110:0 ^property[0].code = #parent 
* #8110:0 ^property[0].valueCode = #809-811 
* #8110:3 "Pilomatrikales Karzinom"
* #8110:3 ^designation[0].value = "Malignes Pilomatrikom" 
* #8110:3 ^designation[1].value = "Malignes Pilomatrixom" 
* #8110:3 ^designation[2].value = "Matrikales Karzinom" 
* #8110:3 ^designation[3].value = "Pilomatrix-Karzinom" 
* #8110:3 ^property[0].code = #parent 
* #8110:3 ^property[0].valueCode = #809-811 
* #812-813 "Übergangszellpapillome und -karzinome"
* #812-813 ^property[0].code = #parent 
* #812-813 ^property[0].valueCode = #M 
* #812-813 ^property[1].code = #child 
* #812-813 ^property[1].valueCode = #8120:0 
* #812-813 ^property[2].code = #child 
* #812-813 ^property[2].valueCode = #8120:2 
* #812-813 ^property[3].code = #child 
* #812-813 ^property[3].valueCode = #8120:3 
* #812-813 ^property[4].code = #child 
* #812-813 ^property[4].valueCode = #8121:0 
* #812-813 ^property[5].code = #child 
* #812-813 ^property[5].valueCode = #8121:1 
* #812-813 ^property[6].code = #child 
* #812-813 ^property[6].valueCode = #8121:3 
* #812-813 ^property[7].code = #child 
* #812-813 ^property[7].valueCode = #8122:3 
* #812-813 ^property[8].code = #child 
* #812-813 ^property[8].valueCode = #8123:3 
* #812-813 ^property[9].code = #child 
* #812-813 ^property[9].valueCode = #8124:3 
* #812-813 ^property[10].code = #child 
* #812-813 ^property[10].valueCode = #8130:1 
* #812-813 ^property[11].code = #child 
* #812-813 ^property[11].valueCode = #8130:2 
* #812-813 ^property[12].code = #child 
* #812-813 ^property[12].valueCode = #8130:3 
* #812-813 ^property[13].code = #child 
* #812-813 ^property[13].valueCode = #8131:3 
* #8120:0 "Urothelpapillom o.n.A."
* #8120:0 ^designation[0].value = "Benignes Übergangszellpapillom [obs.]" 
* #8120:0 ^designation[1].value = "Harnblasenpapillom" 
* #8120:0 ^designation[2].value = "Übergangspapillom [obs.]" 
* #8120:0 ^designation[3].value = "Übergangszellpapillom o.n.A." 
* #8120:0 ^property[0].code = #parent 
* #8120:0 ^property[0].valueCode = #812-813 
* #8120:2 "Urothel-Carcinoma in situ"
* #8120:2 ^designation[0].value = "Übergangszell-Carcinoma in situ" 
* #8120:2 ^property[0].code = #parent 
* #8120:2 ^property[0].valueCode = #812-813 
* #8120:3 "Übergangszellkarzinom o.n.A."
* #8120:3 ^designation[0].value = "Urothelkarzinom o.n.A." 
* #8120:3 ^designation[1].value = "Squamöses Übergangszellkarzinom" 
* #8120:3 ^designation[2].value = "Transitionalkarzinom" 
* #8120:3 ^property[0].code = #parent 
* #8120:3 ^property[0].valueCode = #812-813 
* #8121:0 "Exophytisches sinonasales Papillom"
* #8121:0 ^designation[0].value = "Fungiformes sinonasales Papillom" 
* #8121:0 ^designation[1].value = "Schneider-Papillom o.n.A." 
* #8121:0 ^designation[2].value = "Sinonasales Papillom o.n.A." 
* #8121:0 ^designation[3].value = "Invertiertes Transitionalpapillom o.n.A.Invertiertes Übergangszellpapillom o.n.A." 
* #8121:0 ^designation[4].value = "Invertiertes Urothelpapillom" 
* #8121:0 ^property[0].code = #parent 
* #8121:0 ^property[0].valueCode = #812-813 
* #8121:1 "Invertiertes sinonasales Papillom"
* #8121:1 ^designation[0].value = "Invertiertes Schneider-Papillom" 
* #8121:1 ^designation[1].value = "Onkozytäres Schneider-Papillom" 
* #8121:1 ^designation[2].value = "Onkozytäres sinonasales Papillom" 
* #8121:1 ^designation[3].value = "Säulenzell-Papillom" 
* #8121:1 ^designation[4].value = "Zylinderzell-Papillom" 
* #8121:1 ^property[0].code = #parent 
* #8121:1 ^property[0].valueCode = #812-813 
* #8121:3 "Schneider-Karzinom"
* #8121:3 ^designation[0].value = "Zylinderzellkarzinom" 
* #8121:3 ^property[0].code = #parent 
* #8121:3 ^property[0].valueCode = #812-813 
* #8122:3 "Sarkomatoides Urothelkarzinom"
* #8122:3 ^designation[0].value = "Sarkomartiges Übergangszellkarzinom" 
* #8122:3 ^designation[1].value = "Spindelzelliges Übergangszellkarzinom" 
* #8122:3 ^designation[2].value = "Spindelzelliges Urothelkarzinom" 
* #8122:3 ^property[0].code = #parent 
* #8122:3 ^property[0].valueCode = #812-813 
* #8123:3 "Basaloidkarzinom"
* #8123:3 ^property[0].code = #parent 
* #8123:3 ^property[0].valueCode = #812-813 
* #8124:3 "Kloakogenes Karzinom"
* #8124:3 ^property[0].code = #parent 
* #8124:3 ^property[0].valueCode = #812-813 
* #8130:1 "Papilläre Neoplasie des Urothels mit niedrigem Malignitätspotential"
* #8130:1 ^designation[0].value = "Papilläre Neoplasie der Übergangszellen mit niedrigem Malignitätspotential" 
* #8130:1 ^property[0].code = #parent 
* #8130:1 ^property[0].valueCode = #812-813 
* #8130:2 "Nichtinvasives papilläres Urothelkarzinom"
* #8130:2 ^designation[0].value = "Nichtinvasives papilläres Übergangszellkarzinom" 
* #8130:2 ^property[0].code = #parent 
* #8130:2 ^property[0].valueCode = #812-813 
* #8130:3 "Papilläres Urothelkarzinom"
* #8130:3 ^designation[0].value = "Papilläres Übergangszellkarzinom" 
* #8130:3 ^property[0].code = #parent 
* #8130:3 ^property[0].valueCode = #812-813 
* #8131:3 "Mikropapilläres Urothelkarzinom"
* #8131:3 ^designation[0].value = "Mikropapilläres Übergangszellkarzinom" 
* #8131:3 ^property[0].code = #parent 
* #8131:3 ^property[0].valueCode = #812-813 
* #814-838 "Adenome und Adenokarzinome"
* #814-838 ^property[0].code = #parent 
* #814-838 ^property[0].valueCode = #M 
* #814-838 ^property[1].code = #child 
* #814-838 ^property[1].valueCode = #8140:0 
* #814-838 ^property[2].code = #child 
* #814-838 ^property[2].valueCode = #8140:1 
* #814-838 ^property[3].code = #child 
* #814-838 ^property[3].valueCode = #8140:2 
* #814-838 ^property[4].code = #child 
* #814-838 ^property[4].valueCode = #8140:3 
* #814-838 ^property[5].code = #child 
* #814-838 ^property[5].valueCode = #8140:6 
* #814-838 ^property[6].code = #child 
* #814-838 ^property[6].valueCode = #8141:3 
* #814-838 ^property[7].code = #child 
* #814-838 ^property[7].valueCode = #8142:3 
* #814-838 ^property[8].code = #child 
* #814-838 ^property[8].valueCode = #8143:3 
* #814-838 ^property[9].code = #child 
* #814-838 ^property[9].valueCode = #8144:0 
* #814-838 ^property[10].code = #child 
* #814-838 ^property[10].valueCode = #8144:3 
* #814-838 ^property[11].code = #child 
* #814-838 ^property[11].valueCode = #8145:3 
* #814-838 ^property[12].code = #child 
* #814-838 ^property[12].valueCode = #8146:0 
* #814-838 ^property[13].code = #child 
* #814-838 ^property[13].valueCode = #8147:0 
* #814-838 ^property[14].code = #child 
* #814-838 ^property[14].valueCode = #8147:3 
* #814-838 ^property[15].code = #child 
* #814-838 ^property[15].valueCode = #8148:0 
* #814-838 ^property[16].code = #child 
* #814-838 ^property[16].valueCode = #8148:2 
* #814-838 ^property[17].code = #child 
* #814-838 ^property[17].valueCode = #8149:0 
* #814-838 ^property[18].code = #child 
* #814-838 ^property[18].valueCode = #8150:0 
* #814-838 ^property[19].code = #child 
* #814-838 ^property[19].valueCode = #8150:3 
* #814-838 ^property[20].code = #child 
* #814-838 ^property[20].valueCode = #8151:3 
* #814-838 ^property[21].code = #child 
* #814-838 ^property[21].valueCode = #8152:3 
* #814-838 ^property[22].code = #child 
* #814-838 ^property[22].valueCode = #8153:1 
* #814-838 ^property[23].code = #child 
* #814-838 ^property[23].valueCode = #8153:3 
* #814-838 ^property[24].code = #child 
* #814-838 ^property[24].valueCode = #8154:3 
* #814-838 ^property[25].code = #child 
* #814-838 ^property[25].valueCode = #8155:1 
* #814-838 ^property[26].code = #child 
* #814-838 ^property[26].valueCode = #8155:3 
* #814-838 ^property[27].code = #child 
* #814-838 ^property[27].valueCode = #8156:3 
* #814-838 ^property[28].code = #child 
* #814-838 ^property[28].valueCode = #8158:3 
* #814-838 ^property[29].code = #child 
* #814-838 ^property[29].valueCode = #8160:0 
* #814-838 ^property[30].code = #child 
* #814-838 ^property[30].valueCode = #8160:3 
* #814-838 ^property[31].code = #child 
* #814-838 ^property[31].valueCode = #8161:0 
* #814-838 ^property[32].code = #child 
* #814-838 ^property[32].valueCode = #8161:3 
* #814-838 ^property[33].code = #child 
* #814-838 ^property[33].valueCode = #8162:3 
* #814-838 ^property[34].code = #child 
* #814-838 ^property[34].valueCode = #8163:0 
* #814-838 ^property[35].code = #child 
* #814-838 ^property[35].valueCode = #8163:2 
* #814-838 ^property[36].code = #child 
* #814-838 ^property[36].valueCode = #8163:3 
* #814-838 ^property[37].code = #child 
* #814-838 ^property[37].valueCode = #8170:0 
* #814-838 ^property[38].code = #child 
* #814-838 ^property[38].valueCode = #8170:3 
* #814-838 ^property[39].code = #child 
* #814-838 ^property[39].valueCode = #8171:3 
* #814-838 ^property[40].code = #child 
* #814-838 ^property[40].valueCode = #8172:3 
* #814-838 ^property[41].code = #child 
* #814-838 ^property[41].valueCode = #8173:3 
* #814-838 ^property[42].code = #child 
* #814-838 ^property[42].valueCode = #8174:3 
* #814-838 ^property[43].code = #child 
* #814-838 ^property[43].valueCode = #8175:3 
* #814-838 ^property[44].code = #child 
* #814-838 ^property[44].valueCode = #8180:3 
* #814-838 ^property[45].code = #child 
* #814-838 ^property[45].valueCode = #8190:0 
* #814-838 ^property[46].code = #child 
* #814-838 ^property[46].valueCode = #8190:3 
* #814-838 ^property[47].code = #child 
* #814-838 ^property[47].valueCode = #8191:0 
* #814-838 ^property[48].code = #child 
* #814-838 ^property[48].valueCode = #8200:0 
* #814-838 ^property[49].code = #child 
* #814-838 ^property[49].valueCode = #8200:3 
* #814-838 ^property[50].code = #child 
* #814-838 ^property[50].valueCode = #8201:2 
* #814-838 ^property[51].code = #child 
* #814-838 ^property[51].valueCode = #8201:3 
* #814-838 ^property[52].code = #child 
* #814-838 ^property[52].valueCode = #8202:0 
* #814-838 ^property[53].code = #child 
* #814-838 ^property[53].valueCode = #8204:0 
* #814-838 ^property[54].code = #child 
* #814-838 ^property[54].valueCode = #8210:0 
* #814-838 ^property[55].code = #child 
* #814-838 ^property[55].valueCode = #8210:2 
* #814-838 ^property[56].code = #child 
* #814-838 ^property[56].valueCode = #8210:3 
* #814-838 ^property[57].code = #child 
* #814-838 ^property[57].valueCode = #8211:0 
* #814-838 ^property[58].code = #child 
* #814-838 ^property[58].valueCode = #8211:3 
* #814-838 ^property[59].code = #child 
* #814-838 ^property[59].valueCode = #8212:0 
* #814-838 ^property[60].code = #child 
* #814-838 ^property[60].valueCode = #8213:0 
* #814-838 ^property[61].code = #child 
* #814-838 ^property[61].valueCode = #8213:3 
* #814-838 ^property[62].code = #child 
* #814-838 ^property[62].valueCode = #8214:3 
* #814-838 ^property[63].code = #child 
* #814-838 ^property[63].valueCode = #8215:3 
* #814-838 ^property[64].code = #child 
* #814-838 ^property[64].valueCode = #8220:0 
* #814-838 ^property[65].code = #child 
* #814-838 ^property[65].valueCode = #8220:3 
* #814-838 ^property[66].code = #child 
* #814-838 ^property[66].valueCode = #8221:0 
* #814-838 ^property[67].code = #child 
* #814-838 ^property[67].valueCode = #8221:3 
* #814-838 ^property[68].code = #child 
* #814-838 ^property[68].valueCode = #8230:2 
* #814-838 ^property[69].code = #child 
* #814-838 ^property[69].valueCode = #8230:3 
* #814-838 ^property[70].code = #child 
* #814-838 ^property[70].valueCode = #8231:3 
* #814-838 ^property[71].code = #child 
* #814-838 ^property[71].valueCode = #8240:3 
* #814-838 ^property[72].code = #child 
* #814-838 ^property[72].valueCode = #8241:3 
* #814-838 ^property[73].code = #child 
* #814-838 ^property[73].valueCode = #8242:3 
* #814-838 ^property[74].code = #child 
* #814-838 ^property[74].valueCode = #8243:3 
* #814-838 ^property[75].code = #child 
* #814-838 ^property[75].valueCode = #8244:3 
* #814-838 ^property[76].code = #child 
* #814-838 ^property[76].valueCode = #8245:1 
* #814-838 ^property[77].code = #child 
* #814-838 ^property[77].valueCode = #8245:3 
* #814-838 ^property[78].code = #child 
* #814-838 ^property[78].valueCode = #8246:3 
* #814-838 ^property[79].code = #child 
* #814-838 ^property[79].valueCode = #8247:3 
* #814-838 ^property[80].code = #child 
* #814-838 ^property[80].valueCode = #8248:1 
* #814-838 ^property[81].code = #child 
* #814-838 ^property[81].valueCode = #8249:3 
* #814-838 ^property[82].code = #child 
* #814-838 ^property[82].valueCode = #8250:0 
* #814-838 ^property[83].code = #child 
* #814-838 ^property[83].valueCode = #8250:1 
* #814-838 ^property[84].code = #child 
* #814-838 ^property[84].valueCode = #8250:2 
* #814-838 ^property[85].code = #child 
* #814-838 ^property[85].valueCode = #8250:3 
* #814-838 ^property[86].code = #child 
* #814-838 ^property[86].valueCode = #8251:0 
* #814-838 ^property[87].code = #child 
* #814-838 ^property[87].valueCode = #8251:3 
* #814-838 ^property[88].code = #child 
* #814-838 ^property[88].valueCode = #8252:3 
* #814-838 ^property[89].code = #child 
* #814-838 ^property[89].valueCode = #8253:2 
* #814-838 ^property[90].code = #child 
* #814-838 ^property[90].valueCode = #8253:3 
* #814-838 ^property[91].code = #child 
* #814-838 ^property[91].valueCode = #8254:3 
* #814-838 ^property[92].code = #child 
* #814-838 ^property[92].valueCode = #8255:3 
* #814-838 ^property[93].code = #child 
* #814-838 ^property[93].valueCode = #8256:3 
* #814-838 ^property[94].code = #child 
* #814-838 ^property[94].valueCode = #8257:3 
* #814-838 ^property[95].code = #child 
* #814-838 ^property[95].valueCode = #8260:0 
* #814-838 ^property[96].code = #child 
* #814-838 ^property[96].valueCode = #8260:1 
* #814-838 ^property[97].code = #child 
* #814-838 ^property[97].valueCode = #8260:3 
* #814-838 ^property[98].code = #child 
* #814-838 ^property[98].valueCode = #8261:0 
* #814-838 ^property[99].code = #child 
* #814-838 ^property[99].valueCode = #8261:2 
* #814-838 ^property[100].code = #child 
* #814-838 ^property[100].valueCode = #8261:3 
* #814-838 ^property[101].code = #child 
* #814-838 ^property[101].valueCode = #8262:3 
* #814-838 ^property[102].code = #child 
* #814-838 ^property[102].valueCode = #8263:0 
* #814-838 ^property[103].code = #child 
* #814-838 ^property[103].valueCode = #8263:2 
* #814-838 ^property[104].code = #child 
* #814-838 ^property[104].valueCode = #8263:3 
* #814-838 ^property[105].code = #child 
* #814-838 ^property[105].valueCode = #8264:0 
* #814-838 ^property[106].code = #child 
* #814-838 ^property[106].valueCode = #8265:3 
* #814-838 ^property[107].code = #child 
* #814-838 ^property[107].valueCode = #8270:0 
* #814-838 ^property[108].code = #child 
* #814-838 ^property[108].valueCode = #8270:3 
* #814-838 ^property[109].code = #child 
* #814-838 ^property[109].valueCode = #8271:0 
* #814-838 ^property[110].code = #child 
* #814-838 ^property[110].valueCode = #8272:0 
* #814-838 ^property[111].code = #child 
* #814-838 ^property[111].valueCode = #8272:3 
* #814-838 ^property[112].code = #child 
* #814-838 ^property[112].valueCode = #8273:3 
* #814-838 ^property[113].code = #child 
* #814-838 ^property[113].valueCode = #8280:0 
* #814-838 ^property[114].code = #child 
* #814-838 ^property[114].valueCode = #8280:3 
* #814-838 ^property[115].code = #child 
* #814-838 ^property[115].valueCode = #8281:0 
* #814-838 ^property[116].code = #child 
* #814-838 ^property[116].valueCode = #8281:3 
* #814-838 ^property[117].code = #child 
* #814-838 ^property[117].valueCode = #8290:0 
* #814-838 ^property[118].code = #child 
* #814-838 ^property[118].valueCode = #8290:3 
* #814-838 ^property[119].code = #child 
* #814-838 ^property[119].valueCode = #8300:0 
* #814-838 ^property[120].code = #child 
* #814-838 ^property[120].valueCode = #8300:3 
* #814-838 ^property[121].code = #child 
* #814-838 ^property[121].valueCode = #8310:0 
* #814-838 ^property[122].code = #child 
* #814-838 ^property[122].valueCode = #8310:3 
* #814-838 ^property[123].code = #child 
* #814-838 ^property[123].valueCode = #8311:1 
* #814-838 ^property[124].code = #child 
* #814-838 ^property[124].valueCode = #8311:3 
* #814-838 ^property[125].code = #child 
* #814-838 ^property[125].valueCode = #8312:3 
* #814-838 ^property[126].code = #child 
* #814-838 ^property[126].valueCode = #8313:0 
* #814-838 ^property[127].code = #child 
* #814-838 ^property[127].valueCode = #8313:1 
* #814-838 ^property[128].code = #child 
* #814-838 ^property[128].valueCode = #8313:3 
* #814-838 ^property[129].code = #child 
* #814-838 ^property[129].valueCode = #8314:3 
* #814-838 ^property[130].code = #child 
* #814-838 ^property[130].valueCode = #8315:3 
* #814-838 ^property[131].code = #child 
* #814-838 ^property[131].valueCode = #8316:1 
* #814-838 ^property[132].code = #child 
* #814-838 ^property[132].valueCode = #8316:3 
* #814-838 ^property[133].code = #child 
* #814-838 ^property[133].valueCode = #8317:3 
* #814-838 ^property[134].code = #child 
* #814-838 ^property[134].valueCode = #8318:3 
* #814-838 ^property[135].code = #child 
* #814-838 ^property[135].valueCode = #8319:3 
* #814-838 ^property[136].code = #child 
* #814-838 ^property[136].valueCode = #8320:3 
* #814-838 ^property[137].code = #child 
* #814-838 ^property[137].valueCode = #8321:0 
* #814-838 ^property[138].code = #child 
* #814-838 ^property[138].valueCode = #8322:0 
* #814-838 ^property[139].code = #child 
* #814-838 ^property[139].valueCode = #8322:3 
* #814-838 ^property[140].code = #child 
* #814-838 ^property[140].valueCode = #8323:0 
* #814-838 ^property[141].code = #child 
* #814-838 ^property[141].valueCode = #8323:1 
* #814-838 ^property[142].code = #child 
* #814-838 ^property[142].valueCode = #8323:3 
* #814-838 ^property[143].code = #child 
* #814-838 ^property[143].valueCode = #8324:0 
* #814-838 ^property[144].code = #child 
* #814-838 ^property[144].valueCode = #8325:0 
* #814-838 ^property[145].code = #child 
* #814-838 ^property[145].valueCode = #8330:0 
* #814-838 ^property[146].code = #child 
* #814-838 ^property[146].valueCode = #8330:1 
* #814-838 ^property[147].code = #child 
* #814-838 ^property[147].valueCode = #8330:3 
* #814-838 ^property[148].code = #child 
* #814-838 ^property[148].valueCode = #8331:3 
* #814-838 ^property[149].code = #child 
* #814-838 ^property[149].valueCode = #8332:3 
* #814-838 ^property[150].code = #child 
* #814-838 ^property[150].valueCode = #8333:0 
* #814-838 ^property[151].code = #child 
* #814-838 ^property[151].valueCode = #8333:3 
* #814-838 ^property[152].code = #child 
* #814-838 ^property[152].valueCode = #8334:0 
* #814-838 ^property[153].code = #child 
* #814-838 ^property[153].valueCode = #8335:1 
* #814-838 ^property[154].code = #child 
* #814-838 ^property[154].valueCode = #8335:3 
* #814-838 ^property[155].code = #child 
* #814-838 ^property[155].valueCode = #8336:1 
* #814-838 ^property[156].code = #child 
* #814-838 ^property[156].valueCode = #8337:3 
* #814-838 ^property[157].code = #child 
* #814-838 ^property[157].valueCode = #8339:3 
* #814-838 ^property[158].code = #child 
* #814-838 ^property[158].valueCode = #8340:3 
* #814-838 ^property[159].code = #child 
* #814-838 ^property[159].valueCode = #8341:3 
* #814-838 ^property[160].code = #child 
* #814-838 ^property[160].valueCode = #8342:3 
* #814-838 ^property[161].code = #child 
* #814-838 ^property[161].valueCode = #8343:3 
* #814-838 ^property[162].code = #child 
* #814-838 ^property[162].valueCode = #8344:3 
* #814-838 ^property[163].code = #child 
* #814-838 ^property[163].valueCode = #8345:3 
* #814-838 ^property[164].code = #child 
* #814-838 ^property[164].valueCode = #8346:3 
* #814-838 ^property[165].code = #child 
* #814-838 ^property[165].valueCode = #8347:3 
* #814-838 ^property[166].code = #child 
* #814-838 ^property[166].valueCode = #8348:1 
* #814-838 ^property[167].code = #child 
* #814-838 ^property[167].valueCode = #8349:1 
* #814-838 ^property[168].code = #child 
* #814-838 ^property[168].valueCode = #8350:3 
* #814-838 ^property[169].code = #child 
* #814-838 ^property[169].valueCode = #8360:1 
* #814-838 ^property[170].code = #child 
* #814-838 ^property[170].valueCode = #8361:0 
* #814-838 ^property[171].code = #child 
* #814-838 ^property[171].valueCode = #8370:0 
* #814-838 ^property[172].code = #child 
* #814-838 ^property[172].valueCode = #8370:3 
* #814-838 ^property[173].code = #child 
* #814-838 ^property[173].valueCode = #8371:0 
* #814-838 ^property[174].code = #child 
* #814-838 ^property[174].valueCode = #8372:0 
* #814-838 ^property[175].code = #child 
* #814-838 ^property[175].valueCode = #8373:0 
* #814-838 ^property[176].code = #child 
* #814-838 ^property[176].valueCode = #8374:0 
* #814-838 ^property[177].code = #child 
* #814-838 ^property[177].valueCode = #8375:0 
* #814-838 ^property[178].code = #child 
* #814-838 ^property[178].valueCode = #8380:0 
* #814-838 ^property[179].code = #child 
* #814-838 ^property[179].valueCode = #8380:1 
* #814-838 ^property[180].code = #child 
* #814-838 ^property[180].valueCode = #8380:2 
* #814-838 ^property[181].code = #child 
* #814-838 ^property[181].valueCode = #8380:3 
* #814-838 ^property[182].code = #child 
* #814-838 ^property[182].valueCode = #8381:0 
* #814-838 ^property[183].code = #child 
* #814-838 ^property[183].valueCode = #8381:1 
* #814-838 ^property[184].code = #child 
* #814-838 ^property[184].valueCode = #8381:3 
* #814-838 ^property[185].code = #child 
* #814-838 ^property[185].valueCode = #8382:3 
* #814-838 ^property[186].code = #child 
* #814-838 ^property[186].valueCode = #8383:3 
* #814-838 ^property[187].code = #child 
* #814-838 ^property[187].valueCode = #8384:3 
* #8140:0 "Adenom o.n.A."
* #8140:0 ^property[0].code = #parent 
* #8140:0 ^property[0].valueCode = #814-838 
* #8140:1 "Atypisches Adenom"
* #8140:1 ^designation[0].value = "Bronchialadenom o.n.A." 
* #8140:1 ^property[0].code = #parent 
* #8140:1 ^property[0].valueCode = #814-838 
* #8140:2 "Adenocarcinoma in situ o.n.A."
* #8140:2 ^property[0].code = #parent 
* #8140:2 ^property[0].valueCode = #814-838 
* #8140:3 "Adenokarzinom o.n.A."
* #8140:3 ^designation[0].value = "Gewöhnliches Adenokarzinom" 
* #8140:3 ^designation[1].value = "Azinäres Adenokarzinom der Prostata" 
* #8140:3 ^designation[2].value = "Karzinom der Skene-, Cowper- und Littré-Drüsen" 
* #8140:3 ^designation[3].value = "Nebenschilddrüsenkarzinom" 
* #8140:3 ^designation[4].value = "Tumor des endolymphatischen Sacks" 
* #8140:3 ^property[0].code = #parent 
* #8140:3 ^property[0].valueCode = #814-838 
* #8140:6 "Adenokarzinom-Metastase o.n.A."
* #8140:6 ^property[0].code = #parent 
* #8140:6 ^property[0].valueCode = #814-838 
* #8141:3 "Szirrhöses Adenokarzinom [obs.]"
* #8141:3 ^designation[0].value = "Karzinom mit produktiver Fibrose [obs.]" 
* #8141:3 ^designation[1].value = "Szirrhöses Karzinom [obs.]" 
* #8141:3 ^property[0].code = #parent 
* #8141:3 ^property[0].valueCode = #814-838 
* #8142:3 "Linitis plastica"
* #8142:3 ^property[0].code = #parent 
* #8142:3 ^property[0].valueCode = #814-838 
* #8143:3 "Oberflächlich spreitendes Adenokarzinom"
* #8143:3 ^property[0].code = #parent 
* #8143:3 ^property[0].valueCode = #814-838 
* #8144:0 "Adenom vom intestinalen Typ"
* #8144:0 ^property[0].code = #parent 
* #8144:0 ^property[0].valueCode = #814-838 
* #8144:3 "Adenokarzinom vom intestinalen Typ"
* #8144:3 ^designation[0].value = "Enterales Adenokarzinom" 
* #8144:3 ^designation[1].value = "Karzinom vom intestinalen Typ" 
* #8144:3 ^designation[2].value = "Muzinöses Karzinom vom intestinalen Typ" 
* #8144:3 ^property[0].code = #parent 
* #8144:3 ^property[0].valueCode = #814-838 
* #8145:3 "Diffuses Karzinom"
* #8145:3 ^designation[0].value = "Diffuses Adenokarzinom" 
* #8145:3 ^property[0].code = #parent 
* #8145:3 ^property[0].valueCode = #814-838 
* #8146:0 "Monomorphes Adenom"
* #8146:0 ^property[0].code = #parent 
* #8146:0 ^property[0].valueCode = #814-838 
* #8147:0 "Basalzelladenom"
* #8147:0 ^property[0].code = #parent 
* #8147:0 ^property[0].valueCode = #814-838 
* #8147:3 "Basalzell-Adenokarzinom"
* #8147:3 ^property[0].code = #parent 
* #8147:3 ^property[0].valueCode = #814-838 
* #8148:0 "Glanduläre intraepitheliale Neoplasie, niedriggradig"
* #8148:0 ^designation[0].value = "Glanduläre intraepitheliale Neoplasie Grad 1" 
* #8148:0 ^designation[1].value = "Glanduläre intraepitheliale Neoplasie Grad 2" 
* #8148:0 ^designation[2].value = "Biliäre intraepitheliale Neoplasie, niedriggradig" 
* #8148:0 ^designation[3].value = "Ösophageale glanduläre Dysplasie (intraepitheliale Neoplasie), niedriggradig" 
* #8148:0 ^property[0].code = #parent 
* #8148:0 ^property[0].valueCode = #814-838 
* #8148:2 "Glanduläre intraepitheliale Neoplasie, hochgradig"
* #8148:2 ^designation[0].value = "Glanduläre intraepitheliale Neoplasie Grad 3" 
* #8148:2 ^designation[1].value = "Biliäre intraepitheliale Neoplasie, hochgradigBiliäre intraepitheliale Neoplasie Grad 3 (BilIN-3)" 
* #8148:2 ^designation[2].value = "Flache intraepitheliale glanduläre Neoplasie, hochgradigFlache intraepitheliale Neoplasie (Dysplasie), hochgradig" 
* #8148:2 ^designation[3].value = "Ösophageale glanduläre Dysplasie (intraepitheliale Neoplasie), hochgradigÖsophageale intraepitheliale Neoplasie, hochgradig" 
* #8148:2 ^designation[4].value = "Prostatische intraepitheliale Neoplasie Grad 3PIN 3" 
* #8148:2 ^property[0].code = #parent 
* #8148:2 ^property[0].valueCode = #814-838 
* #8149:0 "Intrakanalikuläres Adenom"
* #8149:0 ^property[0].code = #parent 
* #8149:0 ^property[0].valueCode = #814-838 
* #8150:0 "Neuroendokrines Pankreas-Mikroadenom"
* #8150:0 ^designation[0].value = "Benigner endokriner Pankreastumor" 
* #8150:0 ^designation[1].value = "Benigner Inselzelltumor" 
* #8150:0 ^designation[2].value = "Pankreas-Mikroadenom" 
* #8150:0 ^property[0].code = #parent 
* #8150:0 ^property[0].valueCode = #814-838 
* #8150:3 "Neuroendokriner Pankreastumor, hormoninaktiv"
* #8150:3 ^designation[0].value = "Endokriner Pankreastumor, hormoninaktiv" 
* #8150:3 ^designation[1].value = "Endokriner Pankreastumor o.n.A." 
* #8150:3 ^designation[2].value = "Inselzelladenokarzinom" 
* #8150:3 ^designation[3].value = "Inselzelladenom" 
* #8150:3 ^designation[4].value = "Inselzelladenomatose" 
* #8150:3 ^designation[5].value = "Inselzellkarzinom" 
* #8150:3 ^designation[6].value = "Inselzelltumor o.n.A." 
* #8150:3 ^designation[7].value = "Nesidioblastom" 
* #8150:3 ^property[0].code = #parent 
* #8150:3 ^property[0].valueCode = #814-838 
* #8151:3 "Insulinom o.n.A."
* #8151:3 ^designation[0].value = "Beta-Zell-Adenom" 
* #8151:3 ^designation[1].value = "Beta-Zell-Tumor" 
* #8151:3 ^property[0].code = #parent 
* #8151:3 ^property[0].valueCode = #814-838 
* #8152:3 "Glukagonom"
* #8152:3 ^designation[0].value = "Alpha-Zell-Tumor" 
* #8152:3 ^designation[1].value = "Enteroglukagonom" 
* #8152:3 ^designation[2].value = "Glucagon-like peptide-producing tumor" 
* #8152:3 ^designation[3].value = "L-Zell-Tumor" 
* #8152:3 ^designation[4].value = "Pankreaspeptid und pancreas-peptide-like Peptid bei terminal Tyrosinamid produzierendem TumorPP/PYY produzierender Tumor" 
* #8152:3 ^property[0].code = #parent 
* #8152:3 ^property[0].valueCode = #814-838 
* #8153:1 "Gastrinom o.n.A."
* #8153:1 ^designation[0].value = "G-Zell-Tumor o.n.A." 
* #8153:1 ^designation[1].value = "Gastrinzellen-Tumor" 
* #8153:1 ^property[0].code = #parent 
* #8153:1 ^property[0].valueCode = #814-838 
* #8153:3 "Gastrinom"
* #8153:3 ^designation[0].value = "G-Zell-Tumor" 
* #8153:3 ^designation[1].value = "Gastrinzellen-Tumor" 
* #8153:3 ^property[0].code = #parent 
* #8153:3 ^property[0].valueCode = #814-838 
* #8154:3 "Maligner gemischter endokriner und exokriner Pankreastumor"
* #8154:3 ^designation[0].value = "Gemischtes Inselzell- und exokrines Adenokarzinom" 
* #8154:3 ^designation[1].value = "Gemischt azinär-endokrin-duktales Karzinom" 
* #8154:3 ^designation[2].value = "Gemischt azinär-endokrines Karzinom" 
* #8154:3 ^designation[3].value = "Gemischt duktal-endokrines Karzinom" 
* #8154:3 ^designation[4].value = "Gemischtes endokrines und exokrines Adenokarzinom" 
* #8154:3 ^property[0].code = #parent 
* #8154:3 ^property[0].valueCode = #814-838 
* #8155:1 "Vipom o.n.A."
* #8155:1 ^property[0].code = #parent 
* #8155:1 ^property[0].valueCode = #814-838 
* #8155:3 "Malignes Vipom"
* #8155:3 ^property[0].code = #parent 
* #8155:3 ^property[0].valueCode = #814-838 
* #8156:3 "Malignes Somatostatinom"
* #8156:3 ^designation[0].value = "Maligner Somatostatin-Zell-Tumor" 
* #8156:3 ^property[0].code = #parent 
* #8156:3 ^property[0].valueCode = #814-838 
* #8158:3 "ACTH-produzierender Tumor"
* #8158:3 ^designation[0].value = "Endokriner Tumor, hormonaktiv, o.n.A. [obs.]" 
* #8158:3 ^property[0].code = #parent 
* #8158:3 ^property[0].valueCode = #814-838 
* #8160:0 "Gallengangsadenom"
* #8160:0 ^designation[0].value = "Cholangiom" 
* #8160:0 ^property[0].code = #parent 
* #8160:0 ^property[0].valueCode = #814-838 
* #8160:3 "Intrahepatisches Cholangiokarzinom"
* #8160:3 ^designation[0].value = "Gallengangs-Adenokarzinom" 
* #8160:3 ^designation[1].value = "Gallengangskarzinom" 
* #8160:3 ^property[0].code = #parent 
* #8160:3 ^property[0].valueCode = #814-838 
* #8161:0 "Gallengangs-Zystadenom"
* #8161:0 ^property[0].code = #parent 
* #8161:0 ^property[0].valueCode = #814-838 
* #8161:3 "Gallengangs-Zystadenokarzinom"
* #8161:3 ^property[0].code = #parent 
* #8161:3 ^property[0].valueCode = #814-838 
* #8162:3 "Perihiläres Cholangiokarzinom"
* #8162:3 ^designation[0].value = "Klatskin-Tumor" 
* #8162:3 ^property[0].code = #parent 
* #8162:3 ^property[0].valueCode = #814-838 
* #8163:0 "Pankreatobiliäre Neoplasie, nichtinvasiv"
* #8163:0 ^designation[0].value = "Nichtinvasive pankreatobiliäre papilläre Neoplasie mit niedriggradiger Dysplasie" 
* #8163:0 ^designation[1].value = "Nichtinvasive pankreatobiliäre papilläre Neoplasie mit niedriggradiger intraepithelialer Neoplasie" 
* #8163:0 ^property[0].code = #parent 
* #8163:0 ^property[0].valueCode = #814-838 
* #8163:2 "Papilläre Neoplasie, pankreatobiliären Typs, mit hochgradiger intraepithelialer Neoplasie"
* #8163:2 ^designation[0].value = "Nichtinvasive pankreatobiliäre papilläre Neoplasie mit hochgradiger Dysplasie" 
* #8163:2 ^designation[1].value = "Nichtinvasive pankreatobiliäre papilläre Neoplasie mit hochgradiger intraepithelialer Neoplasie" 
* #8163:2 ^property[0].code = #parent 
* #8163:2 ^property[0].valueCode = #814-838 
* #8163:3 "Karzinom vom pankreatobiliären Typ"
* #8163:3 ^designation[0].value = "Adenokarzinom vom pankreatobiliären Typ" 
* #8163:3 ^property[0].code = #parent 
* #8163:3 ^property[0].valueCode = #814-838 
* #8170:0 "Leberzelladenom"
* #8170:0 ^designation[0].value = "Benignes Hepatom" 
* #8170:0 ^designation[1].value = "Hepatozelluläres Adenom" 
* #8170:0 ^property[0].code = #parent 
* #8170:0 ^property[0].valueCode = #814-838 
* #8170:3 "Hepatozelluläres Karzinom o.n.A."
* #8170:3 ^designation[0].value = "Hepatokarzinom" 
* #8170:3 ^designation[1].value = "Hepatom o.n.A." 
* #8170:3 ^designation[2].value = "Leberzellkarzinom" 
* #8170:3 ^designation[3].value = "Malignes Hepatom" 
* #8170:3 ^property[0].code = #parent 
* #8170:3 ^property[0].valueCode = #814-838 
* #8171:3 "Fibrolamelläres Leberzellkarzinom"
* #8171:3 ^property[0].code = #parent 
* #8171:3 ^property[0].valueCode = #814-838 
* #8172:3 "Szirrhöses hepatozelluläres Karzinom"
* #8172:3 ^designation[0].value = "Sklerosierendes hepatozelluläres Karzinom" 
* #8172:3 ^property[0].code = #parent 
* #8172:3 ^property[0].valueCode = #814-838 
* #8173:3 "Spindelzelliges hepatozelluläres Karzinom"
* #8173:3 ^designation[0].value = "Sarkomatoides hepatozelluläres Karzinom" 
* #8173:3 ^property[0].code = #parent 
* #8173:3 ^property[0].valueCode = #814-838 
* #8174:3 "Klarzelliges hepatozelluläres Karzinom"
* #8174:3 ^property[0].code = #parent 
* #8174:3 ^property[0].valueCode = #814-838 
* #8175:3 "Pleomorphes hepatozelluläres Karzinom"
* #8175:3 ^property[0].code = #parent 
* #8175:3 ^property[0].valueCode = #814-838 
* #8180:3 "Kombiniertes hepatozelluläres Karzinom und Cholangiokarzinom"
* #8180:3 ^designation[0].value = "Gemischtes Leberzell- und Gallengangskarzinom" 
* #8180:3 ^designation[1].value = "Hepatocholangiokarzinom" 
* #8180:3 ^property[0].code = #parent 
* #8180:3 ^property[0].valueCode = #814-838 
* #8190:0 "Trabekuläres Adenom"
* #8190:0 ^property[0].code = #parent 
* #8190:0 ^property[0].valueCode = #814-838 
* #8190:3 "Trabekuläres Adenokarzinom"
* #8190:3 ^designation[0].value = "Trabekuläres Karzinom" 
* #8190:3 ^property[0].code = #parent 
* #8190:3 ^property[0].valueCode = #814-838 
* #8191:0 "Embryonales Adenom"
* #8191:0 ^property[0].code = #parent 
* #8191:0 ^property[0].valueCode = #814-838 
* #8200:0 "Ekkrines dermales Zylindrom"
* #8200:0 ^designation[0].value = "Turbantumor" 
* #8200:0 ^designation[1].value = "Zylindrom der Brust" 
* #8200:0 ^designation[2].value = "Zylindrom der Haut" 
* #8200:0 ^property[0].code = #parent 
* #8200:0 ^property[0].valueCode = #814-838 
* #8200:3 "Adenoid-zystisches Karzinom"
* #8200:3 ^designation[0].value = "Adenozystisches Karzinom" 
* #8200:3 ^designation[1].value = "Zylindroides Adenokarzinom [obs.]" 
* #8200:3 ^designation[2].value = "Zylindrom o.n.A. [obs.]" 
* #8200:3 ^designation[3].value = "Thymuskarzinom mit adenoiden zystischen karzinomähnlichen Merkmalen" 
* #8200:3 ^designation[4].value = "Zylindroides Bronchusadenom" 
* #8200:3 ^property[0].code = #parent 
* #8200:3 ^property[0].valueCode = #814-838 
* #8201:2 "Kribriformes Carcinoma in situ"
* #8201:2 ^designation[0].value = "Kribriformes duktales Carcinoma in situ" 
* #8201:2 ^property[0].code = #parent 
* #8201:2 ^property[0].valueCode = #814-838 
* #8201:3 "Kribriformes Karzinom o.n.A."
* #8201:3 ^designation[0].value = "Kribriformes duktales Karzinom" 
* #8201:3 ^designation[1].value = "Kribriformes Karzinom vom Comedo-TypKribriformes Adenokarzinom vom Comedo-Typ" 
* #8201:3 ^property[0].code = #parent 
* #8201:3 ^property[0].valueCode = #814-838 
* #8202:0 "Mikrozystisches Adenom"
* #8202:0 ^property[0].code = #parent 
* #8202:0 ^property[0].valueCode = #814-838 
* #8204:0 "Laktierendes Adenom"
* #8204:0 ^property[0].code = #parent 
* #8204:0 ^property[0].valueCode = #814-838 
* #8210:0 "Adenomatöser Polyp o.n.A."
* #8210:0 ^designation[0].value = "Polypoides Adenom" 
* #8210:0 ^property[0].code = #parent 
* #8210:0 ^property[0].valueCode = #814-838 
* #8210:2 "Adenocarcinoma in situ in adenomatösem Polypen"
* #8210:2 ^designation[0].value = "Adenocarcinoma in situ in einem Polypen o.n.A." 
* #8210:2 ^designation[1].value = "Adenocarcinoma in situ in polypoidem Adenom" 
* #8210:2 ^designation[2].value = "Adenocarcinoma in situ in tubulärem Adenom" 
* #8210:2 ^designation[3].value = "Carcinoma in situ in einem Polypen o.n.A." 
* #8210:2 ^designation[4].value = "Carcinoma in situ in adenomatösem Polypen" 
* #8210:2 ^property[0].code = #parent 
* #8210:2 ^property[0].valueCode = #814-838 
* #8210:3 "Adenokarzinom in adenomatösem Polypen"
* #8210:3 ^designation[0].value = "Adenokarzinom in einem Polypen o.n.A." 
* #8210:3 ^designation[1].value = "Adenokarzinom in polypoidem Adenom" 
* #8210:3 ^designation[2].value = "Adenokarzinom in tubulärem Adenom" 
* #8210:3 ^designation[3].value = "Karzinom in adenomatösem Polypen" 
* #8210:3 ^designation[4].value = "Karzinom in einem Polypen o.n.A." 
* #8210:3 ^property[0].code = #parent 
* #8210:3 ^property[0].valueCode = #814-838 
* #8211:0 "Tubuläres Adenom o.n.A."
* #8211:0 ^property[0].code = #parent 
* #8211:0 ^property[0].valueCode = #814-838 
* #8211:3 "Tubuläres Adenokarzinom"
* #8211:3 ^designation[0].value = "Tubuläres Karzinom" 
* #8211:3 ^property[0].code = #parent 
* #8211:3 ^property[0].valueCode = #814-838 
* #8212:0 "Flaches Adenom"
* #8212:0 ^designation[0].value = "Flat adenoma" 
* #8212:0 ^property[0].code = #parent 
* #8212:0 ^property[0].valueCode = #814-838 
* #8213:0 "Serrated adenoma o.n.A."
* #8213:0 ^designation[0].value = "Traditional serrated adenoma" 
* #8213:0 ^designation[1].value = "Gemischt adenomatös-hyperplastischer Polyp" 
* #8213:0 ^designation[2].value = "Sessile serrated adenoma o.n.A." 
* #8213:0 ^designation[3].value = "Sessile serrated polyp" 
* #8213:0 ^designation[4].value = "Traditional sessile serrated adenoma" 
* #8213:0 ^property[0].code = #parent 
* #8213:0 ^property[0].valueCode = #814-838 
* #8213:3 "Serrated adenocarcinoma"
* #8213:3 ^property[0].code = #parent 
* #8213:3 ^property[0].valueCode = #814-838 
* #8214:3 "Parietalzellkarzinom"
* #8214:3 ^designation[0].value = "Parietalzell-Adenokarzinom" 
* #8214:3 ^property[0].code = #parent 
* #8214:3 ^property[0].valueCode = #814-838 
* #8215:3 "Adenokarzinom der Analdrüsen"
* #8215:3 ^designation[0].value = "Adenokarzinom der Proktodealdrüsen" 
* #8215:3 ^property[0].code = #parent 
* #8215:3 ^property[0].valueCode = #814-838 
* #8220:0 "Familiäre adenomatöse Polypose [FAP]"
* #8220:0 ^designation[0].value = "Adenomatose o.n.A." 
* #8220:0 ^designation[1].value = "Familiäre Polyposis coli" 
* #8220:0 ^property[0].code = #parent 
* #8220:0 ^property[0].valueCode = #814-838 
* #8220:3 "Adenokarzinom in familiärer adenomatöser Polypose [FAP]"
* #8220:3 ^property[0].code = #parent 
* #8220:3 ^property[0].valueCode = #814-838 
* #8221:0 "Multiple adenomatöse Polypen"
* #8221:0 ^property[0].code = #parent 
* #8221:0 ^property[0].valueCode = #814-838 
* #8221:3 "Adenokarzinom in multiplen adenomatösen Polypen"
* #8221:3 ^property[0].code = #parent 
* #8221:3 ^property[0].valueCode = #814-838 
* #8230:2 "Solides duktales Carcinoma in situ"
* #8230:2 ^designation[0].value = "Solides intraduktales Karzinom" 
* #8230:2 ^property[0].code = #parent 
* #8230:2 ^property[0].valueCode = #814-838 
* #8230:3 "Solides Karzinom o.n.A."
* #8230:3 ^designation[0].value = "Solides Adenokarzinom, o.n.A." 
* #8230:3 ^designation[1].value = "Solides Karzinom mit SchleimbildungSolides Adenokarzinom mit Schleimbildung" 
* #8230:3 ^property[0].code = #parent 
* #8230:3 ^property[0].valueCode = #814-838 
* #8231:3 "Carcinoma simplex"
* #8231:3 ^property[0].code = #parent 
* #8231:3 ^property[0].valueCode = #814-838 
* #8240:3 "Neuroendokriner Tumor o.n.A."
* #8240:3 ^designation[0].value = "Karzinoid o.n.A." 
* #8240:3 ^designation[1].value = "Karzinoidtumor o.n.A." 
* #8240:3 ^designation[2].value = "Bronchialadenom vom KarzinoidtypNeuroendokrines Karzinom, gut differenziert" 
* #8240:3 ^designation[3].value = "Bronchialadenom vom KarzinoidtypNeuroendokrines Karzinom, niedriggradig" 
* #8240:3 ^designation[4].value = "Neuroendokriner Tumor, Grad 1Typisches Karzinoid" 
* #8240:3 ^property[0].code = #parent 
* #8240:3 ^property[0].valueCode = #814-838 
* #8241:3 "Enterochromaffinzell-Karzinoid"
* #8241:3 ^designation[0].value = "Argentaffiner maligner Karzinoidtumor" 
* #8241:3 ^designation[1].value = "EC-Zell-Tumor" 
* #8241:3 ^designation[2].value = "Malignes Argentaffinom" 
* #8241:3 ^designation[3].value = "Serotonin produzierendes Karzinoid" 
* #8241:3 ^property[0].code = #parent 
* #8241:3 ^property[0].valueCode = #814-838 
* #8242:3 "Maligner Enterochromaffin-like-cell-Tumor"
* #8242:3 ^designation[0].value = "Malignes ECL-Zell-Karzinoid" 
* #8242:3 ^property[0].code = #parent 
* #8242:3 ^property[0].valueCode = #814-838 
* #8243:3 "Becherzellkarzinoid"
* #8243:3 ^designation[0].value = "Mukokarzinoidtumor" 
* #8243:3 ^designation[1].value = "Muzinöses Karzinoid" 
* #8243:3 ^property[0].code = #parent 
* #8243:3 ^property[0].valueCode = #814-838 
* #8244:3 "Gemischtes adeno-neuroendokrines Karzinom"
* #8244:3 ^designation[0].value = "Gemischtes Karzinoid und Adenokarzinom" 
* #8244:3 ^designation[1].value = "Kombiniertes/gemischtes Karzinoid und Adenokarzinom" 
* #8244:3 ^designation[2].value = "Kombiniertes Karzinoid" 
* #8244:3 ^designation[3].value = "MANEC" 
* #8244:3 ^designation[4].value = "Mischzelliges Karzinoid-Adenokarzinom" 
* #8244:3 ^property[0].code = #parent 
* #8244:3 ^property[0].valueCode = #814-838 
* #8245:1 "Tubuläres Karzinoid"
* #8245:1 ^property[0].code = #parent 
* #8245:1 ^property[0].valueCode = #814-838 
* #8245:3 "Adenokarzinoidtumor"
* #8245:3 ^property[0].code = #parent 
* #8245:3 ^property[0].valueCode = #814-838 
* #8246:3 "Neuroendokrines Karzinom o.n.A."
* #8246:3 ^designation[0].value = "Gering differenzierte neuroendokrine Neoplasie" 
* #8246:3 ^property[0].code = #parent 
* #8246:3 ^property[0].valueCode = #814-838 
* #8247:3 "Merkel-Zell-Karzinom"
* #8247:3 ^designation[0].value = "Merkel-Zell-Tumor" 
* #8247:3 ^designation[1].value = "Primäres kutanes neuroendokrines Karzinom" 
* #8247:3 ^property[0].code = #parent 
* #8247:3 ^property[0].valueCode = #814-838 
* #8248:1 "Apudom"
* #8248:1 ^property[0].code = #parent 
* #8248:1 ^property[0].valueCode = #814-838 
* #8249:3 "Neuroendokriner Tumor, Grad 2"
* #8249:3 ^designation[0].value = "Atypischer Karzinoidtumor" 
* #8249:3 ^designation[1].value = "Mäßig differenziertes neuroendokrines Karzinom" 
* #8249:3 ^designation[2].value = "Neuroendokriner Tumor, Grad 3" 
* #8249:3 ^property[0].code = #parent 
* #8249:3 ^property[0].valueCode = #814-838 
* #8250:0 "Atypische adenomatöse Hyperplasie"
* #8250:0 ^property[0].code = #parent 
* #8250:0 ^property[0].valueCode = #814-838 
* #8250:1 "Lungenadenomatose"
* #8250:1 ^property[0].code = #parent 
* #8250:1 ^property[0].valueCode = #814-838 
* #8250:2 "Nichtmuzinöses Adenocarcinoma in situ der Lunge"
* #8250:2 ^property[0].code = #parent 
* #8250:2 ^property[0].valueCode = #814-838 
* #8250:3 "Lepidisches Adenokarzinom"
* #8250:3 ^designation[0].value = "Alveolarzellkarzinom" 
* #8250:3 ^designation[1].value = "Bronchioläres Adenokarzinom" 
* #8250:3 ^designation[2].value = "Bronchioläres Karzinom" 
* #8250:3 ^designation[3].value = "Bronchiolo-alveoläres Adenokarzinom o.n.A." 
* #8250:3 ^designation[4].value = "Bronchiolo-alveoläres Karzinom o.n.A." 
* #8250:3 ^property[0].code = #parent 
* #8250:3 ^property[0].valueCode = #814-838 
* #8251:0 "Alveoläres Adenom"
* #8251:0 ^property[0].code = #parent 
* #8251:0 ^property[0].valueCode = #814-838 
* #8251:3 "Alveoläres Adenokarzinom"
* #8251:3 ^designation[0].value = "Alveoläres Karzinom" 
* #8251:3 ^property[0].code = #parent 
* #8251:3 ^property[0].valueCode = #814-838 
* #8252:3 "Nichtmuzinöses bronchiolo-alveoläres Karzinom"
* #8252:3 ^designation[0].value = "Bronchiolo-alveoläres Karzinom vom Clara-Zell-Typ" 
* #8252:3 ^designation[1].value = "Bronchiolo-alveoläres Karzinom vom Typ-II-Pneumozyten-Typ" 
* #8252:3 ^property[0].code = #parent 
* #8252:3 ^property[0].valueCode = #814-838 
* #8253:2 "Muzinöses Adenocarcinoma in situ"
* #8253:2 ^property[0].code = #parent 
* #8253:2 ^property[0].valueCode = #814-838 
* #8253:3 "Muzinöses Adenokarzinom der Lunge"
* #8253:3 ^designation[0].value = "Bronchiolo-alveoläres Karzinom vom Becherzell-Typ" 
* #8253:3 ^designation[1].value = "Muzinöses bronchiolo-alveoläres Karzinom" 
* #8253:3 ^property[0].code = #parent 
* #8253:3 ^property[0].valueCode = #814-838 
* #8254:3 "Gemischtes muzinöses und nichtmuzinöses Adenokarzinom der Lunge"
* #8254:3 ^designation[0].value = "Bronchiolo-alveoläres Karzinom mit unbestimmtem Typ" 
* #8254:3 ^designation[1].value = "Bronchiolo-alveoläres Karzinom vom Clara- und Becherzell-Typ" 
* #8254:3 ^designation[2].value = "Bronchiolo-alveoläres Karzinom vom Typ-II-Pneumozyten- und Becherzell-Typ" 
* #8254:3 ^designation[3].value = "Gemischtes muzinöses und nichtmuzinöses bronchiolo-alveoläres Karzinom" 
* #8254:3 ^property[0].code = #parent 
* #8254:3 ^property[0].valueCode = #814-838 
* #8255:3 "Adenokarzinom mit gemischten Subtypen"
* #8255:3 ^designation[0].value = "Adenokarzinom, kombiniert mit anderen Karzinom-Typen" 
* #8255:3 ^property[0].code = #parent 
* #8255:3 ^property[0].valueCode = #814-838 
* #8256:3 "Nichtmuzinöses minimal-invasives Adenokarzinom"
* #8256:3 ^property[0].code = #parent 
* #8256:3 ^property[0].valueCode = #814-838 
* #8257:3 "Muzinöses minimal-invasives Adenokarzinom"
* #8257:3 ^property[0].code = #parent 
* #8257:3 ^property[0].valueCode = #814-838 
* #8260:0 "Papilläres Adenom o.n.A."
* #8260:0 ^designation[0].value = "Glanduläres Papillom" 
* #8260:0 ^property[0].code = #parent 
* #8260:0 ^property[0].valueCode = #814-838 
* #8260:1 "Aggressiver papillärer Tumor"
* #8260:1 ^property[0].code = #parent 
* #8260:1 ^property[0].valueCode = #814-838 
* #8260:3 "Papilläres Adenokarzinom o.n.A."
* #8260:3 ^designation[0].value = "Papilläres Karzinom der Schilddrüse" 
* #8260:3 ^designation[1].value = "Papilläres Nierenzellkarzinom" 
* #8260:3 ^property[0].code = #parent 
* #8260:3 ^property[0].valueCode = #814-838 
* #8261:0 "Villöses Adenom o.n.A."
* #8261:0 ^designation[0].value = "Villöses Papillom" 
* #8261:0 ^property[0].code = #parent 
* #8261:0 ^property[0].valueCode = #814-838 
* #8261:2 "Adenocarcinoma in situ in villösem Adenom"
* #8261:2 ^property[0].code = #parent 
* #8261:2 ^property[0].valueCode = #814-838 
* #8261:3 "Adenokarzinom in villösem Adenom"
* #8261:3 ^property[0].code = #parent 
* #8261:3 ^property[0].valueCode = #814-838 
* #8262:3 "Villöses Adenokarzinom"
* #8262:3 ^property[0].code = #parent 
* #8262:3 ^property[0].valueCode = #814-838 
* #8263:0 "Tubulovillöses Adenom o.n.A."
* #8263:0 ^designation[0].value = "Villoglanduläres Adenom" 
* #8263:0 ^designation[1].value = "Papillotubuläres AdenomTubulopapilläres Adenom" 
* #8263:0 ^property[0].code = #parent 
* #8263:0 ^property[0].valueCode = #814-838 
* #8263:2 "Adenocarcinoma in situ in tubulovillösem Adenom"
* #8263:2 ^property[0].code = #parent 
* #8263:2 ^property[0].valueCode = #814-838 
* #8263:3 "Adenokarzinom in tubulovillösem Adenom"
* #8263:3 ^designation[0].value = "Papillotubuläres AdenokarzinomTubulopapilläres Adenokarzinom" 
* #8263:3 ^designation[1].value = "Villoglanduläre Variante des endometrioiden Adenokarzinoms" 
* #8263:3 ^designation[2].value = "Villoglanduläres Karzinom" 
* #8263:3 ^property[0].code = #parent 
* #8263:3 ^property[0].valueCode = #814-838 
* #8264:0 "Glanduläre Papillomatose"
* #8264:0 ^designation[0].value = "Biliäre Papillomatose" 
* #8264:0 ^property[0].code = #parent 
* #8264:0 ^property[0].valueCode = #814-838 
* #8265:3 "Mikropapilläres Karzinom o.n.A."
* #8265:3 ^designation[0].value = "Mikropapilläres Adenokarzinom" 
* #8265:3 ^property[0].code = #parent 
* #8265:3 ^property[0].valueCode = #814-838 
* #8270:0 "Chromophobes Adenom"
* #8270:0 ^property[0].code = #parent 
* #8270:0 ^property[0].valueCode = #814-838 
* #8270:3 "Chromophobes Karzinom"
* #8270:3 ^designation[0].value = "Chromophobes Adenokarzinom" 
* #8270:3 ^property[0].code = #parent 
* #8270:3 ^property[0].valueCode = #814-838 
* #8271:0 "Laktotrophes Adenom"
* #8271:0 ^designation[0].value = "Prolaktinom" 
* #8271:0 ^property[0].code = #parent 
* #8271:0 ^property[0].valueCode = #814-838 
* #8272:0 "Hypophysenadenom o.n.A."
* #8272:0 ^designation[0].value = "Ektopes Hypophysenadenom" 
* #8272:0 ^designation[1].value = "Gonadotropes Adenom" 
* #8272:0 ^designation[2].value = "Kortikotropes Adenom" 
* #8272:0 ^designation[3].value = "Null-Zell-Adenom" 
* #8272:0 ^designation[4].value = "Plurihormonales Adenom" 
* #8272:0 ^designation[5].value = "Somatotropes Adenom" 
* #8272:0 ^designation[6].value = "Thyreotropes Adenom" 
* #8272:0 ^property[0].code = #parent 
* #8272:0 ^property[0].valueCode = #814-838 
* #8272:3 "Hypophysenkarzinom o.n.A."
* #8272:3 ^property[0].code = #parent 
* #8272:3 ^property[0].valueCode = #814-838 
* #8273:3 "Hypophysenblastom"
* #8273:3 ^property[0].code = #parent 
* #8273:3 ^property[0].valueCode = #814-838 
* #8280:0 "Azidophiles Adenom"
* #8280:0 ^designation[0].value = "Eosinophiles Adenom" 
* #8280:0 ^property[0].code = #parent 
* #8280:0 ^property[0].valueCode = #814-838 
* #8280:3 "Azidophiles Karzinom"
* #8280:3 ^designation[0].value = "Azidophiles Adenokarzinom" 
* #8280:3 ^designation[1].value = "Eosinophiles Adenokarzinom" 
* #8280:3 ^designation[2].value = "Eosinophiles Karzinom" 
* #8280:3 ^property[0].code = #parent 
* #8280:3 ^property[0].valueCode = #814-838 
* #8281:0 "Gemischtzelliges azidophil-basophiles Adenom"
* #8281:0 ^property[0].code = #parent 
* #8281:0 ^property[0].valueCode = #814-838 
* #8281:3 "Gemischtzelliges azidophil-basophiles Karzinom"
* #8281:3 ^property[0].code = #parent 
* #8281:3 ^property[0].valueCode = #814-838 
* #8290:0 "Oxyphiles Adenom"
* #8290:0 ^designation[0].value = "Onkozytäres Adenom" 
* #8290:0 ^designation[1].value = "Onkozytom" 
* #8290:0 ^designation[2].value = "Hürthle-Zell-Adenom" 
* #8290:0 ^designation[3].value = "Hürthle-Zell-Tumor" 
* #8290:0 ^designation[4].value = "Onkozytäres papilläres Zystadenom" 
* #8290:0 ^designation[5].value = "Oxyphiles follikuläres Adenom" 
* #8290:0 ^designation[6].value = "Spindelzellonkozytom" 
* #8290:0 ^property[0].code = #parent 
* #8290:0 ^property[0].valueCode = #814-838 
* #8290:3 "Oxyphiles Adenokarzinom"
* #8290:3 ^designation[0].value = "Onkozytäres Adenokarzinom" 
* #8290:3 ^designation[1].value = "Onkozytäres Karzinom" 
* #8290:3 ^designation[2].value = "Hürthle-Zell-Adenokarzinom" 
* #8290:3 ^designation[3].value = "Hürthle-Zell-Karzinom" 
* #8290:3 ^designation[4].value = "Oxyphiles follikuläres Karzinom" 
* #8290:3 ^property[0].code = #parent 
* #8290:3 ^property[0].valueCode = #814-838 
* #8300:0 "Basophiles Adenom"
* #8300:0 ^designation[0].value = "Mukoidzelladenom" 
* #8300:0 ^property[0].code = #parent 
* #8300:0 ^property[0].valueCode = #814-838 
* #8300:3 "Basophiles Karzinom"
* #8300:3 ^designation[0].value = "Basophiles Adenokarzinom" 
* #8300:3 ^designation[1].value = "Mukoidzelladenokarzinom" 
* #8300:3 ^property[0].code = #parent 
* #8300:3 ^property[0].valueCode = #814-838 
* #8310:0 "Klarzelliges Adenom"
* #8310:0 ^property[0].code = #parent 
* #8310:0 ^property[0].valueCode = #814-838 
* #8310:3 "Klarzelliges Adenokarzinom o.n.A."
* #8310:3 ^designation[0].value = "Klarzelliges Karzinom o.n.A." 
* #8310:3 ^designation[1].value = "Klarzelliges Nierenzellkarzinom o.n.A." 
* #8310:3 ^designation[2].value = "Mesonephroides klarzelliges Adenokarzinom" 
* #8310:3 ^property[0].code = #parent 
* #8310:3 ^property[0].valueCode = #814-838 
* #8311:1 "Hypernephroider Tumor"
* #8311:1 ^property[0].code = #parent 
* #8311:1 ^property[0].valueCode = #814-838 
* #8311:3 "HRCC (Hereditary leiomyomatosis and renal cell carcinoma) assoziiertes Nierenzellkarzinom"
* #8311:3 ^designation[0].value = "Nierenzellkarzinom mit Succinatdehydrogenase-Mangel" 
* #8311:3 ^designation[1].value = "Translokationskarzinom der MiT-Familie" 
* #8311:3 ^property[0].code = #parent 
* #8311:3 ^property[0].valueCode = #814-838 
* #8312:3 "Nierenzellkarzinom o.n.A."
* #8312:3 ^designation[0].value = "Nierenzelladenokarzinom" 
* #8312:3 ^designation[1].value = "Unklassifiziertes NierenzellkarzinomGrawitz-Tumor" 
* #8312:3 ^designation[2].value = "Unklassifiziertes NierenzellkarzinomHypernephrom" 
* #8312:3 ^property[0].code = #parent 
* #8312:3 ^property[0].valueCode = #814-838 
* #8313:0 "Klarzelliges Adenofibrom"
* #8313:0 ^designation[0].value = "Klarzelliges Zystadenofibrom" 
* #8313:0 ^property[0].code = #parent 
* #8313:0 ^property[0].valueCode = #814-838 
* #8313:1 "Klarzelliger Borderline-Tumor"
* #8313:1 ^designation[0].value = "Atypisch proliferierender klarzelliger Tumor" 
* #8313:1 ^designation[1].value = "Klarzelliger zystischer Tumor mit Borderline-Malignität" 
* #8313:1 ^designation[2].value = "Klarzelliges Adenofibrom mit Borderline-MalignitätKlarzelliges Zystadenofibrom mit Borderline-Malignität" 
* #8313:1 ^property[0].code = #parent 
* #8313:1 ^property[0].valueCode = #814-838 
* #8313:3 "Klarzelliges Adenokarzinofibrom"
* #8313:3 ^designation[0].value = "Klarzelliges Zystadenokarzinofibrom" 
* #8313:3 ^property[0].code = #parent 
* #8313:3 ^property[0].valueCode = #814-838 
* #8314:3 "Lipidreiches Karzinom"
* #8314:3 ^property[0].code = #parent 
* #8314:3 ^property[0].valueCode = #814-838 
* #8315:3 "Glykogenreiches Karzinom"
* #8315:3 ^designation[0].value = "Glykogenreiches klarzelliges Karzinom" 
* #8315:3 ^property[0].code = #parent 
* #8315:3 ^property[0].valueCode = #814-838 
* #8316:1 "Multilokuläre zystische Neoplasie der Niere mit niedrigem Malignitätspotential"
* #8316:1 ^property[0].code = #parent 
* #8316:1 ^property[0].valueCode = #814-838 
* #8316:3 "Zystenassoziiertes Nierenzellkarzinom"
* #8316:3 ^designation[0].value = "Erworbene zystische Nierenkrankheit mit assoziiertem Nierenzellkarzinom" 
* #8316:3 ^designation[1].value = "Tubulozystisches Nierenzellkarzinom" 
* #8316:3 ^property[0].code = #parent 
* #8316:3 ^property[0].valueCode = #814-838 
* #8317:3 "Nierenzellkarzinom vom chromophoben Zelltyp"
* #8317:3 ^designation[0].value = "Chromophobes Nierenzellkarzinom" 
* #8317:3 ^designation[1].value = "Hybrid-onkozytärer chromophober Tumor" 
* #8317:3 ^property[0].code = #parent 
* #8317:3 ^property[0].valueCode = #814-838 
* #8318:3 "Sarkomatoides Nierenzellkarzinom"
* #8318:3 ^designation[0].value = "Spindelzelliges Nierenzellkarzinom" 
* #8318:3 ^property[0].code = #parent 
* #8318:3 ^property[0].valueCode = #814-838 
* #8319:3 "Sammelrohrkarzinom"
* #8319:3 ^designation[0].value = "Bellini-Duct-Karzinom" 
* #8319:3 ^designation[1].value = "Nierenkarzinom vom Sammelrohr-Typ" 
* #8319:3 ^property[0].code = #parent 
* #8319:3 ^property[0].valueCode = #814-838 
* #8320:3 "Granularzellkarzinom"
* #8320:3 ^designation[0].value = "Granularzelladenokarzinom" 
* #8320:3 ^property[0].code = #parent 
* #8320:3 ^property[0].valueCode = #814-838 
* #8321:0 "Hauptzelladenom"
* #8321:0 ^property[0].code = #parent 
* #8321:0 ^property[0].valueCode = #814-838 
* #8322:0 "Wasserklares Adenom"
* #8322:0 ^property[0].code = #parent 
* #8322:0 ^property[0].valueCode = #814-838 
* #8322:3 "Wasserklares Adenokarzinom"
* #8322:3 ^designation[0].value = "Wasserklares Karzinom" 
* #8322:3 ^property[0].code = #parent 
* #8322:3 ^property[0].valueCode = #814-838 
* #8323:0 "Gemischtzelliges Adenom"
* #8323:0 ^property[0].code = #parent 
* #8323:0 ^property[0].valueCode = #814-838 
* #8323:1 "Klarzelliges papilläres Nierenzellkarzinom"
* #8323:1 ^property[0].code = #parent 
* #8323:1 ^property[0].valueCode = #814-838 
* #8323:3 "Gemischtzelliges Adenokarzinom"
* #8323:3 ^property[0].code = #parent 
* #8323:3 ^property[0].valueCode = #814-838 
* #8324:0 "Lipoadenom"
* #8324:0 ^designation[0].value = "Adenolipom" 
* #8324:0 ^property[0].code = #parent 
* #8324:0 ^property[0].valueCode = #814-838 
* #8325:0 "Nachnieren-Adenom"
* #8325:0 ^property[0].code = #parent 
* #8325:0 ^property[0].valueCode = #814-838 
* #8330:0 "Follikuläres Adenom o.n.A."
* #8330:0 ^property[0].code = #parent 
* #8330:0 ^property[0].valueCode = #814-838 
* #8330:1 "Atypisches follikuläres Adenom"
* #8330:1 ^property[0].code = #parent 
* #8330:1 ^property[0].valueCode = #814-838 
* #8330:3 "Follikuläres Karzinom o.n.A."
* #8330:3 ^designation[0].value = "Follikuläres Adenokarzinom o.n.A." 
* #8330:3 ^property[0].code = #parent 
* #8330:3 ^property[0].valueCode = #814-838 
* #8331:3 "Gut differenziertes follikuläres Adenokarzinom"
* #8331:3 ^designation[0].value = "Gut differenziertes follikuläres Karzinom" 
* #8331:3 ^property[0].code = #parent 
* #8331:3 ^property[0].valueCode = #814-838 
* #8332:3 "Trabekuläres follikuläres Adenokarzinom"
* #8332:3 ^designation[0].value = "Trabekuläres follikuläres Karzinom" 
* #8332:3 ^designation[1].value = "Mäßig differenziertes follikuläres AdenokarzinomMäßig differenziertes follikuläres Karzinom" 
* #8332:3 ^property[0].code = #parent 
* #8332:3 ^property[0].valueCode = #814-838 
* #8333:0 "Mikrofollikuläres Adenom"
* #8333:0 ^designation[0].value = "Fetales Adenom" 
* #8333:0 ^property[0].code = #parent 
* #8333:0 ^property[0].valueCode = #814-838 
* #8333:3 "Fetales Adenokarzinom"
* #8333:3 ^property[0].code = #parent 
* #8333:3 ^property[0].valueCode = #814-838 
* #8334:0 "Makrofollikuläres Adenom"
* #8334:0 ^designation[0].value = "Kolloidadenom" 
* #8334:0 ^property[0].code = #parent 
* #8334:0 ^property[0].valueCode = #814-838 
* #8335:1 "Follikulärer Tumor mit unsicherem Malignitätspotential"
* #8335:1 ^designation[0].value = "Abgekapseltes follikuläres Karzinom o.n.A." 
* #8335:1 ^property[0].code = #parent 
* #8335:1 ^property[0].valueCode = #814-838 
* #8335:3 "Minimal-invasives follikuläres Karzinom"
* #8335:3 ^property[0].code = #parent 
* #8335:3 ^property[0].valueCode = #814-838 
* #8336:1 "Hyalinisierender trabekulärer Tumor"
* #8336:1 ^designation[0].value = "Hyalinisierendes trabekuläres Adenom" 
* #8336:1 ^property[0].code = #parent 
* #8336:1 ^property[0].valueCode = #814-838 
* #8337:3 "Schlecht differenziertes Schilddrüsenkarzinom"
* #8337:3 ^designation[0].value = "Insuläres Karzinom" 
* #8337:3 ^property[0].code = #parent 
* #8337:3 ^property[0].valueCode = #814-838 
* #8339:3 "Angio-invasives, verkapseltes follikuläres Karzinom"
* #8339:3 ^property[0].code = #parent 
* #8339:3 ^property[0].valueCode = #814-838 
* #8340:3 "Papilläres Karzinom, follikuläre Variante"
* #8340:3 ^designation[0].value = "Papilläres Adenokarzinom, follikuläre Variante" 
* #8340:3 ^designation[1].value = "Gemischt papillär-follikuläres Adenokarzinom" 
* #8340:3 ^designation[2].value = "Gemischt papillär-follikuläres Karzinom" 
* #8340:3 ^property[0].code = #parent 
* #8340:3 ^property[0].valueCode = #814-838 
* #8341:3 "Papilläres Mikrokarzinom"
* #8341:3 ^property[0].code = #parent 
* #8341:3 ^property[0].valueCode = #814-838 
* #8342:3 "Papilläres Karzinom, onkozytische Variante"
* #8342:3 ^designation[0].value = "Oxyphiles papilläres Karzinom" 
* #8342:3 ^property[0].code = #parent 
* #8342:3 ^property[0].valueCode = #814-838 
* #8343:3 "Abgekapseltes papilläres Karzinom"
* #8343:3 ^property[0].code = #parent 
* #8343:3 ^property[0].valueCode = #814-838 
* #8344:3 "Zylinderzelliges papilläres Schilddrüsenkarzinom"
* #8344:3 ^designation[0].value = "Großzelliges papilläres SchilddrüsenkarzinomTall cell papillary carcinoma" 
* #8344:3 ^property[0].code = #parent 
* #8344:3 ^property[0].valueCode = #814-838 
* #8345:3 "Medulläres Schilddrüsenkarzinom"
* #8345:3 ^designation[0].value = "C-Zell-Karzinom" 
* #8345:3 ^designation[1].value = "Parafollikulärzellkarzinom" 
* #8345:3 ^designation[2].value = "Medulläres Karzinom mit amyloidem Stroma" 
* #8345:3 ^property[0].code = #parent 
* #8345:3 ^property[0].valueCode = #814-838 
* #8346:3 "Gemischtzelliges medullär-follikuläres Karzinom"
* #8346:3 ^property[0].code = #parent 
* #8346:3 ^property[0].valueCode = #814-838 
* #8347:3 "Medulläres Karzinom mit papillärer Komponente"
* #8347:3 ^property[0].code = #parent 
* #8347:3 ^property[0].valueCode = #814-838 
* #8348:1 "Gut differenzierter Tumor mit unsicherem Malignitätspotential"
* #8348:1 ^property[0].code = #parent 
* #8348:1 ^property[0].valueCode = #814-838 
* #8349:1 "Nicht-invasive follikuläre Schilddrüsenneoplasie mit Kernmerkmalen eines papillären Tumors (NIFTP)"
* #8349:1 ^property[0].code = #parent 
* #8349:1 ^property[0].valueCode = #814-838 
* #8350:3 "Nichtabgekapseltes sklerosierendes Karzinom"
* #8350:3 ^designation[0].value = "Nichtabgekapselter sklerosierender Tumor" 
* #8350:3 ^designation[1].value = "Nichtabgekapseltes sklerosierendes Adenokarzinom" 
* #8350:3 ^designation[2].value = "Diffus sklerosierendes papilläres Karzinom" 
* #8350:3 ^property[0].code = #parent 
* #8350:3 ^property[0].valueCode = #814-838 
* #8360:1 "Multiple endokrine Adenome"
* #8360:1 ^designation[0].value = "Endokrine Adenomatose" 
* #8360:1 ^property[0].code = #parent 
* #8360:1 ^property[0].valueCode = #814-838 
* #8361:0 "Juxtaglomerulärer Tumor"
* #8361:0 ^designation[0].value = "Reninom" 
* #8361:0 ^property[0].code = #parent 
* #8361:0 ^property[0].valueCode = #814-838 
* #8370:0 "Nebennierenrindenadenom o.n.A."
* #8370:0 ^designation[0].value = "Benigner Nebennierenrindentumor" 
* #8370:0 ^designation[1].value = "Benigner Nebennierenrindentumor o.n.A." 
* #8370:0 ^property[0].code = #parent 
* #8370:0 ^property[0].valueCode = #814-838 
* #8370:3 "Nebennierenrindenkarzinom"
* #8370:3 ^designation[0].value = "Maligner Nebennierenrindentumor" 
* #8370:3 ^designation[1].value = "Nebennierenrindenadenokarzinom" 
* #8370:3 ^property[0].code = #parent 
* #8370:3 ^property[0].valueCode = #814-838 
* #8371:0 "Nebennierenrindenadenom vom Kompaktzelltyp"
* #8371:0 ^property[0].code = #parent 
* #8371:0 ^property[0].valueCode = #814-838 
* #8372:0 "Stark pigmentiertes Nebennierenrindenadenom"
* #8372:0 ^designation[0].value = "Pigmentiertes Adenom" 
* #8372:0 ^designation[1].value = "Schwarzes Adenom" 
* #8372:0 ^property[0].code = #parent 
* #8372:0 ^property[0].valueCode = #814-838 
* #8373:0 "Klarzelliges Nebennierenrindenadenom"
* #8373:0 ^property[0].code = #parent 
* #8373:0 ^property[0].valueCode = #814-838 
* #8374:0 "Glomerulosazelliges Nebennierenrindenadenom"
* #8374:0 ^property[0].code = #parent 
* #8374:0 ^property[0].valueCode = #814-838 
* #8375:0 "Gemischtzelliges Nebennierenrindenadenom"
* #8375:0 ^property[0].code = #parent 
* #8375:0 ^property[0].valueCode = #814-838 
* #8380:0 "Endometrioides Adenom o.n.A."
* #8380:0 ^designation[0].value = "Endometrioides Zystadenom o.n.A." 
* #8380:0 ^property[0].code = #parent 
* #8380:0 ^property[0].valueCode = #814-838 
* #8380:1 "Endometrioides Adenom mit Borderline-Malignität"
* #8380:1 ^designation[0].value = "Atypischer proliferativer endometrioider Tumor" 
* #8380:1 ^designation[1].value = "Endometrioider Borderline-Tumor" 
* #8380:1 ^designation[2].value = "Endometrioider Tumor mit niedrigem Malignitätspotential" 
* #8380:1 ^designation[3].value = "Endometrioides Zystadenom mit Borderline-Malignität" 
* #8380:1 ^property[0].code = #parent 
* #8380:1 ^property[0].valueCode = #814-838 
* #8380:2 "Endometrioide intraepitheliale Neoplasie"
* #8380:2 ^designation[0].value = "Atypische Hyperplasie des Endometriums" 
* #8380:2 ^property[0].code = #parent 
* #8380:2 ^property[0].valueCode = #814-838 
* #8380:3 "Endometrioides Adenokarzinom o.n.A."
* #8380:3 ^designation[0].value = "Endometrioides Karzinom o.n.A." 
* #8380:3 ^designation[1].value = "Endometrioides Zystadenokarzinom" 
* #8380:3 ^property[0].code = #parent 
* #8380:3 ^property[0].valueCode = #814-838 
* #8381:0 "Endometrioides Adenofibrom o.n.A."
* #8381:0 ^designation[0].value = "Endometrioides Zystadenofibrom o.n.A." 
* #8381:0 ^property[0].code = #parent 
* #8381:0 ^property[0].valueCode = #814-838 
* #8381:1 "Endometrioides Adenofibrom mit Borderline-Malignität"
* #8381:1 ^designation[0].value = "Endometrioides Zystadenofibrom mit Borderline-Malignität" 
* #8381:1 ^property[0].code = #parent 
* #8381:1 ^property[0].valueCode = #814-838 
* #8381:3 "Malignes endometrioides Adenofibrom"
* #8381:3 ^designation[0].value = "Malignes endometrioides Zystadenofibrom" 
* #8381:3 ^property[0].code = #parent 
* #8381:3 ^property[0].valueCode = #814-838 
* #8382:3 "Endometrioides Adenokarzinom, sekretorische Variante"
* #8382:3 ^property[0].code = #parent 
* #8382:3 ^property[0].valueCode = #814-838 
* #8383:3 "Endometrioides Adenokarzinom, Flimmerepithel-Variante"
* #8383:3 ^property[0].code = #parent 
* #8383:3 ^property[0].valueCode = #814-838 
* #8384:3 "Endozervikales Adenokarzinom"
* #8384:3 ^property[0].code = #parent 
* #8384:3 ^property[0].valueCode = #814-838 
* #839-842 "Neoplasien der Haut und der Hautanhangsgebilde"
* #839-842 ^property[0].code = #parent 
* #839-842 ^property[0].valueCode = #M 
* #839-842 ^property[1].code = #child 
* #839-842 ^property[1].valueCode = #8390:0 
* #839-842 ^property[2].code = #child 
* #839-842 ^property[2].valueCode = #8390:3 
* #839-842 ^property[3].code = #child 
* #839-842 ^property[3].valueCode = #8391:0 
* #839-842 ^property[4].code = #child 
* #839-842 ^property[4].valueCode = #8392:0 
* #839-842 ^property[5].code = #child 
* #839-842 ^property[5].valueCode = #8400:0 
* #839-842 ^property[6].code = #child 
* #839-842 ^property[6].valueCode = #8400:1 
* #839-842 ^property[7].code = #child 
* #839-842 ^property[7].valueCode = #8400:3 
* #839-842 ^property[8].code = #child 
* #839-842 ^property[8].valueCode = #8401:0 
* #839-842 ^property[9].code = #child 
* #839-842 ^property[9].valueCode = #8401:3 
* #839-842 ^property[10].code = #child 
* #839-842 ^property[10].valueCode = #8402:0 
* #839-842 ^property[11].code = #child 
* #839-842 ^property[11].valueCode = #8402:3 
* #839-842 ^property[12].code = #child 
* #839-842 ^property[12].valueCode = #8403:0 
* #839-842 ^property[13].code = #child 
* #839-842 ^property[13].valueCode = #8403:3 
* #839-842 ^property[14].code = #child 
* #839-842 ^property[14].valueCode = #8404:0 
* #839-842 ^property[15].code = #child 
* #839-842 ^property[15].valueCode = #8405:0 
* #839-842 ^property[16].code = #child 
* #839-842 ^property[16].valueCode = #8406:0 
* #839-842 ^property[17].code = #child 
* #839-842 ^property[17].valueCode = #8406:3 
* #839-842 ^property[18].code = #child 
* #839-842 ^property[18].valueCode = #8407:0 
* #839-842 ^property[19].code = #child 
* #839-842 ^property[19].valueCode = #8407:3 
* #839-842 ^property[20].code = #child 
* #839-842 ^property[20].valueCode = #8408:0 
* #839-842 ^property[21].code = #child 
* #839-842 ^property[21].valueCode = #8408:3 
* #839-842 ^property[22].code = #child 
* #839-842 ^property[22].valueCode = #8409:0 
* #839-842 ^property[23].code = #child 
* #839-842 ^property[23].valueCode = #8409:2 
* #839-842 ^property[24].code = #child 
* #839-842 ^property[24].valueCode = #8409:3 
* #839-842 ^property[25].code = #child 
* #839-842 ^property[25].valueCode = #8410:0 
* #839-842 ^property[26].code = #child 
* #839-842 ^property[26].valueCode = #8410:3 
* #839-842 ^property[27].code = #child 
* #839-842 ^property[27].valueCode = #8413:3 
* #839-842 ^property[28].code = #child 
* #839-842 ^property[28].valueCode = #8420:0 
* #839-842 ^property[29].code = #child 
* #839-842 ^property[29].valueCode = #8420:3 
* #8390:0 "Adenom der Hautanhangsgebilde"
* #8390:0 ^designation[0].value = "Benigner Adnextumor" 
* #8390:0 ^designation[1].value = "Benigner Tumor der Hautanhangsgebilde" 
* #8390:0 ^property[0].code = #parent 
* #8390:0 ^property[0].valueCode = #839-842 
* #8390:3 "Adenokarzinom der Hautanhangsgebilde o.n.A."
* #8390:3 ^designation[0].value = "Adnexkarzinom" 
* #8390:3 ^designation[1].value = "Karzinom der Hautanhangsgebilde" 
* #8390:3 ^property[0].code = #parent 
* #8390:3 ^property[0].valueCode = #839-842 
* #8391:0 "Follikuläres Fibrom"
* #8391:0 ^designation[0].value = "Fibrofollikulom" 
* #8391:0 ^designation[1].value = "Perifollikuläres Fibrom" 
* #8391:0 ^designation[2].value = "Trichodiskom" 
* #8391:0 ^designation[3].value = "Plattenepithel-prädominantes Trichodiskom" 
* #8391:0 ^property[0].code = #parent 
* #8391:0 ^property[0].valueCode = #839-842 
* #8392:0 "Syringofibroadenom"
* #8392:0 ^property[0].code = #parent 
* #8392:0 ^property[0].valueCode = #839-842 
* #8400:0 "Schweißdrüsenadenom"
* #8400:0 ^designation[0].value = "Benigner Schweißdrüsentumor" 
* #8400:0 ^designation[1].value = "Syringadenom o.n.A." 
* #8400:0 ^property[0].code = #parent 
* #8400:0 ^property[0].valueCode = #839-842 
* #8400:1 "Schweißdrüsentumor o.n.A."
* #8400:1 ^property[0].code = #parent 
* #8400:1 ^property[0].valueCode = #839-842 
* #8400:3 "Schweißdrüsenadenokarzinom"
* #8400:3 ^designation[0].value = "Maligner Schweißdrüsentumor" 
* #8400:3 ^designation[1].value = "Schweißdrüsenkarzinom" 
* #8400:3 ^property[0].code = #parent 
* #8400:3 ^property[0].valueCode = #839-842 
* #8401:0 "Apokrines Adenom"
* #8401:0 ^designation[0].value = "Apokrines Zystadenom" 
* #8401:0 ^property[0].code = #parent 
* #8401:0 ^property[0].valueCode = #839-842 
* #8401:3 "Apokrines Adenokarzinom"
* #8401:3 ^designation[0].value = "Apokrines Karzinom" 
* #8401:3 ^property[0].code = #parent 
* #8401:3 ^property[0].valueCode = #839-842 
* #8402:0 "Hidradenom o.n.A."
* #8402:0 ^designation[0].value = "Noduläres Hidradenom" 
* #8402:0 ^designation[1].value = "Ekkrines Akrospirom" 
* #8402:0 ^designation[2].value = "Klarzell-Hidradenom" 
* #8402:0 ^property[0].code = #parent 
* #8402:0 ^property[0].valueCode = #839-842 
* #8402:3 "Malignes noduläres Hidradenom"
* #8402:3 ^designation[0].value = "Hidradenokarzinom" 
* #8402:3 ^property[0].code = #parent 
* #8402:3 ^property[0].valueCode = #839-842 
* #8403:0 "Spiradenom o.n.A."
* #8403:0 ^designation[0].value = "Ekkrines Spiradenom o.n.A." 
* #8403:0 ^property[0].code = #parent 
* #8403:0 ^property[0].valueCode = #839-842 
* #8403:3 "Malignes ekkrines Spiradenom"
* #8403:3 ^designation[0].value = "Bösartige Neubildung in vorbestehendem Spiradenom" 
* #8403:3 ^designation[1].value = "Bösartige Neubildung in vorbestehendem Spiradenozylindrom" 
* #8403:3 ^designation[2].value = "Bösartige Neubildung in vorbestehendem Zylindrom" 
* #8403:3 ^property[0].code = #parent 
* #8403:3 ^property[0].valueCode = #839-842 
* #8404:0 "Hidrozystom"
* #8404:0 ^designation[0].value = "Ekkrines Zystadenom" 
* #8404:0 ^designation[1].value = "Hydreozystadenom" 
* #8404:0 ^property[0].code = #parent 
* #8404:0 ^property[0].valueCode = #839-842 
* #8405:0 "Papilläres Hidradenom"
* #8405:0 ^designation[0].value = "Hidroadenoma papilliferum" 
* #8405:0 ^property[0].code = #parent 
* #8405:0 ^property[0].valueCode = #839-842 
* #8406:0 "Syringocystadenoma papilliferum (SCAP)"
* #8406:0 ^designation[0].value = "Papilläres Syringadenom" 
* #8406:0 ^designation[1].value = "Papilläres Syringozystadenom" 
* #8406:0 ^designation[2].value = "Sialadenoma papilliferum" 
* #8406:0 ^property[0].code = #parent 
* #8406:0 ^property[0].valueCode = #839-842 
* #8406:3 "Syringocystadenocarcinoma papilliferum"
* #8406:3 ^property[0].code = #parent 
* #8406:3 ^property[0].valueCode = #839-842 
* #8407:0 "Syringom o.n.A."
* #8407:0 ^designation[0].value = "Syringomatöser Tumor der BrustwarzeInfiltrierendes syringomatöses Adenom der Brustwarze" 
* #8407:0 ^designation[1].value = "Syringomatöser Tumor der BrustwarzeSyringomatöses Adenom der Brustwarze" 
* #8407:0 ^property[0].code = #parent 
* #8407:0 ^property[0].valueCode = #839-842 
* #8407:3 "Mikrozystisches Karzinom der Hautadnexe"
* #8407:3 ^designation[0].value = "Sklerosierendes Karzinom der Schweißdrüsenausführungsgänge" 
* #8407:3 ^designation[1].value = "Syringomatöses Karzinom" 
* #8407:3 ^property[0].code = #parent 
* #8407:3 ^property[0].valueCode = #839-842 
* #8408:0 "Ekkrines papilläres Adenom"
* #8408:0 ^property[0].code = #parent 
* #8408:0 ^property[0].valueCode = #839-842 
* #8408:3 "Digitales papilläres Adenokarzinom"
* #8408:3 ^designation[0].value = "Aggressives digitales papilläres Adenom" 
* #8408:3 ^designation[1].value = "Ekkrines papilläres Adenokarzinom" 
* #8408:3 ^property[0].code = #parent 
* #8408:3 ^property[0].valueCode = #839-842 
* #8409:0 "Porom o.n.A."
* #8409:0 ^designation[0].value = "Apokrines Porom" 
* #8409:0 ^designation[1].value = "Ekkrines Porom o.n.A." 
* #8409:0 ^property[0].code = #parent 
* #8409:0 ^property[0].valueCode = #839-842 
* #8409:2 "Porocarcinoma in situ"
* #8409:2 ^property[0].code = #parent 
* #8409:2 ^property[0].valueCode = #839-842 
* #8409:3 "Porokarzinom o.n.A."
* #8409:3 ^designation[0].value = "Malignes ekkrines Porom" 
* #8409:3 ^property[0].code = #parent 
* #8409:3 ^property[0].valueCode = #839-842 
* #8410:0 "Sebazeom"
* #8410:0 ^designation[0].value = "Epithelioma sebaceum" 
* #8410:0 ^designation[1].value = "Talgdrüsenadenom" 
* #8410:0 ^property[0].code = #parent 
* #8410:0 ^property[0].valueCode = #839-842 
* #8410:3 "Talgdrüsenkarzinom"
* #8410:3 ^designation[0].value = "Talgdrüsenadenokarzinom" 
* #8410:3 ^property[0].code = #parent 
* #8410:3 ^property[0].valueCode = #839-842 
* #8413:3 "Ekkrines Adenokarzinom"
* #8413:3 ^property[0].code = #parent 
* #8413:3 ^property[0].valueCode = #839-842 
* #8420:0 "Zeruminaladenom"
* #8420:0 ^property[0].code = #parent 
* #8420:0 ^property[0].valueCode = #839-842 
* #8420:3 "Zeruminaladenokarzinom"
* #8420:3 ^designation[0].value = "Zeruminalkarzinom" 
* #8420:3 ^property[0].code = #parent 
* #8420:3 ^property[0].valueCode = #839-842 
* #843-843 "Mukoepidermoide Neoplasien"
* #843-843 ^property[0].code = #parent 
* #843-843 ^property[0].valueCode = #M 
* #843-843 ^property[1].code = #child 
* #843-843 ^property[1].valueCode = #8430:1 
* #843-843 ^property[2].code = #child 
* #843-843 ^property[2].valueCode = #8430:3 
* #8430:1 "Mukoepidermoidtumor"
* #8430:1 ^property[0].code = #parent 
* #8430:1 ^property[0].valueCode = #843-843 
* #8430:3 "Mukoepidermoid-Karzinom"
* #8430:3 ^property[0].code = #parent 
* #8430:3 ^property[0].valueCode = #843-843 
* #844-849 "Zystische, muzinöse und seröse Neoplasien"
* #844-849 ^property[0].code = #parent 
* #844-849 ^property[0].valueCode = #M 
* #844-849 ^property[1].code = #child 
* #844-849 ^property[1].valueCode = #8440:0 
* #844-849 ^property[2].code = #child 
* #844-849 ^property[2].valueCode = #8440:3 
* #844-849 ^property[3].code = #child 
* #844-849 ^property[3].valueCode = #8441:0 
* #844-849 ^property[4].code = #child 
* #844-849 ^property[4].valueCode = #8441:2 
* #844-849 ^property[5].code = #child 
* #844-849 ^property[5].valueCode = #8441:3 
* #844-849 ^property[6].code = #child 
* #844-849 ^property[6].valueCode = #8442:1 
* #844-849 ^property[7].code = #child 
* #844-849 ^property[7].valueCode = #8443:0 
* #844-849 ^property[8].code = #child 
* #844-849 ^property[8].valueCode = #8450:0 
* #844-849 ^property[9].code = #child 
* #844-849 ^property[9].valueCode = #8450:3 
* #844-849 ^property[10].code = #child 
* #844-849 ^property[10].valueCode = #8451:1 
* #844-849 ^property[11].code = #child 
* #844-849 ^property[11].valueCode = #8452:1 
* #844-849 ^property[12].code = #child 
* #844-849 ^property[12].valueCode = #8452:3 
* #844-849 ^property[13].code = #child 
* #844-849 ^property[13].valueCode = #8453:0 
* #844-849 ^property[14].code = #child 
* #844-849 ^property[14].valueCode = #8453:2 
* #844-849 ^property[15].code = #child 
* #844-849 ^property[15].valueCode = #8453:3 
* #844-849 ^property[16].code = #child 
* #844-849 ^property[16].valueCode = #8454:0 
* #844-849 ^property[17].code = #child 
* #844-849 ^property[17].valueCode = #8460:2 
* #844-849 ^property[18].code = #child 
* #844-849 ^property[18].valueCode = #8460:3 
* #844-849 ^property[19].code = #child 
* #844-849 ^property[19].valueCode = #8461:0 
* #844-849 ^property[20].code = #child 
* #844-849 ^property[20].valueCode = #8461:3 
* #844-849 ^property[21].code = #child 
* #844-849 ^property[21].valueCode = #8470:0 
* #844-849 ^property[22].code = #child 
* #844-849 ^property[22].valueCode = #8470:2 
* #844-849 ^property[23].code = #child 
* #844-849 ^property[23].valueCode = #8470:3 
* #844-849 ^property[24].code = #child 
* #844-849 ^property[24].valueCode = #8472:1 
* #844-849 ^property[25].code = #child 
* #844-849 ^property[25].valueCode = #8474:0 
* #844-849 ^property[26].code = #child 
* #844-849 ^property[26].valueCode = #8474:1 
* #844-849 ^property[27].code = #child 
* #844-849 ^property[27].valueCode = #8474:3 
* #844-849 ^property[28].code = #child 
* #844-849 ^property[28].valueCode = #8480:0 
* #844-849 ^property[29].code = #child 
* #844-849 ^property[29].valueCode = #8480:1 
* #844-849 ^property[30].code = #child 
* #844-849 ^property[30].valueCode = #8480:3 
* #844-849 ^property[31].code = #child 
* #844-849 ^property[31].valueCode = #8480:6 
* #844-849 ^property[32].code = #child 
* #844-849 ^property[32].valueCode = #8481:3 
* #844-849 ^property[33].code = #child 
* #844-849 ^property[33].valueCode = #8482:3 
* #844-849 ^property[34].code = #child 
* #844-849 ^property[34].valueCode = #8490:3 
* #844-849 ^property[35].code = #child 
* #844-849 ^property[35].valueCode = #8490:6 
* #8440:0 "Zystadenom o.n.A"
* #8440:0 ^designation[0].value = "Zystom o.n.A." 
* #8440:0 ^property[0].code = #parent 
* #8440:0 ^property[0].valueCode = #844-849 
* #8440:3 "Zystadenokarzinom o.n.A."
* #8440:3 ^property[0].code = #parent 
* #8440:3 ^property[0].valueCode = #844-849 
* #8441:0 "Seröses Zystadenom o.n.A."
* #8441:0 ^designation[0].value = "Seröses Zystom" 
* #8441:0 ^designation[1].value = "Seröses mikrozystisches AdenomPapilläres seröses Zystadenom o.n.A" 
* #8441:0 ^property[0].code = #parent 
* #8441:0 ^property[0].valueCode = #844-849 
* #8441:2 "Seröses intraepitheliales Karzinom"
* #8441:2 ^designation[0].value = "Seröses intraepitheliales Karzinom des Endometriums" 
* #8441:2 ^designation[1].value = "Seröses tubares intraepitheliales Karzinom (STIC)" 
* #8441:2 ^property[0].code = #parent 
* #8441:2 ^property[0].valueCode = #844-849 
* #8441:3 "Seröses Karzinom o.n.A."
* #8441:3 ^designation[0].value = "Seröses Adenokarzinom o.n.A." 
* #8441:3 ^designation[1].value = "Seröses papilläres Adenokarzinom o.n.A." 
* #8441:3 ^designation[2].value = "Seröses papilläres Oberflächenkarzinom" 
* #8441:3 ^designation[3].value = "Seröses papilläres Zystadenokarzinom" 
* #8441:3 ^designation[4].value = "Seröses Zystadenokarzinom o.n.A." 
* #8441:3 ^property[0].code = #parent 
* #8441:3 ^property[0].valueCode = #844-849 
* #8442:1 "Seröser Borderline-Tumor o.n.A."
* #8442:1 ^designation[0].value = "Atypisch proliferierender seröser Tumor" 
* #8442:1 ^designation[1].value = "Atypisch proliferierender serös-papillärer Tumor" 
* #8442:1 ^designation[2].value = "Papilläres seröses Zystadenom mit Borderline-Malignität" 
* #8442:1 ^designation[3].value = "Serös-papillärer Oberflächentumor mit Borderline-Malignität" 
* #8442:1 ^designation[4].value = "Serös-papillärer Tumor mit geringem Malignitätspotential" 
* #8442:1 ^designation[5].value = "Serös-papillärer zystischer Tumor mit Borderline-Malignität" 
* #8442:1 ^designation[6].value = "Seröser Tumor mit geringem Malignitätspotential o.n.A." 
* #8442:1 ^designation[7].value = "Seröses Zystadenom mit Borderline-Malignität" 
* #8442:1 ^property[0].code = #parent 
* #8442:1 ^property[0].valueCode = #844-849 
* #8443:0 "Klarzelliges Zystadenom"
* #8443:0 ^property[0].code = #parent 
* #8443:0 ^property[0].valueCode = #844-849 
* #8450:0 "Papilläres Zystadenom o.n.A."
* #8450:0 ^designation[0].value = "Papilläres Zystadenofibrom" 
* #8450:0 ^property[0].code = #parent 
* #8450:0 ^property[0].valueCode = #844-849 
* #8450:3 "Papilläres Zystadenokarzinom o.n.A."
* #8450:3 ^designation[0].value = "Papillär-zystisches Adenokarzinom" 
* #8450:3 ^property[0].code = #parent 
* #8450:3 ^property[0].valueCode = #844-849 
* #8451:1 "Papilläres Zystadenom mit Borderline-Malignität"
* #8451:1 ^property[0].code = #parent 
* #8451:1 ^property[0].valueCode = #844-849 
* #8452:1 "Solider pseudopapillärer Ovarialtumor"
* #8452:1 ^designation[0].value = "Papillär-zystischer Tumor" 
* #8452:1 ^designation[1].value = "Solide und papilläre epitheliale Neoplasie" 
* #8452:1 ^designation[2].value = "Solider und zystischer Tumor" 
* #8452:1 ^property[0].code = #parent 
* #8452:1 ^property[0].valueCode = #844-849 
* #8452:3 "Solide pseudopapilläre Neoplasie des Pankreas"
* #8452:3 ^designation[0].value = "Solid-pseudopapilläres Karzinom" 
* #8452:3 ^property[0].code = #parent 
* #8452:3 ^property[0].valueCode = #844-849 
* #8453:0 "Intraduktales papillär-muzinöses Adenom"
* #8453:0 ^designation[0].value = "Intraduktaler papillär-muzinöser Tumor mit niedriggradiger DysplasieIntraduktale papillär-muzinöse Neoplasie mit niedriggradiger Dysplasie" 
* #8453:0 ^designation[1].value = "Intraduktaler papillär-muzinöser Tumor mit mäßiger DysplasieIntraduktale papillär-muzinöse Neoplasie mit mäßiger Dysplasie" 
* #8453:0 ^designation[2].value = "Intraduktaler papillär-muzinöser Tumor mit intermediärer Dysplasie" 
* #8453:0 ^property[0].code = #parent 
* #8453:0 ^property[0].valueCode = #844-849 
* #8453:2 "Intraduktale papillär-muzinöse Neoplasie mit hochgradiger Dysplasie"
* #8453:2 ^designation[0].value = "Nichtinvasives intraduktales papillär-muzinöses Karzinom" 
* #8453:2 ^property[0].code = #parent 
* #8453:2 ^property[0].valueCode = #844-849 
* #8453:3 "Intraduktale papillär-muzinöse Neoplasie assoziiert mit invasivem Karzinom"
* #8453:3 ^designation[0].value = "Invasives intraduktales papillär-muzinöses Adenokarzinom" 
* #8453:3 ^property[0].code = #parent 
* #8453:3 ^property[0].valueCode = #844-849 
* #8454:0 "Zystischer Tumor des atrioventrikulären Knotens"
* #8454:0 ^property[0].code = #parent 
* #8454:0 ^property[0].valueCode = #844-849 
* #8460:2 "Seröser Borderline-Tumor, mikropapilläre Variante"
* #8460:2 ^designation[0].value = "Nichtinvasives seröses Karzinom, niedriggradig" 
* #8460:2 ^property[0].code = #parent 
* #8460:2 ^property[0].valueCode = #844-849 
* #8460:3 "Niedriggradiges seröses Karzinom"
* #8460:3 ^designation[0].value = "Seröses mikropapilläres Karzinom" 
* #8460:3 ^property[0].code = #parent 
* #8460:3 ^property[0].valueCode = #844-849 
* #8461:0 "Seröses Oberflächenpapillom o.n.A."
* #8461:0 ^property[0].code = #parent 
* #8461:0 ^property[0].valueCode = #844-849 
* #8461:3 "Hochgradiges seröses Karzinom"
* #8461:3 ^property[0].code = #parent 
* #8461:3 ^property[0].valueCode = #844-849 
* #8470:0 "Muzinöses Zystadenom o.n.A."
* #8470:0 ^designation[0].value = "Muzinöses Zystom" 
* #8470:0 ^designation[1].value = "Papilläres muzinöses Zystadenom o.n.A." 
* #8470:0 ^designation[2].value = "Papilläres pseudomuzinöses Zystadenom o.n.A." 
* #8470:0 ^designation[3].value = "Pseudomuzinöses Zystadenom o.n.A." 
* #8470:0 ^designation[4].value = "Muzinöse zystische Neoplasie mit intermediärgradiger Dysplasie" 
* #8470:0 ^designation[5].value = "Muzinöse zystische Neoplasie mit intermediärgradiger intraepithelialer Neoplasie" 
* #8470:0 ^designation[6].value = "Muzinöse zystische Neoplasie mit niedriggradiger Dysplasie" 
* #8470:0 ^designation[7].value = "Muzinöse zystische Neoplasie mit niedriggradiger intraepithelialer Neoplasie" 
* #8470:0 ^designation[8].value = "Muzinöser zystischer Tumor mit intermediärer Dysplasie" 
* #8470:0 ^designation[9].value = "Muzinöser zystischer Tumor mit mäßiger Dysplasie" 
* #8470:0 ^designation[10].value = "Muzinöser zystischer Tumor mit niedriggradiger Dysplasie" 
* #8470:0 ^property[0].code = #parent 
* #8470:0 ^property[0].valueCode = #844-849 
* #8470:2 "Muzinöse zystische Neoplasie mit hochgradiger Dysplasie"
* #8470:2 ^designation[0].value = "Muzinöser zystischer Tumor mit hochgradiger DysplasieMuzinöse zystische Neoplasie mit hochgradiger intraepithelialer Neoplasie" 
* #8470:2 ^designation[1].value = "Nichtinvasives muzinöses Zystadenom" 
* #8470:2 ^property[0].code = #parent 
* #8470:2 ^property[0].valueCode = #844-849 
* #8470:3 "Muzinöses Zystadenokarzinom o.n.A."
* #8470:3 ^designation[0].value = "Pseudomuzinöses Adenokarzinom" 
* #8470:3 ^designation[1].value = "Pseudomuzinöses Zystadenokarzinom o.n.A." 
* #8470:3 ^designation[2].value = "Muzinöser zystischer Tumor assoziiert mit invasivem KarzinomMuzinöse zystische Neoplasie assoziiert mit invasivem Karzinom" 
* #8470:3 ^designation[3].value = "Muzinöser zystischer Tumor assoziiert mit invasivem KarzinomPapilläres muzinöses Zystadenokarzinom" 
* #8470:3 ^designation[4].value = "Muzinöser zystischer Tumor assoziiert mit invasivem KarzinomPapilläres pseudomuzinöses Zystadenokarzinom" 
* #8470:3 ^property[0].code = #parent 
* #8470:3 ^property[0].valueCode = #844-849 
* #8472:1 "Muzinöser zystischer Tumor mit Borderline-Malignität"
* #8472:1 ^designation[0].value = "Atypisch proliferierender muzinöser Tumor" 
* #8472:1 ^designation[1].value = "Muzinöser Borderline-Tumor" 
* #8472:1 ^designation[2].value = "Muzinöser papillärer Tumor mit geringem Malignitätspotential" 
* #8472:1 ^designation[3].value = "Muzinöser Tumor mit geringem Malignitätspotential o.n.A." 
* #8472:1 ^designation[4].value = "Muzinöses papilläres Zystadenom mit Borderline-Malignität" 
* #8472:1 ^designation[5].value = "Muzinöses Zystadenom mit Borderline-Malignität" 
* #8472:1 ^designation[6].value = "Pseudomuzinöses papilläres Zystadenom mit Borderline-Malignität" 
* #8472:1 ^designation[7].value = "Pseudomuzinöses Zystadenom mit Borderline-Malignität" 
* #8472:1 ^property[0].code = #parent 
* #8472:1 ^property[0].valueCode = #844-849 
* #8474:0 "Seromuzinöses Zystadenom"
* #8474:0 ^property[0].code = #parent 
* #8474:0 ^property[0].valueCode = #844-849 
* #8474:1 "Seromuzinöser Borderline-Tumor"
* #8474:1 ^designation[0].value = "Atypisch proliferierender seromuzinöser Tumor" 
* #8474:1 ^property[0].code = #parent 
* #8474:1 ^property[0].valueCode = #844-849 
* #8474:3 "Seromuzinöses Karzinom"
* #8474:3 ^property[0].code = #parent 
* #8474:3 ^property[0].valueCode = #844-849 
* #8480:0 "Muzinöses Adenom"
* #8480:0 ^designation[0].value = "Adenom der Schleimdrüse" 
* #8480:0 ^property[0].code = #parent 
* #8480:0 ^property[0].valueCode = #844-849 
* #8480:1 "Niedriggradige muzinöse Neoplasie der Appendix"
* #8480:1 ^property[0].code = #parent 
* #8480:1 ^property[0].valueCode = #844-849 
* #8480:3 "Muzinöses Adenokarzinom"
* #8480:3 ^designation[0].value = "Carcinoma gelatinosum" 
* #8480:3 ^designation[1].value = "Gallertadenokarzinom" 
* #8480:3 ^designation[2].value = "Gallertkarzinom" 
* #8480:3 ^designation[3].value = "Kolloidales Adenokarzinom" 
* #8480:3 ^designation[4].value = "Kolloidkarzinom" 
* #8480:3 ^designation[5].value = "Mukoides Karzinom" 
* #8480:3 ^designation[6].value = "Mukoides Adenokarzinom" 
* #8480:3 ^designation[7].value = "Muköses Adenokarzinom" 
* #8480:3 ^designation[8].value = "Muköses Karzinom" 
* #8480:3 ^designation[9].value = "Muzinöses Karzinom" 
* #8480:3 ^designation[10].value = "Muzinöses tubuläres und spindelzelliges Karzinom" 
* #8480:3 ^designation[11].value = "Pseudomyxoma peritonei mit unbekanntem Primärtumor" 
* #8480:3 ^property[0].code = #parent 
* #8480:3 ^property[0].valueCode = #844-849 
* #8480:6 "Pseudomyxoma peritonei"
* #8480:6 ^property[0].code = #parent 
* #8480:6 ^property[0].valueCode = #844-849 
* #8481:3 "Schleimbildendes Adenokarzinom"
* #8481:3 ^designation[0].value = "Schleimbildendes Karzinom" 
* #8481:3 ^designation[1].value = "Schleimsezernierendes Karzinom" 
* #8481:3 ^designation[2].value = "Schleimsezernierendes Adenokarzinom" 
* #8481:3 ^property[0].code = #parent 
* #8481:3 ^property[0].valueCode = #844-849 
* #8482:3 "Muzinöses Adenokarzinom vom gastrischen Typ"
* #8482:3 ^designation[0].value = "Endozervikales muzinöses Adenokarzinom" 
* #8482:3 ^property[0].code = #parent 
* #8482:3 ^property[0].valueCode = #844-849 
* #8490:3 "Siegelringzellkarzinom"
* #8490:3 ^designation[0].value = "Azinäres Adenokarzinom, Siegelring-ähnliche Variante" 
* #8490:3 ^designation[1].value = "Muzinöses Karzinom vom Siegelringzell-Typ" 
* #8490:3 ^designation[2].value = "Siegelringzelladenokarzinom" 
* #8490:3 ^designation[3].value = "Siegelringzell-histiozytäres Karzinom" 
* #8490:3 ^designation[4].value = "Wenig kohäsives Karzinom" 
* #8490:3 ^property[0].code = #parent 
* #8490:3 ^property[0].valueCode = #844-849 
* #8490:6 "Metastase eines Siegelringzellkarzinomes"
* #8490:6 ^designation[0].value = "Krukenberg-Tumor" 
* #8490:6 ^property[0].code = #parent 
* #8490:6 ^property[0].valueCode = #844-849 
* #850-854 "Duktale und lobuläre Neoplasien"
* #850-854 ^property[0].code = #parent 
* #850-854 ^property[0].valueCode = #M 
* #850-854 ^property[1].code = #child 
* #850-854 ^property[1].valueCode = #8500:2 
* #850-854 ^property[2].code = #child 
* #850-854 ^property[2].valueCode = #8500:3 
* #850-854 ^property[3].code = #child 
* #850-854 ^property[3].valueCode = #8501:2 
* #850-854 ^property[4].code = #child 
* #850-854 ^property[4].valueCode = #8501:3 
* #850-854 ^property[5].code = #child 
* #850-854 ^property[5].valueCode = #8502:3 
* #850-854 ^property[6].code = #child 
* #850-854 ^property[6].valueCode = #8503:0 
* #850-854 ^property[7].code = #child 
* #850-854 ^property[7].valueCode = #8503:2 
* #850-854 ^property[8].code = #child 
* #850-854 ^property[8].valueCode = #8503:3 
* #850-854 ^property[9].code = #child 
* #850-854 ^property[9].valueCode = #8504:0 
* #850-854 ^property[10].code = #child 
* #850-854 ^property[10].valueCode = #8504:2 
* #850-854 ^property[11].code = #child 
* #850-854 ^property[11].valueCode = #8504:3 
* #850-854 ^property[12].code = #child 
* #850-854 ^property[12].valueCode = #8505:0 
* #850-854 ^property[13].code = #child 
* #850-854 ^property[13].valueCode = #8506:0 
* #850-854 ^property[14].code = #child 
* #850-854 ^property[14].valueCode = #8507:2 
* #850-854 ^property[15].code = #child 
* #850-854 ^property[15].valueCode = #8507:3 
* #850-854 ^property[16].code = #child 
* #850-854 ^property[16].valueCode = #8508:3 
* #850-854 ^property[17].code = #child 
* #850-854 ^property[17].valueCode = #8509:2 
* #850-854 ^property[18].code = #child 
* #850-854 ^property[18].valueCode = #8509:3 
* #850-854 ^property[19].code = #child 
* #850-854 ^property[19].valueCode = #8510:3 
* #850-854 ^property[20].code = #child 
* #850-854 ^property[20].valueCode = #8512:3 
* #850-854 ^property[21].code = #child 
* #850-854 ^property[21].valueCode = #8513:3 
* #850-854 ^property[22].code = #child 
* #850-854 ^property[22].valueCode = #8514:3 
* #850-854 ^property[23].code = #child 
* #850-854 ^property[23].valueCode = #8519:2 
* #850-854 ^property[24].code = #child 
* #850-854 ^property[24].valueCode = #8520:2 
* #850-854 ^property[25].code = #child 
* #850-854 ^property[25].valueCode = #8520:3 
* #850-854 ^property[26].code = #child 
* #850-854 ^property[26].valueCode = #8521:3 
* #850-854 ^property[27].code = #child 
* #850-854 ^property[27].valueCode = #8522:2 
* #850-854 ^property[28].code = #child 
* #850-854 ^property[28].valueCode = #8522:3 
* #850-854 ^property[29].code = #child 
* #850-854 ^property[29].valueCode = #8523:3 
* #850-854 ^property[30].code = #child 
* #850-854 ^property[30].valueCode = #8524:3 
* #850-854 ^property[31].code = #child 
* #850-854 ^property[31].valueCode = #8525:3 
* #850-854 ^property[32].code = #child 
* #850-854 ^property[32].valueCode = #8530:3 
* #850-854 ^property[33].code = #child 
* #850-854 ^property[33].valueCode = #8540:3 
* #850-854 ^property[34].code = #child 
* #850-854 ^property[34].valueCode = #8541:3 
* #850-854 ^property[35].code = #child 
* #850-854 ^property[35].valueCode = #8542:3 
* #850-854 ^property[36].code = #child 
* #850-854 ^property[36].valueCode = #8543:3 
* #8500:2 "Nichtinfiltrierendes intraduktales Karzinom o.n.A."
* #8500:2 ^designation[0].value = "Intraduktales Karzinom o.n.A." 
* #8500:2 ^designation[1].value = "Nichtinvasives intraduktales Adenokarzinom o.n.A." 
* #8500:2 ^designation[2].value = "Duktales Carcinoma in situ o.n.A.DCIS o.n.A." 
* #8500:2 ^designation[3].value = "Duktales Carcinoma in situ o.n.A.DIN 3" 
* #8500:2 ^designation[4].value = "Duktales Carcinoma in situ o.n.A.Duktale intraepitheliale Neoplasie 3" 
* #8500:2 ^designation[5].value = "Intraduktal zystisch-hypersekretorisches Karzinom" 
* #8500:2 ^property[0].code = #parent 
* #8500:2 ^property[0].valueCode = #850-854 
* #8500:3 "Invasives duktales Karzinom o.n.A."
* #8500:3 ^designation[0].value = "Duktales Adenokarzinom o.n.A." 
* #8500:3 ^designation[1].value = "Duktales Karzinom o.n.A." 
* #8500:3 ^designation[2].value = "Duktalzell-Karzinom" 
* #8500:3 ^designation[3].value = "Invasives duktales Adenokarzinom" 
* #8500:3 ^designation[4].value = "Invasives Mammakarzinom, unspezifischer Typ" 
* #8500:3 ^designation[5].value = "Adenokarzinom der Brustdrüsen-ähnlichen Anogenitaldrüsen" 
* #8500:3 ^designation[6].value = "Adenokarzinom vom Brustdrüsentyp" 
* #8500:3 ^designation[7].value = "Basal-ähnliches Mammakarzinom" 
* #8500:3 ^designation[8].value = "Karzinom der männlichen Brust" 
* #8500:3 ^property[0].code = #parent 
* #8500:3 ^property[0].valueCode = #850-854 
* #8501:2 "Nichtinvasives Komedokarzinom"
* #8501:2 ^designation[0].value = "DCIS vom Komedo-Typ" 
* #8501:2 ^designation[1].value = "Duktales Carcinoma in situ vom Komedo-Typ" 
* #8501:2 ^property[0].code = #parent 
* #8501:2 ^property[0].valueCode = #850-854 
* #8501:3 "Komedokarzinom o.n.A."
* #8501:3 ^property[0].code = #parent 
* #8501:3 ^property[0].valueCode = #850-854 
* #8502:3 "Sekretorisches Mammakarzinom"
* #8502:3 ^designation[0].value = "Juveniles Mammakarzinom" 
* #8502:3 ^property[0].code = #parent 
* #8502:3 ^property[0].valueCode = #850-854 
* #8503:0 "Intraduktales Papillom"
* #8503:0 ^designation[0].value = "Duktales Papillom" 
* #8503:0 ^designation[1].value = "Duktales Adenom o.n.A." 
* #8503:0 ^designation[2].value = "Intraduktale papilläre Neoplasie mit niedriggradiger intraepithelialer Neoplasie" 
* #8503:0 ^designation[3].value = "Intraduktale papilläre Neoplasie o.n.A." 
* #8503:0 ^designation[4].value = "Intraduktale tubulopapilläre Neoplasie, niedriggradig" 
* #8503:0 ^designation[5].value = "Intraduktales Papillom mit atypischer duktaler Hyperplasie" 
* #8503:0 ^designation[6].value = "Intrazystische papilläre Neoplasie mit niedriggradiger intraepithelialer NeoplasieIntraduktale papilläre Neoplasie mit intermediärgradiger Neoplasie" 
* #8503:0 ^designation[7].value = "Intrazystische papilläre Neoplasie mit niedriggradiger intraepithelialer NeoplasieIntraglanduläre papilläre Neoplasie mit niedriggradiger intraepithelialer Neoplasie" 
* #8503:0 ^designation[8].value = "Intrazystische papilläre Neoplasie mit niedriggradiger intraepithelialer NeoplasieIntrazystische papilläre Neoplasie mit intermediärgradiger intraepithelialer Neoplasie" 
* #8503:0 ^property[0].code = #parent 
* #8503:0 ^property[0].valueCode = #850-854 
* #8503:2 "Nichtinvasives intraduktales papilläres Adenokarzinom"
* #8503:2 ^designation[0].value = "Duktales papilläres Carcinoma in situ" 
* #8503:2 ^designation[1].value = "Intraduktales papilläres Adenokarzinom o.n.A." 
* #8503:2 ^designation[2].value = "Intraduktales papilläres Karzinom o.n.A." 
* #8503:2 ^designation[3].value = "Nichtinvasives intraduktales papilläres Karzinom" 
* #8503:2 ^designation[4].value = "Papilläres DCIS" 
* #8503:2 ^designation[5].value = "Hochgradige intraduktale tubulopapilläre Neoplasie" 
* #8503:2 ^designation[6].value = "Intraduktale papilläre Neoplasie mit hochgradiger intraepithelialer NeoplasieIntraduktale papilläre Neoplasie mit hochgradiger Dysplasie" 
* #8503:2 ^designation[7].value = "Intraduktale papilläre Neoplasie mit hochgradiger intraepithelialer NeoplasieIntraduktaler papillärer Tumor mit hochgradiger Dysplasie" 
* #8503:2 ^designation[8].value = "Intraduktale papilläre Neoplasie mit hochgradiger intraepithelialer NeoplasieIntraduktaler papillärer Tumor mit hochgradiger intraepithelialer Neoplasie" 
* #8503:2 ^designation[9].value = "Intraduktale papilläre Neoplasie mit hochgradiger intraepithelialer NeoplasieIntrazystische papilläre Neoplasie mit hochgradiger intraepithelialer Neoplasie" 
* #8503:2 ^designation[10].value = "Intraduktale papilläre Neoplasie mit hochgradiger intraepithelialer NeoplasieIntrazystischer papillärer Tumor mit hochgradiger Dysplasie" 
* #8503:2 ^designation[11].value = "Intraduktale papilläre Neoplasie mit hochgradiger intraepithelialer NeoplasieIntrazystischer papillärer Tumor mit hochgradiger intraepithelialer Neoplasie" 
* #8503:2 ^designation[12].value = "Intraduktale papilläre Neoplasie mit hochgradiger intraepithelialer NeoplasieIntrazystischer papillärer Tumor mit hochgradiger intraepithelialer Neoplasie" 
* #8503:2 ^designation[13].value = "Intraduktale tubulopapilläre Neoplasie" 
* #8503:2 ^designation[14].value = "Intraduktales Papillom mit DCIS" 
* #8503:2 ^property[0].code = #parent 
* #8503:2 ^property[0].valueCode = #850-854 
* #8503:3 "Intraduktales papilläres Adenokarzinom mit Invasion"
* #8503:3 ^designation[0].value = "Invasives papilläres Adenokarzinom" 
* #8503:3 ^designation[1].value = "Invasives und papilläres Adenokarzinom" 
* #8503:3 ^designation[2].value = "Intraduktale papilläre Neoplasie assoziiert mit invasivem KarzinomIntrazystische papilläre Neoplasie assoziiert mit invasivem Karzinom" 
* #8503:3 ^property[0].code = #parent 
* #8503:3 ^property[0].valueCode = #850-854 
* #8504:0 "Intrazystisches papilläres Adenom"
* #8504:0 ^designation[0].value = "Intrazystisches Papillom" 
* #8504:0 ^property[0].code = #parent 
* #8504:0 ^property[0].valueCode = #850-854 
* #8504:2 "Abgekapseltes papilläres Karzinom"
* #8504:2 ^designation[0].value = "Eingekapseltes papilläres Karzinom" 
* #8504:2 ^designation[1].value = "Intrazystisches Karzinom o.n.A." 
* #8504:2 ^designation[2].value = "Intrazystisches papilläres Adenokarzinom" 
* #8504:2 ^designation[3].value = "Intrazystisches papilläres Karzinom" 
* #8504:2 ^designation[4].value = "Nichtinvasives intrazystisches Karzinom" 
* #8504:2 ^property[0].code = #parent 
* #8504:2 ^property[0].valueCode = #850-854 
* #8504:3 "Abgekapseltes papilläres Karzinom mit Invasion"
* #8504:3 ^designation[0].value = "Eingekapseltes papilläres Karzinom mit Invasion" 
* #8504:3 ^designation[1].value = "Intrazystisches papilläres Karzinom mit Invasion" 
* #8504:3 ^property[0].code = #parent 
* #8504:3 ^property[0].valueCode = #850-854 
* #8505:0 "Intraduktale Papillomatose o.n.A."
* #8505:0 ^designation[0].value = "Diffuse intraduktale Papillomatose" 
* #8505:0 ^property[0].code = #parent 
* #8505:0 ^property[0].valueCode = #850-854 
* #8506:0 "Brustwarzen-Adenom"
* #8506:0 ^designation[0].value = "Subareoläre Milchgangs-Papillomatose" 
* #8506:0 ^property[0].code = #parent 
* #8506:0 ^property[0].valueCode = #850-854 
* #8507:2 "Intraduktales mikropapilläres Karzinom"
* #8507:2 ^designation[0].value = "Mikropapilläres duktales Carcinoma in situ" 
* #8507:2 ^designation[1].value = "Tapetenartiges intraduktales Karzinom" 
* #8507:2 ^property[0].code = #parent 
* #8507:2 ^property[0].valueCode = #850-854 
* #8507:3 "Invasives mikropapilläres Mammakarzinom"
* #8507:3 ^designation[0].value = "Mikropapilläres Mammakarzinom" 
* #8507:3 ^property[0].code = #parent 
* #8507:3 ^property[0].valueCode = #850-854 
* #8508:3 "Zystisch-hypersekretorisches Karzinom"
* #8508:3 ^property[0].code = #parent 
* #8508:3 ^property[0].valueCode = #850-854 
* #8509:2 "Solides papilläres Carcinoma in situ"
* #8509:2 ^designation[0].value = "Endokrines muzin-produzierendes Carcinoma in situ der Schweißdrüse" 
* #8509:2 ^property[0].code = #parent 
* #8509:2 ^property[0].valueCode = #850-854 
* #8509:3 "Solides papilläres Karzinom mit Invasion"
* #8509:3 ^designation[0].value = "Endokrines muzin-produzierendes Schweißdrüsenkarzinom" 
* #8509:3 ^property[0].code = #parent 
* #8509:3 ^property[0].valueCode = #850-854 
* #8510:3 "Medulläres Karzinom o.n.A."
* #8510:3 ^designation[0].value = "Medullär-ähnliches Karzinom" 
* #8510:3 ^designation[1].value = "Medulläres Adenokarzinom" 
* #8510:3 ^property[0].code = #parent 
* #8510:3 ^property[0].valueCode = #850-854 
* #8512:3 "Medulläres Karzinom mit lymphoidem Stroma"
* #8512:3 ^property[0].code = #parent 
* #8512:3 ^property[0].valueCode = #850-854 
* #8513:3 "Atypisches medulläres Karzinom"
* #8513:3 ^property[0].code = #parent 
* #8513:3 ^property[0].valueCode = #850-854 
* #8514:3 "Desmoplastisches duktales Karzinom"
* #8514:3 ^property[0].code = #parent 
* #8514:3 ^property[0].valueCode = #850-854 
* #8519:2 "Pleomorphes lobuläres Carcinoma in situ"
* #8519:2 ^designation[0].value = "Pleomorphes LCIS" 
* #8519:2 ^property[0].code = #parent 
* #8519:2 ^property[0].valueCode = #850-854 
* #8520:2 "Lobuläres Carcinoma in situ o.n.A."
* #8520:2 ^designation[0].value = "LCIS o.n.A." 
* #8520:2 ^designation[1].value = "Klassisches lobuläres Carcinoma in situ" 
* #8520:2 ^designation[2].value = "Nichtinvasives lobuläres Karzinom" 
* #8520:2 ^designation[3].value = "Intraduktales Papillom mit lobulärem Carcinoma in situ" 
* #8520:2 ^property[0].code = #parent 
* #8520:2 ^property[0].valueCode = #850-854 
* #8520:3 "Lobuläres Karzinom o.n.A."
* #8520:3 ^designation[0].value = "Invasives lobuläres Karzinom o.n.A." 
* #8520:3 ^designation[1].value = "Lobuläres Adenokarzinom" 
* #8520:3 ^designation[2].value = "Pleomorphes lobuläres Karzinom" 
* #8520:3 ^designation[3].value = "Tubulolobuläres Karzinom" 
* #8520:3 ^property[0].code = #parent 
* #8520:3 ^property[0].valueCode = #850-854 
* #8521:3 "Invasives duktuläres Karzinom"
* #8521:3 ^property[0].code = #parent 
* #8521:3 ^property[0].valueCode = #850-854 
* #8522:2 "Intraduktales Karzinom und lobuläres Carcinoma in situ"
* #8522:2 ^property[0].code = #parent 
* #8522:2 ^property[0].valueCode = #850-854 
* #8522:3 "Invasives duktales und lobuläres Karzinom"
* #8522:3 ^designation[0].value = "Lobuläres und duktales Karzinom" 
* #8522:3 ^designation[1].value = "Intraduktales und lobuläres Karzinom" 
* #8522:3 ^designation[2].value = "Invasives duktales und lobuläres Carcinoma in situ" 
* #8522:3 ^designation[3].value = "Invasives lobuläres Karzinom und duktales Carcinoma in situ" 
* #8522:3 ^property[0].code = #parent 
* #8522:3 ^property[0].valueCode = #850-854 
* #8523:3 "Invasives duktales Karzinom gemischt mit anderen Karzinom-Typen"
* #8523:3 ^designation[0].value = "Invasives duktales und Kolloidkarzinom" 
* #8523:3 ^designation[1].value = "Invasives duktales und kribriformes Karzinom" 
* #8523:3 ^designation[2].value = "Invasives duktales und muzinöses Karzinom" 
* #8523:3 ^designation[3].value = "Invasives duktales und tubuläres Karzinom" 
* #8523:3 ^property[0].code = #parent 
* #8523:3 ^property[0].valueCode = #850-854 
* #8524:3 "Invasives lobuläres Karzinom gemischt mit anderen Karzinom-Typen"
* #8524:3 ^property[0].code = #parent 
* #8524:3 ^property[0].valueCode = #850-854 
* #8525:3 "Polymorphes Adenokarzinom"
* #8525:3 ^designation[0].value = "Polymorphes Karzinom" 
* #8525:3 ^designation[1].value = "Terminales duktales Adenokarzinom" 
* #8525:3 ^property[0].code = #parent 
* #8525:3 ^property[0].valueCode = #850-854 
* #8530:3 "Inflammatorisches Karzinom"
* #8530:3 ^designation[0].value = "Entzündliches Adenokarzinom" 
* #8530:3 ^property[0].code = #parent 
* #8530:3 ^property[0].valueCode = #850-854 
* #8540:3 "M. Paget der Brust"
* #8540:3 ^designation[0].value = "M. Paget der Brustwarze" 
* #8540:3 ^property[0].code = #parent 
* #8540:3 ^property[0].valueCode = #850-854 
* #8541:3 "M. Paget mit invasivem duktalem Karzinom"
* #8541:3 ^property[0].code = #parent 
* #8541:3 ^property[0].valueCode = #850-854 
* #8542:3 "Extramammärer M. Paget"
* #8542:3 ^property[0].code = #parent 
* #8542:3 ^property[0].valueCode = #850-854 
* #8543:3 "M. Paget mit intraduktalem Karzinom"
* #8543:3 ^property[0].code = #parent 
* #8543:3 ^property[0].valueCode = #850-854 
* #855-855 "Azinuszellneoplasien"
* #855-855 ^property[0].code = #parent 
* #855-855 ^property[0].valueCode = #M 
* #855-855 ^property[1].code = #child 
* #855-855 ^property[1].valueCode = #8550:0 
* #855-855 ^property[2].code = #child 
* #855-855 ^property[2].valueCode = #8550:1 
* #855-855 ^property[3].code = #child 
* #855-855 ^property[3].valueCode = #8550:3 
* #855-855 ^property[4].code = #child 
* #855-855 ^property[4].valueCode = #8551:3 
* #855-855 ^property[5].code = #child 
* #855-855 ^property[5].valueCode = #8552:3 
* #8550:0 "Azinarzelladenom"
* #8550:0 ^designation[0].value = "Azinuszelladenom" 
* #8550:0 ^property[0].code = #parent 
* #8550:0 ^property[0].valueCode = #855-855 
* #8550:1 "Azinarzelltumor"
* #8550:1 ^designation[0].value = "Azinuszelltumor" 
* #8550:1 ^property[0].code = #parent 
* #8550:1 ^property[0].valueCode = #855-855 
* #8550:3 "Azinuszellkarzinom"
* #8550:3 ^designation[0].value = "Azinäres Adenokarzinom" 
* #8550:3 ^designation[1].value = "Azinarkarzinom" 
* #8550:3 ^designation[2].value = "Azinuszell-Adenokarzinom" 
* #8550:3 ^property[0].code = #parent 
* #8550:3 ^property[0].valueCode = #855-855 
* #8551:3 "Azinuszell-Zystadenokarzinom"
* #8551:3 ^designation[0].value = "Azinäres Adenokarzinom der Lunge" 
* #8551:3 ^property[0].code = #parent 
* #8551:3 ^property[0].valueCode = #855-855 
* #8552:3 "Gemischtes azinär-duktales Karzinom"
* #8552:3 ^property[0].code = #parent 
* #8552:3 ^property[0].valueCode = #855-855 
* #856-857 "Komplexe epitheliale Neoplasien"
* #856-857 ^property[0].code = #parent 
* #856-857 ^property[0].valueCode = #M 
* #856-857 ^property[1].code = #child 
* #856-857 ^property[1].valueCode = #8560:0 
* #856-857 ^property[2].code = #child 
* #856-857 ^property[2].valueCode = #8560:3 
* #856-857 ^property[3].code = #child 
* #856-857 ^property[3].valueCode = #8561:0 
* #856-857 ^property[4].code = #child 
* #856-857 ^property[4].valueCode = #8562:3 
* #856-857 ^property[5].code = #child 
* #856-857 ^property[5].valueCode = #8563:0 
* #856-857 ^property[6].code = #child 
* #856-857 ^property[6].valueCode = #8570:3 
* #856-857 ^property[7].code = #child 
* #856-857 ^property[7].valueCode = #8571:3 
* #856-857 ^property[8].code = #child 
* #856-857 ^property[8].valueCode = #8572:3 
* #856-857 ^property[9].code = #child 
* #856-857 ^property[9].valueCode = #8573:3 
* #856-857 ^property[10].code = #child 
* #856-857 ^property[10].valueCode = #8574:3 
* #856-857 ^property[11].code = #child 
* #856-857 ^property[11].valueCode = #8575:3 
* #856-857 ^property[12].code = #child 
* #856-857 ^property[12].valueCode = #8576:3 
* #8560:0 "Gemischtes squamös-glanduläres Papillom"
* #8560:0 ^designation[0].value = "Tubulo-squamöser Polyp" 
* #8560:0 ^property[0].code = #parent 
* #8560:0 ^property[0].valueCode = #856-857 
* #8560:3 "Adenosquamöses Karzinom"
* #8560:3 ^designation[0].value = "Adenokarzinomatös-epidermoider Tumor" 
* #8560:3 ^designation[1].value = "Kombiniertes Adeno-Plattenepithelkarzinom" 
* #8560:3 ^designation[2].value = "Ekkrin-duktales Plattenepithelzellkarzinom" 
* #8560:3 ^property[0].code = #parent 
* #8560:3 ^property[0].valueCode = #856-857 
* #8561:0 "Adenolymphom"
* #8561:0 ^designation[0].value = "Papilläres lymphomatöses Zystadenom" 
* #8561:0 ^designation[1].value = "Warthin-Tumor" 
* #8561:0 ^property[0].code = #parent 
* #8561:0 ^property[0].valueCode = #856-857 
* #8562:3 "Epithelial-myoepitheliales Karzinom"
* #8562:3 ^property[0].code = #parent 
* #8562:3 ^property[0].valueCode = #856-857 
* #8563:0 "Lymphadenom"
* #8563:0 ^property[0].code = #parent 
* #8563:0 ^property[0].valueCode = #856-857 
* #8570:3 "Adenokarzinom mit Plattenepithelmetaplasie"
* #8570:3 ^designation[0].value = "Adenoakanthom" 
* #8570:3 ^designation[1].value = "Endometrioidkarzinom mit plattenepithelialer Differenzierung" 
* #8570:3 ^property[0].code = #parent 
* #8570:3 ^property[0].valueCode = #856-857 
* #8571:3 "Adenokarzinom mit Knorpel- und Knochenmetaplasie"
* #8571:3 ^designation[0].value = "Adenokarzinom mit chondroider Differenzierung" 
* #8571:3 ^designation[1].value = "Adenokarzinom mit Knochenmetaplasie" 
* #8571:3 ^designation[2].value = "Adenokarzinom mit Knorpelmetaplasie" 
* #8571:3 ^property[0].code = #parent 
* #8571:3 ^property[0].valueCode = #856-857 
* #8572:3 "Adenokarzinom mit Spindelzellmetaplasie"
* #8572:3 ^designation[0].value = "Azinäres Adenokarzinom, sarkomatoide Variante" 
* #8572:3 ^designation[1].value = "Fibromatose-ähnliches metaplastisches Karzinom" 
* #8572:3 ^property[0].code = #parent 
* #8572:3 ^property[0].valueCode = #856-857 
* #8573:3 "Adenokarzinom mit apokriner Metaplasie"
* #8573:3 ^designation[0].value = "Karzinom mit apokriner Metaplasie" 
* #8573:3 ^property[0].code = #parent 
* #8573:3 ^property[0].valueCode = #856-857 
* #8574:3 "Adenokarzinom mit neuroendokriner Differenzierung"
* #8574:3 ^designation[0].value = "Adenokarzinom gemischt mit neuroendokrinem Karzinom" 
* #8574:3 ^designation[1].value = "Karzinom mit neuroendokriner Differenzierung" 
* #8574:3 ^property[0].code = #parent 
* #8574:3 ^property[0].valueCode = #856-857 
* #8575:3 "Metaplastisches Karzinom o.n.A."
* #8575:3 ^property[0].code = #parent 
* #8575:3 ^property[0].valueCode = #856-857 
* #8576:3 "Hepatoides Adenokarzinom"
* #8576:3 ^designation[0].value = "Hepatoides Karzinom" 
* #8576:3 ^property[0].code = #parent 
* #8576:3 ^property[0].valueCode = #856-857 
* #858-858 "Epitheliale Neoplasien des Thymus"
* #858-858 ^property[0].code = #parent 
* #858-858 ^property[0].valueCode = #M 
* #858-858 ^property[1].code = #child 
* #858-858 ^property[1].valueCode = #8580:0 
* #858-858 ^property[2].code = #child 
* #858-858 ^property[2].valueCode = #8580:1 
* #858-858 ^property[3].code = #child 
* #858-858 ^property[3].valueCode = #8580:3 
* #858-858 ^property[4].code = #child 
* #858-858 ^property[4].valueCode = #8581:3 
* #858-858 ^property[5].code = #child 
* #858-858 ^property[5].valueCode = #8582:3 
* #858-858 ^property[6].code = #child 
* #858-858 ^property[6].valueCode = #8583:3 
* #858-858 ^property[7].code = #child 
* #858-858 ^property[7].valueCode = #8584:3 
* #858-858 ^property[8].code = #child 
* #858-858 ^property[8].valueCode = #8585:3 
* #858-858 ^property[9].code = #child 
* #858-858 ^property[9].valueCode = #8586:3 
* #858-858 ^property[10].code = #child 
* #858-858 ^property[10].valueCode = #8587:0 
* #858-858 ^property[11].code = #child 
* #858-858 ^property[11].valueCode = #8588:3 
* #858-858 ^property[12].code = #child 
* #858-858 ^property[12].valueCode = #8589:3 
* #8580:0 "Mikroskopisches Thymom"
* #8580:0 ^designation[0].value = "Benignes Thymom" 
* #8580:0 ^property[0].code = #parent 
* #8580:0 ^property[0].valueCode = #858-858 
* #8580:1 "Mikronoduläres Thymom mit lymphoidem Stroma"
* #8580:1 ^property[0].code = #parent 
* #8580:1 ^property[0].valueCode = #858-858 
* #8580:3 "Thymom o.n.A."
* #8580:3 ^designation[0].value = "Intrapulmonales Thymom" 
* #8580:3 ^designation[1].value = "Metaplastisches Thymom" 
* #8580:3 ^designation[2].value = "Sklerosierendes Thymom" 
* #8580:3 ^property[0].code = #parent 
* #8580:3 ^property[0].valueCode = #858-858 
* #8581:3 "Thymom vom Typ A"
* #8581:3 ^designation[0].value = "Medulläres Thymom" 
* #8581:3 ^designation[1].value = "Spindelzelliges Thymom" 
* #8581:3 ^property[0].code = #parent 
* #8581:3 ^property[0].valueCode = #858-858 
* #8582:3 "Thymom vom Typ AB"
* #8582:3 ^designation[0].value = "Thymom vom Mischtyp" 
* #8582:3 ^property[0].code = #parent 
* #8582:3 ^property[0].valueCode = #858-858 
* #8583:3 "Thymom vom Typ B1"
* #8583:3 ^designation[0].value = "Lymphozytenreiches Thymom" 
* #8583:3 ^designation[1].value = "Lymphozytisches Thymom" 
* #8583:3 ^designation[2].value = "Organoides Thymom" 
* #8583:3 ^designation[3].value = "Prädominant kortikales Thymom" 
* #8583:3 ^property[0].code = #parent 
* #8583:3 ^property[0].valueCode = #858-858 
* #8584:3 "Thymom vom Typ B2"
* #8584:3 ^designation[0].value = "Kortikales Thymom" 
* #8584:3 ^property[0].code = #parent 
* #8584:3 ^property[0].valueCode = #858-858 
* #8585:3 "Thymom vom Typ B3"
* #8585:3 ^designation[0].value = "Atypisches Thymom" 
* #8585:3 ^designation[1].value = "Epitheliales Thymom" 
* #8585:3 ^designation[2].value = "Gut differenziertes Thymuskarzinom" 
* #8585:3 ^property[0].code = #parent 
* #8585:3 ^property[0].valueCode = #858-858 
* #8586:3 "Thymuskarzinom o.n.A."
* #8586:3 ^designation[0].value = "Thymom vom Typ C" 
* #8586:3 ^property[0].code = #parent 
* #8586:3 ^property[0].valueCode = #858-858 
* #8587:0 "Ektope hamartomatöse Thymome"
* #8587:0 ^property[0].code = #parent 
* #8587:0 ^property[0].valueCode = #858-858 
* #8588:3 "Spindelzellig-epithelialer Tumor mit thymusähnlichen Anteilen"
* #8588:3 ^designation[0].value = "Spindelzellig-epithelialer Tumor mit thymusähnlicher Differenzierung" 
* #8588:3 ^designation[1].value = "Spindle epithelial tumor with thymus-like elementSETTLE" 
* #8588:3 ^property[0].code = #parent 
* #8588:3 ^property[0].valueCode = #858-858 
* #8589:3 "Intrathyroidales Thymuskarzinom"
* #8589:3 ^designation[0].value = "Carcinoma showing thymus-like elementCASTLE" 
* #8589:3 ^designation[1].value = "Karzinome mit thymusähnlichen Anteilen" 
* #8589:3 ^designation[2].value = "Karzinome mit thymusähnlicher Differenzierung" 
* #8589:3 ^property[0].code = #parent 
* #8589:3 ^property[0].valueCode = #858-858 
* #859-867 "Spezielle Neoplasien der Gonaden"
* #859-867 ^property[0].code = #parent 
* #859-867 ^property[0].valueCode = #M 
* #859-867 ^property[1].code = #child 
* #859-867 ^property[1].valueCode = #8590:0 
* #859-867 ^property[2].code = #child 
* #859-867 ^property[2].valueCode = #8590:1 
* #859-867 ^property[3].code = #child 
* #859-867 ^property[3].valueCode = #8591:1 
* #859-867 ^property[4].code = #child 
* #859-867 ^property[4].valueCode = #8592:1 
* #859-867 ^property[5].code = #child 
* #859-867 ^property[5].valueCode = #8593:1 
* #859-867 ^property[6].code = #child 
* #859-867 ^property[6].valueCode = #8594:1 
* #859-867 ^property[7].code = #child 
* #859-867 ^property[7].valueCode = #8600:0 
* #859-867 ^property[8].code = #child 
* #859-867 ^property[8].valueCode = #8600:3 
* #859-867 ^property[9].code = #child 
* #859-867 ^property[9].valueCode = #8601:0 
* #859-867 ^property[10].code = #child 
* #859-867 ^property[10].valueCode = #8602:0 
* #859-867 ^property[11].code = #child 
* #859-867 ^property[11].valueCode = #8610:0 
* #859-867 ^property[12].code = #child 
* #859-867 ^property[12].valueCode = #8620:1 
* #859-867 ^property[13].code = #child 
* #859-867 ^property[13].valueCode = #8620:3 
* #859-867 ^property[14].code = #child 
* #859-867 ^property[14].valueCode = #8621:1 
* #859-867 ^property[15].code = #child 
* #859-867 ^property[15].valueCode = #8622:0 
* #859-867 ^property[16].code = #child 
* #859-867 ^property[16].valueCode = #8622:1 
* #859-867 ^property[17].code = #child 
* #859-867 ^property[17].valueCode = #8623:1 
* #859-867 ^property[18].code = #child 
* #859-867 ^property[18].valueCode = #8630:0 
* #859-867 ^property[19].code = #child 
* #859-867 ^property[19].valueCode = #8630:1 
* #859-867 ^property[20].code = #child 
* #859-867 ^property[20].valueCode = #8630:3 
* #859-867 ^property[21].code = #child 
* #859-867 ^property[21].valueCode = #8631:0 
* #859-867 ^property[22].code = #child 
* #859-867 ^property[22].valueCode = #8631:1 
* #859-867 ^property[23].code = #child 
* #859-867 ^property[23].valueCode = #8631:3 
* #859-867 ^property[24].code = #child 
* #859-867 ^property[24].valueCode = #8632:1 
* #859-867 ^property[25].code = #child 
* #859-867 ^property[25].valueCode = #8633:1 
* #859-867 ^property[26].code = #child 
* #859-867 ^property[26].valueCode = #8634:1 
* #859-867 ^property[27].code = #child 
* #859-867 ^property[27].valueCode = #8634:3 
* #859-867 ^property[28].code = #child 
* #859-867 ^property[28].valueCode = #8640:1 
* #859-867 ^property[29].code = #child 
* #859-867 ^property[29].valueCode = #8640:3 
* #859-867 ^property[30].code = #child 
* #859-867 ^property[30].valueCode = #8641:0 
* #859-867 ^property[31].code = #child 
* #859-867 ^property[31].valueCode = #8642:1 
* #859-867 ^property[32].code = #child 
* #859-867 ^property[32].valueCode = #8643:1 
* #859-867 ^property[33].code = #child 
* #859-867 ^property[33].valueCode = #8650:0 
* #859-867 ^property[34].code = #child 
* #859-867 ^property[34].valueCode = #8650:1 
* #859-867 ^property[35].code = #child 
* #859-867 ^property[35].valueCode = #8650:3 
* #859-867 ^property[36].code = #child 
* #859-867 ^property[36].valueCode = #8660:0 
* #859-867 ^property[37].code = #child 
* #859-867 ^property[37].valueCode = #8670:0 
* #859-867 ^property[38].code = #child 
* #859-867 ^property[38].valueCode = #8670:3 
* #859-867 ^property[39].code = #child 
* #859-867 ^property[39].valueCode = #8671:0 
* #8590:0 "Benigner Keimstrang-Stromatumor"
* #8590:0 ^designation[0].value = "Mikrozystischer Stromatumor" 
* #8590:0 ^designation[1].value = "Siegelring-Stromatumor" 
* #8590:0 ^property[0].code = #parent 
* #8590:0 ^property[0].valueCode = #859-867 
* #8590:1 "Keimstrang-Stromatumor o.n.A."
* #8590:1 ^designation[0].value = "Gonaden-Stromatumor o.n.A." 
* #8590:1 ^designation[1].value = "Hoden-Stromatumor" 
* #8590:1 ^designation[2].value = "Keimstrangtumor o.n.A." 
* #8590:1 ^designation[3].value = "Ovar-Stromatumor" 
* #8590:1 ^designation[4].value = "Uteriner Tumor mit keimstrang-ähnlicher Differenzierung" 
* #8590:1 ^property[0].code = #parent 
* #8590:1 ^property[0].valueCode = #859-867 
* #8591:1 "Inkomplett differenzierter Keimstrang-Stromatumor"
* #8591:1 ^designation[0].value = "Keimstrangtumor, nicht klassifiziert" 
* #8591:1 ^property[0].code = #parent 
* #8591:1 ^property[0].valueCode = #859-867 
* #8592:1 "Keimstrang-Stromatumor, Mischtypen"
* #8592:1 ^property[0].code = #parent 
* #8592:1 ^property[0].valueCode = #859-867 
* #8593:1 "Stromatumor mit geringeren Keimstranganteilen"
* #8593:1 ^property[0].code = #parent 
* #8593:1 ^property[0].valueCode = #859-867 
* #8594:1 "Gemischter Keimzell-Keimstrang-Stromatumor o.n.A."
* #8594:1 ^designation[0].value = "Unklassifizierter gemischter Keimzell-Keimstrang-Stromatumor" 
* #8594:1 ^property[0].code = #parent 
* #8594:1 ^property[0].valueCode = #859-867 
* #8600:0 "Thekom o.n.A."
* #8600:0 ^designation[0].value = "Thekazelltumor" 
* #8600:0 ^property[0].code = #parent 
* #8600:0 ^property[0].valueCode = #859-867 
* #8600:3 "Malignes Thekom"
* #8600:3 ^property[0].code = #parent 
* #8600:3 ^property[0].valueCode = #859-867 
* #8601:0 "Luteinisiertes Thekom"
* #8601:0 ^property[0].code = #parent 
* #8601:0 ^property[0].valueCode = #859-867 
* #8602:0 "Sklerosierender Stromatumor"
* #8602:0 ^property[0].code = #parent 
* #8602:0 ^property[0].valueCode = #859-867 
* #8610:0 "Luteom o.n.A."
* #8610:0 ^designation[0].value = "Luteinom" 
* #8610:0 ^property[0].code = #parent 
* #8610:0 ^property[0].valueCode = #859-867 
* #8620:1 "Adulter Granulosazelltumor der Hoden"
* #8620:1 ^designation[0].value = "Granulosazelltumor der Hoden o.n.A." 
* #8620:1 ^property[0].code = #parent 
* #8620:1 ^property[0].valueCode = #859-867 
* #8620:3 "Maligner Granulosazelltumor des Ovars"
* #8620:3 ^designation[0].value = "Adulter Granulosazelltumor" 
* #8620:3 ^designation[1].value = "Granulosazellkarzinom" 
* #8620:3 ^designation[2].value = "Sarkomatoider Granulosazelltumor" 
* #8620:3 ^property[0].code = #parent 
* #8620:3 ^property[0].valueCode = #859-867 
* #8621:1 "Granulosa-Thekazelltumor"
* #8621:1 ^designation[0].value = "Theka-Granulosazelltumor" 
* #8621:1 ^property[0].code = #parent 
* #8621:1 ^property[0].valueCode = #859-867 
* #8622:0 "Juveniler Granulosazelltumor der Hoden"
* #8622:0 ^property[0].code = #parent 
* #8622:0 ^property[0].valueCode = #859-867 
* #8622:1 "Juveniler Granulosazelltumor"
* #8622:1 ^property[0].code = #parent 
* #8622:1 ^property[0].valueCode = #859-867 
* #8623:1 "Keimstrangtumor mit anulären Tubuli"
* #8623:1 ^property[0].code = #parent 
* #8623:1 ^property[0].valueCode = #859-867 
* #8630:0 "Benignes Androblastom"
* #8630:0 ^designation[0].value = "Benignes Arrhenoblastom" 
* #8630:0 ^property[0].code = #parent 
* #8630:0 ^property[0].valueCode = #859-867 
* #8630:1 "Androblastom o.n.A."
* #8630:1 ^designation[0].value = "Arrhenoblastom o.n.A." 
* #8630:1 ^property[0].code = #parent 
* #8630:1 ^property[0].valueCode = #859-867 
* #8630:3 "Malignes Androblastom"
* #8630:3 ^designation[0].value = "Malignes Arrhenoblastom" 
* #8630:3 ^property[0].code = #parent 
* #8630:3 ^property[0].valueCode = #859-867 
* #8631:0 "Gut differenzierter Sertoli-Leydig-Zell-Tumor"
* #8631:0 ^property[0].code = #parent 
* #8631:0 ^property[0].valueCode = #859-867 
* #8631:1 "Intermediär differenzierter Sertoli-Leydig-Zell-Tumor"
* #8631:1 ^designation[0].value = "Mäßig differenzierter Sertoli-Leydig-Zell-Tumor" 
* #8631:1 ^designation[1].value = "Sertoli-Leydig-Zell-Tumor o.n.A." 
* #8631:1 ^property[0].code = #parent 
* #8631:1 ^property[0].valueCode = #859-867 
* #8631:3 "Schlecht differenzierter Sertoli-Leydig-Zell-Tumor"
* #8631:3 ^designation[0].value = "Sarkomatoider Sertoli-Leydig-Zell-Tumor" 
* #8631:3 ^property[0].code = #parent 
* #8631:3 ^property[0].valueCode = #859-867 
* #8632:1 "Gynandroblastom"
* #8632:1 ^property[0].code = #parent 
* #8632:1 ^property[0].valueCode = #859-867 
* #8633:1 "Retiformer Sertoli-Leydig-Zell-Tumor"
* #8633:1 ^property[0].code = #parent 
* #8633:1 ^property[0].valueCode = #859-867 
* #8634:1 "Intermediär differenzierter Sertoli-Leydig-Zell-Tumor mit heterologen Elementen"
* #8634:1 ^designation[0].value = "Mäßig differenzierter Sertoli-Leydig-Zell-Tumor mit heterologen Elementen" 
* #8634:1 ^designation[1].value = "Retiformer Sertoli-Leydig-Zell-Tumor mit heterologen Elementen" 
* #8634:1 ^property[0].code = #parent 
* #8634:1 ^property[0].valueCode = #859-867 
* #8634:3 "Schlecht differenzierter Sertoli-Leydig-Zell-Tumor mit heterologen Elementen"
* #8634:3 ^property[0].code = #parent 
* #8634:3 ^property[0].valueCode = #859-867 
* #8640:1 "Sertoli-Zell-Tumor o.n.A."
* #8640:1 ^designation[0].value = "Hodenadenom" 
* #8640:1 ^designation[1].value = "Sertoli-Zell-Adenom" 
* #8640:1 ^designation[2].value = "Tubuläres Adenom [Pick]" 
* #8640:1 ^designation[3].value = "Tubuläres Androblastom o.n.A." 
* #8640:1 ^property[0].code = #parent 
* #8640:1 ^property[0].valueCode = #859-867 
* #8640:3 "Sertoli-Zell-Karzinom"
* #8640:3 ^property[0].code = #parent 
* #8640:3 ^property[0].valueCode = #859-867 
* #8641:0 "Lipidspeichernder Sertoli-Zell-Tumor"
* #8641:0 ^designation[0].value = "Lipidfollikulom [Lecne]" 
* #8641:0 ^designation[1].value = "Lipidreicher Sertoli-Zell-Tumor" 
* #8641:0 ^designation[2].value = "Tubuläres Androblastom mit Lipidspeicherung" 
* #8641:0 ^property[0].code = #parent 
* #8641:0 ^property[0].valueCode = #859-867 
* #8642:1 "Großzelliger verkalkender Sertoli-Zell-Tumor"
* #8642:1 ^property[0].code = #parent 
* #8642:1 ^property[0].valueCode = #859-867 
* #8643:1 "Intratubuläre großzellige hyalinisierende Sertoli-Zell-Neoplasie"
* #8643:1 ^property[0].code = #parent 
* #8643:1 ^property[0].valueCode = #859-867 
* #8650:0 "Leydig-Zell-Tumor des Ovars o.n.A."
* #8650:0 ^property[0].code = #parent 
* #8650:0 ^property[0].valueCode = #859-867 
* #8650:1 "Leydig-Zell-Tumor der Hoden o.n.A."
* #8650:1 ^designation[0].value = "Zwischenzelltumor o.n.A." 
* #8650:1 ^property[0].code = #parent 
* #8650:1 ^property[0].valueCode = #859-867 
* #8650:3 "Maligner Leydig-Zell-Tumor"
* #8650:3 ^designation[0].value = "Maligner Zwischenzelltumor" 
* #8650:3 ^property[0].code = #parent 
* #8650:3 ^property[0].valueCode = #859-867 
* #8660:0 "Hiluszelltumor"
* #8660:0 ^designation[0].value = "Leydig-Zell-Tumor vom Hilustyp" 
* #8660:0 ^property[0].code = #parent 
* #8660:0 ^property[0].valueCode = #859-867 
* #8670:0 "Lipidzelliger Ovarialtumor"
* #8670:0 ^designation[0].value = "Lipoidzelliger Ovarialtumor" 
* #8670:0 ^designation[1].value = "Steroidzelltumor o.n.A." 
* #8670:0 ^designation[2].value = "Maskulinovoblastom" 
* #8670:0 ^property[0].code = #parent 
* #8670:0 ^property[0].valueCode = #859-867 
* #8670:3 "Maligner Steroidzelltumor"
* #8670:3 ^property[0].code = #parent 
* #8670:3 ^property[0].valueCode = #859-867 
* #8671:0 "Nebennierenresttumor"
* #8671:0 ^property[0].code = #parent 
* #8671:0 ^property[0].valueCode = #859-867 
* #868-871 "Paragangliome und Stromatumoren"
* #868-871 ^property[0].code = #parent 
* #868-871 ^property[0].valueCode = #M 
* #868-871 ^property[1].code = #child 
* #868-871 ^property[1].valueCode = #8680:3 
* #868-871 ^property[2].code = #child 
* #868-871 ^property[2].valueCode = #8681:3 
* #868-871 ^property[3].code = #child 
* #868-871 ^property[3].valueCode = #8682:3 
* #868-871 ^property[4].code = #child 
* #868-871 ^property[4].valueCode = #8683:0 
* #868-871 ^property[5].code = #child 
* #868-871 ^property[5].valueCode = #8690:3 
* #868-871 ^property[6].code = #child 
* #868-871 ^property[6].valueCode = #8691:3 
* #868-871 ^property[7].code = #child 
* #868-871 ^property[7].valueCode = #8692:3 
* #868-871 ^property[8].code = #child 
* #868-871 ^property[8].valueCode = #8693:3 
* #868-871 ^property[9].code = #child 
* #868-871 ^property[9].valueCode = #8700:3 
* #868-871 ^property[10].code = #child 
* #868-871 ^property[10].valueCode = #8710:3 
* #868-871 ^property[11].code = #child 
* #868-871 ^property[11].valueCode = #8711:0 
* #868-871 ^property[12].code = #child 
* #868-871 ^property[12].valueCode = #8711:1 
* #868-871 ^property[13].code = #child 
* #868-871 ^property[13].valueCode = #8711:3 
* #868-871 ^property[14].code = #child 
* #868-871 ^property[14].valueCode = #8712:0 
* #868-871 ^property[15].code = #child 
* #868-871 ^property[15].valueCode = #8713:0 
* #868-871 ^property[16].code = #child 
* #868-871 ^property[16].valueCode = #8714:0 
* #868-871 ^property[17].code = #child 
* #868-871 ^property[17].valueCode = #8714:3 
* #8680:3 "Paragangliom o.n.A."
* #8680:3 ^property[0].code = #parent 
* #8680:3 ^property[0].valueCode = #868-871 
* #8681:3 "Sympathisches Paragangliom"
* #8681:3 ^property[0].code = #parent 
* #8681:3 ^property[0].valueCode = #868-871 
* #8682:3 "Parasympathisches Paragangliom"
* #8682:3 ^property[0].code = #parent 
* #8682:3 ^property[0].valueCode = #868-871 
* #8683:0 "Ganglienzell-Paragangliom"
* #8683:0 ^property[0].code = #parent 
* #8683:0 ^property[0].valueCode = #868-871 
* #8690:3 "Paragangliom des Mittelohrs"
* #8690:3 ^designation[0].value = "Glomus-jugulare-Tumor o.n.A." 
* #8690:3 ^designation[1].value = "Juguläres Paragangliom" 
* #8690:3 ^designation[2].value = "Tympano-juguläres Paragangliom" 
* #8690:3 ^property[0].code = #parent 
* #8690:3 ^property[0].valueCode = #868-871 
* #8691:3 "Glomus-aorticum-Tumor"
* #8691:3 ^designation[0].value = "Aortopulmonales Paragangliom" 
* #8691:3 ^designation[1].value = "Paragangliom des Aortenglomus" 
* #8691:3 ^property[0].code = #parent 
* #8691:3 ^property[0].valueCode = #868-871 
* #8692:3 "Paragangliom des Glomus caroticum"
* #8692:3 ^designation[0].value = "Glomus-caroticum-Tumor" 
* #8692:3 ^property[0].code = #parent 
* #8692:3 ^property[0].valueCode = #868-871 
* #8693:3 "Extraadrenales Paragangliom o.n.A."
* #8693:3 ^designation[0].value = "Chemodektom" 
* #8693:3 ^designation[1].value = "Kombiniertes Paragangliom" 
* #8693:3 ^designation[2].value = "Laryngeales Paragangliom" 
* #8693:3 ^designation[3].value = "Nichtchromaffines Paragangliom o.n.A." 
* #8693:3 ^designation[4].value = "Vagales Paragangliom" 
* #8693:3 ^property[0].code = #parent 
* #8693:3 ^property[0].valueCode = #868-871 
* #8700:3 "Phäochromozytom o.n.A."
* #8700:3 ^designation[0].value = "Chromaffiner Tumor" 
* #8700:3 ^designation[1].value = "Chromaffines Paragangliom" 
* #8700:3 ^designation[2].value = "Chromaffinom" 
* #8700:3 ^designation[3].value = "Nebennierenmarks-Paragangliom" 
* #8700:3 ^designation[4].value = "Kombiniertes PhäochromozytomPhäochromoblastom" 
* #8700:3 ^property[0].code = #parent 
* #8700:3 ^property[0].valueCode = #868-871 
* #8710:3 "Glomangiosarkom"
* #8710:3 ^designation[0].value = "Glomoid-Sarkom" 
* #8710:3 ^property[0].code = #parent 
* #8710:3 ^property[0].valueCode = #868-871 
* #8711:0 "Glomustumor o.n.A."
* #8711:0 ^property[0].code = #parent 
* #8711:0 ^property[0].valueCode = #868-871 
* #8711:1 "Glomangiomatose"
* #8711:1 ^designation[0].value = "Glomustumor mit unsicherem malignem Potential" 
* #8711:1 ^property[0].code = #parent 
* #8711:1 ^property[0].valueCode = #868-871 
* #8711:3 "Maligner Glomustumor"
* #8711:3 ^property[0].code = #parent 
* #8711:3 ^property[0].valueCode = #868-871 
* #8712:0 "Glomangiom"
* #8712:0 ^property[0].code = #parent 
* #8712:0 ^property[0].valueCode = #868-871 
* #8713:0 "Glomangiomyom"
* #8713:0 ^property[0].code = #parent 
* #8713:0 ^property[0].valueCode = #868-871 
* #8714:0 "Benigner perivaskulärer Epitheloidtumor"
* #8714:0 ^designation[0].value = "Benignes PECom" 
* #8714:0 ^property[0].code = #parent 
* #8714:0 ^property[0].valueCode = #868-871 
* #8714:3 "Maligner perivaskulärer Epitheloidtumor"
* #8714:3 ^designation[0].value = "Malignes PECom" 
* #8714:3 ^property[0].code = #parent 
* #8714:3 ^property[0].valueCode = #868-871 
* #872-879 "Nävi und Melanome"
* #872-879 ^property[0].code = #parent 
* #872-879 ^property[0].valueCode = #M 
* #872-879 ^property[1].code = #child 
* #872-879 ^property[1].valueCode = #8720:0 
* #872-879 ^property[2].code = #child 
* #872-879 ^property[2].valueCode = #8720:2 
* #872-879 ^property[3].code = #child 
* #872-879 ^property[3].valueCode = #8720:3 
* #872-879 ^property[4].code = #child 
* #872-879 ^property[4].valueCode = #8721:3 
* #872-879 ^property[5].code = #child 
* #872-879 ^property[5].valueCode = #8722:0 
* #872-879 ^property[6].code = #child 
* #872-879 ^property[6].valueCode = #8722:3 
* #872-879 ^property[7].code = #child 
* #872-879 ^property[7].valueCode = #8723:0 
* #872-879 ^property[8].code = #child 
* #872-879 ^property[8].valueCode = #8723:3 
* #872-879 ^property[9].code = #child 
* #872-879 ^property[9].valueCode = #8725:0 
* #872-879 ^property[10].code = #child 
* #872-879 ^property[10].valueCode = #8726:0 
* #872-879 ^property[11].code = #child 
* #872-879 ^property[11].valueCode = #8727:0 
* #872-879 ^property[12].code = #child 
* #872-879 ^property[12].valueCode = #8728:0 
* #872-879 ^property[13].code = #child 
* #872-879 ^property[13].valueCode = #8728:1 
* #872-879 ^property[14].code = #child 
* #872-879 ^property[14].valueCode = #8728:3 
* #872-879 ^property[15].code = #child 
* #872-879 ^property[15].valueCode = #8730:0 
* #872-879 ^property[16].code = #child 
* #872-879 ^property[16].valueCode = #8730:3 
* #872-879 ^property[17].code = #child 
* #872-879 ^property[17].valueCode = #8740:0 
* #872-879 ^property[18].code = #child 
* #872-879 ^property[18].valueCode = #8740:3 
* #872-879 ^property[19].code = #child 
* #872-879 ^property[19].valueCode = #8741:2 
* #872-879 ^property[20].code = #child 
* #872-879 ^property[20].valueCode = #8741:3 
* #872-879 ^property[21].code = #child 
* #872-879 ^property[21].valueCode = #8742:0 
* #872-879 ^property[22].code = #child 
* #872-879 ^property[22].valueCode = #8742:2 
* #872-879 ^property[23].code = #child 
* #872-879 ^property[23].valueCode = #8742:3 
* #872-879 ^property[24].code = #child 
* #872-879 ^property[24].valueCode = #8743:3 
* #872-879 ^property[25].code = #child 
* #872-879 ^property[25].valueCode = #8744:0 
* #872-879 ^property[26].code = #child 
* #872-879 ^property[26].valueCode = #8744:3 
* #872-879 ^property[27].code = #child 
* #872-879 ^property[27].valueCode = #8745:3 
* #872-879 ^property[28].code = #child 
* #872-879 ^property[28].valueCode = #8746:3 
* #872-879 ^property[29].code = #child 
* #872-879 ^property[29].valueCode = #8750:0 
* #872-879 ^property[30].code = #child 
* #872-879 ^property[30].valueCode = #8760:0 
* #872-879 ^property[31].code = #child 
* #872-879 ^property[31].valueCode = #8761:0 
* #872-879 ^property[32].code = #child 
* #872-879 ^property[32].valueCode = #8761:1 
* #872-879 ^property[33].code = #child 
* #872-879 ^property[33].valueCode = #8761:3 
* #872-879 ^property[34].code = #child 
* #872-879 ^property[34].valueCode = #8762:1 
* #872-879 ^property[35].code = #child 
* #872-879 ^property[35].valueCode = #8770:0 
* #872-879 ^property[36].code = #child 
* #872-879 ^property[36].valueCode = #8770:3 
* #872-879 ^property[37].code = #child 
* #872-879 ^property[37].valueCode = #8771:0 
* #872-879 ^property[38].code = #child 
* #872-879 ^property[38].valueCode = #8771:3 
* #872-879 ^property[39].code = #child 
* #872-879 ^property[39].valueCode = #8772:0 
* #872-879 ^property[40].code = #child 
* #872-879 ^property[40].valueCode = #8772:3 
* #872-879 ^property[41].code = #child 
* #872-879 ^property[41].valueCode = #8773:3 
* #872-879 ^property[42].code = #child 
* #872-879 ^property[42].valueCode = #8774:3 
* #872-879 ^property[43].code = #child 
* #872-879 ^property[43].valueCode = #8780:0 
* #872-879 ^property[44].code = #child 
* #872-879 ^property[44].valueCode = #8780:1 
* #872-879 ^property[45].code = #child 
* #872-879 ^property[45].valueCode = #8780:3 
* #872-879 ^property[46].code = #child 
* #872-879 ^property[46].valueCode = #8790:0 
* #8720:0 "Pigmentierter Nävus o.n.A."
* #8720:0 ^designation[0].value = "Melanozytennävus o.n.A." 
* #8720:0 ^designation[1].value = "Nävus o.n.A." 
* #8720:0 ^designation[2].value = "Genitaler Nävus" 
* #8720:0 ^designation[3].value = "Haarnävus" 
* #8720:0 ^designation[4].value = "Kombinierter Nävus" 
* #8720:0 ^designation[5].value = "Konjunktivaler Nävus" 
* #8720:0 ^designation[6].value = "Meyerson-Nävus" 
* #8720:0 ^designation[7].value = "Naevus spilus" 
* #8720:0 ^designation[8].value = "Tief infiltrierender Nävus" 
* #8720:0 ^property[0].code = #parent 
* #8720:0 ^property[0].valueCode = #872-879 
* #8720:2 "Melanoma in situ"
* #8720:2 ^property[0].code = #parent 
* #8720:2 ^property[0].valueCode = #872-879 
* #8720:3 "Malignes Melanom o.n.A."
* #8720:3 ^designation[0].value = "Melanom o.n.A" 
* #8720:3 ^designation[1].value = "Meningeales Melanom" 
* #8720:3 ^designation[2].value = "Nävoides Melanom" 
* #8720:3 ^property[0].code = #parent 
* #8720:3 ^property[0].valueCode = #872-879 
* #8721:3 "Noduläres malignes Melanom (NM)"
* #8721:3 ^property[0].code = #parent 
* #8721:3 ^property[0].valueCode = #872-879 
* #8722:0 "Ballonzellnävus"
* #8722:0 ^property[0].code = #parent 
* #8722:0 ^property[0].valueCode = #872-879 
* #8722:3 "Ballonzellmelanom"
* #8722:3 ^property[0].code = #parent 
* #8722:3 ^property[0].valueCode = #872-879 
* #8723:0 "Halonävus"
* #8723:0 ^designation[0].value = "Regressiver Nävus" 
* #8723:0 ^property[0].code = #parent 
* #8723:0 ^property[0].valueCode = #872-879 
* #8723:3 "Malignes Melanom in Regression"
* #8723:3 ^property[0].code = #parent 
* #8723:3 ^property[0].valueCode = #872-879 
* #8725:0 "Neuronävus"
* #8725:0 ^property[0].code = #parent 
* #8725:0 ^property[0].valueCode = #872-879 
* #8726:0 "Großzelliger Nävus"
* #8726:0 ^designation[0].value = "Melanozytom des Augapfels" 
* #8726:0 ^designation[1].value = "Melanozytom o.n.A." 
* #8726:0 ^property[0].code = #parent 
* #8726:0 ^property[0].valueCode = #872-879 
* #8727:0 "Dysplastischer Nävus"
* #8727:0 ^property[0].code = #parent 
* #8727:0 ^property[0].valueCode = #872-879 
* #8728:0 "Meningeale Melanozytose"
* #8728:0 ^designation[0].value = "Diffuse Melanozytose" 
* #8728:0 ^property[0].code = #parent 
* #8728:0 ^property[0].valueCode = #872-879 
* #8728:1 "Meningeales Melanozytom"
* #8728:1 ^property[0].code = #parent 
* #8728:1 ^property[0].valueCode = #872-879 
* #8728:3 "Meningeale Melanomatose"
* #8728:3 ^property[0].code = #parent 
* #8728:3 ^property[0].valueCode = #872-879 
* #8730:0 "Nichtpigmentierter Nävus"
* #8730:0 ^designation[0].value = "Achromer Nävus" 
* #8730:0 ^property[0].code = #parent 
* #8730:0 ^property[0].valueCode = #872-879 
* #8730:3 "Amelanotisches malignes Melanom"
* #8730:3 ^property[0].code = #parent 
* #8730:3 ^property[0].valueCode = #872-879 
* #8740:0 "Junktionaler Nävus o.n.A."
* #8740:0 ^designation[0].value = "Intraepidermaler Nävus" 
* #8740:0 ^designation[1].value = "Junktions-Nävus" 
* #8740:0 ^property[0].code = #parent 
* #8740:0 ^property[0].valueCode = #872-879 
* #8740:3 "Malignes Melanom in Junktions-Nävus"
* #8740:3 ^property[0].code = #parent 
* #8740:3 ^property[0].valueCode = #872-879 
* #8741:2 "Prämaligne Melanose o.n.A."
* #8741:2 ^property[0].code = #parent 
* #8741:2 ^property[0].valueCode = #872-879 
* #8741:3 "Malignes Melanom in prämaligner Melanose"
* #8741:3 ^property[0].code = #parent 
* #8741:3 ^property[0].valueCode = #872-879 
* #8742:0 "Lentiginöser melanozytischer Nävus"
* #8742:0 ^designation[0].value = "Einfache LentigoLentigo simplex" 
* #8742:0 ^property[0].code = #parent 
* #8742:0 ^property[0].valueCode = #872-879 
* #8742:2 "Lentigo maligna"
* #8742:2 ^designation[0].value = "Hutchinson-Pigmentfleck o.n.A." 
* #8742:2 ^property[0].code = #parent 
* #8742:2 ^property[0].valueCode = #872-879 
* #8742:3 "Lentigo-maligna-Melanom"
* #8742:3 ^designation[0].value = "LMM" 
* #8742:3 ^designation[1].value = "Malignes Melanom in Hutchinson-Melanose" 
* #8742:3 ^property[0].code = #parent 
* #8742:3 ^property[0].valueCode = #872-879 
* #8743:3 "Melanom nach geringem kumulativem Sonnenschaden"
* #8743:3 ^designation[0].value = "Oberflächlich spreitendes MelanomSuperficial spreading melanomaSSM" 
* #8743:3 ^property[0].code = #parent 
* #8743:3 ^property[0].valueCode = #872-879 
* #8744:0 "Akraler Nävus"
* #8744:0 ^property[0].code = #parent 
* #8744:0 ^property[0].valueCode = #872-879 
* #8744:3 "Akrales Melanom"
* #8744:3 ^designation[0].value = "Akral-lentiginöses malignes Melanom" 
* #8744:3 ^property[0].code = #parent 
* #8744:3 ^property[0].valueCode = #872-879 
* #8745:3 "Desmoplastisches Melanom o.n.A."
* #8745:3 ^designation[0].value = "Desmoplastisches amelanotisches Melanom" 
* #8745:3 ^designation[1].value = "Neurotropes malignes Melanom" 
* #8745:3 ^property[0].code = #parent 
* #8745:3 ^property[0].valueCode = #872-879 
* #8746:3 "Mukosal-lentiginöses Melanom"
* #8746:3 ^property[0].code = #parent 
* #8746:3 ^property[0].valueCode = #872-879 
* #8750:0 "Dermaler Nävus"
* #8750:0 ^designation[0].value = "Intradermaler Nävus" 
* #8750:0 ^designation[1].value = "Stromaler Nävus" 
* #8750:0 ^property[0].code = #parent 
* #8750:0 ^property[0].valueCode = #872-879 
* #8760:0 "Compound-Nävus"
* #8760:0 ^designation[0].value = "Dermaler und epidermaler Nävus" 
* #8760:0 ^property[0].code = #parent 
* #8760:0 ^property[0].valueCode = #872-879 
* #8761:0 "Kongenitaler melanozytischer Nävus o.n.A."
* #8761:0 ^property[0].code = #parent 
* #8761:0 ^property[0].valueCode = #872-879 
* #8761:1 "Pigmentierter Riesennävus o.n.A."
* #8761:1 ^designation[0].value = "Mittelgroßer- und Riesennävus" 
* #8761:1 ^property[0].code = #parent 
* #8761:1 ^property[0].valueCode = #872-879 
* #8761:3 "Malignes Melanom in kongenitalem Melanozytennävus"
* #8761:3 ^designation[0].value = "Malignes Melanom in pigmentiertem Riesennävus" 
* #8761:3 ^property[0].code = #parent 
* #8761:3 ^property[0].valueCode = #872-879 
* #8762:1 "Proliferative dermale Läsion in kongenitalem Nävus"
* #8762:1 ^designation[0].value = "Proliferativer Knoten in kongenitalem Nävus" 
* #8762:1 ^property[0].code = #parent 
* #8762:1 ^property[0].valueCode = #872-879 
* #8770:0 "Epitheloid- und Spindelzellnävus"
* #8770:0 ^designation[0].value = "Juveniler Nävus" 
* #8770:0 ^designation[1].value = "Juveniles Melanom" 
* #8770:0 ^designation[2].value = "Spitz-Nävus o.n.A." 
* #8770:0 ^designation[3].value = "Atypischer Spitz-Nävus" 
* #8770:0 ^designation[4].value = "Pigmentierter Spindelzellnävus (Reed)Pigmentierter Spindelzell-Spitz-Nävus" 
* #8770:0 ^property[0].code = #parent 
* #8770:0 ^property[0].valueCode = #872-879 
* #8770:3 "Maligner Spitz-Tumor"
* #8770:3 ^designation[0].value = "Gemischtes Epitheloid- und SpindelzellmelanomSpitz-Melanom" 
* #8770:3 ^designation[1].value = "Gemischtes Epitheloid- und SpindelzellmelanomSpitzoides Melanom" 
* #8770:3 ^property[0].code = #parent 
* #8770:3 ^property[0].valueCode = #872-879 
* #8771:0 "Epitheloidzellnävus"
* #8771:0 ^property[0].code = #parent 
* #8771:0 ^property[0].valueCode = #872-879 
* #8771:3 "Epitheloidzellmelanom"
* #8771:3 ^property[0].code = #parent 
* #8771:3 ^property[0].valueCode = #872-879 
* #8772:0 "Spindelzellnävus o.n.A."
* #8772:0 ^property[0].code = #parent 
* #8772:0 ^property[0].valueCode = #872-879 
* #8772:3 "Spindelzellmelanom o.n.A."
* #8772:3 ^property[0].code = #parent 
* #8772:3 ^property[0].valueCode = #872-879 
* #8773:3 "Spindelzellmelanom Typ A"
* #8773:3 ^property[0].code = #parent 
* #8773:3 ^property[0].valueCode = #872-879 
* #8774:3 "Spindelzellmelanom Typ B"
* #8774:3 ^property[0].code = #parent 
* #8774:3 ^property[0].valueCode = #872-879 
* #8780:0 "Blauer Nävus o.n.A."
* #8780:0 ^designation[0].value = "Blauer Nävus (Jadassohn)" 
* #8780:0 ^property[0].code = #parent 
* #8780:0 ^property[0].valueCode = #872-879 
* #8780:1 "Pigmentiertes epitheloides Melanozytom"
* #8780:1 ^designation[0].value = "Epitheloider blauer Nävus" 
* #8780:1 ^property[0].code = #parent 
* #8780:1 ^property[0].valueCode = #872-879 
* #8780:3 "Maligner blauer Nävus"
* #8780:3 ^designation[0].value = "Melanom in blauem Nävus" 
* #8780:3 ^property[0].code = #parent 
* #8780:3 ^property[0].valueCode = #872-879 
* #8790:0 "Zellreicher blauer Nävus"
* #8790:0 ^property[0].code = #parent 
* #8790:0 ^property[0].valueCode = #872-879 
* #880-880 "Weichteiltumoren und Sarkome o.n.A."
* #880-880 ^property[0].code = #parent 
* #880-880 ^property[0].valueCode = #M 
* #880-880 ^property[1].code = #child 
* #880-880 ^property[1].valueCode = #8800:0 
* #880-880 ^property[2].code = #child 
* #880-880 ^property[2].valueCode = #8800:3 
* #880-880 ^property[3].code = #child 
* #880-880 ^property[3].valueCode = #8800:9 
* #880-880 ^property[4].code = #child 
* #880-880 ^property[4].valueCode = #8801:3 
* #880-880 ^property[5].code = #child 
* #880-880 ^property[5].valueCode = #8802:1 
* #880-880 ^property[6].code = #child 
* #880-880 ^property[6].valueCode = #8802:3 
* #880-880 ^property[7].code = #child 
* #880-880 ^property[7].valueCode = #8803:3 
* #880-880 ^property[8].code = #child 
* #880-880 ^property[8].valueCode = #8804:3 
* #880-880 ^property[9].code = #child 
* #880-880 ^property[9].valueCode = #8805:3 
* #880-880 ^property[10].code = #child 
* #880-880 ^property[10].valueCode = #8806:3 
* #8800:0 "Benigner Weichteiltumor"
* #8800:0 ^property[0].code = #parent 
* #8800:0 ^property[0].valueCode = #880-880 
* #8800:3 "Sarkom o.n.A."
* #8800:3 ^designation[0].value = "Maligner Weichteiltumor" 
* #8800:3 ^designation[1].value = "Maligner mesenchymaler Tumor" 
* #8800:3 ^designation[2].value = "Weichteilsarkom" 
* #8800:3 ^property[0].code = #parent 
* #8800:3 ^property[0].valueCode = #880-880 
* #8800:9 "Sarkomatose o.n.A."
* #8800:9 ^property[0].code = #parent 
* #8800:9 ^property[0].valueCode = #880-880 
* #8801:3 "Spindelzellsarkom"
* #8801:3 ^designation[0].value = "Undifferenziertes Spindelzellsarkom" 
* #8801:3 ^property[0].code = #parent 
* #8801:3 ^property[0].valueCode = #880-880 
* #8802:1 "Pleomorpher hyalinisierender angiektatischer Tumor"
* #8802:1 ^property[0].code = #parent 
* #8802:1 ^property[0].valueCode = #880-880 
* #8802:3 "Riesenzellsarkom"
* #8802:3 ^designation[0].value = "Pleomorphes Sarkom" 
* #8802:3 ^designation[1].value = "Pleomorphzelliges Sarkom" 
* #8802:3 ^designation[2].value = "Undifferenziertes pleomorphzelliges Sarkom" 
* #8802:3 ^designation[3].value = "Pleomorphes dermales Sarkom" 
* #8802:3 ^property[0].code = #parent 
* #8802:3 ^property[0].valueCode = #880-880 
* #8803:3 "Kleinzelliges Sarkom"
* #8803:3 ^designation[0].value = "Rundzellsarkom" 
* #8803:3 ^designation[1].value = "Undifferenziertes Rundzellsarkom" 
* #8803:3 ^property[0].code = #parent 
* #8803:3 ^property[0].valueCode = #880-880 
* #8804:3 "Epitheloidsarkom"
* #8804:3 ^designation[0].value = "Epitheloidzelliges Sarkom" 
* #8804:3 ^designation[1].value = "Undifferenziertes epitheloidzelliges Sarkom" 
* #8804:3 ^property[0].code = #parent 
* #8804:3 ^property[0].valueCode = #880-880 
* #8805:3 "Undifferenziertes Sarkom"
* #8805:3 ^property[0].code = #parent 
* #8805:3 ^property[0].valueCode = #880-880 
* #8806:3 "Desmoplastischer kleinzelliger Tumor"
* #8806:3 ^property[0].code = #parent 
* #8806:3 ^property[0].valueCode = #880-880 
* #881-883 "Fibromatöse Neoplasien"
* #881-883 ^property[0].code = #parent 
* #881-883 ^property[0].valueCode = #M 
* #881-883 ^property[1].code = #child 
* #881-883 ^property[1].valueCode = #8810:0 
* #881-883 ^property[2].code = #child 
* #881-883 ^property[2].valueCode = #8810:1 
* #881-883 ^property[3].code = #child 
* #881-883 ^property[3].valueCode = #8810:3 
* #881-883 ^property[4].code = #child 
* #881-883 ^property[4].valueCode = #8811:0 
* #881-883 ^property[5].code = #child 
* #881-883 ^property[5].valueCode = #8811:1 
* #881-883 ^property[6].code = #child 
* #881-883 ^property[6].valueCode = #8811:3 
* #881-883 ^property[7].code = #child 
* #881-883 ^property[7].valueCode = #8812:0 
* #881-883 ^property[8].code = #child 
* #881-883 ^property[8].valueCode = #8812:3 
* #881-883 ^property[9].code = #child 
* #881-883 ^property[9].valueCode = #8813:0 
* #881-883 ^property[10].code = #child 
* #881-883 ^property[10].valueCode = #8813:1 
* #881-883 ^property[11].code = #child 
* #881-883 ^property[11].valueCode = #8813:3 
* #881-883 ^property[12].code = #child 
* #881-883 ^property[12].valueCode = #8814:3 
* #881-883 ^property[13].code = #child 
* #881-883 ^property[13].valueCode = #8815:0 
* #881-883 ^property[14].code = #child 
* #881-883 ^property[14].valueCode = #8815:1 
* #881-883 ^property[15].code = #child 
* #881-883 ^property[15].valueCode = #8815:3 
* #881-883 ^property[16].code = #child 
* #881-883 ^property[16].valueCode = #8816:0 
* #881-883 ^property[17].code = #child 
* #881-883 ^property[17].valueCode = #8817:0 
* #881-883 ^property[18].code = #child 
* #881-883 ^property[18].valueCode = #8818:0 
* #881-883 ^property[19].code = #child 
* #881-883 ^property[19].valueCode = #8820:0 
* #881-883 ^property[20].code = #child 
* #881-883 ^property[20].valueCode = #8821:1 
* #881-883 ^property[21].code = #child 
* #881-883 ^property[21].valueCode = #8822:1 
* #881-883 ^property[22].code = #child 
* #881-883 ^property[22].valueCode = #8823:0 
* #881-883 ^property[23].code = #child 
* #881-883 ^property[23].valueCode = #8823:1 
* #881-883 ^property[24].code = #child 
* #881-883 ^property[24].valueCode = #8824:0 
* #881-883 ^property[25].code = #child 
* #881-883 ^property[25].valueCode = #8824:1 
* #881-883 ^property[26].code = #child 
* #881-883 ^property[26].valueCode = #8825:0 
* #881-883 ^property[27].code = #child 
* #881-883 ^property[27].valueCode = #8825:1 
* #881-883 ^property[28].code = #child 
* #881-883 ^property[28].valueCode = #8825:3 
* #881-883 ^property[29].code = #child 
* #881-883 ^property[29].valueCode = #8826:0 
* #881-883 ^property[30].code = #child 
* #881-883 ^property[30].valueCode = #8827:1 
* #881-883 ^property[31].code = #child 
* #881-883 ^property[31].valueCode = #8828:0 
* #881-883 ^property[32].code = #child 
* #881-883 ^property[32].valueCode = #8830:0 
* #881-883 ^property[33].code = #child 
* #881-883 ^property[33].valueCode = #8830:1 
* #881-883 ^property[34].code = #child 
* #881-883 ^property[34].valueCode = #8830:3 
* #881-883 ^property[35].code = #child 
* #881-883 ^property[35].valueCode = #8831:0 
* #881-883 ^property[36].code = #child 
* #881-883 ^property[36].valueCode = #8832:0 
* #881-883 ^property[37].code = #child 
* #881-883 ^property[37].valueCode = #8832:1 
* #881-883 ^property[38].code = #child 
* #881-883 ^property[38].valueCode = #8832:3 
* #881-883 ^property[39].code = #child 
* #881-883 ^property[39].valueCode = #8833:1 
* #881-883 ^property[40].code = #child 
* #881-883 ^property[40].valueCode = #8834:1 
* #881-883 ^property[41].code = #child 
* #881-883 ^property[41].valueCode = #8835:1 
* #881-883 ^property[42].code = #child 
* #881-883 ^property[42].valueCode = #8836:1 
* #8810:0 "Fibrom o.n.A."
* #8810:0 ^designation[0].value = "Desmoplastiches Fibroblastom" 
* #8810:0 ^designation[1].value = "Gardner-Fibrom" 
* #8810:0 ^designation[2].value = "Kollagenöses Fibrom" 
* #8810:0 ^designation[3].value = "Nuchales Fibrom" 
* #8810:0 ^designation[4].value = "Plack-ähnliches CD34 positives dermales Fibrom" 
* #8810:0 ^property[0].code = #parent 
* #8810:0 ^property[0].valueCode = #881-883 
* #8810:1 "Zellreiches Fibrom"
* #8810:1 ^property[0].code = #parent 
* #8810:1 ^property[0].valueCode = #881-883 
* #8810:3 "Fibrosarkom o.n.A."
* #8810:3 ^property[0].code = #parent 
* #8810:3 ^property[0].valueCode = #881-883 
* #8811:0 "Fibromyxom o.n.A."
* #8811:0 ^designation[0].value = "Myxofibrom o.n.A." 
* #8811:0 ^designation[1].value = "Myxoides Fibrom" 
* #8811:0 ^designation[2].value = "Akrales Fibromyxom" 
* #8811:0 ^designation[3].value = "Plexiformes Fibromyxom" 
* #8811:0 ^property[0].code = #parent 
* #8811:0 ^property[0].valueCode = #881-883 
* #8811:1 "Myxoinflammatorisches fibroblastisches Sarkom"
* #8811:1 ^designation[0].value = "Atypischer myxoinflammatorischer fibroblastischer Tumor" 
* #8811:1 ^designation[1].value = "Hämosiderotischer fibrolipomatöser Tumor" 
* #8811:1 ^property[0].code = #parent 
* #8811:1 ^property[0].valueCode = #881-883 
* #8811:3 "Myxofibrosarkom"
* #8811:3 ^designation[0].value = "Fibromyxosarkom" 
* #8811:3 ^property[0].code = #parent 
* #8811:3 ^property[0].valueCode = #881-883 
* #8812:0 "Periostales Fibrom"
* #8812:0 ^property[0].code = #parent 
* #8812:0 ^property[0].valueCode = #881-883 
* #8812:3 "Periostales Fibrosarkom"
* #8812:3 ^designation[0].value = "Periostales Sarkom o.n.A." 
* #8812:3 ^property[0].code = #parent 
* #8812:3 ^property[0].valueCode = #881-883 
* #8813:0 "Fibrom der Sehnenscheide"
* #8813:0 ^designation[0].value = "Faszienfibrom" 
* #8813:0 ^property[0].code = #parent 
* #8813:0 ^property[0].valueCode = #881-883 
* #8813:1 "Fibromatose vom palmaren/plantaren Typ"
* #8813:1 ^designation[0].value = "Oberflächliche Fibromatose" 
* #8813:1 ^property[0].code = #parent 
* #8813:1 ^property[0].valueCode = #881-883 
* #8813:3 "Faszienfibrosarkom"
* #8813:3 ^property[0].code = #parent 
* #8813:3 ^property[0].valueCode = #881-883 
* #8814:3 "Infantiles Fibrosarkom"
* #8814:3 ^designation[0].value = "Kongenitales Fibrosarkom" 
* #8814:3 ^property[0].code = #parent 
* #8814:3 ^property[0].valueCode = #881-883 
* #8815:0 "Solitärer fibröser Tumor/Hämangioperizytom, Grad 1"
* #8815:0 ^designation[0].value = "Benignes Hämangioperizytom" 
* #8815:0 ^property[0].code = #parent 
* #8815:0 ^property[0].valueCode = #881-883 
* #8815:1 "Solitärer fibröser Tumor o.n.A."
* #8815:1 ^designation[0].value = "Hämangioperizytom o.n.A." 
* #8815:1 ^designation[1].value = "Lokalisierter fibröser Tumor" 
* #8815:1 ^designation[2].value = "Hämangioperizytisches Meningeom" 
* #8815:1 ^designation[3].value = "Solitärer fibröser Tumor/Hämangioperizytom, Grad 2" 
* #8815:1 ^property[0].code = #parent 
* #8815:1 ^property[0].valueCode = #881-883 
* #8815:3 "Maligner solitärer fibröser Tumor"
* #8815:3 ^designation[0].value = "Solitärer fibröser Tumor/Hämangioperizytom, Grad 3" 
* #8815:3 ^designation[1].value = "Malignes Hämangioperizytom" 
* #8815:3 ^property[0].code = #parent 
* #8815:3 ^property[0].valueCode = #881-883 
* #8816:0 "Kalzifizierendes aponeurotisches Fibrom"
* #8816:0 ^property[0].code = #parent 
* #8816:0 ^property[0].valueCode = #881-883 
* #8817:0 "Kalzifizierender fibröser Tumor"
* #8817:0 ^property[0].code = #parent 
* #8817:0 ^property[0].valueCode = #881-883 
* #8818:0 "Fibröse Dysplasie"
* #8818:0 ^property[0].code = #parent 
* #8818:0 ^property[0].valueCode = #881-883 
* #8820:0 "Elastofibrom"
* #8820:0 ^property[0].code = #parent 
* #8820:0 ^property[0].valueCode = #881-883 
* #8821:1 "Aggressive Fibromatose"
* #8821:1 ^designation[0].value = "Desmoid o.n.A." 
* #8821:1 ^designation[1].value = "Desmoidfibromatose" 
* #8821:1 ^designation[2].value = "Desmoidtumor o.n.A." 
* #8821:1 ^designation[3].value = "Extraabdominaler Desmoidtumor" 
* #8821:1 ^designation[4].value = "Invasives Fibrom" 
* #8821:1 ^property[0].code = #parent 
* #8821:1 ^property[0].valueCode = #881-883 
* #8822:1 "Abdominale Fibromatose"
* #8822:1 ^designation[0].value = "Abdominaler Desmoidtumor" 
* #8822:1 ^designation[1].value = "Mesenteriale Fibromatose" 
* #8822:1 ^designation[2].value = "Retroperitoneale Fibromatose" 
* #8822:1 ^property[0].code = #parent 
* #8822:1 ^property[0].valueCode = #881-883 
* #8823:0 "Sklerotisches Fibrom"
* #8823:0 ^property[0].code = #parent 
* #8823:0 ^property[0].valueCode = #881-883 
* #8823:1 "Desmoplastisches Fibrom"
* #8823:1 ^property[0].code = #parent 
* #8823:1 ^property[0].valueCode = #881-883 
* #8824:0 "Myofibrom"
* #8824:0 ^designation[0].value = "Dermatomyofibrom" 
* #8824:0 ^designation[1].value = "Myoperizytom" 
* #8824:0 ^property[0].code = #parent 
* #8824:0 ^property[0].valueCode = #881-883 
* #8824:1 "Myofibromatose"
* #8824:1 ^designation[0].value = "Kongenitale generalisierte FibromatoseInfantile Myofibromatose" 
* #8824:1 ^property[0].code = #parent 
* #8824:1 ^property[0].valueCode = #881-883 
* #8825:0 "Myofibroblastom"
* #8825:0 ^property[0].code = #parent 
* #8825:0 ^property[0].valueCode = #881-883 
* #8825:1 "Myofibroblastentumor o.n.A."
* #8825:1 ^designation[0].value = "Entzündlicher Myofibroblastentumor" 
* #8825:1 ^property[0].code = #parent 
* #8825:1 ^property[0].valueCode = #881-883 
* #8825:3 "Myofibroblastisches Sarkom"
* #8825:3 ^property[0].code = #parent 
* #8825:3 ^property[0].valueCode = #881-883 
* #8826:0 "Angiomyofibroblastom"
* #8826:0 ^property[0].code = #parent 
* #8826:0 ^property[0].valueCode = #881-883 
* #8827:1 "Peribronchialer Myofibroblastentumor"
* #8827:1 ^designation[0].value = "Kongenitaler peribronchialer Myofibroblastentumor" 
* #8827:1 ^property[0].code = #parent 
* #8827:1 ^property[0].valueCode = #881-883 
* #8828:0 "Noduläre Fasziitis"
* #8828:0 ^designation[0].value = "Proliferative Fasziitis" 
* #8828:0 ^designation[1].value = "Proliferative Myositis" 
* #8828:0 ^property[0].code = #parent 
* #8828:0 ^property[0].valueCode = #881-883 
* #8830:0 "Fibröses Histiozytom"
* #8830:0 ^designation[0].value = "Fibröses Histiozytom o.n.A." 
* #8830:0 ^designation[1].value = "Fibroxanthom o.n.A." 
* #8830:0 ^designation[2].value = "Xanthofibrom" 
* #8830:0 ^designation[3].value = "Epitheloides fibröses Histiozytom" 
* #8830:0 ^property[0].code = #parent 
* #8830:0 ^property[0].valueCode = #881-883 
* #8830:1 "Atypisches fibröses Histiozytom"
* #8830:1 ^designation[0].value = "Atypisches Fibroxanthom" 
* #8830:1 ^property[0].code = #parent 
* #8830:1 ^property[0].valueCode = #881-883 
* #8830:3 "Malignes fibröses Histiozytom"
* #8830:3 ^designation[0].value = "Malignes Fibroxanthom" 
* #8830:3 ^designation[1].value = "Undifferenziertes hochmalignes [high-grade] pleomorphes Osteosarkom" 
* #8830:3 ^property[0].code = #parent 
* #8830:3 ^property[0].valueCode = #881-883 
* #8831:0 "Histiozytom o.n.A."
* #8831:0 ^designation[0].value = "Juveniles Histiozytom" 
* #8831:0 ^designation[1].value = "RetikulohistiozytomRetikulohistiozytose" 
* #8831:0 ^designation[2].value = "Tiefes HistiozytomBenignes tiefes fibröses Histiozytom" 
* #8831:0 ^property[0].code = #parent 
* #8831:0 ^property[0].valueCode = #881-883 
* #8832:0 "Dermatofibrom o.n.A."
* #8832:0 ^designation[0].value = "Dermatofibroma lenticulare" 
* #8832:0 ^designation[1].value = "Kutanes Histiozytom o.n.A." 
* #8832:0 ^designation[2].value = "Noduläre Unterhautfibrose" 
* #8832:0 ^designation[3].value = "Sklerosierendes Hämangiom" 
* #8832:0 ^designation[4].value = "Pleomorphes Fibrom" 
* #8832:0 ^designation[5].value = "Sklerosierendes Pneumozytom" 
* #8832:0 ^property[0].code = #parent 
* #8832:0 ^property[0].valueCode = #881-883 
* #8832:1 "Dermatofibrosarcoma protuberans o.n.A."
* #8832:1 ^designation[0].value = "Dermatofibrosarkom o.n.A." 
* #8832:1 ^property[0].code = #parent 
* #8832:1 ^property[0].valueCode = #881-883 
* #8832:3 "Fibrosarkomatöses Dermatofibrosarcoma protuberans"
* #8832:3 ^designation[0].value = "Sarkomatöses Dermatofibrosarkom" 
* #8832:3 ^property[0].code = #parent 
* #8832:3 ^property[0].valueCode = #881-883 
* #8833:1 "Pigmentiertes Dermatofibrosarcoma protuberans"
* #8833:1 ^designation[0].value = "Bednar-Tumor" 
* #8833:1 ^property[0].code = #parent 
* #8833:1 ^property[0].valueCode = #881-883 
* #8834:1 "Riesenzellfibroblastom"
* #8834:1 ^property[0].code = #parent 
* #8834:1 ^property[0].valueCode = #881-883 
* #8835:1 "Plexiformer fibrohistiozytärer Tumor"
* #8835:1 ^property[0].code = #parent 
* #8835:1 ^property[0].valueCode = #881-883 
* #8836:1 "Angiomatoides fibröses Histiozytom"
* #8836:1 ^property[0].code = #parent 
* #8836:1 ^property[0].valueCode = #881-883 
* #884-884 "Myxomatöse Neoplasien"
* #884-884 ^property[0].code = #parent 
* #884-884 ^property[0].valueCode = #M 
* #884-884 ^property[1].code = #child 
* #884-884 ^property[1].valueCode = #8840:0 
* #884-884 ^property[2].code = #child 
* #884-884 ^property[2].valueCode = #8840:3 
* #884-884 ^property[3].code = #child 
* #884-884 ^property[3].valueCode = #8841:0 
* #884-884 ^property[4].code = #child 
* #884-884 ^property[4].valueCode = #8842:0 
* #884-884 ^property[5].code = #child 
* #884-884 ^property[5].valueCode = #8842:3 
* #8840:0 "Myxom o.n.A."
* #8840:0 ^property[0].code = #parent 
* #8840:0 ^property[0].valueCode = #884-884 
* #8840:3 "Myxosarkom"
* #8840:3 ^designation[0].value = "Niedriggradiges fibromyxoides Sarkom" 
* #8840:3 ^designation[1].value = "Sklerosierendes epitheloides Fibrosarkom" 
* #8840:3 ^property[0].code = #parent 
* #8840:3 ^property[0].valueCode = #884-884 
* #8841:0 "Angiomyxom o.n.A."
* #8841:0 ^designation[0].value = "Aggressives Angiomyxom" 
* #8841:0 ^designation[1].value = "Oberflächliches Angiomyxom" 
* #8841:0 ^property[0].code = #parent 
* #8841:0 ^property[0].valueCode = #884-884 
* #8842:0 "Ossifizierender fibromyxoider Weichteiltumor o.n.A."
* #8842:0 ^property[0].code = #parent 
* #8842:0 ^property[0].valueCode = #884-884 
* #8842:3 "Maligner ossifizierender fibromyxoider Weichteiltumor"
* #8842:3 ^designation[0].value = "Pulmonales myxoides Sarkom mit EWSR1-CREB1-Translokation" 
* #8842:3 ^property[0].code = #parent 
* #8842:3 ^property[0].valueCode = #884-884 
* #885-888 "Lipomatöse Neoplasien"
* #885-888 ^property[0].code = #parent 
* #885-888 ^property[0].valueCode = #M 
* #885-888 ^property[1].code = #child 
* #885-888 ^property[1].valueCode = #8850:0 
* #885-888 ^property[2].code = #child 
* #885-888 ^property[2].valueCode = #8850:1 
* #885-888 ^property[3].code = #child 
* #885-888 ^property[3].valueCode = #8850:3 
* #885-888 ^property[4].code = #child 
* #885-888 ^property[4].valueCode = #8851:0 
* #885-888 ^property[5].code = #child 
* #885-888 ^property[5].valueCode = #8851:1 
* #885-888 ^property[6].code = #child 
* #885-888 ^property[6].valueCode = #8851:3 
* #885-888 ^property[7].code = #child 
* #885-888 ^property[7].valueCode = #8852:0 
* #885-888 ^property[8].code = #child 
* #885-888 ^property[8].valueCode = #8852:3 
* #885-888 ^property[9].code = #child 
* #885-888 ^property[9].valueCode = #8853:3 
* #885-888 ^property[10].code = #child 
* #885-888 ^property[10].valueCode = #8854:0 
* #885-888 ^property[11].code = #child 
* #885-888 ^property[11].valueCode = #8854:3 
* #885-888 ^property[12].code = #child 
* #885-888 ^property[12].valueCode = #8855:3 
* #885-888 ^property[13].code = #child 
* #885-888 ^property[13].valueCode = #8856:0 
* #885-888 ^property[14].code = #child 
* #885-888 ^property[14].valueCode = #8857:0 
* #885-888 ^property[15].code = #child 
* #885-888 ^property[15].valueCode = #8857:3 
* #885-888 ^property[16].code = #child 
* #885-888 ^property[16].valueCode = #8858:3 
* #885-888 ^property[17].code = #child 
* #885-888 ^property[17].valueCode = #8860:0 
* #885-888 ^property[18].code = #child 
* #885-888 ^property[18].valueCode = #8860:1 
* #885-888 ^property[19].code = #child 
* #885-888 ^property[19].valueCode = #8861:0 
* #885-888 ^property[20].code = #child 
* #885-888 ^property[20].valueCode = #8862:0 
* #885-888 ^property[21].code = #child 
* #885-888 ^property[21].valueCode = #8870:0 
* #885-888 ^property[22].code = #child 
* #885-888 ^property[22].valueCode = #8880:0 
* #885-888 ^property[23].code = #child 
* #885-888 ^property[23].valueCode = #8881:0 
* #8850:0 "Lipom o.n.A."
* #8850:0 ^designation[0].value = "Tymuslipom" 
* #8850:0 ^property[0].code = #parent 
* #8850:0 ^property[0].valueCode = #885-888 
* #8850:1 "Atypischer lipomatöser Tumor"
* #8850:1 ^designation[0].value = "Atypisches Lipom" 
* #8850:1 ^designation[1].value = "Oberflächliches gut differenziertes Liposarkom" 
* #8850:1 ^property[0].code = #parent 
* #8850:1 ^property[0].valueCode = #885-888 
* #8850:3 "Liposarkom o.n.A."
* #8850:3 ^designation[0].value = "Fibroliposarkom" 
* #8850:3 ^property[0].code = #parent 
* #8850:3 ^property[0].valueCode = #885-888 
* #8851:0 "Fibrolipom"
* #8851:0 ^property[0].code = #parent 
* #8851:0 ^property[0].valueCode = #885-888 
* #8851:1 "Lipofibromatose"
* #8851:1 ^property[0].code = #parent 
* #8851:1 ^property[0].valueCode = #885-888 
* #8851:3 "Gut differenziertes Liposarkom o.n.A."
* #8851:3 ^designation[0].value = "Differenziertes Liposarkom" 
* #8851:3 ^designation[1].value = "Lipomartiges Liposarkom" 
* #8851:3 ^designation[2].value = "Entzündliches Liposarkom" 
* #8851:3 ^designation[3].value = "Sklerosierendes Liposarkom" 
* #8851:3 ^property[0].code = #parent 
* #8851:3 ^property[0].valueCode = #885-888 
* #8852:0 "Fibromyxolipom"
* #8852:0 ^designation[0].value = "Myxolipom" 
* #8852:0 ^property[0].code = #parent 
* #8852:0 ^property[0].valueCode = #885-888 
* #8852:3 "Myxoides Liposarkom"
* #8852:3 ^designation[0].value = "Myxoliposarkom" 
* #8852:3 ^property[0].code = #parent 
* #8852:3 ^property[0].valueCode = #885-888 
* #8853:3 "Rundzelliges Liposarkom"
* #8853:3 ^property[0].code = #parent 
* #8853:3 ^property[0].valueCode = #885-888 
* #8854:0 "Pleomorphes Lipom"
* #8854:0 ^property[0].code = #parent 
* #8854:0 ^property[0].valueCode = #885-888 
* #8854:3 "Pleomorphes Liposarkom"
* #8854:3 ^property[0].code = #parent 
* #8854:3 ^property[0].valueCode = #885-888 
* #8855:3 "Gemischtzelliges Liposarkom"
* #8855:3 ^property[0].code = #parent 
* #8855:3 ^property[0].valueCode = #885-888 
* #8856:0 "Intramuskuläres Lipom"
* #8856:0 ^designation[0].value = "Infiltrierendes Angiolipom" 
* #8856:0 ^designation[1].value = "Infiltrierendes Lipom" 
* #8856:0 ^property[0].code = #parent 
* #8856:0 ^property[0].valueCode = #885-888 
* #8857:0 "Spindelzell-Lipom"
* #8857:0 ^property[0].code = #parent 
* #8857:0 ^property[0].valueCode = #885-888 
* #8857:3 "Fibroblastisches Liposarkom"
* #8857:3 ^property[0].code = #parent 
* #8857:3 ^property[0].valueCode = #885-888 
* #8858:3 "Entdifferenziertes Liposarkom"
* #8858:3 ^property[0].code = #parent 
* #8858:3 ^property[0].valueCode = #885-888 
* #8860:0 "Angiomyolipom"
* #8860:0 ^property[0].code = #parent 
* #8860:0 ^property[0].valueCode = #885-888 
* #8860:1 "Epitheloides Angiomyolipom"
* #8860:1 ^property[0].code = #parent 
* #8860:1 ^property[0].valueCode = #885-888 
* #8861:0 "Angiolipom o.n.A."
* #8861:0 ^property[0].code = #parent 
* #8861:0 ^property[0].valueCode = #885-888 
* #8862:0 "Chondroides Lipom"
* #8862:0 ^property[0].code = #parent 
* #8862:0 ^property[0].valueCode = #885-888 
* #8870:0 "Myelolipom"
* #8870:0 ^property[0].code = #parent 
* #8870:0 ^property[0].valueCode = #885-888 
* #8880:0 "Hibernom"
* #8880:0 ^designation[0].value = "Brauner Fettzelltumor" 
* #8880:0 ^designation[1].value = "Fetales Fettzellenlipom" 
* #8880:0 ^property[0].code = #parent 
* #8880:0 ^property[0].valueCode = #885-888 
* #8881:0 "Lipoblastomatose"
* #8881:0 ^designation[0].value = "Fetale Lipomatose" 
* #8881:0 ^designation[1].value = "Fetales Lipom o.n.A." 
* #8881:0 ^designation[2].value = "Lipoblastom" 
* #8881:0 ^property[0].code = #parent 
* #8881:0 ^property[0].valueCode = #885-888 
* #889-892 "Myomatöse Neoplasien"
* #889-892 ^property[0].code = #parent 
* #889-892 ^property[0].valueCode = #M 
* #889-892 ^property[1].code = #child 
* #889-892 ^property[1].valueCode = #8890:0 
* #889-892 ^property[2].code = #child 
* #889-892 ^property[2].valueCode = #8890:1 
* #889-892 ^property[3].code = #child 
* #889-892 ^property[3].valueCode = #8890:3 
* #889-892 ^property[4].code = #child 
* #889-892 ^property[4].valueCode = #8891:0 
* #889-892 ^property[5].code = #child 
* #889-892 ^property[5].valueCode = #8891:3 
* #889-892 ^property[6].code = #child 
* #889-892 ^property[6].valueCode = #8892:0 
* #889-892 ^property[7].code = #child 
* #889-892 ^property[7].valueCode = #8893:0 
* #889-892 ^property[8].code = #child 
* #889-892 ^property[8].valueCode = #8894:0 
* #889-892 ^property[9].code = #child 
* #889-892 ^property[9].valueCode = #8894:3 
* #889-892 ^property[10].code = #child 
* #889-892 ^property[10].valueCode = #8895:0 
* #889-892 ^property[11].code = #child 
* #889-892 ^property[11].valueCode = #8895:3 
* #889-892 ^property[12].code = #child 
* #889-892 ^property[12].valueCode = #8896:0 
* #889-892 ^property[13].code = #child 
* #889-892 ^property[13].valueCode = #8896:3 
* #889-892 ^property[14].code = #child 
* #889-892 ^property[14].valueCode = #8897:1 
* #889-892 ^property[15].code = #child 
* #889-892 ^property[15].valueCode = #8898:1 
* #889-892 ^property[16].code = #child 
* #889-892 ^property[16].valueCode = #8900:0 
* #889-892 ^property[17].code = #child 
* #889-892 ^property[17].valueCode = #8900:3 
* #889-892 ^property[18].code = #child 
* #889-892 ^property[18].valueCode = #8901:3 
* #889-892 ^property[19].code = #child 
* #889-892 ^property[19].valueCode = #8902:3 
* #889-892 ^property[20].code = #child 
* #889-892 ^property[20].valueCode = #8903:0 
* #889-892 ^property[21].code = #child 
* #889-892 ^property[21].valueCode = #8904:0 
* #889-892 ^property[22].code = #child 
* #889-892 ^property[22].valueCode = #8905:0 
* #889-892 ^property[23].code = #child 
* #889-892 ^property[23].valueCode = #8910:3 
* #889-892 ^property[24].code = #child 
* #889-892 ^property[24].valueCode = #8912:3 
* #889-892 ^property[25].code = #child 
* #889-892 ^property[25].valueCode = #8920:3 
* #889-892 ^property[26].code = #child 
* #889-892 ^property[26].valueCode = #8921:3 
* #8890:0 "Leiomyom o.n.A."
* #8890:0 ^designation[0].value = "Fibroid-Uterus" 
* #8890:0 ^designation[1].value = "Fibromyom" 
* #8890:0 ^designation[2].value = "Leiomyofibrom" 
* #8890:0 ^designation[3].value = "Apoplektisches Leiomyom" 
* #8890:0 ^designation[4].value = "Hydropisches Leiomyom" 
* #8890:0 ^designation[5].value = "Kotyledonoides LeiomyomDisseziierendes Leiomyom" 
* #8890:0 ^designation[6].value = "LipoleiomyomLipomatöses Leiomyom" 
* #8890:0 ^designation[7].value = "Myolipom" 
* #8890:0 ^designation[8].value = "Plexiformes Leiomyom" 
* #8890:0 ^property[0].code = #parent 
* #8890:0 ^property[0].valueCode = #889-892 
* #8890:1 "Leiomyomatose o.n.A."
* #8890:1 ^designation[0].value = "Disseminierte peritoneale Leiomyomatose" 
* #8890:1 ^designation[1].value = "Intravaskuläre LeiomyomatoseIntravenöse Leiomyomatose" 
* #8890:1 ^property[0].code = #parent 
* #8890:1 ^property[0].valueCode = #889-892 
* #8890:3 "Leiomyosarkom o.n.A."
* #8890:3 ^property[0].code = #parent 
* #8890:3 ^property[0].valueCode = #889-892 
* #8891:0 "Epitheloides Leiomyom"
* #8891:0 ^designation[0].value = "Leiomyoblastom" 
* #8891:0 ^property[0].code = #parent 
* #8891:0 ^property[0].valueCode = #889-892 
* #8891:3 "Epitheloides Leiomyosarkom"
* #8891:3 ^property[0].code = #parent 
* #8891:3 ^property[0].valueCode = #889-892 
* #8892:0 "Zellreiches Leiomyom"
* #8892:0 ^property[0].code = #parent 
* #8892:0 ^property[0].valueCode = #889-892 
* #8893:0 "Bizarres Leiomyom"
* #8893:0 ^designation[0].value = "Atypisches Leiomyom" 
* #8893:0 ^designation[1].value = "Pleomorphes Leiomyom" 
* #8893:0 ^designation[2].value = "Symplastisches Leiomyom" 
* #8893:0 ^property[0].code = #parent 
* #8893:0 ^property[0].valueCode = #889-892 
* #8894:0 "Angioleiomyom"
* #8894:0 ^designation[0].value = "Angiomyom" 
* #8894:0 ^designation[1].value = "Vaskuläres Leiomyom" 
* #8894:0 ^property[0].code = #parent 
* #8894:0 ^property[0].valueCode = #889-892 
* #8894:3 "Angiomyosarkom"
* #8894:3 ^property[0].code = #parent 
* #8894:3 ^property[0].valueCode = #889-892 
* #8895:0 "Myom"
* #8895:0 ^property[0].code = #parent 
* #8895:0 ^property[0].valueCode = #889-892 
* #8895:3 "Myosarkom"
* #8895:3 ^property[0].code = #parent 
* #8895:3 ^property[0].valueCode = #889-892 
* #8896:0 "Myxoides Leiomyom"
* #8896:0 ^property[0].code = #parent 
* #8896:0 ^property[0].valueCode = #889-892 
* #8896:3 "Myxoides Leiomyosarkom"
* #8896:3 ^property[0].code = #parent 
* #8896:3 ^property[0].valueCode = #889-892 
* #8897:1 "Tumor der glatten Muskulatur mit fraglichem malignem Potential"
* #8897:1 ^designation[0].value = "Atypischer Tumor der glatten Muskulatur" 
* #8897:1 ^designation[1].value = "Kutanes Leiomyosarkom" 
* #8897:1 ^designation[2].value = "Tumor der glatten Muskulatur o.n.A." 
* #8897:1 ^property[0].code = #parent 
* #8897:1 ^property[0].valueCode = #889-892 
* #8898:1 "Metastasierendes Leiomyom"
* #8898:1 ^property[0].code = #parent 
* #8898:1 ^property[0].valueCode = #889-892 
* #8900:0 "Rhabdomyom o.n.A."
* #8900:0 ^property[0].code = #parent 
* #8900:0 ^property[0].valueCode = #889-892 
* #8900:3 "Rhabdomyosarkom o.n.A."
* #8900:3 ^designation[0].value = "Rhabdosarkom" 
* #8900:3 ^property[0].code = #parent 
* #8900:3 ^property[0].valueCode = #889-892 
* #8901:3 "Adultes pleomorphes Rhabdomyosarkom"
* #8901:3 ^designation[0].value = "Pleomorphes Rhabdomyosarkom o.n.A." 
* #8901:3 ^property[0].code = #parent 
* #8901:3 ^property[0].valueCode = #889-892 
* #8902:3 "Rhabdomyosarkom vom Mischtyp"
* #8902:3 ^designation[0].value = "Gemischtes embryonales und alveoläres Rhabdomyosarkom" 
* #8902:3 ^property[0].code = #parent 
* #8902:3 ^property[0].valueCode = #889-892 
* #8903:0 "Fetales Rhabdomyom"
* #8903:0 ^property[0].code = #parent 
* #8903:0 ^property[0].valueCode = #889-892 
* #8904:0 "Adultes zellreiches Rhabdomyom"
* #8904:0 ^designation[0].value = "Adultes Rhabdomyom" 
* #8904:0 ^designation[1].value = "Glykogenreiches Rhabdomyom" 
* #8904:0 ^property[0].code = #parent 
* #8904:0 ^property[0].valueCode = #889-892 
* #8905:0 "Genitales Rhabdomyom"
* #8905:0 ^property[0].code = #parent 
* #8905:0 ^property[0].valueCode = #889-892 
* #8910:3 "Embryonales Rhabdomyosarkom o.n.A."
* #8910:3 ^designation[0].value = "Rhabdomyosarkom vom embryonalen Typ" 
* #8910:3 ^designation[1].value = "Embryonales pleomorphes Rhabdomyosarkom" 
* #8910:3 ^designation[2].value = "Sarcoma botryoidesBotryoides Sarkom" 
* #8910:3 ^property[0].code = #parent 
* #8910:3 ^property[0].valueCode = #889-892 
* #8912:3 "Spindelzelliges Rhabdomyosarkom"
* #8912:3 ^designation[0].value = "Rhabdomyosarkom, spindelzell-/sklerosierender Typ" 
* #8912:3 ^designation[1].value = "Sklerosierendes Rhabdomyosarkom" 
* #8912:3 ^property[0].code = #parent 
* #8912:3 ^property[0].valueCode = #889-892 
* #8920:3 "Alveoläres Rhabdomyosarkom"
* #8920:3 ^property[0].code = #parent 
* #8920:3 ^property[0].valueCode = #889-892 
* #8921:3 "Ektomesenchymom"
* #8921:3 ^designation[0].value = "Rhabdomyosarkom mit ganglionärer Differenzierung" 
* #8921:3 ^property[0].code = #parent 
* #8921:3 ^property[0].valueCode = #889-892 
* #893-899 "Komplexe Misch- und Stromaneoplasien"
* #893-899 ^property[0].code = #parent 
* #893-899 ^property[0].valueCode = #M 
* #893-899 ^property[1].code = #child 
* #893-899 ^property[1].valueCode = #8930:0 
* #893-899 ^property[2].code = #child 
* #893-899 ^property[2].valueCode = #8930:3 
* #893-899 ^property[3].code = #child 
* #893-899 ^property[3].valueCode = #8931:3 
* #893-899 ^property[4].code = #child 
* #893-899 ^property[4].valueCode = #8932:0 
* #893-899 ^property[5].code = #child 
* #893-899 ^property[5].valueCode = #8933:3 
* #893-899 ^property[6].code = #child 
* #893-899 ^property[6].valueCode = #8934:3 
* #893-899 ^property[7].code = #child 
* #893-899 ^property[7].valueCode = #8935:0 
* #893-899 ^property[8].code = #child 
* #893-899 ^property[8].valueCode = #8935:1 
* #893-899 ^property[9].code = #child 
* #893-899 ^property[9].valueCode = #8935:3 
* #893-899 ^property[10].code = #child 
* #893-899 ^property[10].valueCode = #8936:3 
* #893-899 ^property[11].code = #child 
* #893-899 ^property[11].valueCode = #8940:0 
* #893-899 ^property[12].code = #child 
* #893-899 ^property[12].valueCode = #8940:3 
* #893-899 ^property[13].code = #child 
* #893-899 ^property[13].valueCode = #8941:3 
* #893-899 ^property[14].code = #child 
* #893-899 ^property[14].valueCode = #8950:3 
* #893-899 ^property[15].code = #child 
* #893-899 ^property[15].valueCode = #8951:3 
* #893-899 ^property[16].code = #child 
* #893-899 ^property[16].valueCode = #8959:0 
* #893-899 ^property[17].code = #child 
* #893-899 ^property[17].valueCode = #8959:1 
* #893-899 ^property[18].code = #child 
* #893-899 ^property[18].valueCode = #8959:3 
* #893-899 ^property[19].code = #child 
* #893-899 ^property[19].valueCode = #8960:1 
* #893-899 ^property[20].code = #child 
* #893-899 ^property[20].valueCode = #8960:3 
* #893-899 ^property[21].code = #child 
* #893-899 ^property[21].valueCode = #8963:3 
* #893-899 ^property[22].code = #child 
* #893-899 ^property[22].valueCode = #8964:3 
* #893-899 ^property[23].code = #child 
* #893-899 ^property[23].valueCode = #8966:0 
* #893-899 ^property[24].code = #child 
* #893-899 ^property[24].valueCode = #8967:0 
* #893-899 ^property[25].code = #child 
* #893-899 ^property[25].valueCode = #8970:3 
* #893-899 ^property[26].code = #child 
* #893-899 ^property[26].valueCode = #8971:3 
* #893-899 ^property[27].code = #child 
* #893-899 ^property[27].valueCode = #8972:3 
* #893-899 ^property[28].code = #child 
* #893-899 ^property[28].valueCode = #8973:3 
* #893-899 ^property[29].code = #child 
* #893-899 ^property[29].valueCode = #8974:1 
* #893-899 ^property[30].code = #child 
* #893-899 ^property[30].valueCode = #8975:1 
* #893-899 ^property[31].code = #child 
* #893-899 ^property[31].valueCode = #8980:3 
* #893-899 ^property[32].code = #child 
* #893-899 ^property[32].valueCode = #8981:3 
* #893-899 ^property[33].code = #child 
* #893-899 ^property[33].valueCode = #8982:0 
* #893-899 ^property[34].code = #child 
* #893-899 ^property[34].valueCode = #8982:3 
* #893-899 ^property[35].code = #child 
* #893-899 ^property[35].valueCode = #8983:0 
* #893-899 ^property[36].code = #child 
* #893-899 ^property[36].valueCode = #8983:3 
* #893-899 ^property[37].code = #child 
* #893-899 ^property[37].valueCode = #8990:0 
* #893-899 ^property[38].code = #child 
* #893-899 ^property[38].valueCode = #8990:1 
* #893-899 ^property[39].code = #child 
* #893-899 ^property[39].valueCode = #8990:3 
* #893-899 ^property[40].code = #child 
* #893-899 ^property[40].valueCode = #8991:3 
* #893-899 ^property[41].code = #child 
* #893-899 ^property[41].valueCode = #8992:0 
* #8930:0 "Endometrium-Stromaknoten"
* #8930:0 ^property[0].code = #parent 
* #8930:0 ^property[0].valueCode = #893-899 
* #8930:3 "Stromasarkom des Endometriums o.n.A."
* #8930:3 ^designation[0].value = "Endometriumsarkom o.n.A." 
* #8930:3 ^designation[1].value = "Hochmalignes Stromasarkom des Endometriums" 
* #8930:3 ^property[0].code = #parent 
* #8930:3 ^property[0].valueCode = #893-899 
* #8931:3 "Niedrigmalignes Stromasarkom des Endometriums"
* #8931:3 ^designation[0].value = "Endolymphatische Stromamyose" 
* #8931:3 ^designation[1].value = "Endometrium-Stromatose" 
* #8931:3 ^designation[2].value = "Stroma-Endometriose" 
* #8931:3 ^designation[3].value = "Stromamyose o.n.A." 
* #8931:3 ^property[0].code = #parent 
* #8931:3 ^property[0].valueCode = #893-899 
* #8932:0 "Adenomyom o.n.A."
* #8932:0 ^designation[0].value = "Atypisches polypoides Adenomyom" 
* #8932:0 ^property[0].code = #parent 
* #8932:0 ^property[0].valueCode = #893-899 
* #8933:3 "Adenosarkom"
* #8933:3 ^property[0].code = #parent 
* #8933:3 ^property[0].valueCode = #893-899 
* #8934:3 "Karzinofibrom"
* #8934:3 ^property[0].code = #parent 
* #8934:3 ^property[0].valueCode = #893-899 
* #8935:0 "Benigner Stromatumor"
* #8935:0 ^property[0].code = #parent 
* #8935:0 ^property[0].valueCode = #893-899 
* #8935:1 "Stromatumor o.n.A."
* #8935:1 ^designation[0].value = "Metanephritischer Stromatumor" 
* #8935:1 ^designation[1].value = "Stromatumor mit unsicherem malignem Potential" 
* #8935:1 ^property[0].code = #parent 
* #8935:1 ^property[0].valueCode = #893-899 
* #8935:3 "Stromasarkom o.n.A."
* #8935:3 ^property[0].code = #parent 
* #8935:3 ^property[0].valueCode = #893-899 
* #8936:3 "Gastrointestinaler Stromatumor"
* #8936:3 ^designation[0].value = "Gastrointestinales Stromasarkom" 
* #8936:3 ^designation[1].value = "GIST" 
* #8936:3 ^designation[2].value = "Gastrointestinaler autonomer Nerventumor [obs.]GANT [obs.]" 
* #8936:3 ^designation[3].value = "Gastrointestinaler Schrittmacherzellen-Tumor [obs.]" 
* #8936:3 ^property[0].code = #parent 
* #8936:3 ^property[0].valueCode = #893-899 
* #8940:0 "Pleomorphes Adenom"
* #8940:0 ^designation[0].value = "Mischtumor o.n.A." 
* #8940:0 ^designation[1].value = "Mischtumor vom Speicheldrüsentyp o.n.A." 
* #8940:0 ^designation[2].value = "Chondroides Syringom" 
* #8940:0 ^property[0].code = #parent 
* #8940:0 ^property[0].valueCode = #893-899 
* #8940:3 "Maligner Mischtumor o.n.A."
* #8940:3 ^designation[0].value = "Maligner Mischtumor vom Speicheldrüsentyp" 
* #8940:3 ^designation[1].value = "Malignes chondroides Syringom" 
* #8940:3 ^property[0].code = #parent 
* #8940:3 ^property[0].valueCode = #893-899 
* #8941:3 "Karzinom in pleomorphem Adenom"
* #8941:3 ^property[0].code = #parent 
* #8941:3 ^property[0].valueCode = #893-899 
* #8950:3 "Maligner Müller-Mischtumor"
* #8950:3 ^property[0].code = #parent 
* #8950:3 ^property[0].valueCode = #893-899 
* #8951:3 "Maligner mesodermaler Mischtumor"
* #8951:3 ^property[0].code = #parent 
* #8951:3 ^property[0].valueCode = #893-899 
* #8959:0 "Benignes zystisches Nephrom"
* #8959:0 ^designation[0].value = "Adultes zystisches Nephrom" 
* #8959:0 ^designation[1].value = "Gemischter Epithel- und Stromatumor" 
* #8959:0 ^designation[2].value = "Zystischer Stromatumor des Kindesalters" 
* #8959:0 ^property[0].code = #parent 
* #8959:0 ^property[0].valueCode = #893-899 
* #8959:1 "Zystisches partiell differenziertes Nephroblastom"
* #8959:1 ^property[0].code = #parent 
* #8959:1 ^property[0].valueCode = #893-899 
* #8959:3 "Malignes zystisches Nephrom"
* #8959:3 ^designation[0].value = "Malignes multilokuläres zystisches Nephrom" 
* #8959:3 ^property[0].code = #parent 
* #8959:3 ^property[0].valueCode = #893-899 
* #8960:1 "Mesoblastisches Nephrom"
* #8960:1 ^property[0].code = #parent 
* #8960:1 ^property[0].valueCode = #893-899 
* #8960:3 "Nephroblastom o.n.A."
* #8960:3 ^designation[0].value = "Nephrom o.n.A." 
* #8960:3 ^designation[1].value = "Wilms-Tumor" 
* #8960:3 ^property[0].code = #parent 
* #8960:3 ^property[0].valueCode = #893-899 
* #8963:3 "Rhabdoidtumor o.n.A."
* #8963:3 ^designation[0].value = "Maligner Rhabdoidtumor" 
* #8963:3 ^designation[1].value = "Rhabdoid-Sarkom" 
* #8963:3 ^property[0].code = #parent 
* #8963:3 ^property[0].valueCode = #893-899 
* #8964:3 "Klarzelliges Nierensarkom"
* #8964:3 ^property[0].code = #parent 
* #8964:3 ^property[0].valueCode = #893-899 
* #8966:0 "Interstitialzellentumor des Nierenmarks"
* #8966:0 ^designation[0].value = "Medulläres Fibrom" 
* #8966:0 ^designation[1].value = "Nierenmarkfibrom" 
* #8966:0 ^property[0].code = #parent 
* #8966:0 ^property[0].valueCode = #893-899 
* #8967:0 "Ossifizierender Nierentumor"
* #8967:0 ^property[0].code = #parent 
* #8967:0 ^property[0].valueCode = #893-899 
* #8970:3 "Hepatoblastom"
* #8970:3 ^designation[0].value = "Embryonales Hepatom" 
* #8970:3 ^designation[1].value = "Epitheloides Hepatoblastom" 
* #8970:3 ^designation[2].value = "Gemischtes epithelial-mesenchymales Hepatoblastom" 
* #8970:3 ^property[0].code = #parent 
* #8970:3 ^property[0].valueCode = #893-899 
* #8971:3 "Pankreatoblastom"
* #8971:3 ^property[0].code = #parent 
* #8971:3 ^property[0].valueCode = #893-899 
* #8972:3 "Lungenblastom"
* #8972:3 ^designation[0].value = "Pneumoblastom" 
* #8972:3 ^property[0].code = #parent 
* #8972:3 ^property[0].valueCode = #893-899 
* #8973:3 "Pleuropulmonales Blastom"
* #8973:3 ^property[0].code = #parent 
* #8973:3 ^property[0].valueCode = #893-899 
* #8974:1 "Sialoblastom"
* #8974:1 ^property[0].code = #parent 
* #8974:1 ^property[0].valueCode = #893-899 
* #8975:1 "Kalzifizierender nestförmiger stromal-epithelialer Tumor (CNEST)"
* #8975:1 ^property[0].code = #parent 
* #8975:1 ^property[0].valueCode = #893-899 
* #8980:3 "Karzinosarkom o.n.A."
* #8980:3 ^property[0].code = #parent 
* #8980:3 ^property[0].valueCode = #893-899 
* #8981:3 "Embryonales Karzinosarkom"
* #8981:3 ^property[0].code = #parent 
* #8981:3 ^property[0].valueCode = #893-899 
* #8982:0 "Myoepitheliom o.n.A."
* #8982:0 ^designation[0].value = "Ektomesenchymaler chondromyxoider Tumor" 
* #8982:0 ^designation[1].value = "Myoepithelialer Tumor" 
* #8982:0 ^designation[2].value = "Myoepitheliales Adenom" 
* #8982:0 ^property[0].code = #parent 
* #8982:0 ^property[0].valueCode = #893-899 
* #8982:3 "Myoepitheliales Karzinom"
* #8982:3 ^designation[0].value = "Infiltrierendes Myoepitheliom" 
* #8982:3 ^designation[1].value = "Malignes Myoepitheliom" 
* #8982:3 ^property[0].code = #parent 
* #8982:3 ^property[0].valueCode = #893-899 
* #8983:0 "Adenomyoepitheliom o.n.A."
* #8983:0 ^designation[0].value = "Benignes Adenomyoepitheliom" 
* #8983:0 ^property[0].code = #parent 
* #8983:0 ^property[0].valueCode = #893-899 
* #8983:3 "Adenomyoepitheliom mit Karzinom"
* #8983:3 ^designation[0].value = "Malignes Adenomyoepitheliom" 
* #8983:3 ^property[0].code = #parent 
* #8983:3 ^property[0].valueCode = #893-899 
* #8990:0 "Benignes Mesenchymom"
* #8990:0 ^designation[0].value = "Gutartiger phosphaturischer mesenchymaler TumorPhosphaturischer mesenchymaler Tumor, o.n.A." 
* #8990:0 ^property[0].code = #parent 
* #8990:0 ^property[0].valueCode = #893-899 
* #8990:1 "Mesenchymom o.n.A."
* #8990:1 ^designation[0].value = "Mesenchymaler Mischtumor" 
* #8990:1 ^designation[1].value = "Primitiver nichtneuraler Granularzelltumor" 
* #8990:1 ^property[0].code = #parent 
* #8990:1 ^property[0].valueCode = #893-899 
* #8990:3 "Malignes Mesenchymom"
* #8990:3 ^designation[0].value = "Maligner gemischtzelliger mesenchymaler Tumor" 
* #8990:3 ^designation[1].value = "Maligner phosphaturischer mesenchymaler Tumor" 
* #8990:3 ^property[0].code = #parent 
* #8990:3 ^property[0].valueCode = #893-899 
* #8991:3 "Embryonales Sarkom"
* #8991:3 ^property[0].code = #parent 
* #8991:3 ^property[0].valueCode = #893-899 
* #8992:0 "Pulmonales Hamartom"
* #8992:0 ^property[0].code = #parent 
* #8992:0 ^property[0].valueCode = #893-899 
* #900-903 "Fibroepitheliale Neoplasien"
* #900-903 ^property[0].code = #parent 
* #900-903 ^property[0].valueCode = #M 
* #900-903 ^property[1].code = #child 
* #900-903 ^property[1].valueCode = #9000:0 
* #900-903 ^property[2].code = #child 
* #900-903 ^property[2].valueCode = #9000:1 
* #900-903 ^property[3].code = #child 
* #900-903 ^property[3].valueCode = #9000:3 
* #900-903 ^property[4].code = #child 
* #900-903 ^property[4].valueCode = #9010:0 
* #900-903 ^property[5].code = #child 
* #900-903 ^property[5].valueCode = #9011:0 
* #900-903 ^property[6].code = #child 
* #900-903 ^property[6].valueCode = #9012:0 
* #900-903 ^property[7].code = #child 
* #900-903 ^property[7].valueCode = #9013:0 
* #900-903 ^property[8].code = #child 
* #900-903 ^property[8].valueCode = #9014:0 
* #900-903 ^property[9].code = #child 
* #900-903 ^property[9].valueCode = #9014:1 
* #900-903 ^property[10].code = #child 
* #900-903 ^property[10].valueCode = #9014:3 
* #900-903 ^property[11].code = #child 
* #900-903 ^property[11].valueCode = #9015:0 
* #900-903 ^property[12].code = #child 
* #900-903 ^property[12].valueCode = #9015:1 
* #900-903 ^property[13].code = #child 
* #900-903 ^property[13].valueCode = #9015:3 
* #900-903 ^property[14].code = #child 
* #900-903 ^property[14].valueCode = #9016:0 
* #900-903 ^property[15].code = #child 
* #900-903 ^property[15].valueCode = #9020:0 
* #900-903 ^property[16].code = #child 
* #900-903 ^property[16].valueCode = #9020:1 
* #900-903 ^property[17].code = #child 
* #900-903 ^property[17].valueCode = #9020:3 
* #900-903 ^property[18].code = #child 
* #900-903 ^property[18].valueCode = #9030:0 
* #9000:0 "Brenner-Tumor o.n.A."
* #9000:0 ^property[0].code = #parent 
* #9000:0 ^property[0].valueCode = #900-903 
* #9000:1 "Brenner-Tumor mit Borderline-Malignität"
* #9000:1 ^designation[0].value = "Borderline-Brenner-Tumor" 
* #9000:1 ^designation[1].value = "Brenner-Tumor mit atypischer Proliferation" 
* #9000:1 ^designation[2].value = "Proliferierender Brenner-Tumor" 
* #9000:1 ^property[0].code = #parent 
* #9000:1 ^property[0].valueCode = #900-903 
* #9000:3 "Maligner Brenner-Tumor"
* #9000:3 ^property[0].code = #parent 
* #9000:3 ^property[0].valueCode = #900-903 
* #9010:0 "Fibroadenom"
* #9010:0 ^designation[0].value = "Lipofibroadenom" 
* #9010:0 ^property[0].code = #parent 
* #9010:0 ^property[0].valueCode = #900-903 
* #9011:0 "Intrakanalikuläres Fibroadenom"
* #9011:0 ^property[0].code = #parent 
* #9011:0 ^property[0].valueCode = #900-903 
* #9012:0 "Perikanalikuläres Fibroadenom"
* #9012:0 ^property[0].code = #parent 
* #9012:0 ^property[0].valueCode = #900-903 
* #9013:0 "Adenofibrom o.n.A."
* #9013:0 ^designation[0].value = "Metanephritisches AdenofibromNephrogenes Adenofibrom" 
* #9013:0 ^designation[1].value = "Papilläres Adenofibrom" 
* #9013:0 ^designation[2].value = "Zystadenofibrom o.n.A." 
* #9013:0 ^property[0].code = #parent 
* #9013:0 ^property[0].valueCode = #900-903 
* #9014:0 "Seröses Adenofibrom o.n.A."
* #9014:0 ^designation[0].value = "Seromuzinöses Adenofibrom" 
* #9014:0 ^designation[1].value = "Seröses Zystadenofibrom o.n.A." 
* #9014:0 ^property[0].code = #parent 
* #9014:0 ^property[0].valueCode = #900-903 
* #9014:1 "Seröses Adenofibrom mit Borderline-Malignität"
* #9014:1 ^designation[0].value = "Seröses Zystadenofibrom mit Borderline-Malignität" 
* #9014:1 ^property[0].code = #parent 
* #9014:1 ^property[0].valueCode = #900-903 
* #9014:3 "Seröses Adenokarzinofibrom"
* #9014:3 ^designation[0].value = "Malignes seröses Adenofibrom" 
* #9014:3 ^designation[1].value = "Seröses ZystadenokarzinofibromMalignes seröses Zystadenofibrom" 
* #9014:3 ^property[0].code = #parent 
* #9014:3 ^property[0].valueCode = #900-903 
* #9015:0 "Muzinöses Adenofibrom o.n.A."
* #9015:0 ^designation[0].value = "Muzinöses Zystadenofibrom o.n.A." 
* #9015:0 ^property[0].code = #parent 
* #9015:0 ^property[0].valueCode = #900-903 
* #9015:1 "Muzinöses Adenofibrom mit Borderline-Malignität"
* #9015:1 ^designation[0].value = "Muzinöses Zystadenofibrom mit Borderline-Malignität" 
* #9015:1 ^property[0].code = #parent 
* #9015:1 ^property[0].valueCode = #900-903 
* #9015:3 "Muzinöses Adenokarzinofibrom"
* #9015:3 ^designation[0].value = "Malignes muzinöses Adenofibrom" 
* #9015:3 ^designation[1].value = "Muzinöses ZystadenofibromMalignes muzinöses Zystadenofibrom" 
* #9015:3 ^property[0].code = #parent 
* #9015:3 ^property[0].valueCode = #900-903 
* #9016:0 "Riesenfibroadenom"
* #9016:0 ^property[0].code = #parent 
* #9016:0 ^property[0].valueCode = #900-903 
* #9020:0 "Benigner Phylloides-Tumor"
* #9020:0 ^designation[0].value = "Cystosarcoma phylloides benignum" 
* #9020:0 ^property[0].code = #parent 
* #9020:0 ^property[0].valueCode = #900-903 
* #9020:1 "Phylloides-Tumor mit Borderline-Malignität"
* #9020:1 ^designation[0].value = "Cystosarcoma phylloides o.n.A." 
* #9020:1 ^designation[1].value = "Phylloides-Tumor o.n.A." 
* #9020:1 ^property[0].code = #parent 
* #9020:1 ^property[0].valueCode = #900-903 
* #9020:3 "Maligner Phylloides-Tumor"
* #9020:3 ^designation[0].value = "Cystosarcoma phylloides malignum" 
* #9020:3 ^designation[1].value = "Periduktaler Stromatumor, niedriggradig" 
* #9020:3 ^property[0].code = #parent 
* #9020:3 ^property[0].valueCode = #900-903 
* #9030:0 "Juveniles Fibroadenom"
* #9030:0 ^property[0].code = #parent 
* #9030:0 ^property[0].valueCode = #900-903 
* #904-904 "Synoviaähnliche Neoplasien"
* #904-904 ^property[0].code = #parent 
* #904-904 ^property[0].valueCode = #M 
* #904-904 ^property[1].code = #child 
* #904-904 ^property[1].valueCode = #9040:0 
* #904-904 ^property[2].code = #child 
* #904-904 ^property[2].valueCode = #9040:3 
* #904-904 ^property[3].code = #child 
* #904-904 ^property[3].valueCode = #9041:3 
* #904-904 ^property[4].code = #child 
* #904-904 ^property[4].valueCode = #9042:3 
* #904-904 ^property[5].code = #child 
* #904-904 ^property[5].valueCode = #9043:3 
* #904-904 ^property[6].code = #child 
* #904-904 ^property[6].valueCode = #9044:3 
* #904-904 ^property[7].code = #child 
* #904-904 ^property[7].valueCode = #9045:3 
* #9040:0 "Benignes Synovialom"
* #9040:0 ^property[0].code = #parent 
* #9040:0 ^property[0].valueCode = #904-904 
* #9040:3 "Synovialsarkom o.n.A."
* #9040:3 ^designation[0].value = "Malignes Synoviom" 
* #9040:3 ^designation[1].value = "Synoviom o.n.A." 
* #9040:3 ^property[0].code = #parent 
* #9040:3 ^property[0].valueCode = #904-904 
* #9041:3 "Spindelzelliges Synovialsarkom"
* #9041:3 ^designation[0].value = "Synovialsarkom vom monophasisch-fibrösen Typ" 
* #9041:3 ^property[0].code = #parent 
* #9041:3 ^property[0].valueCode = #904-904 
* #9042:3 "Epitheliales Synovialsarkom"
* #9042:3 ^property[0].code = #parent 
* #9042:3 ^property[0].valueCode = #904-904 
* #9043:3 "Biphasisches Synovialsarkom"
* #9043:3 ^property[0].code = #parent 
* #9043:3 ^property[0].valueCode = #904-904 
* #9044:3 "Klarzellsarkom o.n.A."
* #9044:3 ^designation[0].value = "Malignes Weichteilmelanom" 
* #9044:3 ^property[0].code = #parent 
* #9044:3 ^property[0].valueCode = #904-904 
* #9045:3 "Biphänotypisches sinunasales Sarkom"
* #9045:3 ^property[0].code = #parent 
* #9045:3 ^property[0].valueCode = #904-904 
* #905-905 "Mesotheliale Neoplasien"
* #905-905 ^property[0].code = #parent 
* #905-905 ^property[0].valueCode = #M 
* #905-905 ^property[1].code = #child 
* #905-905 ^property[1].valueCode = #9050:0 
* #905-905 ^property[2].code = #child 
* #905-905 ^property[2].valueCode = #9050:3 
* #905-905 ^property[3].code = #child 
* #905-905 ^property[3].valueCode = #9051:0 
* #905-905 ^property[4].code = #child 
* #905-905 ^property[4].valueCode = #9051:3 
* #905-905 ^property[5].code = #child 
* #905-905 ^property[5].valueCode = #9052:0 
* #905-905 ^property[6].code = #child 
* #905-905 ^property[6].valueCode = #9052:1 
* #905-905 ^property[7].code = #child 
* #905-905 ^property[7].valueCode = #9052:3 
* #905-905 ^property[8].code = #child 
* #905-905 ^property[8].valueCode = #9053:3 
* #905-905 ^property[9].code = #child 
* #905-905 ^property[9].valueCode = #9054:0 
* #905-905 ^property[10].code = #child 
* #905-905 ^property[10].valueCode = #9055:0 
* #9050:0 "Benignes Mesotheliom"
* #9050:0 ^property[0].code = #parent 
* #9050:0 ^property[0].valueCode = #905-905 
* #9050:3 "Malignes Mesotheliom"
* #9050:3 ^designation[0].value = "DMM" 
* #9050:3 ^designation[1].value = "Mesotheliom o.n.A." 
* #9050:3 ^property[0].code = #parent 
* #9050:3 ^property[0].valueCode = #905-905 
* #9051:0 "Fibröses benignes Mesotheliom"
* #9051:0 ^property[0].code = #parent 
* #9051:0 ^property[0].valueCode = #905-905 
* #9051:3 "Fibröses malignes Mesotheliom"
* #9051:3 ^designation[0].value = "Desmoplastisches Mesotheliom" 
* #9051:3 ^designation[1].value = "Fibröses Mesotheliom o.n.A." 
* #9051:3 ^designation[2].value = "Sarkomatöses Mesotheliom" 
* #9051:3 ^designation[3].value = "Spindelzelliges Mesotheliom" 
* #9051:3 ^property[0].code = #parent 
* #9051:3 ^property[0].valueCode = #905-905 
* #9052:0 "Epitheloides benignes Mesotheliom"
* #9052:0 ^designation[0].value = "Gut differenziertes papilläres Mesotheliom Mesotheliales Papillom" 
* #9052:0 ^property[0].code = #parent 
* #9052:0 ^property[0].valueCode = #905-905 
* #9052:1 "Gut differenziertes papilläres Mesotheliom der Pleura"
* #9052:1 ^property[0].code = #parent 
* #9052:1 ^property[0].valueCode = #905-905 
* #9052:3 "Epitheloides malignes Mesotheliom"
* #9052:3 ^designation[0].value = "Epitheloides Mesotheliom o.n.A." 
* #9052:3 ^property[0].code = #parent 
* #9052:3 ^property[0].valueCode = #905-905 
* #9053:3 "Biphasisches malignes Mesotheliom"
* #9053:3 ^designation[0].value = "Biphasisches Mesotheliom o.n.A." 
* #9053:3 ^property[0].code = #parent 
* #9053:3 ^property[0].valueCode = #905-905 
* #9054:0 "Adenomatoid-Tumor o.n.A."
* #9054:0 ^property[0].code = #parent 
* #9054:0 ^property[0].valueCode = #905-905 
* #9055:0 "Peritoneale Einschlusszysten"
* #9055:0 ^designation[0].value = "Multizystisches Mesotheliom" 
* #9055:0 ^designation[1].value = "Zystisches Mesotheliom" 
* #9055:0 ^property[0].code = #parent 
* #9055:0 ^property[0].valueCode = #905-905 
* #906-909 "Neoplasien der Keimzellen"
* #906-909 ^property[0].code = #parent 
* #906-909 ^property[0].valueCode = #M 
* #906-909 ^property[1].code = #child 
* #906-909 ^property[1].valueCode = #9060:3 
* #906-909 ^property[2].code = #child 
* #906-909 ^property[2].valueCode = #9061:3 
* #906-909 ^property[3].code = #child 
* #906-909 ^property[3].valueCode = #9062:3 
* #906-909 ^property[4].code = #child 
* #906-909 ^property[4].valueCode = #9063:3 
* #906-909 ^property[5].code = #child 
* #906-909 ^property[5].valueCode = #9064:2 
* #906-909 ^property[6].code = #child 
* #906-909 ^property[6].valueCode = #9064:3 
* #906-909 ^property[7].code = #child 
* #906-909 ^property[7].valueCode = #9065:3 
* #906-909 ^property[8].code = #child 
* #906-909 ^property[8].valueCode = #9070:3 
* #906-909 ^property[9].code = #child 
* #906-909 ^property[9].valueCode = #9071:3 
* #906-909 ^property[10].code = #child 
* #906-909 ^property[10].valueCode = #9072:3 
* #906-909 ^property[11].code = #child 
* #906-909 ^property[11].valueCode = #9073:1 
* #906-909 ^property[12].code = #child 
* #906-909 ^property[12].valueCode = #9080:0 
* #906-909 ^property[13].code = #child 
* #906-909 ^property[13].valueCode = #9080:1 
* #906-909 ^property[14].code = #child 
* #906-909 ^property[14].valueCode = #9080:3 
* #906-909 ^property[15].code = #child 
* #906-909 ^property[15].valueCode = #9081:3 
* #906-909 ^property[16].code = #child 
* #906-909 ^property[16].valueCode = #9082:3 
* #906-909 ^property[17].code = #child 
* #906-909 ^property[17].valueCode = #9083:3 
* #906-909 ^property[18].code = #child 
* #906-909 ^property[18].valueCode = #9084:0 
* #906-909 ^property[19].code = #child 
* #906-909 ^property[19].valueCode = #9084:3 
* #906-909 ^property[20].code = #child 
* #906-909 ^property[20].valueCode = #9085:3 
* #906-909 ^property[21].code = #child 
* #906-909 ^property[21].valueCode = #9086:3 
* #906-909 ^property[22].code = #child 
* #906-909 ^property[22].valueCode = #9090:0 
* #906-909 ^property[23].code = #child 
* #906-909 ^property[23].valueCode = #9090:3 
* #906-909 ^property[24].code = #child 
* #906-909 ^property[24].valueCode = #9091:1 
* #9060:3 "Dysgerminom"
* #9060:3 ^property[0].code = #parent 
* #9060:3 ^property[0].valueCode = #906-909 
* #9061:3 "Seminom o.n.A."
* #9061:3 ^property[0].code = #parent 
* #9061:3 ^property[0].valueCode = #906-909 
* #9062:3 "Anaplastisches Seminom"
* #9062:3 ^designation[0].value = "Seminom mit hohem mitotischem Index" 
* #9062:3 ^property[0].code = #parent 
* #9062:3 ^property[0].valueCode = #906-909 
* #9063:3 "Spermatozytisches Seminom"
* #9063:3 ^designation[0].value = "Spermatozytom" 
* #9063:3 ^designation[1].value = "Spermazytischer Tumor" 
* #9063:3 ^property[0].code = #parent 
* #9063:3 ^property[0].valueCode = #906-909 
* #9064:2 "Maligne intratubuläre Keimzellen"
* #9064:2 ^designation[0].value = "Germinales Carcinoma in situ" 
* #9064:2 ^designation[1].value = "Testikuläre intraepitheliale NeoplasieTIN" 
* #9064:2 ^property[0].code = #parent 
* #9064:2 ^property[0].valueCode = #906-909 
* #9064:3 "Germinom"
* #9064:3 ^designation[0].value = "Keimzelltumor o.n.A." 
* #9064:3 ^property[0].code = #parent 
* #9064:3 ^property[0].valueCode = #906-909 
* #9065:3 "Nichtseminomatöser Keimzelltumor"
* #9065:3 ^property[0].code = #parent 
* #9065:3 ^property[0].valueCode = #906-909 
* #9070:3 "Embryonalkarzinom o.n.A."
* #9070:3 ^designation[0].value = "Embryonales Adenokarzinom" 
* #9070:3 ^property[0].code = #parent 
* #9070:3 ^property[0].valueCode = #906-909 
* #9071:3 "Dottersacktumor o.n.A."
* #9071:3 ^designation[0].value = "Dottersacktumor vom präpubertären Typ" 
* #9071:3 ^designation[1].value = "Endodermaler Sinustumor" 
* #9071:3 ^designation[2].value = "Infantiles Embryonalkarzinom" 
* #9071:3 ^designation[3].value = "Orchioblastom" 
* #9071:3 ^designation[4].value = "Polyvesikulärer Vitellintumor" 
* #9071:3 ^designation[5].value = "Dottersacktumor vom postpubertären Typ" 
* #9071:3 ^designation[6].value = "Hepatoider Dottersacktumor" 
* #9071:3 ^property[0].code = #parent 
* #9071:3 ^property[0].valueCode = #906-909 
* #9072:3 "Polyembryom"
* #9072:3 ^designation[0].value = "Embryonalkarzinom vom polyembryonalen Typ" 
* #9072:3 ^property[0].code = #parent 
* #9072:3 ^property[0].valueCode = #906-909 
* #9073:1 "Gonadoblastom"
* #9073:1 ^designation[0].value = "Gonadozytom" 
* #9073:1 ^property[0].code = #parent 
* #9073:1 ^property[0].valueCode = #906-909 
* #9080:0 "Benignes Teratom"
* #9080:0 ^designation[0].value = "Adultes Teratom o.n.A." 
* #9080:0 ^designation[1].value = "Adultes zystisches Teratom" 
* #9080:0 ^designation[2].value = "Differenziertes Teratom" 
* #9080:0 ^designation[3].value = "Reifes Teratom o.n.A." 
* #9080:0 ^designation[4].value = "Zystisches Teratom o.n.A." 
* #9080:0 ^property[0].code = #parent 
* #9080:0 ^property[0].valueCode = #906-909 
* #9080:1 "Teratom o.n.A."
* #9080:1 ^designation[0].value = "Solides Teratom" 
* #9080:1 ^designation[1].value = "Regredierter Keimzelltumor" 
* #9080:1 ^designation[2].value = "Unreifes Teratom der Lunge" 
* #9080:1 ^designation[3].value = "Unreifes Teratom der Schilddrüse" 
* #9080:1 ^designation[4].value = "Unreifes Teratom des Thymus" 
* #9080:1 ^property[0].code = #parent 
* #9080:1 ^property[0].valueCode = #906-909 
* #9080:3 "Malignes Teratom o.n.A."
* #9080:3 ^designation[0].value = "Embryonales Teratom" 
* #9080:3 ^designation[1].value = "Malignes Teratoblastom" 
* #9080:3 ^designation[2].value = "Unreifes malignes TeratomUnreifes Teratom o.n.A." 
* #9080:3 ^property[0].code = #parent 
* #9080:3 ^property[0].valueCode = #906-909 
* #9081:3 "Teratokarzinom"
* #9081:3 ^designation[0].value = "Kombiniertes Embryonalkarzinom und Teratom" 
* #9081:3 ^designation[1].value = "Teratokarzinosarkom" 
* #9081:3 ^property[0].code = #parent 
* #9081:3 ^property[0].valueCode = #906-909 
* #9082:3 "Undifferenziertes malignes Teratom"
* #9082:3 ^designation[0].value = "Anaplastisches malignes Teratom" 
* #9082:3 ^property[0].code = #parent 
* #9082:3 ^property[0].valueCode = #906-909 
* #9083:3 "Malignes Teratom vom intermediären Typ"
* #9083:3 ^property[0].code = #parent 
* #9083:3 ^property[0].valueCode = #906-909 
* #9084:0 "Dermoidzyste o.n.A."
* #9084:0 ^designation[0].value = "Dermoid o.n.A." 
* #9084:0 ^designation[1].value = "Teratom vom präpubertären TypReifes Teratom vom präpubertären Typ" 
* #9084:0 ^property[0].code = #parent 
* #9084:0 ^property[0].valueCode = #906-909 
* #9084:3 "Teratom mit maligner Transformation"
* #9084:3 ^designation[0].value = "Dermoidzyste mit maligner Transformation" 
* #9084:3 ^designation[1].value = "Dermoidzyste mit Sekundärtumor" 
* #9084:3 ^designation[2].value = "Teratom mit somatischer Malignität" 
* #9084:3 ^property[0].code = #parent 
* #9084:3 ^property[0].valueCode = #906-909 
* #9085:3 "Germinaler Mischtumor"
* #9085:3 ^designation[0].value = "Gemischtes Teratom und Seminom" 
* #9085:3 ^property[0].code = #parent 
* #9085:3 ^property[0].valueCode = #906-909 
* #9086:3 "Keimzelltumor mit assoziiertem hämatologischem Malignom"
* #9086:3 ^property[0].code = #parent 
* #9086:3 ^property[0].valueCode = #906-909 
* #9090:0 "Struma ovarii o.n.A."
* #9090:0 ^property[0].code = #parent 
* #9090:0 ^property[0].valueCode = #906-909 
* #9090:3 "Maligne Struma ovarii"
* #9090:3 ^property[0].code = #parent 
* #9090:3 ^property[0].valueCode = #906-909 
* #9091:1 "Struma-Karzinoid"
* #9091:1 ^designation[0].value = "Struma ovarii und Karzinoid" 
* #9091:1 ^property[0].code = #parent 
* #9091:1 ^property[0].valueCode = #906-909 
* #910-910 "Trophoblastische Neoplasien"
* #910-910 ^property[0].code = #parent 
* #910-910 ^property[0].valueCode = #M 
* #910-910 ^property[1].code = #child 
* #910-910 ^property[1].valueCode = #9100:0 
* #910-910 ^property[2].code = #child 
* #910-910 ^property[2].valueCode = #9100:1 
* #910-910 ^property[3].code = #child 
* #910-910 ^property[3].valueCode = #9100:3 
* #910-910 ^property[4].code = #child 
* #910-910 ^property[4].valueCode = #9101:3 
* #910-910 ^property[5].code = #child 
* #910-910 ^property[5].valueCode = #9102:3 
* #910-910 ^property[6].code = #child 
* #910-910 ^property[6].valueCode = #9103:0 
* #910-910 ^property[7].code = #child 
* #910-910 ^property[7].valueCode = #9104:1 
* #910-910 ^property[8].code = #child 
* #910-910 ^property[8].valueCode = #9105:3 
* #9100:0 "Blasenmole o.n.A."
* #9100:0 ^designation[0].value = "Klassische Blasenmole" 
* #9100:0 ^designation[1].value = "Komplette Blasenmole" 
* #9100:0 ^property[0].code = #parent 
* #9100:0 ^property[0].valueCode = #910-910 
* #9100:1 "Invasive Blasenmole"
* #9100:1 ^designation[0].value = "Chorionadenom" 
* #9100:1 ^designation[1].value = "Chorioadenoma destruens" 
* #9100:1 ^designation[2].value = "Invasive Mole o.n.A." 
* #9100:1 ^designation[3].value = "Maligne Blasenmole" 
* #9100:1 ^property[0].code = #parent 
* #9100:1 ^property[0].valueCode = #910-910 
* #9100:3 "Chorionkarzinom o.n.A."
* #9100:3 ^designation[0].value = "Chorionepitheliom" 
* #9100:3 ^property[0].code = #parent 
* #9100:3 ^property[0].valueCode = #910-910 
* #9101:3 "Chorionkarzinom in Kombination mit sonstigen Keimzellelementen"
* #9101:3 ^designation[0].value = "Chorionkarzinom kombiniert mit Teratom" 
* #9101:3 ^designation[1].value = "Chorionkarzinom kombiniert mit Embryonalkarzinom" 
* #9101:3 ^property[0].code = #parent 
* #9101:3 ^property[0].valueCode = #910-910 
* #9102:3 "Trophoblastisches malignes Teratom"
* #9102:3 ^property[0].code = #parent 
* #9102:3 ^property[0].valueCode = #910-910 
* #9103:0 "Partielle Blasenmole"
* #9103:0 ^property[0].code = #parent 
* #9103:0 ^property[0].valueCode = #910-910 
* #9104:1 "Trophoblastischer Plazentatumor"
* #9104:1 ^property[0].code = #parent 
* #9104:1 ^property[0].valueCode = #910-910 
* #9105:3 "Epitheloider Trophoblasttumor"
* #9105:3 ^property[0].code = #parent 
* #9105:3 ^property[0].valueCode = #910-910 
* #911-911 "Neoplasien des Mesonephros"
* #911-911 ^property[0].code = #parent 
* #911-911 ^property[0].valueCode = #M 
* #911-911 ^property[1].code = #child 
* #911-911 ^property[1].valueCode = #9110:0 
* #911-911 ^property[2].code = #child 
* #911-911 ^property[2].valueCode = #9110:1 
* #911-911 ^property[3].code = #child 
* #911-911 ^property[3].valueCode = #9110:3 
* #9110:0 "Adenom der Rete ovarii"
* #9110:0 ^designation[0].value = "Benignes Mesonephrom" 
* #9110:0 ^designation[1].value = "Adenom des Wolff-Ganges" 
* #9110:0 ^designation[2].value = "Mesonephrom" 
* #9110:0 ^property[0].code = #parent 
* #9110:0 ^property[0].valueCode = #911-911 
* #9110:1 "Wolff´scher Tumor"
* #9110:1 ^designation[0].value = "Mesonephrischer Tumor" 
* #9110:1 ^designation[1].value = "Tumor des Wolff-Ganges" 
* #9110:1 ^property[0].code = #parent 
* #9110:1 ^property[0].valueCode = #911-911 
* #9110:3 "Malignes Mesonephrom"
* #9110:3 ^designation[0].value = "Karzinom des Wolff-Ganges" 
* #9110:3 ^designation[1].value = "Mesonephrisches Adenokarzinom" 
* #9110:3 ^designation[2].value = "Mesonephrom o.n.A." 
* #9110:3 ^designation[3].value = "Adenokarzinom der Rete ovarii" 
* #9110:3 ^property[0].code = #parent 
* #9110:3 ^property[0].valueCode = #911-911 
* #912-916 "Neoplasien der Blutgefäße"
* #912-916 ^property[0].code = #parent 
* #912-916 ^property[0].valueCode = #M 
* #912-916 ^property[1].code = #child 
* #912-916 ^property[1].valueCode = #9120:0 
* #912-916 ^property[2].code = #child 
* #912-916 ^property[2].valueCode = #9120:3 
* #912-916 ^property[3].code = #child 
* #912-916 ^property[3].valueCode = #9121:0 
* #912-916 ^property[4].code = #child 
* #912-916 ^property[4].valueCode = #9122:0 
* #912-916 ^property[5].code = #child 
* #912-916 ^property[5].valueCode = #9123:0 
* #912-916 ^property[6].code = #child 
* #912-916 ^property[6].valueCode = #9124:3 
* #912-916 ^property[7].code = #child 
* #912-916 ^property[7].valueCode = #9125:0 
* #912-916 ^property[8].code = #child 
* #912-916 ^property[8].valueCode = #9126:0 
* #912-916 ^property[9].code = #child 
* #912-916 ^property[9].valueCode = #9130:0 
* #912-916 ^property[10].code = #child 
* #912-916 ^property[10].valueCode = #9130:1 
* #912-916 ^property[11].code = #child 
* #912-916 ^property[11].valueCode = #9130:3 
* #912-916 ^property[12].code = #child 
* #912-916 ^property[12].valueCode = #9131:0 
* #912-916 ^property[13].code = #child 
* #912-916 ^property[13].valueCode = #9132:0 
* #912-916 ^property[14].code = #child 
* #912-916 ^property[14].valueCode = #9133:3 
* #912-916 ^property[15].code = #child 
* #912-916 ^property[15].valueCode = #9135:1 
* #912-916 ^property[16].code = #child 
* #912-916 ^property[16].valueCode = #9136:1 
* #912-916 ^property[17].code = #child 
* #912-916 ^property[17].valueCode = #9137:0 
* #912-916 ^property[18].code = #child 
* #912-916 ^property[18].valueCode = #9137:3 
* #912-916 ^property[19].code = #child 
* #912-916 ^property[19].valueCode = #9138:1 
* #912-916 ^property[20].code = #child 
* #912-916 ^property[20].valueCode = #9140:3 
* #912-916 ^property[21].code = #child 
* #912-916 ^property[21].valueCode = #9141:0 
* #912-916 ^property[22].code = #child 
* #912-916 ^property[22].valueCode = #9142:0 
* #912-916 ^property[23].code = #child 
* #912-916 ^property[23].valueCode = #9160:0 
* #912-916 ^property[24].code = #child 
* #912-916 ^property[24].valueCode = #9161:0 
* #912-916 ^property[25].code = #child 
* #912-916 ^property[25].valueCode = #9161:1 
* #9120:0 "Hämangiom o.n.A."
* #9120:0 ^designation[0].value = "Angiom o.n.A." 
* #9120:0 ^designation[1].value = "Chorionangiom" 
* #9120:0 ^designation[2].value = "Glomeruloides Hämangiom" 
* #9120:0 ^designation[3].value = "Hobnail-Hämangiom" 
* #9120:0 ^designation[4].value = "Kirschhämangiom" 
* #9120:0 ^designation[5].value = "Mikrovenuläres Hämangiom" 
* #9120:0 ^designation[6].value = "Sinusoidales Hämangiom" 
* #9120:0 ^designation[7].value = "Spindelzellhämangiom" 
* #9120:0 ^property[0].code = #parent 
* #9120:0 ^property[0].valueCode = #912-916 
* #9120:3 "Hämangiosarkom"
* #9120:3 ^designation[0].value = "Angiosarkom" 
* #9120:3 ^property[0].code = #parent 
* #9120:3 ^property[0].valueCode = #912-916 
* #9121:0 "Kavernöses Hämangiom"
* #9121:0 ^property[0].code = #parent 
* #9121:0 ^property[0].valueCode = #912-916 
* #9122:0 "Venöses Hämangiom"
* #9122:0 ^property[0].code = #parent 
* #9122:0 ^property[0].valueCode = #912-916 
* #9123:0 "Haemangioma racemosum"
* #9123:0 ^designation[0].value = "Arteriovenöses Hämangiom" 
* #9123:0 ^property[0].code = #parent 
* #9123:0 ^property[0].valueCode = #912-916 
* #9124:3 "Kupffer-Zell-Sarkom"
* #9124:3 ^property[0].code = #parent 
* #9124:3 ^property[0].valueCode = #912-916 
* #9125:0 "Epitheloides Hämangiom"
* #9125:0 ^designation[0].value = "Histiozytoides Hämangiom" 
* #9125:0 ^designation[1].value = "Kutaner epitheloider angiomatöser Knoten" 
* #9125:0 ^property[0].code = #parent 
* #9125:0 ^property[0].valueCode = #912-916 
* #9126:0 "Atypische vaskuläre Läsion"
* #9126:0 ^property[0].code = #parent 
* #9126:0 ^property[0].valueCode = #912-916 
* #9130:0 "Benignes Hämangioendotheliom"
* #9130:0 ^property[0].code = #parent 
* #9130:0 ^property[0].valueCode = #912-916 
* #9130:1 "Hämangioendotheliom o.n.A."
* #9130:1 ^designation[0].value = "Angioendotheliom" 
* #9130:1 ^designation[1].value = "Kaposiformes Hämangioendotheliom" 
* #9130:1 ^property[0].code = #parent 
* #9130:1 ^property[0].valueCode = #912-916 
* #9130:3 "Malignes Hämangioendotheliom"
* #9130:3 ^designation[0].value = "Hämangioendotheliales Sarkom" 
* #9130:3 ^property[0].code = #parent 
* #9130:3 ^property[0].valueCode = #912-916 
* #9131:0 "Kapilläres Hämangiom"
* #9131:0 ^designation[0].value = "Haemangioma simplex" 
* #9131:0 ^designation[1].value = "Infantiles Hämangiom" 
* #9131:0 ^designation[2].value = "Juveniles Hämangiom" 
* #9131:0 ^designation[3].value = "Plexiformes Hämangiom" 
* #9131:0 ^designation[4].value = "Kongenitales Hämangiom mit rascher RückbildungRICH (rapidly involuting congenital hemangioma)" 
* #9131:0 ^designation[5].value = "Kongenitales Hämangiom ohne RückbildungNICH (non-involuting congenital hemangioma)" 
* #9131:0 ^designation[6].value = "Kongenitales Hämangiom o.n.A." 
* #9131:0 ^designation[7].value = "Lobuläres kapilläres Hämangiom" 
* #9131:0 ^property[0].code = #parent 
* #9131:0 ^property[0].valueCode = #912-916 
* #9132:0 "Intramuskuläres Hämangiom"
* #9132:0 ^designation[0].value = "Intramuskuläres Angiom" 
* #9132:0 ^property[0].code = #parent 
* #9132:0 ^property[0].valueCode = #912-916 
* #9133:3 "Epitheloides Hämangioendotheliom o.n.A."
* #9133:3 ^designation[0].value = "Epitheloides malignes Hämangioendotheliom" 
* #9133:3 ^designation[1].value = "Intravaskulärer alveolärer Bronchialtumor" 
* #9133:3 ^property[0].code = #parent 
* #9133:3 ^property[0].valueCode = #912-916 
* #9135:1 "Papilläres intralymphatisches Angioendotheliom"
* #9135:1 ^designation[0].value = "Dabska-Tumor" 
* #9135:1 ^designation[1].value = "Endovaskuläres papilläres Angioendotheliom" 
* #9135:1 ^property[0].code = #parent 
* #9135:1 ^property[0].valueCode = #912-916 
* #9136:1 "Spindelzelliges Hämangioendotheliom"
* #9136:1 ^designation[0].value = "Spindelzelliges Angioendotheliom" 
* #9136:1 ^designation[1].value = "Kombiniertes Hämangioendotheliom" 
* #9136:1 ^designation[2].value = "Retiformes Hämangioendotheliom" 
* #9136:1 ^property[0].code = #parent 
* #9136:1 ^property[0].valueCode = #912-916 
* #9137:0 "Myointimom"
* #9137:0 ^property[0].code = #parent 
* #9137:0 ^property[0].valueCode = #912-916 
* #9137:3 "Intimasarkom"
* #9137:3 ^property[0].code = #parent 
* #9137:3 ^property[0].valueCode = #912-916 
* #9138:1 "Pseudomyogenes (epithelioides sarkomähnliches) Hämangioendotheliom"
* #9138:1 ^property[0].code = #parent 
* #9138:1 ^property[0].valueCode = #912-916 
* #9140:3 "Kaposi-Sarkom"
* #9140:3 ^designation[0].value = "Multiples hämorrhagisches Sarkom" 
* #9140:3 ^property[0].code = #parent 
* #9140:3 ^property[0].valueCode = #912-916 
* #9141:0 "Angiokeratom"
* #9141:0 ^property[0].code = #parent 
* #9141:0 ^property[0].valueCode = #912-916 
* #9142:0 "Verruköses keratotisches Hämangiom"
* #9142:0 ^designation[0].value = "Verruköse venöse Malformation" 
* #9142:0 ^property[0].code = #parent 
* #9142:0 ^property[0].valueCode = #912-916 
* #9160:0 "Angiofibrom o.n.A."
* #9160:0 ^designation[0].value = "Fibröse Nasenpapel" 
* #9160:0 ^designation[1].value = "Involutierter Nävus" 
* #9160:0 ^designation[2].value = "Juveniles Angiofibrom" 
* #9160:0 ^designation[3].value = "Riesenzellangiofibrom" 
* #9160:0 ^designation[4].value = "Zellreiches Angiofibrom" 
* #9160:0 ^property[0].code = #parent 
* #9160:0 ^property[0].valueCode = #912-916 
* #9161:0 "Erworbenes büscheliges Hämangiom"
* #9161:0 ^designation[0].value = "Acquired tufted hemangioma" 
* #9161:0 ^property[0].code = #parent 
* #9161:0 ^property[0].valueCode = #912-916 
* #9161:1 "Hämangioblastom"
* #9161:1 ^designation[0].value = "Angioblastom" 
* #9161:1 ^property[0].code = #parent 
* #9161:1 ^property[0].valueCode = #912-916 
* #917-917 "Neoplasien der Lymphgefäße"
* #917-917 ^property[0].code = #parent 
* #917-917 ^property[0].valueCode = #M 
* #917-917 ^property[1].code = #child 
* #917-917 ^property[1].valueCode = #9170:0 
* #917-917 ^property[2].code = #child 
* #917-917 ^property[2].valueCode = #9170:3 
* #917-917 ^property[3].code = #child 
* #917-917 ^property[3].valueCode = #9171:0 
* #917-917 ^property[4].code = #child 
* #917-917 ^property[4].valueCode = #9172:0 
* #917-917 ^property[5].code = #child 
* #917-917 ^property[5].valueCode = #9173:0 
* #917-917 ^property[6].code = #child 
* #917-917 ^property[6].valueCode = #9174:0 
* #917-917 ^property[7].code = #child 
* #917-917 ^property[7].valueCode = #9174:1 
* #917-917 ^property[8].code = #child 
* #917-917 ^property[8].valueCode = #9175:0 
* #9170:0 "Lymphangiom o.n.A."
* #9170:0 ^designation[0].value = "Lymphangioendotheliom o.n.A." 
* #9170:0 ^property[0].code = #parent 
* #9170:0 ^property[0].valueCode = #917-917 
* #9170:3 "Lymphangiosarkom"
* #9170:3 ^designation[0].value = "Lymphangioendotheliales Sarkom" 
* #9170:3 ^designation[1].value = "Malignes Lymphangioendotheliom" 
* #9170:3 ^property[0].code = #parent 
* #9170:3 ^property[0].valueCode = #917-917 
* #9171:0 "Kapilläres Lymphangiom"
* #9171:0 ^property[0].code = #parent 
* #9171:0 ^property[0].valueCode = #917-917 
* #9172:0 "Kavernöses Lymphangiom"
* #9172:0 ^property[0].code = #parent 
* #9172:0 ^property[0].valueCode = #917-917 
* #9173:0 "Zystisches Lymphangiom"
* #9173:0 ^designation[0].value = "Hygrom o.n.A." 
* #9173:0 ^designation[1].value = "Zystisches Hygrom" 
* #9173:0 ^property[0].code = #parent 
* #9173:0 ^property[0].valueCode = #917-917 
* #9174:0 "Lymphangiomyom"
* #9174:0 ^property[0].code = #parent 
* #9174:0 ^property[0].valueCode = #917-917 
* #9174:1 "Lymphangioleiomyomatose"
* #9174:1 ^designation[0].value = "Lymphangiomyomatose" 
* #9174:1 ^property[0].code = #parent 
* #9174:1 ^property[0].valueCode = #917-917 
* #9175:0 "Hämolymphangiom"
* #9175:0 ^property[0].code = #parent 
* #9175:0 ^property[0].valueCode = #917-917 
* #918-924 "Ossäre und chondromatöse Neoplasien"
* #918-924 ^property[0].code = #parent 
* #918-924 ^property[0].valueCode = #M 
* #918-924 ^property[1].code = #child 
* #918-924 ^property[1].valueCode = #9180:0 
* #918-924 ^property[2].code = #child 
* #918-924 ^property[2].valueCode = #9180:3 
* #918-924 ^property[3].code = #child 
* #918-924 ^property[3].valueCode = #9181:3 
* #918-924 ^property[4].code = #child 
* #918-924 ^property[4].valueCode = #9182:3 
* #918-924 ^property[5].code = #child 
* #918-924 ^property[5].valueCode = #9183:3 
* #918-924 ^property[6].code = #child 
* #918-924 ^property[6].valueCode = #9184:3 
* #918-924 ^property[7].code = #child 
* #918-924 ^property[7].valueCode = #9185:3 
* #918-924 ^property[8].code = #child 
* #918-924 ^property[8].valueCode = #9186:3 
* #918-924 ^property[9].code = #child 
* #918-924 ^property[9].valueCode = #9187:3 
* #918-924 ^property[10].code = #child 
* #918-924 ^property[10].valueCode = #9191:0 
* #918-924 ^property[11].code = #child 
* #918-924 ^property[11].valueCode = #9192:3 
* #918-924 ^property[12].code = #child 
* #918-924 ^property[12].valueCode = #9193:3 
* #918-924 ^property[13].code = #child 
* #918-924 ^property[13].valueCode = #9194:3 
* #918-924 ^property[14].code = #child 
* #918-924 ^property[14].valueCode = #9195:3 
* #918-924 ^property[15].code = #child 
* #918-924 ^property[15].valueCode = #9200:0 
* #918-924 ^property[16].code = #child 
* #918-924 ^property[16].valueCode = #9200:1 
* #918-924 ^property[17].code = #child 
* #918-924 ^property[17].valueCode = #9210:0 
* #918-924 ^property[18].code = #child 
* #918-924 ^property[18].valueCode = #9210:1 
* #918-924 ^property[19].code = #child 
* #918-924 ^property[19].valueCode = #9211:0 
* #918-924 ^property[20].code = #child 
* #918-924 ^property[20].valueCode = #9212:0 
* #918-924 ^property[21].code = #child 
* #918-924 ^property[21].valueCode = #9213:0 
* #918-924 ^property[22].code = #child 
* #918-924 ^property[22].valueCode = #9220:0 
* #918-924 ^property[23].code = #child 
* #918-924 ^property[23].valueCode = #9220:1 
* #918-924 ^property[24].code = #child 
* #918-924 ^property[24].valueCode = #9220:3 
* #918-924 ^property[25].code = #child 
* #918-924 ^property[25].valueCode = #9221:0 
* #918-924 ^property[26].code = #child 
* #918-924 ^property[26].valueCode = #9221:3 
* #918-924 ^property[27].code = #child 
* #918-924 ^property[27].valueCode = #9222:1 
* #918-924 ^property[28].code = #child 
* #918-924 ^property[28].valueCode = #9230:1 
* #918-924 ^property[29].code = #child 
* #918-924 ^property[29].valueCode = #9230:3 
* #918-924 ^property[30].code = #child 
* #918-924 ^property[30].valueCode = #9231:3 
* #918-924 ^property[31].code = #child 
* #918-924 ^property[31].valueCode = #9240:3 
* #918-924 ^property[32].code = #child 
* #918-924 ^property[32].valueCode = #9241:0 
* #918-924 ^property[33].code = #child 
* #918-924 ^property[33].valueCode = #9242:3 
* #918-924 ^property[34].code = #child 
* #918-924 ^property[34].valueCode = #9243:3 
* #9180:0 "Osteom o.n.A."
* #9180:0 ^property[0].code = #parent 
* #9180:0 ^property[0].valueCode = #918-924 
* #9180:3 "Osteosarkom o.n.A."
* #9180:3 ^designation[0].value = "Osteoblastisches Sarkom" 
* #9180:3 ^designation[1].value = "Osteochondrosarkom" 
* #9180:3 ^designation[2].value = "Osteogenes Sarkom o.n.A." 
* #9180:3 ^designation[3].value = "Extraskelettales Osteosarkom" 
* #9180:3 ^property[0].code = #parent 
* #9180:3 ^property[0].valueCode = #918-924 
* #9181:3 "Chondroblastisches Osteosarkom"
* #9181:3 ^property[0].code = #parent 
* #9181:3 ^property[0].valueCode = #918-924 
* #9182:3 "Fibroblastisches Osteosarkom"
* #9182:3 ^designation[0].value = "Osteofibrosarkom" 
* #9182:3 ^property[0].code = #parent 
* #9182:3 ^property[0].valueCode = #918-924 
* #9183:3 "Teleangiektatisches Osteosarkom"
* #9183:3 ^property[0].code = #parent 
* #9183:3 ^property[0].valueCode = #918-924 
* #9184:3 "Osteosarkom in Paget-Knochenkrankheit"
* #9184:3 ^designation[0].value = "Sekundäres Osteosarkom" 
* #9184:3 ^property[0].code = #parent 
* #9184:3 ^property[0].valueCode = #918-924 
* #9185:3 "Kleinzelliges Osteosarkom"
* #9185:3 ^designation[0].value = "Rundzell-Osteosarkom" 
* #9185:3 ^property[0].code = #parent 
* #9185:3 ^property[0].valueCode = #918-924 
* #9186:3 "Zentrales Osteosarkom o.n.A."
* #9186:3 ^designation[0].value = "Konventionelles zentrales Osteosarkom" 
* #9186:3 ^designation[1].value = "Medulläres Osteosarkom" 
* #9186:3 ^property[0].code = #parent 
* #9186:3 ^property[0].valueCode = #918-924 
* #9187:3 "Niedrigmalignes zentrales Osteosarkom"
* #9187:3 ^designation[0].value = "Niedrigmalignes intramedulläres Osteosarkom" 
* #9187:3 ^designation[1].value = "Intraossäres gut differenziertes OsteosarkomIntraossäres Low-grade-Osteosarkom" 
* #9187:3 ^property[0].code = #parent 
* #9187:3 ^property[0].valueCode = #918-924 
* #9191:0 "Osteoidosteom o.n.A"
* #9191:0 ^property[0].code = #parent 
* #9191:0 ^property[0].valueCode = #918-924 
* #9192:3 "Parossales Osteosarkom"
* #9192:3 ^designation[0].value = "Juxtakortikales Osteosarkom" 
* #9192:3 ^property[0].code = #parent 
* #9192:3 ^property[0].valueCode = #918-924 
* #9193:3 "Periostales Osteosarkom"
* #9193:3 ^property[0].code = #parent 
* #9193:3 ^property[0].valueCode = #918-924 
* #9194:3 "Hochmalignes [High-grade] Oberflächen-Osteosarkom"
* #9194:3 ^property[0].code = #parent 
* #9194:3 ^property[0].valueCode = #918-924 
* #9195:3 "Intrakortikales Osteosarkom"
* #9195:3 ^property[0].code = #parent 
* #9195:3 ^property[0].valueCode = #918-924 
* #9200:0 "Osteoblastom o.n.A."
* #9200:0 ^designation[0].value = "Riesen-Osteoidosteom" 
* #9200:0 ^property[0].code = #parent 
* #9200:0 ^property[0].valueCode = #918-924 
* #9200:1 "Aggressives Osteoblastom"
* #9200:1 ^property[0].code = #parent 
* #9200:1 ^property[0].valueCode = #918-924 
* #9210:0 "Osteochondrom"
* #9210:0 ^designation[0].value = "Ekchondrom" 
* #9210:0 ^designation[1].value = "Kartilaginäre Exostose" 
* #9210:0 ^designation[2].value = "Osteokartilaginäre Exostose" 
* #9210:0 ^property[0].code = #parent 
* #9210:0 ^property[0].valueCode = #918-924 
* #9210:1 "Osteochondromatose o.n.A."
* #9210:1 ^designation[0].value = "Ekchondromatose" 
* #9210:1 ^property[0].code = #parent 
* #9210:1 ^property[0].valueCode = #918-924 
* #9211:0 "Osteochondromyxom"
* #9211:0 ^property[0].code = #parent 
* #9211:0 ^property[0].valueCode = #918-924 
* #9212:0 "Bizarre parostale osteochondromatöse Proliferation"
* #9212:0 ^property[0].code = #parent 
* #9212:0 ^property[0].valueCode = #918-924 
* #9213:0 "Subunguale Exostose"
* #9213:0 ^property[0].code = #parent 
* #9213:0 ^property[0].valueCode = #918-924 
* #9220:0 "Chondrom o.n.A."
* #9220:0 ^designation[0].value = "Enchondrom" 
* #9220:0 ^property[0].code = #parent 
* #9220:0 ^property[0].valueCode = #918-924 
* #9220:1 "Chondromatose o.n.A."
* #9220:1 ^property[0].code = #parent 
* #9220:1 ^property[0].valueCode = #918-924 
* #9220:3 "Chondrosarkom o.n.A."
* #9220:3 ^designation[0].value = "Chondrosarkom, Grad 2" 
* #9220:3 ^designation[1].value = "Chondrosarkom, Grad 3" 
* #9220:3 ^designation[2].value = "Fibrochondrosarkom" 
* #9220:3 ^property[0].code = #parent 
* #9220:3 ^property[0].valueCode = #918-924 
* #9221:0 "Periostales Chondrom"
* #9221:0 ^designation[0].value = "Juxtakortikales Chondrom" 
* #9221:0 ^property[0].code = #parent 
* #9221:0 ^property[0].valueCode = #918-924 
* #9221:3 "Periostales Chondrosarkom"
* #9221:3 ^designation[0].value = "Juxtakortikales Chondrosarkom" 
* #9221:3 ^property[0].code = #parent 
* #9221:3 ^property[0].valueCode = #918-924 
* #9222:1 "Atypischer kartiaginärer Tumor"
* #9222:1 ^designation[0].value = "Chondrosarkom, Grad 1" 
* #9222:1 ^property[0].code = #parent 
* #9222:1 ^property[0].valueCode = #918-924 
* #9230:1 "Chondroblastom o.n.A."
* #9230:1 ^designation[0].value = "Chondromatöser Riesenzelltumor" 
* #9230:1 ^designation[1].value = "Codman-Tumor" 
* #9230:1 ^property[0].code = #parent 
* #9230:1 ^property[0].valueCode = #918-924 
* #9230:3 "Malignes Chondroblastom"
* #9230:3 ^property[0].code = #parent 
* #9230:3 ^property[0].valueCode = #918-924 
* #9231:3 "Myxoides Chondrosarkom"
* #9231:3 ^property[0].code = #parent 
* #9231:3 ^property[0].valueCode = #918-924 
* #9240:3 "Mesenchymales Chondrosarkom"
* #9240:3 ^property[0].code = #parent 
* #9240:3 ^property[0].valueCode = #918-924 
* #9241:0 "Chondromyxoides Fibrom"
* #9241:0 ^property[0].code = #parent 
* #9241:0 ^property[0].valueCode = #918-924 
* #9242:3 "Klarzell-Chondrosarkom"
* #9242:3 ^property[0].code = #parent 
* #9242:3 ^property[0].valueCode = #918-924 
* #9243:3 "Entdifferenziertes Chondrosarkom"
* #9243:3 ^property[0].code = #parent 
* #9243:3 ^property[0].valueCode = #918-924 
* #925-925 "Riesenzellneoplasien"
* #925-925 ^property[0].code = #parent 
* #925-925 ^property[0].valueCode = #M 
* #925-925 ^property[1].code = #child 
* #925-925 ^property[1].valueCode = #9250:1 
* #925-925 ^property[2].code = #child 
* #925-925 ^property[2].valueCode = #9250:3 
* #925-925 ^property[3].code = #child 
* #925-925 ^property[3].valueCode = #9251:1 
* #925-925 ^property[4].code = #child 
* #925-925 ^property[4].valueCode = #9251:3 
* #925-925 ^property[5].code = #child 
* #925-925 ^property[5].valueCode = #9252:0 
* #925-925 ^property[6].code = #child 
* #925-925 ^property[6].valueCode = #9252:1 
* #925-925 ^property[7].code = #child 
* #925-925 ^property[7].valueCode = #9252:3 
* #9250:1 "Riesenzelltumor des Knochens o.n.A."
* #9250:1 ^designation[0].value = "Osteoklastom o.n.A." 
* #9250:1 ^property[0].code = #parent 
* #9250:1 ^property[0].valueCode = #925-925 
* #9250:3 "Maligner Riesenzelltumor des Knochens"
* #9250:3 ^designation[0].value = "Malignes Osteoklastom" 
* #9250:3 ^designation[1].value = "Riesenzellsarkom des Knochens" 
* #9250:3 ^property[0].code = #parent 
* #9250:3 ^property[0].valueCode = #925-925 
* #9251:1 "Riesenzelltumor der Weichteile o.n.A."
* #9251:1 ^property[0].code = #parent 
* #9251:1 ^property[0].valueCode = #925-925 
* #9251:3 "Maligner Riesenzelltumor der Weichteile"
* #9251:3 ^property[0].code = #parent 
* #9251:3 ^property[0].valueCode = #925-925 
* #9252:0 "Lokalisierter tendosynovialer Riesenzelltumor"
* #9252:0 ^designation[0].value = "Fibröses Histiozytom der Sehnenscheide" 
* #9252:0 ^designation[1].value = "Riesenzelltumor der Sehnenscheide" 
* #9252:0 ^designation[2].value = "Tendosynovialer Riesenzelltumor o.n.A." 
* #9252:0 ^property[0].code = #parent 
* #9252:0 ^property[0].valueCode = #925-925 
* #9252:1 "Tenosynovialer Riesenzelltumor der diffusen Form"
* #9252:1 ^designation[0].value = "Pigmentierte villonoduläre Synovitis" 
* #9252:1 ^property[0].code = #parent 
* #9252:1 ^property[0].valueCode = #925-925 
* #9252:3 "Maligner tendosynovialer Riesenzelltumor"
* #9252:3 ^designation[0].value = "Maligner Riesenzelltumor der Sehnenscheide" 
* #9252:3 ^property[0].code = #parent 
* #9252:3 ^property[0].valueCode = #925-925 
* #926-926 "Sonstige Neoplasien des Knochens"
* #926-926 ^property[0].code = #parent 
* #926-926 ^property[0].valueCode = #M 
* #926-926 ^property[1].code = #child 
* #926-926 ^property[1].valueCode = #9260:0 
* #926-926 ^property[2].code = #child 
* #926-926 ^property[2].valueCode = #9261:3 
* #926-926 ^property[3].code = #child 
* #926-926 ^property[3].valueCode = #9262:0 
* #9260:0 "Aneurysmatische Knochenzyste"
* #9260:0 ^property[0].code = #parent 
* #9260:0 ^property[0].valueCode = #926-926 
* #9261:3 "Adamantinom der langen Röhrenknochen"
* #9261:3 ^designation[0].value = "Adamantinom der Tibia" 
* #9261:3 ^property[0].code = #parent 
* #9261:3 ^property[0].valueCode = #926-926 
* #9262:0 "Ossifizierendes Fibrom"
* #9262:0 ^designation[0].value = "Fibro-Osteom" 
* #9262:0 ^designation[1].value = "Osteofibrom" 
* #9262:0 ^property[0].code = #parent 
* #9262:0 ^property[0].valueCode = #926-926 
* #927-934 "Odontogene Neoplasien"
* #927-934 ^property[0].code = #parent 
* #927-934 ^property[0].valueCode = #M 
* #927-934 ^property[1].code = #child 
* #927-934 ^property[1].valueCode = #9270:0 
* #927-934 ^property[2].code = #child 
* #927-934 ^property[2].valueCode = #9270:1 
* #927-934 ^property[3].code = #child 
* #927-934 ^property[3].valueCode = #9270:3 
* #927-934 ^property[4].code = #child 
* #927-934 ^property[4].valueCode = #9271:0 
* #927-934 ^property[5].code = #child 
* #927-934 ^property[5].valueCode = #9272:0 
* #927-934 ^property[6].code = #child 
* #927-934 ^property[6].valueCode = #9273:0 
* #927-934 ^property[7].code = #child 
* #927-934 ^property[7].valueCode = #9274:0 
* #927-934 ^property[8].code = #child 
* #927-934 ^property[8].valueCode = #9275:0 
* #927-934 ^property[9].code = #child 
* #927-934 ^property[9].valueCode = #9280:0 
* #927-934 ^property[10].code = #child 
* #927-934 ^property[10].valueCode = #9281:0 
* #927-934 ^property[11].code = #child 
* #927-934 ^property[11].valueCode = #9282:0 
* #927-934 ^property[12].code = #child 
* #927-934 ^property[12].valueCode = #9290:0 
* #927-934 ^property[13].code = #child 
* #927-934 ^property[13].valueCode = #9290:3 
* #927-934 ^property[14].code = #child 
* #927-934 ^property[14].valueCode = #9300:0 
* #927-934 ^property[15].code = #child 
* #927-934 ^property[15].valueCode = #9301:0 
* #927-934 ^property[16].code = #child 
* #927-934 ^property[16].valueCode = #9302:0 
* #927-934 ^property[17].code = #child 
* #927-934 ^property[17].valueCode = #9302:3 
* #927-934 ^property[18].code = #child 
* #927-934 ^property[18].valueCode = #9310:0 
* #927-934 ^property[19].code = #child 
* #927-934 ^property[19].valueCode = #9310:3 
* #927-934 ^property[20].code = #child 
* #927-934 ^property[20].valueCode = #9311:0 
* #927-934 ^property[21].code = #child 
* #927-934 ^property[21].valueCode = #9312:0 
* #927-934 ^property[22].code = #child 
* #927-934 ^property[22].valueCode = #9320:0 
* #927-934 ^property[23].code = #child 
* #927-934 ^property[23].valueCode = #9321:0 
* #927-934 ^property[24].code = #child 
* #927-934 ^property[24].valueCode = #9322:0 
* #927-934 ^property[25].code = #child 
* #927-934 ^property[25].valueCode = #9330:0 
* #927-934 ^property[26].code = #child 
* #927-934 ^property[26].valueCode = #9330:3 
* #927-934 ^property[27].code = #child 
* #927-934 ^property[27].valueCode = #9340:0 
* #927-934 ^property[28].code = #child 
* #927-934 ^property[28].valueCode = #9341:3 
* #927-934 ^property[29].code = #child 
* #927-934 ^property[29].valueCode = #9342:3 
* #9270:0 "Benigner odontogener Tumor"
* #9270:0 ^property[0].code = #parent 
* #9270:0 ^property[0].valueCode = #927-934 
* #9270:1 "Odontogener Tumor o.n.A."
* #9270:1 ^property[0].code = #parent 
* #9270:1 ^property[0].valueCode = #927-934 
* #9270:3 "Maligner odontogener Tumor"
* #9270:3 ^designation[0].value = "Ameloblastisches Karzinom" 
* #9270:3 ^designation[1].value = "Odontogenes Karzinom" 
* #9270:3 ^designation[2].value = "Odontogenes Sarkom" 
* #9270:3 ^designation[3].value = "Primäres intraossäres Karzinom" 
* #9270:3 ^property[0].code = #parent 
* #9270:3 ^property[0].valueCode = #927-934 
* #9271:0 "Ameloblastisches Fibrodentinom"
* #9271:0 ^designation[0].value = "Dentinom" 
* #9271:0 ^property[0].code = #parent 
* #9271:0 ^property[0].valueCode = #927-934 
* #9272:0 "Zementom o.n.A."
* #9272:0 ^designation[0].value = "Periapikale zementale Dysplasie" 
* #9272:0 ^designation[1].value = "Periapikale zemento-ossäre Dysplasie" 
* #9272:0 ^property[0].code = #parent 
* #9272:0 ^property[0].valueCode = #927-934 
* #9273:0 "Benignes Zementoblastom"
* #9273:0 ^property[0].code = #parent 
* #9273:0 ^property[0].valueCode = #927-934 
* #9274:0 "Zemento-ossifizierendes Fibrom"
* #9274:0 ^designation[0].value = "Zementbildendes Fibrom" 
* #9274:0 ^property[0].code = #parent 
* #9274:0 ^property[0].valueCode = #927-934 
* #9275:0 "Riesenzementom"
* #9275:0 ^designation[0].value = "Floride ossäre Dysplasie" 
* #9275:0 ^property[0].code = #parent 
* #9275:0 ^property[0].valueCode = #927-934 
* #9280:0 "Odontom o.n.A."
* #9280:0 ^property[0].code = #parent 
* #9280:0 ^property[0].valueCode = #927-934 
* #9281:0 "Compound-Odontom"
* #9281:0 ^property[0].code = #parent 
* #9281:0 ^property[0].valueCode = #927-934 
* #9282:0 "Komplexes Odontom"
* #9282:0 ^property[0].code = #parent 
* #9282:0 ^property[0].valueCode = #927-934 
* #9290:0 "Ameloblastisches Fibro-Odontom"
* #9290:0 ^designation[0].value = "Fibro-ameloblastisches Odontom" 
* #9290:0 ^property[0].code = #parent 
* #9290:0 ^property[0].valueCode = #927-934 
* #9290:3 "Ameloblastisches Odontosarkom"
* #9290:3 ^designation[0].value = "Ameloblastisches Fibrodentinosarkom" 
* #9290:3 ^designation[1].value = "Ameloblastisches Fibro-Odontosarkom" 
* #9290:3 ^property[0].code = #parent 
* #9290:3 ^property[0].valueCode = #927-934 
* #9300:0 "Adenomatoider odontogener Tumor"
* #9300:0 ^designation[0].value = "Adenoameloblastom" 
* #9300:0 ^property[0].code = #parent 
* #9300:0 ^property[0].valueCode = #927-934 
* #9301:0 "Kalzifizierende odontogene Zyste"
* #9301:0 ^property[0].code = #parent 
* #9301:0 ^property[0].valueCode = #927-934 
* #9302:0 "Dentinogener Schattenzelltumor"
* #9302:0 ^designation[0].value = "Odontogener Schattenzelltumor" 
* #9302:0 ^property[0].code = #parent 
* #9302:0 ^property[0].valueCode = #927-934 
* #9302:3 "Odontogenes Schattenzellkarzinom"
* #9302:3 ^property[0].code = #parent 
* #9302:3 ^property[0].valueCode = #927-934 
* #9310:0 "Ameloblastom o.n.A."
* #9310:0 ^designation[0].value = "Adamantinom o.n.A." 
* #9310:0 ^property[0].code = #parent 
* #9310:0 ^property[0].valueCode = #927-934 
* #9310:3 "Metastasierendes Ameloblastom"
* #9310:3 ^designation[0].value = "Malignes Adamantinom" 
* #9310:3 ^designation[1].value = "Malignes Ameloblastom" 
* #9310:3 ^property[0].code = #parent 
* #9310:3 ^property[0].valueCode = #927-934 
* #9311:0 "Odontoameloblastom"
* #9311:0 ^property[0].code = #parent 
* #9311:0 ^property[0].valueCode = #927-934 
* #9312:0 "Odontogener Plattenepitheltumor"
* #9312:0 ^property[0].code = #parent 
* #9312:0 ^property[0].valueCode = #927-934 
* #9320:0 "Odontogenes Myxom"
* #9320:0 ^designation[0].value = "Odontogenes Myxofibrom" 
* #9320:0 ^property[0].code = #parent 
* #9320:0 ^property[0].valueCode = #927-934 
* #9321:0 "Odontogenes Fibrom o.n.A."
* #9321:0 ^designation[0].value = "Zentrales odontogenes Fibrom" 
* #9321:0 ^property[0].code = #parent 
* #9321:0 ^property[0].valueCode = #927-934 
* #9322:0 "Peripheres odontogenes Fibrom"
* #9322:0 ^property[0].code = #parent 
* #9322:0 ^property[0].valueCode = #927-934 
* #9330:0 "Ameloblastisches Fibrom"
* #9330:0 ^property[0].code = #parent 
* #9330:0 ^property[0].valueCode = #927-934 
* #9330:3 "Ameloblastisches Fibrosarkom"
* #9330:3 ^designation[0].value = "Ameloblastisches Sarkom" 
* #9330:3 ^designation[1].value = "Odontogenes Fibrosarkom" 
* #9330:3 ^property[0].code = #parent 
* #9330:3 ^property[0].valueCode = #927-934 
* #9340:0 "Kalzifizierender epithelialer odontogener Tumor"
* #9340:0 ^designation[0].value = "Pindborg-Tumor" 
* #9340:0 ^property[0].code = #parent 
* #9340:0 ^property[0].valueCode = #927-934 
* #9341:3 "Klarzelliges odontogenes Karzinom"
* #9341:3 ^designation[0].value = "Odontogener Klarzelltumor" 
* #9341:3 ^property[0].code = #parent 
* #9341:3 ^property[0].valueCode = #927-934 
* #9342:3 "Odontogenes Karzinosarkom"
* #9342:3 ^property[0].code = #parent 
* #9342:3 ^property[0].valueCode = #927-934 
* #935-937 "Sonstige Neoplasien"
* #935-937 ^property[0].code = #parent 
* #935-937 ^property[0].valueCode = #M 
* #935-937 ^property[1].code = #child 
* #935-937 ^property[1].valueCode = #9350:1 
* #935-937 ^property[2].code = #child 
* #935-937 ^property[2].valueCode = #9351:1 
* #935-937 ^property[3].code = #child 
* #935-937 ^property[3].valueCode = #9352:1 
* #935-937 ^property[4].code = #child 
* #935-937 ^property[4].valueCode = #9360:1 
* #935-937 ^property[5].code = #child 
* #935-937 ^property[5].valueCode = #9361:1 
* #935-937 ^property[6].code = #child 
* #935-937 ^property[6].valueCode = #9362:3 
* #935-937 ^property[7].code = #child 
* #935-937 ^property[7].valueCode = #9363:0 
* #935-937 ^property[8].code = #child 
* #935-937 ^property[8].valueCode = #9364:3 
* #935-937 ^property[9].code = #child 
* #935-937 ^property[9].valueCode = #9365:3 
* #935-937 ^property[10].code = #child 
* #935-937 ^property[10].valueCode = #9370:0 
* #935-937 ^property[11].code = #child 
* #935-937 ^property[11].valueCode = #9370:3 
* #935-937 ^property[12].code = #child 
* #935-937 ^property[12].valueCode = #9371:3 
* #935-937 ^property[13].code = #child 
* #935-937 ^property[13].valueCode = #9372:3 
* #935-937 ^property[14].code = #child 
* #935-937 ^property[14].valueCode = #9373:0 
* #9350:1 "Kraniopharyngeom"
* #9350:1 ^designation[0].value = "Rathke-Taschen-Tumor" 
* #9350:1 ^property[0].code = #parent 
* #9350:1 ^property[0].valueCode = #935-937 
* #9351:1 "Adamantinöses Kraniopharyngeom"
* #9351:1 ^property[0].code = #parent 
* #9351:1 ^property[0].valueCode = #935-937 
* #9352:1 "Papilläres Kraniopharyngeom"
* #9352:1 ^property[0].code = #parent 
* #9352:1 ^property[0].valueCode = #935-937 
* #9360:1 "Pinealom"
* #9360:1 ^property[0].code = #parent 
* #9360:1 ^property[0].valueCode = #935-937 
* #9361:1 "Pineozytom"
* #9361:1 ^property[0].code = #parent 
* #9361:1 ^property[0].valueCode = #935-937 
* #9362:3 "Pineoblastom"
* #9362:3 ^designation[0].value = "Pineal-Tumor vom Übergangstyp" 
* #9362:3 ^designation[1].value = "Pinealer MischtumorPineozytärer und Pineoblastärer Mischtumor" 
* #9362:3 ^designation[2].value = "Pinealis-Tumor mit intermediärer Differenzierung" 
* #9362:3 ^property[0].code = #parent 
* #9362:3 ^property[0].valueCode = #935-937 
* #9363:0 "Pigmentierter neuroektodermaler Tumor"
* #9363:0 ^designation[0].value = "Melanoameloblastom" 
* #9363:0 ^designation[1].value = "Melanotisches Progonom" 
* #9363:0 ^designation[2].value = "Retinalanlage-Tumor" 
* #9363:0 ^property[0].code = #parent 
* #9363:0 ^property[0].valueCode = #935-937 
* #9364:3 "Ewing-Sarkom"
* #9364:3 ^designation[0].value = "Ewing-Tumor" 
* #9364:3 ^designation[1].value = "Peripherer neuroektodermaler Tumor" 
* #9364:3 ^designation[2].value = "Neuroektodermaler Tumor o.n.A." 
* #9364:3 ^designation[3].value = "Peripherer primitiver neuroektodermaler TumorPPNET" 
* #9364:3 ^property[0].code = #parent 
* #9364:3 ^property[0].valueCode = #935-937 
* #9365:3 "Askin-Tumor"
* #9365:3 ^property[0].code = #parent 
* #9365:3 ^property[0].valueCode = #935-937 
* #9370:0 "Gutartiger notochordaler Tumor"
* #9370:0 ^property[0].code = #parent 
* #9370:0 ^property[0].valueCode = #935-937 
* #9370:3 "Chordom o.n.A."
* #9370:3 ^property[0].code = #parent 
* #9370:3 ^property[0].valueCode = #935-937 
* #9371:3 "Chondroides Chordom"
* #9371:3 ^property[0].code = #parent 
* #9371:3 ^property[0].valueCode = #935-937 
* #9372:3 "Entdifferenziertes Chordom"
* #9372:3 ^property[0].code = #parent 
* #9372:3 ^property[0].valueCode = #935-937 
* #9373:0 "Parachordom"
* #9373:0 ^property[0].code = #parent 
* #9373:0 ^property[0].valueCode = #935-937 
* #938-948 "Neoplasien der Glia"
* #938-948 ^property[0].code = #parent 
* #938-948 ^property[0].valueCode = #M 
* #938-948 ^property[1].code = #child 
* #938-948 ^property[1].valueCode = #9380:3 
* #938-948 ^property[2].code = #child 
* #938-948 ^property[2].valueCode = #9381:3 
* #938-948 ^property[3].code = #child 
* #938-948 ^property[3].valueCode = #9382:3 
* #938-948 ^property[4].code = #child 
* #938-948 ^property[4].valueCode = #9383:1 
* #938-948 ^property[5].code = #child 
* #938-948 ^property[5].valueCode = #9384:1 
* #938-948 ^property[6].code = #child 
* #938-948 ^property[6].valueCode = #9385:3 
* #938-948 ^property[7].code = #child 
* #938-948 ^property[7].valueCode = #9390:0 
* #938-948 ^property[8].code = #child 
* #938-948 ^property[8].valueCode = #9390:1 
* #938-948 ^property[9].code = #child 
* #938-948 ^property[9].valueCode = #9390:3 
* #938-948 ^property[10].code = #child 
* #938-948 ^property[10].valueCode = #9391:1 
* #938-948 ^property[11].code = #child 
* #938-948 ^property[11].valueCode = #9391:3 
* #938-948 ^property[12].code = #child 
* #938-948 ^property[12].valueCode = #9392:3 
* #938-948 ^property[13].code = #child 
* #938-948 ^property[13].valueCode = #9393:3 
* #938-948 ^property[14].code = #child 
* #938-948 ^property[14].valueCode = #9394:1 
* #938-948 ^property[15].code = #child 
* #938-948 ^property[15].valueCode = #9395:3 
* #938-948 ^property[16].code = #child 
* #938-948 ^property[16].valueCode = #9396:3 
* #938-948 ^property[17].code = #child 
* #938-948 ^property[17].valueCode = #9400:3 
* #938-948 ^property[18].code = #child 
* #938-948 ^property[18].valueCode = #9401:3 
* #938-948 ^property[19].code = #child 
* #938-948 ^property[19].valueCode = #9410:3 
* #938-948 ^property[20].code = #child 
* #938-948 ^property[20].valueCode = #9411:3 
* #938-948 ^property[21].code = #child 
* #938-948 ^property[21].valueCode = #9412:1 
* #938-948 ^property[22].code = #child 
* #938-948 ^property[22].valueCode = #9413:0 
* #938-948 ^property[23].code = #child 
* #938-948 ^property[23].valueCode = #9420:3 
* #938-948 ^property[24].code = #child 
* #938-948 ^property[24].valueCode = #9421:1 
* #938-948 ^property[25].code = #child 
* #938-948 ^property[25].valueCode = #9423:3 
* #938-948 ^property[26].code = #child 
* #938-948 ^property[26].valueCode = #9424:3 
* #938-948 ^property[27].code = #child 
* #938-948 ^property[27].valueCode = #9425:3 
* #938-948 ^property[28].code = #child 
* #938-948 ^property[28].valueCode = #9430:3 
* #938-948 ^property[29].code = #child 
* #938-948 ^property[29].valueCode = #9431:1 
* #938-948 ^property[30].code = #child 
* #938-948 ^property[30].valueCode = #9432:1 
* #938-948 ^property[31].code = #child 
* #938-948 ^property[31].valueCode = #9440:3 
* #938-948 ^property[32].code = #child 
* #938-948 ^property[32].valueCode = #9441:3 
* #938-948 ^property[33].code = #child 
* #938-948 ^property[33].valueCode = #9442:1 
* #938-948 ^property[34].code = #child 
* #938-948 ^property[34].valueCode = #9442:3 
* #938-948 ^property[35].code = #child 
* #938-948 ^property[35].valueCode = #9444:1 
* #938-948 ^property[36].code = #child 
* #938-948 ^property[36].valueCode = #9445:3 
* #938-948 ^property[37].code = #child 
* #938-948 ^property[37].valueCode = #9450:3 
* #938-948 ^property[38].code = #child 
* #938-948 ^property[38].valueCode = #9451:3 
* #938-948 ^property[39].code = #child 
* #938-948 ^property[39].valueCode = #9460:3 
* #938-948 ^property[40].code = #child 
* #938-948 ^property[40].valueCode = #9470:3 
* #938-948 ^property[41].code = #child 
* #938-948 ^property[41].valueCode = #9471:3 
* #938-948 ^property[42].code = #child 
* #938-948 ^property[42].valueCode = #9472:3 
* #938-948 ^property[43].code = #child 
* #938-948 ^property[43].valueCode = #9473:3 
* #938-948 ^property[44].code = #child 
* #938-948 ^property[44].valueCode = #9474:3 
* #938-948 ^property[45].code = #child 
* #938-948 ^property[45].valueCode = #9475:3 
* #938-948 ^property[46].code = #child 
* #938-948 ^property[46].valueCode = #9476:3 
* #938-948 ^property[47].code = #child 
* #938-948 ^property[47].valueCode = #9477:3 
* #938-948 ^property[48].code = #child 
* #938-948 ^property[48].valueCode = #9478:3 
* #938-948 ^property[49].code = #child 
* #938-948 ^property[49].valueCode = #9480:3 
* #9380:3 "Malignes Gliom"
* #9380:3 ^designation[0].value = "Gliom o.n.A." 
* #9380:3 ^property[0].code = #parent 
* #9380:3 ^property[0].valueCode = #938-948 
* #9381:3 "Gliomatosis cerebri"
* #9381:3 ^property[0].code = #parent 
* #9381:3 ^property[0].valueCode = #938-948 
* #9382:3 "Oligoastrozytom o.n.A."
* #9382:3 ^designation[0].value = "Anaplastisches Oligoastrozytom" 
* #9382:3 ^designation[1].value = "Mischgliom" 
* #9382:3 ^property[0].code = #parent 
* #9382:3 ^property[0].valueCode = #938-948 
* #9383:1 "Subependymom"
* #9383:1 ^designation[0].value = "Subependymales Astrozytom o.n.A." 
* #9383:1 ^designation[1].value = "Subependymales Gliom" 
* #9383:1 ^designation[2].value = "Gemischtes Subependymom und Ependymom" 
* #9383:1 ^property[0].code = #parent 
* #9383:1 ^property[0].valueCode = #938-948 
* #9384:1 "Subependymales Riesenzellastrozytom"
* #9384:1 ^property[0].code = #parent 
* #9384:1 ^property[0].valueCode = #938-948 
* #9385:3 "Diffuses Mittelliniengliom, H3 K27M Mutation"
* #9385:3 ^designation[0].value = "Diffuses intrinsisches Ponsgliom, H3 K27M Mutation" 
* #9385:3 ^property[0].code = #parent 
* #9385:3 ^property[0].valueCode = #938-948 
* #9390:0 "Plexus-chorioideus-Papillom o.n.A."
* #9390:0 ^property[0].code = #parent 
* #9390:0 ^property[0].valueCode = #938-948 
* #9390:1 "Atypisches Plexus-chorioideus-Papillom"
* #9390:1 ^property[0].code = #parent 
* #9390:1 ^property[0].valueCode = #938-948 
* #9390:3 "Plexus-chorioideus-Karzinom"
* #9390:3 ^designation[0].value = "Anaplastisches Plexus-chorioideus-Papillom" 
* #9390:3 ^designation[1].value = "Malignes Papillom des Plexus chorioideus" 
* #9390:3 ^property[0].code = #parent 
* #9390:3 ^property[0].valueCode = #938-948 
* #9391:1 "Selläres Ependymom"
* #9391:1 ^property[0].code = #parent 
* #9391:1 ^property[0].valueCode = #938-948 
* #9391:3 "Ependymom o.n.A."
* #9391:3 ^designation[0].value = "Epitheliales Ependymom" 
* #9391:3 ^designation[1].value = "Klarzelliges Ependymom" 
* #9391:3 ^designation[2].value = "Tanyzytisches Ependymom" 
* #9391:3 ^designation[3].value = "Zellreiches Ependymom" 
* #9391:3 ^property[0].code = #parent 
* #9391:3 ^property[0].valueCode = #938-948 
* #9392:3 "Anaplastisches Ependymom"
* #9392:3 ^designation[0].value = "Ependymoblastom" 
* #9392:3 ^property[0].code = #parent 
* #9392:3 ^property[0].valueCode = #938-948 
* #9393:3 "Papilläres Ependymom"
* #9393:3 ^property[0].code = #parent 
* #9393:3 ^property[0].valueCode = #938-948 
* #9394:1 "Myxopapilläres Ependymom"
* #9394:1 ^property[0].code = #parent 
* #9394:1 ^property[0].valueCode = #938-948 
* #9395:3 "Papillärer Tumor der Pinealisloge"
* #9395:3 ^property[0].code = #parent 
* #9395:3 ^property[0].valueCode = #938-948 
* #9396:3 "Ependymom, RELA-Fusion positiv"
* #9396:3 ^property[0].code = #parent 
* #9396:3 ^property[0].valueCode = #938-948 
* #9400:3 "Astrozytom o.n.A."
* #9400:3 ^designation[0].value = "Astrogliom" 
* #9400:3 ^designation[1].value = "Astrozytisches Gliom" 
* #9400:3 ^designation[2].value = "Astrozytom, Low-Grade" 
* #9400:3 ^designation[3].value = "Diffuses Astrozytom o.n.A." 
* #9400:3 ^designation[4].value = "Diffuses Astrozytom, IDH-Mutation" 
* #9400:3 ^designation[5].value = "Diffuses Astrozytom, IDH-Wildtyp" 
* #9400:3 ^designation[6].value = "Diffuses Astrozytom, Low-Grade" 
* #9400:3 ^designation[7].value = "Zystisches Astrozytom" 
* #9400:3 ^property[0].code = #parent 
* #9400:3 ^property[0].valueCode = #938-948 
* #9401:3 "Anaplastisches Astrozytom o.n.A."
* #9401:3 ^designation[0].value = "Anaplastisches Astrozytom, IDH-Mutation" 
* #9401:3 ^designation[1].value = "Anaplastisches Astrozytom, IDH-Wildtyp" 
* #9401:3 ^property[0].code = #parent 
* #9401:3 ^property[0].valueCode = #938-948 
* #9410:3 "Protoplasmatisches Astrozytom"
* #9410:3 ^property[0].code = #parent 
* #9410:3 ^property[0].valueCode = #938-948 
* #9411:3 "Gemistozytisches Astrozytom o.n.A."
* #9411:3 ^designation[0].value = "Gemistozytisches Astrozytom, IDH-Mutation" 
* #9411:3 ^designation[1].value = "Gemistozytom" 
* #9411:3 ^property[0].code = #parent 
* #9411:3 ^property[0].valueCode = #938-948 
* #9412:1 "Infantiles desmoplastisches Astrozytom"
* #9412:1 ^designation[0].value = "Desmoplastisches infantiles GangliogliomDIG" 
* #9412:1 ^property[0].code = #parent 
* #9412:1 ^property[0].valueCode = #938-948 
* #9413:0 "Dysembryoblastischer neuroepithelialer Tumor"
* #9413:0 ^property[0].code = #parent 
* #9413:0 ^property[0].valueCode = #938-948 
* #9420:3 "Fibrilläres Astrozytom"
* #9420:3 ^designation[0].value = "Fibröses Astrozytom" 
* #9420:3 ^property[0].code = #parent 
* #9420:3 ^property[0].valueCode = #938-948 
* #9421:1 "Pilozytisches Astrozytom"
* #9421:1 ^designation[0].value = "Juveniles Astrozytom" 
* #9421:1 ^designation[1].value = "Piloides Astrozytom" 
* #9421:1 ^designation[2].value = "Spongioblastom o.n.A." 
* #9421:1 ^property[0].code = #parent 
* #9421:1 ^property[0].valueCode = #938-948 
* #9423:3 "Polares Spongioblastom"
* #9423:3 ^designation[0].value = "Primitives polares Spongioblastom" 
* #9423:3 ^designation[1].value = "Spongioblastoma polare" 
* #9423:3 ^property[0].code = #parent 
* #9423:3 ^property[0].valueCode = #938-948 
* #9424:3 "Pleomorphes Xanthoastrozytom o.n.A."
* #9424:3 ^designation[0].value = "Anaplastisches pleomorphes Xanthoastrozytom" 
* #9424:3 ^property[0].code = #parent 
* #9424:3 ^property[0].valueCode = #938-948 
* #9425:3 "Pilomyxoides Astrozytom"
* #9425:3 ^property[0].code = #parent 
* #9425:3 ^property[0].valueCode = #938-948 
* #9430:3 "Astroblastom"
* #9430:3 ^property[0].code = #parent 
* #9430:3 ^property[0].valueCode = #938-948 
* #9431:1 "Angiozentrisches Gliom"
* #9431:1 ^property[0].code = #parent 
* #9431:1 ^property[0].valueCode = #938-948 
* #9432:1 "Pituizytom"
* #9432:1 ^property[0].code = #parent 
* #9432:1 ^property[0].valueCode = #938-948 
* #9440:3 "Glioblastom o.n.A."
* #9440:3 ^designation[0].value = "Epitheloides Glioblastom" 
* #9440:3 ^designation[1].value = "Glioblastom, IDH-Wildtyp" 
* #9440:3 ^designation[2].value = "Primäres Glioblastom o.n.A.Glioblastoma multiforme" 
* #9440:3 ^designation[3].value = "Spongioblastoma multiforme" 
* #9440:3 ^designation[4].value = "Diffuses intrinsisches Ponsgliom" 
* #9440:3 ^designation[5].value = "Diffuses Mittellinien-Gliom o.n.A." 
* #9440:3 ^property[0].code = #parent 
* #9440:3 ^property[0].valueCode = #938-948 
* #9441:3 "Riesenzelliges Glioblastom"
* #9441:3 ^designation[0].value = "Monstrozelluläres Sarkom" 
* #9441:3 ^property[0].code = #parent 
* #9441:3 ^property[0].valueCode = #938-948 
* #9442:1 "Gliofibrom"
* #9442:1 ^property[0].code = #parent 
* #9442:1 ^property[0].valueCode = #938-948 
* #9442:3 "Gliosarkom"
* #9442:3 ^designation[0].value = "Glioblastom mit Sarkomanteilen" 
* #9442:3 ^property[0].code = #parent 
* #9442:3 ^property[0].valueCode = #938-948 
* #9444:1 "Chordoides Gliom"
* #9444:1 ^designation[0].value = "Chordoides Gliom des 3. Ventrikels" 
* #9444:1 ^property[0].code = #parent 
* #9444:1 ^property[0].valueCode = #938-948 
* #9445:3 "Glioblastom, IDH-mutiert"
* #9445:3 ^designation[0].value = "Sekundäres Glioblastom, IDH-Mutation" 
* #9445:3 ^designation[1].value = "Sekundäres Glioblastom, o.n.A." 
* #9445:3 ^property[0].code = #parent 
* #9445:3 ^property[0].valueCode = #938-948 
* #9450:3 "Oligodendrogliom o.n.A."
* #9450:3 ^designation[0].value = "Oligodendrogliom, IDH-mutiert und mit 1p/19q-Co-Deletion" 
* #9450:3 ^property[0].code = #parent 
* #9450:3 ^property[0].valueCode = #938-948 
* #9451:3 "Anaplastisches Oligodendrogliom"
* #9451:3 ^designation[0].value = "Anaplastisches Oligodendrogliom, IDH-Mutation und 1p/19q-Co-Deletion" 
* #9451:3 ^property[0].code = #parent 
* #9451:3 ^property[0].valueCode = #938-948 
* #9460:3 "Oligodendroblastom"
* #9460:3 ^property[0].code = #parent 
* #9460:3 ^property[0].valueCode = #938-948 
* #9470:3 "Medulloblastom o.n.A."
* #9470:3 ^designation[0].value = "Klassisches Medulloblastom" 
* #9470:3 ^designation[1].value = "Melanotisches Medulloblastom" 
* #9470:3 ^property[0].code = #parent 
* #9470:3 ^property[0].valueCode = #938-948 
* #9471:3 "Desmoplastisches noduläres Medulloblastom"
* #9471:3 ^designation[0].value = "Arachnoidales Kleinhirnsarkom" 
* #9471:3 ^designation[1].value = "Desmoplastisches Medulloblastom" 
* #9471:3 ^designation[2].value = "Medulloblastom mit extensiver Nodularität" 
* #9471:3 ^designation[3].value = "Medulloblastom, SHH-aktiviert o.n.A." 
* #9471:3 ^designation[4].value = "Medulloblastom, SHH-aktiviert und TP53-Wildtyp" 
* #9471:3 ^property[0].code = #parent 
* #9471:3 ^property[0].valueCode = #938-948 
* #9472:3 "Medullomyoblastom"
* #9472:3 ^property[0].code = #parent 
* #9472:3 ^property[0].valueCode = #938-948 
* #9473:3 "Embryonaler Tumor des ZNS o.n.A."
* #9473:3 ^designation[0].value = "Primitiver neuroektodermaler Tumor o.n.A." 
* #9473:3 ^designation[1].value = "PNET o.n.A." 
* #9473:3 ^designation[2].value = "Supratentorieller PNET" 
* #9473:3 ^designation[3].value = "Zentraler primitiver neuroektodermaler Tumor o.n.A.CPNET" 
* #9473:3 ^property[0].code = #parent 
* #9473:3 ^property[0].valueCode = #938-948 
* #9474:3 "Großzelliges Medulloblastom"
* #9474:3 ^designation[0].value = "Anaplastisches Medulloblastom" 
* #9474:3 ^property[0].code = #parent 
* #9474:3 ^property[0].valueCode = #938-948 
* #9475:3 "Medulloblastom, WNT-aktiviert o.n.A."
* #9475:3 ^designation[0].value = "Anaplastisches Medulloblastom, WNT-aktiviert" 
* #9475:3 ^designation[1].value = "Großzelliges Medulloblastom, WNT-aktiviert" 
* #9475:3 ^designation[2].value = "Klassisches Medulloblastom, WNT-aktiviert" 
* #9475:3 ^property[0].code = #parent 
* #9475:3 ^property[0].valueCode = #938-948 
* #9476:3 "Medulloblastom, SHH-aktiviert und TP53 mutiert"
* #9476:3 ^property[0].code = #parent 
* #9476:3 ^property[0].valueCode = #938-948 
* #9477:3 "Medulloblastom, nicht-WNT-/nicht-SHH-aktiviert"
* #9477:3 ^designation[0].value = "Medulloblastom, Gruppe 3" 
* #9477:3 ^designation[1].value = "Medulloblastom, Gruppe 4" 
* #9477:3 ^property[0].code = #parent 
* #9477:3 ^property[0].valueCode = #938-948 
* #9478:3 "Embryonaler Tumor mit mehrschichtigen Rosetten und C19MC-Alteration"
* #9478:3 ^designation[0].value = "Embryonaler Tumor mit mehrschichtigen Rosetten o.n.A." 
* #9478:3 ^property[0].code = #parent 
* #9478:3 ^property[0].valueCode = #938-948 
* #9480:3 "Kleinhirnsarkom o.n.A."
* #9480:3 ^property[0].code = #parent 
* #9480:3 ^property[0].valueCode = #938-948 
* #949-952 "Neuroepitheliomatöse Neoplasien"
* #949-952 ^property[0].code = #parent 
* #949-952 ^property[0].valueCode = #M 
* #949-952 ^property[1].code = #child 
* #949-952 ^property[1].valueCode = #9490:0 
* #949-952 ^property[2].code = #child 
* #949-952 ^property[2].valueCode = #9490:3 
* #949-952 ^property[3].code = #child 
* #949-952 ^property[3].valueCode = #9491:0 
* #949-952 ^property[4].code = #child 
* #949-952 ^property[4].valueCode = #9492:0 
* #949-952 ^property[5].code = #child 
* #949-952 ^property[5].valueCode = #9493:0 
* #949-952 ^property[6].code = #child 
* #949-952 ^property[6].valueCode = #9500:3 
* #949-952 ^property[7].code = #child 
* #949-952 ^property[7].valueCode = #9501:0 
* #949-952 ^property[8].code = #child 
* #949-952 ^property[8].valueCode = #9501:3 
* #949-952 ^property[9].code = #child 
* #949-952 ^property[9].valueCode = #9502:0 
* #949-952 ^property[10].code = #child 
* #949-952 ^property[10].valueCode = #9502:3 
* #949-952 ^property[11].code = #child 
* #949-952 ^property[11].valueCode = #9503:3 
* #949-952 ^property[12].code = #child 
* #949-952 ^property[12].valueCode = #9504:3 
* #949-952 ^property[13].code = #child 
* #949-952 ^property[13].valueCode = #9505:1 
* #949-952 ^property[14].code = #child 
* #949-952 ^property[14].valueCode = #9505:3 
* #949-952 ^property[15].code = #child 
* #949-952 ^property[15].valueCode = #9506:1 
* #949-952 ^property[16].code = #child 
* #949-952 ^property[16].valueCode = #9507:0 
* #949-952 ^property[17].code = #child 
* #949-952 ^property[17].valueCode = #9508:3 
* #949-952 ^property[18].code = #child 
* #949-952 ^property[18].valueCode = #9509:1 
* #949-952 ^property[19].code = #child 
* #949-952 ^property[19].valueCode = #9510:0 
* #949-952 ^property[20].code = #child 
* #949-952 ^property[20].valueCode = #9510:3 
* #949-952 ^property[21].code = #child 
* #949-952 ^property[21].valueCode = #9511:3 
* #949-952 ^property[22].code = #child 
* #949-952 ^property[22].valueCode = #9512:3 
* #949-952 ^property[23].code = #child 
* #949-952 ^property[23].valueCode = #9513:3 
* #949-952 ^property[24].code = #child 
* #949-952 ^property[24].valueCode = #9514:1 
* #949-952 ^property[25].code = #child 
* #949-952 ^property[25].valueCode = #9520:3 
* #949-952 ^property[26].code = #child 
* #949-952 ^property[26].valueCode = #9521:3 
* #949-952 ^property[27].code = #child 
* #949-952 ^property[27].valueCode = #9522:3 
* #949-952 ^property[28].code = #child 
* #949-952 ^property[28].valueCode = #9523:3 
* #9490:0 "Ganglioneurom"
* #9490:0 ^property[0].code = #parent 
* #9490:0 ^property[0].valueCode = #949-952 
* #9490:3 "Ganglioneuroblastom"
* #9490:3 ^designation[0].value = "Ganglineuroblastom des ZNS" 
* #9490:3 ^property[0].code = #parent 
* #9490:3 ^property[0].valueCode = #949-952 
* #9491:0 "Ganglioneuromatose"
* #9491:0 ^property[0].code = #parent 
* #9491:0 ^property[0].valueCode = #949-952 
* #9492:0 "Gangliozytom o.n.A."
* #9492:0 ^property[0].code = #parent 
* #9492:0 ^property[0].valueCode = #949-952 
* #9493:0 "Dysplastisches Gangliozytom des Kleinhirns (Lhermitte-Duclos)"
* #9493:0 ^property[0].code = #parent 
* #9493:0 ^property[0].valueCode = #949-952 
* #9500:3 "Neuroblastom o.n.A."
* #9500:3 ^designation[0].value = "Neuroblastom des ZNS" 
* #9500:3 ^designation[1].value = "Sympathikoblastom" 
* #9500:3 ^designation[2].value = "Zentrales Neuroblastom" 
* #9500:3 ^property[0].code = #parent 
* #9500:3 ^property[0].valueCode = #949-952 
* #9501:0 "Benignes Medulloepitheliom"
* #9501:0 ^designation[0].value = "Benignes Diktyom" 
* #9501:0 ^property[0].code = #parent 
* #9501:0 ^property[0].valueCode = #949-952 
* #9501:3 "Medulloepitheliom o.n.A."
* #9501:3 ^designation[0].value = "Malignes Diktyom" 
* #9501:3 ^property[0].code = #parent 
* #9501:3 ^property[0].valueCode = #949-952 
* #9502:0 "Benignes teratoides Medulloepitheliom"
* #9502:0 ^property[0].code = #parent 
* #9502:0 ^property[0].valueCode = #949-952 
* #9502:3 "Teratoides Medulloepitheliom o.n.A."
* #9502:3 ^property[0].code = #parent 
* #9502:3 ^property[0].valueCode = #949-952 
* #9503:3 "Neuroepitheliom o.n.A."
* #9503:3 ^property[0].code = #parent 
* #9503:3 ^property[0].valueCode = #949-952 
* #9504:3 "Spongioneuroblastom"
* #9504:3 ^property[0].code = #parent 
* #9504:3 ^property[0].valueCode = #949-952 
* #9505:1 "Gangliogliom o.n.A."
* #9505:1 ^designation[0].value = "Glioneurom" 
* #9505:1 ^designation[1].value = "Neuroastrozytom" 
* #9505:1 ^property[0].code = #parent 
* #9505:1 ^property[0].valueCode = #949-952 
* #9505:3 "Anaplastisches Gangliogliom"
* #9505:3 ^property[0].code = #parent 
* #9505:3 ^property[0].valueCode = #949-952 
* #9506:1 "Zentrales Neurozytom"
* #9506:1 ^designation[0].value = "Neurozytom o.n.A." 
* #9506:1 ^designation[1].value = "Extraventrikuläres Neurozytom" 
* #9506:1 ^designation[2].value = "Zerebelläres LiponeurozytomLipomartiges Medulloblastom" 
* #9506:1 ^designation[3].value = "Zerebelläres LiponeurozytomMedullozytom" 
* #9506:1 ^designation[4].value = "Zerebelläres LiponeurozytomNeurolipozytom" 
* #9506:1 ^property[0].code = #parent 
* #9506:1 ^property[0].valueCode = #949-952 
* #9507:0 "Pacini-Tumor"
* #9507:0 ^property[0].code = #parent 
* #9507:0 ^property[0].valueCode = #949-952 
* #9508:3 "Atypischer teratoider/rhabdoider Tumor"
* #9508:3 ^designation[0].value = "Embryonaler Tumor des ZNS mit rhabdoiden Merkmalen" 
* #9508:3 ^property[0].code = #parent 
* #9508:3 ^property[0].valueCode = #949-952 
* #9509:1 "Papillärer glioneuronaler Tumor"
* #9509:1 ^designation[0].value = "Rosettenförmiger glioneuronaler Tumor" 
* #9509:1 ^property[0].code = #parent 
* #9509:1 ^property[0].valueCode = #949-952 
* #9510:0 "Retinozytom"
* #9510:0 ^property[0].code = #parent 
* #9510:0 ^property[0].valueCode = #949-952 
* #9510:3 "Retinoblastom o.n.A."
* #9510:3 ^property[0].code = #parent 
* #9510:3 ^property[0].valueCode = #949-952 
* #9511:3 "Differenziertes Retinoblastom"
* #9511:3 ^property[0].code = #parent 
* #9511:3 ^property[0].valueCode = #949-952 
* #9512:3 "Undifferenziertes Retinoblastom"
* #9512:3 ^property[0].code = #parent 
* #9512:3 ^property[0].valueCode = #949-952 
* #9513:3 "Diffuses Retinoblastom"
* #9513:3 ^property[0].code = #parent 
* #9513:3 ^property[0].valueCode = #949-952 
* #9514:1 "Spontan regrediertes Retinoblastom"
* #9514:1 ^property[0].code = #parent 
* #9514:1 ^property[0].valueCode = #949-952 
* #9520:3 "Neurogener Olfaktoriustumor"
* #9520:3 ^property[0].code = #parent 
* #9520:3 ^property[0].valueCode = #949-952 
* #9521:3 "Olfaktorius-Neurozytom"
* #9521:3 ^designation[0].value = "Ästhesioneurozytom" 
* #9521:3 ^property[0].code = #parent 
* #9521:3 ^property[0].valueCode = #949-952 
* #9522:3 "Olfaktorius-Neuroblastom"
* #9522:3 ^designation[0].value = "Ästhesioneuroblastom" 
* #9522:3 ^property[0].code = #parent 
* #9522:3 ^property[0].valueCode = #949-952 
* #9523:3 "Olfaktorius-Neuroepitheliom"
* #9523:3 ^designation[0].value = "Ästhesioneuroepitheliom" 
* #9523:3 ^property[0].code = #parent 
* #9523:3 ^property[0].valueCode = #949-952 
* #953-953 "Neoplasien der Meningen"
* #953-953 ^property[0].code = #parent 
* #953-953 ^property[0].valueCode = #M 
* #953-953 ^property[1].code = #child 
* #953-953 ^property[1].valueCode = #9530:0 
* #953-953 ^property[2].code = #child 
* #953-953 ^property[2].valueCode = #9530:3 
* #953-953 ^property[3].code = #child 
* #953-953 ^property[3].valueCode = #9531:0 
* #953-953 ^property[4].code = #child 
* #953-953 ^property[4].valueCode = #9532:0 
* #953-953 ^property[5].code = #child 
* #953-953 ^property[5].valueCode = #9533:0 
* #953-953 ^property[6].code = #child 
* #953-953 ^property[6].valueCode = #9534:0 
* #953-953 ^property[7].code = #child 
* #953-953 ^property[7].valueCode = #9535:0 
* #953-953 ^property[8].code = #child 
* #953-953 ^property[8].valueCode = #9537:0 
* #953-953 ^property[9].code = #child 
* #953-953 ^property[9].valueCode = #9538:1 
* #953-953 ^property[10].code = #child 
* #953-953 ^property[10].valueCode = #9538:3 
* #953-953 ^property[11].code = #child 
* #953-953 ^property[11].valueCode = #9539:1 
* #953-953 ^property[12].code = #child 
* #953-953 ^property[12].valueCode = #9539:3 
* #9530:0 "Meningeom o.n.A."
* #9530:0 ^designation[0].value = "Lymphoplasmozytenreiches Meningeom" 
* #9530:0 ^designation[1].value = "Metaplastisches Meningeom" 
* #9530:0 ^designation[2].value = "Mikrozystisches Meningeom" 
* #9530:0 ^designation[3].value = "Sekretorisches Meningeom" 
* #9530:0 ^property[0].code = #parent 
* #9530:0 ^property[0].valueCode = #953-953 
* #9530:3 "Malignes Meningeom"
* #9530:3 ^designation[0].value = "Anaplastisches Meningeom" 
* #9530:3 ^designation[1].value = "Leptomeningeales Sarkom" 
* #9530:3 ^designation[2].value = "Meningeales Sarkom" 
* #9530:3 ^designation[3].value = "Meningotheliales Sarkom" 
* #9530:3 ^property[0].code = #parent 
* #9530:3 ^property[0].valueCode = #953-953 
* #9531:0 "Meningotheliales Meningeom"
* #9531:0 ^designation[0].value = "Endotheliales Meningeom" 
* #9531:0 ^designation[1].value = "Synzytiales Meningeom" 
* #9531:0 ^property[0].code = #parent 
* #9531:0 ^property[0].valueCode = #953-953 
* #9532:0 "Fibröses Meningeom"
* #9532:0 ^designation[0].value = "Fibroblastisches Meningeom" 
* #9532:0 ^property[0].code = #parent 
* #9532:0 ^property[0].valueCode = #953-953 
* #9533:0 "Psammöses Meningeom"
* #9533:0 ^property[0].code = #parent 
* #9533:0 ^property[0].valueCode = #953-953 
* #9534:0 "Angiomatöses Meningeom"
* #9534:0 ^property[0].code = #parent 
* #9534:0 ^property[0].valueCode = #953-953 
* #9535:0 "Hämangioblastisches Meningeom"
* #9535:0 ^designation[0].value = "Angioblastisches Meningeom" 
* #9535:0 ^property[0].code = #parent 
* #9535:0 ^property[0].valueCode = #953-953 
* #9537:0 "Meningeom vom Übergangstyp"
* #9537:0 ^designation[0].value = "Mischmeningeom" 
* #9537:0 ^property[0].code = #parent 
* #9537:0 ^property[0].valueCode = #953-953 
* #9538:1 "Klarzell-Meningeom"
* #9538:1 ^designation[0].value = "Chordoides Meningeom" 
* #9538:1 ^property[0].code = #parent 
* #9538:1 ^property[0].valueCode = #953-953 
* #9538:3 "Papilläres Meningeom"
* #9538:3 ^designation[0].value = "Rhabdoides Meningeom" 
* #9538:3 ^property[0].code = #parent 
* #9538:3 ^property[0].valueCode = #953-953 
* #9539:1 "Atypisches Meningeom"
* #9539:1 ^property[0].code = #parent 
* #9539:1 ^property[0].valueCode = #953-953 
* #9539:3 "Meningeale Sarkomatose"
* #9539:3 ^property[0].code = #parent 
* #9539:3 ^property[0].valueCode = #953-953 
* #954-957 "Neoplasien der Nervenscheiden"
* #954-957 ^property[0].code = #parent 
* #954-957 ^property[0].valueCode = #M 
* #954-957 ^property[1].code = #child 
* #954-957 ^property[1].valueCode = #9540:0 
* #954-957 ^property[2].code = #child 
* #954-957 ^property[2].valueCode = #9540:3 
* #954-957 ^property[3].code = #child 
* #954-957 ^property[3].valueCode = #9541:0 
* #954-957 ^property[4].code = #child 
* #954-957 ^property[4].valueCode = #9542:3 
* #954-957 ^property[5].code = #child 
* #954-957 ^property[5].valueCode = #9550:0 
* #954-957 ^property[6].code = #child 
* #954-957 ^property[6].valueCode = #9560:0 
* #954-957 ^property[7].code = #child 
* #954-957 ^property[7].valueCode = #9560:1 
* #954-957 ^property[8].code = #child 
* #954-957 ^property[8].valueCode = #9560:3 
* #954-957 ^property[9].code = #child 
* #954-957 ^property[9].valueCode = #9561:3 
* #954-957 ^property[10].code = #child 
* #954-957 ^property[10].valueCode = #9562:0 
* #954-957 ^property[11].code = #child 
* #954-957 ^property[11].valueCode = #9563:0 
* #954-957 ^property[12].code = #child 
* #954-957 ^property[12].valueCode = #9570:0 
* #954-957 ^property[13].code = #child 
* #954-957 ^property[13].valueCode = #9571:0 
* #954-957 ^property[14].code = #child 
* #954-957 ^property[14].valueCode = #9571:3 
* #9540:0 "Neurofibrom o.n.A."
* #9540:0 ^property[0].code = #parent 
* #9540:0 ^property[0].valueCode = #954-957 
* #9540:3 "Maligner peripherer Nervenscheidentumor o.n.A."
* #9540:3 ^designation[0].value = "MPNST o.n.A." 
* #9540:3 ^designation[1].value = "Neurofibrosarkom" 
* #9540:3 ^designation[2].value = "Neurogenes Sarkom" 
* #9540:3 ^designation[3].value = "Neurosarkom" 
* #9540:3 ^designation[4].value = "Melanotischer MPNST" 
* #9540:3 ^designation[5].value = "Melanotischer psammomatöser MPNST" 
* #9540:3 ^designation[6].value = "MPNST mit divergierender mesenchymaler Differenzierung" 
* #9540:3 ^designation[7].value = "MPNST mit glandulärer Differenzierung" 
* #9540:3 ^designation[8].value = "MPNST mit perineuraler Differenzierung" 
* #9540:3 ^property[0].code = #parent 
* #9540:3 ^property[0].valueCode = #954-957 
* #9541:0 "Melanotisches Neurofibrom"
* #9541:0 ^property[0].code = #parent 
* #9541:0 ^property[0].valueCode = #954-957 
* #9542:3 "Epitheloider maligner peripherer Nervenscheidentumor"
* #9542:3 ^designation[0].value = "Epithelioid MPNST" 
* #9542:3 ^property[0].code = #parent 
* #9542:3 ^property[0].valueCode = #954-957 
* #9550:0 "Plexiformes Neurofibrom"
* #9550:0 ^designation[0].value = "Plexiformes Neurom" 
* #9550:0 ^property[0].code = #parent 
* #9550:0 ^property[0].valueCode = #954-957 
* #9560:0 "Schwannom o.n.A."
* #9560:0 ^designation[0].value = "Neurilemmom o.n.A." 
* #9560:0 ^designation[1].value = "Neurinom" 
* #9560:0 ^designation[2].value = "Akustikusneurinom" 
* #9560:0 ^designation[3].value = "Altes (degeneriertes) Schwannom" 
* #9560:0 ^designation[4].value = "Degeneriertes Schwannom" 
* #9560:0 ^designation[5].value = "Plexiformes Schwannom" 
* #9560:0 ^designation[6].value = "Psammomatöses Schwannom" 
* #9560:0 ^designation[7].value = "Zellreiches Schwannom" 
* #9560:0 ^property[0].code = #parent 
* #9560:0 ^property[0].valueCode = #954-957 
* #9560:1 "Melanozytisches Schwannom"
* #9560:1 ^designation[0].value = "Pigmentiertes Schwannom" 
* #9560:1 ^property[0].code = #parent 
* #9560:1 ^property[0].valueCode = #954-957 
* #9560:3 "Malignes Neurilemmom"
* #9560:3 ^designation[0].value = "Malignes Schwannom o.n.A." 
* #9560:3 ^designation[1].value = "Neurilemmosarkom" 
* #9560:3 ^property[0].code = #parent 
* #9560:3 ^property[0].valueCode = #954-957 
* #9561:3 "Maligner peripherer Nervenscheidentumor mit rhabdomyoblastischer Differenzierung"
* #9561:3 ^designation[0].value = "Maligner Tritontumor" 
* #9561:3 ^designation[1].value = "Malignes Schwannom mit rhabdomyoblastischer Differenzierung" 
* #9561:3 ^designation[2].value = "MPNST mit rhabdomyoblastischer Differenzierung" 
* #9561:3 ^property[0].code = #parent 
* #9561:3 ^property[0].valueCode = #954-957 
* #9562:0 "Nervenscheidenmyxom"
* #9562:0 ^designation[0].value = "Neurothekom" 
* #9562:0 ^designation[1].value = "Zelluläres Neurothekom" 
* #9562:0 ^property[0].code = #parent 
* #9562:0 ^property[0].valueCode = #954-957 
* #9563:0 "Nervenscheidentumor o.n.A."
* #9563:0 ^designation[0].value = "Hybrider Nervenscheidentumor" 
* #9563:0 ^property[0].code = #parent 
* #9563:0 ^property[0].valueCode = #954-957 
* #9570:0 "Neurom o.n.A."
* #9570:0 ^designation[0].value = "Solitäres umschriebenes Neurom" 
* #9570:0 ^property[0].code = #parent 
* #9570:0 ^property[0].valueCode = #954-957 
* #9571:0 "Perineuriom o.n.A."
* #9571:0 ^designation[0].value = "Intraneurales Perineuriom" 
* #9571:0 ^designation[1].value = "Weichteil-Perineuriom" 
* #9571:0 ^property[0].code = #parent 
* #9571:0 ^property[0].valueCode = #954-957 
* #9571:3 "Malignes Perineuriom"
* #9571:3 ^property[0].code = #parent 
* #9571:3 ^property[0].valueCode = #954-957 
* #958-958 "Neoplasien der Granularzellen und alveoläres Weichteilsarkom"
* #958-958 ^property[0].code = #parent 
* #958-958 ^property[0].valueCode = #M 
* #958-958 ^property[1].code = #child 
* #958-958 ^property[1].valueCode = #9580:0 
* #958-958 ^property[2].code = #child 
* #958-958 ^property[2].valueCode = #9580:3 
* #958-958 ^property[3].code = #child 
* #958-958 ^property[3].valueCode = #9581:3 
* #958-958 ^property[4].code = #child 
* #958-958 ^property[4].valueCode = #9582:0 
* #9580:0 "Granularzelltumor o.n.A."
* #9580:0 ^designation[0].value = "Granularzellmyoblastom o.n.A." 
* #9580:0 ^property[0].code = #parent 
* #9580:0 ^property[0].valueCode = #958-958 
* #9580:3 "Maligner Granularzelltumor"
* #9580:3 ^designation[0].value = "Malignes Granularzellmyoblastom" 
* #9580:3 ^property[0].code = #parent 
* #9580:3 ^property[0].valueCode = #958-958 
* #9581:3 "Alveoläres Weichteilsarkom"
* #9581:3 ^property[0].code = #parent 
* #9581:3 ^property[0].valueCode = #958-958 
* #9582:0 "Granularzelltumor des Infundibulums"
* #9582:0 ^property[0].code = #parent 
* #9582:0 ^property[0].valueCode = #958-958 
* #959-972 "Hodgkin- und Non-Hodgkin-Lymphome"
* #959-972 ^property[0].code = #parent 
* #959-972 ^property[0].valueCode = #M 
* #959-972 ^property[1].code = #child 
* #959-972 ^property[1].valueCode = #959-959 
* #959-972 ^property[2].code = #child 
* #959-972 ^property[2].valueCode = #965-966 
* #959-972 ^property[3].code = #child 
* #959-972 ^property[3].valueCode = #967-972 
* #959-959 "Maligne Lymphome, o.n.A. oder diffus"
* #959-959 ^property[0].code = #parent 
* #959-959 ^property[0].valueCode = #959-972 
* #959-959 ^property[1].code = #child 
* #959-959 ^property[1].valueCode = #9590:3 
* #959-959 ^property[2].code = #child 
* #959-959 ^property[2].valueCode = #9591:1 
* #959-959 ^property[3].code = #child 
* #959-959 ^property[3].valueCode = #9591:3 
* #959-959 ^property[4].code = #child 
* #959-959 ^property[4].valueCode = #9596:3 
* #959-959 ^property[5].code = #child 
* #959-959 ^property[5].valueCode = #9597:3 
* #9590:3 "Malignes Lymphom o.n.A."
* #9590:3 ^designation[0].value = "Lymphom o.n.A." 
* #9590:3 ^designation[1].value = "Mikrogliom" 
* #9590:3 ^property[0].code = #parent 
* #9590:3 ^property[0].valueCode = #959-959 
* #9591:1 "Monoklonale B-Zell-Lymphozytose, o.n.A."
* #9591:1 ^designation[0].value = "Monoklonale B-Zell-Lymphozytose, non-CLL-Typ" 
* #9591:1 ^property[0].code = #parent 
* #9591:1 ^property[0].valueCode = #959-959 
* #9591:3 "Malignes Non-Hodgkin-Lymphom o.n.A"
* #9591:3 ^designation[0].value = "Non-Hodgkin-Lymphom o.n.A" 
* #9591:3 ^designation[1].value = "B-Zell-Lymphom o.n.A." 
* #9591:3 ^designation[2].value = "Diffuses malignes Lymphom o.n.A." 
* #9591:3 ^designation[3].value = "Haarzellleukämie-Variante" 
* #9591:3 ^designation[4].value = "Kleinzelliges nichtgekerbtkerniges diffuses malignes LymphomMalignes Lymphom vom undifferenzierten Zelltyp, Nicht-Burkitt" 
* #9591:3 ^designation[5].value = "Kleinzelliges nichtgekerbtkerniges diffuses malignes LymphomMalignes Lymphom vom undifferenzierten Zelltyp o.n.A." 
* #9591:3 ^designation[6].value = "Kleinzelliges und gekerbtkerniges diffuses malignes Lymphom o.n.A." 
* #9591:3 ^designation[7].value = "Lymphosarkom o.n.A.Diffuses Lymphosarkom" 
* #9591:3 ^designation[8].value = "Lymphozytisches schlecht differenziertes diffuses malignes LymphomGekerbtkerniges malignes Lymphom o.n.A." 
* #9591:3 ^designation[9].value = "Lymphozytisches schlecht differenziertes diffuses malignes LymphomKleinzelliges und gekerbtkerniges malignes Lymphom o.n.A." 
* #9591:3 ^designation[10].value = "Nichtgekerbtkerniges malignes Lymphom o.n.A." 
* #9591:3 ^designation[11].value = "Noduläres lymphozytisches malignes Lymphom von intermediärer Differenzierung" 
* #9591:3 ^designation[12].value = "Retikulumzellsarkom o.n.A.Diffuses Retikulumzellsarkom" 
* #9591:3 ^designation[13].value = "Retikulumzellsarkom o.n.A.Diffuses Retikulosarkom" 
* #9591:3 ^designation[14].value = "Retikulumzellsarkom o.n.A.Retikulosarkom o.n.A." 
* #9591:3 ^designation[15].value = "Splenische/s B-Zell-Lymphom/-Leukämie, nicht klassifizierbar" 
* #9591:3 ^designation[16].value = "Splenisches diffuses kleinzelliges B-Zell-Lymphom der roten Pulpa" 
* #9591:3 ^property[0].code = #parent 
* #9591:3 ^property[0].valueCode = #959-959 
* #9596:3 "Kombiniertes malignes Hodgkin- und Non-Hodgkin-Lymphom"
* #9596:3 ^designation[0].value = "B-Zell-Lymphom, nicht klassifizierbar, mit Eigenschaften zwischen denen eines diffus großzelligen B-Zell-Lymphoms und eines klassischen Hodgkin-Lymphoms" 
* #9596:3 ^property[0].code = #parent 
* #9596:3 ^property[0].valueCode = #959-959 
* #9597:3 "Primär kutanes Follikelzentrumslymphom"
* #9597:3 ^property[0].code = #parent 
* #9597:3 ^property[0].valueCode = #959-959 
* #965-966 "Hodgkin-Lymphome"
* #965-966 ^property[0].code = #parent 
* #965-966 ^property[0].valueCode = #959-972 
* #965-966 ^property[1].code = #child 
* #965-966 ^property[1].valueCode = #9650:3 
* #965-966 ^property[2].code = #child 
* #965-966 ^property[2].valueCode = #9651:3 
* #965-966 ^property[3].code = #child 
* #965-966 ^property[3].valueCode = #9652:3 
* #965-966 ^property[4].code = #child 
* #965-966 ^property[4].valueCode = #9653:3 
* #965-966 ^property[5].code = #child 
* #965-966 ^property[5].valueCode = #9654:3 
* #965-966 ^property[6].code = #child 
* #965-966 ^property[6].valueCode = #9655:3 
* #965-966 ^property[7].code = #child 
* #965-966 ^property[7].valueCode = #9659:3 
* #965-966 ^property[8].code = #child 
* #965-966 ^property[8].valueCode = #9661:3 
* #965-966 ^property[9].code = #child 
* #965-966 ^property[9].valueCode = #9662:3 
* #965-966 ^property[10].code = #child 
* #965-966 ^property[10].valueCode = #9663:3 
* #965-966 ^property[11].code = #child 
* #965-966 ^property[11].valueCode = #9664:3 
* #965-966 ^property[12].code = #child 
* #965-966 ^property[12].valueCode = #9665:3 
* #965-966 ^property[13].code = #child 
* #965-966 ^property[13].valueCode = #9667:3 
* #9650:3 "Hodgkin-Lymphom o.n.A."
* #9650:3 ^designation[0].value = "M. Hodgkin o.n.A." 
* #9650:3 ^designation[1].value = "Malignes Lymphom, Hodgkin" 
* #9650:3 ^designation[2].value = "Klassisches Hodgkin-Lymphom als Post-Transplantations-lymphoproliferative Krankheit" 
* #9650:3 ^property[0].code = #parent 
* #9650:3 ^property[0].valueCode = #965-966 
* #9651:3 "Hodgkin-Lymphom, lymphozytenreich"
* #9651:3 ^designation[0].value = "Lymphozytenreiches klassisches Hodgkin-Lymphom" 
* #9651:3 ^designation[1].value = "M. Hodgkin vom lymphozytenprädominanten Typ o.n.A.Lymphozytenprädominanter diffuser M. Hodgkin" 
* #9651:3 ^designation[2].value = "M. Hodgkin vom lymphozytenprädominanten Typ o.n.A.M. Hodgkin mit Lymphozyten-Histiozyten-Prädominanz" 
* #9651:3 ^property[0].code = #parent 
* #9651:3 ^property[0].valueCode = #965-966 
* #9652:3 "Hodgkin-Lymphom, gemischtzellige Form o.n.A."
* #9652:3 ^designation[0].value = "Gemischtzelliges klassisches Hodgkin-Lymphom o.n.A." 
* #9652:3 ^property[0].code = #parent 
* #9652:3 ^property[0].valueCode = #965-966 
* #9653:3 "Hodgkin-Lymphom, lymphozytenarmer Typ o.n.A."
* #9653:3 ^designation[0].value = "Lymphozytenarmes klassisches Hodgkin-Lymphom o.n.A." 
* #9653:3 ^property[0].code = #parent 
* #9653:3 ^property[0].valueCode = #965-966 
* #9654:3 "Hodgkin-Lymphom, lymphozytenarmer Typ, diffuse Fibrose"
* #9654:3 ^designation[0].value = "Lymphozytenarmes klassisches Hodgkin-Lymphom, diffuse Fibrose" 
* #9654:3 ^property[0].code = #parent 
* #9654:3 ^property[0].valueCode = #965-966 
* #9655:3 "Hodgkin-Lymphom, lymphozytenarmer Typ, retikuläre Form"
* #9655:3 ^designation[0].value = "Lymphozytenarmes klassisches Hodgkin-Lymphom, retikuläre Form" 
* #9655:3 ^property[0].code = #parent 
* #9655:3 ^property[0].valueCode = #965-966 
* #9659:3 "Nodulär Lymphozyten-prädominantes Hodgkin-Lymphom"
* #9659:3 ^designation[0].value = "Hodgkin-Paragranulom o.n.A." 
* #9659:3 ^designation[1].value = "Lymphozytenprädominantes noduläres Hodgkin-Lymphom" 
* #9659:3 ^designation[2].value = "Noduläres Hodgkin-Paragranulom" 
* #9659:3 ^property[0].code = #parent 
* #9659:3 ^property[0].valueCode = #965-966 
* #9661:3 "Hodgkin-Granulom"
* #9661:3 ^property[0].code = #parent 
* #9661:3 ^property[0].valueCode = #965-966 
* #9662:3 "Hodgkin-Sarkom"
* #9662:3 ^property[0].code = #parent 
* #9662:3 ^property[0].valueCode = #965-966 
* #9663:3 "Hodgkin-Lymphom, nodulär-sklerosierender Typ o.n.A."
* #9663:3 ^designation[0].value = "Nodulär-sklerosierender M. Hodgkin o.n.A." 
* #9663:3 ^designation[1].value = "Nodulär-sklerosierendes klassisches Hodgkin-Lymphom o.n.A." 
* #9663:3 ^property[0].code = #parent 
* #9663:3 ^property[0].valueCode = #965-966 
* #9664:3 "Hodgkin-Lymphom, nodulär-sklerosierender Typ, zelluläre Phase"
* #9664:3 ^designation[0].value = "Nodulär-sklerosierendes klassisches Hodgkin-Lymphom, zelluläre Phase" 
* #9664:3 ^property[0].code = #parent 
* #9664:3 ^property[0].valueCode = #965-966 
* #9665:3 "Hodgkin-Lymphom, nodulär-sklerosierender Typ, Grad I"
* #9665:3 ^designation[0].value = "Gemischtzelliger M. Hodgkin, nodulär-sklerosierender Typ" 
* #9665:3 ^designation[1].value = "Lymphozytenreicher M. Hodgkin, nodulär-sklerosierender Typ" 
* #9665:3 ^designation[2].value = "Nodulär-sklerosierendes klassisches Hodgkin-Lymphom, Grad I" 
* #9665:3 ^property[0].code = #parent 
* #9665:3 ^property[0].valueCode = #965-966 
* #9667:3 "Hodgkin-Lymphom, nodulär-sklerosierender Typ, Grad II"
* #9667:3 ^designation[0].value = "Lymphozytenarmer M. Hodgkin, nodulär-sklerosierender Typ" 
* #9667:3 ^designation[1].value = "M. Hodgkin, nodulär-sklerosierender Typ, synzytiale Variante" 
* #9667:3 ^designation[2].value = "Nodulär-sklerosierendes klassisches Hodgkin-Lymphom, Grad II" 
* #9667:3 ^property[0].code = #parent 
* #9667:3 ^property[0].valueCode = #965-966 
* #967-972 "Non-Hodgkin-Lymphome"
* #967-972 ^property[0].code = #parent 
* #967-972 ^property[0].valueCode = #959-972 
* #967-972 ^property[1].code = #child 
* #967-972 ^property[1].valueCode = #967-969 
* #967-972 ^property[2].code = #child 
* #967-972 ^property[2].valueCode = #970-971 
* #967-972 ^property[3].code = #child 
* #967-972 ^property[3].valueCode = #972-972 
* #967-969 "Reifzellige B-Zell-Lymphome"
* #967-969 ^property[0].code = #parent 
* #967-969 ^property[0].valueCode = #967-972 
* #967-969 ^property[1].code = #child 
* #967-969 ^property[1].valueCode = #9671:3 
* #967-969 ^property[2].code = #child 
* #967-969 ^property[2].valueCode = #9673:1 
* #967-969 ^property[3].code = #child 
* #967-969 ^property[3].valueCode = #9673:3 
* #967-969 ^property[4].code = #child 
* #967-969 ^property[4].valueCode = #9675:3 
* #967-969 ^property[5].code = #child 
* #967-969 ^property[5].valueCode = #9678:3 
* #967-969 ^property[6].code = #child 
* #967-969 ^property[6].valueCode = #9679:3 
* #967-969 ^property[7].code = #child 
* #967-969 ^property[7].valueCode = #9680:1 
* #967-969 ^property[8].code = #child 
* #967-969 ^property[8].valueCode = #9680:3 
* #967-969 ^property[9].code = #child 
* #967-969 ^property[9].valueCode = #9684:3 
* #967-969 ^property[10].code = #child 
* #967-969 ^property[10].valueCode = #9687:3 
* #967-969 ^property[11].code = #child 
* #967-969 ^property[11].valueCode = #9688:3 
* #967-969 ^property[12].code = #child 
* #967-969 ^property[12].valueCode = #9689:3 
* #967-969 ^property[13].code = #child 
* #967-969 ^property[13].valueCode = #9690:3 
* #967-969 ^property[14].code = #child 
* #967-969 ^property[14].valueCode = #9691:3 
* #967-969 ^property[15].code = #child 
* #967-969 ^property[15].valueCode = #9695:1 
* #967-969 ^property[16].code = #child 
* #967-969 ^property[16].valueCode = #9695:3 
* #967-969 ^property[17].code = #child 
* #967-969 ^property[17].valueCode = #9698:3 
* #967-969 ^property[18].code = #child 
* #967-969 ^property[18].valueCode = #9699:3 
* #9671:3 "Lymphoplasmozytisches Lymphom"
* #9671:3 ^designation[0].value = "Lymphoplasmozytoides Lymphom" 
* #9671:3 ^designation[1].value = "Immunozytom" 
* #9671:3 ^designation[2].value = "Plasmozytisches Lymphom" 
* #9671:3 ^designation[3].value = "Plasmozytoides Lymphom" 
* #9671:3 ^property[0].code = #parent 
* #9671:3 ^property[0].valueCode = #967-969 
* #9673:1 "In situ Mantelzell-Neoplasie"
* #9673:1 ^designation[0].value = "In situ Mantelzell-Lymphom" 
* #9673:1 ^property[0].code = #parent 
* #9673:1 ^property[0].valueCode = #967-969 
* #9673:3 "Mantelzell-Lymphom"
* #9673:3 ^designation[0].value = "Lymphozytisches mittelgradig differenziertes diffuses Lymphom" 
* #9673:3 ^designation[1].value = "Maligne lymphomatöse Polypose" 
* #9673:3 ^designation[2].value = "Mantelzonen-Lymphom" 
* #9673:3 ^designation[3].value = "Zentrozytisches Lymphom" 
* #9673:3 ^property[0].code = #parent 
* #9673:3 ^property[0].valueCode = #967-969 
* #9675:3 "Gemischt klein- und großzelliges diffuses Lymphom"
* #9675:3 ^designation[0].value = "Diffuses Lymphom vom Mischzelltyp" 
* #9675:3 ^designation[1].value = "Lymphozytisch-histiozytisches diffuses Lymphom" 
* #9675:3 ^designation[2].value = "Zentroblastisch-zentrozytisches diffuses Lymphom" 
* #9675:3 ^designation[3].value = "Zentroblastisch-zentrozytisches Lymphom o.n.A." 
* #9675:3 ^property[0].code = #parent 
* #9675:3 ^property[0].valueCode = #967-969 
* #9678:3 "Primary effusion lymphoma"
* #9678:3 ^property[0].code = #parent 
* #9678:3 ^property[0].valueCode = #967-969 
* #9679:3 "Primär mediastinales großzelliges B-Zell-Lymphom"
* #9679:3 ^designation[0].value = "Thymisches großzelliges B-Zell-Lymphom" 
* #9679:3 ^property[0].code = #parent 
* #9679:3 ^property[0].valueCode = #967-969 
* #9680:1 "EBV-positives mukokutanes Ulkus"
* #9680:1 ^property[0].code = #parent 
* #9680:1 ^property[0].valueCode = #967-969 
* #9680:3 "Diffuses großzelliges B-Zell-Lymphom o.n.A."
* #9680:3 ^designation[0].value = "Diffuses großzelliges Lymphom o.n.A." 
* #9680:3 ^designation[1].value = "Gekerbtkerniges diffuses großzelliges Lymphom" 
* #9680:3 ^designation[2].value = "Gekerbtkerniges großzelliges Lymphom o.n.A." 
* #9680:3 ^designation[3].value = "Großzelliges B-Zell-Lymphom o.n.A." 
* #9680:3 ^designation[4].value = "Großzelliges diffuses B-Zell-Lymphom o.n.A." 
* #9680:3 ^designation[5].value = "Großzelliges diffuses B-Zell-Lymphom vom zentroblastischen Typ o.n.A." 
* #9680:3 ^designation[6].value = "Großzelliges Lymphom o.n.A." 
* #9680:3 ^designation[7].value = "Großzelliges Lymphom vom gekerbtkernigen und nichtgekerbtkernigen Typ" 
* #9680:3 ^designation[8].value = "Histiozytäres Lymphom o.n.A." 
* #9680:3 ^designation[9].value = "Histiozytisches diffuses Lymphom" 
* #9680:3 ^designation[10].value = "Nichtgekerbtkerniges diffuses großzelliges Lymphom o.n.A." 
* #9680:3 ^designation[11].value = "Nichtgekerbtkerniges diffuses Lymphom o.n.A." 
* #9680:3 ^designation[12].value = "Nichtgekerbtkerniges großzelliges Lymphom o.n.A." 
* #9680:3 ^designation[13].value = "Nichtgekerbtkerniges Lymphom o.n.A." 
* #9680:3 ^designation[14].value = "Anaplastisches großzelliges B-Zell-Lymphom" 
* #9680:3 ^designation[15].value = "B-Zell-Lymphom, nicht klassifizierbar, mit Eigenschaften zwischen denen eines diffus großzelligen B-Zell-Lymphoms und eines Burkitt-Lymphoms" 
* #9680:3 ^designation[16].value = "Diffuses großzelliges B-Zell-Lymphom, aktivierter B-Zell-Subtyp" 
* #9680:3 ^designation[17].value = "Diffuses großzelliges B-Zell-Lymphom, Keimzentrum-B-Zell-Subtyp" 
* #9680:3 ^designation[18].value = "Diffuses großzelliges B-Zell-Lymphom mit chronischer Entzündung" 
* #9680:3 ^designation[19].value = "EBV-positives diffuses großzelliges B-Zell-Lymphom des älteren Menschen" 
* #9680:3 ^designation[20].value = "Großzelliges B-Zell-Lymphom, T-Zell-reiche Variante" 
* #9680:3 ^designation[21].value = "Hochmaligne B-Zell-Lymphome mit MYC und BCL2 und/oder BCL6 Rearrangements (HGBL)" 
* #9680:3 ^designation[22].value = "Intravaskuläres großzelliges B-Zell-Lymphom" 
* #9680:3 ^designation[23].value = "Primäres diffuses großzelliges B-Zell-Lymphom des ZNS" 
* #9680:3 ^designation[24].value = "Primäres kutanes diffuses großzelliges B-Zell-Lymphom des Beines" 
* #9680:3 ^designation[25].value = "Vitreoretinales Lymphom" 
* #9680:3 ^designation[26].value = "Zentroblastisches diffuses Lymphom" 
* #9680:3 ^designation[27].value = "Zentroblastisches Lymphom o.n.A." 
* #9680:3 ^property[0].code = #parent 
* #9680:3 ^property[0].valueCode = #967-969 
* #9684:3 "Großzelliges diffuses B-Zell-Lymphom, immunoblastische Variante o.n.A."
* #9684:3 ^designation[0].value = "Großzelliges immunoblastisches Lymphom" 
* #9684:3 ^designation[1].value = "Immunoblastisches Lymphom o.n.A." 
* #9684:3 ^designation[2].value = "Immunoblastisches Sarkom" 
* #9684:3 ^property[0].code = #parent 
* #9684:3 ^property[0].valueCode = #967-969 
* #9687:3 "Burkitt-Lymphom o.n.A."
* #9687:3 ^designation[0].value = "Burkitt-Tumor" 
* #9687:3 ^designation[1].value = "Kleinzelliges nichtgekerbtkerniges malignes Lymphom vom Burkitt-Typ" 
* #9687:3 ^designation[2].value = "Undifferenziertes malignes Lymphom vom Burkitt-Typ" 
* #9687:3 ^designation[3].value = "B-Zell-Lymphom, Burkitt-ähnlich" 
* #9687:3 ^designation[4].value = "Burkitt-ähnliches Lymphom mit 11q-AberrationAkute Leukämie vom Burkitt-Typ" 
* #9687:3 ^designation[5].value = "Burkitt-ähnliches Lymphom mit 11q-AberrationAkute reifzellige lymphoblastische B-Zell-Leukämie" 
* #9687:3 ^designation[6].value = "Burkitt-ähnliches Lymphom mit 11q-AberrationB-ALL" 
* #9687:3 ^designation[7].value = "Burkitt-ähnliches Lymphom mit 11q-AberrationBurkitt-Zell-Leukämie" 
* #9687:3 ^designation[8].value = "Burkitt-ähnliches Lymphom mit 11q-AberrationFAB L3" 
* #9687:3 ^designation[9].value = "Burkitt-ähnliches Lymphom o.n.A." 
* #9687:3 ^property[0].code = #parent 
* #9687:3 ^property[0].valueCode = #967-969 
* #9688:3 "T-Zell-reiches/histiozytenreiches großzelliges B-Zell-Lymphom"
* #9688:3 ^designation[0].value = "Histiozytenreiches großzelliges B-Zell-Lymphom" 
* #9688:3 ^designation[1].value = "T-Zell-reiches großzelliges B-Zell-Lymphom" 
* #9688:3 ^property[0].code = #parent 
* #9688:3 ^property[0].valueCode = #967-969 
* #9689:3 "Marginalzonen-B-Zell-Lymphom der Milz"
* #9689:3 ^designation[0].value = "Lymphom der Milz mit villösen Lymphozyten" 
* #9689:3 ^designation[1].value = "Marginalzonen-Lymphom der Milz o.n.A." 
* #9689:3 ^property[0].code = #parent 
* #9689:3 ^property[0].valueCode = #967-969 
* #9690:3 "Follikuläres Lymphom o.n.A."
* #9690:3 ^designation[0].value = "Follikelzentrums-Lymphom o.n.A." 
* #9690:3 ^designation[1].value = "Follikuläres Follikelzentrums-Lymphom" 
* #9690:3 ^designation[2].value = "Follikuläres malignes Lymphom o.n.A." 
* #9690:3 ^designation[3].value = "Lymphozytisches noduläres Lymphom o.n.A." 
* #9690:3 ^designation[4].value = "Noduläres Lymphom o.n.A." 
* #9690:3 ^designation[5].value = "Zentroblastisch-zentrozytisches follikuläres Lymphom" 
* #9690:3 ^designation[6].value = "Follikuläres Lymphom, pädiatrischer Typ" 
* #9690:3 ^property[0].code = #parent 
* #9690:3 ^property[0].valueCode = #967-969 
* #9691:3 "Follikuläres Lymphom, Grad 2"
* #9691:3 ^designation[0].value = "Follikuläres Lymphom vom Mischzelltyp" 
* #9691:3 ^designation[1].value = "Gemischt kleinzellig-gekerbtkerniges und großzelliges follikuläres Lymphom" 
* #9691:3 ^designation[2].value = "Gemischt lymphozytisch-histiozytisches noduläres Lymphom" 
* #9691:3 ^designation[3].value = "Noduläres Lymphom vom Mischzelltyp" 
* #9691:3 ^property[0].code = #parent 
* #9691:3 ^property[0].valueCode = #967-969 
* #9695:1 "In situ follikuläre Neoplasie"
* #9695:1 ^designation[0].value = "In situ follikuläres Lymphom" 
* #9695:1 ^property[0].code = #parent 
* #9695:1 ^property[0].valueCode = #967-969 
* #9695:3 "Follikuläres Lymphom, Grad 1"
* #9695:3 ^designation[0].value = "Kleinzelliges gekerbtkerniges follikuläres Lymphom" 
* #9695:3 ^designation[1].value = "Kleinzelliges gekerbtkerniges Lymphom" 
* #9695:3 ^designation[2].value = "Lymphozytisches schlecht differenziertes noduläres Lymphom" 
* #9695:3 ^designation[3].value = "Follikuläres Lymphom, duodenaler Typ" 
* #9695:3 ^property[0].code = #parent 
* #9695:3 ^property[0].valueCode = #967-969 
* #9698:3 "Follikuläres Lymphom, Grad 3"
* #9698:3 ^designation[0].value = "Follikuläres Lymphom, Grad 3A" 
* #9698:3 ^designation[1].value = "Follikuläres Lymphom, Grad 3B" 
* #9698:3 ^designation[2].value = "Großzelliges follikuläres Lymphom o.n.A." 
* #9698:3 ^designation[3].value = "Großzelliges gekerbtkerniges follikuläres Lymphom" 
* #9698:3 ^designation[4].value = "Großzelliges nichtgekerbtkerniges follikuläres Lymphom" 
* #9698:3 ^designation[5].value = "Histiozytisches noduläres Lymphom" 
* #9698:3 ^designation[6].value = "Lymphozytisches gut differenziertes noduläres Lymphom" 
* #9698:3 ^designation[7].value = "Nichtgekerbtkerniges follikuläres Lymphom o.n.A." 
* #9698:3 ^designation[8].value = "Zentroblastisches follikuläres Lymphom" 
* #9698:3 ^designation[9].value = "Großzelliges B-Zell-Lymphom mit IRF4-Rearrangement" 
* #9698:3 ^property[0].code = #parent 
* #9698:3 ^property[0].valueCode = #967-969 
* #9699:3 "Marginalzonen-B-Zell-Lymphom o.n.A."
* #9699:3 ^designation[0].value = "Lymphom des Bronchus-assoziierten lymphatischen GewebesBALT-Lymphom" 
* #9699:3 ^designation[1].value = "Lymphom des Haut-assoziierten lymphatischen GewebesSALT-Lymphom" 
* #9699:3 ^designation[2].value = "Lymphom des Mukosa-assoziierten lymphatischen GewebesMALT-Lymphom" 
* #9699:3 ^designation[3].value = "Marginalzonen-Lymphom o.n.A." 
* #9699:3 ^designation[4].value = "Monozytoides B-Zell-LymphomNodales Marginalzonen-Lymphom" 
* #9699:3 ^designation[5].value = "Extranodales Marginalzonen-Lymphom des mukosa-assoziierten lymphatischen Gewebes" 
* #9699:3 ^designation[6].value = "Primäres choroidales Lymphom" 
* #9699:3 ^property[0].code = #parent 
* #9699:3 ^property[0].valueCode = #967-969 
* #970-971 "Reifzellige T- und NK-Zell-Lymphome"
* #970-971 ^property[0].code = #parent 
* #970-971 ^property[0].valueCode = #967-972 
* #970-971 ^property[1].code = #child 
* #970-971 ^property[1].valueCode = #9700:3 
* #970-971 ^property[2].code = #child 
* #970-971 ^property[2].valueCode = #9701:3 
* #970-971 ^property[3].code = #child 
* #970-971 ^property[3].valueCode = #9702:1 
* #970-971 ^property[4].code = #child 
* #970-971 ^property[4].valueCode = #9702:3 
* #970-971 ^property[5].code = #child 
* #970-971 ^property[5].valueCode = #9705:3 
* #970-971 ^property[6].code = #child 
* #970-971 ^property[6].valueCode = #9708:3 
* #970-971 ^property[7].code = #child 
* #970-971 ^property[7].valueCode = #9709:1 
* #970-971 ^property[8].code = #child 
* #970-971 ^property[8].valueCode = #9709:3 
* #970-971 ^property[9].code = #child 
* #970-971 ^property[9].valueCode = #9712:3 
* #970-971 ^property[10].code = #child 
* #970-971 ^property[10].valueCode = #9714:3 
* #970-971 ^property[11].code = #child 
* #970-971 ^property[11].valueCode = #9715:3 
* #970-971 ^property[12].code = #child 
* #970-971 ^property[12].valueCode = #9716:3 
* #970-971 ^property[13].code = #child 
* #970-971 ^property[13].valueCode = #9717:3 
* #970-971 ^property[14].code = #child 
* #970-971 ^property[14].valueCode = #9718:1 
* #970-971 ^property[15].code = #child 
* #970-971 ^property[15].valueCode = #9718:3 
* #970-971 ^property[16].code = #child 
* #970-971 ^property[16].valueCode = #9719:3 
* #9700:3 "Mycosis fungoides"
* #9700:3 ^designation[0].value = "Pagetoide Retikulose" 
* #9700:3 ^designation[1].value = "Granulomatöse schlaffe Haut" 
* #9700:3 ^property[0].code = #parent 
* #9700:3 ^property[0].valueCode = #970-971 
* #9701:3 "Szary-Syndrom"
* #9701:3 ^designation[0].value = "M. Szary" 
* #9701:3 ^property[0].code = #parent 
* #9701:3 ^property[0].valueCode = #970-971 
* #9702:1 "Indolente T-Zell-lymphoproliferative Krankheit des Gastrointestinaltraktes"
* #9702:1 ^property[0].code = #parent 
* #9702:1 ^property[0].valueCode = #970-971 
* #9702:3 "Reifzelliges T-Zell-Lymphom o.n.A."
* #9702:3 ^designation[0].value = "Peripheres großzelliges T-Zell-Lymphom" 
* #9702:3 ^designation[1].value = "Peripheres T-Zell-Lymphom o.n.A." 
* #9702:3 ^designation[2].value = "Pleomorphes kleinzelliges peripheres T-Zell-Lymphom" 
* #9702:3 ^designation[3].value = "Pleomorphes mittel- und großzelliges peripheres T-Zell-Lymphom" 
* #9702:3 ^designation[4].value = "T-Zell-Lymphom o.n.A." 
* #9702:3 ^designation[5].value = "T-Zonen-Lymphom" 
* #9702:3 ^designation[6].value = "Follikuläres T-Zell-Lymphom" 
* #9702:3 ^designation[7].value = "Lymphoepitheloides LymphomLennert-Lymphom" 
* #9702:3 ^designation[8].value = "Nodales peripheres T-Zell-Lymphom mit follikulärem T-Helfer-Phänotyp" 
* #9702:3 ^property[0].code = #parent 
* #9702:3 ^property[0].valueCode = #970-971 
* #9705:3 "Angioimmunoblastisches T-Zell-Lymphom"
* #9705:3 ^designation[0].value = "Angioimmunoblastisches Lymphom" 
* #9705:3 ^designation[1].value = "Peripheres T-Zell-Lymphom vom AILD-Typ (Angioimmunoblastische Lymphadenopathie mit Dysproteinämie)" 
* #9705:3 ^property[0].code = #parent 
* #9705:3 ^property[0].valueCode = #970-971 
* #9708:3 "Subkutanes pannikulitisches T-Zell-Lymphom"
* #9708:3 ^property[0].code = #parent 
* #9708:3 ^property[0].valueCode = #970-971 
* #9709:1 "Primär kutane CD4-positive klein/medium T-Zell-lymphoproliferative Erkrankung"
* #9709:1 ^designation[0].value = "Primär kutanes CD4-positives klein/medium T-Zell-Lymphom" 
* #9709:1 ^property[0].code = #parent 
* #9709:1 ^property[0].valueCode = #970-971 
* #9709:3 "Kutanes T-Zell-Lymphom"
* #9709:3 ^designation[0].value = "Hautlymphom o.n.A." 
* #9709:3 ^designation[1].value = "Primär kutanes akrales CD8-positives T-Zell-Lymphom" 
* #9709:3 ^designation[2].value = "Primär kutanes CD8-positives aggressives epidermotropes zytotoxisches T-Zell-Lymphom" 
* #9709:3 ^property[0].code = #parent 
* #9709:3 ^property[0].valueCode = #970-971 
* #9712:3 "Intravaskuläres großzelliges B-Zell-Lymphom"
* #9712:3 ^designation[0].value = "Angioendotheliomatose" 
* #9712:3 ^designation[1].value = "Angiotropes Lymphom" 
* #9712:3 ^designation[2].value = "Intravaskuläres B-Zell-Lymphom" 
* #9712:3 ^property[0].code = #parent 
* #9712:3 ^property[0].valueCode = #970-971 
* #9714:3 "Großzelliges anaplastisches T-Zell und Null-Zell-Lymphom"
* #9714:3 ^designation[0].value = "Großzelliges (Ki-1-positives) Lymphom" 
* #9714:3 ^designation[1].value = "Anaplastisches großzelliges Lymphom, ALK-positiv" 
* #9714:3 ^designation[2].value = "Anaplastisches großzelliges Lymphom o.n.A.Anaplastisches großzelliges Lymphom, CD30-positiv" 
* #9714:3 ^property[0].code = #parent 
* #9714:3 ^property[0].valueCode = #970-971 
* #9715:3 "Anaplastisches großzelliges Lymphom, ALK-negativ"
* #9715:3 ^designation[0].value = "Mammaimplantat-assoziiertes anaplastisches großzelliges Lymphom" 
* #9715:3 ^property[0].code = #parent 
* #9715:3 ^property[0].valueCode = #970-971 
* #9716:3 "Hepatosplenisches T-Zell-Lymphom"
* #9716:3 ^designation[0].value = "Hepatosplenisches Gamma-Delta-Zell-Lymphom" 
* #9716:3 ^property[0].code = #parent 
* #9716:3 ^property[0].valueCode = #970-971 
* #9717:3 "Intestinales T-Zell-Lymphom"
* #9717:3 ^designation[0].value = "Enteropathie-assoziiertes T-Zell-Lymphom" 
* #9717:3 ^designation[1].value = "Intestinales T-Zell-Lymphom mit Enteropathie" 
* #9717:3 ^designation[2].value = "Monomorphes epitheliotropes intestinales T-Zell-LymphomMEITL" 
* #9717:3 ^property[0].code = #parent 
* #9717:3 ^property[0].valueCode = #970-971 
* #9718:1 "Primär kutane CD30-positive T-zellige lymphoproliferative Erkrankung"
* #9718:1 ^designation[0].value = "Lymphomatoide Papulose" 
* #9718:1 ^property[0].code = #parent 
* #9718:1 ^property[0].valueCode = #970-971 
* #9718:3 "Primär kutanes anaplastisches großzelliges Lymphom der Haut"
* #9718:3 ^designation[0].value = "CD30+ großzelliges T-Zell-Lymphom der Haut" 
* #9718:3 ^property[0].code = #parent 
* #9718:3 ^property[0].valueCode = #970-971 
* #9719:3 "Nasales NK/T-Zell-Lymphom"
* #9719:3 ^designation[0].value = "Angiozentrisches T-Zell-Lymphom" 
* #9719:3 ^designation[1].value = "Extranodales NK/T-Zell-Lymphom vom nasalen Typ" 
* #9719:3 ^designation[2].value = "Maligne Midline-Retikulose" 
* #9719:3 ^designation[3].value = "Maligne Retikulose o.n.A." 
* #9719:3 ^designation[4].value = "Polymorphe Retikulose" 
* #9719:3 ^designation[5].value = "T/NK-Zell-Lymphom" 
* #9719:3 ^property[0].code = #parent 
* #9719:3 ^property[0].valueCode = #970-971 
* #972-972 "Lymphoblastische Lymphome der Vorläuferzellen"
* #972-972 ^property[0].code = #parent 
* #972-972 ^property[0].valueCode = #967-972 
* #972-972 ^property[1].code = #child 
* #972-972 ^property[1].valueCode = #9724:3 
* #972-972 ^property[2].code = #child 
* #972-972 ^property[2].valueCode = #9725:1 
* #972-972 ^property[3].code = #child 
* #972-972 ^property[3].valueCode = #9726:3 
* #972-972 ^property[4].code = #child 
* #972-972 ^property[4].valueCode = #9727:3 
* #9724:3 "Systemische EBV-positive T-Zell-lymphoproliferative Erkrankung der Kindheit"
* #9724:3 ^property[0].code = #parent 
* #9724:3 ^property[0].valueCode = #972-972 
* #9725:1 "Hydroa-vacciniform-lymphoproliferative Erkrankung"
* #9725:1 ^designation[0].value = "Hydroa-vacciniform-artiges Lymphom" 
* #9725:1 ^property[0].code = #parent 
* #9725:1 ^property[0].valueCode = #972-972 
* #9726:3 "Primär kutanes Gamma-Delta-T-Zell-Lymphom"
* #9726:3 ^property[0].code = #parent 
* #9726:3 ^property[0].valueCode = #972-972 
* #9727:3 "Lymphoblastisches Lymphom der Vorläuferzellen o.n.A."
* #9727:3 ^designation[0].value = "Convoluted-Cell-Lymphom" 
* #9727:3 ^designation[1].value = "Lymphoblastisches Lymphom o.n.A" 
* #9727:3 ^designation[2].value = "Lymphoblastom" 
* #9727:3 ^designation[3].value = "Blastische plasmazytoide Neoplasie der dendritischen Zellen" 
* #9727:3 ^designation[4].value = "Blastisches NK-Zell-Lymphom" 
* #9727:3 ^property[0].code = #parent 
* #9727:3 ^property[0].valueCode = #972-972 
* #973-973 "Neoplasien der Plasmazellen"
* #973-973 ^property[0].code = #parent 
* #973-973 ^property[0].valueCode = #M 
* #973-973 ^property[1].code = #child 
* #973-973 ^property[1].valueCode = #9731:3 
* #973-973 ^property[2].code = #child 
* #973-973 ^property[2].valueCode = #9732:3 
* #973-973 ^property[3].code = #child 
* #973-973 ^property[3].valueCode = #9733:3 
* #973-973 ^property[4].code = #child 
* #973-973 ^property[4].valueCode = #9734:3 
* #973-973 ^property[5].code = #child 
* #973-973 ^property[5].valueCode = #9735:3 
* #973-973 ^property[6].code = #child 
* #973-973 ^property[6].valueCode = #9737:3 
* #973-973 ^property[7].code = #child 
* #973-973 ^property[7].valueCode = #9738:3 
* #9731:3 "Plasmozytom o.n.A."
* #9731:3 ^designation[0].value = "Plasmazelltumor" 
* #9731:3 ^designation[1].value = "Plasmozytom des Knochens" 
* #9731:3 ^designation[2].value = "Solitäres Plasmozytom" 
* #9731:3 ^designation[3].value = "Solitäres Myelom" 
* #9731:3 ^property[0].code = #parent 
* #9731:3 ^property[0].valueCode = #973-973 
* #9732:3 "Plasmazellmyelom"
* #9732:3 ^designation[0].value = "Multiples Myelom" 
* #9732:3 ^designation[1].value = "Myelom o.n.A." 
* #9732:3 ^designation[2].value = "Myelomatose" 
* #9732:3 ^property[0].code = #parent 
* #9732:3 ^property[0].valueCode = #973-973 
* #9733:3 "Plasmazell-Leukämie"
* #9733:3 ^designation[0].value = "Plasmozytäre Leukämie" 
* #9733:3 ^property[0].code = #parent 
* #9733:3 ^property[0].valueCode = #973-973 
* #9734:3 "Extramedulläres Plasmozytom"
* #9734:3 ^designation[0].value = "Extraossäres Plasmozytom" 
* #9734:3 ^property[0].code = #parent 
* #9734:3 ^property[0].valueCode = #973-973 
* #9735:3 "Plasmablastisches Lymphom"
* #9735:3 ^property[0].code = #parent 
* #9735:3 ^property[0].valueCode = #973-973 
* #9737:3 "ALK-positives großzelliges B-Zell-Lymphom"
* #9737:3 ^property[0].code = #parent 
* #9737:3 ^property[0].valueCode = #973-973 
* #9738:3 "HHV8-positives diffuses großzelliges B-Zell-Lymphom"
* #9738:3 ^designation[0].value = "Großzelliges B-Zell-Lymphom bei HHV8-assoziierter multizentrischer Castleman-Krankheit" 
* #9738:3 ^property[0].code = #parent 
* #9738:3 ^property[0].valueCode = #973-973 
* #974-974 "Neoplasien der Mastzellen"
* #974-974 ^property[0].code = #parent 
* #974-974 ^property[0].valueCode = #M 
* #974-974 ^property[1].code = #child 
* #974-974 ^property[1].valueCode = #9740:1 
* #974-974 ^property[2].code = #child 
* #974-974 ^property[2].valueCode = #9740:3 
* #974-974 ^property[3].code = #child 
* #974-974 ^property[3].valueCode = #9741:1 
* #974-974 ^property[4].code = #child 
* #974-974 ^property[4].valueCode = #9741:3 
* #974-974 ^property[5].code = #child 
* #974-974 ^property[5].valueCode = #9742:3 
* #974-974 ^property[6].code = #child 
* #974-974 ^property[6].valueCode = #9749:3 
* #9740:1 "Mastozytom o.n.A."
* #9740:1 ^designation[0].value = "Mastzelltumor o.n.A." 
* #9740:1 ^designation[1].value = "Diffuse kutane Mastozytose" 
* #9740:1 ^designation[2].value = "Extrakutanes Mastozytom" 
* #9740:1 ^designation[3].value = "Kutane Mastozytose o.n.A." 
* #9740:1 ^designation[4].value = "Solitäres Mastoyztom der Haut" 
* #9740:1 ^designation[5].value = "Urticaria pigmentosa" 
* #9740:1 ^property[0].code = #parent 
* #9740:1 ^property[0].valueCode = #974-974 
* #9740:3 "Mastzellsarkom"
* #9740:3 ^designation[0].value = "Maligner Mastzelltumor" 
* #9740:3 ^designation[1].value = "Malignes Mastozytom" 
* #9740:3 ^property[0].code = #parent 
* #9740:3 ^property[0].valueCode = #974-974 
* #9741:1 "Indolente systemische Mastozytose"
* #9741:1 ^property[0].code = #parent 
* #9741:1 ^property[0].valueCode = #974-974 
* #9741:3 "Maligne Mastozytose"
* #9741:3 ^designation[0].value = "Systemische Mastozytose" 
* #9741:3 ^designation[1].value = "Aggressive systemische Mastozytose" 
* #9741:3 ^designation[2].value = "Systemische Mastozytose mit AHNMD" 
* #9741:3 ^designation[3].value = "Systemische Mastozytose mit assoziierter hämatologischer klonaler Nicht-Mastzell-Krankheit" 
* #9741:3 ^property[0].code = #parent 
* #9741:3 ^property[0].valueCode = #974-974 
* #9742:3 "Mastzell-Leukämie"
* #9742:3 ^property[0].code = #parent 
* #9742:3 ^property[0].valueCode = #974-974 
* #9749:3 "Erdheim-Chester-Krankheit"
* #9749:3 ^property[0].code = #parent 
* #9749:3 ^property[0].valueCode = #974-974 
* #975-975 "Neoplasien der Histiozyten und akzessorischer lymphoider Zellen"
* #975-975 ^property[0].code = #parent 
* #975-975 ^property[0].valueCode = #M 
* #975-975 ^property[1].code = #child 
* #975-975 ^property[1].valueCode = #9750:3 
* #975-975 ^property[2].code = #child 
* #975-975 ^property[2].valueCode = #9751:1 
* #975-975 ^property[3].code = #child 
* #975-975 ^property[3].valueCode = #9751:3 
* #975-975 ^property[4].code = #child 
* #975-975 ^property[4].valueCode = #9755:3 
* #975-975 ^property[5].code = #child 
* #975-975 ^property[5].valueCode = #9756:3 
* #975-975 ^property[6].code = #child 
* #975-975 ^property[6].valueCode = #9757:3 
* #975-975 ^property[7].code = #child 
* #975-975 ^property[7].valueCode = #9758:3 
* #975-975 ^property[8].code = #child 
* #975-975 ^property[8].valueCode = #9759:3 
* #9750:3 "Maligne Histiozytose"
* #9750:3 ^designation[0].value = "Histiozytäre medulläre Retikulose" 
* #9750:3 ^property[0].code = #parent 
* #9750:3 ^property[0].valueCode = #975-975 
* #9751:1 "Langerhans-Zell-Histiozytose o.n.A."
* #9751:1 ^designation[0].value = "Langerhans-Zell-Histiozytose mehrerer Knochen" 
* #9751:1 ^designation[1].value = "Langerhans-Zell-Histiozytose nur eines Knochens" 
* #9751:1 ^property[0].code = #parent 
* #9751:1 ^property[0].valueCode = #975-975 
* #9751:3 "Disseminierte Langerhans-Zell-Histiozytose"
* #9751:3 ^designation[0].value = "Langerhans-Zell-Granulomatose" 
* #9751:3 ^designation[1].value = "Abt-Letterer-Siwe-Krankheit" 
* #9751:3 ^designation[2].value = "Akute progressive Histiozytose X" 
* #9751:3 ^designation[3].value = "Eosinophiles Granulom" 
* #9751:3 ^designation[4].value = "Hand-Schüller-Christian-Krankheit" 
* #9751:3 ^designation[5].value = "Histiozytose X o.n.A." 
* #9751:3 ^designation[6].value = "Nichtlipidhaltige Retikuloendotheliose" 
* #9751:3 ^property[0].code = #parent 
* #9751:3 ^property[0].valueCode = #975-975 
* #9755:3 "Histiozytäres Sarkom"
* #9755:3 ^designation[0].value = "Echtes histiozytisches Lymphom" 
* #9755:3 ^property[0].code = #parent 
* #9755:3 ^property[0].valueCode = #975-975 
* #9756:3 "Langerhans-Zell-Sarkom"
* #9756:3 ^property[0].code = #parent 
* #9756:3 ^property[0].valueCode = #975-975 
* #9757:3 "Sarkom der dendritischen Retikulumzellen"
* #9757:3 ^designation[0].value = "Retikulumzellsarkom" 
* #9757:3 ^designation[1].value = "Dendriten-Zell-Sarkom" 
* #9757:3 ^designation[2].value = "Indeterminanter dendritischer Zelltumor" 
* #9757:3 ^property[0].code = #parent 
* #9757:3 ^property[0].valueCode = #975-975 
* #9758:3 "Follikuläres Dendriten-Zell-Sarkom"
* #9758:3 ^designation[0].value = "Follikulärer Dendriten-Zell-Tumor" 
* #9758:3 ^property[0].code = #parent 
* #9758:3 ^property[0].valueCode = #975-975 
* #9759:3 "Fibroblastischer retikulärer Zelltumor"
* #9759:3 ^property[0].code = #parent 
* #9759:3 ^property[0].valueCode = #975-975 
* #976-976 "Immunoproliferative Krankheiten"
* #976-976 ^property[0].code = #parent 
* #976-976 ^property[0].valueCode = #M 
* #976-976 ^property[1].code = #child 
* #976-976 ^property[1].valueCode = #9760:3 
* #976-976 ^property[2].code = #child 
* #976-976 ^property[2].valueCode = #9761:1 
* #976-976 ^property[3].code = #child 
* #976-976 ^property[3].valueCode = #9761:3 
* #976-976 ^property[4].code = #child 
* #976-976 ^property[4].valueCode = #9762:3 
* #976-976 ^property[5].code = #child 
* #976-976 ^property[5].valueCode = #9764:3 
* #976-976 ^property[6].code = #child 
* #976-976 ^property[6].valueCode = #9765:1 
* #976-976 ^property[7].code = #child 
* #976-976 ^property[7].valueCode = #9766:1 
* #976-976 ^property[8].code = #child 
* #976-976 ^property[8].valueCode = #9766:3 
* #976-976 ^property[9].code = #child 
* #976-976 ^property[9].valueCode = #9767:1 
* #976-976 ^property[10].code = #child 
* #976-976 ^property[10].valueCode = #9768:1 
* #976-976 ^property[11].code = #child 
* #976-976 ^property[11].valueCode = #9769:1 
* #9760:3 "Immunoproliferative Krankheit o.n.A."
* #9760:3 ^property[0].code = #parent 
* #9760:3 ^property[0].valueCode = #976-976 
* #9761:1 "Monoklonale IgM-Gammopathie unbestimmter Signifikanz"
* #9761:1 ^property[0].code = #parent 
* #9761:1 ^property[0].valueCode = #976-976 
* #9761:3 "Waldenström-Makroglobulinämie"
* #9761:3 ^property[0].code = #parent 
* #9761:3 ^property[0].valueCode = #976-976 
* #9762:3 "Schwerketten-Krankheit o.n.A."
* #9762:3 ^designation[0].value = "Alpha-Schwerketten-Krankheit" 
* #9762:3 ^designation[1].value = "Gamma-Schwerketten-KrankheitFranklin-Krankheit" 
* #9762:3 ^designation[2].value = "My-Schwerketten-Krankheit" 
* #9762:3 ^property[0].code = #parent 
* #9762:3 ^property[0].valueCode = #976-976 
* #9764:3 "Immunoproliferative Krankheit des Dünndarms"
* #9764:3 ^designation[0].value = "Mittelmeer-Lymphom" 
* #9764:3 ^property[0].code = #parent 
* #9764:3 ^property[0].valueCode = #976-976 
* #9765:1 "Monoklonale Gammopathie unbestimmter Signifikanz o.n.A."
* #9765:1 ^designation[0].value = "MGUS" 
* #9765:1 ^designation[1].value = "Monoklonale Gammopathie o.n.A." 
* #9765:1 ^property[0].code = #parent 
* #9765:1 ^property[0].valueCode = #976-976 
* #9766:1 "Angiozentrische immunoproliferative Veränderung"
* #9766:1 ^designation[0].value = "Lymphomatoide Granulomatose o.n.A." 
* #9766:1 ^designation[1].value = "Lymphomatoide Granulomatose, Grad 1" 
* #9766:1 ^designation[2].value = "Lymphomatoide Granulomatose, Grad 2" 
* #9766:1 ^property[0].code = #parent 
* #9766:1 ^property[0].valueCode = #976-976 
* #9766:3 "Lymphomatoide Granulomatose, Grad 3"
* #9766:3 ^property[0].code = #parent 
* #9766:3 ^property[0].valueCode = #976-976 
* #9767:1 "Angioimmunoblastische Lymphadenopathie"
* #9767:1 ^designation[0].value = "AIL" 
* #9767:1 ^designation[1].value = "Immunoblastische LymphadenopathieIBL" 
* #9767:1 ^property[0].code = #parent 
* #9767:1 ^property[0].valueCode = #976-976 
* #9768:1 "T-Gamma-lymphoproliferative Krankheit"
* #9768:1 ^property[0].code = #parent 
* #9768:1 ^property[0].valueCode = #976-976 
* #9769:1 "Immunglobulin-Ablagerungs-Krankheit"
* #9769:1 ^designation[0].value = "Primäre Amyloidose" 
* #9769:1 ^designation[1].value = "Systemische Leichtketten-Krankheit" 
* #9769:1 ^property[0].code = #parent 
* #9769:1 ^property[0].valueCode = #976-976 
* #980-994 "Leukämien"
* #980-994 ^property[0].code = #parent 
* #980-994 ^property[0].valueCode = #M 
* #980-994 ^property[1].code = #child 
* #980-994 ^property[1].valueCode = #980-980 
* #980-994 ^property[2].code = #child 
* #980-994 ^property[2].valueCode = #981-983 
* #980-994 ^property[3].code = #child 
* #980-994 ^property[3].valueCode = #984-993 
* #980-994 ^property[4].code = #child 
* #980-994 ^property[4].valueCode = #994-994 
* #980-980 "Leukämien o.n.A."
* #980-980 ^property[0].code = #parent 
* #980-980 ^property[0].valueCode = #980-994 
* #980-980 ^property[1].code = #child 
* #980-980 ^property[1].valueCode = #9800:3 
* #980-980 ^property[2].code = #child 
* #980-980 ^property[2].valueCode = #9801:3 
* #980-980 ^property[3].code = #child 
* #980-980 ^property[3].valueCode = #9805:3 
* #980-980 ^property[4].code = #child 
* #980-980 ^property[4].valueCode = #9806:3 
* #980-980 ^property[5].code = #child 
* #980-980 ^property[5].valueCode = #9807:3 
* #980-980 ^property[6].code = #child 
* #980-980 ^property[6].valueCode = #9808:3 
* #980-980 ^property[7].code = #child 
* #980-980 ^property[7].valueCode = #9809:3 
* #9800:3 "Leukämie o.n.A."
* #9800:3 ^designation[0].value = "Aleukämische Leukämie o.n.A." 
* #9800:3 ^designation[1].value = "Chronische Leukämie o.n.A." 
* #9800:3 ^designation[2].value = "Subakute Leukämie o.n.A." 
* #9800:3 ^property[0].code = #parent 
* #9800:3 ^property[0].valueCode = #980-980 
* #9801:3 "Akute Leukämie o.n.A."
* #9801:3 ^designation[0].value = "Blasten-Leukämie" 
* #9801:3 ^designation[1].value = "Stammzell-Leukämie" 
* #9801:3 ^designation[2].value = "Undifferenzierte Leukämie" 
* #9801:3 ^property[0].code = #parent 
* #9801:3 ^property[0].valueCode = #980-980 
* #9805:3 "Akute biphänotypische Leukämie"
* #9805:3 ^designation[0].value = "Akute Bilineage-Leukämie" 
* #9805:3 ^designation[1].value = "Akute gemischtzellige Leukämie" 
* #9805:3 ^property[0].code = #parent 
* #9805:3 ^property[0].valueCode = #980-980 
* #9806:3 "Akute gemischt-phänotyptische Leukämie mit t(9;22)(q34;q11.2); BCR-ABL1"
* #9806:3 ^property[0].code = #parent 
* #9806:3 ^property[0].valueCode = #980-980 
* #9807:3 "Akute gemischt-phänotyptische Leukämie mit t(v;11q23); MLL rearranged"
* #9807:3 ^property[0].code = #parent 
* #9807:3 ^property[0].valueCode = #980-980 
* #9808:3 "Akute gemischt-phänotyptische Leukämie B/myeloisch, o.n.A."
* #9808:3 ^property[0].code = #parent 
* #9808:3 ^property[0].valueCode = #980-980 
* #9809:3 "Akute gemischt-phänotyptische Leukämie T/myeloisch, o.n.A."
* #9809:3 ^property[0].code = #parent 
* #9809:3 ^property[0].valueCode = #980-980 
* #981-983 "Lymphatische Leukämien"
* #981-983 ^property[0].code = #parent 
* #981-983 ^property[0].valueCode = #980-994 
* #981-983 ^property[1].code = #child 
* #981-983 ^property[1].valueCode = #9811:3 
* #981-983 ^property[2].code = #child 
* #981-983 ^property[2].valueCode = #9812:3 
* #981-983 ^property[3].code = #child 
* #981-983 ^property[3].valueCode = #9813:3 
* #981-983 ^property[4].code = #child 
* #981-983 ^property[4].valueCode = #9814:3 
* #981-983 ^property[5].code = #child 
* #981-983 ^property[5].valueCode = #9815:3 
* #981-983 ^property[6].code = #child 
* #981-983 ^property[6].valueCode = #9816:3 
* #981-983 ^property[7].code = #child 
* #981-983 ^property[7].valueCode = #9817:3 
* #981-983 ^property[8].code = #child 
* #981-983 ^property[8].valueCode = #9818:3 
* #981-983 ^property[9].code = #child 
* #981-983 ^property[9].valueCode = #9819:3 
* #981-983 ^property[10].code = #child 
* #981-983 ^property[10].valueCode = #9820:3 
* #981-983 ^property[11].code = #child 
* #981-983 ^property[11].valueCode = #9823:1 
* #981-983 ^property[12].code = #child 
* #981-983 ^property[12].valueCode = #9823:3 
* #981-983 ^property[13].code = #child 
* #981-983 ^property[13].valueCode = #9827:3 
* #981-983 ^property[14].code = #child 
* #981-983 ^property[14].valueCode = #9831:3 
* #981-983 ^property[15].code = #child 
* #981-983 ^property[15].valueCode = #9832:3 
* #981-983 ^property[16].code = #child 
* #981-983 ^property[16].valueCode = #9833:3 
* #981-983 ^property[17].code = #child 
* #981-983 ^property[17].valueCode = #9834:3 
* #981-983 ^property[18].code = #child 
* #981-983 ^property[18].valueCode = #9835:3 
* #981-983 ^property[19].code = #child 
* #981-983 ^property[19].valueCode = #9837:3 
* #9811:3 "B-lymphoblastische/s Leukämie/Lymphom o.n.A."
* #9811:3 ^designation[0].value = "B-lymphoblastisches Lymphom vom Vorläufer-Typ" 
* #9811:3 ^designation[1].value = "c-ALL" 
* #9811:3 ^designation[2].value = "Common Vorläufer-B-ALL" 
* #9811:3 ^designation[3].value = "Common-ALL" 
* #9811:3 ^designation[4].value = "Pre-B-ALL" 
* #9811:3 ^designation[5].value = "Pre-pre-B-ALL" 
* #9811:3 ^designation[6].value = "Pro-B-ALL" 
* #9811:3 ^designation[7].value = "B-lymphoblastische/s Leukämie/Lymphom mit iAMP21" 
* #9811:3 ^property[0].code = #parent 
* #9811:3 ^property[0].valueCode = #981-983 
* #9812:3 "B-lymphoblastische/s Leukämie/Lymphom mit t(9;22)(q34;q11.2); BCR-ABL1"
* #9812:3 ^property[0].code = #parent 
* #9812:3 ^property[0].valueCode = #981-983 
* #9813:3 "B-lymphoblastische/s Leukämie/Lymphom mit t(v;11q23); MLL rearranged"
* #9813:3 ^property[0].code = #parent 
* #9813:3 ^property[0].valueCode = #981-983 
* #9814:3 "B-lymphoblastische/s Leukämie/Lymphom mit t(12;21)(p13;q22); TEL-AML1 (ETV6-RUNX1)"
* #9814:3 ^property[0].code = #parent 
* #9814:3 ^property[0].valueCode = #981-983 
* #9815:3 "B-lymphoblastische/s Leukämie/Lymphom mit Hyperdiploidie"
* #9815:3 ^property[0].code = #parent 
* #9815:3 ^property[0].valueCode = #981-983 
* #9816:3 "B-lymphoblastische/s Leukämie/Lymphom mit Hypodiploidie (Hypodiploid-ALL )"
* #9816:3 ^property[0].code = #parent 
* #9816:3 ^property[0].valueCode = #981-983 
* #9817:3 "B-lymphoblastische/s Leukämie/Lymphom mit t(5;14)(q31;q32); IL3-IGH"
* #9817:3 ^property[0].code = #parent 
* #9817:3 ^property[0].valueCode = #981-983 
* #9818:3 "B-lymphoblastische/s Leukämie/Lymphom mit t(1;19)(q23;p13.3); E2A-PBX1 (TCF3-PBX1)"
* #9818:3 ^property[0].code = #parent 
* #9818:3 ^property[0].valueCode = #981-983 
* #9819:3 "BCR-ABL1-like B-lymphoblastische/s Leukämie/Lymphom"
* #9819:3 ^property[0].code = #parent 
* #9819:3 ^property[0].valueCode = #981-983 
* #9820:3 "Lymphatische Leukämie o.n.A."
* #9820:3 ^designation[0].value = "Aleukämische lymphoide LeukämieAleukämische lymphozytische Leukämie" 
* #9820:3 ^designation[1].value = "Aleukämische lymphoide LeukämieAleukämische lymphatische Leukämie" 
* #9820:3 ^designation[2].value = "Lymphoide Leukämie o.n.A." 
* #9820:3 ^designation[3].value = "Lymphosarkomzell-Leukämie" 
* #9820:3 ^designation[4].value = "Lymphozytische Leukämie o.n.A." 
* #9820:3 ^designation[5].value = "Subakute lymphoide LeukämieSubakute lymphatische Leukämie" 
* #9820:3 ^designation[6].value = "Subakute lymphoide LeukämieSubakute lymphozytische Leukämie" 
* #9820:3 ^property[0].code = #parent 
* #9820:3 ^property[0].valueCode = #981-983 
* #9823:1 "Monoklonale B-Zell-Lymphozytose vom CLL-Typ"
* #9823:1 ^property[0].code = #parent 
* #9823:1 ^property[0].valueCode = #981-983 
* #9823:3 "Chronische lymphatische B-Zell-Leukämie/kleinzelliges lymphozytisches Lymphom"
* #9823:3 ^designation[0].value = "Chronische lymphoide Leukämie" 
* #9823:3 ^designation[1].value = "Chronische lymphatische Leukämie" 
* #9823:3 ^designation[2].value = "Chronische lymphozytische B-Zell-Leukämie" 
* #9823:3 ^designation[3].value = "Chronische lymphozytische Leukämie" 
* #9823:3 ^designation[4].value = "Kleinzelliges Lymphom o.n.A." 
* #9823:3 ^designation[5].value = "Kleinzelliges diffuses Lymphom o.n.A." 
* #9823:3 ^designation[6].value = "Kleinzelliges lymphozytisches B-Zell-Lymphom o.n.A." 
* #9823:3 ^designation[7].value = "Kleinzelliges lymphozytisches diffuses Lymphom" 
* #9823:3 ^designation[8].value = "Kleinzelliges lymphozytisches Lymphom o.n.A." 
* #9823:3 ^designation[9].value = "Lymphozytisches diffuses Lymphom o.n.A." 
* #9823:3 ^designation[10].value = "Lymphozytisches gut differenziertes diffuses Lymphom" 
* #9823:3 ^designation[11].value = "Lymphozytisches Lymphom o.n.A." 
* #9823:3 ^property[0].code = #parent 
* #9823:3 ^property[0].valueCode = #981-983 
* #9827:3 "Adulte(s) T-Zell-Lymphom/Leukämie (HTLV1-positiv)"
* #9827:3 ^designation[0].value = "Adulte T-Zell-Leukämie" 
* #9827:3 ^designation[1].value = "Adultes T-Zell-Lymphom" 
* #9827:3 ^designation[2].value = "Adulte(s) T-Zell-Lymphom/Leukämie" 
* #9827:3 ^property[0].code = #parent 
* #9827:3 ^property[0].valueCode = #981-983 
* #9831:3 "Lymphatische T-Zell-Leukämie vom grobgranulären Typ"
* #9831:3 ^designation[0].value = "Grobgranuläre lymphatische T-Zell-Lymphozytose" 
* #9831:3 ^designation[1].value = "Lymphatische Leukämie vom grobgranulären Typ o.n.A." 
* #9831:3 ^designation[2].value = "Lymphatische NK-Zell-Leukämie vom grobgranulären Typ" 
* #9831:3 ^designation[3].value = "Chronische lymphoproliferative Krankheit der NK-Zellen" 
* #9831:3 ^property[0].code = #parent 
* #9831:3 ^property[0].valueCode = #981-983 
* #9832:3 "Prolymphozytenleukämie o.n.A."
* #9832:3 ^property[0].code = #parent 
* #9832:3 ^property[0].valueCode = #981-983 
* #9833:3 "Prolymphozytenleukämie vom B-Zell-Typ"
* #9833:3 ^property[0].code = #parent 
* #9833:3 ^property[0].valueCode = #981-983 
* #9834:3 "Prolymphozytenleukämie vom T-Zell-Typ"
* #9834:3 ^property[0].code = #parent 
* #9834:3 ^property[0].valueCode = #981-983 
* #9835:3 "Vorläuferzell-lymphoblastische Leukämie o.n.A."
* #9835:3 ^designation[0].value = "Akute lymphatische Leukämie" 
* #9835:3 ^designation[1].value = "Akute Lymphoblastenleukämie, L2 o.n.A." 
* #9835:3 ^designation[2].value = "Akute lymphoblastische Leukämie o.n.A." 
* #9835:3 ^designation[3].value = "Akute lymphoblastische Leukämie vom Vorläuferzell-Typ" 
* #9835:3 ^designation[4].value = "Akute lymphoide Leukämie" 
* #9835:3 ^designation[5].value = "Akute lymphozytische Leukämie" 
* #9835:3 ^designation[6].value = "Akute(s) lymphoblastische(s) Leukämie-Lymphom o.n.A." 
* #9835:3 ^designation[7].value = "FAB L1" 
* #9835:3 ^designation[8].value = "FAB L2" 
* #9835:3 ^designation[9].value = "Lymphoblastische Leukämie o.n.A." 
* #9835:3 ^designation[10].value = "Nicht phänotypisierte Vorläuferzell-lymphoblastische Leukämie" 
* #9835:3 ^property[0].code = #parent 
* #9835:3 ^property[0].valueCode = #981-983 
* #9837:3 "Vorläufer-T-lymphoblastische Leukämie"
* #9837:3 ^designation[0].value = "Cortical T-ALL" 
* #9837:3 ^designation[1].value = "Pre-T-ALL" 
* #9837:3 ^designation[2].value = "Pro-T-ALL" 
* #9837:3 ^designation[3].value = "T-ALL" 
* #9837:3 ^designation[4].value = "T-lymphoblastisches Lymphom vom Vorläuferzell-Typ" 
* #9837:3 ^designation[5].value = "Frühe Vorläufer-T-lymphoblastische Leukämie" 
* #9837:3 ^designation[6].value = "T-lymphoblastische/s Leukämie/Lymphom" 
* #9837:3 ^property[0].code = #parent 
* #9837:3 ^property[0].valueCode = #981-983 
* #984-993 "Myeloische Leukämien"
* #984-993 ^property[0].code = #parent 
* #984-993 ^property[0].valueCode = #980-994 
* #984-993 ^property[1].code = #child 
* #984-993 ^property[1].valueCode = #9840:3 
* #984-993 ^property[2].code = #child 
* #984-993 ^property[2].valueCode = #9860:3 
* #984-993 ^property[3].code = #child 
* #984-993 ^property[3].valueCode = #9861:3 
* #984-993 ^property[4].code = #child 
* #984-993 ^property[4].valueCode = #9863:3 
* #984-993 ^property[5].code = #child 
* #984-993 ^property[5].valueCode = #9865:3 
* #984-993 ^property[6].code = #child 
* #984-993 ^property[6].valueCode = #9866:3 
* #984-993 ^property[7].code = #child 
* #984-993 ^property[7].valueCode = #9867:3 
* #984-993 ^property[8].code = #child 
* #984-993 ^property[8].valueCode = #9869:3 
* #984-993 ^property[9].code = #child 
* #984-993 ^property[9].valueCode = #9870:3 
* #984-993 ^property[10].code = #child 
* #984-993 ^property[10].valueCode = #9871:3 
* #984-993 ^property[11].code = #child 
* #984-993 ^property[11].valueCode = #9872:3 
* #984-993 ^property[12].code = #child 
* #984-993 ^property[12].valueCode = #9873:3 
* #984-993 ^property[13].code = #child 
* #984-993 ^property[13].valueCode = #9874:3 
* #984-993 ^property[14].code = #child 
* #984-993 ^property[14].valueCode = #9875:3 
* #984-993 ^property[15].code = #child 
* #984-993 ^property[15].valueCode = #9876:3 
* #984-993 ^property[16].code = #child 
* #984-993 ^property[16].valueCode = #9877:3 
* #984-993 ^property[17].code = #child 
* #984-993 ^property[17].valueCode = #9878:3 
* #984-993 ^property[18].code = #child 
* #984-993 ^property[18].valueCode = #9879:3 
* #984-993 ^property[19].code = #child 
* #984-993 ^property[19].valueCode = #9891:3 
* #984-993 ^property[20].code = #child 
* #984-993 ^property[20].valueCode = #9895:3 
* #984-993 ^property[21].code = #child 
* #984-993 ^property[21].valueCode = #9896:3 
* #984-993 ^property[22].code = #child 
* #984-993 ^property[22].valueCode = #9897:3 
* #984-993 ^property[23].code = #child 
* #984-993 ^property[23].valueCode = #9898:1 
* #984-993 ^property[24].code = #child 
* #984-993 ^property[24].valueCode = #9898:3 
* #984-993 ^property[25].code = #child 
* #984-993 ^property[25].valueCode = #9910:3 
* #984-993 ^property[26].code = #child 
* #984-993 ^property[26].valueCode = #9911:3 
* #984-993 ^property[27].code = #child 
* #984-993 ^property[27].valueCode = #9912:3 
* #984-993 ^property[28].code = #child 
* #984-993 ^property[28].valueCode = #9920:3 
* #984-993 ^property[29].code = #child 
* #984-993 ^property[29].valueCode = #9930:3 
* #984-993 ^property[30].code = #child 
* #984-993 ^property[30].valueCode = #9931:3 
* #9840:3 "Akute erythroide Leukämie"
* #9840:3 ^designation[0].value = "Akute Erythrämie" 
* #9840:3 ^designation[1].value = "Akute erythrämische Myelose" 
* #9840:3 ^designation[2].value = "Akute myeloische Leukämie, M6-Typ" 
* #9840:3 ^designation[3].value = "AML M6" 
* #9840:3 ^designation[4].value = "Di-Guglielmo-Krankheit" 
* #9840:3 ^designation[5].value = "Erythrämische Myelose o.n.A." 
* #9840:3 ^designation[6].value = "Erythroleukämie" 
* #9840:3 ^designation[7].value = "FAB M6" 
* #9840:3 ^designation[8].value = "M6A" 
* #9840:3 ^designation[9].value = "M6B" 
* #9840:3 ^property[0].code = #parent 
* #9840:3 ^property[0].valueCode = #984-993 
* #9860:3 "Myeloische Leukämie o.n.A."
* #9860:3 ^designation[0].value = "Granulozytäre Leukämie o.n.A." 
* #9860:3 ^designation[1].value = "Myelogene Leukämie o.n.A." 
* #9860:3 ^designation[2].value = "Myelomonozytäre Leukämie o.n.A." 
* #9860:3 ^designation[3].value = "Myelozytäre Leukämie o.n.A." 
* #9860:3 ^designation[4].value = "Nichtlymphozytäre Leukämie o.n.A." 
* #9860:3 ^designation[5].value = "Aleukämische Monozytenleukämie" 
* #9860:3 ^designation[6].value = "Aleukämische myeloische LeukämieAleukämische granulozytäre Leukämie" 
* #9860:3 ^designation[7].value = "Aleukämische myeloische LeukämieAleukämische myelogene Leukämie" 
* #9860:3 ^designation[8].value = "Chronische Monozytenleukämie" 
* #9860:3 ^designation[9].value = "Eosinophilenleukämie o.n.A." 
* #9860:3 ^designation[10].value = "Monozytäre Leukämie o.n.A." 
* #9860:3 ^designation[11].value = "Subakute Monozytenleukämie" 
* #9860:3 ^designation[12].value = "Subakute myeloische LeukämieSubakute granulozytäre Leukämie" 
* #9860:3 ^designation[13].value = "Subakute myeloische LeukämieSubakute myelogene Leukämie" 
* #9860:3 ^property[0].code = #parent 
* #9860:3 ^property[0].valueCode = #984-993 
* #9861:3 "Akute myeloische Leukämie o.n.A."
* #9861:3 ^designation[0].value = "Akute granulozytäre Leukämie" 
* #9861:3 ^designation[1].value = "Akute myelogene Leukämie" 
* #9861:3 ^designation[2].value = "Akute myelozytäre Leukämie" 
* #9861:3 ^designation[3].value = "Akute nichtlymphozytäre Leukämie" 
* #9861:3 ^property[0].code = #parent 
* #9861:3 ^property[0].valueCode = #984-993 
* #9863:3 "Chronische myeloische Leukämie o.n.A."
* #9863:3 ^designation[0].value = "CML" 
* #9863:3 ^designation[1].value = "Chronische granulozytäre Leukämie o.n.A." 
* #9863:3 ^designation[2].value = "Chronische myelogene Leukämie o.n.A." 
* #9863:3 ^designation[3].value = "Chronische myelozytäre Leukämie o.n.A." 
* #9863:3 ^property[0].code = #parent 
* #9863:3 ^property[0].valueCode = #984-993 
* #9865:3 "Akute myeloische Leukämie mit t(6;9)(p23;q34); DEK-NUP214"
* #9865:3 ^property[0].code = #parent 
* #9865:3 ^property[0].valueCode = #984-993 
* #9866:3 "Akute Promyelozytenleukämie, t(15;17)(q22;q11-12)"
* #9866:3 ^designation[0].value = "Akute myeloische Leukämie, t(15;17)(q22;q11-12)" 
* #9866:3 ^designation[1].value = "Akute myeloische Leukämie, PML/RAR-alpha" 
* #9866:3 ^designation[2].value = "Akute Promyelozytenleukämie o.n.A." 
* #9866:3 ^designation[3].value = "Akute Promyelozytenleukämie, PML/RAR- alpha" 
* #9866:3 ^designation[4].value = "FAB M3" 
* #9866:3 ^property[0].code = #parent 
* #9866:3 ^property[0].valueCode = #984-993 
* #9867:3 "Akute myelomonozytäre Leukämie"
* #9867:3 ^designation[0].value = "FAB M4" 
* #9867:3 ^property[0].code = #parent 
* #9867:3 ^property[0].valueCode = #984-993 
* #9869:3 "Akute myeloische Leukämie mit inv(3)(q21q26.2) oder t(3;3)(q21;q26.2); RPN1-EVI1"
* #9869:3 ^property[0].code = #parent 
* #9869:3 ^property[0].valueCode = #984-993 
* #9870:3 "Akute Basophilenleukämie"
* #9870:3 ^property[0].code = #parent 
* #9870:3 ^property[0].valueCode = #984-993 
* #9871:3 "Akute myelomonozytäre Leukämie mit Eosinophilie"
* #9871:3 ^designation[0].value = "Akute myeloische Leukämie, CBF-beta/MYH11" 
* #9871:3 ^designation[1].value = "Akute myeloische Leukämie, inv(16)(p13;q22)" 
* #9871:3 ^designation[2].value = "Akute myeloische Leukämie, t(16;16)(p13;q11)" 
* #9871:3 ^designation[3].value = "Akute myelomonozytäre Leukämie mit anormalen Eosinophilen" 
* #9871:3 ^designation[4].value = "FAB M4Eo" 
* #9871:3 ^property[0].code = #parent 
* #9871:3 ^property[0].valueCode = #984-993 
* #9872:3 "Akute myeloische Leukämie mit minimaler Ausreifung"
* #9872:3 ^designation[0].value = "Akute blastär-undifferenzierte myeloische Leukämie" 
* #9872:3 ^designation[1].value = "FAB M0" 
* #9872:3 ^property[0].code = #parent 
* #9872:3 ^property[0].valueCode = #984-993 
* #9873:3 "Akute myeloische Leukämie ohne Ausreifung"
* #9873:3 ^designation[0].value = "FAB M1" 
* #9873:3 ^property[0].code = #parent 
* #9873:3 ^property[0].valueCode = #984-993 
* #9874:3 "Akute myeloische Leukämie mit Ausreifung"
* #9874:3 ^designation[0].value = "FAB M2 o.n.A." 
* #9874:3 ^property[0].code = #parent 
* #9874:3 ^property[0].valueCode = #984-993 
* #9875:3 "Chronische myeloische Leukämie, BCR/ABL positiv"
* #9875:3 ^designation[0].value = "Chronische granulozytäre Leukämie, BCR/ABL" 
* #9875:3 ^designation[1].value = "Chronische granulozytäre Leukämie, Philadelphia-Chromosom positiv" 
* #9875:3 ^designation[2].value = "Chronische granulozytäre Leukämie, t(9;22)(q34;q11)" 
* #9875:3 ^designation[3].value = "Chronische myeloische Leukämie, Philadelphia-Chromosom positiv" 
* #9875:3 ^designation[4].value = "Chronische myeloische Leukämie, t(9;22)(q34;q11)" 
* #9875:3 ^property[0].code = #parent 
* #9875:3 ^property[0].valueCode = #984-993 
* #9876:3 "Atypische chronische myeloische Leukämie, BCR/ABL negativ"
* #9876:3 ^designation[0].value = "Atypische chronische myeloische Leukämie, Philadelphia-Chromosom negativ" 
* #9876:3 ^property[0].code = #parent 
* #9876:3 ^property[0].valueCode = #984-993 
* #9877:3 "Akute myeloische Leukämie mit mutiertem NPM1"
* #9877:3 ^property[0].code = #parent 
* #9877:3 ^property[0].valueCode = #984-993 
* #9878:3 "Akute myeloische Leukämie mit biallelischer CEBPA-Mutation"
* #9878:3 ^property[0].code = #parent 
* #9878:3 ^property[0].valueCode = #984-993 
* #9879:3 "Akute myeloische Leukämie mit mutiertem RUNX1"
* #9879:3 ^property[0].code = #parent 
* #9879:3 ^property[0].valueCode = #984-993 
* #9891:3 "Akute Monozytenleukämie"
* #9891:3 ^designation[0].value = "Akute Monoblastenleukämie" 
* #9891:3 ^designation[1].value = "FAB M5" 
* #9891:3 ^designation[2].value = "Monoblastenleukämie o.n.A." 
* #9891:3 ^designation[3].value = "Akute monoblastische und monozytische Leukämie" 
* #9891:3 ^property[0].code = #parent 
* #9891:3 ^property[0].valueCode = #984-993 
* #9895:3 "Akute myeloische Leukämie mit Myelodysplasie-ähnlichen Veränderungen"
* #9895:3 ^designation[0].value = "Akute myeloische Leukämie mit Mehrlinien-Dysplasie" 
* #9895:3 ^designation[1].value = "Akute myeloische Leukämie mit vorangegangenem myelodysplastischem Syndrom" 
* #9895:3 ^designation[2].value = "Akute myeloische Leukämie ohne vorangegangenes myelodysplastisches Syndrom" 
* #9895:3 ^property[0].code = #parent 
* #9895:3 ^property[0].valueCode = #984-993 
* #9896:3 "Akute myeloische Leukämie, t(8;21)(q22;q22)"
* #9896:3 ^designation[0].value = "Akute myeloische Leukämie, AML1(CBF- alpha)/ETO" 
* #9896:3 ^designation[1].value = "Akute myeloische Leukämie mit t(8;21)(q22;q22); RUNX1-RUNX1T1" 
* #9896:3 ^designation[2].value = "FAB M2, AML1(CBF-alpha)/ETO" 
* #9896:3 ^designation[3].value = "FAB M2, t(8;21)(q22;q22)" 
* #9896:3 ^property[0].code = #parent 
* #9896:3 ^property[0].valueCode = #984-993 
* #9897:3 "Akute myeloische Leukämie mit 11q23-Abnormitäten"
* #9897:3 ^designation[0].value = "Akute myeloische Leukämie mit t(9;11)(p22;q23); MLLT3-MLL" 
* #9897:3 ^designation[1].value = "Akute myeloische Leukämie, MLL" 
* #9897:3 ^property[0].code = #parent 
* #9897:3 ^property[0].valueCode = #984-993 
* #9898:1 "Transiente abnorme Myelopoese"
* #9898:1 ^property[0].code = #parent 
* #9898:1 ^property[0].valueCode = #984-993 
* #9898:3 "Myeloische Leukämie assoziiert mit Down-Syndrom"
* #9898:3 ^property[0].code = #parent 
* #9898:3 ^property[0].valueCode = #984-993 
* #9910:3 "Akute Megakaryoblastenleukämie"
* #9910:3 ^designation[0].value = "Akute megakaryozytäre Leukämie" 
* #9910:3 ^designation[1].value = "FAB M7" 
* #9910:3 ^property[0].code = #parent 
* #9910:3 ^property[0].valueCode = #984-993 
* #9911:3 "Akute myeloische Leukämie (megakaryoblastisch) mit t(1;22)(p13;q13); RBM15-MKL1"
* #9911:3 ^property[0].code = #parent 
* #9911:3 ^property[0].valueCode = #984-993 
* #9912:3 "Akute myeloische Leukämie mit BCR-ABL1"
* #9912:3 ^property[0].code = #parent 
* #9912:3 ^property[0].valueCode = #984-993 
* #9920:3 "Therapiebedingte myeloische Neoplasie"
* #9920:3 ^designation[0].value = "Akute myeloische Leukämie infolge Therapie o.n.A." 
* #9920:3 ^designation[1].value = "Akute myeloische Leukämie infolge Therapie, nach alkylierenden Substanzen" 
* #9920:3 ^designation[2].value = "Akute myeloische Leukämie infolge Therapie, nach Epipodophyllotoxin" 
* #9920:3 ^property[0].code = #parent 
* #9920:3 ^property[0].valueCode = #984-993 
* #9930:3 "Myelosarkom"
* #9930:3 ^designation[0].value = "Chlorom" 
* #9930:3 ^designation[1].value = "Granulozytäres Sarkom" 
* #9930:3 ^property[0].code = #parent 
* #9930:3 ^property[0].valueCode = #984-993 
* #9931:3 "Akute Panmyelose mit Myelofibrose"
* #9931:3 ^designation[0].value = "Akute Myelofibrose" 
* #9931:3 ^designation[1].value = "Akute Myelosklerose" 
* #9931:3 ^designation[2].value = "Akute Panmyelose o.n.A." 
* #9931:3 ^designation[3].value = "Maligne Myelosklerose" 
* #9931:3 ^property[0].code = #parent 
* #9931:3 ^property[0].valueCode = #984-993 
* #994-994 "Sonstige Leukämien"
* #994-994 ^property[0].code = #parent 
* #994-994 ^property[0].valueCode = #980-994 
* #994-994 ^property[1].code = #child 
* #994-994 ^property[1].valueCode = #9940:3 
* #994-994 ^property[2].code = #child 
* #994-994 ^property[2].valueCode = #9945:3 
* #994-994 ^property[3].code = #child 
* #994-994 ^property[3].valueCode = #9946:3 
* #994-994 ^property[4].code = #child 
* #994-994 ^property[4].valueCode = #9948:3 
* #9940:3 "Haarzell-Leukämie"
* #9940:3 ^designation[0].value = "Haarzell-Leukämie Variante" 
* #9940:3 ^designation[1].value = "Leukämische Retikuloendotheliose" 
* #9940:3 ^property[0].code = #parent 
* #9940:3 ^property[0].valueCode = #994-994 
* #9945:3 "Chronische myelomonozytäre Leukämie o.n.A."
* #9945:3 ^designation[0].value = "Chronische myelomonozytäre Leukämie vom Typ 1" 
* #9945:3 ^designation[1].value = "Chronische myelomonozytäre Leukämie vom Typ 2Chronische myelomonozytäre Leukämie in Transformation" 
* #9945:3 ^property[0].code = #parent 
* #9945:3 ^property[0].valueCode = #994-994 
* #9946:3 "Juvenile myelomonozytäre Leukämie"
* #9946:3 ^designation[0].value = "Juvenile chronische myelomonozytäre Leukämie" 
* #9946:3 ^property[0].code = #parent 
* #9946:3 ^property[0].valueCode = #994-994 
* #9948:3 "Aggressive NK-Zell-Leukämie"
* #9948:3 ^property[0].code = #parent 
* #9948:3 ^property[0].valueCode = #994-994 
* #995-996 "Chronische myeloproliferative Krankheiten"
* #995-996 ^property[0].code = #parent 
* #995-996 ^property[0].valueCode = #M 
* #995-996 ^property[1].code = #child 
* #995-996 ^property[1].valueCode = #9950:3 
* #995-996 ^property[2].code = #child 
* #995-996 ^property[2].valueCode = #9960:3 
* #995-996 ^property[3].code = #child 
* #995-996 ^property[3].valueCode = #9961:3 
* #995-996 ^property[4].code = #child 
* #995-996 ^property[4].valueCode = #9962:3 
* #995-996 ^property[5].code = #child 
* #995-996 ^property[5].valueCode = #9963:3 
* #995-996 ^property[6].code = #child 
* #995-996 ^property[6].valueCode = #9964:3 
* #995-996 ^property[7].code = #child 
* #995-996 ^property[7].valueCode = #9965:3 
* #995-996 ^property[8].code = #child 
* #995-996 ^property[8].valueCode = #9966:3 
* #995-996 ^property[9].code = #child 
* #995-996 ^property[9].valueCode = #9967:3 
* #995-996 ^property[10].code = #child 
* #995-996 ^property[10].valueCode = #9968:3 
* #9950:3 "Polycythaemia vera"
* #9950:3 ^designation[0].value = "Chronische Erythrämie" 
* #9950:3 ^designation[1].value = "Polycythaemia vera rubra" 
* #9950:3 ^designation[2].value = "Proliferative Polycythaemia vera" 
* #9950:3 ^property[0].code = #parent 
* #9950:3 ^property[0].valueCode = #995-996 
* #9960:3 "Myeloproliferative Neoplasie o.n.A."
* #9960:3 ^designation[0].value = "Chronische myeloproliferative Erkrankung" 
* #9960:3 ^designation[1].value = "Chronische myeloproliferative Erkrankung o.n.A." 
* #9960:3 ^designation[2].value = "Myeloproliferative Krankheit o.n.A." 
* #9960:3 ^property[0].code = #parent 
* #9960:3 ^property[0].valueCode = #995-996 
* #9961:3 "Primäre Myelofibrose"
* #9961:3 ^designation[0].value = "Chronische idiopathische Myelofibrose" 
* #9961:3 ^designation[1].value = "Megakaryozytäre Myelosklerose" 
* #9961:3 ^designation[2].value = "Myelofibrose nach myeloproliferativer Erkrankung" 
* #9961:3 ^designation[3].value = "Myelofibrose mit myeloischer Metaplasie" 
* #9961:3 ^designation[4].value = "Myelosklerose mit myeloider Metaplasie" 
* #9961:3 ^designation[5].value = "Primäre Osteomyelosklerose" 
* #9961:3 ^property[0].code = #parent 
* #9961:3 ^property[0].valueCode = #995-996 
* #9962:3 "Essentielle Thrombozythämie"
* #9962:3 ^designation[0].value = "Essentielle hämorrhagische Thrombozythämie" 
* #9962:3 ^designation[1].value = "Idiopathische hämorrhagische Thrombozythämie" 
* #9962:3 ^designation[2].value = "Idiopathische Thrombozythämie" 
* #9962:3 ^property[0].code = #parent 
* #9962:3 ^property[0].valueCode = #995-996 
* #9963:3 "Chronische Neutrophilen-Leukämie"
* #9963:3 ^property[0].code = #parent 
* #9963:3 ^property[0].valueCode = #995-996 
* #9964:3 "Chronische eosinophile Leukämie"
* #9964:3 ^designation[0].value = "Hypereosinophilie-Syndrom" 
* #9964:3 ^property[0].code = #parent 
* #9964:3 ^property[0].valueCode = #995-996 
* #9965:3 "Myeloische und lymphatische Neoplasien mit PDGFRA-Rearrangement"
* #9965:3 ^property[0].code = #parent 
* #9965:3 ^property[0].valueCode = #995-996 
* #9966:3 "Myeloische Neoplasien mit PDGFRB-Rearrangement"
* #9966:3 ^property[0].code = #parent 
* #9966:3 ^property[0].valueCode = #995-996 
* #9967:3 "Myeloische und lymphatische Neoplasien mit FGFR1-Abnormalitäten"
* #9967:3 ^property[0].code = #parent 
* #9967:3 ^property[0].valueCode = #995-996 
* #9968:3 "Myeloische und lymphoide Neoplasie mit PCM1-JAK2"
* #9968:3 ^property[0].code = #parent 
* #9968:3 ^property[0].valueCode = #995-996 
* #997-997 "Sonstige myeloproliferative Krankheiten"
* #997-997 ^property[0].code = #parent 
* #997-997 ^property[0].valueCode = #M 
* #997-997 ^property[1].code = #child 
* #997-997 ^property[1].valueCode = #9970:1 
* #997-997 ^property[2].code = #child 
* #997-997 ^property[2].valueCode = #9971:1 
* #997-997 ^property[3].code = #child 
* #997-997 ^property[3].valueCode = #9975:3 
* #9970:1 "Lymphoproliferative Erkrankung o.n.A."
* #9970:1 ^designation[0].value = "Lymphoproliferative Krankheit o.n.A." 
* #9970:1 ^property[0].code = #parent 
* #9970:1 ^property[0].valueCode = #997-997 
* #9971:1 "Lymphoproliferative Krankheit nach Transplantation o.n.A."
* #9971:1 ^designation[0].value = "PTLD o.n.A." 
* #9971:1 ^designation[1].value = "Polymorphe lymphoproliferative Krankheit nach Transplantation" 
* #9971:1 ^property[0].code = #parent 
* #9971:1 ^property[0].valueCode = #997-997 
* #9975:3 "Myeloproliferative Neoplasie, nicht klassifizierbar"
* #9975:3 ^designation[0].value = "Myelodysplastische/myeloproliferative Neoplasie, nicht klassifizierbar" 
* #9975:3 ^property[0].code = #parent 
* #9975:3 ^property[0].valueCode = #997-997 
* #998-999 "Myelodysplastische Syndrome"
* #998-999 ^property[0].code = #parent 
* #998-999 ^property[0].valueCode = #M 
* #998-999 ^property[1].code = #child 
* #998-999 ^property[1].valueCode = #9980:3 
* #998-999 ^property[2].code = #child 
* #998-999 ^property[2].valueCode = #9982:3 
* #998-999 ^property[3].code = #child 
* #998-999 ^property[3].valueCode = #9983:3 
* #998-999 ^property[4].code = #child 
* #998-999 ^property[4].valueCode = #9984:3 
* #998-999 ^property[5].code = #child 
* #998-999 ^property[5].valueCode = #9985:3 
* #998-999 ^property[6].code = #child 
* #998-999 ^property[6].valueCode = #9986:3 
* #998-999 ^property[7].code = #child 
* #998-999 ^property[7].valueCode = #9987:3 
* #998-999 ^property[8].code = #child 
* #998-999 ^property[8].valueCode = #9989:3 
* #998-999 ^property[9].code = #child 
* #998-999 ^property[9].valueCode = #9993:3 
* #9980:3 "Myelodysplastisches Syndrom mit Single-Lineage-Dysplasie"
* #9980:3 ^designation[0].value = "Refraktäre Anämie ohne Sideroblasten" 
* #9980:3 ^designation[1].value = "Refraktäre Anämie o.n.A." 
* #9980:3 ^designation[2].value = "Refraktäre Neutropenie" 
* #9980:3 ^designation[3].value = "Refraktäre Thrombozytopenie" 
* #9980:3 ^property[0].code = #parent 
* #9980:3 ^property[0].valueCode = #998-999 
* #9982:3 "Myelodysplastisches Syndrom mit Ringsideroblasten und Single-Lineage-Dysplasie"
* #9982:3 ^designation[0].value = "Myelodysplastisches Syndrom mit Ringsideroblasten, o.n.A." 
* #9982:3 ^designation[1].value = "RARS" 
* #9982:3 ^designation[2].value = "Refraktäre Anämie mit Ringsideroblasten" 
* #9982:3 ^designation[3].value = "Refraktäre Anämie mit Ringsideroblasten assoziiert mit ausgeprägter Thrombozytose" 
* #9982:3 ^designation[4].value = "Refraktäre Anämie mit Sideroblasten" 
* #9982:3 ^property[0].code = #parent 
* #9982:3 ^property[0].valueCode = #998-999 
* #9983:3 "Myelodysplastisches Syndrom mit Blastenüberschuss"
* #9983:3 ^designation[0].value = "Refraktäre Anämie mit Blastenüberschuss" 
* #9983:3 ^designation[1].value = "RAEB" 
* #9983:3 ^designation[2].value = "RAEB I" 
* #9983:3 ^designation[3].value = "RAEB II" 
* #9983:3 ^property[0].code = #parent 
* #9983:3 ^property[0].valueCode = #998-999 
* #9984:3 "Refraktäre Anämie mit Blastenüberschuss in Transformation"
* #9984:3 ^designation[0].value = "RAEB-T" 
* #9984:3 ^property[0].code = #parent 
* #9984:3 ^property[0].valueCode = #998-999 
* #9985:3 "Myelodysplastisches Syndrom mit multilineärer Dysplasie"
* #9985:3 ^designation[0].value = "Refraktäre Zytopenie mit Mehrlinien-Dysplasie" 
* #9985:3 ^designation[1].value = "Refraktäre Zytopenie im Kindesalter" 
* #9985:3 ^property[0].code = #parent 
* #9985:3 ^property[0].valueCode = #998-999 
* #9986:3 "Myelodysplastisches Syndrom mit isolierter del (5q)"
* #9986:3 ^designation[0].value = "Myelodysplastisches Syndrom mit 5q-Deletion (5q-)" 
* #9986:3 ^property[0].code = #parent 
* #9986:3 ^property[0].valueCode = #998-999 
* #9987:3 "Therapiebedingtes myelodysplastisches Syndrom o.n.A."
* #9987:3 ^designation[0].value = "Therapiebedingtes myelodysplastisches Syndrom durch Alkylantien" 
* #9987:3 ^designation[1].value = "Therapiebedingtes myelodysplastisches Syndrom durch Epipodophyllotoxin" 
* #9987:3 ^property[0].code = #parent 
* #9987:3 ^property[0].valueCode = #998-999 
* #9989:3 "Myelodysplastisches Syndrom o.n.A."
* #9989:3 ^designation[0].value = "Präleukämie" 
* #9989:3 ^designation[1].value = "Präleukämisches Syndrom" 
* #9989:3 ^designation[2].value = "Myelodysplastisches Syndrom, nicht klassifizierbar" 
* #9989:3 ^property[0].code = #parent 
* #9989:3 ^property[0].valueCode = #998-999 
* #9993:3 "Myelodysplastisches Syndrom mit Ringsideroblasten und multilineärer Dysplasie"
* #9993:3 ^property[0].code = #parent 
* #9993:3 ^property[0].valueCode = #998-999 
* #T "Topographie"
* #T ^definition = In den Kategorien C00 bis C80.9 sollen die Neoplasien den jeweiligen Unterpunkten entsprechend dem Ursprung des Tumors zugeordnet werden. Ein Tumor, der die Grenzen zweier oder mehrerer Unterkategorien überschreitet und dessen Ursprung nicht genauer ermittelt werden kann, wird der Unterkategorie ".8" zugeordnet. Beispielsweise wird eine Neoplasie des zervikothorakalen Überganges des Ösophagus mit C15.8 verschlüsselt.
* #T ^property[0].code = #child 
* #T ^property[0].valueCode = #C00-C14 
* #T ^property[1].code = #child 
* #T ^property[1].valueCode = #C15-C26 
* #T ^property[2].code = #child 
* #T ^property[2].valueCode = #C30-C39 
* #T ^property[3].code = #child 
* #T ^property[3].valueCode = #C40-C41 
* #T ^property[4].code = #child 
* #T ^property[4].valueCode = #C42-C42 
* #T ^property[5].code = #child 
* #T ^property[5].valueCode = #C44-C44 
* #T ^property[6].code = #child 
* #T ^property[6].valueCode = #C47-C47 
* #T ^property[7].code = #child 
* #T ^property[7].valueCode = #C48-C48 
* #T ^property[8].code = #child 
* #T ^property[8].valueCode = #C49-C49 
* #T ^property[9].code = #child 
* #T ^property[9].valueCode = #C50-C50 
* #T ^property[10].code = #child 
* #T ^property[10].valueCode = #C51-C58 
* #T ^property[11].code = #child 
* #T ^property[11].valueCode = #C60-C63 
* #T ^property[12].code = #child 
* #T ^property[12].valueCode = #C64-C68 
* #T ^property[13].code = #child 
* #T ^property[13].valueCode = #C69-C72 
* #T ^property[14].code = #child 
* #T ^property[14].valueCode = #C73-C75 
* #T ^property[15].code = #child 
* #T ^property[15].valueCode = #C76-C80 
* #C00-C14 "Lippe, Mundhöhle und Pharynx"
* #C00-C14 ^property[0].code = #parent 
* #C00-C14 ^property[0].valueCode = #T 
* #C00-C14 ^property[1].code = #child 
* #C00-C14 ^property[1].valueCode = #C00 
* #C00-C14 ^property[2].code = #child 
* #C00-C14 ^property[2].valueCode = #C01 
* #C00-C14 ^property[3].code = #child 
* #C00-C14 ^property[3].valueCode = #C02 
* #C00-C14 ^property[4].code = #child 
* #C00-C14 ^property[4].valueCode = #C03 
* #C00-C14 ^property[5].code = #child 
* #C00-C14 ^property[5].valueCode = #C04 
* #C00-C14 ^property[6].code = #child 
* #C00-C14 ^property[6].valueCode = #C05 
* #C00-C14 ^property[7].code = #child 
* #C00-C14 ^property[7].valueCode = #C06 
* #C00-C14 ^property[8].code = #child 
* #C00-C14 ^property[8].valueCode = #C07 
* #C00-C14 ^property[9].code = #child 
* #C00-C14 ^property[9].valueCode = #C08 
* #C00-C14 ^property[10].code = #child 
* #C00-C14 ^property[10].valueCode = #C09 
* #C00-C14 ^property[11].code = #child 
* #C00-C14 ^property[11].valueCode = #C10 
* #C00-C14 ^property[12].code = #child 
* #C00-C14 ^property[12].valueCode = #C11 
* #C00-C14 ^property[13].code = #child 
* #C00-C14 ^property[13].valueCode = #C12 
* #C00-C14 ^property[14].code = #child 
* #C00-C14 ^property[14].valueCode = #C13 
* #C00-C14 ^property[15].code = #child 
* #C00-C14 ^property[15].valueCode = #C14 
* #C00 "Lippe"
* #C00 ^designation[0].value = "Äußere Haut der Lippe" 
* #C00 ^property[0].code = #parent 
* #C00 ^property[0].valueCode = #C00-C14 
* #C00 ^property[1].code = #child 
* #C00 ^property[1].valueCode = #C00.0 
* #C00 ^property[2].code = #child 
* #C00 ^property[2].valueCode = #C00.1 
* #C00 ^property[3].code = #child 
* #C00 ^property[3].valueCode = #C00.2 
* #C00 ^property[4].code = #child 
* #C00 ^property[4].valueCode = #C00.3 
* #C00 ^property[5].code = #child 
* #C00 ^property[5].valueCode = #C00.4 
* #C00 ^property[6].code = #child 
* #C00 ^property[6].valueCode = #C00.5 
* #C00 ^property[7].code = #child 
* #C00 ^property[7].valueCode = #C00.6 
* #C00 ^property[8].code = #child 
* #C00 ^property[8].valueCode = #C00.8 
* #C00 ^property[9].code = #child 
* #C00 ^property[9].valueCode = #C00.9 
* #C00.0 "Äußere Oberlippe"
* #C00.0 ^designation[0].value = "Oberlippe, Lippenrot" 
* #C00.0 ^designation[1].value = "Oberlippe o.n.A." 
* #C00.0 ^property[0].code = #parent 
* #C00.0 ^property[0].valueCode = #C00 
* #C00.1 "Äußere Unterlippe"
* #C00.1 ^designation[0].value = "Unterlippe, Lippenrot" 
* #C00.1 ^designation[1].value = "Unterlippe o.n.A." 
* #C00.1 ^property[0].code = #parent 
* #C00.1 ^property[0].valueCode = #C00 
* #C00.2 "Äußere Lippe o.n.A."
* #C00.2 ^property[0].code = #parent 
* #C00.2 ^property[0].valueCode = #C00 
* #C00.3 "Schleimhaut der Oberlippe"
* #C00.3 ^designation[0].value = "Oberlippe, innerer Bereich" 
* #C00.3 ^designation[1].value = "Lippenbändchen der OberlippeFrenulum der Oberlippe" 
* #C00.3 ^property[0].code = #parent 
* #C00.3 ^property[0].valueCode = #C00 
* #C00.4 "Schleimhaut der Unterlippe"
* #C00.4 ^designation[0].value = "Unterlippe, innerer Bereich" 
* #C00.4 ^designation[1].value = "Lippenbändchen der UnterlippeFrenulum der Oberlippe" 
* #C00.4 ^property[0].code = #parent 
* #C00.4 ^property[0].valueCode = #C00 
* #C00.5 "Lippenschleimhaut o.n.A."
* #C00.5 ^designation[0].value = "Lippe, innerer Bereich o.n.A." 
* #C00.5 ^designation[1].value = "Lippeninnenseite o.n.A." 
* #C00.5 ^designation[2].value = "Lippenbändchen o.n.A.Frenulum labii o.n.A." 
* #C00.5 ^property[0].code = #parent 
* #C00.5 ^property[0].valueCode = #C00 
* #C00.6 "Lippenkommissur"
* #C00.6 ^designation[0].value = "Mundwinkel" 
* #C00.6 ^property[0].code = #parent 
* #C00.6 ^property[0].valueCode = #C00 
* #C00.8 "Lippe, mehrere Teilbereiche überlappend"
* #C00.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C00.8 ^property[0].code = #parent 
* #C00.8 ^property[0].valueCode = #C00 
* #C00.9 "Lippe o.n.A."
* #C00.9 ^designation[0].value = "Äußere Haut der Lippe" 
* #C00.9 ^property[0].code = #parent 
* #C00.9 ^property[0].valueCode = #C00 
* #C01 "Zungengrund"
* #C01 ^property[0].code = #parent 
* #C01 ^property[0].valueCode = #C00-C14 
* #C01 ^property[1].code = #child 
* #C01 ^property[1].valueCode = #C01.9 
* #C01.9 "Zungengrund o.n.A."
* #C01.9 ^designation[0].value = "Hinterer Zungenanteil o.n.A." 
* #C01.9 ^designation[1].value = "Hinteres Zungendrittel" 
* #C01.9 ^designation[2].value = "Rücken des Zungengrundes" 
* #C01.9 ^designation[3].value = "Zungenwurzel" 
* #C01.9 ^property[0].code = #parent 
* #C01.9 ^property[0].valueCode = #C01 
* #C02 "Sonstige und nicht näher bezeichnete Teile der Zunge"
* #C02 ^property[0].code = #parent 
* #C02 ^property[0].valueCode = #C00-C14 
* #C02 ^property[1].code = #child 
* #C02 ^property[1].valueCode = #C02.0 
* #C02 ^property[2].code = #child 
* #C02 ^property[2].valueCode = #C02.1 
* #C02 ^property[3].code = #child 
* #C02 ^property[3].valueCode = #C02.2 
* #C02 ^property[4].code = #child 
* #C02 ^property[4].valueCode = #C02.3 
* #C02 ^property[5].code = #child 
* #C02 ^property[5].valueCode = #C02.4 
* #C02 ^property[6].code = #child 
* #C02 ^property[6].valueCode = #C02.8 
* #C02 ^property[7].code = #child 
* #C02 ^property[7].valueCode = #C02.9 
* #C02.0 "Dorsale Oberfläche der Zunge"
* #C02.0 ^designation[0].value = "Dorsale Oberfläche der vorderen 2/3 der Zunge" 
* #C02.0 ^designation[1].value = "Dorsale Oberfläche des vorderen Anteiles der Zunge" 
* #C02.0 ^designation[2].value = "Mittellinie der Zunge" 
* #C02.0 ^property[0].code = #parent 
* #C02.0 ^property[0].valueCode = #C02 
* #C02.1 "Zungenrand"
* #C02.1 ^designation[0].value = "Zungenspitze" 
* #C02.1 ^property[0].code = #parent 
* #C02.1 ^property[0].valueCode = #C02 
* #C02.2 "Ventrale Oberfläche der Zunge o.n.A."
* #C02.2 ^designation[0].value = "Frenulum linguae" 
* #C02.2 ^designation[1].value = "Ventrale Oberfläche des vorderen Teiles der Zunge o.n.A." 
* #C02.2 ^designation[2].value = "Vordere 2/3 der Zunge, ventrale Oberfläche" 
* #C02.2 ^property[0].code = #parent 
* #C02.2 ^property[0].valueCode = #C02 
* #C02.3 "Vordere 2/3 der Zunge"
* #C02.3 ^designation[0].value = "Vorderer Teil der Zunge o.n.A." 
* #C02.3 ^property[0].code = #parent 
* #C02.3 ^property[0].valueCode = #C02 
* #C02.4 "Zungentonsille"
* #C02.4 ^designation[0].value = "Tonsilla lingualis" 
* #C02.4 ^property[0].code = #parent 
* #C02.4 ^property[0].valueCode = #C02 
* #C02.8 "Zunge, mehrere Bereiche überlappend"
* #C02.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C02.8 ^designation[0].value = "Verbindungszone der Zunge" 
* #C02.8 ^property[0].code = #parent 
* #C02.8 ^property[0].valueCode = #C02 
* #C02.9 "Zunge o.n.A."
* #C02.9 ^designation[0].value = "Lingua o.n.A." 
* #C02.9 ^property[0].code = #parent 
* #C02.9 ^property[0].valueCode = #C02 
* #C03 "Zahnfleisch"
* #C03 ^property[0].code = #parent 
* #C03 ^property[0].valueCode = #C00-C14 
* #C03 ^property[1].code = #child 
* #C03 ^property[1].valueCode = #C03.0 
* #C03 ^property[2].code = #child 
* #C03 ^property[2].valueCode = #C03.1 
* #C03 ^property[3].code = #child 
* #C03 ^property[3].valueCode = #C03.9 
* #C03.0 "Oberkieferzahnfleisch"
* #C03.0 ^designation[0].value = "Alveole im Oberkiefer" 
* #C03.0 ^designation[1].value = "Gingiva des Oberkiefers" 
* #C03.0 ^designation[2].value = "Schleimhaut des Alveolarfortsatzes des Oberkiefers" 
* #C03.0 ^designation[3].value = "Schleimhaut des Zahndammes des Oberkiefers" 
* #C03.0 ^property[0].code = #parent 
* #C03.0 ^property[0].valueCode = #C03 
* #C03.1 "Unterkieferzahnfleisch"
* #C03.1 ^designation[0].value = "Alveole im Unterkiefer" 
* #C03.1 ^designation[1].value = "Gingiva des Unterkiefers" 
* #C03.1 ^designation[2].value = "Schleimhaut des Alveolarfortsatzes des Unterkiefers" 
* #C03.1 ^designation[3].value = "Schleimhaut des Zahndammes des Unterkiefers" 
* #C03.1 ^property[0].code = #parent 
* #C03.1 ^property[0].valueCode = #C03 
* #C03.9 "Zahnfleisch o.n.A."
* #C03.9 ^designation[0].value = "Alveolus o.n.A." 
* #C03.9 ^designation[1].value = "Schleimhaut des Alveolarfortsatzes o.n.A." 
* #C03.9 ^designation[2].value = "Zahndamm o.n.A." 
* #C03.9 ^designation[3].value = "Parodontales Gewebe" 
* #C03.9 ^designation[4].value = "Zahnfach" 
* #C03.9 ^property[0].code = #parent 
* #C03.9 ^property[0].valueCode = #C03 
* #C04 "Mundboden"
* #C04 ^property[0].code = #parent 
* #C04 ^property[0].valueCode = #C00-C14 
* #C04 ^property[1].code = #child 
* #C04 ^property[1].valueCode = #C04.0 
* #C04 ^property[2].code = #child 
* #C04 ^property[2].valueCode = #C04.1 
* #C04 ^property[3].code = #child 
* #C04 ^property[3].valueCode = #C04.8 
* #C04 ^property[4].code = #child 
* #C04 ^property[4].valueCode = #C04.9 
* #C04.0 "Vorderer Teil des Mundbodens"
* #C04.0 ^property[0].code = #parent 
* #C04.0 ^property[0].valueCode = #C04 
* #C04.1 "Seitlicher Teil des Mundbodens"
* #C04.1 ^property[0].code = #parent 
* #C04.1 ^property[0].valueCode = #C04 
* #C04.8 "Mundboden, mehrere Teilbereiche überlappend"
* #C04.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C04.8 ^property[0].code = #parent 
* #C04.8 ^property[0].valueCode = #C04 
* #C04.9 "Mundboden o.n.A."
* #C04.9 ^property[0].code = #parent 
* #C04.9 ^property[0].valueCode = #C04 
* #C05 "Gaumen"
* #C05 ^property[0].code = #parent 
* #C05 ^property[0].valueCode = #C00-C14 
* #C05 ^property[1].code = #child 
* #C05 ^property[1].valueCode = #C05.0 
* #C05 ^property[2].code = #child 
* #C05 ^property[2].valueCode = #C05.1 
* #C05 ^property[3].code = #child 
* #C05 ^property[3].valueCode = #C05.2 
* #C05 ^property[4].code = #child 
* #C05 ^property[4].valueCode = #C05.8 
* #C05 ^property[5].code = #child 
* #C05 ^property[5].valueCode = #C05.9 
* #C05.0 "Harter Gaumen"
* #C05.0 ^property[0].code = #parent 
* #C05.0 ^property[0].valueCode = #C05 
* #C05.1 "Weicher Gaumen o.n.A."
* #C05.1 ^designation[0].value = "Nasopharyngeale Oberfläche des weichen Gaumens" 
* #C05.1 ^property[0].code = #parent 
* #C05.1 ^property[0].valueCode = #C05 
* #C05.2 "Uvula"
* #C05.2 ^property[0].code = #parent 
* #C05.2 ^property[0].valueCode = #C05 
* #C05.8 "Gaumen, mehrere Teilbereiche überlappend"
* #C05.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C05.8 ^designation[0].value = "Übergangsbereich zwischen hartem und weichem Gaumen" 
* #C05.8 ^property[0].code = #parent 
* #C05.8 ^property[0].valueCode = #C05 
* #C05.9 "Gaumen o.n.A."
* #C05.9 ^property[0].code = #parent 
* #C05.9 ^property[0].valueCode = #C05 
* #C06 "Sonstige und nicht näher bezeichnete Teile des Mundes"
* #C06 ^property[0].code = #parent 
* #C06 ^property[0].valueCode = #C00-C14 
* #C06 ^property[1].code = #child 
* #C06 ^property[1].valueCode = #C06.0 
* #C06 ^property[2].code = #child 
* #C06 ^property[2].valueCode = #C06.1 
* #C06 ^property[3].code = #child 
* #C06 ^property[3].valueCode = #C06.2 
* #C06 ^property[4].code = #child 
* #C06 ^property[4].valueCode = #C06.8 
* #C06 ^property[5].code = #child 
* #C06 ^property[5].valueCode = #C06.9 
* #C06.0 "Wangenschleimhaut"
* #C06.0 ^designation[0].value = "Mundschleimhaut" 
* #C06.0 ^designation[1].value = "Wangeninnenseite" 
* #C06.0 ^property[0].code = #parent 
* #C06.0 ^property[0].valueCode = #C06 
* #C06.1 "Vestibulum oris"
* #C06.1 ^designation[0].value = "Sulcus buccomandibularis" 
* #C06.1 ^designation[1].value = "Sulcus buccomaxillaris" 
* #C06.1 ^property[0].code = #parent 
* #C06.1 ^property[0].valueCode = #C06 
* #C06.2 "Retromolarregion"
* #C06.2 ^designation[0].value = "Retromolares Dreieck" 
* #C06.2 ^designation[1].value = "Trigonum retromolare" 
* #C06.2 ^property[0].code = #parent 
* #C06.2 ^property[0].valueCode = #C06 
* #C06.8 "Sonstige und nicht näher bezeichnete Teile des Mundes, mehrere Teilbereiche überlappend"
* #C06.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C06.8 ^property[0].code = #parent 
* #C06.8 ^property[0].valueCode = #C06 
* #C06.9 "Mund o.n.A."
* #C06.9 ^designation[0].value = "Glandulae salivariae minores o.n.A." 
* #C06.9 ^designation[1].value = "Mukosa der Mundhöhle" 
* #C06.9 ^designation[2].value = "Mundhöhle" 
* #C06.9 ^property[0].code = #parent 
* #C06.9 ^property[0].valueCode = #C06 
* #C07 "Parotis"
* #C07 ^property[0].code = #parent 
* #C07 ^property[0].valueCode = #C00-C14 
* #C07 ^property[1].code = #child 
* #C07 ^property[1].valueCode = #C07.9 
* #C07.9 "Parotis"
* #C07.9 ^designation[0].value = "Parotis o.n.A." 
* #C07.9 ^designation[1].value = "Stensen-GangAusführungsgang der Parotis" 
* #C07.9 ^property[0].code = #parent 
* #C07.9 ^property[0].valueCode = #C07 
* #C08 "Sonstige und nicht näher bezeichnete große Speicheldrüsen"
* #C08 ^property[0].code = #parent 
* #C08 ^property[0].valueCode = #C00-C14 
* #C08 ^property[1].code = #child 
* #C08 ^property[1].valueCode = #C08.0 
* #C08 ^property[2].code = #child 
* #C08 ^property[2].valueCode = #C08.1 
* #C08 ^property[3].code = #child 
* #C08 ^property[3].valueCode = #C08.8 
* #C08 ^property[4].code = #child 
* #C08 ^property[4].valueCode = #C08.9 
* #C08.0 "Glandula submandibularis"
* #C08.0 ^designation[0].value = "Glandula submaxillaris" 
* #C08.0 ^designation[1].value = "Wharton-GangAusführungsgang der Glandula submaxillaris" 
* #C08.0 ^property[0].code = #parent 
* #C08.0 ^property[0].valueCode = #C08 
* #C08.1 "Glandula sublingualis"
* #C08.1 ^designation[0].value = "Ausführungsgang der Glandula sublingualis" 
* #C08.1 ^property[0].code = #parent 
* #C08.1 ^property[0].valueCode = #C08 
* #C08.8 "Große Speicheldrüsen, mehrere Bereiche überlappend"
* #C08.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C08.8 ^property[0].code = #parent 
* #C08.8 ^property[0].valueCode = #C08 
* #C08.9 "Große Speicheldrüsen o.n.A."
* #C08.9 ^designation[0].value = "Speicheldrüse o.n.A." 
* #C08.9 ^property[0].code = #parent 
* #C08.9 ^property[0].valueCode = #C08 
* #C09 "Tonsille"
* #C09 ^property[0].code = #parent 
* #C09 ^property[0].valueCode = #C00-C14 
* #C09 ^property[1].code = #child 
* #C09 ^property[1].valueCode = #C09.0 
* #C09 ^property[2].code = #child 
* #C09 ^property[2].valueCode = #C09.1 
* #C09 ^property[3].code = #child 
* #C09 ^property[3].valueCode = #C09.8 
* #C09 ^property[4].code = #child 
* #C09 ^property[4].valueCode = #C09.9 
* #C09.0 "Fossa tonsillaris"
* #C09.0 ^property[0].code = #parent 
* #C09.0 ^property[0].valueCode = #C09 
* #C09.1 "Gaumenbogen"
* #C09.1 ^designation[0].value = "Arcus palatoglossus" 
* #C09.1 ^designation[1].value = "Arcus palatopharyngeus" 
* #C09.1 ^designation[2].value = "Plica glossopalatina" 
* #C09.1 ^property[0].code = #parent 
* #C09.1 ^property[0].valueCode = #C09 
* #C09.8 "Tonsille, mehrere Teilbereiche überlappend"
* #C09.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C09.8 ^property[0].code = #parent 
* #C09.8 ^property[0].valueCode = #C09 
* #C09.9 "Tonsille o.n.A."
* #C09.9 ^designation[0].value = "Gaumenmandel" 
* #C09.9 ^designation[1].value = "Tonsilla palatina" 
* #C09.9 ^designation[2].value = "Tonsilla pharyngea" 
* #C09.9 ^designation[3].value = "Zungentonsille" 
* #C09.9 ^property[0].code = #parent 
* #C09.9 ^property[0].valueCode = #C09 
* #C10 "Oropharynx"
* #C10 ^property[0].code = #parent 
* #C10 ^property[0].valueCode = #C00-C14 
* #C10 ^property[1].code = #child 
* #C10 ^property[1].valueCode = #C10.0 
* #C10 ^property[2].code = #child 
* #C10 ^property[2].valueCode = #C10.1 
* #C10 ^property[3].code = #child 
* #C10 ^property[3].valueCode = #C10.2 
* #C10 ^property[4].code = #child 
* #C10 ^property[4].valueCode = #C10.3 
* #C10 ^property[5].code = #child 
* #C10 ^property[5].valueCode = #C10.4 
* #C10 ^property[6].code = #child 
* #C10 ^property[6].valueCode = #C10.8 
* #C10 ^property[7].code = #child 
* #C10 ^property[7].valueCode = #C10.9 
* #C10.0 "Vallecula epiglottica"
* #C10.0 ^property[0].code = #parent 
* #C10.0 ^property[0].valueCode = #C10 
* #C10.1 "Vorderfläche der Epiglottis"
* #C10.1 ^designation[0].value = "Vorderfläche des Kehldeckels" 
* #C10.1 ^property[0].code = #parent 
* #C10.1 ^property[0].valueCode = #C10 
* #C10.2 "Seitenwand des Oropharynx"
* #C10.2 ^designation[0].value = "Seitenwand des Mesopharynx" 
* #C10.2 ^designation[1].value = "Seitenwand des Rachenringes" 
* #C10.2 ^property[0].code = #parent 
* #C10.2 ^property[0].valueCode = #C10 
* #C10.3 "Hinterwand des Oropharynx"
* #C10.3 ^designation[0].value = "Hinterwand des Mesopharynx" 
* #C10.3 ^designation[1].value = "Hinterwand des Rachenringes" 
* #C10.3 ^property[0].code = #parent 
* #C10.3 ^property[0].valueCode = #C10 
* #C10.4 "Kiemengang"
* #C10.4 ^property[0].code = #parent 
* #C10.4 ^property[0].valueCode = #C10 
* #C10.8 "Oropharynx, mehrere Teilbereiche überlappend"
* #C10.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C10.8 ^designation[0].value = "Übergangszone des Rachenringes" 
* #C10.8 ^property[0].code = #parent 
* #C10.8 ^property[0].valueCode = #C10 
* #C10.9 "Oropharynx o.n.A."
* #C10.9 ^designation[0].value = "Fauces o.n.A." 
* #C10.9 ^designation[1].value = "Mesopharynx o.n.A." 
* #C10.9 ^designation[2].value = "Schlund o.n.A." 
* #C10.9 ^property[0].code = #parent 
* #C10.9 ^property[0].valueCode = #C10 
* #C11 "Nasopharynx [Nasenrachenraum]"
* #C11 ^property[0].code = #parent 
* #C11 ^property[0].valueCode = #C00-C14 
* #C11 ^property[1].code = #child 
* #C11 ^property[1].valueCode = #C11.0 
* #C11 ^property[2].code = #child 
* #C11 ^property[2].valueCode = #C11.1 
* #C11 ^property[3].code = #child 
* #C11 ^property[3].valueCode = #C11.2 
* #C11 ^property[4].code = #child 
* #C11 ^property[4].valueCode = #C11.3 
* #C11 ^property[5].code = #child 
* #C11 ^property[5].valueCode = #C11.8 
* #C11 ^property[6].code = #child 
* #C11 ^property[6].valueCode = #C11.9 
* #C11.0 "Obere Wand des Nasopharynx"
* #C11.0 ^designation[0].value = "Obere Wand des Nasenrachenraumes" 
* #C11.0 ^property[0].code = #parent 
* #C11.0 ^property[0].valueCode = #C11 
* #C11.1 "Hinterwand des Nasopharynx"
* #C11.1 ^designation[0].value = "Hinterwand des Nasenrachenraumes" 
* #C11.1 ^designation[1].value = "AdenoideTonsilla pharyngealis" 
* #C11.1 ^property[0].code = #parent 
* #C11.1 ^property[0].valueCode = #C11 
* #C11.2 "Seitenwand des Nasopharynx"
* #C11.2 ^designation[0].value = "Seitenwand des Nasenrachenraumes" 
* #C11.2 ^designation[1].value = "Rosenmüller-Grube" 
* #C11.2 ^property[0].code = #parent 
* #C11.2 ^property[0].valueCode = #C11 
* #C11.3 "Vorderwand des Nasopharynx"
* #C11.3 ^designation[0].value = "Vorderwand des Nasenrachenraumes" 
* #C11.3 ^designation[1].value = "Choanen" 
* #C11.3 ^designation[2].value = "Dach des Schlundes" 
* #C11.3 ^designation[3].value = "Fornix pharyngis" 
* #C11.3 ^designation[4].value = "Hinterrand des Nasenseptums" 
* #C11.3 ^designation[5].value = "Nasopharyngeale Oberfläche des weichen Gaumens" 
* #C11.3 ^property[0].code = #parent 
* #C11.3 ^property[0].valueCode = #C11 
* #C11.8 "Nasopharynx, mehrere Teilbereiche überlappend"
* #C11.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C11.8 ^property[0].code = #parent 
* #C11.8 ^property[0].valueCode = #C11 
* #C11.9 "Nasopharynx o.n.A."
* #C11.9 ^designation[0].value = "Nasenrachenraum o.n.A." 
* #C11.9 ^designation[1].value = "Wand des Nasopharynx" 
* #C11.9 ^property[0].code = #parent 
* #C11.9 ^property[0].valueCode = #C11 
* #C12 "Sinus piriformis"
* #C12 ^property[0].code = #parent 
* #C12 ^property[0].valueCode = #C00-C14 
* #C12 ^property[1].code = #child 
* #C12 ^property[1].valueCode = #C12.9 
* #C12.9 "Sinus piriformis"
* #C12.9 ^designation[0].value = "Fossa piriformis" 
* #C12.9 ^designation[1].value = "Recessus piriformis" 
* #C12.9 ^property[0].code = #parent 
* #C12.9 ^property[0].valueCode = #C12 
* #C13 "Hypopharynx"
* #C13 ^property[0].code = #parent 
* #C13 ^property[0].valueCode = #C00-C14 
* #C13 ^property[1].code = #child 
* #C13 ^property[1].valueCode = #C13.0 
* #C13 ^property[2].code = #child 
* #C13 ^property[2].valueCode = #C13.1 
* #C13 ^property[3].code = #child 
* #C13 ^property[3].valueCode = #C13.2 
* #C13 ^property[4].code = #child 
* #C13 ^property[4].valueCode = #C13.8 
* #C13 ^property[5].code = #child 
* #C13 ^property[5].valueCode = #C13.9 
* #C13.0 "Regio postcricoidea"
* #C13.0 ^designation[0].value = "Krikoid o.n.A." 
* #C13.0 ^property[0].code = #parent 
* #C13.0 ^property[0].valueCode = #C13 
* #C13.1 "Plica aryepiglottica, hypopharyngeale Seite"
* #C13.1 ^designation[0].value = "Aryepiglottische Falte" 
* #C13.1 ^designation[1].value = "Aryepiglottische Falte o.n.A." 
* #C13.1 ^property[0].code = #parent 
* #C13.1 ^property[0].valueCode = #C13 
* #C13.2 "Hinterwand des Hypopharynx"
* #C13.2 ^property[0].code = #parent 
* #C13.2 ^property[0].valueCode = #C13 
* #C13.8 "Hypopharynx, mehrere Teilbereiche überlappend"
* #C13.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C13.8 ^property[0].code = #parent 
* #C13.8 ^property[0].valueCode = #C13 
* #C13.9 "Hypopharynx o.n.A."
* #C13.9 ^designation[0].value = "Laryngopharynx" 
* #C13.9 ^designation[1].value = "Wand des Hypopharynx" 
* #C13.9 ^property[0].code = #parent 
* #C13.9 ^property[0].valueCode = #C13 
* #C14 "Sonstiger oder mangelhaft bezeichneter Sitz an Lippe, Mundhöhle und Pharynx"
* #C14 ^property[0].code = #parent 
* #C14 ^property[0].valueCode = #C00-C14 
* #C14 ^property[1].code = #child 
* #C14 ^property[1].valueCode = #C14.0 
* #C14 ^property[2].code = #child 
* #C14 ^property[2].valueCode = #C14.2 
* #C14 ^property[3].code = #child 
* #C14 ^property[3].valueCode = #C14.8 
* #C14.0 "Pharynx o.n.A."
* #C14.0 ^designation[0].value = "Hinterwand des Pharynx" 
* #C14.0 ^designation[1].value = "Rachen" 
* #C14.0 ^designation[2].value = "Rachenwand o.n.A.Wand des Pharynx o.n.A." 
* #C14.0 ^designation[3].value = "Retropharynx" 
* #C14.0 ^designation[4].value = "Seitenwand des Pharynx" 
* #C14.0 ^property[0].code = #parent 
* #C14.0 ^property[0].valueCode = #C14 
* #C14.2 "Waldeyer-Ring"
* #C14.2 ^property[0].code = #parent 
* #C14.2 ^property[0].valueCode = #C14 
* #C14.8 "Lippe, Mundhöhle und Pharynx, mehrere Bereiche überlappend"
* #C14.8 ^property[0].code = #parent 
* #C14.8 ^property[0].valueCode = #C14 
* #C15-C26 "Verdauungsorgane"
* #C15-C26 ^property[0].code = #parent 
* #C15-C26 ^property[0].valueCode = #T 
* #C15-C26 ^property[1].code = #child 
* #C15-C26 ^property[1].valueCode = #C15 
* #C15-C26 ^property[2].code = #child 
* #C15-C26 ^property[2].valueCode = #C16 
* #C15-C26 ^property[3].code = #child 
* #C15-C26 ^property[3].valueCode = #C17 
* #C15-C26 ^property[4].code = #child 
* #C15-C26 ^property[4].valueCode = #C18 
* #C15-C26 ^property[5].code = #child 
* #C15-C26 ^property[5].valueCode = #C19 
* #C15-C26 ^property[6].code = #child 
* #C15-C26 ^property[6].valueCode = #C20 
* #C15-C26 ^property[7].code = #child 
* #C15-C26 ^property[7].valueCode = #C21 
* #C15-C26 ^property[8].code = #child 
* #C15-C26 ^property[8].valueCode = #C22 
* #C15-C26 ^property[9].code = #child 
* #C15-C26 ^property[9].valueCode = #C23 
* #C15-C26 ^property[10].code = #child 
* #C15-C26 ^property[10].valueCode = #C24 
* #C15-C26 ^property[11].code = #child 
* #C15-C26 ^property[11].valueCode = #C25 
* #C15-C26 ^property[12].code = #child 
* #C15-C26 ^property[12].valueCode = #C26 
* #C15 "Ösophagus"
* #C15 ^property[0].code = #parent 
* #C15 ^property[0].valueCode = #C15-C26 
* #C15 ^property[1].code = #child 
* #C15 ^property[1].valueCode = #C15.0 
* #C15 ^property[2].code = #child 
* #C15 ^property[2].valueCode = #C15.1 
* #C15 ^property[3].code = #child 
* #C15 ^property[3].valueCode = #C15.2 
* #C15 ^property[4].code = #child 
* #C15 ^property[4].valueCode = #C15.3 
* #C15 ^property[5].code = #child 
* #C15 ^property[5].valueCode = #C15.4 
* #C15 ^property[6].code = #child 
* #C15 ^property[6].valueCode = #C15.5 
* #C15 ^property[7].code = #child 
* #C15 ^property[7].valueCode = #C15.8 
* #C15 ^property[8].code = #child 
* #C15 ^property[8].valueCode = #C15.9 
* #C15.0 "Ösophagus, Pars cervicalis"
* #C15.0 ^property[0].code = #parent 
* #C15.0 ^property[0].valueCode = #C15 
* #C15.1 "Ösophagus, Pars thoracalis"
* #C15.1 ^property[0].code = #parent 
* #C15.1 ^property[0].valueCode = #C15 
* #C15.2 "Ösophagus, Pars abdominalis"
* #C15.2 ^property[0].code = #parent 
* #C15.2 ^property[0].valueCode = #C15 
* #C15.3 "Ösophagus, oberes Drittel"
* #C15.3 ^designation[0].value = "Ösophagus, oberes intrathorakales Drittel" 
* #C15.3 ^property[0].code = #parent 
* #C15.3 ^property[0].valueCode = #C15 
* #C15.4 "Ösophagus, mittleres Drittel"
* #C15.4 ^property[0].code = #parent 
* #C15.4 ^property[0].valueCode = #C15 
* #C15.5 "Ösophagus, unteres intrathorakales Drittel"
* #C15.5 ^designation[0].value = "Ösophagus, distales Drittel" 
* #C15.5 ^property[0].code = #parent 
* #C15.5 ^property[0].valueCode = #C15 
* #C15.8 "Ösophagus, mehrere Teilbereiche überlappend"
* #C15.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C15.8 ^property[0].code = #parent 
* #C15.8 ^property[0].valueCode = #C15 
* #C15.9 "Ösophagus o.n.A."
* #C15.9 ^property[0].code = #parent 
* #C15.9 ^property[0].valueCode = #C15 
* #C16 "Magen"
* #C16 ^property[0].code = #parent 
* #C16 ^property[0].valueCode = #C15-C26 
* #C16 ^property[1].code = #child 
* #C16 ^property[1].valueCode = #C16.0 
* #C16 ^property[2].code = #child 
* #C16 ^property[2].valueCode = #C16.1 
* #C16 ^property[3].code = #child 
* #C16 ^property[3].valueCode = #C16.2 
* #C16 ^property[4].code = #child 
* #C16 ^property[4].valueCode = #C16.3 
* #C16 ^property[5].code = #child 
* #C16 ^property[5].valueCode = #C16.4 
* #C16 ^property[6].code = #child 
* #C16 ^property[6].valueCode = #C16.5 
* #C16 ^property[7].code = #child 
* #C16 ^property[7].valueCode = #C16.6 
* #C16 ^property[8].code = #child 
* #C16 ^property[8].valueCode = #C16.8 
* #C16 ^property[9].code = #child 
* #C16 ^property[9].valueCode = #C16.9 
* #C16.0 "Kardia o.n.A."
* #C16.0 ^designation[0].value = "Kardia" 
* #C16.0 ^designation[1].value = "Ösophago-kardialer ÜbergangGastro-ösophagealer Übergang" 
* #C16.0 ^designation[2].value = "Ösophago-kardialer ÜbergangÖsophago-gastraler Übergang" 
* #C16.0 ^property[0].code = #parent 
* #C16.0 ^property[0].valueCode = #C16 
* #C16.1 "Fundus ventriculi"
* #C16.1 ^designation[0].value = "Magenfundus" 
* #C16.1 ^property[0].code = #parent 
* #C16.1 ^property[0].valueCode = #C16 
* #C16.2 "Corpus ventriculi"
* #C16.2 ^designation[0].value = "Magenkorpus" 
* #C16.2 ^property[0].code = #parent 
* #C16.2 ^property[0].valueCode = #C16 
* #C16.3 "Antrum ventriculi"
* #C16.3 ^designation[0].value = "Antrum pyloricum" 
* #C16.3 ^designation[1].value = "Magenantrum" 
* #C16.3 ^property[0].code = #parent 
* #C16.3 ^property[0].valueCode = #C16 
* #C16.4 "Pylorus"
* #C16.4 ^designation[0].value = "Magenpförtner" 
* #C16.4 ^property[0].code = #parent 
* #C16.4 ^property[0].valueCode = #C16 
* #C16.5 "Kleine Kurvatur o.n.A"
* #C16.5 ^property[0].code = #parent 
* #C16.5 ^property[0].valueCode = #C16 
* #C16.6 "Große Kurvatur o.n.A"
* #C16.6 ^property[0].code = #parent 
* #C16.6 ^property[0].valueCode = #C16 
* #C16.8 "Magen, mehrere Teilbereiche überlappend"
* #C16.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C16.8 ^designation[0].value = "Magenhinterwand o.n.A." 
* #C16.8 ^designation[1].value = "Magenvorderwand o.n.A." 
* #C16.8 ^property[0].code = #parent 
* #C16.8 ^property[0].valueCode = #C16 
* #C16.9 "Magen o.n.A."
* #C16.9 ^designation[0].value = "Gastrisch o.n.A." 
* #C16.9 ^property[0].code = #parent 
* #C16.9 ^property[0].valueCode = #C16 
* #C17 "Dünndarm"
* #C17 ^property[0].code = #parent 
* #C17 ^property[0].valueCode = #C15-C26 
* #C17 ^property[1].code = #child 
* #C17 ^property[1].valueCode = #C17.0 
* #C17 ^property[2].code = #child 
* #C17 ^property[2].valueCode = #C17.1 
* #C17 ^property[3].code = #child 
* #C17 ^property[3].valueCode = #C17.2 
* #C17 ^property[4].code = #child 
* #C17 ^property[4].valueCode = #C17.3 
* #C17 ^property[5].code = #child 
* #C17 ^property[5].valueCode = #C17.8 
* #C17 ^property[6].code = #child 
* #C17 ^property[6].valueCode = #C17.9 
* #C17.0 "Duodenum"
* #C17.0 ^property[0].code = #parent 
* #C17.0 ^property[0].valueCode = #C17 
* #C17.1 "Jejunum"
* #C17.1 ^property[0].code = #parent 
* #C17.1 ^property[0].valueCode = #C17 
* #C17.2 "Ileum"
* #C17.2 ^designation[0].value = "Ileozökalklappe" 
* #C17.2 ^property[0].code = #parent 
* #C17.2 ^property[0].valueCode = #C17 
* #C17.3 "Meckel-Divertikel"
* #C17.3 ^property[0].code = #parent 
* #C17.3 ^property[0].valueCode = #C17 
* #C17.8 "Dünndarm, mehrere Teilbereiche überlappend"
* #C17.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C17.8 ^property[0].code = #parent 
* #C17.8 ^property[0].valueCode = #C17 
* #C17.9 "Dünndarm o.n.A."
* #C17.9 ^designation[0].value = "Intestinum tenue o.n.A." 
* #C17.9 ^property[0].code = #parent 
* #C17.9 ^property[0].valueCode = #C17 
* #C18 "Kolon"
* #C18 ^property[0].code = #parent 
* #C18 ^property[0].valueCode = #C15-C26 
* #C18 ^property[1].code = #child 
* #C18 ^property[1].valueCode = #C18.0 
* #C18 ^property[2].code = #child 
* #C18 ^property[2].valueCode = #C18.1 
* #C18 ^property[3].code = #child 
* #C18 ^property[3].valueCode = #C18.2 
* #C18 ^property[4].code = #child 
* #C18 ^property[4].valueCode = #C18.3 
* #C18 ^property[5].code = #child 
* #C18 ^property[5].valueCode = #C18.4 
* #C18 ^property[6].code = #child 
* #C18 ^property[6].valueCode = #C18.5 
* #C18 ^property[7].code = #child 
* #C18 ^property[7].valueCode = #C18.6 
* #C18 ^property[8].code = #child 
* #C18 ^property[8].valueCode = #C18.7 
* #C18 ^property[9].code = #child 
* #C18 ^property[9].valueCode = #C18.8 
* #C18 ^property[10].code = #child 
* #C18 ^property[10].valueCode = #C18.9 
* #C18.0 "Zökum"
* #C18.0 ^designation[0].value = "Ileozökaler Übergang" 
* #C18.0 ^designation[1].value = "Ileozökalklappe" 
* #C18.0 ^property[0].code = #parent 
* #C18.0 ^property[0].valueCode = #C18 
* #C18.1 "Appendix vermiformis"
* #C18.1 ^designation[0].value = "Blinddarm" 
* #C18.1 ^designation[1].value = "Wurmfortsatz" 
* #C18.1 ^property[0].code = #parent 
* #C18.1 ^property[0].valueCode = #C18 
* #C18.2 "Colon ascendens"
* #C18.2 ^designation[0].value = "Rechtes Kolon" 
* #C18.2 ^property[0].code = #parent 
* #C18.2 ^property[0].valueCode = #C18 
* #C18.3 "Flexura hepatica"
* #C18.3 ^designation[0].value = "Rechte Kolonflexur" 
* #C18.3 ^property[0].code = #parent 
* #C18.3 ^property[0].valueCode = #C18 
* #C18.4 "Colon transversum"
* #C18.4 ^designation[0].value = "Querkolon" 
* #C18.4 ^property[0].code = #parent 
* #C18.4 ^property[0].valueCode = #C18 
* #C18.5 "Flexura lienalis coli"
* #C18.5 ^designation[0].value = "Linke Kolonflexur" 
* #C18.5 ^property[0].code = #parent 
* #C18.5 ^property[0].valueCode = #C18 
* #C18.6 "Colon descendens"
* #C18.6 ^designation[0].value = "Linkes Kolon" 
* #C18.6 ^property[0].code = #parent 
* #C18.6 ^property[0].valueCode = #C18 
* #C18.7 "Colon sigmoideum"
* #C18.7 ^designation[0].value = "Flexura sigmoidea coli" 
* #C18.7 ^designation[1].value = "Sigma o.n.A." 
* #C18.7 ^property[0].code = #parent 
* #C18.7 ^property[0].valueCode = #C18 
* #C18.8 "Kolon, mehrere Teilbereiche überlappend"
* #C18.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C18.8 ^property[0].code = #parent 
* #C18.8 ^property[0].valueCode = #C18 
* #C18.9 "Colon"
* #C18.9 ^designation[0].value = "Dickdarm" 
* #C18.9 ^designation[1].value = "Dickdarm o.n.A." 
* #C18.9 ^property[0].code = #parent 
* #C18.9 ^property[0].valueCode = #C18 
* #C19 "Rektosigmoidaler Übergang"
* #C19 ^property[0].code = #parent 
* #C19 ^property[0].valueCode = #C15-C26 
* #C19 ^property[1].code = #child 
* #C19 ^property[1].valueCode = #C19.9 
* #C19.9 "Rektosigmoidaler Übergang"
* #C19.9 ^designation[0].value = "Kolon, am rektosigmoidalen Übergang" 
* #C19.9 ^designation[1].value = "Rektosigmoid o.n.A." 
* #C19.9 ^designation[2].value = "Kolon und Rektum" 
* #C19.9 ^property[0].code = #parent 
* #C19.9 ^property[0].valueCode = #C19 
* #C20 "Rektum"
* #C20 ^property[0].code = #parent 
* #C20 ^property[0].valueCode = #C15-C26 
* #C20 ^property[1].code = #child 
* #C20 ^property[1].valueCode = #C20.9 
* #C20.9 "Rektum o.n.A."
* #C20.9 ^designation[0].value = "Rektumampulle" 
* #C20.9 ^property[0].code = #parent 
* #C20.9 ^property[0].valueCode = #C20 
* #C21 "Analkanal und Anus"
* #C21 ^property[0].code = #parent 
* #C21 ^property[0].valueCode = #C15-C26 
* #C21 ^property[1].code = #child 
* #C21 ^property[1].valueCode = #C21.0 
* #C21 ^property[2].code = #child 
* #C21 ^property[2].valueCode = #C21.1 
* #C21 ^property[3].code = #child 
* #C21 ^property[3].valueCode = #C21.2 
* #C21 ^property[4].code = #child 
* #C21 ^property[4].valueCode = #C21.8 
* #C21.0 "Anus o.n.A."
* #C21.0 ^designation[0].value = "Haut am Anus und perianale Haut" 
* #C21.0 ^property[0].code = #parent 
* #C21.0 ^property[0].valueCode = #C21 
* #C21.1 "Analkanal"
* #C21.1 ^designation[0].value = "Analsphinkter" 
* #C21.1 ^designation[1].value = "Sphincter ani" 
* #C21.1 ^property[0].code = #parent 
* #C21.1 ^property[0].valueCode = #C21 
* #C21.2 "Kloakenregion"
* #C21.2 ^property[0].code = #parent 
* #C21.2 ^property[0].valueCode = #C21 
* #C21.8 "Rektum, Anus und Analkanal, mehrere Bereiche überlappend"
* #C21.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C21.8 ^designation[0].value = "Anorektaler Übergang" 
* #C21.8 ^designation[1].value = "Anorektum" 
* #C21.8 ^property[0].code = #parent 
* #C21.8 ^property[0].valueCode = #C21 
* #C22 "Leber und intrahepatische Gallengänge"
* #C22 ^property[0].code = #parent 
* #C22 ^property[0].valueCode = #C15-C26 
* #C22 ^property[1].code = #child 
* #C22 ^property[1].valueCode = #C22.0 
* #C22 ^property[2].code = #child 
* #C22 ^property[2].valueCode = #C22.1 
* #C22.0 "Leber"
* #C22.0 ^designation[0].value = "Hepatisch o.n.A." 
* #C22.0 ^property[0].code = #parent 
* #C22.0 ^property[0].valueCode = #C22 
* #C22.1 "Intrahepatische Gallengänge"
* #C22.1 ^designation[0].value = "Cholangioli" 
* #C22.1 ^designation[1].value = "Gallenkanälchen" 
* #C22.1 ^property[0].code = #parent 
* #C22.1 ^property[0].valueCode = #C22 
* #C23 "Gallenblase"
* #C23 ^property[0].code = #parent 
* #C23 ^property[0].valueCode = #C15-C26 
* #C23 ^property[1].code = #child 
* #C23 ^property[1].valueCode = #C23.9 
* #C23.9 "Gallenblase"
* #C23.9 ^property[0].code = #parent 
* #C23.9 ^property[0].valueCode = #C23 
* #C24 "Sonstige und nicht näher bezeichnete Teile der Gallenwege"
* #C24 ^property[0].code = #parent 
* #C24 ^property[0].valueCode = #C15-C26 
* #C24 ^property[1].code = #child 
* #C24 ^property[1].valueCode = #C24.0 
* #C24 ^property[2].code = #child 
* #C24 ^property[2].valueCode = #C24.1 
* #C24 ^property[3].code = #child 
* #C24 ^property[3].valueCode = #C24.8 
* #C24 ^property[4].code = #child 
* #C24 ^property[4].valueCode = #C24.9 
* #C24.0 "Extrahepatischer Gallengang"
* #C24.0 ^designation[0].value = "Gallengang o.n.A." 
* #C24.0 ^designation[1].value = "Ductus choledochus" 
* #C24.0 ^designation[2].value = "Ductus hepaticus communis" 
* #C24.0 ^designation[3].value = "GallenblasengangDuctus cysticus" 
* #C24.0 ^designation[4].value = "Leber-GallengangDuctus hepaticus" 
* #C24.0 ^designation[5].value = "Sphincter Oddi" 
* #C24.0 ^property[0].code = #parent 
* #C24.0 ^property[0].valueCode = #C24 
* #C24.1 "Ampulla Vateri"
* #C24.1 ^designation[0].value = "Periampullär" 
* #C24.1 ^property[0].code = #parent 
* #C24.1 ^property[0].valueCode = #C24 
* #C24.8 "Gallenwege, mehrere Bereiche überlappend"
* #C24.8 ^designation[0].value = "Intrahepatische und extrahepatische Gallengänge" 
* #C24.8 ^property[0].code = #parent 
* #C24.8 ^property[0].valueCode = #C24 
* #C24.9 "Gallenwege o.n.A."
* #C24.9 ^property[0].code = #parent 
* #C24.9 ^property[0].valueCode = #C24 
* #C25 "Pankreas"
* #C25 ^property[0].code = #parent 
* #C25 ^property[0].valueCode = #C15-C26 
* #C25 ^property[1].code = #child 
* #C25 ^property[1].valueCode = #C25.0 
* #C25 ^property[2].code = #child 
* #C25 ^property[2].valueCode = #C25.1 
* #C25 ^property[3].code = #child 
* #C25 ^property[3].valueCode = #C25.2 
* #C25 ^property[4].code = #child 
* #C25 ^property[4].valueCode = #C25.3 
* #C25 ^property[5].code = #child 
* #C25 ^property[5].valueCode = #C25.4 
* #C25 ^property[6].code = #child 
* #C25 ^property[6].valueCode = #C25.7 
* #C25 ^property[7].code = #child 
* #C25 ^property[7].valueCode = #C25.8 
* #C25 ^property[8].code = #child 
* #C25 ^property[8].valueCode = #C25.9 
* #C25.0 "Pankreaskopf"
* #C25.0 ^property[0].code = #parent 
* #C25.0 ^property[0].valueCode = #C25 
* #C25.1 "Pankreaskörper"
* #C25.1 ^property[0].code = #parent 
* #C25.1 ^property[0].valueCode = #C25 
* #C25.2 "Pankreasschwanz"
* #C25.2 ^property[0].code = #parent 
* #C25.2 ^property[0].valueCode = #C25 
* #C25.3 "Ductus pancreaticus"
* #C25.3 ^designation[0].value = "Ductus Wirsungi" 
* #C25.3 ^designation[1].value = "Ductus pancreaticus accessoriusDuctus Santorini" 
* #C25.3 ^property[0].code = #parent 
* #C25.3 ^property[0].valueCode = #C25 
* #C25.4 "Pankreas-Inselzellen"
* #C25.4 ^designation[0].value = "Endokriner Drüsenanteil des Pankreas" 
* #C25.4 ^designation[1].value = "Langerhans-Inseln" 
* #C25.4 ^property[0].code = #parent 
* #C25.4 ^property[0].valueCode = #C25 
* #C25.7 "Sonstige näher bezeichnete Teile des Pankreas"
* #C25.7 ^property[0].code = #parent 
* #C25.7 ^property[0].valueCode = #C25 
* #C25.8 "Pankreas, mehrere Teilbereiche überlappend"
* #C25.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C25.8 ^property[0].code = #parent 
* #C25.8 ^property[0].valueCode = #C25 
* #C25.9 "Pankreas o.n.A."
* #C25.9 ^property[0].code = #parent 
* #C25.9 ^property[0].valueCode = #C25 
* #C26 "Sonstiger oder mangelhaft bezeichneter Sitz innerhalb der Verdauungsorgane"
* #C26 ^property[0].code = #parent 
* #C26 ^property[0].valueCode = #C15-C26 
* #C26 ^property[1].code = #child 
* #C26 ^property[1].valueCode = #C26.0 
* #C26 ^property[2].code = #child 
* #C26 ^property[2].valueCode = #C26.8 
* #C26 ^property[3].code = #child 
* #C26 ^property[3].valueCode = #C26.9 
* #C26.0 "Intestinaltrakt o.n.A."
* #C26.0 ^designation[0].value = "Darm o.n.A." 
* #C26.0 ^designation[1].value = "Intestinum o.n.A." 
* #C26.0 ^property[0].code = #parent 
* #C26.0 ^property[0].valueCode = #C26 
* #C26.8 "Verdauungssystem, mehrere Bereiche überlappend"
* #C26.8 ^property[0].code = #parent 
* #C26.8 ^property[0].valueCode = #C26 
* #C26.9 "Gastrointestinaltrakt o.n.A."
* #C26.9 ^designation[0].value = "Verdauungsorgane o.n.A." 
* #C26.9 ^property[0].code = #parent 
* #C26.9 ^property[0].valueCode = #C26 
* #C30-C39 "Atemwege [Respirationstrakt] und intrathorakale Organe"
* #C30-C39 ^property[0].code = #parent 
* #C30-C39 ^property[0].valueCode = #T 
* #C30-C39 ^property[1].code = #child 
* #C30-C39 ^property[1].valueCode = #C30 
* #C30-C39 ^property[2].code = #child 
* #C30-C39 ^property[2].valueCode = #C31 
* #C30-C39 ^property[3].code = #child 
* #C30-C39 ^property[3].valueCode = #C32 
* #C30-C39 ^property[4].code = #child 
* #C30-C39 ^property[4].valueCode = #C33 
* #C30-C39 ^property[5].code = #child 
* #C30-C39 ^property[5].valueCode = #C34 
* #C30-C39 ^property[6].code = #child 
* #C30-C39 ^property[6].valueCode = #C37 
* #C30-C39 ^property[7].code = #child 
* #C30-C39 ^property[7].valueCode = #C38 
* #C30-C39 ^property[8].code = #child 
* #C30-C39 ^property[8].valueCode = #C39 
* #C30 "Nasenhöhle und Mittelohr"
* #C30 ^property[0].code = #parent 
* #C30 ^property[0].valueCode = #C30-C39 
* #C30 ^property[1].code = #child 
* #C30 ^property[1].valueCode = #C30.0 
* #C30 ^property[2].code = #child 
* #C30 ^property[2].valueCode = #C30.1 
* #C30.0 "Nasenhöhle"
* #C30.0 ^designation[0].value = "Innere Nase" 
* #C30.0 ^designation[1].value = "Nasenknorpel" 
* #C30.0 ^designation[2].value = "NasenlöcherNares" 
* #C30.0 ^designation[3].value = "Nasenmuscheln" 
* #C30.0 ^designation[4].value = "Nasenscheidewand o.n.A." 
* #C30.0 ^designation[5].value = "Nasenschleimhaut" 
* #C30.0 ^designation[6].value = "Nasenvorhof" 
* #C30.0 ^designation[7].value = "Nase o.n.A." 
* #C30.0 ^property[0].code = #parent 
* #C30.0 ^property[0].valueCode = #C30 
* #C30.1 "Mittelohr"
* #C30.1 ^designation[0].value = "Antrum mastoideum" 
* #C30.1 ^designation[1].value = "Eustachische Röhre" 
* #C30.1 ^designation[2].value = "Innenohr" 
* #C30.1 ^designation[3].value = "Paukenhöhle" 
* #C30.1 ^designation[4].value = "Tuba auditiva" 
* #C30.1 ^property[0].code = #parent 
* #C30.1 ^property[0].valueCode = #C30 
* #C31 "Nasennebenhöhlen"
* #C31 ^property[0].code = #parent 
* #C31 ^property[0].valueCode = #C30-C39 
* #C31 ^property[1].code = #child 
* #C31 ^property[1].valueCode = #C31.0 
* #C31 ^property[2].code = #child 
* #C31 ^property[2].valueCode = #C31.1 
* #C31 ^property[3].code = #child 
* #C31 ^property[3].valueCode = #C31.2 
* #C31 ^property[4].code = #child 
* #C31 ^property[4].valueCode = #C31.3 
* #C31 ^property[5].code = #child 
* #C31 ^property[5].valueCode = #C31.8 
* #C31 ^property[6].code = #child 
* #C31 ^property[6].valueCode = #C31.9 
* #C31.0 "Sinus maxillaris"
* #C31.0 ^designation[0].value = "Highmore-Höhle" 
* #C31.0 ^designation[1].value = "Kieferhöhle o.n.A" 
* #C31.0 ^property[0].code = #parent 
* #C31.0 ^property[0].valueCode = #C31 
* #C31.1 "Sinus ethmoidalis"
* #C31.1 ^designation[0].value = "Labyrinthus ethmoidalis" 
* #C31.1 ^designation[1].value = "SiebbeinzellenCellulae ethmoidales" 
* #C31.1 ^property[0].code = #parent 
* #C31.1 ^property[0].valueCode = #C31 
* #C31.2 "Sinus frontalis"
* #C31.2 ^designation[0].value = "Stirnhöhle" 
* #C31.2 ^property[0].code = #parent 
* #C31.2 ^property[0].valueCode = #C31 
* #C31.3 "Sinus sphenoidalis"
* #C31.3 ^designation[0].value = "Keilbeinhöhle" 
* #C31.3 ^property[0].code = #parent 
* #C31.3 ^property[0].valueCode = #C31 
* #C31.8 "Nasennebenhöhlen, mehrere Teilbereiche überlappend"
* #C31.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C31.8 ^property[0].code = #parent 
* #C31.8 ^property[0].valueCode = #C31 
* #C31.9 "Nasennebenhöhlen o.n.A."
* #C31.9 ^designation[0].value = "Nasennebenhöhle" 
* #C31.9 ^designation[1].value = "Sinus paranasalis" 
* #C31.9 ^property[0].code = #parent 
* #C31.9 ^property[0].valueCode = #C31 
* #C32 "Larynx"
* #C32 ^property[0].code = #parent 
* #C32 ^property[0].valueCode = #C30-C39 
* #C32 ^property[1].code = #child 
* #C32 ^property[1].valueCode = #C32.0 
* #C32 ^property[2].code = #child 
* #C32 ^property[2].valueCode = #C32.1 
* #C32 ^property[3].code = #child 
* #C32 ^property[3].valueCode = #C32.2 
* #C32 ^property[4].code = #child 
* #C32 ^property[4].valueCode = #C32.3 
* #C32 ^property[5].code = #child 
* #C32 ^property[5].valueCode = #C32.8 
* #C32 ^property[6].code = #child 
* #C32 ^property[6].valueCode = #C32.9 
* #C32.0 "Glottis"
* #C32.0 ^designation[0].value = "Larynx-Kommissur" 
* #C32.0 ^designation[1].value = "Stimmband o.n.A.Plica vocalis" 
* #C32.0 ^designation[2].value = "Stimmband o.n.A.Stimmband" 
* #C32.0 ^designation[3].value = "Ventriculus laryngis" 
* #C32.0 ^property[0].code = #parent 
* #C32.0 ^property[0].valueCode = #C32 
* #C32.1 "Supraglottis"
* #C32.1 ^designation[0].value = "Epiglottis o.n.A." 
* #C32.1 ^designation[1].value = "Hinterfläche der Epiglottis" 
* #C32.1 ^designation[2].value = "Laryngeale Seite der Plica aryepiglottica" 
* #C32.1 ^designation[3].value = "Plica vestibularisTaschenband" 
* #C32.1 ^designation[4].value = "Plica vestibularisTaschenfalte" 
* #C32.1 ^designation[5].value = "Vestibulum laryngis" 
* #C32.1 ^property[0].code = #parent 
* #C32.1 ^property[0].valueCode = #C32 
* #C32.2 "Subglottis"
* #C32.2 ^property[0].code = #parent 
* #C32.2 ^property[0].valueCode = #C32 
* #C32.3 "Larynxknorpel"
* #C32.3 ^designation[0].value = "Cartilago arytaenoidea" 
* #C32.3 ^designation[1].value = "Cartilago cuneiformis" 
* #C32.3 ^designation[2].value = "RingknorpelCartilago cricoidea" 
* #C32.3 ^designation[3].value = "SchildknorpelCartilago thyroidea" 
* #C32.3 ^property[0].code = #parent 
* #C32.3 ^property[0].valueCode = #C32 
* #C32.8 "Larynx, mehrere Teilbereiche überlappend"
* #C32.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C32.8 ^property[0].code = #parent 
* #C32.8 ^property[0].valueCode = #C32 
* #C32.9 "Kehlkopf o.n.A."
* #C32.9 ^property[0].code = #parent 
* #C32.9 ^property[0].valueCode = #C32 
* #C33 "Trachea"
* #C33 ^property[0].code = #parent 
* #C33 ^property[0].valueCode = #C30-C39 
* #C33 ^property[1].code = #child 
* #C33 ^property[1].valueCode = #C33.9 
* #C33.9 "Trachea"
* #C33.9 ^property[0].code = #parent 
* #C33.9 ^property[0].valueCode = #C33 
* #C34 "Bronchus und Lunge"
* #C34 ^property[0].code = #parent 
* #C34 ^property[0].valueCode = #C30-C39 
* #C34 ^property[1].code = #child 
* #C34 ^property[1].valueCode = #C34.0 
* #C34 ^property[2].code = #child 
* #C34 ^property[2].valueCode = #C34.1 
* #C34 ^property[3].code = #child 
* #C34 ^property[3].valueCode = #C34.2 
* #C34 ^property[4].code = #child 
* #C34 ^property[4].valueCode = #C34.3 
* #C34 ^property[5].code = #child 
* #C34 ^property[5].valueCode = #C34.8 
* #C34 ^property[6].code = #child 
* #C34 ^property[6].valueCode = #C34.9 
* #C34.0 "Hauptbronchus"
* #C34.0 ^designation[0].value = "Carina" 
* #C34.0 ^designation[1].value = "Lungenhilus" 
* #C34.0 ^property[0].code = #parent 
* #C34.0 ^property[0].valueCode = #C34 
* #C34.1 "Lungenoberlappen"
* #C34.1 ^designation[0].value = "Lingula" 
* #C34.1 ^designation[1].value = "Oberlappenbronchus" 
* #C34.1 ^property[0].code = #parent 
* #C34.1 ^property[0].valueCode = #C34 
* #C34.2 "Lungenmittellappen"
* #C34.2 ^designation[0].value = "Mittellappenbronchus" 
* #C34.2 ^property[0].code = #parent 
* #C34.2 ^property[0].valueCode = #C34 
* #C34.3 "Lungenunterlappen"
* #C34.3 ^designation[0].value = "Unterlappenbronchus" 
* #C34.3 ^property[0].code = #parent 
* #C34.3 ^property[0].valueCode = #C34 
* #C34.8 "Lunge, mehrere Teilbereiche überlappend"
* #C34.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C34.8 ^property[0].code = #parent 
* #C34.8 ^property[0].valueCode = #C34 
* #C34.9 "Lunge o.n.A."
* #C34.9 ^designation[0].value = "Bronchial o.n.A." 
* #C34.9 ^designation[1].value = "Bronchien o.n.A." 
* #C34.9 ^designation[2].value = "Bronchiogen" 
* #C34.9 ^designation[3].value = "Bronchiolen" 
* #C34.9 ^designation[4].value = "Pulmonal o.n.A." 
* #C34.9 ^property[0].code = #parent 
* #C34.9 ^property[0].valueCode = #C34 
* #C37 "Thymus"
* #C37 ^property[0].code = #parent 
* #C37 ^property[0].valueCode = #C30-C39 
* #C37 ^property[1].code = #child 
* #C37 ^property[1].valueCode = #C37.9 
* #C37.9 "Thymus"
* #C37.9 ^property[0].code = #parent 
* #C37.9 ^property[0].valueCode = #C37 
* #C38 "Herz, Mediastinum und Pleura"
* #C38 ^property[0].code = #parent 
* #C38 ^property[0].valueCode = #C30-C39 
* #C38 ^property[1].code = #child 
* #C38 ^property[1].valueCode = #C38.0 
* #C38 ^property[2].code = #child 
* #C38 ^property[2].valueCode = #C38.1 
* #C38 ^property[3].code = #child 
* #C38 ^property[3].valueCode = #C38.2 
* #C38 ^property[4].code = #child 
* #C38 ^property[4].valueCode = #C38.3 
* #C38 ^property[5].code = #child 
* #C38 ^property[5].valueCode = #C38.4 
* #C38 ^property[6].code = #child 
* #C38 ^property[6].valueCode = #C38.8 
* #C38.0 "Herz"
* #C38.0 ^designation[0].value = "Endokard" 
* #C38.0 ^designation[1].value = "Epikard" 
* #C38.0 ^designation[2].value = "Herzkammer" 
* #C38.0 ^designation[3].value = "Herzvorhof" 
* #C38.0 ^designation[4].value = "Myokard" 
* #C38.0 ^designation[5].value = "Perikard" 
* #C38.0 ^property[0].code = #parent 
* #C38.0 ^property[0].valueCode = #C38 
* #C38.1 "Vorderes Mediastinum"
* #C38.1 ^property[0].code = #parent 
* #C38.1 ^property[0].valueCode = #C38 
* #C38.2 "Hinteres Mediastinum"
* #C38.2 ^property[0].code = #parent 
* #C38.2 ^property[0].valueCode = #C38 
* #C38.3 "Mediastinum o.n.A."
* #C38.3 ^property[0].code = #parent 
* #C38.3 ^property[0].valueCode = #C38 
* #C38.4 "Pleura o.n.A."
* #C38.4 ^designation[0].value = "Pleura parietalis" 
* #C38.4 ^designation[1].value = "Pleura visceralis" 
* #C38.4 ^property[0].code = #parent 
* #C38.4 ^property[0].valueCode = #C38 
* #C38.8 "Herz, Mediastinum und Pleura, mehrere Teilbereiche überlappend"
* #C38.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C38.8 ^property[0].code = #parent 
* #C38.8 ^property[0].valueCode = #C38 
* #C39 "Sonstiger oder mangelhaft bezeichneter Sitz innerhalb der Atemwege [Respirationstrakt] und intrathorakaler Organe"
* #C39 ^property[0].code = #parent 
* #C39 ^property[0].valueCode = #C30-C39 
* #C39 ^property[1].code = #child 
* #C39 ^property[1].valueCode = #C39.0 
* #C39 ^property[2].code = #child 
* #C39 ^property[2].valueCode = #C39.8 
* #C39 ^property[3].code = #child 
* #C39 ^property[3].valueCode = #C39.9 
* #C39.0 "Obere Atemwege o.n.A."
* #C39.0 ^designation[0].value = "Oberer Respirationstrakt o.n.A." 
* #C39.0 ^property[0].code = #parent 
* #C39.0 ^property[0].valueCode = #C39 
* #C39.8 "Atemwege [Respirationstrakt] und intrathorakale Organe, mehrere Bereiche überlappend"
* #C39.8 ^property[0].code = #parent 
* #C39.8 ^property[0].valueCode = #C39 
* #C39.9 "Mangelhaft bezeichneter Sitz innerhalb der Atmungsorgane"
* #C39.9 ^designation[0].value = "Respirationstrakt o.n.A." 
* #C39.9 ^property[0].code = #parent 
* #C39.9 ^property[0].valueCode = #C39 
* #C40-C41 "Knochen, Gelenke und Gelenkknorpel"
* #C40-C41 ^property[0].code = #parent 
* #C40-C41 ^property[0].valueCode = #T 
* #C40-C41 ^property[1].code = #child 
* #C40-C41 ^property[1].valueCode = #C40 
* #C40-C41 ^property[2].code = #child 
* #C40-C41 ^property[2].valueCode = #C41 
* #C40 "Knochen, Gelenke und Gelenkknorpel der Extremitäten"
* #C40 ^property[0].code = #parent 
* #C40 ^property[0].valueCode = #C40-C41 
* #C40 ^property[1].code = #child 
* #C40 ^property[1].valueCode = #C40.0 
* #C40 ^property[2].code = #child 
* #C40 ^property[2].valueCode = #C40.1 
* #C40 ^property[3].code = #child 
* #C40 ^property[3].valueCode = #C40.2 
* #C40 ^property[4].code = #child 
* #C40 ^property[4].valueCode = #C40.3 
* #C40 ^property[5].code = #child 
* #C40 ^property[5].valueCode = #C40.8 
* #C40 ^property[6].code = #child 
* #C40 ^property[6].valueCode = #C40.9 
* #C40.0 "Lange Knochen von Arm und Schulter und zugehörige Gelenke"
* #C40.0 ^designation[0].value = "Akromioklavikulargelenk" 
* #C40.0 ^designation[1].value = "Armknochen" 
* #C40.0 ^designation[2].value = "Ellbogengelenk" 
* #C40.0 ^designation[3].value = "Humerus" 
* #C40.0 ^designation[4].value = "Knochen der Schulter" 
* #C40.0 ^designation[5].value = "Knochen des Unterarmes" 
* #C40.0 ^designation[6].value = "Radius" 
* #C40.0 ^designation[7].value = "Schultergelenk" 
* #C40.0 ^designation[8].value = "Schultergürtel" 
* #C40.0 ^designation[9].value = "Skapula" 
* #C40.0 ^designation[10].value = "Ulna" 
* #C40.0 ^property[0].code = #parent 
* #C40.0 ^property[0].valueCode = #C40 
* #C40.1 "Kurze Knochen der oberen Extremitäten und zugehörige Gelenke"
* #C40.1 ^designation[0].value = "Daumenknochen" 
* #C40.1 ^designation[1].value = "Fingerknochen" 
* #C40.1 ^designation[2].value = "Gelenk an der Hand" 
* #C40.1 ^designation[3].value = "Handgelenk" 
* #C40.1 ^designation[4].value = "Handknochen" 
* #C40.1 ^designation[5].value = "Handwurzelknochen" 
* #C40.1 ^designation[6].value = "Mittelhandknochen" 
* #C40.1 ^designation[7].value = "Os carpi" 
* #C40.1 ^designation[8].value = "Phalangen der Hand" 
* #C40.1 ^property[0].code = #parent 
* #C40.1 ^property[0].valueCode = #C40 
* #C40.2 "Lange Knochen der unteren Extremitäten und zugehörige Gelenke"
* #C40.2 ^designation[0].value = "Beinknochen" 
* #C40.2 ^designation[1].value = "Cartilago semilunaris" 
* #C40.2 ^designation[2].value = "Femur" 
* #C40.2 ^designation[3].value = "Fibula" 
* #C40.2 ^designation[4].value = "Kniegelenk o.n.A." 
* #C40.2 ^designation[5].value = "Meniscus lateralisAußenmeniskus" 
* #C40.2 ^designation[6].value = "Meniscus medialisInnenmeniskus" 
* #C40.2 ^designation[7].value = "Tibia" 
* #C40.2 ^property[0].code = #parent 
* #C40.2 ^property[0].valueCode = #C40 
* #C40.3 "Kurze Knochen der unteren Extremitäten und zugehörige Gelenke"
* #C40.3 ^designation[0].value = "Fußknochen" 
* #C40.3 ^designation[1].value = "FußwurzelknochenTarsalia" 
* #C40.3 ^designation[2].value = "Knochen der Ferse" 
* #C40.3 ^designation[3].value = "Knochen des Sprunggelenkes" 
* #C40.3 ^designation[4].value = "Knochen einer Zehe" 
* #C40.3 ^designation[5].value = "Mittelfußknochen" 
* #C40.3 ^designation[6].value = "Patella" 
* #C40.3 ^designation[7].value = "Sprunggelenk" 
* #C40.3 ^designation[8].value = "Tarsalgelenk" 
* #C40.3 ^designation[9].value = "Zehenphalangen" 
* #C40.3 ^property[0].code = #parent 
* #C40.3 ^property[0].valueCode = #C40 
* #C40.8 "Knochen, Gelenke und Gelenkknorpel der Extremitäten, mehrere Teilbereiche überlappend"
* #C40.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C40.8 ^property[0].code = #parent 
* #C40.8 ^property[0].valueCode = #C40 
* #C40.9 "Knochen einer Extremität o.n.A."
* #C40.9 ^designation[0].value = "Gelenk einer Extremität o.n.A." 
* #C40.9 ^designation[1].value = "Gelenkknorpel einer Extremität o.n.A." 
* #C40.9 ^designation[2].value = "Knorpel einer Extremität o.n.A." 
* #C40.9 ^property[0].code = #parent 
* #C40.9 ^property[0].valueCode = #C40 
* #C41 "Knochen, Gelenke und Gelenkknorpel sonstiger und nicht näher bezeichneter Lokalisationen"
* #C41 ^property[0].code = #parent 
* #C41 ^property[0].valueCode = #C40-C41 
* #C41 ^property[1].code = #child 
* #C41 ^property[1].valueCode = #C41.0 
* #C41 ^property[2].code = #child 
* #C41 ^property[2].valueCode = #C41.1 
* #C41 ^property[3].code = #child 
* #C41 ^property[3].valueCode = #C41.2 
* #C41 ^property[4].code = #child 
* #C41 ^property[4].valueCode = #C41.3 
* #C41 ^property[5].code = #child 
* #C41 ^property[5].valueCode = #C41.4 
* #C41 ^property[6].code = #child 
* #C41 ^property[6].valueCode = #C41.8 
* #C41 ^property[7].code = #child 
* #C41 ^property[7].valueCode = #C41.9 
* #C41.0 "Knochen des Hirn- und Gesichtsschädels und zugehörige Gelenke"
* #C41.0 ^designation[0].value = "Calvaria" 
* #C41.0 ^designation[1].value = "Gehirnschädel" 
* #C41.0 ^designation[2].value = "Gesichtsknochen" 
* #C41.0 ^designation[3].value = "Knochen der Orbita" 
* #C41.0 ^designation[4].value = "MaxillaOberkieferknochen" 
* #C41.0 ^designation[5].value = "Os ethmoidaleSiebbein" 
* #C41.0 ^designation[6].value = "Os frontaleStirnbein" 
* #C41.0 ^designation[7].value = "Os hyoideumZungenbein" 
* #C41.0 ^designation[8].value = "Os nasaleNasenbein" 
* #C41.0 ^designation[9].value = "Os occipitaleHinterhauptsbein" 
* #C41.0 ^designation[10].value = "Os parietaleScheitelbein" 
* #C41.0 ^designation[11].value = "Os sphenoidaleKeilbein" 
* #C41.0 ^designation[12].value = "Os temporaleSchläfenbein" 
* #C41.0 ^designation[13].value = "Os zygomaticumJochbein" 
* #C41.0 ^designation[14].value = "Schädel o.n.A." 
* #C41.0 ^designation[15].value = "Mandibula" 
* #C41.0 ^property[0].code = #parent 
* #C41.0 ^property[0].valueCode = #C41 
* #C41.1 "Mandibula"
* #C41.1 ^designation[0].value = "Kieferknochen o.n.A." 
* #C41.1 ^designation[1].value = "Unterkieferknochen" 
* #C41.1 ^designation[2].value = "TemporomandibulargelenkKiefergelenk" 
* #C41.1 ^property[0].code = #parent 
* #C41.1 ^property[0].valueCode = #C41 
* #C41.2 "Wirbelsäule"
* #C41.2 ^designation[0].value = "Atlas" 
* #C41.2 ^designation[1].value = "Axis" 
* #C41.2 ^designation[2].value = "Columna vertebralis" 
* #C41.2 ^designation[3].value = "Discus intervertebralisBandscheibe" 
* #C41.2 ^designation[4].value = "Knochen am Rücken" 
* #C41.2 ^designation[5].value = "Nucleus pulposus" 
* #C41.2 ^designation[6].value = "VertebraWirbel" 
* #C41.2 ^designation[7].value = "Os sacrum und Os coccygeum [Kreuzbein und Steißbein]" 
* #C41.2 ^property[0].code = #parent 
* #C41.2 ^property[0].valueCode = #C41 
* #C41.3 "Rippen, Sternum, Klavikula und zugehörige Gelenke"
* #C41.3 ^designation[0].value = "Kostovertebralgelenk" 
* #C41.3 ^designation[1].value = "Rippenknorpel" 
* #C41.3 ^designation[2].value = "Sternokostalgelenk" 
* #C41.3 ^property[0].code = #parent 
* #C41.3 ^property[0].valueCode = #C41 
* #C41.4 "Beckenknochen, Kreuzbein, Steißbein und zugehörige Gelenke"
* #C41.4 ^designation[0].value = "Azetabulum" 
* #C41.4 ^designation[1].value = "Beckenknochen" 
* #C41.4 ^designation[2].value = "Hüftgelenk" 
* #C41.4 ^designation[3].value = "Knochen der Hüfte" 
* #C41.4 ^designation[4].value = "Os coccygisSteißbein" 
* #C41.4 ^designation[5].value = "Os coxaeHüftbein" 
* #C41.4 ^designation[6].value = "Os iliumDarmbein" 
* #C41.4 ^designation[7].value = "Os ischiiSitzbein" 
* #C41.4 ^designation[8].value = "Os pubisSchambein" 
* #C41.4 ^designation[9].value = "Os sacrumKreuzbein" 
* #C41.4 ^designation[10].value = "Symphysis pubica" 
* #C41.4 ^property[0].code = #parent 
* #C41.4 ^property[0].valueCode = #C41 
* #C41.8 "Knochen, Gelenke und Gelenkknorpel, mehrere Bereiche überlappend"
* #C41.8 ^property[0].code = #parent 
* #C41.8 ^property[0].valueCode = #C41 
* #C41.9 "Knochen o.n.A."
* #C41.9 ^designation[0].value = "Gelenk o.n.A." 
* #C41.9 ^designation[1].value = "Gelenkknorpel o.n.A." 
* #C41.9 ^designation[2].value = "Knorpel o.n.A." 
* #C41.9 ^designation[3].value = "Skelettknochen o.n.A." 
* #C41.9 ^property[0].code = #parent 
* #C41.9 ^property[0].valueCode = #C41 
* #C42-C42 "Hämatopoetisches und retikuloendotheliales System"
* #C42-C42 ^property[0].code = #parent 
* #C42-C42 ^property[0].valueCode = #T 
* #C42-C42 ^property[1].code = #child 
* #C42-C42 ^property[1].valueCode = #C42 
* #C42 "Hämatopoetisches und retikuloendotheliales System"
* #C42 ^property[0].code = #parent 
* #C42 ^property[0].valueCode = #C42-C42 
* #C42 ^property[1].code = #child 
* #C42 ^property[1].valueCode = #C42.0 
* #C42 ^property[2].code = #child 
* #C42 ^property[2].valueCode = #C42.1 
* #C42 ^property[3].code = #child 
* #C42 ^property[3].valueCode = #C42.2 
* #C42 ^property[4].code = #child 
* #C42 ^property[4].valueCode = #C42.3 
* #C42 ^property[5].code = #child 
* #C42 ^property[5].valueCode = #C42.4 
* #C42.0 "Blut"
* #C42.0 ^property[0].code = #parent 
* #C42.0 ^property[0].valueCode = #C42 
* #C42.1 "Knochenmark"
* #C42.1 ^property[0].code = #parent 
* #C42.1 ^property[0].valueCode = #C42 
* #C42.2 "Milz"
* #C42.2 ^property[0].code = #parent 
* #C42.2 ^property[0].valueCode = #C42 
* #C42.3 "Retikuloendotheliales System o.n.A."
* #C42.3 ^property[0].code = #parent 
* #C42.3 ^property[0].valueCode = #C42 
* #C42.4 "Hämatopoetisches System o.n.A."
* #C42.4 ^property[0].code = #parent 
* #C42.4 ^property[0].valueCode = #C42 
* #C44-C44 "Haut"
* #C44-C44 ^property[0].code = #parent 
* #C44-C44 ^property[0].valueCode = #T 
* #C44-C44 ^property[1].code = #child 
* #C44-C44 ^property[1].valueCode = #C44 
* #C44 "Haut"
* #C44 ^designation[0].value = "Haut am Penis" 
* #C44 ^designation[1].value = "Haut an der Vulva" 
* #C44 ^designation[2].value = "Skrotalhaut" 
* #C44 ^property[0].code = #parent 
* #C44 ^property[0].valueCode = #C44-C44 
* #C44 ^property[1].code = #child 
* #C44 ^property[1].valueCode = #C44.0 
* #C44 ^property[2].code = #child 
* #C44 ^property[2].valueCode = #C44.1 
* #C44 ^property[3].code = #child 
* #C44 ^property[3].valueCode = #C44.2 
* #C44 ^property[4].code = #child 
* #C44 ^property[4].valueCode = #C44.3 
* #C44 ^property[5].code = #child 
* #C44 ^property[5].valueCode = #C44.4 
* #C44 ^property[6].code = #child 
* #C44 ^property[6].valueCode = #C44.5 
* #C44 ^property[7].code = #child 
* #C44 ^property[7].valueCode = #C44.6 
* #C44 ^property[8].code = #child 
* #C44 ^property[8].valueCode = #C44.7 
* #C44 ^property[9].code = #child 
* #C44 ^property[9].valueCode = #C44.8 
* #C44 ^property[10].code = #child 
* #C44 ^property[10].valueCode = #C44.9 
* #C44.0 "Lippenhaut o.n.A."
* #C44.0 ^designation[0].value = "Äußere Haut der Oberlippe" 
* #C44.0 ^designation[1].value = "Äußere Haut der Unterlippe" 
* #C44.0 ^property[0].code = #parent 
* #C44.0 ^property[0].valueCode = #C44 
* #C44.1 "Augenlid"
* #C44.1 ^designation[0].value = "Lid o.n.A." 
* #C44.1 ^designation[1].value = "Palpebra" 
* #C44.1 ^designation[2].value = "Äußerer AugenwinkelLateraler Augenwinkel" 
* #C44.1 ^designation[3].value = "Innerer AugenwinkelMedialer Augenwinkel" 
* #C44.1 ^designation[4].value = "Kanthus o.n.A." 
* #C44.1 ^designation[5].value = "Meibom-Drüse" 
* #C44.1 ^designation[6].value = "Oberlid" 
* #C44.1 ^designation[7].value = "Unterlid" 
* #C44.1 ^property[0].code = #parent 
* #C44.1 ^property[0].valueCode = #C44 
* #C44.2 "Äußeres Ohr"
* #C44.2 ^designation[0].value = "Auricula o.n.A." 
* #C44.2 ^designation[1].value = "Äußerer GehörgangGehörgang" 
* #C44.2 ^designation[2].value = "Äußerer GehörgangGehörgang o.n.A." 
* #C44.2 ^designation[3].value = "Äußerer GehörgangMeatus acusticus externus" 
* #C44.2 ^designation[4].value = "Glandulae ceruminosae" 
* #C44.2 ^designation[5].value = "Haut an der OhrmuschelHaut am Ohr o.n.A." 
* #C44.2 ^designation[6].value = "HelixSchnecke" 
* #C44.2 ^designation[7].value = "Lobulus auriculaeOhrläppchen" 
* #C44.2 ^designation[8].value = "Ohr o.n.A." 
* #C44.2 ^designation[9].value = "Ohrmuschel" 
* #C44.2 ^designation[10].value = "Tragus" 
* #C44.2 ^property[0].code = #parent 
* #C44.2 ^property[0].valueCode = #C44 
* #C44.3 "Haut sonstiger und nicht näher bezeichneter Teile des Gesichtes"
* #C44.3 ^designation[0].value = "AugenbraueBraue" 
* #C44.3 ^designation[1].value = "Haut vonGesicht" 
* #C44.3 ^designation[2].value = "Haut vonKiefer" 
* #C44.3 ^designation[3].value = "Haut vonKinn" 
* #C44.3 ^designation[4].value = "Haut vonNase" 
* #C44.3 ^designation[5].value = "Haut vonSchläfe" 
* #C44.3 ^designation[6].value = "Haut vonStirn" 
* #C44.3 ^designation[7].value = "Haut vonWange" 
* #C44.3 ^designation[8].value = "Kinn o.n.A" 
* #C44.3 ^designation[9].value = "Nase, außen" 
* #C44.3 ^designation[10].value = "Nasenflügel" 
* #C44.3 ^designation[11].value = "Nasensteg" 
* #C44.3 ^designation[12].value = "Schläfe o.n.A." 
* #C44.3 ^designation[13].value = "Stirn o.n.A." 
* #C44.3 ^designation[14].value = "Wange, außen" 
* #C44.3 ^property[0].code = #parent 
* #C44.3 ^property[0].valueCode = #C44 
* #C44.4 "Behaarte Kopfhaut und Haut am Hals"
* #C44.4 ^designation[0].value = "Behaarte KopfhautKopfschwarte o.n.A." 
* #C44.4 ^designation[1].value = "Haut am Hals" 
* #C44.4 ^designation[2].value = "Haut der Supraklavikularregion" 
* #C44.4 ^designation[3].value = "Haut der Zervikalregion" 
* #C44.4 ^designation[4].value = "Kopfhaut o.n.A." 
* #C44.4 ^property[0].code = #parent 
* #C44.4 ^property[0].valueCode = #C44 
* #C44.5 "Haut am Stamm"
* #C44.5 ^designation[0].value = "Haut vonAbdomen" 
* #C44.5 ^designation[1].value = "Haut vonAnus" 
* #C44.5 ^designation[2].value = "Haut vonAxilla" 
* #C44.5 ^designation[3].value = "Haut vonBauchwand" 
* #C44.5 ^designation[4].value = "Haut vonBrust" 
* #C44.5 ^designation[5].value = "Haut vonBrustkorb" 
* #C44.5 ^designation[6].value = "Haut vonBrustwand" 
* #C44.5 ^designation[7].value = "Haut vonDamm" 
* #C44.5 ^designation[8].value = "Haut vonFlanke" 
* #C44.5 ^designation[9].value = "Haut vonGesäß" 
* #C44.5 ^designation[10].value = "Haut vonGlutealregion" 
* #C44.5 ^designation[11].value = "Haut vonInfraklavikularregion" 
* #C44.5 ^designation[12].value = "Haut vonLeiste" 
* #C44.5 ^designation[13].value = "Haut vonLeistengegend" 
* #C44.5 ^designation[14].value = "Haut vonNabel" 
* #C44.5 ^designation[15].value = "Haut vonRegio sacrococcygea" 
* #C44.5 ^designation[16].value = "Haut vonRücken" 
* #C44.5 ^designation[17].value = "Haut vonSkapularregion" 
* #C44.5 ^designation[18].value = "Haut vonThorax" 
* #C44.5 ^designation[19].value = "Haut vonThoraxwand" 
* #C44.5 ^designation[20].value = "Nabel o.n.A." 
* #C44.5 ^designation[21].value = "Perianal" 
* #C44.5 ^property[0].code = #parent 
* #C44.5 ^property[0].valueCode = #C44 
* #C44.6 "Haut der oberen Extremitäten und der Schulter"
* #C44.6 ^designation[0].value = "Fingernagel" 
* #C44.6 ^designation[1].value = "Haut vonArm" 
* #C44.6 ^designation[2].value = "Haut vonDaumen" 
* #C44.6 ^designation[3].value = "Haut vonEllbogen" 
* #C44.6 ^designation[4].value = "Haut vonEllenbeuge" 
* #C44.6 ^designation[5].value = "Haut vonFinger" 
* #C44.6 ^designation[6].value = "Haut vonHand" 
* #C44.6 ^designation[7].value = "Haut vonHandfläche" 
* #C44.6 ^designation[8].value = "Haut vonHandwurzel" 
* #C44.6 ^designation[9].value = "Haut vonOberarm" 
* #C44.6 ^designation[10].value = "Haut vonObere Extremität" 
* #C44.6 ^designation[11].value = "Haut vonSchulter" 
* #C44.6 ^designation[12].value = "Haut vonUnterarm" 
* #C44.6 ^designation[13].value = "Palmarhaut" 
* #C44.6 ^property[0].code = #parent 
* #C44.6 ^property[0].valueCode = #C44 
* #C44.7 "Haut der unteren Extremität und der Hüfte"
* #C44.7 ^designation[0].value = "Fußsohle" 
* #C44.7 ^designation[1].value = "Haut vonBein" 
* #C44.7 ^designation[2].value = "Haut vonFerse" 
* #C44.7 ^designation[3].value = "Haut vonFuß" 
* #C44.7 ^designation[4].value = "Haut vonFußknöchel" 
* #C44.7 ^designation[5].value = "Haut vonHüfte" 
* #C44.7 ^designation[6].value = "Haut vonKnie" 
* #C44.7 ^designation[7].value = "Haut vonKniekehle" 
* #C44.7 ^designation[8].value = "Haut vonOberschenkel" 
* #C44.7 ^designation[9].value = "Haut vonUntere Extremität" 
* #C44.7 ^designation[10].value = "Haut vonUnterschenkel" 
* #C44.7 ^designation[11].value = "Haut vonWade" 
* #C44.7 ^designation[12].value = "Haut vonZehe" 
* #C44.7 ^designation[13].value = "Plantarhaut" 
* #C44.7 ^designation[14].value = "Zehennagel" 
* #C44.7 ^property[0].code = #parent 
* #C44.7 ^property[0].valueCode = #C44 
* #C44.8 "Haut, mehrere Teilbereiche überlappend"
* #C44.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C44.8 ^property[0].code = #parent 
* #C44.8 ^property[0].valueCode = #C44 
* #C44.9 "Haut o.n.A."
* #C44.9 ^designation[0].value = "Haut am Penis" 
* #C44.9 ^designation[1].value = "Haut an den großen Schamlippen" 
* #C44.9 ^designation[2].value = "Haut an der Vulva" 
* #C44.9 ^designation[3].value = "Skrotalhaut" 
* #C44.9 ^property[0].code = #parent 
* #C44.9 ^property[0].valueCode = #C44 
* #C47-C47 "Periphere Nerven und autonomes Nervensystem"
* #C47-C47 ^property[0].code = #parent 
* #C47-C47 ^property[0].valueCode = #T 
* #C47-C47 ^property[1].code = #child 
* #C47-C47 ^property[1].valueCode = #C47 
* #C47 "Periphere Nerven und autonomes Nervensystem"
* #C47 ^designation[0].value = "Autonomes Nervensystem" 
* #C47 ^designation[1].value = "Ganglien" 
* #C47 ^designation[2].value = "Nerven" 
* #C47 ^designation[3].value = "Parasympathisches Nervensystem" 
* #C47 ^designation[4].value = "Periphere Nerven" 
* #C47 ^designation[5].value = "Spinale Nerven" 
* #C47 ^designation[6].value = "Sympathisches System" 
* #C47 ^property[0].code = #parent 
* #C47 ^property[0].valueCode = #C47-C47 
* #C47 ^property[1].code = #child 
* #C47 ^property[1].valueCode = #C47.0 
* #C47 ^property[2].code = #child 
* #C47 ^property[2].valueCode = #C47.1 
* #C47 ^property[3].code = #child 
* #C47 ^property[3].valueCode = #C47.2 
* #C47 ^property[4].code = #child 
* #C47 ^property[4].valueCode = #C47.3 
* #C47 ^property[5].code = #child 
* #C47 ^property[5].valueCode = #C47.4 
* #C47 ^property[6].code = #child 
* #C47 ^property[6].valueCode = #C47.5 
* #C47 ^property[7].code = #child 
* #C47 ^property[7].valueCode = #C47.6 
* #C47 ^property[8].code = #child 
* #C47 ^property[8].valueCode = #C47.8 
* #C47 ^property[9].code = #child 
* #C47 ^property[9].valueCode = #C47.9 
* #C47.0 "Periphere Nerven und autonomes Nervensystem des Kopfes, des Gesichtes und des Halses"
* #C47.0 ^designation[0].value = "Periphere Nerven und autonomes Nervensystem von Fossa pterygoidea" 
* #C47.0 ^designation[1].value = "Periphere Nerven und autonomes Nervensystem von Gesicht" 
* #C47.0 ^designation[2].value = "Periphere Nerven und autonomes Nervensystem von Hals" 
* #C47.0 ^designation[3].value = "Periphere Nerven und autonomes Nervensystem von Halsregion" 
* #C47.0 ^designation[4].value = "Periphere Nerven und autonomes Nervensystem von Kinn" 
* #C47.0 ^designation[5].value = "Periphere Nerven und autonomes Nervensystem von Kopf" 
* #C47.0 ^designation[6].value = "Periphere Nerven und autonomes Nervensystem von Kopfschwarte" 
* #C47.0 ^designation[7].value = "Periphere Nerven und autonomes Nervensystem von Schläfe" 
* #C47.0 ^designation[8].value = "Periphere Nerven und autonomes Nervensystem von Stirn" 
* #C47.0 ^designation[9].value = "Periphere Nerven und autonomes Nervensystem von Supraklavikularregion" 
* #C47.0 ^designation[10].value = "Periphere Nerven und autonomes Nervensystem von Wange" 
* #C47.0 ^designation[11].value = "Plexus cervicalis" 
* #C47.0 ^designation[12].value = "Periphere Nerven und autonomes Nervensystem der Orbita" 
* #C47.0 ^property[0].code = #parent 
* #C47.0 ^property[0].valueCode = #C47 
* #C47.1 "Periphere Nerven und autonomes Nervensystem der oberen Extremität und der Schulter"
* #C47.1 ^designation[0].value = "Armnerv" 
* #C47.1 ^designation[1].value = "N. medianus" 
* #C47.1 ^designation[2].value = "N. radialis" 
* #C47.1 ^designation[3].value = "N. ulnaris" 
* #C47.1 ^designation[4].value = "Periphere Nerven und autonomes Nervensystem von Arm" 
* #C47.1 ^designation[5].value = "Periphere Nerven und autonomes Nervensystem von Daumen" 
* #C47.1 ^designation[6].value = "Periphere Nerven und autonomes Nervensystem von Ellbogen" 
* #C47.1 ^designation[7].value = "Periphere Nerven und autonomes Nervensystem von Ellenbeuge" 
* #C47.1 ^designation[8].value = "Periphere Nerven und autonomes Nervensystem von Finger" 
* #C47.1 ^designation[9].value = "Periphere Nerven und autonomes Nervensystem von Hand" 
* #C47.1 ^designation[10].value = "Periphere Nerven und autonomes Nervensystem von Handwurzel" 
* #C47.1 ^designation[11].value = "Periphere Nerven und autonomes Nervensystem von Oberarm" 
* #C47.1 ^designation[12].value = "Periphere Nerven und autonomes Nervensystem von Schulter" 
* #C47.1 ^designation[13].value = "Periphere Nerven und autonomes Nervensystem von Unterarm" 
* #C47.1 ^designation[14].value = "Plexus brachialis" 
* #C47.1 ^property[0].code = #parent 
* #C47.1 ^property[0].valueCode = #C47 
* #C47.2 "Periphere Nerven und autonomes Nervensystem der unteren Extremität und der Hüfte"
* #C47.2 ^designation[0].value = "N. femoralis" 
* #C47.2 ^designation[1].value = "N. ischiadicus" 
* #C47.2 ^designation[2].value = "N. obturatorius" 
* #C47.2 ^designation[3].value = "Periphere Nerven und autonomes Nervensystem von Bein" 
* #C47.2 ^designation[4].value = "Periphere Nerven und autonomes Nervensystem von Ferse" 
* #C47.2 ^designation[5].value = "Periphere Nerven und autonomes Nervensystem von Fuß" 
* #C47.2 ^designation[6].value = "Periphere Nerven und autonomes Nervensystem von Fußknöchel" 
* #C47.2 ^designation[7].value = "Periphere Nerven und autonomes Nervensystem von Hüfte" 
* #C47.2 ^designation[8].value = "Periphere Nerven und autonomes Nervensystem von Knie" 
* #C47.2 ^designation[9].value = "Periphere Nerven und autonomes Nervensystem von Kniekehle" 
* #C47.2 ^designation[10].value = "Periphere Nerven und autonomes Nervensystem von Oberschenkel" 
* #C47.2 ^designation[11].value = "Periphere Nerven und autonomes Nervensystem von Unterschenkel" 
* #C47.2 ^designation[12].value = "Periphere Nerven und autonomes Nervensystem von Wade" 
* #C47.2 ^designation[13].value = "Periphere Nerven und autonomes Nervensystem von Zehe" 
* #C47.2 ^property[0].code = #parent 
* #C47.2 ^property[0].valueCode = #C47 
* #C47.3 "Periphere Nerven und autonomes Nervensystem des Thorax"
* #C47.3 ^designation[0].value = "Interkostalnerven" 
* #C47.3 ^designation[1].value = "Periphere Nerven und autonomes Nervensystem von Axilla" 
* #C47.3 ^designation[2].value = "Periphere Nerven und autonomes Nervensystem von Brustkorb" 
* #C47.3 ^designation[3].value = "Periphere Nerven und autonomes Nervensystem von Brustwand" 
* #C47.3 ^designation[4].value = "Periphere Nerven und autonomes Nervensystem von Infraklavikularregion" 
* #C47.3 ^designation[5].value = "Periphere Nerven und autonomes Nervensystem von Skapularregion" 
* #C47.3 ^designation[6].value = "Periphere Nerven und autonomes Nervensystem von Thoraxwand" 
* #C47.3 ^property[0].code = #parent 
* #C47.3 ^property[0].valueCode = #C47 
* #C47.4 "Periphere Nerven und autonomes Nervensystem des Abdomens"
* #C47.4 ^designation[0].value = "Periphere Nerven und autonomes Nervensystem von Bauchwand" 
* #C47.4 ^designation[1].value = "Periphere Nerven und autonomes Nervensystem von Nabel" 
* #C47.4 ^property[0].code = #parent 
* #C47.4 ^property[0].valueCode = #C47 
* #C47.5 "Periphere Nerven und autonomes Nervensystem des Beckens"
* #C47.5 ^designation[0].value = "N. sacralis" 
* #C47.5 ^designation[1].value = "Periphere Nerven und autonomes Nervensystem von Damm" 
* #C47.5 ^designation[2].value = "Periphere Nerven und autonomes Nervensystem von Gesäß" 
* #C47.5 ^designation[3].value = "Periphere Nerven und autonomes Nervensystem von Glutealregion" 
* #C47.5 ^designation[4].value = "Periphere Nerven und autonomes Nervensystem von Leiste" 
* #C47.5 ^designation[5].value = "Periphere Nerven und autonomes Nervensystem von Leistengegend" 
* #C47.5 ^designation[6].value = "Periphere Nerven und autonomes Nervensystem von Regio sacrococcygea" 
* #C47.5 ^designation[7].value = "Plexus lumbosacralis" 
* #C47.5 ^designation[8].value = "Plexus sacralis" 
* #C47.5 ^designation[9].value = "Sakralplexus" 
* #C47.5 ^property[0].code = #parent 
* #C47.5 ^property[0].valueCode = #C47 
* #C47.6 "Periphere Nerven und autonomes Nervensystem des Stammes"
* #C47.6 ^designation[0].value = "N. lumbalis" 
* #C47.6 ^designation[1].value = "Periphere Nerven und autonomes Nervensystem von Flanke" 
* #C47.6 ^designation[2].value = "Periphere Nerven und autonomes Nervensystem von Rücken" 
* #C47.6 ^designation[3].value = "Periphere Nerven und autonomes Nervensystem von Stamm" 
* #C47.6 ^property[0].code = #parent 
* #C47.6 ^property[0].valueCode = #C47 
* #C47.8 "Periphere Nerven und autonomes Nervensystem, mehrere Teilbereiche überlappend"
* #C47.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C47.8 ^property[0].code = #parent 
* #C47.8 ^property[0].valueCode = #C47 
* #C47.9 "Autonomes Nervensystem o.n.A."
* #C47.9 ^designation[0].value = "Ganglion o.n.A." 
* #C47.9 ^designation[1].value = "Nerv o.n.A." 
* #C47.9 ^designation[2].value = "Parasympathisches Nervensystem o.n.A." 
* #C47.9 ^designation[3].value = "Periphere Nerven o.n.A." 
* #C47.9 ^designation[4].value = "Spinalnerven o.n.A." 
* #C47.9 ^designation[5].value = "Sympathisches Nervensystem o.n.A." 
* #C47.9 ^property[0].code = #parent 
* #C47.9 ^property[0].valueCode = #C47 
* #C48-C48 "Retroperitoneum und Peritoneum"
* #C48-C48 ^property[0].code = #parent 
* #C48-C48 ^property[0].valueCode = #T 
* #C48-C48 ^property[1].code = #child 
* #C48-C48 ^property[1].valueCode = #C48 
* #C48 "Retroperitoneum und Peritoneum"
* #C48 ^property[0].code = #parent 
* #C48 ^property[0].valueCode = #C48-C48 
* #C48 ^property[1].code = #child 
* #C48 ^property[1].valueCode = #C48.0 
* #C48 ^property[2].code = #child 
* #C48 ^property[2].valueCode = #C48.1 
* #C48 ^property[3].code = #child 
* #C48 ^property[3].valueCode = #C48.2 
* #C48 ^property[4].code = #child 
* #C48 ^property[4].valueCode = #C48.8 
* #C48.0 "Retroperitoneum"
* #C48.0 ^designation[0].value = "Periadrenales Gewebe" 
* #C48.0 ^designation[1].value = "Perinephritisches Gewebe" 
* #C48.0 ^designation[2].value = "Peripankreatisches Gewebe" 
* #C48.0 ^designation[3].value = "Perirenales Gewebe" 
* #C48.0 ^designation[4].value = "Retroperitoneales Gewebe" 
* #C48.0 ^designation[5].value = "Retrozäkales Gewebe" 
* #C48.0 ^property[0].code = #parent 
* #C48.0 ^property[0].valueCode = #C48 
* #C48.1 "Näher bezeichnete Teile des Peritoneums"
* #C48.1 ^designation[0].value = "Excavatio rectouterinaDouglas-Raum" 
* #C48.1 ^designation[1].value = "Mesenteriolum der Appendix" 
* #C48.1 ^designation[2].value = "Mesenterium" 
* #C48.1 ^designation[3].value = "Mesokolon" 
* #C48.1 ^designation[4].value = "Omentum" 
* #C48.1 ^property[0].code = #parent 
* #C48.1 ^property[0].valueCode = #C48 
* #C48.2 "Peritoneum o.n.A."
* #C48.2 ^designation[0].value = "Peritonealhöhle" 
* #C48.2 ^property[0].code = #parent 
* #C48.2 ^property[0].valueCode = #C48 
* #C48.8 "Retroperitoneum und Peritoneum, mehrere Teilbereiche überlappend"
* #C48.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C48.8 ^property[0].code = #parent 
* #C48.8 ^property[0].valueCode = #C48 
* #C49-C49 "Bindegewebe, Subkutangewebe und sonstige Weichteile"
* #C49-C49 ^property[0].code = #parent 
* #C49-C49 ^property[0].valueCode = #T 
* #C49-C49 ^property[1].code = #child 
* #C49-C49 ^property[1].valueCode = #C49 
* #C49 "Bindegewebe, Subkutangewebe und sonstige Weichteile"
* #C49 ^designation[0].value = "Aponeurosen" 
* #C49 ^designation[1].value = "Arterien" 
* #C49 ^designation[2].value = "Bänder" 
* #C49 ^designation[3].value = "Bindegewebe" 
* #C49 ^designation[4].value = "Blutgefäße" 
* #C49 ^designation[5].value = "Bursae" 
* #C49 ^designation[6].value = "Faszien" 
* #C49 ^designation[7].value = "Fettgewebe" 
* #C49 ^designation[8].value = "Gefäße" 
* #C49 ^designation[9].value = "Lymphatisch" 
* #C49 ^designation[10].value = "Muskel" 
* #C49 ^designation[11].value = "Schleimbeutel" 
* #C49 ^designation[12].value = "Sehnen" 
* #C49 ^designation[13].value = "Sehnenscheiden" 
* #C49 ^designation[14].value = "Skelettmuskel" 
* #C49 ^designation[15].value = "Subkutangewebe" 
* #C49 ^designation[16].value = "Synovia" 
* #C49 ^designation[17].value = "Venen" 
* #C49 ^property[0].code = #parent 
* #C49 ^property[0].valueCode = #C49-C49 
* #C49 ^property[1].code = #child 
* #C49 ^property[1].valueCode = #C49.0 
* #C49 ^property[2].code = #child 
* #C49 ^property[2].valueCode = #C49.1 
* #C49 ^property[3].code = #child 
* #C49 ^property[3].valueCode = #C49.2 
* #C49 ^property[4].code = #child 
* #C49 ^property[4].valueCode = #C49.3 
* #C49 ^property[5].code = #child 
* #C49 ^property[5].valueCode = #C49.4 
* #C49 ^property[6].code = #child 
* #C49 ^property[6].valueCode = #C49.5 
* #C49 ^property[7].code = #child 
* #C49 ^property[7].valueCode = #C49.6 
* #C49 ^property[8].code = #child 
* #C49 ^property[8].valueCode = #C49.8 
* #C49 ^property[9].code = #child 
* #C49 ^property[9].valueCode = #C49.9 
* #C49.0 "Bindegewebe, Subkutangewebe und sonstige Weichteile des Kopfes, des Gesichtes und des Halses"
* #C49.0 ^designation[0].value = "A. carotis" 
* #C49.0 ^designation[1].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Fossa pterygoidea" 
* #C49.0 ^designation[2].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Gesicht" 
* #C49.0 ^designation[3].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Hals" 
* #C49.0 ^designation[4].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Halsregion" 
* #C49.0 ^designation[5].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Kinn" 
* #C49.0 ^designation[6].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Kopf" 
* #C49.0 ^designation[7].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Kopfschwarte" 
* #C49.0 ^designation[8].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Schläfe" 
* #C49.0 ^designation[9].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Stirn" 
* #C49.0 ^designation[10].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Supraklavikularregion" 
* #C49.0 ^designation[11].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Wange" 
* #C49.0 ^designation[12].value = "M. masseter" 
* #C49.0 ^designation[13].value = "M. sternocleidomastoideus" 
* #C49.0 ^designation[14].value = "OhrknorpelKnorpel des Ohres" 
* #C49.0 ^designation[15].value = "Bindegewebe der Orbita" 
* #C49.0 ^designation[16].value = "Nasenknorpel" 
* #C49.0 ^property[0].code = #parent 
* #C49.0 ^property[0].valueCode = #C49 
* #C49.1 "Bindegewebe, Subkutangewebe und sonstige Weichteile der oberen Extremität und der Schulter"
* #C49.1 ^designation[0].value = "A. radialis" 
* #C49.1 ^designation[1].value = "A. ulnaris" 
* #C49.1 ^designation[2].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Arm" 
* #C49.1 ^designation[3].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Daumen" 
* #C49.1 ^designation[4].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Ellbogen" 
* #C49.1 ^designation[5].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Ellenbeuge" 
* #C49.1 ^designation[6].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Finger" 
* #C49.1 ^designation[7].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Hand" 
* #C49.1 ^designation[8].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Handwurzel" 
* #C49.1 ^designation[9].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Oberarm" 
* #C49.1 ^designation[10].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Schulter" 
* #C49.1 ^designation[11].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Unterarm" 
* #C49.1 ^designation[12].value = "M. biceps brachii" 
* #C49.1 ^designation[13].value = "M. brachialis" 
* #C49.1 ^designation[14].value = "M. coracobrachialis" 
* #C49.1 ^designation[15].value = "M. deltoideus" 
* #C49.1 ^designation[16].value = "M. triceps brachii" 
* #C49.1 ^designation[17].value = "Palmaraponeurose" 
* #C49.1 ^designation[18].value = "Palmarfaszie" 
* #C49.1 ^property[0].code = #parent 
* #C49.1 ^property[0].valueCode = #C49 
* #C49.2 "Bindegewebe, Subkutangewebe und sonstige Weichteile der unteren Extremität und der Hüfte"
* #C49.2 ^designation[0].value = "A. femoralis" 
* #C49.2 ^designation[1].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Bein" 
* #C49.2 ^designation[2].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Ferse" 
* #C49.2 ^designation[3].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Fuß" 
* #C49.2 ^designation[4].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Fußknöchel" 
* #C49.2 ^designation[5].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Hüfte" 
* #C49.2 ^designation[6].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Knie" 
* #C49.2 ^designation[7].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Kniekehle" 
* #C49.2 ^designation[8].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Oberschenkel" 
* #C49.2 ^designation[9].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Unterschenkel" 
* #C49.2 ^designation[10].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Wade" 
* #C49.2 ^designation[11].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Zehe" 
* #C49.2 ^designation[12].value = "M. biceps femoris" 
* #C49.2 ^designation[13].value = "M. gastrocnemius" 
* #C49.2 ^designation[14].value = "M. quadriceps femoris" 
* #C49.2 ^designation[15].value = "Plantaraponeurose" 
* #C49.2 ^designation[16].value = "Plantarfaszie" 
* #C49.2 ^property[0].code = #parent 
* #C49.2 ^property[0].valueCode = #C49 
* #C49.3 "Bindegewebe, Subkutangewebe und sonstige Weichteile des Thorax"
* #C49.3 ^designation[0].value = "A. axillaris" 
* #C49.3 ^designation[1].value = "A. mammaria interna" 
* #C49.3 ^designation[2].value = "A. subclavia" 
* #C49.3 ^designation[3].value = "Aorta o.n.A." 
* #C49.3 ^designation[4].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Axilla" 
* #C49.3 ^designation[5].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Brustkorb" 
* #C49.3 ^designation[6].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Brustwand" 
* #C49.3 ^designation[7].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Infraklavikularregion" 
* #C49.3 ^designation[8].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Skapularregion" 
* #C49.3 ^designation[9].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Thorax" 
* #C49.3 ^designation[10].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Thoraxwand" 
* #C49.3 ^designation[11].value = "Ductus thoracicus" 
* #C49.3 ^designation[12].value = "Interkostalmuskeln" 
* #C49.3 ^designation[13].value = "M. latissimus dorsi" 
* #C49.3 ^designation[14].value = "M. pectoralis major" 
* #C49.3 ^designation[15].value = "M. trapezius" 
* #C49.3 ^designation[16].value = "V. cava superior" 
* #C49.3 ^designation[17].value = "Zwerchfell" 
* #C49.3 ^designation[18].value = "Herz und Mediastinum" 
* #C49.3 ^designation[19].value = "Thymus" 
* #C49.3 ^property[0].code = #parent 
* #C49.3 ^property[0].valueCode = #C49 
* #C49.4 "Bindegewebe, Subkutangewebe und sonstige Weichteilgewebe des Abdomen"
* #C49.4 ^designation[0].value = "A. coeliaca" 
* #C49.4 ^designation[1].value = "A. renalis" 
* #C49.4 ^designation[2].value = "Aorta abdominalis" 
* #C49.4 ^designation[3].value = "Bauchmuskeln" 
* #C49.4 ^designation[4].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Abdomen" 
* #C49.4 ^designation[5].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Bauchwand" 
* #C49.4 ^designation[6].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Nabel" 
* #C49.4 ^designation[7].value = "M. iliopsoas" 
* #C49.4 ^designation[8].value = "M. rectus abdominis" 
* #C49.4 ^designation[9].value = "Mesenterialarterie" 
* #C49.4 ^designation[10].value = "Psoasmuskeln" 
* #C49.4 ^designation[11].value = "V. cava inferior" 
* #C49.4 ^designation[12].value = "V. cava o.n.A." 
* #C49.4 ^designation[13].value = "V. cava, pars abdominalis" 
* #C49.4 ^property[0].code = #parent 
* #C49.4 ^property[0].valueCode = #C49 
* #C49.5 "Bindegewebe, Subkutangewebe und sonstige Weichteile des Beckens"
* #C49.5 ^designation[0].value = "A. iliaca" 
* #C49.5 ^designation[1].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Damm" 
* #C49.5 ^designation[2].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Gesäß" 
* #C49.5 ^designation[3].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Glutealregion" 
* #C49.5 ^designation[4].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Leiste" 
* #C49.5 ^designation[5].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Leistengegend" 
* #C49.5 ^designation[6].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Regio sacrococcygea" 
* #C49.5 ^designation[7].value = "M. gluteus maximus" 
* #C49.5 ^designation[8].value = "V. iliaca" 
* #C49.5 ^property[0].code = #parent 
* #C49.5 ^property[0].valueCode = #C49 
* #C49.6 "Bindegewebe, Subkutangewebe und sonstige Weichteile des Stammes o.n.A."
* #C49.6 ^designation[0].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Flanke" 
* #C49.6 ^designation[1].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Rücken" 
* #C49.6 ^designation[2].value = "Bindegewebe, Subkutangewebe und sonstige Weichteile von Stamm" 
* #C49.6 ^property[0].code = #parent 
* #C49.6 ^property[0].valueCode = #C49 
* #C49.8 "Bindegewebe, Subkutangewebe und sonstige Weichteile, mehrere Bereiche überlappend"
* #C49.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C49.8 ^property[0].code = #parent 
* #C49.8 ^property[0].valueCode = #C49 
* #C49.9 "Bindegewebe, Subkutangewebe und sonstige Weichteile o.n.A."
* #C49.9 ^designation[0].value = "Aponeurose o.n.A." 
* #C49.9 ^designation[1].value = "Arterie o.n.A." 
* #C49.9 ^designation[2].value = "Band o.n.A." 
* #C49.9 ^designation[3].value = "Bindegewebe o.n.A." 
* #C49.9 ^designation[4].value = "Blutgefäß o.n.A." 
* #C49.9 ^designation[5].value = "Faszie o.n.A." 
* #C49.9 ^designation[6].value = "Fettgewebe o.n.A." 
* #C49.9 ^designation[7].value = "Gefäß o.n.A." 
* #C49.9 ^designation[8].value = "Lymphatisch o.n.A." 
* #C49.9 ^designation[9].value = "Muskel o.n.A." 
* #C49.9 ^designation[10].value = "Schleimbeutel o.n.A." 
* #C49.9 ^designation[11].value = "Sehne o.n.A." 
* #C49.9 ^designation[12].value = "Sehnenscheide o.n.A." 
* #C49.9 ^designation[13].value = "Skelettmuskel o.n.A." 
* #C49.9 ^designation[14].value = "Straffes Bindegewe o.n.A." 
* #C49.9 ^designation[15].value = "Subkutangewebe o.n.A." 
* #C49.9 ^designation[16].value = "Synovia o.n.A." 
* #C49.9 ^designation[17].value = "Vene o.n.A." 
* #C49.9 ^property[0].code = #parent 
* #C49.9 ^property[0].valueCode = #C49 
* #C50-C50 "Brust [Mamma]"
* #C50-C50 ^property[0].code = #parent 
* #C50-C50 ^property[0].valueCode = #T 
* #C50-C50 ^property[1].code = #child 
* #C50-C50 ^property[1].valueCode = #C50 
* #C50 "Brust [Mamma]"
* #C50 ^designation[0].value = "Haut der Brust" 
* #C50 ^property[0].code = #parent 
* #C50 ^property[0].valueCode = #C50-C50 
* #C50 ^property[1].code = #child 
* #C50 ^property[1].valueCode = #C50.0 
* #C50 ^property[2].code = #child 
* #C50 ^property[2].valueCode = #C50.1 
* #C50 ^property[3].code = #child 
* #C50 ^property[3].valueCode = #C50.2 
* #C50 ^property[4].code = #child 
* #C50 ^property[4].valueCode = #C50.3 
* #C50 ^property[5].code = #child 
* #C50 ^property[5].valueCode = #C50.4 
* #C50 ^property[6].code = #child 
* #C50 ^property[6].valueCode = #C50.5 
* #C50 ^property[7].code = #child 
* #C50 ^property[7].valueCode = #C50.6 
* #C50 ^property[8].code = #child 
* #C50 ^property[8].valueCode = #C50.8 
* #C50 ^property[9].code = #child 
* #C50 ^property[9].valueCode = #C50.9 
* #C50.0 "Mamille"
* #C50.0 ^designation[0].value = "Areola" 
* #C50.0 ^designation[1].value = "Brustwarze" 
* #C50.0 ^property[0].code = #parent 
* #C50.0 ^property[0].valueCode = #C50 
* #C50.1 "Zentraler Drüsenkörper der Brust"
* #C50.1 ^designation[0].value = "Zentraler Drüsenkörper der Mamma" 
* #C50.1 ^property[0].code = #parent 
* #C50.1 ^property[0].valueCode = #C50 
* #C50.2 "Oberer innerer Quadrant der Brust"
* #C50.2 ^designation[0].value = "Oberer innerer Quadrant der Mamma" 
* #C50.2 ^property[0].code = #parent 
* #C50.2 ^property[0].valueCode = #C50 
* #C50.3 "Unterer innerer Quadrant der Brust"
* #C50.3 ^designation[0].value = "Unterer innerer Quadrant der Mamma" 
* #C50.3 ^property[0].code = #parent 
* #C50.3 ^property[0].valueCode = #C50 
* #C50.4 "Oberer äußerer Quadrant der Brust"
* #C50.4 ^designation[0].value = "Oberer äußerer Quadrant der Mamma" 
* #C50.4 ^property[0].code = #parent 
* #C50.4 ^property[0].valueCode = #C50 
* #C50.5 "Unterer äußerer Quadrant der Brust"
* #C50.5 ^designation[0].value = "Unterer äußerer Quadrant der Mamma" 
* #C50.5 ^property[0].code = #parent 
* #C50.5 ^property[0].valueCode = #C50 
* #C50.6 "Recessus axillaris der Brust"
* #C50.6 ^designation[0].value = "Recessus axillaris der Mamma" 
* #C50.6 ^designation[1].value = "Axillärer Ausläufer der Brust o.n.A." 
* #C50.6 ^property[0].code = #parent 
* #C50.6 ^property[0].valueCode = #C50 
* #C50.8 "Brust, mehrere Teilbereiche überlappend"
* #C50.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C50.8 ^designation[0].value = "Mamma, mehrere Teilbereiche überlappend" 
* #C50.8 ^designation[1].value = "Kaudaler Anteil der Brust" 
* #C50.8 ^designation[2].value = "Kranialer Anteil der Brust" 
* #C50.8 ^designation[3].value = "Lateraler Anteil der Brust" 
* #C50.8 ^designation[4].value = "Medialer Anteil der Brust" 
* #C50.8 ^designation[5].value = "Mittellinie der Brust" 
* #C50.8 ^property[0].code = #parent 
* #C50.8 ^property[0].valueCode = #C50 
* #C50.9 "Brust o.n.A."
* #C50.9 ^designation[0].value = "Brustdrüse" 
* #C50.9 ^designation[1].value = "Mamma o.n.A." 
* #C50.9 ^property[0].code = #parent 
* #C50.9 ^property[0].valueCode = #C50 
* #C51-C58 "Weibliche Geschlechtsorgane"
* #C51-C58 ^property[0].code = #parent 
* #C51-C58 ^property[0].valueCode = #T 
* #C51-C58 ^property[1].code = #child 
* #C51-C58 ^property[1].valueCode = #C51 
* #C51-C58 ^property[2].code = #child 
* #C51-C58 ^property[2].valueCode = #C52 
* #C51-C58 ^property[3].code = #child 
* #C51-C58 ^property[3].valueCode = #C53 
* #C51-C58 ^property[4].code = #child 
* #C51-C58 ^property[4].valueCode = #C54 
* #C51-C58 ^property[5].code = #child 
* #C51-C58 ^property[5].valueCode = #C55 
* #C51-C58 ^property[6].code = #child 
* #C51-C58 ^property[6].valueCode = #C56 
* #C51-C58 ^property[7].code = #child 
* #C51-C58 ^property[7].valueCode = #C57 
* #C51-C58 ^property[8].code = #child 
* #C51-C58 ^property[8].valueCode = #C58 
* #C51 "Vulva"
* #C51 ^property[0].code = #parent 
* #C51 ^property[0].valueCode = #C51-C58 
* #C51 ^property[1].code = #child 
* #C51 ^property[1].valueCode = #C51.0 
* #C51 ^property[2].code = #child 
* #C51 ^property[2].valueCode = #C51.1 
* #C51 ^property[3].code = #child 
* #C51 ^property[3].valueCode = #C51.2 
* #C51 ^property[4].code = #child 
* #C51 ^property[4].valueCode = #C51.8 
* #C51 ^property[5].code = #child 
* #C51 ^property[5].valueCode = #C51.9 
* #C51.0 "Labium majus"
* #C51.0 ^designation[0].value = "Labia majora o.n.A." 
* #C51.0 ^designation[1].value = "Bartholin-Drüse" 
* #C51.0 ^designation[2].value = "Haut an den großen Schamlippen" 
* #C51.0 ^property[0].code = #parent 
* #C51.0 ^property[0].valueCode = #C51 
* #C51.1 "Labium minus"
* #C51.1 ^designation[0].value = "Labia minora" 
* #C51.1 ^property[0].code = #parent 
* #C51.1 ^property[0].valueCode = #C51 
* #C51.2 "Klitoris"
* #C51.2 ^property[0].code = #parent 
* #C51.2 ^property[0].valueCode = #C51 
* #C51.8 "Vulva, mehrere Teilbereiche überlappend"
* #C51.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C51.8 ^property[0].code = #parent 
* #C51.8 ^property[0].valueCode = #C51 
* #C51.9 "Vulva o.n.A."
* #C51.9 ^designation[0].value = "Äußeres weibliches Genitale" 
* #C51.9 ^designation[1].value = "Frenulum labium" 
* #C51.9 ^designation[2].value = "Haut an der Vulva" 
* #C51.9 ^designation[3].value = "Labia o.n.A." 
* #C51.9 ^designation[4].value = "Labiom o.n.A." 
* #C51.9 ^designation[5].value = "Mons pubis" 
* #C51.9 ^designation[6].value = "Mons veneris" 
* #C51.9 ^designation[7].value = "Scham" 
* #C51.9 ^property[0].code = #parent 
* #C51.9 ^property[0].valueCode = #C51 
* #C52 "Vagina"
* #C52 ^property[0].code = #parent 
* #C52 ^property[0].valueCode = #C51-C58 
* #C52 ^property[1].code = #child 
* #C52 ^property[1].valueCode = #C52.9 
* #C52.9 "Vagina o.n.A."
* #C52.9 ^designation[0].value = "Fornix vaginae" 
* #C52.9 ^designation[1].value = "Gartner-Gang" 
* #C52.9 ^designation[2].value = "Hymen" 
* #C52.9 ^designation[3].value = "Scheidengewölbe" 
* #C52.9 ^property[0].code = #parent 
* #C52.9 ^property[0].valueCode = #C52 
* #C53 "Cervix uteri"
* #C53 ^property[0].code = #parent 
* #C53 ^property[0].valueCode = #C51-C58 
* #C53 ^property[1].code = #child 
* #C53 ^property[1].valueCode = #C53.0 
* #C53 ^property[2].code = #child 
* #C53 ^property[2].valueCode = #C53.1 
* #C53 ^property[3].code = #child 
* #C53 ^property[3].valueCode = #C53.8 
* #C53 ^property[4].code = #child 
* #C53 ^property[4].valueCode = #C53.9 
* #C53.0 "Endozervix"
* #C53.0 ^designation[0].value = "Innerer Muttermund" 
* #C53.0 ^designation[1].value = "Ovula Nabothi" 
* #C53.0 ^designation[2].value = "Zervikaldrüsen" 
* #C53.0 ^designation[3].value = "Zervikalkanal" 
* #C53.0 ^designation[4].value = "Zervixhöhle" 
* #C53.0 ^property[0].code = #parent 
* #C53.0 ^property[0].valueCode = #C53 
* #C53.1 "Ektozervix"
* #C53.1 ^designation[0].value = "Äußerer Muttermund" 
* #C53.1 ^property[0].code = #parent 
* #C53.1 ^property[0].valueCode = #C53 
* #C53.8 "Cervix uteri, mehrere Teilbereiche überlappend"
* #C53.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C53.8 ^designation[0].value = "Cervixstumpf" 
* #C53.8 ^designation[1].value = "Zervikoportale Epithelgrenze" 
* #C53.8 ^property[0].code = #parent 
* #C53.8 ^property[0].valueCode = #C53 
* #C53.9 "Cervix uteri"
* #C53.9 ^designation[0].value = "Cervix uteri o.n.A." 
* #C53.9 ^designation[1].value = "Gebärmutterhals" 
* #C53.9 ^property[0].code = #parent 
* #C53.9 ^property[0].valueCode = #C53 
* #C54 "Corpus uteri"
* #C54 ^property[0].code = #parent 
* #C54 ^property[0].valueCode = #C51-C58 
* #C54 ^property[1].code = #child 
* #C54 ^property[1].valueCode = #C54.0 
* #C54 ^property[2].code = #child 
* #C54 ^property[2].valueCode = #C54.1 
* #C54 ^property[3].code = #child 
* #C54 ^property[3].valueCode = #C54.2 
* #C54 ^property[4].code = #child 
* #C54 ^property[4].valueCode = #C54.3 
* #C54 ^property[5].code = #child 
* #C54 ^property[5].valueCode = #C54.8 
* #C54 ^property[6].code = #child 
* #C54 ^property[6].valueCode = #C54.9 
* #C54.0 "Isthmus uteri"
* #C54.0 ^designation[0].value = "Unteres Uterinsegment" 
* #C54.0 ^property[0].code = #parent 
* #C54.0 ^property[0].valueCode = #C54 
* #C54.1 "Endometrium"
* #C54.1 ^designation[0].value = "Glandulae uterinae" 
* #C54.1 ^designation[1].value = "Stroma des Endometriums" 
* #C54.1 ^property[0].code = #parent 
* #C54.1 ^property[0].valueCode = #C54 
* #C54.2 "Myometrium"
* #C54.2 ^property[0].code = #parent 
* #C54.2 ^property[0].valueCode = #C54 
* #C54.3 "Fundus uteri"
* #C54.3 ^property[0].code = #parent 
* #C54.3 ^property[0].valueCode = #C54 
* #C54.8 "Corpus uteri, mehrere Teilbereiche überlappend"
* #C54.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C54.8 ^property[0].code = #parent 
* #C54.8 ^property[0].valueCode = #C54 
* #C54.9 "Corpus uteri"
* #C54.9 ^designation[0].value = "Uteruskörper" 
* #C54.9 ^property[0].code = #parent 
* #C54.9 ^property[0].valueCode = #C54 
* #C55 "Uterus o.n.A."
* #C55 ^property[0].code = #parent 
* #C55 ^property[0].valueCode = #C51-C58 
* #C55 ^property[1].code = #child 
* #C55 ^property[1].valueCode = #C55.9 
* #C55.9 "Uterus o.n.A."
* #C55.9 ^designation[0].value = "Uterin o.n.A." 
* #C55.9 ^property[0].code = #parent 
* #C55.9 ^property[0].valueCode = #C55 
* #C56 "Ovar"
* #C56 ^property[0].code = #parent 
* #C56 ^property[0].valueCode = #C51-C58 
* #C56 ^property[1].code = #child 
* #C56 ^property[1].valueCode = #C56.9 
* #C56.9 "Ovar"
* #C56.9 ^property[0].code = #parent 
* #C56.9 ^property[0].valueCode = #C56 
* #C57 "Sonstige und nicht näher bezeichnete Teile der weiblichen Geschlechtsorgane"
* #C57 ^property[0].code = #parent 
* #C57 ^property[0].valueCode = #C51-C58 
* #C57 ^property[1].code = #child 
* #C57 ^property[1].valueCode = #C57.0 
* #C57 ^property[2].code = #child 
* #C57 ^property[2].valueCode = #C57.1 
* #C57 ^property[3].code = #child 
* #C57 ^property[3].valueCode = #C57.2 
* #C57 ^property[4].code = #child 
* #C57 ^property[4].valueCode = #C57.3 
* #C57 ^property[5].code = #child 
* #C57 ^property[5].valueCode = #C57.4 
* #C57 ^property[6].code = #child 
* #C57 ^property[6].valueCode = #C57.7 
* #C57 ^property[7].code = #child 
* #C57 ^property[7].valueCode = #C57.8 
* #C57 ^property[8].code = #child 
* #C57 ^property[8].valueCode = #C57.9 
* #C57.0 "Eileiter"
* #C57.0 ^designation[0].value = "Tubae uterinae" 
* #C57.0 ^property[0].code = #parent 
* #C57.0 ^property[0].valueCode = #C57 
* #C57.1 "Ligamentum latum uteri"
* #C57.1 ^designation[0].value = "Mesovarium" 
* #C57.1 ^designation[1].value = "Parovarialregion" 
* #C57.1 ^property[0].code = #parent 
* #C57.1 ^property[0].valueCode = #C57 
* #C57.2 "Ligamentum rotundum"
* #C57.2 ^property[0].code = #parent 
* #C57.2 ^property[0].valueCode = #C57 
* #C57.3 "Parametrium"
* #C57.3 ^designation[0].value = "Ligamentum cardinale" 
* #C57.3 ^designation[1].value = "Uterusband o.n.A." 
* #C57.3 ^property[0].code = #parent 
* #C57.3 ^property[0].valueCode = #C57 
* #C57.4 "Weibliche Adnexe"
* #C57.4 ^designation[0].value = "Adnexe o.n.A." 
* #C57.4 ^property[0].code = #parent 
* #C57.4 ^property[0].valueCode = #C57 
* #C57.7 "Sonstige näher bezeichnete Teile der weiblichen Geschlechtsorgane"
* #C57.7 ^designation[0].value = "Mesonephros" 
* #C57.7 ^designation[1].value = "Wolff-Gang" 
* #C57.7 ^property[0].code = #parent 
* #C57.7 ^property[0].valueCode = #C57 
* #C57.8 "Weibliche Geschlechtsorgane, mehrere Bereiche überlappend"
* #C57.8 ^designation[0].value = "Tube und Ovar" 
* #C57.8 ^designation[1].value = "Uterus und Ovar" 
* #C57.8 ^property[0].code = #parent 
* #C57.8 ^property[0].valueCode = #C57 
* #C57.9 "Weibliche Geschlechtsorgane o.n.A."
* #C57.9 ^designation[0].value = "Weibliches Genitale o.n.A." 
* #C57.9 ^designation[1].value = "Septum urethrovaginale" 
* #C57.9 ^designation[2].value = "Septum vesicovaginale" 
* #C57.9 ^designation[3].value = "Vesikozervikales Gewebe" 
* #C57.9 ^designation[4].value = "Weiblicher Urogenitaltrakt o.n.A." 
* #C57.9 ^property[0].code = #parent 
* #C57.9 ^property[0].valueCode = #C57 
* #C58 "Plazenta"
* #C58 ^property[0].code = #parent 
* #C58 ^property[0].valueCode = #C51-C58 
* #C58 ^property[1].code = #child 
* #C58 ^property[1].valueCode = #C58.9 
* #C58.9 "Plazenta"
* #C58.9 ^designation[0].value = "Eihäute" 
* #C58.9 ^property[0].code = #parent 
* #C58.9 ^property[0].valueCode = #C58 
* #C60-C63 "Männliche Geschlechtsorgane"
* #C60-C63 ^property[0].code = #parent 
* #C60-C63 ^property[0].valueCode = #T 
* #C60-C63 ^property[1].code = #child 
* #C60-C63 ^property[1].valueCode = #C60 
* #C60-C63 ^property[2].code = #child 
* #C60-C63 ^property[2].valueCode = #C61 
* #C60-C63 ^property[3].code = #child 
* #C60-C63 ^property[3].valueCode = #C62 
* #C60-C63 ^property[4].code = #child 
* #C60-C63 ^property[4].valueCode = #C63 
* #C60 "Penis"
* #C60 ^property[0].code = #parent 
* #C60 ^property[0].valueCode = #C60-C63 
* #C60 ^property[1].code = #child 
* #C60 ^property[1].valueCode = #C60.0 
* #C60 ^property[2].code = #child 
* #C60 ^property[2].valueCode = #C60.1 
* #C60 ^property[3].code = #child 
* #C60 ^property[3].valueCode = #C60.2 
* #C60 ^property[4].code = #child 
* #C60 ^property[4].valueCode = #C60.8 
* #C60 ^property[5].code = #child 
* #C60 ^property[5].valueCode = #C60.9 
* #C60.0 "Präputium"
* #C60.0 ^designation[0].value = "Vorhaut" 
* #C60.0 ^property[0].code = #parent 
* #C60.0 ^property[0].valueCode = #C60 
* #C60.1 "Glans penis"
* #C60.1 ^property[0].code = #parent 
* #C60.1 ^property[0].valueCode = #C60 
* #C60.2 "Penisschaft"
* #C60.2 ^designation[0].value = "Corpus cavernosum" 
* #C60.2 ^designation[1].value = "Schwellkörper des Penis" 
* #C60.2 ^property[0].code = #parent 
* #C60.2 ^property[0].valueCode = #C60 
* #C60.8 "Penis, mehrere Teilbereiche überlappend"
* #C60.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C60.8 ^property[0].code = #parent 
* #C60.8 ^property[0].valueCode = #C60 
* #C60.9 "Penis o.n.A."
* #C60.9 ^property[0].code = #parent 
* #C60.9 ^property[0].valueCode = #C60 
* #C61 "Prostata"
* #C61 ^property[0].code = #parent 
* #C61 ^property[0].valueCode = #C60-C63 
* #C61 ^property[1].code = #child 
* #C61 ^property[1].valueCode = #C61.9 
* #C61.9 "Prostata"
* #C61.9 ^designation[0].value = "Prostata o.n.A." 
* #C61.9 ^property[0].code = #parent 
* #C61.9 ^property[0].valueCode = #C61 
* #C62 "Testis"
* #C62 ^property[0].code = #parent 
* #C62 ^property[0].valueCode = #C60-C63 
* #C62 ^property[1].code = #child 
* #C62 ^property[1].valueCode = #C62.0 
* #C62 ^property[2].code = #child 
* #C62 ^property[2].valueCode = #C62.1 
* #C62 ^property[3].code = #child 
* #C62 ^property[3].valueCode = #C62.9 
* #C62.0 "Kryptorchider Hoden"
* #C62.0 ^designation[0].value = "Dystoper Hoden" 
* #C62.0 ^designation[1].value = "Hodenhochstand" 
* #C62.0 ^property[0].code = #parent 
* #C62.0 ^property[0].valueCode = #C62 
* #C62.1 "Deszendierter Hoden"
* #C62.1 ^designation[0].value = "Hoden im Skrotum" 
* #C62.1 ^property[0].code = #parent 
* #C62.1 ^property[0].valueCode = #C62 
* #C62.9 "Testis o.n.A."
* #C62.9 ^designation[0].value = "Hoden o.n.A." 
* #C62.9 ^property[0].code = #parent 
* #C62.9 ^property[0].valueCode = #C62 
* #C63 "Sonstige und nicht näher bezeichnete Teile der männlichen Geschlechtsorgane"
* #C63 ^property[0].code = #parent 
* #C63 ^property[0].valueCode = #C60-C63 
* #C63 ^property[1].code = #child 
* #C63 ^property[1].valueCode = #C63.0 
* #C63 ^property[2].code = #child 
* #C63 ^property[2].valueCode = #C63.1 
* #C63 ^property[3].code = #child 
* #C63 ^property[3].valueCode = #C63.2 
* #C63 ^property[4].code = #child 
* #C63 ^property[4].valueCode = #C63.7 
* #C63 ^property[5].code = #child 
* #C63 ^property[5].valueCode = #C63.8 
* #C63 ^property[6].code = #child 
* #C63 ^property[6].valueCode = #C63.9 
* #C63.0 "Nebenhoden"
* #C63.0 ^property[0].code = #parent 
* #C63.0 ^property[0].valueCode = #C63 
* #C63.1 "Samenstrang"
* #C63.1 ^designation[0].value = "Ductus deferens" 
* #C63.1 ^property[0].code = #parent 
* #C63.1 ^property[0].valueCode = #C63 
* #C63.2 "Skrotum o.n.A."
* #C63.2 ^designation[0].value = "Skrotalhaut" 
* #C63.2 ^property[0].code = #parent 
* #C63.2 ^property[0].valueCode = #C63 
* #C63.7 "Sonstige näher bezeichnete Teile der männlichen Geschlechtsorgane"
* #C63.7 ^designation[0].value = "Tunica vaginalis" 
* #C63.7 ^designation[1].value = "Vesicula seminalis" 
* #C63.7 ^property[0].code = #parent 
* #C63.7 ^property[0].valueCode = #C63 
* #C63.8 "Männliche Geschlechtsorgane, mehrere Bereiche überlappend"
* #C63.8 ^property[0].code = #parent 
* #C63.8 ^property[0].valueCode = #C63 
* #C63.9 "Männliche Geschlechtsorgane o.n.A."
* #C63.9 ^designation[0].value = "Männliches Genitale" 
* #C63.9 ^designation[1].value = "Männlicher Urogenitaltrakt" 
* #C63.9 ^property[0].code = #parent 
* #C63.9 ^property[0].valueCode = #C63 
* #C64-C68 "Harntrakt"
* #C64-C68 ^property[0].code = #parent 
* #C64-C68 ^property[0].valueCode = #T 
* #C64-C68 ^property[1].code = #child 
* #C64-C68 ^property[1].valueCode = #C64 
* #C64-C68 ^property[2].code = #child 
* #C64-C68 ^property[2].valueCode = #C65 
* #C64-C68 ^property[3].code = #child 
* #C64-C68 ^property[3].valueCode = #C66 
* #C64-C68 ^property[4].code = #child 
* #C64-C68 ^property[4].valueCode = #C67 
* #C64-C68 ^property[5].code = #child 
* #C64-C68 ^property[5].valueCode = #C68 
* #C64 "Niere"
* #C64 ^property[0].code = #parent 
* #C64 ^property[0].valueCode = #C64-C68 
* #C64 ^property[1].code = #child 
* #C64 ^property[1].valueCode = #C64.9 
* #C64.9 "Niere o.n.A."
* #C64.9 ^designation[0].value = "Nierenparenchym" 
* #C64.9 ^designation[1].value = "Renal o.n.A." 
* #C64.9 ^property[0].code = #parent 
* #C64.9 ^property[0].valueCode = #C64 
* #C65 "Nierenbecken"
* #C65 ^property[0].code = #parent 
* #C65 ^property[0].valueCode = #C64-C68 
* #C65 ^property[1].code = #child 
* #C65 ^property[1].valueCode = #C65.9 
* #C65.9 "Nierenbecken"
* #C65.9 ^designation[0].value = "Pelvis renalis" 
* #C65.9 ^designation[1].value = "Nierenkelch" 
* #C65.9 ^designation[2].value = "Nierenkelche" 
* #C65.9 ^designation[3].value = "Ureterabgang" 
* #C65.9 ^property[0].code = #parent 
* #C65.9 ^property[0].valueCode = #C65 
* #C66 "Ureter"
* #C66 ^property[0].code = #parent 
* #C66 ^property[0].valueCode = #C64-C68 
* #C66 ^property[1].code = #child 
* #C66 ^property[1].valueCode = #C66.9 
* #C66.9 "Ureter"
* #C66.9 ^property[0].code = #parent 
* #C66.9 ^property[0].valueCode = #C66 
* #C67 "Harnblase"
* #C67 ^property[0].code = #parent 
* #C67 ^property[0].valueCode = #C64-C68 
* #C67 ^property[1].code = #child 
* #C67 ^property[1].valueCode = #C67.0 
* #C67 ^property[2].code = #child 
* #C67 ^property[2].valueCode = #C67.1 
* #C67 ^property[3].code = #child 
* #C67 ^property[3].valueCode = #C67.2 
* #C67 ^property[4].code = #child 
* #C67 ^property[4].valueCode = #C67.3 
* #C67 ^property[5].code = #child 
* #C67 ^property[5].valueCode = #C67.4 
* #C67 ^property[6].code = #child 
* #C67 ^property[6].valueCode = #C67.5 
* #C67 ^property[7].code = #child 
* #C67 ^property[7].valueCode = #C67.6 
* #C67 ^property[8].code = #child 
* #C67 ^property[8].valueCode = #C67.7 
* #C67 ^property[9].code = #child 
* #C67 ^property[9].valueCode = #C67.8 
* #C67 ^property[10].code = #child 
* #C67 ^property[10].valueCode = #C67.9 
* #C67.0 "Trigonum vesicae"
* #C67.0 ^property[0].code = #parent 
* #C67.0 ^property[0].valueCode = #C67 
* #C67.1 "Blasendach"
* #C67.1 ^property[0].code = #parent 
* #C67.1 ^property[0].valueCode = #C67 
* #C67.2 "Laterale Harnblasenwand"
* #C67.2 ^property[0].code = #parent 
* #C67.2 ^property[0].valueCode = #C67 
* #C67.3 "Vordere Harnblasenwand"
* #C67.3 ^property[0].code = #parent 
* #C67.3 ^property[0].valueCode = #C67 
* #C67.4 "Hintere Harnblasenwand"
* #C67.4 ^property[0].code = #parent 
* #C67.4 ^property[0].valueCode = #C67 
* #C67.5 "Harnblasenhals"
* #C67.5 ^designation[0].value = "Harnröhrenöffnung" 
* #C67.5 ^property[0].code = #parent 
* #C67.5 ^property[0].valueCode = #C67 
* #C67.6 "Ostium ureteris"
* #C67.6 ^property[0].code = #parent 
* #C67.6 ^property[0].valueCode = #C67 
* #C67.7 "Urachus"
* #C67.7 ^property[0].code = #parent 
* #C67.7 ^property[0].valueCode = #C67 
* #C67.8 "Harnblase, mehrere Teilbereiche überlappend"
* #C67.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C67.8 ^property[0].code = #parent 
* #C67.8 ^property[0].valueCode = #C67 
* #C67.9 "Blase o.n.A."
* #C67.9 ^designation[0].value = "Blasenwand o.n.A." 
* #C67.9 ^designation[1].value = "Harnblase o.n.A." 
* #C67.9 ^property[0].code = #parent 
* #C67.9 ^property[0].valueCode = #C67 
* #C68 "Sonstige und nicht näher bezeichnete Teile des Harntraktes"
* #C68 ^property[0].code = #parent 
* #C68 ^property[0].valueCode = #C64-C68 
* #C68 ^property[1].code = #child 
* #C68 ^property[1].valueCode = #C68.0 
* #C68 ^property[2].code = #child 
* #C68 ^property[2].valueCode = #C68.1 
* #C68 ^property[3].code = #child 
* #C68 ^property[3].valueCode = #C68.8 
* #C68 ^property[4].code = #child 
* #C68 ^property[4].valueCode = #C68.9 
* #C68.0 "Urethra"
* #C68.0 ^designation[0].value = "Cowper-Drüsen" 
* #C68.0 ^designation[1].value = "Glandula urethralis" 
* #C68.0 ^designation[2].value = "Utriculus prostaticus" 
* #C68.0 ^property[0].code = #parent 
* #C68.0 ^property[0].valueCode = #C68 
* #C68.1 "Paraurethrale Drüse"
* #C68.1 ^property[0].code = #parent 
* #C68.1 ^property[0].valueCode = #C68 
* #C68.8 "Harntrakt, mehrere Bereiche überlappend"
* #C68.8 ^property[0].code = #parent 
* #C68.8 ^property[0].valueCode = #C68 
* #C68.9 "Harntrakt o.n.A."
* #C68.9 ^property[0].code = #parent 
* #C68.9 ^property[0].valueCode = #C68 
* #C69-C72 "Auge, Gehirn und sonstige Teile des Zentralnervensystems"
* #C69-C72 ^property[0].code = #parent 
* #C69-C72 ^property[0].valueCode = #T 
* #C69-C72 ^property[1].code = #child 
* #C69-C72 ^property[1].valueCode = #C69 
* #C69-C72 ^property[2].code = #child 
* #C69-C72 ^property[2].valueCode = #C70 
* #C69-C72 ^property[3].code = #child 
* #C69-C72 ^property[3].valueCode = #C71 
* #C69-C72 ^property[4].code = #child 
* #C69-C72 ^property[4].valueCode = #C72 
* #C69 "Auge und Augenanhangsgebilde"
* #C69 ^property[0].code = #parent 
* #C69 ^property[0].valueCode = #C69-C72 
* #C69 ^property[1].code = #child 
* #C69 ^property[1].valueCode = #C69.0 
* #C69 ^property[2].code = #child 
* #C69 ^property[2].valueCode = #C69.1 
* #C69 ^property[3].code = #child 
* #C69 ^property[3].valueCode = #C69.2 
* #C69 ^property[4].code = #child 
* #C69 ^property[4].valueCode = #C69.3 
* #C69 ^property[5].code = #child 
* #C69 ^property[5].valueCode = #C69.4 
* #C69 ^property[6].code = #child 
* #C69 ^property[6].valueCode = #C69.5 
* #C69 ^property[7].code = #child 
* #C69 ^property[7].valueCode = #C69.6 
* #C69 ^property[8].code = #child 
* #C69 ^property[8].valueCode = #C69.8 
* #C69 ^property[9].code = #child 
* #C69 ^property[9].valueCode = #C69.9 
* #C69.0 "Konjunktiva"
* #C69.0 ^designation[0].value = "Bindehaut" 
* #C69.0 ^property[0].code = #parent 
* #C69.0 ^property[0].valueCode = #C69 
* #C69.1 "Kornea o.n.A."
* #C69.1 ^designation[0].value = "Limbus corneae" 
* #C69.1 ^property[0].code = #parent 
* #C69.1 ^property[0].valueCode = #C69 
* #C69.2 "Retina"
* #C69.2 ^designation[0].value = "Netzhaut" 
* #C69.2 ^property[0].code = #parent 
* #C69.2 ^property[0].valueCode = #C69 
* #C69.3 "Chorioidea"
* #C69.3 ^property[0].code = #parent 
* #C69.3 ^property[0].valueCode = #C69 
* #C69.4 "Bulbus oculi"
* #C69.4 ^designation[0].value = "Augapfel" 
* #C69.4 ^designation[1].value = "Augenlinse" 
* #C69.4 ^designation[2].value = "Intraokulär" 
* #C69.4 ^designation[3].value = "Iris" 
* #C69.4 ^designation[4].value = "Sklera" 
* #C69.4 ^designation[5].value = "Uvea" 
* #C69.4 ^designation[6].value = "Ziliarkörper" 
* #C69.4 ^property[0].code = #parent 
* #C69.4 ^property[0].valueCode = #C69 
* #C69.5 "Tränendrüse"
* #C69.5 ^designation[0].value = "Ductus lacrimalis o.n.A.Ductus nasolacrimalis" 
* #C69.5 ^designation[1].value = "Tränen-Nasen-Gang" 
* #C69.5 ^designation[2].value = "Tränensack" 
* #C69.5 ^property[0].code = #parent 
* #C69.5 ^property[0].valueCode = #C69 
* #C69.6 "Orbita o.n.A."
* #C69.6 ^designation[0].value = "Autonomes Nervensystem der Orbita" 
* #C69.6 ^designation[1].value = "Äußere Augenmuskeln" 
* #C69.6 ^designation[2].value = "Bindegewebe der Orbita" 
* #C69.6 ^designation[3].value = "Periphere Nerven der Orbita" 
* #C69.6 ^designation[4].value = "Retrobulbäres Gewebe" 
* #C69.6 ^designation[5].value = "Weichteilgewebe der Orbita" 
* #C69.6 ^property[0].code = #parent 
* #C69.6 ^property[0].valueCode = #C69 
* #C69.8 "Auge und Augenanhangsgebilde, mehrere Teilbereiche überlappend"
* #C69.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C69.8 ^property[0].code = #parent 
* #C69.8 ^property[0].valueCode = #C69 
* #C69.9 "Auge o.n.A."
* #C69.9 ^property[0].code = #parent 
* #C69.9 ^property[0].valueCode = #C69 
* #C70 "Meningen"
* #C70 ^property[0].code = #parent 
* #C70 ^property[0].valueCode = #C69-C72 
* #C70 ^property[1].code = #child 
* #C70 ^property[1].valueCode = #C70.0 
* #C70 ^property[2].code = #child 
* #C70 ^property[2].valueCode = #C70.1 
* #C70 ^property[3].code = #child 
* #C70 ^property[3].valueCode = #C70.9 
* #C70.0 "Hirnhäute"
* #C70.0 ^designation[0].value = "Arachnoidea encephali" 
* #C70.0 ^designation[1].value = "Dura mater encephali" 
* #C70.0 ^designation[2].value = "Falx cerebelli" 
* #C70.0 ^designation[3].value = "Falx cerebri" 
* #C70.0 ^designation[4].value = "Falx o.n.A." 
* #C70.0 ^designation[5].value = "Pia mater encephali" 
* #C70.0 ^designation[6].value = "Tentorium cerebelliTentorium o.n.A." 
* #C70.0 ^property[0].code = #parent 
* #C70.0 ^property[0].valueCode = #C70 
* #C70.1 "Rückenmarkhäute"
* #C70.1 ^designation[0].value = "Arachnoidea spinalis" 
* #C70.1 ^designation[1].value = "Dura mater spinalis" 
* #C70.1 ^designation[2].value = "Pia mater spinalis" 
* #C70.1 ^property[0].code = #parent 
* #C70.1 ^property[0].valueCode = #C70 
* #C70.9 "Meningen o.n.A."
* #C70.9 ^designation[0].value = "Arachnoidea o.n.A." 
* #C70.9 ^designation[1].value = "Dura mater o.n.A." 
* #C70.9 ^designation[2].value = "Dura o.n.A." 
* #C70.9 ^designation[3].value = "Pia mater o.n.A." 
* #C70.9 ^property[0].code = #parent 
* #C70.9 ^property[0].valueCode = #C70 
* #C71 "Gehirn"
* #C71 ^property[0].code = #parent 
* #C71 ^property[0].valueCode = #C69-C72 
* #C71 ^property[1].code = #child 
* #C71 ^property[1].valueCode = #C71.0 
* #C71 ^property[2].code = #child 
* #C71 ^property[2].valueCode = #C71.1 
* #C71 ^property[3].code = #child 
* #C71 ^property[3].valueCode = #C71.2 
* #C71 ^property[4].code = #child 
* #C71 ^property[4].valueCode = #C71.3 
* #C71 ^property[5].code = #child 
* #C71 ^property[5].valueCode = #C71.4 
* #C71 ^property[6].code = #child 
* #C71 ^property[6].valueCode = #C71.5 
* #C71 ^property[7].code = #child 
* #C71 ^property[7].valueCode = #C71.6 
* #C71 ^property[8].code = #child 
* #C71 ^property[8].valueCode = #C71.7 
* #C71 ^property[9].code = #child 
* #C71 ^property[9].valueCode = #C71.8 
* #C71 ^property[10].code = #child 
* #C71 ^property[10].valueCode = #C71.9 
* #C71.0 "Cerebrum"
* #C71.0 ^designation[0].value = "Basalganglien" 
* #C71.0 ^designation[1].value = "Capsula interna" 
* #C71.0 ^designation[2].value = "Corpus striatum" 
* #C71.0 ^designation[3].value = "Cortex cerebri" 
* #C71.0 ^designation[4].value = "Gehirn, supratentoriell o.n.A." 
* #C71.0 ^designation[5].value = "Globus pallidusPallidum" 
* #C71.0 ^designation[6].value = "Großhirn" 
* #C71.0 ^designation[7].value = "Großhirnhemisphäre" 
* #C71.0 ^designation[8].value = "Hypothalamus" 
* #C71.0 ^designation[9].value = "Insel" 
* #C71.0 ^designation[10].value = "Operculum" 
* #C71.0 ^designation[11].value = "Pallium" 
* #C71.0 ^designation[12].value = "Putamen" 
* #C71.0 ^designation[13].value = "Reil-Insel" 
* #C71.0 ^designation[14].value = "Rhinencephalon" 
* #C71.0 ^designation[15].value = "Thalamus" 
* #C71.0 ^designation[16].value = "Weiße Substanz des Großhirns" 
* #C71.0 ^designation[17].value = "Zentrale weiße Substanz" 
* #C71.0 ^property[0].code = #parent 
* #C71.0 ^property[0].valueCode = #C71 
* #C71.1 "Frontallappen"
* #C71.1 ^designation[0].value = "Polus frontalis" 
* #C71.1 ^property[0].code = #parent 
* #C71.1 ^property[0].valueCode = #C71 
* #C71.2 "Temporallappen"
* #C71.2 ^designation[0].value = "Hippokampus" 
* #C71.2 ^designation[1].value = "Unkus" 
* #C71.2 ^property[0].code = #parent 
* #C71.2 ^property[0].valueCode = #C71 
* #C71.3 "Parietallappen"
* #C71.3 ^property[0].code = #parent 
* #C71.3 ^property[0].valueCode = #C71 
* #C71.4 "Okzipitallappen"
* #C71.4 ^designation[0].value = "Polus occipitalis" 
* #C71.4 ^property[0].code = #parent 
* #C71.4 ^property[0].valueCode = #C71 
* #C71.5 "Ventrikel o.n.A."
* #C71.5 ^designation[0].value = "Dritter Ventrikel o.n.A." 
* #C71.5 ^designation[1].value = "Ependym" 
* #C71.5 ^designation[2].value = "Hirnventrikel" 
* #C71.5 ^designation[3].value = "Plexus chorioideus, dritter Ventrikel" 
* #C71.5 ^designation[4].value = "Plexus chorioideus o.n.A." 
* #C71.5 ^designation[5].value = "Plexus chorioideus, Seitenventrikel" 
* #C71.5 ^designation[6].value = "Seitenventrikel o.n.A." 
* #C71.5 ^property[0].code = #parent 
* #C71.5 ^property[0].valueCode = #C71 
* #C71.6 "Kleinhirn o.n.A."
* #C71.6 ^designation[0].value = "Cerebellum" 
* #C71.6 ^designation[1].value = "Kleinhirnbrückenwinkel" 
* #C71.6 ^designation[2].value = "Wurm (Kleinhirn)" 
* #C71.6 ^property[0].code = #parent 
* #C71.6 ^property[0].valueCode = #C71 
* #C71.7 "Hirnstamm"
* #C71.7 ^designation[0].value = "Infratentorielle Hirnanteile o.n.A." 
* #C71.7 ^designation[1].value = "Medulla oblongata" 
* #C71.7 ^designation[2].value = "Mittelhirn" 
* #C71.7 ^designation[3].value = "Olive" 
* #C71.7 ^designation[4].value = "Pedunculus cerbriAmmonshorn" 
* #C71.7 ^designation[5].value = "Plexus chorioideus, vierter Ventrikel" 
* #C71.7 ^designation[6].value = "Pons" 
* #C71.7 ^designation[7].value = "Pyramide" 
* #C71.7 ^designation[8].value = "Vierter Ventrikel o.n.A." 
* #C71.7 ^property[0].code = #parent 
* #C71.7 ^property[0].valueCode = #C71 
* #C71.8 "Gehirn, mehrere Teilbereiche überlappend"
* #C71.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C71.8 ^designation[0].value = "Corpus callosum" 
* #C71.8 ^designation[1].value = "Tapetum" 
* #C71.8 ^property[0].code = #parent 
* #C71.8 ^property[0].valueCode = #C71 
* #C71.9 "Gehirn o.n.A."
* #C71.9 ^designation[0].value = "Hintere Schädelgrube" 
* #C71.9 ^designation[1].value = "Intrakranieller Sitz" 
* #C71.9 ^designation[2].value = "Mittlere Schädelgrube" 
* #C71.9 ^designation[3].value = "Schädelgrube o.n.A." 
* #C71.9 ^designation[4].value = "Suprasellär" 
* #C71.9 ^designation[5].value = "Vordere Schädelgrube" 
* #C71.9 ^property[0].code = #parent 
* #C71.9 ^property[0].valueCode = #C71 
* #C72 "Rückenmark, Hirnnerven und sonstige Teile des Zentralnervensystems"
* #C72 ^designation[0].value = "Periphere Nerven und autonomes Nervensystem [sympatisch, parasympatisch und Ganglien]" 
* #C72 ^property[0].code = #parent 
* #C72 ^property[0].valueCode = #C69-C72 
* #C72 ^property[1].code = #child 
* #C72 ^property[1].valueCode = #C72.0 
* #C72 ^property[2].code = #child 
* #C72 ^property[2].valueCode = #C72.1 
* #C72 ^property[3].code = #child 
* #C72 ^property[3].valueCode = #C72.2 
* #C72 ^property[4].code = #child 
* #C72 ^property[4].valueCode = #C72.3 
* #C72 ^property[5].code = #child 
* #C72 ^property[5].valueCode = #C72.4 
* #C72 ^property[6].code = #child 
* #C72 ^property[6].valueCode = #C72.5 
* #C72 ^property[7].code = #child 
* #C72 ^property[7].valueCode = #C72.8 
* #C72 ^property[8].code = #child 
* #C72 ^property[8].valueCode = #C72.9 
* #C72.0 "Rückenmark"
* #C72.0 ^designation[0].value = "Conus medullaris" 
* #C72.0 ^designation[1].value = "Filum terminale" 
* #C72.0 ^designation[2].value = "Lumbalmark" 
* #C72.0 ^designation[3].value = "Sakralmark" 
* #C72.0 ^designation[4].value = "Thorakalmark" 
* #C72.0 ^designation[5].value = "Zervikalmark" 
* #C72.0 ^property[0].code = #parent 
* #C72.0 ^property[0].valueCode = #C72 
* #C72.1 "Cauda equina"
* #C72.1 ^property[0].code = #parent 
* #C72.1 ^property[0].valueCode = #C72 
* #C72.2 "N. olfactorius"
* #C72.2 ^property[0].code = #parent 
* #C72.2 ^property[0].valueCode = #C72 
* #C72.3 "N. opticus"
* #C72.3 ^designation[0].value = "Chiasma opticum" 
* #C72.3 ^designation[1].value = "Tractus opticus" 
* #C72.3 ^property[0].code = #parent 
* #C72.3 ^property[0].valueCode = #C72 
* #C72.4 "N. acusticus"
* #C72.4 ^property[0].code = #parent 
* #C72.4 ^property[0].valueCode = #C72 
* #C72.5 "Hirnnerven o.n.A."
* #C72.5 ^designation[0].value = "N. abducens" 
* #C72.5 ^designation[1].value = "N. accessorius o.n.A." 
* #C72.5 ^designation[2].value = "N. facialis" 
* #C72.5 ^designation[3].value = "N. glossopharyngeus" 
* #C72.5 ^designation[4].value = "N. hypoglossus" 
* #C72.5 ^designation[5].value = "N. oculomotorius" 
* #C72.5 ^designation[6].value = "N. trigeminus" 
* #C72.5 ^designation[7].value = "N. trochlearis" 
* #C72.5 ^designation[8].value = "N. vagus" 
* #C72.5 ^property[0].code = #parent 
* #C72.5 ^property[0].valueCode = #C72 
* #C72.8 "Gehirn und andere Teile des Zentralnervensystems, mehrere Teilbereiche überlappend"
* #C72.8 ^property[0].code = #parent 
* #C72.8 ^property[0].valueCode = #C72 
* #C72.9 "Nervensystem"
* #C72.9 ^designation[0].value = "Epidural" 
* #C72.9 ^designation[1].value = "Extradural" 
* #C72.9 ^designation[2].value = "Parasellär" 
* #C72.9 ^designation[3].value = "Zentralnervensystem" 
* #C72.9 ^property[0].code = #parent 
* #C72.9 ^property[0].valueCode = #C72 
* #C73-C75 "Schilddrüse und sonstige endokrine Drüsen"
* #C73-C75 ^property[0].code = #parent 
* #C73-C75 ^property[0].valueCode = #T 
* #C73-C75 ^property[1].code = #child 
* #C73-C75 ^property[1].valueCode = #C73 
* #C73-C75 ^property[2].code = #child 
* #C73-C75 ^property[2].valueCode = #C74 
* #C73-C75 ^property[3].code = #child 
* #C73-C75 ^property[3].valueCode = #C75 
* #C73 "Schilddrüse"
* #C73 ^property[0].code = #parent 
* #C73 ^property[0].valueCode = #C73-C75 
* #C73 ^property[1].code = #child 
* #C73 ^property[1].valueCode = #C73.9 
* #C73.9 "Schilddrüse"
* #C73.9 ^designation[0].value = "Schilddrüse o.n.A." 
* #C73.9 ^designation[1].value = "Ductus thyreoglossus" 
* #C73.9 ^property[0].code = #parent 
* #C73.9 ^property[0].valueCode = #C73 
* #C74 "Nebenniere"
* #C74 ^property[0].code = #parent 
* #C74 ^property[0].valueCode = #C73-C75 
* #C74 ^property[1].code = #child 
* #C74 ^property[1].valueCode = #C74.0 
* #C74 ^property[2].code = #child 
* #C74 ^property[2].valueCode = #C74.1 
* #C74 ^property[3].code = #child 
* #C74 ^property[3].valueCode = #C74.9 
* #C74.0 "Nebennierenrinde"
* #C74.0 ^property[0].code = #parent 
* #C74.0 ^property[0].valueCode = #C74 
* #C74.1 "Nebennierenmark"
* #C74.1 ^property[0].code = #parent 
* #C74.1 ^property[0].valueCode = #C74 
* #C74.9 "Nebenniere o.n.A."
* #C74.9 ^designation[0].value = "Adrenal o.n.A." 
* #C74.9 ^designation[1].value = "Glandula suprarenalis" 
* #C74.9 ^property[0].code = #parent 
* #C74.9 ^property[0].valueCode = #C74 
* #C75 "Sonstige endokrine Drüsen und verwandte Strukturen"
* #C75 ^property[0].code = #parent 
* #C75 ^property[0].valueCode = #C73-C75 
* #C75 ^property[1].code = #child 
* #C75 ^property[1].valueCode = #C75.0 
* #C75 ^property[2].code = #child 
* #C75 ^property[2].valueCode = #C75.1 
* #C75 ^property[3].code = #child 
* #C75 ^property[3].valueCode = #C75.2 
* #C75 ^property[4].code = #child 
* #C75 ^property[4].valueCode = #C75.3 
* #C75 ^property[5].code = #child 
* #C75 ^property[5].valueCode = #C75.4 
* #C75 ^property[6].code = #child 
* #C75 ^property[6].valueCode = #C75.5 
* #C75 ^property[7].code = #child 
* #C75 ^property[7].valueCode = #C75.8 
* #C75 ^property[8].code = #child 
* #C75 ^property[8].valueCode = #C75.9 
* #C75.0 "Nebenschilddrüse"
* #C75.0 ^property[0].code = #parent 
* #C75.0 ^property[0].valueCode = #C75 
* #C75.1 "Hirnanhangsdrüse"
* #C75.1 ^designation[0].value = "Hypophysär" 
* #C75.1 ^designation[1].value = "Hypophyse" 
* #C75.1 ^designation[2].value = "Fossa hypophysialis" 
* #C75.1 ^designation[3].value = "Rathke-Tasche" 
* #C75.1 ^designation[4].value = "Sella turcica" 
* #C75.1 ^property[0].code = #parent 
* #C75.1 ^property[0].valueCode = #C75 
* #C75.2 "Ductus craniopharyngealis"
* #C75.2 ^property[0].code = #parent 
* #C75.2 ^property[0].valueCode = #C75 
* #C75.3 "Glandula pinealis"
* #C75.3 ^property[0].code = #parent 
* #C75.3 ^property[0].valueCode = #C75 
* #C75.4 "Glomus caroticum"
* #C75.4 ^property[0].code = #parent 
* #C75.4 ^property[0].valueCode = #C75 
* #C75.5 "Glomus aorticum und sonstige Paraganglien"
* #C75.5 ^designation[0].value = "Corpus coccygeum" 
* #C75.5 ^designation[1].value = "Glomus coccygeum" 
* #C75.5 ^designation[2].value = "Glomus jugulare" 
* #C75.5 ^designation[3].value = "Glomus paraaorticumZuckerkandl-Organ" 
* #C75.5 ^designation[4].value = "Paraganglion" 
* #C75.5 ^property[0].code = #parent 
* #C75.5 ^property[0].valueCode = #C75 
* #C75.8 "Endokrine Drüsen und verwandte Strukturen, mehrere Teilbereiche überlappend"
* #C75.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C75.8 ^designation[0].value = "Multiple endokrine DrüsenPluriglandulär" 
* #C75.8 ^property[0].code = #parent 
* #C75.8 ^property[0].valueCode = #C75 
* #C75.9 "Endokrine Drüse o.n.A."
* #C75.9 ^property[0].code = #parent 
* #C75.9 ^property[0].valueCode = #C75 
* #C76-C80 "Anderer oder mangelhaft bezeichneter Sitz"
* #C76-C80 ^property[0].code = #parent 
* #C76-C80 ^property[0].valueCode = #T 
* #C76-C80 ^property[1].code = #child 
* #C76-C80 ^property[1].valueCode = #C76 
* #C76-C80 ^property[2].code = #child 
* #C76-C80 ^property[2].valueCode = #C77 
* #C76-C80 ^property[3].code = #child 
* #C76-C80 ^property[3].valueCode = #C80 
* #C76 "Sonstiger oder mangelhaft bezeichneter Sitz"
* #C76 ^property[0].code = #parent 
* #C76 ^property[0].valueCode = #C76-C80 
* #C76 ^property[1].code = #child 
* #C76 ^property[1].valueCode = #C76.0 
* #C76 ^property[2].code = #child 
* #C76 ^property[2].valueCode = #C76.1 
* #C76 ^property[3].code = #child 
* #C76 ^property[3].valueCode = #C76.2 
* #C76 ^property[4].code = #child 
* #C76 ^property[4].valueCode = #C76.3 
* #C76 ^property[5].code = #child 
* #C76 ^property[5].valueCode = #C76.4 
* #C76 ^property[6].code = #child 
* #C76 ^property[6].valueCode = #C76.5 
* #C76 ^property[7].code = #child 
* #C76 ^property[7].valueCode = #C76.7 
* #C76 ^property[8].code = #child 
* #C76 ^property[8].valueCode = #C76.8 
* #C76.0 "Kopf, Gesicht oder Hals o.n.A."
* #C76.0 ^designation[0].value = "Halsregion o.n.A." 
* #C76.0 ^designation[1].value = "Kiefer o.n.A." 
* #C76.0 ^designation[2].value = "Nase o.n.A." 
* #C76.0 ^designation[3].value = "Supraklavikularregion o.n.A." 
* #C76.0 ^designation[4].value = "Wange o.n.A." 
* #C76.0 ^property[0].code = #parent 
* #C76.0 ^property[0].valueCode = #C76 
* #C76.1 "Thorax o.n.A."
* #C76.1 ^designation[0].value = "Axilla o.n.A." 
* #C76.1 ^designation[1].value = "Brustkorb o.n.A." 
* #C76.1 ^designation[2].value = "Brustwand o.n.A." 
* #C76.1 ^designation[3].value = "Infraklavikularregion o.n.A." 
* #C76.1 ^designation[4].value = "Intrathorakaler Sitz o.n.A." 
* #C76.1 ^designation[5].value = "Schulterblattregion o.n.A." 
* #C76.1 ^designation[6].value = "Thoraxwand o.n.A." 
* #C76.1 ^property[0].code = #parent 
* #C76.1 ^property[0].valueCode = #C76 
* #C76.2 "Abdomen o.n.A."
* #C76.2 ^designation[0].value = "Bauchdecke o.n.A." 
* #C76.2 ^designation[1].value = "Intraabdomineller Sitz o.n.A." 
* #C76.2 ^property[0].code = #parent 
* #C76.2 ^property[0].valueCode = #C76 
* #C76.3 "Becken o.n.A."
* #C76.3 ^designation[0].value = "Beckenboden o.n.A." 
* #C76.3 ^designation[1].value = "Damm o.n.A." 
* #C76.3 ^designation[2].value = "Fossa ischioanalis" 
* #C76.3 ^designation[3].value = "Gesäß o.n.A." 
* #C76.3 ^designation[4].value = "Glutealregion o.n.A." 
* #C76.3 ^designation[5].value = "Inguinalregion o.n.A." 
* #C76.3 ^designation[6].value = "Leiste o.n.A." 
* #C76.3 ^designation[7].value = "Perirektalregion o.n.A." 
* #C76.3 ^designation[8].value = "Präsakralregion o.n.A." 
* #C76.3 ^designation[9].value = "Regio coccygea" 
* #C76.3 ^designation[10].value = "Sakrokokzygealregion o.n.A." 
* #C76.3 ^designation[11].value = "Septum rectovaginale" 
* #C76.3 ^designation[12].value = "Septum rectovesicale" 
* #C76.3 ^property[0].code = #parent 
* #C76.3 ^property[0].valueCode = #C76 
* #C76.4 "Obere Extremität o.n.A."
* #C76.4 ^designation[0].value = "Arm o.n.A." 
* #C76.4 ^designation[1].value = "Daumen o.n.A." 
* #C76.4 ^designation[2].value = "Ellenbeuge o.n.A." 
* #C76.4 ^designation[3].value = "Ellbogen o.n.A." 
* #C76.4 ^designation[4].value = "Finger o.n.A." 
* #C76.4 ^designation[5].value = "Hand o.n.A." 
* #C76.4 ^designation[6].value = "Handwurzel o.n.A." 
* #C76.4 ^designation[7].value = "Oberarm o.n.A." 
* #C76.4 ^designation[8].value = "Schulter o.n.A." 
* #C76.4 ^designation[9].value = "Unterarm o.n.A." 
* #C76.4 ^property[0].code = #parent 
* #C76.4 ^property[0].valueCode = #C76 
* #C76.5 "Untere Extremität o.n.A."
* #C76.5 ^designation[0].value = "Bein o.n.A." 
* #C76.5 ^designation[1].value = "Ferse o.n.A." 
* #C76.5 ^designation[2].value = "Fuß o.n.A." 
* #C76.5 ^designation[3].value = "Fußknöchel o.n.A." 
* #C76.5 ^designation[4].value = "Hüfte o.n.A." 
* #C76.5 ^designation[5].value = "Knie o.n.A." 
* #C76.5 ^designation[6].value = "Kniekehle o.n.A." 
* #C76.5 ^designation[7].value = "Oberschenkel o.n.A." 
* #C76.5 ^designation[8].value = "Unterschenkel o.n.A." 
* #C76.5 ^designation[9].value = "Wade o.n.A." 
* #C76.5 ^designation[10].value = "Zehe o.n.A." 
* #C76.5 ^property[0].code = #parent 
* #C76.5 ^property[0].valueCode = #C76 
* #C76.7 "Sonstiger mangelhaft bezeichneter Sitz"
* #C76.7 ^designation[0].value = "Flanke o.n.A." 
* #C76.7 ^designation[1].value = "Rücken o.n.A." 
* #C76.7 ^designation[2].value = "Stamm o.n.A." 
* #C76.7 ^property[0].code = #parent 
* #C76.7 ^property[0].valueCode = #C76 
* #C76.8 "Mangelhaft bezeichneter Sitz, mehrere Teilbereiche überlappend"
* #C76.8 ^definition = siehe Anmerkung am Anfang des Abschnittes Topographie
* #C76.8 ^property[0].code = #parent 
* #C76.8 ^property[0].valueCode = #C76 
* #C77 "Lymphknoten"
* #C77 ^property[0].code = #parent 
* #C77 ^property[0].valueCode = #C76-C80 
* #C77 ^property[1].code = #child 
* #C77 ^property[1].valueCode = #C77.0 
* #C77 ^property[2].code = #child 
* #C77 ^property[2].valueCode = #C77.1 
* #C77 ^property[3].code = #child 
* #C77 ^property[3].valueCode = #C77.2 
* #C77 ^property[4].code = #child 
* #C77 ^property[4].valueCode = #C77.3 
* #C77 ^property[5].code = #child 
* #C77 ^property[5].valueCode = #C77.4 
* #C77 ^property[6].code = #child 
* #C77 ^property[6].valueCode = #C77.5 
* #C77 ^property[7].code = #child 
* #C77 ^property[7].valueCode = #C77.8 
* #C77 ^property[8].code = #child 
* #C77 ^property[8].valueCode = #C77.9 
* #C77.0 "Lymphknoten des Kopfes, des Gesichtes und des Halses"
* #C77.0 ^designation[0].value = "Aurikulärer Lymphknoten" 
* #C77.0 ^designation[1].value = "Fazialer Lymphknoten" 
* #C77.0 ^designation[2].value = "Jugulärer LymphknotenLn. jugulares" 
* #C77.0 ^designation[3].value = "Mandibulärer Lymphknoten" 
* #C77.0 ^designation[4].value = "Okzipitaler LymphknotenLn. occipitales" 
* #C77.0 ^designation[5].value = "Parotidealer LymphknotenLn. parotidei" 
* #C77.0 ^designation[6].value = "Präauriculärer LymphknotenLn. praeauriculares" 
* #C77.0 ^designation[7].value = "Prälaryngealer LymphknotenLn. praelaryngei" 
* #C77.0 ^designation[8].value = "Prätrachealer Lymphknoten" 
* #C77.0 ^designation[9].value = "Retropharyngealer LymphknotenLn. retropharyngei" 
* #C77.0 ^designation[10].value = "Skalenus-LymphknotenLn. cervicales profundi superiores" 
* #C77.0 ^designation[11].value = "Sublingualer LymphknotenLn. linguales" 
* #C77.0 ^designation[12].value = "Submandibulärer LymphknotenLn. submandibulares" 
* #C77.0 ^designation[13].value = "Submaxillärer LymphknotenLn. buccales" 
* #C77.0 ^designation[14].value = "Submentaler LymphknotenLn. submentales" 
* #C77.0 ^designation[15].value = "Supraklavikulärer LymphknotenLn. supraclaviculares" 
* #C77.0 ^designation[16].value = "Zervikaler LymphknotenLn. cervicales" 
* #C77.0 ^property[0].code = #parent 
* #C77.0 ^property[0].valueCode = #C77 
* #C77.1 "Intrathorakaler Lymphknoten"
* #C77.1 ^designation[0].value = "Bronchialer Lymphknoten" 
* #C77.1 ^designation[1].value = "Bronchopulmonaler LymphknotenLn. bronchopulmonales" 
* #C77.1 ^designation[2].value = "Hilärer Lymphknoten o.n.A." 
* #C77.1 ^designation[3].value = "HiluslymphknotenLunge" 
* #C77.1 ^designation[4].value = "Interkostaler LymphknotenLn. intercostales" 
* #C77.1 ^designation[5].value = "Lungenlymphknoten o.n.A.Ln. pulmonales o.n.A." 
* #C77.1 ^designation[6].value = "Mediastinaler Lymphknoten" 
* #C77.1 ^designation[7].value = "Ösophagealer Lymphknoten" 
* #C77.1 ^designation[8].value = "Parasternaler LymphknotenLn. parasternales" 
* #C77.1 ^designation[9].value = "Thorakaler Lymphknoten" 
* #C77.1 ^designation[10].value = "Trachealer Lymphknoten" 
* #C77.1 ^designation[11].value = "Tracheobronchialer LymphknotenLn. tracheobronchiales" 
* #C77.1 ^designation[12].value = "ZwerchfellymphknotenLn. phrenici" 
* #C77.1 ^property[0].code = #parent 
* #C77.1 ^property[0].valueCode = #C77 
* #C77.2 "Intraabdominaler Lymphknoten"
* #C77.2 ^designation[0].value = "Abdominaler Lymphknoten" 
* #C77.2 ^designation[1].value = "Aortaler Lymphknoten" 
* #C77.2 ^designation[2].value = "Gastraler LymphknotenLn. gastrici" 
* #C77.2 ^designation[3].value = "Hepatischer LymphknotenLn. hepatici" 
* #C77.2 ^designation[4].value = "Ileokolischer LymphknotenLn. ileocolici" 
* #C77.2 ^designation[5].value = "Intestinaler LymphknotenLn. abdominales viscerales" 
* #C77.2 ^designation[6].value = "Ln. mesenterici inferiores" 
* #C77.2 ^designation[7].value = "Ln. mesenterici superiores" 
* #C77.2 ^designation[8].value = "Lumbaler LymphknotenLn. lumbales" 
* #C77.2 ^designation[9].value = "Lymphknoten am ductus hepaticus communis" 
* #C77.2 ^designation[10].value = "Lymphknoten am Leberhilus" 
* #C77.2 ^designation[11].value = "Lymphknoten am Milzhilus" 
* #C77.2 ^designation[12].value = "Mesenterialer Lymphknoten o.n.A.Ln. mesenterici o.n.A." 
* #C77.2 ^designation[13].value = "Mesokolischer LymphknotenLn. mesocolici" 
* #C77.2 ^designation[14].value = "MilzlymphknotenLn. lienales" 
* #C77.2 ^designation[15].value = "Ln. colici medii" 
* #C77.2 ^designation[16].value = "Pankreatischer Lymphknoten o.n.A.Ln. pancreatici o.n.A." 
* #C77.2 ^designation[17].value = "Paraaortaler Lymphknoten" 
* #C77.2 ^designation[18].value = "Periaortaler Lymphknoten" 
* #C77.2 ^designation[19].value = "Peripankreatischer Lymphknoten" 
* #C77.2 ^designation[20].value = "Portaler Lymphknoten" 
* #C77.2 ^designation[21].value = "Pylorischer LymphknotenLn. pylorici" 
* #C77.2 ^designation[22].value = "Retroperitonealer Lymphknoten" 
* #C77.2 ^designation[23].value = "Zöliakaler LymphknotenLn. coeliaci" 
* #C77.2 ^property[0].code = #parent 
* #C77.2 ^property[0].valueCode = #C77 
* #C77.3 "Lymphknoten der Achseln und Arme"
* #C77.3 ^designation[0].value = "Axillärer LymphknotenLn. axillares" 
* #C77.3 ^designation[1].value = "Epitrochlearer Lymphknoten" 
* #C77.3 ^designation[2].value = "Infraklavikulärer LymphknotenLn. infraclaviculares" 
* #C77.3 ^designation[3].value = "Kubitaler LymphknotenLn. cubitales" 
* #C77.3 ^designation[4].value = "Lateraler AchsellymphknotenLn. brachiales" 
* #C77.3 ^designation[5].value = "Lymphknoten der oberen Extremität" 
* #C77.3 ^designation[6].value = "Pektoraler LymphknotenLn. axillares pectorales" 
* #C77.3 ^designation[7].value = "Subklavikulärer LymphknotenLn. subclaviculares" 
* #C77.3 ^designation[8].value = "Subskapularer LymphknotenLn. axillares subscapulares" 
* #C77.3 ^property[0].code = #parent 
* #C77.3 ^property[0].valueCode = #C77 
* #C77.4 "Lymphknoten der Inguinalregion und des Beines"
* #C77.4 ^designation[0].value = "Cloquet-Lymphknoten" 
* #C77.4 ^designation[1].value = "Femoraler Lymphknoten" 
* #C77.4 ^designation[2].value = "Inguinaler Lymphknoten" 
* #C77.4 ^designation[3].value = "Leistenlymphknoten" 
* #C77.4 ^designation[4].value = "Lymphknoten der unteren Extremität" 
* #C77.4 ^designation[5].value = "Poplitealer LymphknotenLn. popliteales" 
* #C77.4 ^designation[6].value = "Rosenmüller- Lymphknoten" 
* #C77.4 ^designation[7].value = "SchienbeinlymphknotenLn. tibialis anterior" 
* #C77.4 ^designation[8].value = "Tiefer LeistenlymphknotenLn. inguinales profundi" 
* #C77.4 ^property[0].code = #parent 
* #C77.4 ^property[0].valueCode = #C77 
* #C77.5 "Beckenlymphknoten"
* #C77.5 ^designation[0].value = "Hypogastrische LymphknotenLn. hypogastrici" 
* #C77.5 ^designation[1].value = "Ln. iliaci interni" 
* #C77.5 ^designation[2].value = "Lymphknoten der Parametrien" 
* #C77.5 ^designation[3].value = "Lymphknoten entlang der A. obturatoriaLn. obturatorii" 
* #C77.5 ^designation[4].value = "Lymphknoten vor der Symphyse" 
* #C77.5 ^designation[5].value = "Parazervikaler LymphknotenLn. parauterini" 
* #C77.5 ^designation[6].value = "Sakraler LymphknotenLn. sacrales" 
* #C77.5 ^designation[7].value = "Unterer epigastrischer Lymphknoten" 
* #C77.5 ^property[0].code = #parent 
* #C77.5 ^property[0].valueCode = #C77 
* #C77.8 "Lymphknoten mehrerer Regionen"
* #C77.8 ^property[0].code = #parent 
* #C77.8 ^property[0].valueCode = #C77 
* #C77.9 "Lymphknoten o.n.A."
* #C77.9 ^property[0].code = #parent 
* #C77.9 ^property[0].valueCode = #C77 
* #C80 "Unbekannte Primärlokalisation"
* #C80 ^property[0].code = #parent 
* #C80 ^property[0].valueCode = #C76-C80 
* #C80 ^property[1].code = #child 
* #C80 ^property[1].valueCode = #C80.9 
* #C80.9 "Unbekannte Primärlokalisation"
* #C80.9 ^property[0].code = #parent 
* #C80.9 ^property[0].valueCode = #C80 
