Instance: elga-serviceeventperformer 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-serviceeventperformer" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-serviceeventperformer" 
* name = "elga-serviceeventperformer" 
* title = "ELGA_ServiceEventPerformer" 
* status = #active 
* version = "1.3.0+20240820" 
* description = "**Description:** Codes to distinguish performer(s) of an event

**Beschreibung:** Value Set zur Unterscheidung des Service Event Performers" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.43" 
* date = "2024-08-20" 
* publisher = "ELGA GmbH" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.90"
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/v3-ParticipationType"
* compose.include[0].version = "4.0.0"
* compose.include[0].concept[0].code = #PRF
* compose.include[0].concept[0].display = "performer"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Ausführender" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[1].value = "A person, non-person living subject, organization or device that who actually and principally carries out the action. Device should only be assigned as a performer in circumstances where the device is performing independent of human intervention. Need not be the principal responsible actor." 
* compose.include[0].concept[1].code = #PPRF
* compose.include[0].concept[1].display = "primary performer"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Hauptausführender" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[1].value = "The principal or primary performer of the act." 
* compose.include[0].concept[2].code = #SPRF
* compose.include[0].concept[2].display = "secondary performer"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Weiterer Ausführender" 
* compose.include[0].concept[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[1].value = "A person assisting in an act through his substantial presence and involvement This includes: assistants, technicians, associates, or whatever the job titles may be." 

* expansion.timestamp = "2024-08-20T09:00:32.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/v3-ParticipationType"
* expansion.contains[0].version = "4.0.0"
* expansion.contains[0].code = #PRF
* expansion.contains[0].display = "performer"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Ausführender" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[1].value = "A person, non-person living subject, organization or device that who actually and principally carries out the action. Device should only be assigned as a performer in circumstances where the device is performing independent of human intervention. Need not be the principal responsible actor." 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.90"
* expansion.contains[0].contains[0].system = "http://terminology.hl7.org/CodeSystem/v3-ParticipationType"
* expansion.contains[0].contains[0].version = "4.0.0"
* expansion.contains[0].contains[0].code = #PPRF
* expansion.contains[0].contains[0].display = "primary performer"
* expansion.contains[0].contains[0].designation[0].language = #de-AT 
* expansion.contains[0].contains[0].designation[0].value = "Hauptausführender" 
* expansion.contains[0].contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].contains[0].designation[1].value = "The principal or primary performer of the act." 
* expansion.contains[0].contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.90"
* expansion.contains[0].contains[1].system = "http://terminology.hl7.org/CodeSystem/v3-ParticipationType"
* expansion.contains[0].contains[1].version = "4.0.0"
* expansion.contains[0].contains[1].code = #SPRF
* expansion.contains[0].contains[1].display = "secondary performer"
* expansion.contains[0].contains[1].designation[0].language = #de-AT 
* expansion.contains[0].contains[1].designation[0].value = "Weiterer Ausführender" 
* expansion.contains[0].contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].contains[1].designation[1].value = "A person assisting in an act through his substantial presence and involvement This includes: assistants, technicians, associates, or whatever the job titles may be." 
* expansion.contains[0].contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.90"
