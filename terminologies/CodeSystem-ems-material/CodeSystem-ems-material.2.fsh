Instance: ems-material 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "ems-material" 
* url = "https://termgit.elga.gv.at/CodeSystem/ems-material" 
* name = "ems-material" 
* title = "EMS_Material" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230131" 
* description = "Liste der Probenmaterialien für EMS Labormeldungen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.58" 
* date = "2020-11-06" 
* count = 65 
* #ABSTRICH "Abstriche"
* #ABSZESS "Abszess"
* #ANAL "Rektalabstrich"
* #ANAL ^definition = Analabstrich | Rektalabstrich | Stuhl/Analabstrich
* #ASCITES "Ascites"
* #BIOPSIE "Biopsiematerial"
* #BIOPSIE ^definition = Biopsate | Biopsie (periphere Nerven) | Biopsiematerial
* #BLOOD "Blut"
* #BLOOD ^definition = Blut | Blut (DNA/Leukozyten)
* #BLOODAUS "Blutausstrich"
* #BLOODDRY "getrockneter Bluttropfen"
* #BLOODFULL "Vollblut"
* #BLOODPOST "Leichenblut"
* #BRONCHLAV "Bronchiallavage"
* #BRONCHSEKR "Bronchialsekret"
* #CERVSWAB "Cervicalabstrich"
* #CSF "Liquor"
* #EDTA "EDTA-Blut"
* #EDTAPLAS "EDTA-Blut/Plas,a"
* #EDTAPLAS ^definition = EDTA Blut/Plasma
* #EITER "Eiter"
* #EJAKULAT "Ejakulat"
* #ERBR "Erbrochenes"
* #EXSUPERI "Exsudat Peritoneum"
* #EXSUPLEURA "Exsudat Pleura"
* #GALLE "Gallenflüssigkeit"
* #GELENK "Gelenkflüssigkeit"
* #GEWEBE "Gewebe"
* #GEWEBE ^definition = Gewebeprobe | Gewebeproben | Epithelabstrich
* #GURGELAT "Gurgelat"
* #HAUT "Haut"
* #HAUT ^definition = Abstrich von Hautläsionen | Haut | Hautbiopsie oder Aspirat von Purpura/Petechien | verdächtige Hautareale
* #HIRN "Hirn"
* #HIRN ^definition = Hirnbiopsie/ -gewebe | Hirnbiopsie-Gewebe | Hirngewebe
* #ISOLAT "Isolat"
* #ISOLAT ^definition = Bakterienisolat | Isolat | Isolate
* #KNOCHENMARK "Knochenmark"
* #KRUSTE "Kruste"
* #LAVAGE "Lavage"
* #LEBER "Leberbiopsie"
* #LUNGE "Lunge"
* #LUNGE ^definition = Lunge | Pleurapunktat
* #LYMPH "Lymphknoten"
* #LYMPH ^definition = Punktat von Lymphknoten | Resektionsmaterial aus Lymphknoten
* #MAGENSAFT "Magensaft"
* #MEMBRAN "Membran"
* #MILZ "Milz"
* #NACKENHAUT "Nackenhautbiopsie"
* #NASALSWAB "Nasenabstrich"
* #NASALTHROATSWAB "Rachenabstrich"
* #NASALTHROATSWAB ^definition = Abstrich Nasen-Rachenschleimhaut | Nasen- oder Nasen-/Rachenabstrich
* #NEWBORNSWAB "Neugeborenenabstrich"
* #OPMATERIAL "Operationsmaterial"
* #OPMATERIAL ^definition = intraoperatives Material | Operationsmaterial
* #OTHER "anderes"
* #OTHERBODY "andere erregerhaltige Körperflüssigkeiten"
* #OTHERNOTSTERILE "andere nicht sterile Probe"
* #OTHERNOTSTERILE ^definition = andere nicht sterile Probe | andere nicht-sterile Probe
* #OTHERSTERILE "andere sterile Probe"
* #OTHERSTERILE ^definition = andere sterile Probe | andere sterile Stelle
* #OTHERSWAB "andere Abstriche"
* #PLASMA "Plasma"
* #POSTMORTEM "post mortem-Material"
* #POSTMORTEM ^definition = post mortem-Material | Proben von Verstorbenen
* #PUNKTAT "Punktat"
* #PUNKTAT ^definition = Punktionsmaterial
* #RESPSEKR "Respiratorische Sekrete"
* #RNA "nCoV RNA"
* #SEKRETE "Sekrete"
* #SERUM "Serum"
* #SONST "sonstiges"
* #SPEICHEL "Speichel"
* #SPEICHEL ^definition = oral Fluid | Speichel | Speichel/oral fluid
* #SPRITZE "Spritzeninhalt"
* #SPUTUM "Sputum"
* #SPUTUM ^definition = Sputum | tiefes respiratorisches Sekret
* #STUHL "Stuhl"
* #THROATSWAB "Rachenabstrich"
* #THROATSWABFLUSS "Rachenspülflüssigkeit Serum"
* #TONS "Tonsillenabstrich"
* #UMGEB "Umgebungsprobe"
* #URINE "Harn"
* #URINE ^definition = Harn | Urin
* #WUND "Wundsekret"
* #WUND ^definition = Wundabstrich | Wundsekret
