Instance: medikationtherapieart 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "medikationtherapieart" 
* url = "https://termgit.elga.gv.at/CodeSystem/medikationtherapieart" 
* name = "medikationtherapieart" 
* title = "MedikationTherapieart" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230131" 
* description = "**Description:** ELGA Codelist for type of therapy

**Beschreibung:** ELGA Codeliste für Therapieart" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.6" 
* date = "2015-03-31" 
* count = 2 
* #EINZEL "Einzelverordnung"
* #NICHTEINZEL "Nicht-Einzelverordnung"
