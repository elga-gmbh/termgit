Instance: elga-patienten-identifizierungsmethoden 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-patienten-identifizierungsmethoden" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-patienten-identifizierungsmethoden" 
* name = "elga-patienten-identifizierungsmethoden" 
* title = "ELGA_Patienten-Identifizierungsmethoden" 
* status = #active 
* content = #complete 
* version = "1.2.0+20240820" 
* description = "ELGA-Codeliste für Identifizierungsmethoden von Patienten" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.162" 
* date = "2024-08-20" 
* publisher = "ELGA GmbH" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 6 
* #PIM101 "e-card wurde in den Kartenleser gesteckt"
* #PIM102 "Bürgerkarte wurde in den Kartenleser gesteckt"
* #PIM103 "Über lokalen Patientenindex identifiziert"
* #PIM104 "Patient ist identifiziert via e-card System ohne gesteckte e-card"
* #PIM105 "Identifikation anhand amtlichen Lichtbildausweis oder e-card für mobile e-Impfpass Anwendung"
* #PIM106 "Zuweisung"
