Instance: gda-attribute 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "gda-attribute" 
* url = "https://termgit.elga.gv.at/CodeSystem/gda-attribute" 
* name = "gda-attribute" 
* title = "GDA_Attribute" 
* status = #active 
* content = #complete 
* version = "1.1.0+20240820" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.4" 
* date = "2024-08-20" 
* publisher = "ELGA GmbH" 
* contact[0].name = "http://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.elga.gv.at" 
* count = 3 
* #SSCHG "Ermächtigung gemäß § 35 Strahlenschutzgesetz"
* #SUBUB "Berechtigung zur umfassenden Substitutionsbehandlung"
* #WBSUB "Berechtigung zur Weiterbehandlung Substitution"
