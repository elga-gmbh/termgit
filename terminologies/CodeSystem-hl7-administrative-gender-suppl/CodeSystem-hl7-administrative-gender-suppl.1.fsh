Instance: hl7-administrative-gender-suppl 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "hl7-administrative-gender-suppl" 
* url = "https://termgit.elga.gv.at/CodeSystem/hl7-administrative-gender-suppl" 
* name = "hl7-administrative-gender-suppl" 
* title = "HL7_Administrative-Gender_Suppl" 
* status = #active 
* content = #supplement 
* version = "1.0.0+20250304" 
* description = "Supplement zum Administrative-Gender-Codesystem von HL7.org mit deutschsprachigen Designations." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.230" 
* date = "2025-03-04" 
* caseSensitive = "True" 
* supplements = "http://hl7.org/fhir/administrative-gender" 
* count = 4 
* concept[0].code = #female
* concept[0].display = "Female"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Weiblich" 
* concept[1].code = #male
* concept[1].display = "Male"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Männlich" 
* concept[2].code = #other
* concept[2].display = "Other"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "Nicht-binär" 
* concept[3].code = #unknown
* concept[3].display = "Unknown"
* concept[3].designation[0].language = #de-AT 
* concept[3].designation[0].value = "Unbekannt" 
