Instance: hl7-administrative-gender-suppl 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "hl7-administrative-gender-suppl" 
* url = "https://termgit.elga.gv.at/CodeSystem/hl7-administrative-gender-suppl" 
* name = "hl7-administrative-gender-suppl" 
* title = "HL7_Administrative-Gender_Suppl" 
* status = #active 
* content = #supplement 
* version = "1.0.0+20250304" 
* description = "Supplement zum Administrative-Gender-Codesystem von HL7.org mit deutschsprachigen Designations." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.230" 
* date = "2025-03-04" 
* caseSensitive = "True" 
* supplements = "http://hl7.org/fhir/administrative-gender" 
* count = 4 
* #female "Female"
* #female ^designation[0].language = #de-AT 
* #female ^designation[0].value = "Weiblich" 
* #male "Male"
* #male ^designation[0].language = #de-AT 
* #male ^designation[0].value = "Männlich" 
* #other "Other"
* #other ^designation[0].language = #de-AT 
* #other ^designation[0].value = "Nicht-binär" 
* #unknown "Unknown"
* #unknown ^designation[0].language = #de-AT 
* #unknown ^designation[0].value = "Unbekannt" 
