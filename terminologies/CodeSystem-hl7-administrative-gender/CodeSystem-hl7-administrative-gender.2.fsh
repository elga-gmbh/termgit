Instance: hl7-administrative-gender 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "hl7-administrative-gender" 
* url = "https://termgit.elga.gv.at/CodeSystem/hl7-administrative-gender" 
* name = "hl7-administrative-gender" 
* title = "HL7 Administrative Gender" 
* status = #retired 
* content = #complete 
* version = "1.0.0+20230131" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.1" 
* date = "2023-06-30" 
* count = 3 
* #F "Female"
* #F ^designation[0].language = #de-AT 
* #F ^designation[0].value = "Weiblich" 
* #M "Male"
* #M ^designation[0].language = #de-AT 
* #M ^designation[0].value = "Männlich" 
* #UN "Undifferentiated"
* #UN ^designation[0].language = #de-AT 
* #UN ^designation[0].value = "Divers" 
