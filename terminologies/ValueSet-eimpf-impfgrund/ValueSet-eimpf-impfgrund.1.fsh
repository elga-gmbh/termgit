Instance: eimpf-impfgrund 
InstanceOf: ValueSet 
Usage: #definition 
* id = "eimpf-impfgrund" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/eimpf-impfgrund" 
* name = "eimpf-impfgrund" 
* title = "eImpf_Impfgrund" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "**Description:** Billing-related reasons for vaccinations

**Beschreibung:** Abrechnungsrelevante Gründe für Impfungen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.7" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* compose.include[0].version = "2.0.0+20230131"
* compose.include[0].concept[0].code = #IG1
* compose.include[0].concept[0].display = "Indikationsimpfung für Risikogruppe"
* compose.include[0].concept[1].code = #IG2
* compose.include[0].concept[1].display = "Wiederholungsimpfung aufgrund medizinischer Indikation"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[0].version = "2.0.0+20230131"
* expansion.contains[0].code = #IG1
* expansion.contains[0].display = "Indikationsimpfung für Risikogruppe"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[1].version = "2.0.0+20230131"
* expansion.contains[1].code = #IG2
* expansion.contains[1].display = "Wiederholungsimpfung aufgrund medizinischer Indikation"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
