<?xml version="1.0" ?>
<ValueSet xmlns="http://hl7.org/fhir">
    <id value="elga-dosisparameter"/>
    <meta>
        <profile value="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset"/>
    </meta>
    <url value="https://termgit.elga.gv.at/ValueSet/elga-dosisparameter"/>
    <identifier>
        <use value="official"/>
        <system value="urn:ietf:rfc:3986"/>
        <value value="urn:oid:1.2.40.0.34.10.166"/>
    </identifier>
    <version value="1.2.0+20240411"/>
    <name value="elga-dosisparameter"/>
    <title value="ELGA_Dosisparameter"/>
    <status value="active"/>
    <date value="2024-04-11"/>
    <description value="Codes für Dosisparameter zur Ermittlung der Patientendosis"/>
    <compose>
        <include>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.840.10008.2.16.4"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
            <version value="1.0.0+20230131"/>
            <concept>
                <code value="111636"/>
                <display value="Entrance Exposure at RP"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Eingangsdosis"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_codiert"/>
                        <display value="einheit_codiert"/>
                    </use>
                    <value value="mGy"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_print"/>
                        <display value="einheit_print"/>
                    </use>
                    <value value="mGy"/>
                </designation>
            </concept>
            <concept>
                <code value="111637"/>
                <display value="Accumulated Average Glandular Dose"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Mittlere Parenchymdosis (AGD)"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_codiert"/>
                        <display value="einheit_codiert"/>
                    </use>
                    <value value="mGy"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_print"/>
                        <display value="einheit_print"/>
                    </use>
                    <value value="mGy"/>
                </designation>
            </concept>
            <concept>
                <code value="113507"/>
                <display value="Administered activity"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Applizierte Aktivität"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_codiert"/>
                        <display value="einheit_codiert"/>
                    </use>
                    <value value="MBq"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_print"/>
                        <display value="einheit_print"/>
                    </use>
                    <value value="MBq"/>
                </designation>
            </concept>
            <concept>
                <code value="113722"/>
                <display value="Dose Area Product Total"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Dosisflächenprodukt (DAP)"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_codiert"/>
                        <display value="einheit_codiert"/>
                    </use>
                    <value value="cGy.cm2"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_print"/>
                        <display value="einheit_print"/>
                    </use>
                    <value value="cGycm²"/>
                </designation>
            </concept>
            <concept>
                <code value="113813"/>
                <display value="CT Dose Length Product Total"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Dosislängenprodukt (DLP)"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_codiert"/>
                        <display value="einheit_codiert"/>
                    </use>
                    <value value="mGy.cm"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_print"/>
                        <display value="einheit_print"/>
                    </use>
                    <value value="mGycm"/>
                </designation>
            </concept>
            <concept>
                <code value="113839"/>
                <display value="Effective Dose"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Effektive Dosis"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_codiert"/>
                        <display value="einheit_codiert"/>
                    </use>
                    <value value="mSv"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                        <code value="einheit_print"/>
                        <display value="einheit_print"/>
                    </use>
                    <value value="mSv"/>
                </designation>
            </concept>
        </include>
    </compose>
    <expansion>
        <timestamp value="2024-04-11T14:26:59.0000Z"/>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.840.10008.2.16.4"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
            <version value="1.0.0+20230131"/>
            <code value="111636"/>
            <display value="Entrance Exposure at RP"/>
            <designation>
                <language value="de-AT"/>
                <value value="Eingangsdosis"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_codiert"/>
                    <display value="einheit_codiert"/>
                </use>
                <value value="mGy"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_print"/>
                    <display value="einheit_print"/>
                </use>
                <value value="mGy"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.840.10008.2.16.4"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
            <version value="1.0.0+20230131"/>
            <code value="111637"/>
            <display value="Accumulated Average Glandular Dose"/>
            <designation>
                <language value="de-AT"/>
                <value value="Mittlere Parenchymdosis (AGD)"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_codiert"/>
                    <display value="einheit_codiert"/>
                </use>
                <value value="mGy"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_print"/>
                    <display value="einheit_print"/>
                </use>
                <value value="mGy"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.840.10008.2.16.4"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
            <version value="1.0.0+20230131"/>
            <code value="113507"/>
            <display value="Administered activity"/>
            <designation>
                <language value="de-AT"/>
                <value value="Applizierte Aktivität"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_codiert"/>
                    <display value="einheit_codiert"/>
                </use>
                <value value="MBq"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_print"/>
                    <display value="einheit_print"/>
                </use>
                <value value="MBq"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.840.10008.2.16.4"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
            <version value="1.0.0+20230131"/>
            <code value="113722"/>
            <display value="Dose Area Product Total"/>
            <designation>
                <language value="de-AT"/>
                <value value="Dosisflächenprodukt (DAP)"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_codiert"/>
                    <display value="einheit_codiert"/>
                </use>
                <value value="cGy.cm2"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_print"/>
                    <display value="einheit_print"/>
                </use>
                <value value="cGycm²"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.840.10008.2.16.4"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
            <version value="1.0.0+20230131"/>
            <code value="113813"/>
            <display value="CT Dose Length Product Total"/>
            <designation>
                <language value="de-AT"/>
                <value value="Dosislängenprodukt (DLP)"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_codiert"/>
                    <display value="einheit_codiert"/>
                </use>
                <value value="mGy.cm"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_print"/>
                    <display value="einheit_print"/>
                </use>
                <value value="mGycm"/>
            </designation>
        </contains>
        <contains>
            <extension url="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid">
                <valueOid value="urn:oid:1.2.840.10008.2.16.4"/>
            </extension>
            <system value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
            <version value="1.0.0+20230131"/>
            <code value="113839"/>
            <display value="Effective Dose"/>
            <designation>
                <language value="de-AT"/>
                <value value="Effektive Dosis"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_codiert"/>
                    <display value="einheit_codiert"/>
                </use>
                <value value="mSv"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem/austrian-designation-use"/>
                    <code value="einheit_print"/>
                    <display value="einheit_print"/>
                </use>
                <value value="mSv"/>
            </designation>
        </contains>
    </expansion>
</ValueSet>
